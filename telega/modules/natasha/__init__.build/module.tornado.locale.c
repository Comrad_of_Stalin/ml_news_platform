/* Generated code for Python module 'tornado.locale'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_tornado$locale" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_tornado$locale;
PyDictObject *moduledict_tornado$locale;

/* The declarations of module constants used, if any. */
extern PyObject *const_float_60_0;
extern PyObject *const_int_pos_12;
extern PyObject *const_str_plain_gen_log;
static PyObject *const_str_digest_aa2fa87725efc213396ce7440d599ef0;
extern PyObject *const_str_plain_Saturday;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain_str_time;
static PyObject *const_tuple_str_plain_name_str_plain_Unknown_tuple;
extern PyObject *const_str_plain___name__;
static PyObject *const_dict_dd2827a37a0ea3d35f64079aff2696f9;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_str_plain_locale;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_object;
static PyObject *const_tuple_str_digest_c96f4f5d520728e597b7aaa11664f9e1_tuple;
static PyObject *const_tuple_str_digest_3574e84f2a7e015bd543ca78e33acf99_tuple;
static PyObject *const_str_plain_October;
extern PyObject *const_tuple_int_0_true_tuple;
extern PyObject *const_str_plain_bool;
static PyObject *const_str_digest_d2d3529124bbf79c28bd4bb4788ed236;
static PyObject *const_str_digest_3ac0bb3f70842f105c27dddcb88c1d26;
extern PyObject *const_tuple_str_plain___tuple;
extern PyObject *const_str_plain_hour;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
static PyObject *const_tuple_str_plain_August_tuple;
static PyObject *const_str_digest_4e0679129fefff8661d681147edadefe;
extern PyObject *const_int_pos_5;
static PyObject *const_str_digest_ff2e73506fa67da0b46f1c793668749b;
static PyObject *const_tuple_str_plain_November_tuple;
static PyObject *const_str_plain_LC_MESSAGES;
extern PyObject *const_str_plain_utcfromtimestamp;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_minutes;
extern PyObject *const_str_plain_typing;
static PyObject *const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple;
extern PyObject *const_slice_none_int_pos_2_none;
static PyObject *const_str_plain_get_supported_locales;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_pgettext;
extern PyObject *const_str_plain_am;
static PyObject *const_tuple_str_plain_Saturday_tuple;
extern PyObject *const_str_plain_time;
static PyObject *const_tuple_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf_tuple;
extern PyObject *const_str_plain_str;
static PyObject *const_str_digest_e157804d84d435f58356cd3da9c183e4;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_chr_45;
static PyObject *const_str_plain_plural_message;
extern PyObject *const_str_plain_seconds;
static PyObject *const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple;
extern PyObject *const_str_plain_rtl;
static PyObject *const_str_plain_CSVLocale;
extern PyObject *const_str_plain_listdir;
static PyObject *const_str_plain_set_default_locale;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_str_plain_Wednesday;
extern PyObject *const_str_plain_reversed;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_c105a91b9f0e7aa7ab96931fa498bc8b;
extern PyObject *const_str_plain_endswith;
extern PyObject *const_str_plain_en;
static PyObject *const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple;
static PyObject *const_str_plain_message_dict;
extern PyObject *const_str_plain_False;
static PyObject *const_tuple_str_plain_fa_str_plain_ar_str_plain_he_tuple;
static PyObject *const_tuple_str_plain_fa_tuple;
extern PyObject *const_str_plain_tornado;
static PyObject *const_str_digest_9a4cda2f32b95b957c2fd58ebb08c92a;
static PyObject *const_str_digest_c9c85d91a6f9ef6ae7ba064fc7185109;
static PyObject *const_str_plain_load_translations;
extern PyObject *const_int_0;
static PyObject *const_tuple_f7ae2cf4d4b99e7e88e67184ff4a3fea_tuple;
static PyObject *const_dict_3fe11010327cbd4c16bbcf45efc3eecd;
extern PyObject *const_str_plain_code;
static PyObject *const_str_digest_bef59eea50ee627e5c737ba8285a3699;
extern PyObject *const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
extern PyObject *const_tuple_type_int_type_float_tuple;
static PyObject *const_str_digest_0a7809271042678b67580f28ea6c38b7;
extern PyObject *const_str_digest_d19a843ecd797530113ead5e37e2513b;
extern PyObject *const_str_plain_year;
extern PyObject *const_str_plain_setdefault;
extern PyObject *const_slice_int_neg_3_none_none;
extern PyObject *const_str_plain_type;
extern PyObject *const_int_neg_3;
extern PyObject *const_dict_18140f7b7a6bfded467b20acf0df2e9b;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_difference;
extern PyObject *const_str_digest_b92226bee77e9ca773ad5eaea1d64ab0;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
static PyObject *const_str_plain_NullTranslations;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_csv;
extern PyObject *const_str_plain_translate;
static PyObject *const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple;
static PyObject *const_str_digest_4a0c6d12249351433c374c8e2dac95fa;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_startswith;
static PyObject *const_str_plain_local_yesterday;
static PyObject *const_tuple_str_digest_4a0c6d12249351433c374c8e2dac95fa_tuple;
extern PyObject *const_str_plain_Monday;
static PyObject *const_tuple_str_plain_plural_str_plain_singular_str_plain_unknown_tuple;
static PyObject *const_str_plain_dow;
extern PyObject *const_str_plain_full_path;
static PyObject *const_str_plain_month_name;
extern PyObject *const_str_plain_timedelta;
extern PyObject *const_tuple_str_plain_c_tuple;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_str_digest_daaa94ec39bdc51a92e87ff9abd20aad_tuple;
static PyObject *const_str_digest_c2f26402c280cea934f7a4889cf0b12a;
static PyObject *const_str_digest_475df1123b507a27d0e67c96974cd632;
static PyObject *const_tuple_str_plain_Sunday_tuple;
extern PyObject *const_tuple_str_plain_escape_tuple;
extern PyObject *const_str_plain_reader;
static PyObject *const_tuple_str_digest_7871fba5268138f3163960862e12ec1d_tuple;
extern PyObject *const_str_plain_Dict;
extern PyObject *const_str_plain_strip;
extern PyObject *const_int_pos_3000;
extern PyObject *const_str_chr_4;
extern PyObject *const_str_plain_read;
static PyObject *const_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf;
static PyObject *const_str_digest_7f7f8e2802dcb3f5d1279d9ba6ecfe7c;
static PyObject *const_tuple_str_plain_September_tuple;
extern PyObject *const_str_plain_lower;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_556bc24351716bafdca56a89def05747;
extern PyObject *const_tuple_str_plain_gen_log_tuple;
static PyObject *const_str_digest_f499141fb159d3c1f4c2312c62754b04;
static PyObject *const_tuple_str_digest_a007ef1d9765d8486eb48b306c7e3511_tuple;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_hours;
extern PyObject *const_str_digest_03cf11698c63a9ec6d05e7924bb93e03;
static PyObject *const_str_digest_9d183a9f7777afb52500f6cd3f94fde8;
static PyObject *const_tuple_str_plain_December_tuple;
extern PyObject *const_str_plain_value;
static PyObject *const_str_digest_c1f66a265abaea28dae1f02b5da97e35;
static PyObject *const_str_plain_locale_codes;
static PyObject *const_str_plain_gmt_offset;
static PyObject *const_str_plain_local_date;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_message;
extern PyObject *const_str_plain_context;
static PyObject *const_str_digest_bc4c3b36cdd00cb6e1d6ca8f27b40a93;
extern PyObject *const_str_plain_BOM_UTF16_BE;
extern PyObject *const_str_plain_enumerate;
extern PyObject *const_str_plain_datetime;
static PyObject *const_tuple_str_plain_self_str_plain_parts_str_plain___str_plain_comma_tuple;
static PyObject *const_tuple_str_digest_eb0f9b85ed1cef1f2122150e15335d0c_tuple;
static PyObject *const_str_plain_January;
extern PyObject *const_str_plain_error;
static PyObject *const_str_digest_9c0911e86d5a69f513ffa9986416d31c;
static PyObject *const_tuple_str_digest_9d183a9f7777afb52500f6cd3f94fde8_tuple;
extern PyObject *const_int_pos_60;
static PyObject *const_str_digest_eb2e4d4363a413350a3b55abcc716ba6;
static PyObject *const_str_plain_GettextLocale;
static PyObject *const_str_plain__translations;
static PyObject *const_tuple_str_digest_626f47fee6196287729a98b52426b191_tuple;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_plain_November;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_plain_lang;
static PyObject *const_str_plain_translations;
extern PyObject *const_str_plain_upper;
static PyObject *const_str_digest_06670af157044a600ad097110ee7c367;
extern PyObject *const_str_plain_languages;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_warning;
static PyObject *const_str_digest_8673d2aaeb83e68b5a07fcf14c894cf2;
static PyObject *const_str_digest_4f619ff8041ae709fb03a4f58d086f92;
extern PyObject *const_tuple_str_chr_45_str_plain___tuple;
static PyObject *const_str_digest_052d7566e9844fc118a7de62c1266c2d;
extern PyObject *const_str_plain_ar;
static PyObject *const_str_plain_local_now;
static PyObject *const_str_digest_6703d883ad26971c7886a4bc827d59fa;
static PyObject *const_str_plain_friendly_number;
static PyObject *const_str_digest_695283f572d9bc5821fb828221e07f56;
extern PyObject *const_slice_none_int_neg_1_none;
static PyObject *const_str_digest_edc4e236c8c648a0aa64ccf415398bf9;
static PyObject *const_tuple_str_plain_am_str_plain_pm_tuple;
extern PyObject *const_str_plain_day;
static PyObject *const_tuple_str_plain_July_tuple;
static PyObject *const_str_plain_he;
static PyObject *const_str_plain_ngettext;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_days;
static PyObject *const_str_digest_e89810b6cdcb59d43c1349f24947e94c;
static PyObject *const_str_plain_english;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_date;
static PyObject *const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_Iterable;
static PyObject *const_str_digest_1f98531c7ff1755fa56a767f09a0a7a0;
extern PyObject *const_int_pos_334;
extern PyObject *const_tuple_none_none_tuple;
static PyObject *const_str_plain_CONTEXT_SEPARATOR;
extern PyObject *const_str_plain_bf;
static PyObject *const_tuple_str_plain_en_str_plain_en_US_str_plain_zh_CN_tuple;
static PyObject *const_str_plain_format_day;
extern PyObject *const_str_plain_result;
static PyObject *const_tuple_str_plain_yesterday_tuple;
static PyObject *const_tuple_1e6837b60802e5c143d36703de583e6f_tuple;
extern PyObject *const_int_pos_24;
static PyObject *const_str_plain__default_locale;
static PyObject *const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple;
static PyObject *const_tuple_str_plain_October_tuple;
extern PyObject *const_float_3600_0;
extern PyObject *const_str_plain_month;
static PyObject *const_tuple_str_plain_Thursday_tuple;
extern PyObject *const_str_plain_prefix;
static PyObject *const_str_digest_29385822239ba969122cbf138728dfbe;
extern PyObject *const_str_plain_to_unicode;
static PyObject *const_str_digest_c96f4f5d520728e597b7aaa11664f9e1;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain__weekdays;
extern PyObject *const_str_plain_now;
extern PyObject *const_str_plain_return;
static PyObject *const_str_digest_379c58f19104aad46c52cfb6c3902689;
static PyObject *const_str_plain_get_closest;
extern PyObject *const_str_plain_classmethod;
static PyObject *const_str_plain_shorter;
extern PyObject *const_str_plain_directory;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_relative;
static PyObject *const_str_plain_tfhour_clock;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain_BOM_UTF16_LE;
static PyObject *const_tuple_int_0_true_false_false_tuple;
static PyObject *const_str_plain_singular;
extern PyObject *const_str_plain__cache;
static PyObject *const_tuple_503c00455ace194f34ccb09c62c42099_tuple;
static PyObject *const_str_digest_0c060498692fd2cb6f8eedca3dc6fbab;
static PyObject *const_str_digest_7871fba5268138f3163960862e12ec1d;
static PyObject *const_str_digest_1214c21596fee4366c3ea77fd73e2614;
static PyObject *const_tuple_4098b9f363c2cd529fe597c0aaf71d10_tuple;
extern PyObject *const_str_plain_float;
static PyObject *const_str_digest_3505f180c48d7881a1fcd516b48114ef;
static PyObject *const_str_plain_msgs_with_ctxt;
static PyObject *const_str_digest_64acd9d2cb4096ad5db303736a6307e5;
static PyObject *const_str_digest_3f0e87ccc267dc20a6762772e63cb9c6;
static PyObject *const_tuple_str_plain_Friday_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_f5737bc588770cc5264c9b81f8874d8c;
static PyObject *const_str_plain_translation;
extern PyObject *const_str_plain_Any;
static PyObject *const_dict_ba6737774c95763b85c660cd7c84b8a4;
extern PyObject *const_str_plain_split;
static PyObject *const_str_plain_July;
static PyObject *const_dict_94af2e730a3b531141cb165a0fc9a983;
extern PyObject *const_str_plain_list;
static PyObject *const_str_digest_332c0ee92d2f3d71b4732ef2b5d796e2;
extern PyObject *const_str_plain_comma;
extern PyObject *const_slice_none_int_neg_3_none;
extern PyObject *const_str_digest_daaa94ec39bdc51a92e87ff9abd20aad;
static PyObject *const_str_digest_a2593816d0f28e619453b3faa3652e96;
static PyObject *const_str_digest_e1dd5bde31ecab95c214424d92ae147d;
static PyObject *const_tuple_str_plain_January_tuple;
static PyObject *const_str_plain_yesterday;
extern PyObject *const_str_plain_c;
static PyObject *const_str_digest_eb0f9b85ed1cef1f2122150e15335d0c;
static PyObject *const_str_plain_March;
extern PyObject *const_str_plain_Unknown;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain__;
extern PyObject *const_str_plain_Union;
static PyObject *const_tuple_str_plain_locale_codes_tuple;
extern PyObject *const_str_plain_extension;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_codecs;
static PyObject *const_str_plain_August;
extern PyObject *const_str_plain_debug;
static PyObject *const_tuple_str_plain_June_tuple;
static PyObject *const_tuple_829a096044442484452ce55ee5d19a78_tuple;
static PyObject *const_str_digest_2af2f0bb9895a7e9b768093a63173b6d;
static PyObject *const_str_digest_b27e80db6e1cdf2aa0d1c5081ea4a866;
static PyObject *const_str_digest_62523a4f5e0ca4a43f0e85f7a8d10754;
static PyObject *const_str_digest_c16be5bdd7c0f2cffe66ad43f1ff6e1c;
static PyObject *const_str_digest_626f47fee6196287729a98b52426b191;
static PyObject *const_str_digest_8bda2ae8a09ed125b91f60b17cc43541;
extern PyObject *const_str_plain_en_US;
static PyObject *const_str_plain_commas;
static PyObject *const_tuple_str_digest_b640953a68c487cbc89544f72b9babc3_tuple;
static PyObject *const_str_digest_915c1427e06b79b36242659fa2173cde;
extern PyObject *const_str_plain_get;
static PyObject *const_str_plain_load_gettext_translations;
extern PyObject *const_str_plain_escape;
static PyObject *const_str_plain_February;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_unknown;
extern PyObject *const_str_plain_plural;
extern PyObject *const_str_plain___exit__;
static PyObject *const_str_plain_gettext;
extern PyObject *const_str_plain_fa;
extern PyObject *const_str_plain_Thursday;
static PyObject *const_str_plain_December;
extern PyObject *const_str_plain_Locale;
extern PyObject *const_str_plain___enter__;
extern PyObject *const_str_plain_stat;
extern PyObject *const_str_plain_isfile;
extern PyObject *const_str_plain_cls;
static PyObject *const_tuple_str_plain_Monday_tuple;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_pm;
extern PyObject *const_str_dot;
static PyObject *const_tuple_str_plain_Tuesday_tuple;
extern PyObject *const_str_chr_44;
static PyObject *const_str_digest_b640953a68c487cbc89544f72b9babc3;
extern PyObject *const_str_plain_LOCALE_NAMES;
extern PyObject *const_str_plain_int;
static PyObject *const_tuple_15df13279735db77bab82178193ba3a7_tuple;
static PyObject *const_tuple_32523c33b2c5f7d616fdfabb493279a2_tuple;
static PyObject *const_str_plain_format_date;
static PyObject *const_str_digest_eab034a4d05d723cadb02b46db203b41;
extern PyObject *const_str_plain_parts;
extern PyObject *const_str_plain_keys;
extern PyObject *const_str_plain_rb;
extern PyObject *const_str_plain_domain;
static PyObject *const_tuple_str_plain_LOCALE_NAMES_tuple;
static PyObject *const_str_digest_8aa1fb3e25bed2700e722135293009ed;
static PyObject *const_tuple_str_plain_April_tuple;
static PyObject *const_str_digest_a007ef1d9765d8486eb48b306c7e3511;
static PyObject *const_str_plain_September;
static PyObject *const_str_digest_3574e84f2a7e015bd543ca78e33acf99;
extern PyObject *const_str_plain__months;
extern PyObject *const_str_plain_Sunday;
extern PyObject *const_str_plain_match;
static PyObject *const_str_plain_April;
static PyObject *const_str_digest_d04aabd6bd2a7d0af6798b3dfa0b3895;
extern PyObject *const_str_plain_encoding;
extern PyObject *const_str_plain_count;
static PyObject *const_str_plain_May;
extern PyObject *const_str_digest_cb6886bd8b859ac43aab4f50aae79db4;
extern PyObject *const_tuple_str_plain_code_tuple;
extern PyObject *const_str_plain_weekday;
extern PyObject *const_str_plain_row;
static PyObject *const_str_plain__supported_locales;
static PyObject *const_tuple_str_plain_self_str_plain_code_str_plain_prefix_str_plain___tuple;
static PyObject *const_str_plain_zh_CN;
static PyObject *const_tuple_str_plain_Wednesday_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_utcnow;
static PyObject *const_str_plain_msg_with_ctxt;
extern PyObject *const_str_digest_913ae8f3439742e034c09e3f599385fd;
extern PyObject *const_int_pos_50;
extern PyObject *const_str_plain_Friday;
static PyObject *const_tuple_str_plain_March_tuple;
static PyObject *const_str_digest_d1adda4ceffa6c418846b7463c1c0bf3;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain_minute;
static PyObject *const_tuple_str_plain_en_str_plain_en_US_tuple;
static PyObject *const_str_digest_05e07a22ce2c0ccc55eeafcae9e90317;
static PyObject *const_tuple_str_plain_self_str_plain_value_str_plain_s_str_plain_parts_tuple;
extern PyObject *const_str_plain_last;
static PyObject *const_str_digest_b59844748413c35effa069f127d7f6e1;
extern PyObject *const_str_plain_Tuesday;
static PyObject *const_tuple_str_plain_May_tuple;
static PyObject *const_tuple_str_digest_e89810b6cdcb59d43c1349f24947e94c_tuple;
static PyObject *const_str_plain_full_format;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain__use_gettext;
extern PyObject *const_str_digest_83f99027874593222e45fae974a2895d;
static PyObject *const_str_plain_June;
extern PyObject *const_str_plain_round;
extern PyObject *const_int_pos_2;
static PyObject *const_tuple_str_plain_February_tuple;
static PyObject *const_str_digest_2c507f37a18f95b53987a1891593f590;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_aa2fa87725efc213396ce7440d599ef0 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422222 ], 52, 0 );
    const_str_plain_str_time = UNSTREAM_STRING_ASCII( &constant_bin[ 5422274 ], 8, 1 );
    const_tuple_str_plain_name_str_plain_Unknown_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_Unknown_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_Unknown_tuple, 1, const_str_plain_Unknown ); Py_INCREF( const_str_plain_Unknown );
    const_dict_dd2827a37a0ea3d35f64079aff2696f9 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_dd2827a37a0ea3d35f64079aff2696f9, const_str_plain_directory, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_dd2827a37a0ea3d35f64079aff2696f9, const_str_plain_encoding, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_dd2827a37a0ea3d35f64079aff2696f9, const_str_plain_return, Py_None );
    assert( PyDict_Size( const_dict_dd2827a37a0ea3d35f64079aff2696f9 ) == 3 );
    const_tuple_str_digest_c96f4f5d520728e597b7aaa11664f9e1_tuple = PyTuple_New( 1 );
    const_str_digest_c96f4f5d520728e597b7aaa11664f9e1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422282 ], 44, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c96f4f5d520728e597b7aaa11664f9e1_tuple, 0, const_str_digest_c96f4f5d520728e597b7aaa11664f9e1 ); Py_INCREF( const_str_digest_c96f4f5d520728e597b7aaa11664f9e1 );
    const_tuple_str_digest_3574e84f2a7e015bd543ca78e33acf99_tuple = PyTuple_New( 1 );
    const_str_digest_3574e84f2a7e015bd543ca78e33acf99 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422282 ], 32, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_3574e84f2a7e015bd543ca78e33acf99_tuple, 0, const_str_digest_3574e84f2a7e015bd543ca78e33acf99 ); Py_INCREF( const_str_digest_3574e84f2a7e015bd543ca78e33acf99 );
    const_str_plain_October = UNSTREAM_STRING_ASCII( &constant_bin[ 580608 ], 7, 1 );
    const_str_digest_d2d3529124bbf79c28bd4bb4788ed236 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422326 ], 23, 0 );
    const_str_digest_3ac0bb3f70842f105c27dddcb88c1d26 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422349 ], 962, 0 );
    const_tuple_str_plain_August_tuple = PyTuple_New( 1 );
    const_str_plain_August = UNSTREAM_STRING_ASCII( &constant_bin[ 580569 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_August_tuple, 0, const_str_plain_August ); Py_INCREF( const_str_plain_August );
    const_str_digest_4e0679129fefff8661d681147edadefe = UNSTREAM_STRING_ASCII( &constant_bin[ 25537 ], 3, 0 );
    const_str_digest_ff2e73506fa67da0b46f1c793668749b = UNSTREAM_STRING_ASCII( &constant_bin[ 5423311 ], 49, 0 );
    const_tuple_str_plain_November_tuple = PyTuple_New( 1 );
    const_str_plain_November = UNSTREAM_STRING_ASCII( &constant_bin[ 580624 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_November_tuple, 0, const_str_plain_November ); Py_INCREF( const_str_plain_November );
    const_str_plain_LC_MESSAGES = UNSTREAM_STRING_ASCII( &constant_bin[ 5423360 ], 11, 1 );
    const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple = PyTuple_New( 14 );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 0, const_str_plain_directory ); Py_INCREF( const_str_plain_directory );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 1, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 2, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 3, const_str_plain_locale ); Py_INCREF( const_str_plain_locale );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 4, const_str_plain_extension ); Py_INCREF( const_str_plain_extension );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 5, const_str_plain_full_path ); Py_INCREF( const_str_plain_full_path );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 6, const_str_plain_bf ); Py_INCREF( const_str_plain_bf );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 7, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 8, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 9, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 10, const_str_plain_row ); Py_INCREF( const_str_plain_row );
    const_str_plain_english = UNSTREAM_STRING_ASCII( &constant_bin[ 263871 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 11, const_str_plain_english ); Py_INCREF( const_str_plain_english );
    const_str_plain_translation = UNSTREAM_STRING_ASCII( &constant_bin[ 37495 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 12, const_str_plain_translation ); Py_INCREF( const_str_plain_translation );
    PyTuple_SET_ITEM( const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 13, const_str_plain_plural ); Py_INCREF( const_str_plain_plural );
    const_str_plain_get_supported_locales = UNSTREAM_STRING_ASCII( &constant_bin[ 5423371 ], 21, 1 );
    const_tuple_str_plain_Saturday_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Saturday_tuple, 0, const_str_plain_Saturday ); Py_INCREF( const_str_plain_Saturday );
    const_tuple_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf_tuple = PyTuple_New( 1 );
    const_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf = UNSTREAM_STRING_ASCII( &constant_bin[ 5423392 ], 35, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf_tuple, 0, const_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf ); Py_INCREF( const_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf );
    const_str_digest_e157804d84d435f58356cd3da9c183e4 = UNSTREAM_STRING_ASCII( &constant_bin[ 5423427 ], 11, 0 );
    const_str_plain_plural_message = UNSTREAM_STRING_ASCII( &constant_bin[ 5423438 ], 14, 1 );
    const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple, 1, const_str_plain_date ); Py_INCREF( const_str_plain_date );
    const_str_plain_gmt_offset = UNSTREAM_STRING_ASCII( &constant_bin[ 5423452 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple, 2, const_str_plain_gmt_offset ); Py_INCREF( const_str_plain_gmt_offset );
    const_str_plain_dow = UNSTREAM_STRING_ASCII( &constant_bin[ 1822 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple, 3, const_str_plain_dow ); Py_INCREF( const_str_plain_dow );
    const_str_plain_local_date = UNSTREAM_STRING_ASCII( &constant_bin[ 5423462 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple, 4, const_str_plain_local_date ); Py_INCREF( const_str_plain_local_date );
    PyTuple_SET_ITEM( const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple, 5, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    const_str_plain_CSVLocale = UNSTREAM_STRING_ASCII( &constant_bin[ 5423472 ], 9, 1 );
    const_str_plain_set_default_locale = UNSTREAM_STRING_ASCII( &constant_bin[ 5423481 ], 18, 1 );
    const_str_digest_c105a91b9f0e7aa7ab96931fa498bc8b = UNSTREAM_STRING_ASCII( &constant_bin[ 5423499 ], 18, 0 );
    const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple, 1, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    const_str_plain_translations = UNSTREAM_STRING_ASCII( &constant_bin[ 5423059 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple, 2, const_str_plain_translations ); Py_INCREF( const_str_plain_translations );
    PyTuple_SET_ITEM( const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple, 3, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_plain_message_dict = UNSTREAM_STRING_ASCII( &constant_bin[ 5423517 ], 12, 1 );
    const_tuple_str_plain_fa_str_plain_ar_str_plain_he_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fa_str_plain_ar_str_plain_he_tuple, 0, const_str_plain_fa ); Py_INCREF( const_str_plain_fa );
    PyTuple_SET_ITEM( const_tuple_str_plain_fa_str_plain_ar_str_plain_he_tuple, 1, const_str_plain_ar ); Py_INCREF( const_str_plain_ar );
    const_str_plain_he = UNSTREAM_STRING_ASCII( &constant_bin[ 817 ], 2, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fa_str_plain_ar_str_plain_he_tuple, 2, const_str_plain_he ); Py_INCREF( const_str_plain_he );
    const_tuple_str_plain_fa_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fa_tuple, 0, const_str_plain_fa ); Py_INCREF( const_str_plain_fa );
    const_str_digest_9a4cda2f32b95b957c2fd58ebb08c92a = UNSTREAM_STRING_ASCII( &constant_bin[ 5423529 ], 17, 0 );
    const_str_digest_c9c85d91a6f9ef6ae7ba064fc7185109 = UNSTREAM_STRING_ASCII( &constant_bin[ 5423546 ], 10, 0 );
    const_str_plain_load_translations = UNSTREAM_STRING_ASCII( &constant_bin[ 5423054 ], 17, 1 );
    const_tuple_f7ae2cf4d4b99e7e88e67184ff4a3fea_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_f7ae2cf4d4b99e7e88e67184ff4a3fea_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_f7ae2cf4d4b99e7e88e67184ff4a3fea_tuple, 1, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_f7ae2cf4d4b99e7e88e67184ff4a3fea_tuple, 2, const_str_plain_translations ); Py_INCREF( const_str_plain_translations );
    PyTuple_SET_ITEM( const_tuple_f7ae2cf4d4b99e7e88e67184ff4a3fea_tuple, 3, const_str_plain_locale ); Py_INCREF( const_str_plain_locale );
    const_dict_3fe11010327cbd4c16bbcf45efc3eecd = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_3fe11010327cbd4c16bbcf45efc3eecd, const_str_plain_code, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_3fe11010327cbd4c16bbcf45efc3eecd, const_str_plain_return, Py_None );
    assert( PyDict_Size( const_dict_3fe11010327cbd4c16bbcf45efc3eecd ) == 2 );
    const_str_digest_bef59eea50ee627e5c737ba8285a3699 = UNSTREAM_STRING_ASCII( &constant_bin[ 5423556 ], 55, 0 );
    const_str_digest_0a7809271042678b67580f28ea6c38b7 = UNSTREAM_STRING_ASCII( &constant_bin[ 5423611 ], 421, 0 );
    const_str_plain_NullTranslations = UNSTREAM_STRING_ASCII( &constant_bin[ 5424032 ], 16, 1 );
    const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple, 1, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple, 2, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple, 3, const_str_plain_plural_message ); Py_INCREF( const_str_plain_plural_message );
    PyTuple_SET_ITEM( const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple, 4, const_str_plain_count ); Py_INCREF( const_str_plain_count );
    const_str_digest_4a0c6d12249351433c374c8e2dac95fa = UNSTREAM_STRING_ASCII( &constant_bin[ 5424048 ], 38, 0 );
    const_str_plain_local_yesterday = UNSTREAM_STRING_ASCII( &constant_bin[ 5424086 ], 15, 1 );
    const_tuple_str_digest_4a0c6d12249351433c374c8e2dac95fa_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4a0c6d12249351433c374c8e2dac95fa_tuple, 0, const_str_digest_4a0c6d12249351433c374c8e2dac95fa ); Py_INCREF( const_str_digest_4a0c6d12249351433c374c8e2dac95fa );
    const_tuple_str_plain_plural_str_plain_singular_str_plain_unknown_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_plural_str_plain_singular_str_plain_unknown_tuple, 0, const_str_plain_plural ); Py_INCREF( const_str_plain_plural );
    const_str_plain_singular = UNSTREAM_STRING_ASCII( &constant_bin[ 598877 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_plural_str_plain_singular_str_plain_unknown_tuple, 1, const_str_plain_singular ); Py_INCREF( const_str_plain_singular );
    PyTuple_SET_ITEM( const_tuple_str_plain_plural_str_plain_singular_str_plain_unknown_tuple, 2, const_str_plain_unknown ); Py_INCREF( const_str_plain_unknown );
    const_str_plain_month_name = UNSTREAM_STRING_ASCII( &constant_bin[ 2379415 ], 10, 1 );
    const_tuple_str_digest_daaa94ec39bdc51a92e87ff9abd20aad_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_daaa94ec39bdc51a92e87ff9abd20aad_tuple, 0, const_str_digest_daaa94ec39bdc51a92e87ff9abd20aad ); Py_INCREF( const_str_digest_daaa94ec39bdc51a92e87ff9abd20aad );
    const_str_digest_c2f26402c280cea934f7a4889cf0b12a = UNSTREAM_STRING_ASCII( &constant_bin[ 5424101 ], 110, 0 );
    const_str_digest_475df1123b507a27d0e67c96974cd632 = UNSTREAM_STRING_ASCII( &constant_bin[ 5424211 ], 49, 0 );
    const_tuple_str_plain_Sunday_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Sunday_tuple, 0, const_str_plain_Sunday ); Py_INCREF( const_str_plain_Sunday );
    const_tuple_str_digest_7871fba5268138f3163960862e12ec1d_tuple = PyTuple_New( 1 );
    const_str_digest_7871fba5268138f3163960862e12ec1d = UNSTREAM_STRING_ASCII( &constant_bin[ 5423392 ], 11, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_7871fba5268138f3163960862e12ec1d_tuple, 0, const_str_digest_7871fba5268138f3163960862e12ec1d ); Py_INCREF( const_str_digest_7871fba5268138f3163960862e12ec1d );
    const_str_digest_7f7f8e2802dcb3f5d1279d9ba6ecfe7c = UNSTREAM_STRING_ASCII( &constant_bin[ 5424260 ], 15, 0 );
    const_tuple_str_plain_September_tuple = PyTuple_New( 1 );
    const_str_plain_September = UNSTREAM_STRING_ASCII( &constant_bin[ 580590 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_September_tuple, 0, const_str_plain_September ); Py_INCREF( const_str_plain_September );
    const_str_digest_556bc24351716bafdca56a89def05747 = UNSTREAM_STRING_ASCII( &constant_bin[ 5424275 ], 540, 0 );
    const_str_digest_f499141fb159d3c1f4c2312c62754b04 = UNSTREAM_STRING_ASCII( &constant_bin[ 5424815 ], 22, 0 );
    const_tuple_str_digest_a007ef1d9765d8486eb48b306c7e3511_tuple = PyTuple_New( 1 );
    const_str_digest_a007ef1d9765d8486eb48b306c7e3511 = UNSTREAM_STRING_ASCII( &constant_bin[ 5424837 ], 23, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_a007ef1d9765d8486eb48b306c7e3511_tuple, 0, const_str_digest_a007ef1d9765d8486eb48b306c7e3511 ); Py_INCREF( const_str_digest_a007ef1d9765d8486eb48b306c7e3511 );
    const_str_digest_9d183a9f7777afb52500f6cd3f94fde8 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422282 ], 22, 0 );
    const_tuple_str_plain_December_tuple = PyTuple_New( 1 );
    const_str_plain_December = UNSTREAM_STRING_ASCII( &constant_bin[ 580641 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_December_tuple, 0, const_str_plain_December ); Py_INCREF( const_str_plain_December );
    const_str_digest_c1f66a265abaea28dae1f02b5da97e35 = UNSTREAM_STRING_ASCII( &constant_bin[ 5424860 ], 23, 0 );
    const_str_plain_locale_codes = UNSTREAM_STRING_ASCII( &constant_bin[ 5424883 ], 12, 1 );
    const_str_digest_bc4c3b36cdd00cb6e1d6ca8f27b40a93 = UNSTREAM_STRING_ASCII( &constant_bin[ 5424895 ], 61, 0 );
    const_tuple_str_plain_self_str_plain_parts_str_plain___str_plain_comma_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_parts_str_plain___str_plain_comma_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_parts_str_plain___str_plain_comma_tuple, 1, const_str_plain_parts ); Py_INCREF( const_str_plain_parts );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_parts_str_plain___str_plain_comma_tuple, 2, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_parts_str_plain___str_plain_comma_tuple, 3, const_str_plain_comma ); Py_INCREF( const_str_plain_comma );
    const_tuple_str_digest_eb0f9b85ed1cef1f2122150e15335d0c_tuple = PyTuple_New( 1 );
    const_str_digest_eb0f9b85ed1cef1f2122150e15335d0c = UNSTREAM_STRING_ASCII( &constant_bin[ 5424956 ], 23, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_eb0f9b85ed1cef1f2122150e15335d0c_tuple, 0, const_str_digest_eb0f9b85ed1cef1f2122150e15335d0c ); Py_INCREF( const_str_digest_eb0f9b85ed1cef1f2122150e15335d0c );
    const_str_plain_January = UNSTREAM_STRING_ASCII( &constant_bin[ 580470 ], 7, 1 );
    const_str_digest_9c0911e86d5a69f513ffa9986416d31c = UNSTREAM_STRING_ASCII( &constant_bin[ 5424979 ], 19, 0 );
    const_tuple_str_digest_9d183a9f7777afb52500f6cd3f94fde8_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_9d183a9f7777afb52500f6cd3f94fde8_tuple, 0, const_str_digest_9d183a9f7777afb52500f6cd3f94fde8 ); Py_INCREF( const_str_digest_9d183a9f7777afb52500f6cd3f94fde8 );
    const_str_digest_eb2e4d4363a413350a3b55abcc716ba6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5424998 ], 163, 0 );
    const_str_plain_GettextLocale = UNSTREAM_STRING_ASCII( &constant_bin[ 5424815 ], 13, 1 );
    const_str_plain__translations = UNSTREAM_STRING_ASCII( &constant_bin[ 5423058 ], 13, 1 );
    const_tuple_str_digest_626f47fee6196287729a98b52426b191_tuple = PyTuple_New( 1 );
    const_str_digest_626f47fee6196287729a98b52426b191 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422318 ], 8, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_626f47fee6196287729a98b52426b191_tuple, 0, const_str_digest_626f47fee6196287729a98b52426b191 ); Py_INCREF( const_str_digest_626f47fee6196287729a98b52426b191 );
    const_str_digest_06670af157044a600ad097110ee7c367 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425161 ], 148, 0 );
    const_str_digest_8673d2aaeb83e68b5a07fcf14c894cf2 = UNSTREAM_STRING( &constant_bin[ 5425309 ], 6, 0 );
    const_str_digest_4f619ff8041ae709fb03a4f58d086f92 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425315 ], 17, 0 );
    const_str_digest_052d7566e9844fc118a7de62c1266c2d = UNSTREAM_STRING_ASCII( &constant_bin[ 5425332 ], 22, 0 );
    const_str_plain_local_now = UNSTREAM_STRING_ASCII( &constant_bin[ 5425354 ], 9, 1 );
    const_str_digest_6703d883ad26971c7886a4bc827d59fa = UNSTREAM_STRING_ASCII( &constant_bin[ 5425363 ], 23, 0 );
    const_str_plain_friendly_number = UNSTREAM_STRING_ASCII( &constant_bin[ 5425386 ], 15, 1 );
    const_str_digest_695283f572d9bc5821fb828221e07f56 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425401 ], 10, 0 );
    const_str_digest_edc4e236c8c648a0aa64ccf415398bf9 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425411 ], 33, 0 );
    const_tuple_str_plain_am_str_plain_pm_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_am_str_plain_pm_tuple, 0, const_str_plain_am ); Py_INCREF( const_str_plain_am );
    PyTuple_SET_ITEM( const_tuple_str_plain_am_str_plain_pm_tuple, 1, const_str_plain_pm ); Py_INCREF( const_str_plain_pm );
    const_tuple_str_plain_July_tuple = PyTuple_New( 1 );
    const_str_plain_July = UNSTREAM_STRING_ASCII( &constant_bin[ 580556 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_July_tuple, 0, const_str_plain_July ); Py_INCREF( const_str_plain_July );
    const_str_plain_ngettext = UNSTREAM_STRING_ASCII( &constant_bin[ 5425444 ], 8, 1 );
    const_str_digest_e89810b6cdcb59d43c1349f24947e94c = UNSTREAM_STRING_ASCII( &constant_bin[ 5425452 ], 34, 0 );
    const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 1, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 2, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 3, const_str_plain_plural_message ); Py_INCREF( const_str_plain_plural_message );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 4, const_str_plain_count ); Py_INCREF( const_str_plain_count );
    const_str_plain_msgs_with_ctxt = UNSTREAM_STRING_ASCII( &constant_bin[ 5425486 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 5, const_str_plain_msgs_with_ctxt ); Py_INCREF( const_str_plain_msgs_with_ctxt );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 6, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    const_str_plain_msg_with_ctxt = UNSTREAM_STRING_ASCII( &constant_bin[ 5425500 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 7, const_str_plain_msg_with_ctxt ); Py_INCREF( const_str_plain_msg_with_ctxt );
    const_str_digest_1f98531c7ff1755fa56a767f09a0a7a0 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425513 ], 9, 0 );
    const_str_plain_CONTEXT_SEPARATOR = UNSTREAM_STRING_ASCII( &constant_bin[ 5425522 ], 17, 1 );
    const_tuple_str_plain_en_str_plain_en_US_str_plain_zh_CN_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_en_str_plain_en_US_str_plain_zh_CN_tuple, 0, const_str_plain_en ); Py_INCREF( const_str_plain_en );
    PyTuple_SET_ITEM( const_tuple_str_plain_en_str_plain_en_US_str_plain_zh_CN_tuple, 1, const_str_plain_en_US ); Py_INCREF( const_str_plain_en_US );
    const_str_plain_zh_CN = UNSTREAM_STRING_ASCII( &constant_bin[ 5318178 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_en_str_plain_en_US_str_plain_zh_CN_tuple, 2, const_str_plain_zh_CN ); Py_INCREF( const_str_plain_zh_CN );
    const_str_plain_format_day = UNSTREAM_STRING_ASCII( &constant_bin[ 5423536 ], 10, 1 );
    const_tuple_str_plain_yesterday_tuple = PyTuple_New( 1 );
    const_str_plain_yesterday = UNSTREAM_STRING_ASCII( &constant_bin[ 5424092 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_yesterday_tuple, 0, const_str_plain_yesterday ); Py_INCREF( const_str_plain_yesterday );
    const_tuple_1e6837b60802e5c143d36703de583e6f_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_1e6837b60802e5c143d36703de583e6f_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_1e6837b60802e5c143d36703de583e6f_tuple, 1, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_1e6837b60802e5c143d36703de583e6f_tuple, 2, const_str_plain_plural_message ); Py_INCREF( const_str_plain_plural_message );
    PyTuple_SET_ITEM( const_tuple_1e6837b60802e5c143d36703de583e6f_tuple, 3, const_str_plain_count ); Py_INCREF( const_str_plain_count );
    const_str_plain__default_locale = UNSTREAM_STRING_ASCII( &constant_bin[ 5423484 ], 15, 1 );
    const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple, 0, const_str_plain_directory ); Py_INCREF( const_str_plain_directory );
    PyTuple_SET_ITEM( const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple, 1, const_str_plain_domain ); Py_INCREF( const_str_plain_domain );
    const_str_plain_gettext = UNSTREAM_STRING_ASCII( &constant_bin[ 5003703 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple, 2, const_str_plain_gettext ); Py_INCREF( const_str_plain_gettext );
    PyTuple_SET_ITEM( const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple, 3, const_str_plain_lang ); Py_INCREF( const_str_plain_lang );
    PyTuple_SET_ITEM( const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple, 4, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_tuple_str_plain_October_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_October_tuple, 0, const_str_plain_October ); Py_INCREF( const_str_plain_October );
    const_tuple_str_plain_Thursday_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Thursday_tuple, 0, const_str_plain_Thursday ); Py_INCREF( const_str_plain_Thursday );
    const_str_digest_29385822239ba969122cbf138728dfbe = UNSTREAM_STRING_ASCII( &constant_bin[ 5425539 ], 18, 0 );
    const_str_digest_379c58f19104aad46c52cfb6c3902689 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425557 ], 23, 0 );
    const_str_plain_get_closest = UNSTREAM_STRING_ASCII( &constant_bin[ 4816751 ], 11, 1 );
    const_str_plain_shorter = UNSTREAM_STRING_ASCII( &constant_bin[ 1453434 ], 7, 1 );
    const_str_plain_tfhour_clock = UNSTREAM_STRING_ASCII( &constant_bin[ 5425580 ], 12, 1 );
    const_tuple_int_0_true_false_false_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_int_0_true_false_false_tuple, 0, const_int_0 ); Py_INCREF( const_int_0 );
    PyTuple_SET_ITEM( const_tuple_int_0_true_false_false_tuple, 1, Py_True ); Py_INCREF( Py_True );
    PyTuple_SET_ITEM( const_tuple_int_0_true_false_false_tuple, 2, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_int_0_true_false_false_tuple, 3, Py_False ); Py_INCREF( Py_False );
    const_tuple_503c00455ace194f34ccb09c62c42099_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_503c00455ace194f34ccb09c62c42099_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_503c00455ace194f34ccb09c62c42099_tuple, 1, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_503c00455ace194f34ccb09c62c42099_tuple, 2, const_str_plain_plural_message ); Py_INCREF( const_str_plain_plural_message );
    PyTuple_SET_ITEM( const_tuple_503c00455ace194f34ccb09c62c42099_tuple, 3, const_str_plain_count ); Py_INCREF( const_str_plain_count );
    PyTuple_SET_ITEM( const_tuple_503c00455ace194f34ccb09c62c42099_tuple, 4, const_str_plain_message_dict ); Py_INCREF( const_str_plain_message_dict );
    const_str_digest_0c060498692fd2cb6f8eedca3dc6fbab = UNSTREAM_STRING_ASCII( &constant_bin[ 5425592 ], 18, 0 );
    const_str_digest_1214c21596fee4366c3ea77fd73e2614 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425610 ], 18, 0 );
    const_tuple_4098b9f363c2cd529fe597c0aaf71d10_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_4098b9f363c2cd529fe597c0aaf71d10_tuple, 0, const_str_digest_8673d2aaeb83e68b5a07fcf14c894cf2 ); Py_INCREF( const_str_digest_8673d2aaeb83e68b5a07fcf14c894cf2 );
    const_str_digest_62523a4f5e0ca4a43f0e85f7a8d10754 = UNSTREAM_STRING( &constant_bin[ 5425628 ], 6, 0 );
    PyTuple_SET_ITEM( const_tuple_4098b9f363c2cd529fe597c0aaf71d10_tuple, 1, const_str_digest_62523a4f5e0ca4a43f0e85f7a8d10754 ); Py_INCREF( const_str_digest_62523a4f5e0ca4a43f0e85f7a8d10754 );
    const_str_digest_3505f180c48d7881a1fcd516b48114ef = UNSTREAM_STRING( &constant_bin[ 5425634 ], 4, 0 );
    const_str_digest_64acd9d2cb4096ad5db303736a6307e5 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425638 ], 12, 0 );
    const_str_digest_3f0e87ccc267dc20a6762772e63cb9c6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425650 ], 21, 0 );
    const_tuple_str_plain_Friday_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Friday_tuple, 0, const_str_plain_Friday ); Py_INCREF( const_str_plain_Friday );
    const_str_digest_f5737bc588770cc5264c9b81f8874d8c = UNSTREAM_STRING_ASCII( &constant_bin[ 5425671 ], 575, 0 );
    const_dict_ba6737774c95763b85c660cd7c84b8a4 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_ba6737774c95763b85c660cd7c84b8a4, const_str_plain_locale_codes, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_ba6737774c95763b85c660cd7c84b8a4, const_str_plain_return, const_str_plain_Locale );
    assert( PyDict_Size( const_dict_ba6737774c95763b85c660cd7c84b8a4 ) == 2 );
    const_dict_94af2e730a3b531141cb165a0fc9a983 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_94af2e730a3b531141cb165a0fc9a983, const_str_plain_directory, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_94af2e730a3b531141cb165a0fc9a983, const_str_plain_domain, (PyObject *)&PyUnicode_Type );
    PyDict_SetItem( const_dict_94af2e730a3b531141cb165a0fc9a983, const_str_plain_return, Py_None );
    assert( PyDict_Size( const_dict_94af2e730a3b531141cb165a0fc9a983 ) == 3 );
    const_str_digest_332c0ee92d2f3d71b4732ef2b5d796e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5426246 ], 17, 0 );
    const_str_digest_a2593816d0f28e619453b3faa3652e96 = UNSTREAM_STRING( &constant_bin[ 5426263 ], 1655, 0 );
    const_str_digest_e1dd5bde31ecab95c214424d92ae147d = UNSTREAM_STRING_ASCII( &constant_bin[ 5427918 ], 311, 0 );
    const_tuple_str_plain_January_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_January_tuple, 0, const_str_plain_January ); Py_INCREF( const_str_plain_January );
    const_str_plain_March = UNSTREAM_STRING_ASCII( &constant_bin[ 580503 ], 5, 1 );
    const_tuple_str_plain_locale_codes_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_locale_codes_tuple, 0, const_str_plain_locale_codes ); Py_INCREF( const_str_plain_locale_codes );
    const_tuple_str_plain_June_tuple = PyTuple_New( 1 );
    const_str_plain_June = UNSTREAM_STRING_ASCII( &constant_bin[ 580543 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_June_tuple, 0, const_str_plain_June ); Py_INCREF( const_str_plain_June );
    const_tuple_829a096044442484452ce55ee5d19a78_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_829a096044442484452ce55ee5d19a78_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_829a096044442484452ce55ee5d19a78_tuple, 1, const_str_plain_locale_codes ); Py_INCREF( const_str_plain_locale_codes );
    PyTuple_SET_ITEM( const_tuple_829a096044442484452ce55ee5d19a78_tuple, 2, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_829a096044442484452ce55ee5d19a78_tuple, 3, const_str_plain_parts ); Py_INCREF( const_str_plain_parts );
    const_str_digest_2af2f0bb9895a7e9b768093a63173b6d = UNSTREAM_STRING_ASCII( &constant_bin[ 5428229 ], 46, 0 );
    const_str_digest_b27e80db6e1cdf2aa0d1c5081ea4a866 = UNSTREAM_STRING_ASCII( &constant_bin[ 5422721 ], 16, 0 );
    const_str_digest_c16be5bdd7c0f2cffe66ad43f1ff6e1c = UNSTREAM_STRING_ASCII( &constant_bin[ 5425401 ], 7, 0 );
    const_str_digest_8bda2ae8a09ed125b91f60b17cc43541 = UNSTREAM_STRING_ASCII( &constant_bin[ 5428275 ], 22, 0 );
    const_str_plain_commas = UNSTREAM_STRING_ASCII( &constant_bin[ 953799 ], 6, 1 );
    const_tuple_str_digest_b640953a68c487cbc89544f72b9babc3_tuple = PyTuple_New( 1 );
    const_str_digest_b640953a68c487cbc89544f72b9babc3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5428297 ], 21, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b640953a68c487cbc89544f72b9babc3_tuple, 0, const_str_digest_b640953a68c487cbc89544f72b9babc3 ); Py_INCREF( const_str_digest_b640953a68c487cbc89544f72b9babc3 );
    const_str_digest_915c1427e06b79b36242659fa2173cde = UNSTREAM_STRING_ASCII( &constant_bin[ 5428318 ], 435, 0 );
    const_str_plain_load_gettext_translations = UNSTREAM_STRING_ASCII( &constant_bin[ 5423110 ], 25, 1 );
    const_str_plain_February = UNSTREAM_STRING_ASCII( &constant_bin[ 580486 ], 8, 1 );
    const_tuple_str_plain_Monday_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Monday_tuple, 0, const_str_plain_Monday ); Py_INCREF( const_str_plain_Monday );
    const_tuple_str_plain_Tuesday_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Tuesday_tuple, 0, const_str_plain_Tuesday ); Py_INCREF( const_str_plain_Tuesday );
    const_tuple_15df13279735db77bab82178193ba3a7_tuple = PyTuple_New( 19 );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 1, const_str_plain_date ); Py_INCREF( const_str_plain_date );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 2, const_str_plain_gmt_offset ); Py_INCREF( const_str_plain_gmt_offset );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 3, const_str_plain_relative ); Py_INCREF( const_str_plain_relative );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 4, const_str_plain_shorter ); Py_INCREF( const_str_plain_shorter );
    const_str_plain_full_format = UNSTREAM_STRING_ASCII( &constant_bin[ 5423876 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 5, const_str_plain_full_format ); Py_INCREF( const_str_plain_full_format );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 6, const_str_plain_now ); Py_INCREF( const_str_plain_now );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 7, const_str_plain_local_date ); Py_INCREF( const_str_plain_local_date );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 8, const_str_plain_local_now ); Py_INCREF( const_str_plain_local_now );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 9, const_str_plain_local_yesterday ); Py_INCREF( const_str_plain_local_yesterday );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 10, const_str_plain_difference ); Py_INCREF( const_str_plain_difference );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 11, const_str_plain_seconds ); Py_INCREF( const_str_plain_seconds );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 12, const_str_plain_days ); Py_INCREF( const_str_plain_days );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 13, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 14, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 15, const_str_plain_minutes ); Py_INCREF( const_str_plain_minutes );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 16, const_str_plain_hours ); Py_INCREF( const_str_plain_hours );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 17, const_str_plain_tfhour_clock ); Py_INCREF( const_str_plain_tfhour_clock );
    PyTuple_SET_ITEM( const_tuple_15df13279735db77bab82178193ba3a7_tuple, 18, const_str_plain_str_time ); Py_INCREF( const_str_plain_str_time );
    const_tuple_32523c33b2c5f7d616fdfabb493279a2_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_32523c33b2c5f7d616fdfabb493279a2_tuple, 0, const_str_plain_Iterable ); Py_INCREF( const_str_plain_Iterable );
    PyTuple_SET_ITEM( const_tuple_32523c33b2c5f7d616fdfabb493279a2_tuple, 1, const_str_plain_Any ); Py_INCREF( const_str_plain_Any );
    PyTuple_SET_ITEM( const_tuple_32523c33b2c5f7d616fdfabb493279a2_tuple, 2, const_str_plain_Union ); Py_INCREF( const_str_plain_Union );
    PyTuple_SET_ITEM( const_tuple_32523c33b2c5f7d616fdfabb493279a2_tuple, 3, const_str_plain_Dict ); Py_INCREF( const_str_plain_Dict );
    const_str_plain_format_date = UNSTREAM_STRING_ASCII( &constant_bin[ 5423506 ], 11, 1 );
    const_str_digest_eab034a4d05d723cadb02b46db203b41 = UNSTREAM_STRING_ASCII( &constant_bin[ 5428753 ], 294, 0 );
    const_tuple_str_plain_LOCALE_NAMES_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LOCALE_NAMES_tuple, 0, const_str_plain_LOCALE_NAMES ); Py_INCREF( const_str_plain_LOCALE_NAMES );
    const_str_digest_8aa1fb3e25bed2700e722135293009ed = UNSTREAM_STRING_ASCII( &constant_bin[ 5425339 ], 15, 0 );
    const_tuple_str_plain_April_tuple = PyTuple_New( 1 );
    const_str_plain_April = UNSTREAM_STRING_ASCII( &constant_bin[ 580517 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_April_tuple, 0, const_str_plain_April ); Py_INCREF( const_str_plain_April );
    const_str_digest_d04aabd6bd2a7d0af6798b3dfa0b3895 = UNSTREAM_STRING_ASCII( &constant_bin[ 5429047 ], 36, 0 );
    const_str_plain_May = UNSTREAM_STRING_ASCII( &constant_bin[ 5564 ], 3, 1 );
    const_str_plain__supported_locales = UNSTREAM_STRING_ASCII( &constant_bin[ 5423374 ], 18, 1 );
    const_tuple_str_plain_self_str_plain_code_str_plain_prefix_str_plain___tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_code_str_plain_prefix_str_plain___tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_code_str_plain_prefix_str_plain___tuple, 1, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_code_str_plain_prefix_str_plain___tuple, 2, const_str_plain_prefix ); Py_INCREF( const_str_plain_prefix );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_code_str_plain_prefix_str_plain___tuple, 3, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    const_tuple_str_plain_Wednesday_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Wednesday_tuple, 0, const_str_plain_Wednesday ); Py_INCREF( const_str_plain_Wednesday );
    const_tuple_str_plain_March_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_March_tuple, 0, const_str_plain_March ); Py_INCREF( const_str_plain_March );
    const_str_digest_d1adda4ceffa6c418846b7463c1c0bf3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5429083 ], 12, 0 );
    const_tuple_str_plain_en_str_plain_en_US_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_en_str_plain_en_US_tuple, 0, const_str_plain_en ); Py_INCREF( const_str_plain_en );
    PyTuple_SET_ITEM( const_tuple_str_plain_en_str_plain_en_US_tuple, 1, const_str_plain_en_US ); Py_INCREF( const_str_plain_en_US );
    const_str_digest_05e07a22ce2c0ccc55eeafcae9e90317 = UNSTREAM_STRING_ASCII( &constant_bin[ 5429095 ], 168, 0 );
    const_tuple_str_plain_self_str_plain_value_str_plain_s_str_plain_parts_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_s_str_plain_parts_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_s_str_plain_parts_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_s_str_plain_parts_tuple, 2, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_s_str_plain_parts_tuple, 3, const_str_plain_parts ); Py_INCREF( const_str_plain_parts );
    const_str_digest_b59844748413c35effa069f127d7f6e1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5429263 ], 19, 0 );
    const_tuple_str_plain_May_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_May_tuple, 0, const_str_plain_May ); Py_INCREF( const_str_plain_May );
    const_tuple_str_digest_e89810b6cdcb59d43c1349f24947e94c_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_e89810b6cdcb59d43c1349f24947e94c_tuple, 0, const_str_digest_e89810b6cdcb59d43c1349f24947e94c ); Py_INCREF( const_str_digest_e89810b6cdcb59d43c1349f24947e94c );
    const_str_plain__use_gettext = UNSTREAM_STRING_ASCII( &constant_bin[ 5429282 ], 12, 1 );
    const_tuple_str_plain_February_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_February_tuple, 0, const_str_plain_February ); Py_INCREF( const_str_plain_February );
    const_str_digest_2c507f37a18f95b53987a1891593f590 = UNSTREAM_STRING_ASCII( &constant_bin[ 5425592 ], 10, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_tornado$locale( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_a0ec6a3f96d0d015c6376ed1a6637a77;
static PyCodeObject *codeobj_5bd5a958260a4066c2f16356d7c027b8;
static PyCodeObject *codeobj_2df287331323093443a3977055e987a6;
static PyCodeObject *codeobj_3da1907eb5bd327c136ce25919a31dea;
static PyCodeObject *codeobj_6048e90e773f64ce9dbb0460f6546cc2;
static PyCodeObject *codeobj_61a8788ba2981b76b0667ee4f53933ac;
static PyCodeObject *codeobj_a842a0780f04fb77527ad613fa687a53;
static PyCodeObject *codeobj_87e572aedb8ed4c853df737392dc0d3b;
static PyCodeObject *codeobj_3fab27a27092cb371e8ae54e29adbcff;
static PyCodeObject *codeobj_c018dbe84ba4c914ac5efad120c78c2a;
static PyCodeObject *codeobj_f153662e8ed47c2118619d52e0b7d53b;
static PyCodeObject *codeobj_173bd6178f556cc83e3014643cc70611;
static PyCodeObject *codeobj_6e9c9ec0fde4e3d3dfa88f4a417ad8df;
static PyCodeObject *codeobj_cbd06a4c08c800b733833a97ba1bb610;
static PyCodeObject *codeobj_5af22b6e07c6e31626b3ccb031b771c5;
static PyCodeObject *codeobj_764b37dccc915f0bcd96d4b5355bab86;
static PyCodeObject *codeobj_6e03f2d96809b9c77d314ed0447d982b;
static PyCodeObject *codeobj_ed18bd3f27453c02c18ebbeeb2000c16;
static PyCodeObject *codeobj_650c77969df137a567f8457a5a94a2f8;
static PyCodeObject *codeobj_541d72d224cccb1ded4070daa30e97b1;
static PyCodeObject *codeobj_58c754dd4825bf3ee9b8e8af194614f5;
static PyCodeObject *codeobj_d876f22011476a59bcd061b910cd5ccf;
static PyCodeObject *codeobj_7b47ccf3d9329dc01e17f4b0c470baaf;
static PyCodeObject *codeobj_067b26dca163587989ee9a2dbf5f8ae4;
static PyCodeObject *codeobj_6b09f54ae5c9f31517b15e2480fcf05f;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_332c0ee92d2f3d71b4732ef2b5d796e2 );
    codeobj_a0ec6a3f96d0d015c6376ed1a6637a77 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 159, const_tuple_str_plain_c_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5bd5a958260a4066c2f16356d7c027b8 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_379c58f19104aad46c52cfb6c3902689, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_2df287331323093443a3977055e987a6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_CSVLocale, 476, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_3da1907eb5bd327c136ce25919a31dea = MAKE_CODEOBJ( module_filename_obj, const_str_plain_GettextLocale, 505, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_6048e90e773f64ce9dbb0460f6546cc2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Locale, 228, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_61a8788ba2981b76b0667ee4f53933ac = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 273, const_tuple_str_plain_self_str_plain_code_str_plain_prefix_str_plain___tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a842a0780f04fb77527ad613fa687a53 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 479, const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_87e572aedb8ed4c853df737392dc0d3b = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 508, const_tuple_9c86f2d48f2e78bde15e0e9a5ec03a23_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_3fab27a27092cb371e8ae54e29adbcff = MAKE_CODEOBJ( module_filename_obj, const_str_plain_format_date, 325, const_tuple_15df13279735db77bab82178193ba3a7_tuple, 6, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c018dbe84ba4c914ac5efad120c78c2a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_format_day, 425, const_tuple_690d190e5d85d2e9a40616b15dda7e97_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f153662e8ed47c2118619d52e0b7d53b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_friendly_number, 464, const_tuple_str_plain_self_str_plain_value_str_plain_s_str_plain_parts_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_173bd6178f556cc83e3014643cc70611 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 255, const_tuple_f7ae2cf4d4b99e7e88e67184ff4a3fea_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6e9c9ec0fde4e3d3dfa88f4a417ad8df = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 63, const_tuple_str_plain_locale_codes_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
    codeobj_cbd06a4c08c800b733833a97ba1bb610 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_closest, 237, const_tuple_829a096044442484452ce55ee5d19a78_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
    codeobj_5af22b6e07c6e31626b3ccb031b771c5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_supported_locales, 223, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_764b37dccc915f0bcd96d4b5355bab86 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_list, 447, const_tuple_str_plain_self_str_plain_parts_str_plain___str_plain_comma_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6e03f2d96809b9c77d314ed0447d982b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load_gettext_translations, 178, const_tuple_7ceaeca10a7de954ea79e2f08bff0813_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ed18bd3f27453c02c18ebbeeb2000c16 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load_translations, 91, const_tuple_cdb22e2c4fbbfb5ae43540654592520d_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_650c77969df137a567f8457a5a94a2f8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_pgettext, 320, const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_541d72d224cccb1ded4070daa30e97b1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_pgettext, 497, const_tuple_901d725cb25bc4dbc720431630d3bf6c_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_58c754dd4825bf3ee9b8e8af194614f5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_pgettext, 524, const_tuple_5936ecf0a5d2db61e248ace6e1fcb0fd_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d876f22011476a59bcd061b910cd5ccf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_default_locale, 77, const_tuple_str_plain_code_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7b47ccf3d9329dc01e17f4b0c470baaf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_translate, 308, const_tuple_1e6837b60802e5c143d36703de583e6f_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_067b26dca163587989ee9a2dbf5f8ae4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_translate, 515, const_tuple_1e6837b60802e5c143d36703de583e6f_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6b09f54ae5c9f31517b15e2480fcf05f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_translate, 483, const_tuple_503c00455ace194f34ccb09c62c42099_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_10_pgettext( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_11_format_date( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_12_format_day( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_13_list( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_14_friendly_number( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_15___init__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_16_translate( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_17_pgettext( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_18___init__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_19_translate( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_1_get( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_20_pgettext( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_2_set_default_locale( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_3_load_translations( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_4_load_gettext_translations( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_5_get_supported_locales( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_6_get_closest( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_7_get( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_8___init__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$locale$$$function_9_translate( PyObject *defaults, PyObject *annotations );


// The module function definitions.
static PyObject *impl_tornado$locale$$$function_1_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_locale_codes = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df, codeobj_6e9c9ec0fde4e3d3dfa88f4a417ad8df, module_tornado$locale, sizeof(void *) );
    frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df = cache_frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dircall_arg2_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Locale );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Locale );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Locale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_closest );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_locale_codes );
        tmp_dircall_arg2_1 = par_locale_codes;
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_return_value = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df,
        type_description_1,
        par_locale_codes
    );


    // Release cached frame.
    if ( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df == cache_frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df )
    {
        Py_DECREF( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df );
    }
    cache_frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df = NULL;

    assertFrameObject( frame_6e9c9ec0fde4e3d3dfa88f4a417ad8df );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_1_get );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_locale_codes );
    Py_DECREF( par_locale_codes );
    par_locale_codes = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_locale_codes );
    Py_DECREF( par_locale_codes );
    par_locale_codes = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_1_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_2_set_default_locale( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_code = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d876f22011476a59bcd061b910cd5ccf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d876f22011476a59bcd061b910cd5ccf = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( par_code );
        tmp_assign_source_1 = par_code;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__default_locale, tmp_assign_source_1 );
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d876f22011476a59bcd061b910cd5ccf, codeobj_d876f22011476a59bcd061b910cd5ccf, module_tornado$locale, sizeof(void *) );
    frame_d876f22011476a59bcd061b910cd5ccf = cache_frame_d876f22011476a59bcd061b910cd5ccf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d876f22011476a59bcd061b910cd5ccf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d876f22011476a59bcd061b910cd5ccf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_frozenset_arg_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__translations );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_translations" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_d876f22011476a59bcd061b910cd5ccf->m_frame.f_lineno = 88;
        tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_keys );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__default_locale );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__default_locale );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_default_locale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_list_element_1 = tmp_mvar_value_2;
        tmp_right_name_1 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_right_name_1, 0, tmp_list_element_1 );
        tmp_frozenset_arg_1 = BINARY_OPERATION_ADD_LIST_LIST( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        assert( !(tmp_frozenset_arg_1 == NULL) );
        tmp_assign_source_2 = PyFrozenSet_New( tmp_frozenset_arg_1 );
        Py_DECREF( tmp_frozenset_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales, tmp_assign_source_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d876f22011476a59bcd061b910cd5ccf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d876f22011476a59bcd061b910cd5ccf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d876f22011476a59bcd061b910cd5ccf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d876f22011476a59bcd061b910cd5ccf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d876f22011476a59bcd061b910cd5ccf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d876f22011476a59bcd061b910cd5ccf,
        type_description_1,
        par_code
    );


    // Release cached frame.
    if ( frame_d876f22011476a59bcd061b910cd5ccf == cache_frame_d876f22011476a59bcd061b910cd5ccf )
    {
        Py_DECREF( frame_d876f22011476a59bcd061b910cd5ccf );
    }
    cache_frame_d876f22011476a59bcd061b910cd5ccf = NULL;

    assertFrameObject( frame_d876f22011476a59bcd061b910cd5ccf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_2_set_default_locale );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_2_set_default_locale );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_3_load_translations( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_directory = python_pars[ 0 ];
    PyObject *par_encoding = python_pars[ 1 ];
    PyObject *var_path = NULL;
    PyObject *var_locale = NULL;
    PyObject *var_extension = NULL;
    PyObject *var_full_path = NULL;
    PyObject *var_bf = NULL;
    PyObject *var_data = NULL;
    PyObject *var_f = NULL;
    PyObject *var_i = NULL;
    PyObject *var_row = NULL;
    PyObject *var_english = NULL;
    PyObject *var_translation = NULL;
    PyObject *var_plural = NULL;
    PyObject *outline_0_var_c = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    PyObject *tmp_with_1__indicator = NULL;
    PyObject *tmp_with_1__source = NULL;
    PyObject *tmp_with_2__enter = NULL;
    PyObject *tmp_with_2__exit = NULL;
    PyObject *tmp_with_2__indicator = NULL;
    PyObject *tmp_with_2__source = NULL;
    struct Nuitka_FrameObject *frame_ed18bd3f27453c02c18ebbeeb2000c16;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    struct Nuitka_FrameObject *frame_a0ec6a3f96d0d015c6376ed1a6637a77_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    static struct Nuitka_FrameObject *cache_frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 = NULL;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    static struct Nuitka_FrameObject *cache_frame_ed18bd3f27453c02c18ebbeeb2000c16 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations, tmp_assign_source_1 );
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ed18bd3f27453c02c18ebbeeb2000c16, codeobj_ed18bd3f27453c02c18ebbeeb2000c16, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ed18bd3f27453c02c18ebbeeb2000c16 = cache_frame_ed18bd3f27453c02c18ebbeeb2000c16;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ed18bd3f27453c02c18ebbeeb2000c16 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ed18bd3f27453c02c18ebbeeb2000c16 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_directory );
        tmp_args_element_name_1 = par_directory;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_listdir, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooo";
                exception_lineno = 129;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_path;
            var_path = tmp_assign_source_4;
            Py_INCREF( var_path );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_path );
        tmp_called_instance_2 = var_path;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 130;
        tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_digest_daaa94ec39bdc51a92e87ff9abd20aad_tuple, 0 ) );

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( var_path );
        tmp_called_instance_3 = var_path;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 132;
        tmp_iter_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_5 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 132;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 132;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooo";
                    exception_lineno = 132;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooo";
            exception_lineno = 132;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_locale;
            var_locale = tmp_assign_source_8;
            Py_INCREF( var_locale );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_extension;
            var_extension = tmp_assign_source_9;
            Py_INCREF( var_extension );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_4 = tmp_mvar_value_2;
        tmp_args_element_name_2 = const_str_digest_4f619ff8041ae709fb03a4f58d086f92;
        CHECK_OBJECT( var_locale );
        tmp_args_element_name_3 = var_locale;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_operand_name_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_match, call_args );
        }

        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gen_log );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen_log );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_source_name_1 = tmp_mvar_value_3;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_error );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_args_element_name_4 = const_str_digest_edc4e236c8c648a0aa64ccf415398bf9;
            CHECK_OBJECT( var_locale );
            tmp_args_element_name_5 = var_locale;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 137;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_source_name_2 = tmp_mvar_value_4;
            tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
            if ( tmp_called_instance_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 137;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( par_directory );
            tmp_args_element_name_7 = par_directory;
            CHECK_OBJECT( var_path );
            tmp_args_element_name_8 = var_path;
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 137;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
                tmp_args_element_name_6 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_5, const_str_plain_join, call_args );
            }

            Py_DECREF( tmp_called_instance_5 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 137;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 134;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto loop_start_1;
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_3 = tmp_mvar_value_5;
        tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_path );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_directory );
        tmp_args_element_name_9 = par_directory;
        CHECK_OBJECT( var_path );
        tmp_args_element_name_10 = var_path;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 140;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_assign_source_10 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain_join, call_args );
        }

        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_full_path;
            var_full_path = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        if ( par_encoding == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "encoding" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_compexpr_left_1 = par_encoding;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        // Tried code:
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_open_filename_1;
            PyObject *tmp_open_mode_1;
            CHECK_OBJECT( var_full_path );
            tmp_open_filename_1 = var_full_path;
            tmp_open_mode_1 = const_str_plain_rb;
            tmp_assign_source_11 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_with_1__source;
                tmp_with_1__source = tmp_assign_source_11;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_4 = tmp_with_1__source;
            tmp_called_name_2 = LOOKUP_SPECIAL( tmp_source_name_4, const_str_plain___enter__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_5;
            }
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 143;
            tmp_assign_source_12 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_2 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_with_1__enter;
                tmp_with_1__enter = tmp_assign_source_12;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_5 = tmp_with_1__source;
            tmp_assign_source_13 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___exit__ );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_with_1__exit;
                tmp_with_1__exit = tmp_assign_source_13;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = Py_True;
            {
                PyObject *old = tmp_with_1__indicator;
                tmp_with_1__indicator = tmp_assign_source_14;
                Py_INCREF( tmp_with_1__indicator );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_15;
            CHECK_OBJECT( tmp_with_1__enter );
            tmp_assign_source_15 = tmp_with_1__enter;
            {
                PyObject *old = var_bf;
                var_bf = tmp_assign_source_15;
                Py_INCREF( var_bf );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_len_arg_1;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_6;
            CHECK_OBJECT( var_bf );
            tmp_source_name_6 = var_bf;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_read );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_7;
            }
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_codecs );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_codecs );
            }

            if ( tmp_mvar_value_6 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "codecs" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 144;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_7;
            }

            tmp_source_name_7 = tmp_mvar_value_6;
            tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_BOM_UTF16_LE );
            if ( tmp_len_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 144;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_7;
            }
            tmp_args_element_name_11 = BUILTIN_LEN( tmp_len_arg_1 );
            Py_DECREF( tmp_len_arg_1 );
            if ( tmp_args_element_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 144;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_7;
            }
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 144;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_7;
            }
            {
                PyObject *old = var_data;
                var_data = tmp_assign_source_16;
                Py_XDECREF( old );
            }

        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_3 == NULL )
        {
            exception_keeper_tb_3 = MAKE_TRACEBACK( frame_ed18bd3f27453c02c18ebbeeb2000c16, exception_keeper_lineno_3 );
        }
        else if ( exception_keeper_lineno_3 != 0 )
        {
            exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_ed18bd3f27453c02c18ebbeeb2000c16, exception_keeper_lineno_3 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
        PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_17;
                tmp_assign_source_17 = Py_False;
                {
                    PyObject *old = tmp_with_1__indicator;
                    assert( old != NULL );
                    tmp_with_1__indicator = tmp_assign_source_17;
                    Py_INCREF( tmp_with_1__indicator );
                    Py_DECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_operand_name_3;
                PyObject *tmp_called_name_4;
                PyObject *tmp_args_element_name_12;
                PyObject *tmp_args_element_name_13;
                PyObject *tmp_args_element_name_14;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_4 = tmp_with_1__exit;
                tmp_args_element_name_12 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_13 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_14 = EXC_TRACEBACK(PyThreadState_GET());
                frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 144;
                {
                    PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
                    tmp_operand_name_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
                }

                if ( tmp_operand_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 144;
                    type_description_1 = "oooooooooooooo";
                    goto try_except_handler_8;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                Py_DECREF( tmp_operand_name_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 144;
                    type_description_1 = "oooooooooooooo";
                    goto try_except_handler_8;
                }
                tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 144;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame) frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_8;
                branch_no_5:;
            }
            goto branch_end_4;
            branch_no_4:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 143;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame) frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_8;
            branch_end_4:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_6;
        // End of try:
        try_end_4:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_3;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_3_load_translations );
        return NULL;
        // End of try:
        try_end_3:;
        goto try_end_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_6;
            nuitka_bool tmp_compexpr_left_3;
            nuitka_bool tmp_compexpr_right_3;
            int tmp_truth_name_1;
            CHECK_OBJECT( tmp_with_1__indicator );
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_with_1__indicator );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_5 );
                Py_XDECREF( exception_keeper_value_5 );
                Py_XDECREF( exception_keeper_tb_5 );

                exception_lineno = 143;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_left_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
            tmp_condition_result_6 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_called_name_5;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_5 = tmp_with_1__exit;
                frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 144;
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_5 );
                    Py_XDECREF( exception_keeper_value_5 );
                    Py_XDECREF( exception_keeper_tb_5 );

                    exception_lineno = 144;
                    type_description_1 = "oooooooooooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_6:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_5;
        // End of try:
        try_end_5:;
        {
            nuitka_bool tmp_condition_result_7;
            nuitka_bool tmp_compexpr_left_4;
            nuitka_bool tmp_compexpr_right_4;
            int tmp_truth_name_2;
            CHECK_OBJECT( tmp_with_1__indicator );
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_with_1__indicator );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_left_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
            tmp_condition_result_7 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_called_name_6;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_6 = tmp_with_1__exit;
                frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 144;
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 144;
                    type_description_1 = "oooooooooooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            branch_no_7:;
        }
        goto try_end_6;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        Py_XDECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        Py_XDECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        Py_XDECREF( tmp_with_1__indicator );
        tmp_with_1__indicator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_2;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        Py_XDECREF( tmp_with_1__indicator );
        tmp_with_1__indicator = NULL;

        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_8;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_source_name_9;
            PyObject *tmp_mvar_value_8;
            if ( var_data == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "data" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 145;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_compexpr_left_5 = var_data;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_codecs );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_codecs );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "codecs" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 145;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_source_name_8 = tmp_mvar_value_7;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_BOM_UTF16_LE );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 145;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_compexpr_right_5 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_compexpr_right_5, 0, tmp_tuple_element_1 );
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_codecs );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_codecs );
            }

            if ( tmp_mvar_value_8 == NULL )
            {
                Py_DECREF( tmp_compexpr_right_5 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "codecs" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 145;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_source_name_9 = tmp_mvar_value_8;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_BOM_UTF16_BE );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_right_5 );

                exception_lineno = 145;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }
            PyTuple_SET_ITEM( tmp_compexpr_right_5, 1, tmp_tuple_element_1 );
            tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
            Py_DECREF( tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 145;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_8 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_18;
                tmp_assign_source_18 = const_str_digest_913ae8f3439742e034c09e3f599385fd;
                {
                    PyObject *old = par_encoding;
                    par_encoding = tmp_assign_source_18;
                    Py_INCREF( par_encoding );
                    Py_XDECREF( old );
                }

            }
            goto branch_end_8;
            branch_no_8:;
            {
                PyObject *tmp_assign_source_19;
                tmp_assign_source_19 = const_str_digest_b92226bee77e9ca773ad5eaea1d64ab0;
                {
                    PyObject *old = par_encoding;
                    par_encoding = tmp_assign_source_19;
                    Py_INCREF( par_encoding );
                    Py_XDECREF( old );
                }

            }
            branch_end_8:;
        }
        branch_no_3:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_open_filename_2;
        PyObject *tmp_open_encoding_1;
        CHECK_OBJECT( var_full_path );
        tmp_open_filename_2 = var_full_path;
        if ( par_encoding == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "encoding" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_9;
        }

        tmp_open_encoding_1 = par_encoding;
        tmp_assign_source_20 = BUILTIN_OPEN( tmp_open_filename_2, NULL, NULL, tmp_open_encoding_1, NULL, NULL, NULL, NULL );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_with_2__source;
            tmp_with_2__source = tmp_assign_source_20;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_7;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( tmp_with_2__source );
        tmp_source_name_10 = tmp_with_2__source;
        tmp_called_name_7 = LOOKUP_SPECIAL( tmp_source_name_10, const_str_plain___enter__ );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_9;
        }
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 154;
        tmp_assign_source_21 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
        Py_DECREF( tmp_called_name_7 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_with_2__enter;
            tmp_with_2__enter = tmp_assign_source_21;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_source_name_11;
        CHECK_OBJECT( tmp_with_2__source );
        tmp_source_name_11 = tmp_with_2__source;
        tmp_assign_source_22 = LOOKUP_SPECIAL( tmp_source_name_11, const_str_plain___exit__ );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_with_2__exit;
            tmp_with_2__exit = tmp_assign_source_22;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = Py_True;
        {
            PyObject *old = tmp_with_2__indicator;
            tmp_with_2__indicator = tmp_assign_source_23;
            Py_INCREF( tmp_with_2__indicator );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_24;
        CHECK_OBJECT( tmp_with_2__enter );
        tmp_assign_source_24 = tmp_with_2__enter;
        {
            PyObject *old = var_f;
            var_f = tmp_assign_source_24;
            Py_INCREF( var_f );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_ass_subscript_1;
        tmp_ass_subvalue_1 = PyDict_New();
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__translations );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_ass_subvalue_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_translations" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_11;
        }

        tmp_ass_subscribed_1 = tmp_mvar_value_9;
        CHECK_OBJECT( var_locale );
        tmp_ass_subscript_1 = var_locale;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_11;
        }
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_name_8;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_16;
        tmp_called_name_8 = (PyObject *)&PyEnum_Type;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_csv );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_csv );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "csv" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_11;
        }

        tmp_called_instance_7 = tmp_mvar_value_10;
        CHECK_OBJECT( var_f );
        tmp_args_element_name_16 = var_f;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 156;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_args_element_name_15 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_reader, call_args );
        }

        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_11;
        }
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 156;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_11;
        }
        tmp_assign_source_25 = MAKE_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_25;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_26;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_26 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_26 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooo";
                exception_lineno = 156;
                goto try_except_handler_12;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_26;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_iter_arg_4;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_iter_arg_4 = tmp_for_loop_2__iter_value;
        tmp_assign_source_27 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_13;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__source_iter;
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_27;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_28 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_28 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 156;
            goto try_except_handler_14;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_1;
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_28;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_29 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_29 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 156;
            goto try_except_handler_14;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_2;
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_29;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooo";
                    exception_lineno = 156;
                    goto try_except_handler_14;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooo";
            exception_lineno = 156;
            goto try_except_handler_14;
        }
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_13;
    // End of try:
    try_end_7:;
    goto try_end_8;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_12;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_30;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_30 = tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_30;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_31;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_31 = tmp_tuple_unpack_2__element_2;
        {
            PyObject *old = var_row;
            var_row = tmp_assign_source_31;
            Py_INCREF( var_row );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_9;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_operand_name_4;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_len_arg_2;
        CHECK_OBJECT( var_row );
        tmp_operand_name_4 = var_row;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }
        tmp_or_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( var_row );
        tmp_len_arg_2 = var_row;
        tmp_compexpr_left_6 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_compexpr_left_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }
        tmp_compexpr_right_6 = const_int_pos_2;
        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        Py_DECREF( tmp_compexpr_left_6 );
        assert( !(tmp_res == -1) );
        tmp_or_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_9 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_9 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        goto loop_start_2;
        branch_no_9:;
    }
    {
        PyObject *tmp_assign_source_32;
        // Tried code:
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_iter_arg_5;
            CHECK_OBJECT( var_row );
            tmp_iter_arg_5 = var_row;
            tmp_assign_source_33 = MAKE_ITERATOR( tmp_iter_arg_5 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_15;
            }
            {
                PyObject *old = tmp_listcomp_1__$0;
                tmp_listcomp_1__$0 = tmp_assign_source_33;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_34;
            tmp_assign_source_34 = PyList_New( 0 );
            {
                PyObject *old = tmp_listcomp_1__contraction;
                tmp_listcomp_1__contraction = tmp_assign_source_34;
                Py_XDECREF( old );
            }

        }
        MAKE_OR_REUSE_FRAME( cache_frame_a0ec6a3f96d0d015c6376ed1a6637a77_2, codeobj_a0ec6a3f96d0d015c6376ed1a6637a77, module_tornado$locale, sizeof(void *) );
        frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 = cache_frame_a0ec6a3f96d0d015c6376ed1a6637a77_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_3:;
        {
            PyObject *tmp_next_source_3;
            PyObject *tmp_assign_source_35;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_3 = tmp_listcomp_1__$0;
            tmp_assign_source_35 = ITERATOR_NEXT( tmp_next_source_3 );
            if ( tmp_assign_source_35 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_3;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 159;
                    goto try_except_handler_16;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_35;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_36;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_36 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_c;
                outline_0_var_c = tmp_assign_source_36;
                Py_INCREF( outline_0_var_c );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_instance_8;
            PyObject *tmp_called_instance_9;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_args_element_name_17;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_escape );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_escape );
            }

            if ( tmp_mvar_value_11 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "escape" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 159;
                type_description_2 = "o";
                goto try_except_handler_16;
            }

            tmp_called_instance_9 = tmp_mvar_value_11;
            CHECK_OBJECT( outline_0_var_c );
            tmp_args_element_name_17 = outline_0_var_c;
            frame_a0ec6a3f96d0d015c6376ed1a6637a77_2->m_frame.f_lineno = 159;
            {
                PyObject *call_args[] = { tmp_args_element_name_17 };
                tmp_called_instance_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_to_unicode, call_args );
            }

            if ( tmp_called_instance_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_2 = "o";
                goto try_except_handler_16;
            }
            frame_a0ec6a3f96d0d015c6376ed1a6637a77_2->m_frame.f_lineno = 159;
            tmp_append_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_strip );
            Py_DECREF( tmp_called_instance_8 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_2 = "o";
                goto try_except_handler_16;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_2 = "o";
                goto try_except_handler_16;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_2 = "o";
            goto try_except_handler_16;
        }
        goto loop_start_3;
        loop_end_3:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_32 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_32 );
        goto try_return_handler_16;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_3_load_translations );
        return NULL;
        // Return handler code:
        try_return_handler_16:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_15;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_a0ec6a3f96d0d015c6376ed1a6637a77_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_a0ec6a3f96d0d015c6376ed1a6637a77_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_a0ec6a3f96d0d015c6376ed1a6637a77_2,
            type_description_2,
            outline_0_var_c
        );


        // Release cached frame.
        if ( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 == cache_frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 )
        {
            Py_DECREF( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 );
        }
        cache_frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 = NULL;

        assertFrameObject( frame_a0ec6a3f96d0d015c6376ed1a6637a77_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oooooooooooooo";
        goto try_except_handler_15;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_3_load_translations );
        return NULL;
        // Return handler code:
        try_return_handler_15:;
        Py_XDECREF( outline_0_var_c );
        outline_0_var_c = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_c );
        outline_0_var_c = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_3_load_translations );
        return NULL;
        outline_exception_1:;
        exception_lineno = 159;
        goto try_except_handler_12;
        outline_result_1:;
        {
            PyObject *old = var_row;
            assert( old != NULL );
            var_row = tmp_assign_source_32;
            Py_DECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_iter_arg_6;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( var_row );
        tmp_subscribed_name_1 = var_row;
        tmp_subscript_name_1 = const_slice_none_int_pos_2_none;
        tmp_iter_arg_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_iter_arg_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_17;
        }
        tmp_assign_source_37 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
        Py_DECREF( tmp_iter_arg_6 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_17;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__source_iter;
            tmp_tuple_unpack_3__source_iter = tmp_assign_source_37;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_unpack_5;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_38 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
        if ( tmp_assign_source_38 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 160;
            goto try_except_handler_18;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_1;
            tmp_tuple_unpack_3__element_1 = tmp_assign_source_38;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_unpack_6;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_39 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
        if ( tmp_assign_source_39 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 160;
            goto try_except_handler_18;
        }
        {
            PyObject *old = tmp_tuple_unpack_3__element_2;
            tmp_tuple_unpack_3__element_2 = tmp_assign_source_39;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_3;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooo";
                    exception_lineno = 160;
                    goto try_except_handler_18;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooo";
            exception_lineno = 160;
            goto try_except_handler_18;
        }
    }
    goto try_end_9;
    // Exception handler code:
    try_except_handler_18:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto try_except_handler_17;
    // End of try:
    try_end_9:;
    goto try_end_10;
    // Exception handler code:
    try_except_handler_17:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto try_except_handler_12;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    {
        PyObject *tmp_assign_source_40;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
        tmp_assign_source_40 = tmp_tuple_unpack_3__element_1;
        {
            PyObject *old = var_english;
            var_english = tmp_assign_source_40;
            Py_INCREF( var_english );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    {
        PyObject *tmp_assign_source_41;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
        tmp_assign_source_41 = tmp_tuple_unpack_3__element_2;
        {
            PyObject *old = var_translation;
            var_translation = tmp_assign_source_41;
            Py_INCREF( var_translation );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        PyObject *tmp_len_arg_3;
        CHECK_OBJECT( var_row );
        tmp_len_arg_3 = var_row;
        tmp_compexpr_left_7 = BUILTIN_LEN( tmp_len_arg_3 );
        if ( tmp_compexpr_left_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }
        tmp_compexpr_right_7 = const_int_pos_2;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
        Py_DECREF( tmp_compexpr_left_7 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_42;
            int tmp_or_left_truth_2;
            PyObject *tmp_or_left_value_2;
            PyObject *tmp_or_right_value_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( var_row );
            tmp_subscribed_name_2 = var_row;
            tmp_subscript_name_2 = const_int_pos_2;
            tmp_or_left_value_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 2 );
            if ( tmp_or_left_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
            if ( tmp_or_left_truth_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_or_left_value_2 );

                exception_lineno = 162;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            if ( tmp_or_left_truth_2 == 1 )
            {
                goto or_left_2;
            }
            else
            {
                goto or_right_2;
            }
            or_right_2:;
            Py_DECREF( tmp_or_left_value_2 );
            tmp_or_right_value_2 = const_str_plain_unknown;
            Py_INCREF( tmp_or_right_value_2 );
            tmp_assign_source_42 = tmp_or_right_value_2;
            goto or_end_2;
            or_left_2:;
            tmp_assign_source_42 = tmp_or_left_value_2;
            or_end_2:;
            {
                PyObject *old = var_plural;
                var_plural = tmp_assign_source_42;
                Py_XDECREF( old );
            }

        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_43;
            tmp_assign_source_43 = const_str_plain_unknown;
            {
                PyObject *old = var_plural;
                var_plural = tmp_assign_source_43;
                Py_INCREF( var_plural );
                Py_XDECREF( old );
            }

        }
        branch_end_10:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        CHECK_OBJECT( var_plural );
        tmp_compexpr_left_8 = var_plural;
        tmp_compexpr_right_8 = const_tuple_str_plain_plural_str_plain_singular_str_plain_unknown_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_8, tmp_compexpr_left_8 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }
        tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_called_name_9;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_18;
            PyObject *tmp_args_element_name_19;
            PyObject *tmp_args_element_name_20;
            PyObject *tmp_args_element_name_21;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gen_log );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen_log );
            }

            if ( tmp_mvar_value_12 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 166;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_source_name_12 = tmp_mvar_value_12;
            tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_error );
            if ( tmp_called_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            tmp_args_element_name_18 = const_str_digest_2af2f0bb9895a7e9b768093a63173b6d;
            CHECK_OBJECT( var_plural );
            tmp_args_element_name_19 = var_plural;
            CHECK_OBJECT( var_path );
            tmp_args_element_name_20 = var_path;
            CHECK_OBJECT( var_i );
            tmp_left_name_1 = var_i;
            tmp_right_name_1 = const_int_pos_1;
            tmp_args_element_name_21 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_args_element_name_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_9 );

                exception_lineno = 170;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 166;
            {
                PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19, tmp_args_element_name_20, tmp_args_element_name_21 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_9, call_args );
            }

            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_element_name_21 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        goto loop_start_2;
        branch_no_11:;
    }
    {
        PyObject *tmp_ass_subvalue_2;
        PyObject *tmp_ass_subscribed_2;
        PyObject *tmp_called_instance_10;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_ass_subscript_2;
        CHECK_OBJECT( var_translation );
        tmp_ass_subvalue_2 = var_translation;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__translations );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_translations" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }

        tmp_subscribed_name_3 = tmp_mvar_value_13;
        CHECK_OBJECT( var_locale );
        tmp_subscript_name_3 = var_locale;
        tmp_called_instance_10 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        if ( tmp_called_instance_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }
        CHECK_OBJECT( var_plural );
        tmp_args_element_name_22 = var_plural;
        tmp_args_element_name_23 = PyDict_New();
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 173;
        {
            PyObject *call_args[] = { tmp_args_element_name_22, tmp_args_element_name_23 };
            tmp_ass_subscribed_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_10, const_str_plain_setdefault, call_args );
        }

        Py_DECREF( tmp_called_instance_10 );
        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_ass_subscribed_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }
        CHECK_OBJECT( var_english );
        tmp_ass_subscript_2 = var_english;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
        Py_DECREF( tmp_ass_subscribed_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_12;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 156;
        type_description_1 = "oooooooooooooo";
        goto try_except_handler_12;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_11;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto try_except_handler_11;
    // End of try:
    try_end_11:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    goto try_end_12;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_14 == NULL )
    {
        exception_keeper_tb_14 = MAKE_TRACEBACK( frame_ed18bd3f27453c02c18ebbeeb2000c16, exception_keeper_lineno_14 );
    }
    else if ( exception_keeper_lineno_14 != 0 )
    {
        exception_keeper_tb_14 = ADD_TRACEBACK( exception_keeper_tb_14, frame_ed18bd3f27453c02c18ebbeeb2000c16, exception_keeper_lineno_14 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
    PyException_SetTraceback( exception_keeper_value_14, (PyObject *)exception_keeper_tb_14 );
    PUBLISH_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_compexpr_left_9;
        PyObject *tmp_compexpr_right_9;
        tmp_compexpr_left_9 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_9 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_9, tmp_compexpr_right_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_19;
        }
        tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            PyObject *tmp_assign_source_44;
            tmp_assign_source_44 = Py_False;
            {
                PyObject *old = tmp_with_2__indicator;
                assert( old != NULL );
                tmp_with_2__indicator = tmp_assign_source_44;
                Py_INCREF( tmp_with_2__indicator );
                Py_DECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_called_name_10;
            PyObject *tmp_args_element_name_24;
            PyObject *tmp_args_element_name_25;
            PyObject *tmp_args_element_name_26;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_10 = tmp_with_2__exit;
            tmp_args_element_name_24 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_25 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_26 = EXC_TRACEBACK(PyThreadState_GET());
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 156;
            {
                PyObject *call_args[] = { tmp_args_element_name_24, tmp_args_element_name_25, tmp_args_element_name_26 };
                tmp_operand_name_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_10, call_args );
            }

            if ( tmp_operand_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_19;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            Py_DECREF( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_19;
            }
            tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 156;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame) frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_19;
            branch_no_13:;
        }
        goto branch_end_12;
        branch_no_12:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 154;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame) frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooooooooo";
        goto try_except_handler_19;
        branch_end_12:;
    }
    goto try_end_13;
    // Exception handler code:
    try_except_handler_19:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto try_except_handler_10;
    // End of try:
    try_end_13:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_12;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_3_load_translations );
    return NULL;
    // End of try:
    try_end_12:;
    goto try_end_14;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_14;
        nuitka_bool tmp_compexpr_left_10;
        nuitka_bool tmp_compexpr_right_10;
        int tmp_truth_name_3;
        CHECK_OBJECT( tmp_with_2__indicator );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_with_2__indicator );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            Py_DECREF( exception_keeper_type_16 );
            Py_XDECREF( exception_keeper_value_16 );
            Py_XDECREF( exception_keeper_tb_16 );

            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_compexpr_left_10 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_compexpr_right_10 = NUITKA_BOOL_TRUE;
        tmp_condition_result_14 = ( tmp_compexpr_left_10 == tmp_compexpr_right_10 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_called_name_11;
            PyObject *tmp_call_result_5;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_11 = tmp_with_2__exit;
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 156;
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_16 );
                Py_XDECREF( exception_keeper_value_16 );
                Py_XDECREF( exception_keeper_tb_16 );

                exception_lineno = 156;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_9;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_14:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto try_except_handler_9;
    // End of try:
    try_end_14:;
    {
        nuitka_bool tmp_condition_result_15;
        nuitka_bool tmp_compexpr_left_11;
        nuitka_bool tmp_compexpr_right_11;
        int tmp_truth_name_4;
        CHECK_OBJECT( tmp_with_2__indicator );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_with_2__indicator );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_compexpr_left_11 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_compexpr_right_11 = NUITKA_BOOL_TRUE;
        tmp_condition_result_15 = ( tmp_compexpr_left_11 == tmp_compexpr_right_11 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_15;
        }
        else
        {
            goto branch_no_15;
        }
        branch_yes_15:;
        {
            PyObject *tmp_called_name_12;
            PyObject *tmp_call_result_6;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_12 = tmp_with_2__exit;
            frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 156;
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_9;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        branch_no_15:;
    }
    goto try_end_15;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_17 = exception_type;
    exception_keeper_value_17 = exception_value;
    exception_keeper_tb_17 = exception_tb;
    exception_keeper_lineno_17 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    Py_XDECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    Py_XDECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    Py_XDECREF( tmp_with_2__indicator );
    tmp_with_2__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_17;
    exception_value = exception_keeper_value_17;
    exception_tb = exception_keeper_tb_17;
    exception_lineno = exception_keeper_lineno_17;

    goto try_except_handler_2;
    // End of try:
    try_end_15:;
    CHECK_OBJECT( (PyObject *)tmp_with_2__source );
    Py_DECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__enter );
    Py_DECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__exit );
    Py_DECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    Py_XDECREF( tmp_with_2__indicator );
    tmp_with_2__indicator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "oooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_16;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_18 = exception_type;
    exception_keeper_value_18 = exception_value;
    exception_keeper_tb_18 = exception_tb;
    exception_keeper_lineno_18 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_18;
    exception_value = exception_keeper_value_18;
    exception_tb = exception_keeper_tb_18;
    exception_lineno = exception_keeper_lineno_18;

    goto frame_exception_exit_1;
    // End of try:
    try_end_16:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_frozenset_arg_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_11;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_right_name_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__translations );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_translations" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_11 = tmp_mvar_value_14;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 174;
        tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_keys );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__default_locale );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__default_locale );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_left_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_default_locale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_list_element_1 = tmp_mvar_value_15;
        tmp_right_name_2 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_right_name_2, 0, tmp_list_element_1 );
        tmp_frozenset_arg_1 = BINARY_OPERATION_ADD_LIST_LIST( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_2 );
        assert( !(tmp_frozenset_arg_1 == NULL) );
        tmp_assign_source_45 = PyFrozenSet_New( tmp_frozenset_arg_1 );
        Py_DECREF( tmp_frozenset_arg_1 );
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_called_name_13;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_called_name_14;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_mvar_value_17;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gen_log );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen_log );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen_log" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_16;
        tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_debug );
        if ( tmp_called_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_27 = const_str_digest_3f0e87ccc267dc20a6762772e63cb9c6;
        tmp_called_name_14 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_14 != NULL );
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__supported_locales );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_called_name_13 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_supported_locales" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_29 = tmp_mvar_value_17;
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_29 };
            tmp_args_element_name_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_13 );

            exception_lineno = 175;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_27, tmp_args_element_name_28 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_called_name_13 );
        Py_DECREF( tmp_args_element_name_28 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed18bd3f27453c02c18ebbeeb2000c16 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed18bd3f27453c02c18ebbeeb2000c16 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ed18bd3f27453c02c18ebbeeb2000c16, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ed18bd3f27453c02c18ebbeeb2000c16->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ed18bd3f27453c02c18ebbeeb2000c16, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ed18bd3f27453c02c18ebbeeb2000c16,
        type_description_1,
        par_directory,
        par_encoding,
        var_path,
        var_locale,
        var_extension,
        var_full_path,
        var_bf,
        var_data,
        var_f,
        var_i,
        var_row,
        var_english,
        var_translation,
        var_plural
    );


    // Release cached frame.
    if ( frame_ed18bd3f27453c02c18ebbeeb2000c16 == cache_frame_ed18bd3f27453c02c18ebbeeb2000c16 )
    {
        Py_DECREF( frame_ed18bd3f27453c02c18ebbeeb2000c16 );
    }
    cache_frame_ed18bd3f27453c02c18ebbeeb2000c16 = NULL;

    assertFrameObject( frame_ed18bd3f27453c02c18ebbeeb2000c16 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_3_load_translations );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_directory );
    Py_DECREF( par_directory );
    par_directory = NULL;

    Py_XDECREF( par_encoding );
    par_encoding = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_locale );
    var_locale = NULL;

    Py_XDECREF( var_extension );
    var_extension = NULL;

    Py_XDECREF( var_full_path );
    var_full_path = NULL;

    Py_XDECREF( var_bf );
    var_bf = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_row );
    var_row = NULL;

    Py_XDECREF( var_english );
    var_english = NULL;

    Py_XDECREF( var_translation );
    var_translation = NULL;

    Py_XDECREF( var_plural );
    var_plural = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_19 = exception_type;
    exception_keeper_value_19 = exception_value;
    exception_keeper_tb_19 = exception_tb;
    exception_keeper_lineno_19 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_directory );
    Py_DECREF( par_directory );
    par_directory = NULL;

    Py_XDECREF( par_encoding );
    par_encoding = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_locale );
    var_locale = NULL;

    Py_XDECREF( var_extension );
    var_extension = NULL;

    Py_XDECREF( var_full_path );
    var_full_path = NULL;

    Py_XDECREF( var_bf );
    var_bf = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_row );
    var_row = NULL;

    Py_XDECREF( var_english );
    var_english = NULL;

    Py_XDECREF( var_translation );
    var_translation = NULL;

    Py_XDECREF( var_plural );
    var_plural = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_19;
    exception_value = exception_keeper_value_19;
    exception_tb = exception_keeper_tb_19;
    exception_lineno = exception_keeper_lineno_19;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_3_load_translations );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_4_load_gettext_translations( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_directory = python_pars[ 0 ];
    PyObject *par_domain = python_pars[ 1 ];
    PyObject *var_gettext = NULL;
    PyObject *var_lang = NULL;
    PyObject *var_e = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_6e03f2d96809b9c77d314ed0447d982b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_6e03f2d96809b9c77d314ed0447d982b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6e03f2d96809b9c77d314ed0447d982b, codeobj_6e03f2d96809b9c77d314ed0447d982b, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6e03f2d96809b9c77d314ed0447d982b = cache_frame_6e03f2d96809b9c77d314ed0447d982b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6e03f2d96809b9c77d314ed0447d982b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6e03f2d96809b9c77d314ed0447d982b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_gettext;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 199;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_gettext == NULL );
        var_gettext = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations, tmp_assign_source_2 );
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 205;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_directory );
        tmp_args_element_name_1 = par_directory;
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 205;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_listdir, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 205;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_lang;
            var_lang = tmp_assign_source_5;
            Py_INCREF( var_lang );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_lang );
        tmp_called_instance_2 = var_lang;
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 206;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 206;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        int tmp_truth_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_isfile );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_path );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_directory );
        tmp_args_element_name_3 = par_directory;
        CHECK_OBJECT( var_lang );
        tmp_args_element_name_4 = var_lang;
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 208;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_3, const_str_plain_join, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 208;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        goto loop_start_1;
        branch_no_2:;
    }
    // Tried code:
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }

        tmp_source_name_4 = tmp_mvar_value_4;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stat );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }

        tmp_source_name_6 = tmp_mvar_value_5;
        tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_path );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_join );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        CHECK_OBJECT( par_directory );
        tmp_args_element_name_6 = par_directory;
        CHECK_OBJECT( var_lang );
        tmp_args_element_name_7 = var_lang;
        tmp_args_element_name_8 = const_str_plain_LC_MESSAGES;
        CHECK_OBJECT( par_domain );
        tmp_left_name_1 = par_domain;
        tmp_right_name_1 = const_str_digest_4e0679129fefff8661d681147edadefe;
        tmp_args_element_name_9 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 211;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 211;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_7;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( var_gettext );
        tmp_source_name_7 = var_gettext;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_translation );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        CHECK_OBJECT( par_domain );
        tmp_tuple_element_1 = par_domain;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_directory );
        tmp_tuple_element_1 = par_directory;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_languages;
        CHECK_OBJECT( var_lang );
        tmp_list_element_1 = var_lang;
        tmp_dict_value_1 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_dict_value_1, 0, tmp_list_element_1 );
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 212;
        tmp_ass_subvalue_1 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_ass_subvalue_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__translations );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_ass_subvalue_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_translations" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 212;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }

        tmp_ass_subscribed_1 = tmp_mvar_value_6;
        CHECK_OBJECT( var_lang );
        tmp_ass_subscript_1 = var_lang;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_6e03f2d96809b9c77d314ed0447d982b, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_6e03f2d96809b9c77d314ed0447d982b, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_Exception;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = EXC_VALUE(PyThreadState_GET());
            {
                PyObject *old = var_e;
                var_e = tmp_assign_source_6;
                Py_INCREF( var_e );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_8;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_unicode_arg_1;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gen_log );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen_log );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 216;
                type_description_1 = "ooooo";
                goto try_except_handler_5;
            }

            tmp_source_name_8 = tmp_mvar_value_7;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_error );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "ooooo";
                goto try_except_handler_5;
            }
            tmp_args_element_name_10 = const_str_digest_d04aabd6bd2a7d0af6798b3dfa0b3895;
            CHECK_OBJECT( var_lang );
            tmp_args_element_name_11 = var_lang;
            CHECK_OBJECT( var_e );
            tmp_unicode_arg_1 = var_e;
            tmp_args_element_name_12 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_args_element_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 216;
                type_description_1 = "ooooo";
                goto try_except_handler_5;
            }
            frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 216;
            {
                PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_12 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "ooooo";
                goto try_except_handler_5;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        goto try_continue_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_4_load_gettext_translations );
        return NULL;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_4;
        // try continue handler code:
        try_continue_handler_5:;
        Py_XDECREF( var_e );
        var_e = NULL;

        goto try_continue_handler_4;
        // End of try:
        goto branch_end_3;
        branch_no_3:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 210;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_6e03f2d96809b9c77d314ed0447d982b->m_frame) frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
        branch_end_3:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_4_load_gettext_translations );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // try continue handler code:
    try_continue_handler_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto loop_start_1;
    // End of try:
    // End of try:
    try_end_1:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 205;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_frozenset_arg_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_right_name_2;
        PyObject *tmp_list_element_2;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__translations );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_translations" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 218;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_8;
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 218;
        tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_keys );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__default_locale );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__default_locale );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_left_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_default_locale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 218;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_list_element_2 = tmp_mvar_value_9;
        tmp_right_name_2 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_right_name_2, 0, tmp_list_element_2 );
        tmp_frozenset_arg_1 = BINARY_OPERATION_ADD_LIST_LIST( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_2 );
        assert( !(tmp_frozenset_arg_1 == NULL) );
        tmp_assign_source_7 = PyFrozenSet_New( tmp_frozenset_arg_1 );
        Py_DECREF( tmp_frozenset_arg_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = Py_True;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__use_gettext, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_called_name_7;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gen_log );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen_log );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen_log" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 220;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_10;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_debug );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 220;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_13 = const_str_digest_3f0e87ccc267dc20a6762772e63cb9c6;
        tmp_called_name_7 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_7 != NULL );
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__supported_locales );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_supported_locales" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 220;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_15 = tmp_mvar_value_11;
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 220;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_args_element_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 220;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_6e03f2d96809b9c77d314ed0447d982b->m_frame.f_lineno = 220;
        {
            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 220;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e03f2d96809b9c77d314ed0447d982b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e03f2d96809b9c77d314ed0447d982b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6e03f2d96809b9c77d314ed0447d982b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6e03f2d96809b9c77d314ed0447d982b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6e03f2d96809b9c77d314ed0447d982b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6e03f2d96809b9c77d314ed0447d982b,
        type_description_1,
        par_directory,
        par_domain,
        var_gettext,
        var_lang,
        var_e
    );


    // Release cached frame.
    if ( frame_6e03f2d96809b9c77d314ed0447d982b == cache_frame_6e03f2d96809b9c77d314ed0447d982b )
    {
        Py_DECREF( frame_6e03f2d96809b9c77d314ed0447d982b );
    }
    cache_frame_6e03f2d96809b9c77d314ed0447d982b = NULL;

    assertFrameObject( frame_6e03f2d96809b9c77d314ed0447d982b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_4_load_gettext_translations );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_directory );
    Py_DECREF( par_directory );
    par_directory = NULL;

    CHECK_OBJECT( (PyObject *)par_domain );
    Py_DECREF( par_domain );
    par_domain = NULL;

    CHECK_OBJECT( (PyObject *)var_gettext );
    Py_DECREF( var_gettext );
    var_gettext = NULL;

    Py_XDECREF( var_lang );
    var_lang = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_directory );
    Py_DECREF( par_directory );
    par_directory = NULL;

    CHECK_OBJECT( (PyObject *)par_domain );
    Py_DECREF( par_domain );
    par_domain = NULL;

    Py_XDECREF( var_gettext );
    var_gettext = NULL;

    Py_XDECREF( var_lang );
    var_lang = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_4_load_gettext_translations );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_5_get_supported_locales( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_5af22b6e07c6e31626b3ccb031b771c5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5af22b6e07c6e31626b3ccb031b771c5 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_5af22b6e07c6e31626b3ccb031b771c5, codeobj_5af22b6e07c6e31626b3ccb031b771c5, module_tornado$locale, 0 );
    frame_5af22b6e07c6e31626b3ccb031b771c5 = cache_frame_5af22b6e07c6e31626b3ccb031b771c5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5af22b6e07c6e31626b3ccb031b771c5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5af22b6e07c6e31626b3ccb031b771c5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__supported_locales );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_supported_locales" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 225;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5af22b6e07c6e31626b3ccb031b771c5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5af22b6e07c6e31626b3ccb031b771c5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5af22b6e07c6e31626b3ccb031b771c5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5af22b6e07c6e31626b3ccb031b771c5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5af22b6e07c6e31626b3ccb031b771c5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5af22b6e07c6e31626b3ccb031b771c5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5af22b6e07c6e31626b3ccb031b771c5,
        type_description_1
    );


    // Release cached frame.
    if ( frame_5af22b6e07c6e31626b3ccb031b771c5 == cache_frame_5af22b6e07c6e31626b3ccb031b771c5 )
    {
        Py_DECREF( frame_5af22b6e07c6e31626b3ccb031b771c5 );
    }
    cache_frame_5af22b6e07c6e31626b3ccb031b771c5 = NULL;

    assertFrameObject( frame_5af22b6e07c6e31626b3ccb031b771c5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_5_get_supported_locales );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_6_get_closest( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_locale_codes = python_pars[ 1 ];
    PyObject *var_code = NULL;
    PyObject *var_parts = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_cbd06a4c08c800b733833a97ba1bb610;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_cbd06a4c08c800b733833a97ba1bb610 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cbd06a4c08c800b733833a97ba1bb610, codeobj_cbd06a4c08c800b733833a97ba1bb610, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cbd06a4c08c800b733833a97ba1bb610 = cache_frame_cbd06a4c08c800b733833a97ba1bb610;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cbd06a4c08c800b733833a97ba1bb610 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cbd06a4c08c800b733833a97ba1bb610 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_locale_codes );
        tmp_iter_arg_1 = par_locale_codes;
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 240;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_code;
            var_code = tmp_assign_source_3;
            Py_INCREF( var_code );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_code );
        tmp_operand_name_1 = var_code;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 241;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_code );
        tmp_called_instance_1 = var_code;
        frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 243;
        tmp_assign_source_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_45_str_plain___tuple, 0 ) );

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_code;
            assert( old != NULL );
            var_code = tmp_assign_source_4;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_code );
        tmp_called_instance_2 = var_code;
        frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 244;
        tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_plain___tuple, 0 ) );

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 244;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_parts;
            var_parts = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( var_parts );
        tmp_len_arg_1 = var_parts;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_1 = const_int_pos_2;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        goto loop_start_1;
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_len_arg_2;
            CHECK_OBJECT( var_parts );
            tmp_len_arg_2 = var_parts;
            tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_2 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 247;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_compexpr_right_2 = const_int_pos_2;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_left_name_1;
                PyObject *tmp_left_name_2;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_right_name_2;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                CHECK_OBJECT( var_parts );
                tmp_subscribed_name_1 = var_parts;
                tmp_subscript_name_1 = const_int_0;
                tmp_called_instance_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
                if ( tmp_called_instance_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 248;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 248;
                tmp_left_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_lower );
                Py_DECREF( tmp_called_instance_3 );
                if ( tmp_left_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 248;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                tmp_right_name_1 = const_str_plain__;
                tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_1 );
                Py_DECREF( tmp_left_name_2 );
                if ( tmp_left_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 248;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_parts );
                tmp_subscribed_name_2 = var_parts;
                tmp_subscript_name_2 = const_int_pos_1;
                tmp_called_instance_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
                if ( tmp_called_instance_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_1 );

                    exception_lineno = 248;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 248;
                tmp_right_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_upper );
                Py_DECREF( tmp_called_instance_4 );
                if ( tmp_right_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_left_name_1 );

                    exception_lineno = 248;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                tmp_assign_source_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
                Py_DECREF( tmp_left_name_1 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 248;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_code;
                    assert( old != NULL );
                    var_code = tmp_assign_source_6;
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        branch_end_2:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_mvar_value_1;
        if ( var_code == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "code" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 249;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_compexpr_left_3 = var_code;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__supported_locales );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_supported_locales" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 249;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_compexpr_right_3 = tmp_mvar_value_1;
        tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_cls );
            tmp_source_name_1 = par_cls;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 250;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            if ( var_code == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "code" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 250;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }

            tmp_args_element_name_1 = var_code;
            frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 250;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 250;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            goto try_return_handler_2;
        }
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_parts );
        tmp_subscribed_name_3 = var_parts;
        tmp_subscript_name_3 = const_int_0;
        tmp_called_instance_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 251;
        tmp_compexpr_left_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_lower );
        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__supported_locales );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_compexpr_left_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_supported_locales" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 251;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_compexpr_right_4 = tmp_mvar_value_2;
        tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_instance_6;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            CHECK_OBJECT( par_cls );
            tmp_source_name_2 = par_cls;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_get );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 252;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( var_parts );
            tmp_subscribed_name_4 = var_parts;
            tmp_subscript_name_4 = const_int_0;
            tmp_called_instance_6 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
            if ( tmp_called_instance_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 252;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 252;
            tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_lower );
            Py_DECREF( tmp_called_instance_6 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 252;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 252;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 252;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            goto try_return_handler_2;
        }
        branch_no_5:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 240;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( par_cls );
        tmp_source_name_3 = par_cls;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_get );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__default_locale );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__default_locale );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_default_locale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 253;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = tmp_mvar_value_3;
        frame_cbd06a4c08c800b733833a97ba1bb610->m_frame.f_lineno = 253;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cbd06a4c08c800b733833a97ba1bb610 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cbd06a4c08c800b733833a97ba1bb610 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cbd06a4c08c800b733833a97ba1bb610 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cbd06a4c08c800b733833a97ba1bb610, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cbd06a4c08c800b733833a97ba1bb610->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cbd06a4c08c800b733833a97ba1bb610, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cbd06a4c08c800b733833a97ba1bb610,
        type_description_1,
        par_cls,
        par_locale_codes,
        var_code,
        var_parts
    );


    // Release cached frame.
    if ( frame_cbd06a4c08c800b733833a97ba1bb610 == cache_frame_cbd06a4c08c800b733833a97ba1bb610 )
    {
        Py_DECREF( frame_cbd06a4c08c800b733833a97ba1bb610 );
    }
    cache_frame_cbd06a4c08c800b733833a97ba1bb610 = NULL;

    assertFrameObject( frame_cbd06a4c08c800b733833a97ba1bb610 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_6_get_closest );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_locale_codes );
    Py_DECREF( par_locale_codes );
    par_locale_codes = NULL;

    Py_XDECREF( var_code );
    var_code = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_locale_codes );
    Py_DECREF( par_locale_codes );
    par_locale_codes = NULL;

    Py_XDECREF( var_code );
    var_code = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_6_get_closest );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_7_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_code = python_pars[ 1 ];
    PyObject *var_translations = NULL;
    PyObject *var_locale = NULL;
    struct Nuitka_FrameObject *frame_173bd6178f556cc83e3014643cc70611;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_173bd6178f556cc83e3014643cc70611 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_173bd6178f556cc83e3014643cc70611, codeobj_173bd6178f556cc83e3014643cc70611, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_173bd6178f556cc83e3014643cc70611 = cache_frame_173bd6178f556cc83e3014643cc70611;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_173bd6178f556cc83e3014643cc70611 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_173bd6178f556cc83e3014643cc70611 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_code );
        tmp_compexpr_left_1 = par_code;
        CHECK_OBJECT( par_cls );
        tmp_source_name_1 = par_cls;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__cache );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_mvar_value_1;
            CHECK_OBJECT( par_code );
            tmp_compexpr_left_2 = par_code;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__supported_locales );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_supported_locales" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 262;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_right_2 = tmp_mvar_value_1;
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 262;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                tmp_raise_type_1 = PyExc_AssertionError;
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_lineno = 262;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__translations );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_translations" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 263;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            CHECK_OBJECT( par_code );
            tmp_args_element_name_1 = par_code;
            tmp_args_element_name_2 = Py_None;
            frame_173bd6178f556cc83e3014643cc70611->m_frame.f_lineno = 263;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 263;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_translations == NULL );
            var_translations = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( var_translations );
            tmp_compexpr_left_3 = var_translations;
            tmp_compexpr_right_3 = Py_None;
            tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_args_element_name_4;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CSVLocale );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CSVLocale );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CSVLocale" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 265;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_1 = tmp_mvar_value_3;
                CHECK_OBJECT( par_code );
                tmp_args_element_name_3 = par_code;
                tmp_args_element_name_4 = PyDict_New();
                frame_173bd6178f556cc83e3014643cc70611->m_frame.f_lineno = 265;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                    tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_args_element_name_4 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 265;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( var_locale == NULL );
                var_locale = tmp_assign_source_2;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_mvar_value_4;
                int tmp_truth_name_1;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__use_gettext );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__use_gettext );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_use_gettext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 266;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }

                tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_4 );
                if ( tmp_truth_name_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 266;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_3;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_mvar_value_5;
                    PyObject *tmp_args_element_name_5;
                    PyObject *tmp_args_element_name_6;
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_GettextLocale );

                    if (unlikely( tmp_mvar_value_5 == NULL ))
                    {
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_GettextLocale );
                    }

                    if ( tmp_mvar_value_5 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "GettextLocale" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 267;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_2 = tmp_mvar_value_5;
                    CHECK_OBJECT( par_code );
                    tmp_args_element_name_5 = par_code;
                    CHECK_OBJECT( var_translations );
                    tmp_args_element_name_6 = var_translations;
                    frame_173bd6178f556cc83e3014643cc70611->m_frame.f_lineno = 267;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                        tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
                    }

                    if ( tmp_assign_source_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 267;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_locale == NULL );
                    var_locale = tmp_assign_source_3;
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    PyObject *tmp_assign_source_4;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_6;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CSVLocale );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CSVLocale );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CSVLocale" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 269;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_3 = tmp_mvar_value_6;
                    CHECK_OBJECT( par_code );
                    tmp_args_element_name_7 = par_code;
                    CHECK_OBJECT( var_translations );
                    tmp_args_element_name_8 = var_translations;
                    frame_173bd6178f556cc83e3014643cc70611->m_frame.f_lineno = 269;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
                        tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                    }

                    if ( tmp_assign_source_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 269;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_locale == NULL );
                    var_locale = tmp_assign_source_4;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_ass_subscript_1;
            if ( var_locale == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "locale" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 270;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_ass_subvalue_1 = var_locale;
            CHECK_OBJECT( par_cls );
            tmp_source_name_2 = par_cls;
            tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__cache );
            if ( tmp_ass_subscribed_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 270;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_code );
            tmp_ass_subscript_1 = par_code;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subscribed_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 270;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_cls );
        tmp_source_name_3 = par_cls;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__cache );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_code );
        tmp_subscript_name_1 = par_code;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_173bd6178f556cc83e3014643cc70611 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_173bd6178f556cc83e3014643cc70611 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_173bd6178f556cc83e3014643cc70611 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_173bd6178f556cc83e3014643cc70611, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_173bd6178f556cc83e3014643cc70611->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_173bd6178f556cc83e3014643cc70611, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_173bd6178f556cc83e3014643cc70611,
        type_description_1,
        par_cls,
        par_code,
        var_translations,
        var_locale
    );


    // Release cached frame.
    if ( frame_173bd6178f556cc83e3014643cc70611 == cache_frame_173bd6178f556cc83e3014643cc70611 )
    {
        Py_DECREF( frame_173bd6178f556cc83e3014643cc70611 );
    }
    cache_frame_173bd6178f556cc83e3014643cc70611 = NULL;

    assertFrameObject( frame_173bd6178f556cc83e3014643cc70611 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_7_get );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    Py_XDECREF( var_translations );
    var_translations = NULL;

    Py_XDECREF( var_locale );
    var_locale = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    Py_XDECREF( var_translations );
    var_translations = NULL;

    Py_XDECREF( var_locale );
    var_locale = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_7_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_8___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_code = python_pars[ 1 ];
    PyObject *var_prefix = NULL;
    PyObject *var__ = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_61a8788ba2981b76b0667ee4f53933ac;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_61a8788ba2981b76b0667ee4f53933ac = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_61a8788ba2981b76b0667ee4f53933ac, codeobj_61a8788ba2981b76b0667ee4f53933ac, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_61a8788ba2981b76b0667ee4f53933ac = cache_frame_61a8788ba2981b76b0667ee4f53933ac;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_61a8788ba2981b76b0667ee4f53933ac );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_61a8788ba2981b76b0667ee4f53933ac ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_code );
        tmp_assattr_name_1 = par_code;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_code, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_LOCALE_NAMES );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LOCALE_NAMES );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LOCALE_NAMES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 275;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_1;
        CHECK_OBJECT( par_code );
        tmp_args_element_name_1 = par_code;
        tmp_args_element_name_2 = PyDict_New();
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 275;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_called_instance_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 275;
        tmp_assattr_name_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_name_str_plain_Unknown_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_name, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        tmp_assattr_name_3 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_rtl, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 276;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        tmp_iter_arg_1 = const_tuple_str_plain_fa_str_plain_ar_str_plain_he_tuple;
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        assert( !(tmp_assign_source_1 == NULL) );
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 277;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_prefix;
            var_prefix = tmp_assign_source_3;
            Py_INCREF( var_prefix );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_code );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 278;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_prefix );
        tmp_args_element_name_3 = var_prefix;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 278;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 278;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 278;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_4;
            PyObject *tmp_assattr_target_4;
            tmp_assattr_name_4 = Py_True;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_4 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_rtl, tmp_assattr_name_4 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 279;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
        }
        goto loop_end_1;
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 277;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_translate );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_called_name_7;
        PyObject *tmp_called_name_8;
        PyObject *tmp_called_name_9;
        PyObject *tmp_called_name_10;
        PyObject *tmp_called_name_11;
        PyObject *tmp_called_name_12;
        PyObject *tmp_assattr_target_5;
        CHECK_OBJECT( var__ );
        tmp_called_name_1 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 285;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_str_plain_January_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_5 = PyList_New( 12 );
        PyList_SET_ITEM( tmp_assattr_name_5, 0, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_2 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 286;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_plain_February_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 286;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 1, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_3 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 287;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_plain_March_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 287;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 2, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_4 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 288;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_plain_April_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 288;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 3, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_5 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 289;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_str_plain_May_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 289;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 4, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_6 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 290;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_plain_June_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 290;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 5, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_7 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 291;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_str_plain_July_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 291;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 6, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_8 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 292;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_str_plain_August_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 292;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 7, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_9 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 293;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_plain_September_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 293;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 8, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_10 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 294;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, &PyTuple_GET_ITEM( const_tuple_str_plain_October_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 294;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 9, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_11 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 295;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_plain_November_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 295;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 10, tmp_list_element_1 );
        CHECK_OBJECT( var__ );
        tmp_called_name_12 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 296;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_str_plain_December_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_5 );

            exception_lineno = 296;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_5, 11, tmp_list_element_1 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__months, tmp_assattr_name_5 );
        Py_DECREF( tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 284;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_list_element_2;
        PyObject *tmp_called_name_13;
        PyObject *tmp_called_name_14;
        PyObject *tmp_called_name_15;
        PyObject *tmp_called_name_16;
        PyObject *tmp_called_name_17;
        PyObject *tmp_called_name_18;
        PyObject *tmp_called_name_19;
        PyObject *tmp_assattr_target_6;
        CHECK_OBJECT( var__ );
        tmp_called_name_13 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 299;
        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_plain_Monday_tuple, 0 ) );

        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_6 = PyList_New( 7 );
        PyList_SET_ITEM( tmp_assattr_name_6, 0, tmp_list_element_2 );
        CHECK_OBJECT( var__ );
        tmp_called_name_14 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 300;
        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_str_plain_Tuesday_tuple, 0 ) );

        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_6 );

            exception_lineno = 300;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_6, 1, tmp_list_element_2 );
        CHECK_OBJECT( var__ );
        tmp_called_name_15 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 301;
        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_plain_Wednesday_tuple, 0 ) );

        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_6 );

            exception_lineno = 301;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_6, 2, tmp_list_element_2 );
        CHECK_OBJECT( var__ );
        tmp_called_name_16 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 302;
        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_str_plain_Thursday_tuple, 0 ) );

        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_6 );

            exception_lineno = 302;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_6, 3, tmp_list_element_2 );
        CHECK_OBJECT( var__ );
        tmp_called_name_17 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 303;
        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_plain_Friday_tuple, 0 ) );

        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_6 );

            exception_lineno = 303;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_6, 4, tmp_list_element_2 );
        CHECK_OBJECT( var__ );
        tmp_called_name_18 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 304;
        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_plain_Saturday_tuple, 0 ) );

        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_6 );

            exception_lineno = 304;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_6, 5, tmp_list_element_2 );
        CHECK_OBJECT( var__ );
        tmp_called_name_19 = var__;
        frame_61a8788ba2981b76b0667ee4f53933ac->m_frame.f_lineno = 305;
        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, &PyTuple_GET_ITEM( const_tuple_str_plain_Sunday_tuple, 0 ) );

        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_6 );

            exception_lineno = 305;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assattr_name_6, 6, tmp_list_element_2 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__weekdays, tmp_assattr_name_6 );
        Py_DECREF( tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 298;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_61a8788ba2981b76b0667ee4f53933ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_61a8788ba2981b76b0667ee4f53933ac );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_61a8788ba2981b76b0667ee4f53933ac, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_61a8788ba2981b76b0667ee4f53933ac->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_61a8788ba2981b76b0667ee4f53933ac, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_61a8788ba2981b76b0667ee4f53933ac,
        type_description_1,
        par_self,
        par_code,
        var_prefix,
        var__
    );


    // Release cached frame.
    if ( frame_61a8788ba2981b76b0667ee4f53933ac == cache_frame_61a8788ba2981b76b0667ee4f53933ac )
    {
        Py_DECREF( frame_61a8788ba2981b76b0667ee4f53933ac );
    }
    cache_frame_61a8788ba2981b76b0667ee4f53933ac = NULL;

    assertFrameObject( frame_61a8788ba2981b76b0667ee4f53933ac );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_8___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    Py_XDECREF( var_prefix );
    var_prefix = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    Py_XDECREF( var_prefix );
    var_prefix = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_8___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_9_translate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_message = python_pars[ 1 ];
    PyObject *par_plural_message = python_pars[ 2 ];
    PyObject *par_count = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_7b47ccf3d9329dc01e17f4b0c470baaf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7b47ccf3d9329dc01e17f4b0c470baaf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7b47ccf3d9329dc01e17f4b0c470baaf, codeobj_7b47ccf3d9329dc01e17f4b0c470baaf, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7b47ccf3d9329dc01e17f4b0c470baaf = cache_frame_7b47ccf3d9329dc01e17f4b0c470baaf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7b47ccf3d9329dc01e17f4b0c470baaf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7b47ccf3d9329dc01e17f4b0c470baaf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        frame_7b47ccf3d9329dc01e17f4b0c470baaf->m_frame.f_lineno = 318;
        tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_NotImplementedError );
        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 318;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b47ccf3d9329dc01e17f4b0c470baaf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b47ccf3d9329dc01e17f4b0c470baaf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7b47ccf3d9329dc01e17f4b0c470baaf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7b47ccf3d9329dc01e17f4b0c470baaf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7b47ccf3d9329dc01e17f4b0c470baaf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7b47ccf3d9329dc01e17f4b0c470baaf,
        type_description_1,
        par_self,
        par_message,
        par_plural_message,
        par_count
    );


    // Release cached frame.
    if ( frame_7b47ccf3d9329dc01e17f4b0c470baaf == cache_frame_7b47ccf3d9329dc01e17f4b0c470baaf )
    {
        Py_DECREF( frame_7b47ccf3d9329dc01e17f4b0c470baaf );
    }
    cache_frame_7b47ccf3d9329dc01e17f4b0c470baaf = NULL;

    assertFrameObject( frame_7b47ccf3d9329dc01e17f4b0c470baaf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_9_translate );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_9_translate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_tornado$locale$$$function_10_pgettext( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    PyObject *par_message = python_pars[ 2 ];
    PyObject *par_plural_message = python_pars[ 3 ];
    PyObject *par_count = python_pars[ 4 ];
    struct Nuitka_FrameObject *frame_650c77969df137a567f8457a5a94a2f8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_650c77969df137a567f8457a5a94a2f8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_650c77969df137a567f8457a5a94a2f8, codeobj_650c77969df137a567f8457a5a94a2f8, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_650c77969df137a567f8457a5a94a2f8 = cache_frame_650c77969df137a567f8457a5a94a2f8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_650c77969df137a567f8457a5a94a2f8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_650c77969df137a567f8457a5a94a2f8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        frame_650c77969df137a567f8457a5a94a2f8->m_frame.f_lineno = 323;
        tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_NotImplementedError );
        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 323;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_650c77969df137a567f8457a5a94a2f8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_650c77969df137a567f8457a5a94a2f8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_650c77969df137a567f8457a5a94a2f8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_650c77969df137a567f8457a5a94a2f8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_650c77969df137a567f8457a5a94a2f8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_650c77969df137a567f8457a5a94a2f8,
        type_description_1,
        par_self,
        par_context,
        par_message,
        par_plural_message,
        par_count
    );


    // Release cached frame.
    if ( frame_650c77969df137a567f8457a5a94a2f8 == cache_frame_650c77969df137a567f8457a5a94a2f8 )
    {
        Py_DECREF( frame_650c77969df137a567f8457a5a94a2f8 );
    }
    cache_frame_650c77969df137a567f8457a5a94a2f8 = NULL;

    assertFrameObject( frame_650c77969df137a567f8457a5a94a2f8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_10_pgettext );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_10_pgettext );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_tornado$locale$$$function_11_format_date( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_date = python_pars[ 1 ];
    PyObject *par_gmt_offset = python_pars[ 2 ];
    PyObject *par_relative = python_pars[ 3 ];
    PyObject *par_shorter = python_pars[ 4 ];
    PyObject *par_full_format = python_pars[ 5 ];
    PyObject *var_now = NULL;
    PyObject *var_local_date = NULL;
    PyObject *var_local_now = NULL;
    PyObject *var_local_yesterday = NULL;
    PyObject *var_difference = NULL;
    PyObject *var_seconds = NULL;
    PyObject *var_days = NULL;
    PyObject *var__ = NULL;
    PyObject *var_format = NULL;
    PyObject *var_minutes = NULL;
    PyObject *var_hours = NULL;
    nuitka_bool var_tfhour_clock = NUITKA_BOOL_UNASSIGNED;
    PyObject *var_str_time = NULL;
    struct Nuitka_FrameObject *frame_3fab27a27092cb371e8ae54e29adbcff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_3fab27a27092cb371e8ae54e29adbcff = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3fab27a27092cb371e8ae54e29adbcff, codeobj_3fab27a27092cb371e8ae54e29adbcff, module_tornado$locale, sizeof(nuitka_bool)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3fab27a27092cb371e8ae54e29adbcff = cache_frame_3fab27a27092cb371e8ae54e29adbcff;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3fab27a27092cb371e8ae54e29adbcff );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3fab27a27092cb371e8ae54e29adbcff ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_date );
        tmp_isinstance_inst_1 = par_date;
        tmp_isinstance_cls_1 = const_tuple_type_int_type_float_tuple;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 344;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 345;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_datetime );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_date );
            tmp_args_element_name_1 = par_date;
            frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 345;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_utcfromtimestamp, call_args );
            }

            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_date;
                assert( old != NULL );
                par_date = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 346;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_datetime );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 346;
        tmp_assign_source_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_utcnow );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var_now == NULL );
        var_now = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_date );
        tmp_compexpr_left_1 = par_date;
        CHECK_OBJECT( var_now );
        tmp_compexpr_right_1 = var_now;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 347;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            int tmp_truth_name_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( par_relative );
            tmp_truth_name_1 = CHECK_IF_TRUE( par_relative );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 348;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( par_date );
            tmp_left_name_1 = par_date;
            CHECK_OBJECT( var_now );
            tmp_right_name_1 = var_now;
            tmp_source_name_3 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 348;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_seconds );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 348;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_int_pos_60;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 348;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_3 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_3 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_3;
                CHECK_OBJECT( var_now );
                tmp_assign_source_3 = var_now;
                {
                    PyObject *old = par_date;
                    assert( old != NULL );
                    par_date = tmp_assign_source_3;
                    Py_INCREF( par_date );
                    Py_DECREF( old );
                }

            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_assign_source_4;
                tmp_assign_source_4 = Py_True;
                {
                    PyObject *old = par_full_format;
                    assert( old != NULL );
                    par_full_format = tmp_assign_source_4;
                    Py_INCREF( par_full_format );
                    Py_DECREF( old );
                }

            }
            branch_end_3:;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        if ( par_date == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 356;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_2 = par_date;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 356;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_3;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_timedelta );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 356;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_minutes;
        CHECK_OBJECT( par_gmt_offset );
        tmp_dict_value_1 = par_gmt_offset;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 356;
        tmp_right_name_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 356;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 356;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var_local_date == NULL );
        var_local_date = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        CHECK_OBJECT( var_now );
        tmp_left_name_3 = var_now;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_4;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_timedelta );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_2 = const_str_plain_minutes;
        CHECK_OBJECT( par_gmt_offset );
        tmp_dict_value_2 = par_gmt_offset;
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 357;
        tmp_right_name_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var_local_now == NULL );
        var_local_now = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_kw_name_3;
        CHECK_OBJECT( var_local_now );
        tmp_left_name_4 = var_local_now;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 358;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_5;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_timedelta );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_3 = PyDict_Copy( const_dict_18140f7b7a6bfded467b20acf0df2e9b );
        frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 358;
        tmp_right_name_4 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_4 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var_local_yesterday == NULL );
        var_local_yesterday = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_5;
        CHECK_OBJECT( var_now );
        tmp_left_name_5 = var_now;
        if ( par_date == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 359;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_5 = par_date;
        tmp_assign_source_8 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var_difference == NULL );
        var_difference = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( var_difference );
        tmp_source_name_7 = var_difference;
        tmp_assign_source_9 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_seconds );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var_seconds == NULL );
        var_seconds = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_source_name_8;
        CHECK_OBJECT( var_difference );
        tmp_source_name_8 = var_difference;
        tmp_assign_source_10 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_days );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var_days == NULL );
        var_days = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_assign_source_11 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_translate );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 363;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = Py_None;
        assert( var_format == NULL );
        Py_INCREF( tmp_assign_source_12 );
        var_format = tmp_assign_source_12;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_operand_name_1;
        if ( par_full_format == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "full_format" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 365;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_1 = par_full_format;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            nuitka_bool tmp_condition_result_5;
            int tmp_and_left_truth_2;
            nuitka_bool tmp_and_left_value_2;
            nuitka_bool tmp_and_right_value_2;
            int tmp_truth_name_2;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par_relative );
            tmp_truth_name_2 = CHECK_IF_TRUE( par_relative );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_2 == 1 )
            {
                goto and_right_2;
            }
            else
            {
                goto and_left_2;
            }
            and_right_2:;
            CHECK_OBJECT( var_days );
            tmp_compexpr_left_3 = var_days;
            tmp_compexpr_right_3 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_5 = tmp_and_right_value_2;
            goto and_end_2;
            and_left_2:;
            tmp_condition_result_5 = tmp_and_left_value_2;
            and_end_2:;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( var_seconds );
                tmp_compexpr_left_4 = var_seconds;
                tmp_compexpr_right_4 = const_int_pos_50;
                tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 367;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_left_name_6;
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_args_element_name_2;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_args_element_name_4;
                    PyObject *tmp_right_name_6;
                    PyObject *tmp_dict_key_3;
                    PyObject *tmp_dict_value_3;
                    CHECK_OBJECT( var__ );
                    tmp_called_name_4 = var__;
                    tmp_args_element_name_2 = const_str_digest_64acd9d2cb4096ad5db303736a6307e5;
                    tmp_args_element_name_3 = const_str_digest_d2d3529124bbf79c28bd4bb4788ed236;
                    CHECK_OBJECT( var_seconds );
                    tmp_args_element_name_4 = var_seconds;
                    frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 368;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                        tmp_left_name_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
                    }

                    if ( tmp_left_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 368;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    tmp_dict_key_3 = const_str_plain_seconds;
                    CHECK_OBJECT( var_seconds );
                    tmp_dict_value_3 = var_seconds;
                    tmp_right_name_6 = _PyDict_NewPresized( 1 );
                    tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_3, tmp_dict_value_3 );
                    assert( !(tmp_res != 0) );
                    tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                    Py_DECREF( tmp_left_name_6 );
                    Py_DECREF( tmp_right_name_6 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 368;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_6:;
            }
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_compexpr_left_5;
                PyObject *tmp_compexpr_right_5;
                CHECK_OBJECT( var_seconds );
                tmp_compexpr_left_5 = var_seconds;
                tmp_compexpr_right_5 = const_int_pos_3000;
                tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 372;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_args_element_name_5;
                    PyObject *tmp_left_name_7;
                    PyObject *tmp_right_name_7;
                    tmp_called_name_5 = LOOKUP_BUILTIN( const_str_plain_round );
                    assert( tmp_called_name_5 != NULL );
                    CHECK_OBJECT( var_seconds );
                    tmp_left_name_7 = var_seconds;
                    tmp_right_name_7 = const_float_60_0;
                    tmp_args_element_name_5 = BINARY_OPERATION_TRUEDIV_OBJECT_FLOAT( tmp_left_name_7, tmp_right_name_7 );
                    if ( tmp_args_element_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 373;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 373;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_5 };
                        tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_5 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 373;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_minutes == NULL );
                    var_minutes = tmp_assign_source_13;
                }
                {
                    PyObject *tmp_left_name_8;
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_right_name_8;
                    PyObject *tmp_dict_key_4;
                    PyObject *tmp_dict_value_4;
                    CHECK_OBJECT( var__ );
                    tmp_called_name_6 = var__;
                    tmp_args_element_name_6 = const_str_digest_d1adda4ceffa6c418846b7463c1c0bf3;
                    tmp_args_element_name_7 = const_str_digest_c1f66a265abaea28dae1f02b5da97e35;
                    CHECK_OBJECT( var_minutes );
                    tmp_args_element_name_8 = var_minutes;
                    frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 374;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
                        tmp_left_name_8 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, call_args );
                    }

                    if ( tmp_left_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 374;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    tmp_dict_key_4 = const_str_plain_minutes;
                    CHECK_OBJECT( var_minutes );
                    tmp_dict_value_4 = var_minutes;
                    tmp_right_name_8 = _PyDict_NewPresized( 1 );
                    tmp_res = PyDict_SetItem( tmp_right_name_8, tmp_dict_key_4, tmp_dict_value_4 );
                    assert( !(tmp_res != 0) );
                    tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_8, tmp_right_name_8 );
                    Py_DECREF( tmp_left_name_8 );
                    Py_DECREF( tmp_right_name_8 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 374;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_7:;
            }
            {
                PyObject *tmp_assign_source_14;
                PyObject *tmp_called_name_7;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_left_name_9;
                PyObject *tmp_right_name_9;
                tmp_called_name_7 = LOOKUP_BUILTIN( const_str_plain_round );
                assert( tmp_called_name_7 != NULL );
                CHECK_OBJECT( var_seconds );
                tmp_left_name_9 = var_seconds;
                tmp_right_name_9 = const_float_3600_0;
                tmp_args_element_name_9 = BINARY_OPERATION_TRUEDIV_OBJECT_FLOAT( tmp_left_name_9, tmp_right_name_9 );
                if ( tmp_args_element_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 378;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 378;
                {
                    PyObject *call_args[] = { tmp_args_element_name_9 };
                    tmp_assign_source_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
                }

                Py_DECREF( tmp_args_element_name_9 );
                if ( tmp_assign_source_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 378;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                assert( var_hours == NULL );
                var_hours = tmp_assign_source_14;
            }
            {
                PyObject *tmp_left_name_10;
                PyObject *tmp_called_name_8;
                PyObject *tmp_args_element_name_10;
                PyObject *tmp_args_element_name_11;
                PyObject *tmp_args_element_name_12;
                PyObject *tmp_right_name_10;
                PyObject *tmp_dict_key_5;
                PyObject *tmp_dict_value_5;
                CHECK_OBJECT( var__ );
                tmp_called_name_8 = var__;
                tmp_args_element_name_10 = const_str_digest_c9c85d91a6f9ef6ae7ba064fc7185109;
                tmp_args_element_name_11 = const_str_digest_b59844748413c35effa069f127d7f6e1;
                CHECK_OBJECT( var_hours );
                tmp_args_element_name_12 = var_hours;
                frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 379;
                {
                    PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12 };
                    tmp_left_name_10 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_8, call_args );
                }

                if ( tmp_left_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 379;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_dict_key_5 = const_str_plain_hours;
                CHECK_OBJECT( var_hours );
                tmp_dict_value_5 = var_hours;
                tmp_right_name_10 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_right_name_10, tmp_dict_key_5, tmp_dict_value_5 );
                assert( !(tmp_res != 0) );
                tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
                Py_DECREF( tmp_left_name_10 );
                Py_DECREF( tmp_right_name_10 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 379;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_no_5:;
        }
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( var_days );
            tmp_compexpr_left_6 = var_days;
            tmp_compexpr_right_6 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_15;
                PyObject *tmp_called_name_9;
                CHECK_OBJECT( var__ );
                tmp_called_name_9 = var__;
                frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 382;
                tmp_assign_source_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_digest_626f47fee6196287729a98b52426b191_tuple, 0 ) );

                if ( tmp_assign_source_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 382;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_format;
                    assert( old != NULL );
                    var_format = tmp_assign_source_15;
                    Py_DECREF( old );
                }

            }
            goto branch_end_8;
            branch_no_8:;
            {
                nuitka_bool tmp_condition_result_9;
                int tmp_and_left_truth_3;
                nuitka_bool tmp_and_left_value_3;
                nuitka_bool tmp_and_right_value_3;
                PyObject *tmp_compexpr_left_7;
                PyObject *tmp_compexpr_right_7;
                int tmp_and_left_truth_4;
                nuitka_bool tmp_and_left_value_4;
                nuitka_bool tmp_and_right_value_4;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                PyObject *tmp_source_name_10;
                PyObject *tmp_source_name_11;
                int tmp_truth_name_3;
                CHECK_OBJECT( var_days );
                tmp_compexpr_left_7 = var_days;
                tmp_compexpr_right_7 = const_int_pos_1;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 383;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_and_left_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
                if ( tmp_and_left_truth_3 == 1 )
                {
                    goto and_right_3;
                }
                else
                {
                    goto and_left_3;
                }
                and_right_3:;
                CHECK_OBJECT( var_local_date );
                tmp_source_name_10 = var_local_date;
                tmp_compexpr_left_8 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_day );
                if ( tmp_compexpr_left_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 383;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_local_yesterday );
                tmp_source_name_11 = var_local_yesterday;
                tmp_compexpr_right_8 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_day );
                if ( tmp_compexpr_right_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_8 );

                    exception_lineno = 383;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                Py_DECREF( tmp_compexpr_left_8 );
                Py_DECREF( tmp_compexpr_right_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 383;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_and_left_value_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_and_left_truth_4 = tmp_and_left_value_4 == NUITKA_BOOL_TRUE ? 1 : 0;
                if ( tmp_and_left_truth_4 == 1 )
                {
                    goto and_right_4;
                }
                else
                {
                    goto and_left_4;
                }
                and_right_4:;
                CHECK_OBJECT( par_relative );
                tmp_truth_name_3 = CHECK_IF_TRUE( par_relative );
                if ( tmp_truth_name_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 383;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_and_right_value_4 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_and_right_value_3 = tmp_and_right_value_4;
                goto and_end_4;
                and_left_4:;
                tmp_and_right_value_3 = tmp_and_left_value_4;
                and_end_4:;
                tmp_condition_result_9 = tmp_and_right_value_3;
                goto and_end_3;
                and_left_3:;
                tmp_condition_result_9 = tmp_and_left_value_3;
                and_end_3:;
                if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_9;
                }
                else
                {
                    goto branch_no_9;
                }
                branch_yes_9:;
                {
                    PyObject *tmp_assign_source_16;
                    nuitka_bool tmp_condition_result_10;
                    int tmp_truth_name_4;
                    PyObject *tmp_called_name_10;
                    PyObject *tmp_called_name_11;
                    CHECK_OBJECT( par_shorter );
                    tmp_truth_name_4 = CHECK_IF_TRUE( par_shorter );
                    if ( tmp_truth_name_4 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 384;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_10 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                    {
                        goto condexpr_true_1;
                    }
                    else
                    {
                        goto condexpr_false_1;
                    }
                    condexpr_true_1:;
                    CHECK_OBJECT( var__ );
                    tmp_called_name_10 = var__;
                    frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 384;
                    tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, &PyTuple_GET_ITEM( const_tuple_str_plain_yesterday_tuple, 0 ) );

                    if ( tmp_assign_source_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 384;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    goto condexpr_end_1;
                    condexpr_false_1:;
                    CHECK_OBJECT( var__ );
                    tmp_called_name_11 = var__;
                    frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 384;
                    tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_digest_b640953a68c487cbc89544f72b9babc3_tuple, 0 ) );

                    if ( tmp_assign_source_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 384;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    condexpr_end_1:;
                    {
                        PyObject *old = var_format;
                        assert( old != NULL );
                        var_format = tmp_assign_source_16;
                        Py_DECREF( old );
                    }

                }
                goto branch_end_9;
                branch_no_9:;
                {
                    nuitka_bool tmp_condition_result_11;
                    PyObject *tmp_compexpr_left_9;
                    PyObject *tmp_compexpr_right_9;
                    CHECK_OBJECT( var_days );
                    tmp_compexpr_left_9 = var_days;
                    tmp_compexpr_right_9 = const_int_pos_5;
                    tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 385;
                        type_description_1 = "ooooooooooooooooobo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_10;
                    }
                    else
                    {
                        goto branch_no_10;
                    }
                    branch_yes_10:;
                    {
                        PyObject *tmp_assign_source_17;
                        nuitka_bool tmp_condition_result_12;
                        int tmp_truth_name_5;
                        PyObject *tmp_called_name_12;
                        PyObject *tmp_called_name_13;
                        CHECK_OBJECT( par_shorter );
                        tmp_truth_name_5 = CHECK_IF_TRUE( par_shorter );
                        if ( tmp_truth_name_5 == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 386;
                            type_description_1 = "ooooooooooooooooobo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_12 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                        {
                            goto condexpr_true_2;
                        }
                        else
                        {
                            goto condexpr_false_2;
                        }
                        condexpr_true_2:;
                        CHECK_OBJECT( var__ );
                        tmp_called_name_12 = var__;
                        frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 386;
                        tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_str_digest_7871fba5268138f3163960862e12ec1d_tuple, 0 ) );

                        if ( tmp_assign_source_17 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 386;
                            type_description_1 = "ooooooooooooooooobo";
                            goto frame_exception_exit_1;
                        }
                        goto condexpr_end_2;
                        condexpr_false_2:;
                        CHECK_OBJECT( var__ );
                        tmp_called_name_13 = var__;
                        frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 386;
                        tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_digest_a007ef1d9765d8486eb48b306c7e3511_tuple, 0 ) );

                        if ( tmp_assign_source_17 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 386;
                            type_description_1 = "ooooooooooooooooobo";
                            goto frame_exception_exit_1;
                        }
                        condexpr_end_2:;
                        {
                            PyObject *old = var_format;
                            assert( old != NULL );
                            var_format = tmp_assign_source_17;
                            Py_DECREF( old );
                        }

                    }
                    goto branch_end_10;
                    branch_no_10:;
                    {
                        nuitka_bool tmp_condition_result_13;
                        PyObject *tmp_compexpr_left_10;
                        PyObject *tmp_compexpr_right_10;
                        CHECK_OBJECT( var_days );
                        tmp_compexpr_left_10 = var_days;
                        tmp_compexpr_right_10 = const_int_pos_334;
                        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 387;
                            type_description_1 = "ooooooooooooooooobo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_11;
                        }
                        else
                        {
                            goto branch_no_11;
                        }
                        branch_yes_11:;
                        {
                            PyObject *tmp_assign_source_18;
                            nuitka_bool tmp_condition_result_14;
                            int tmp_truth_name_6;
                            PyObject *tmp_called_name_14;
                            PyObject *tmp_called_name_15;
                            CHECK_OBJECT( par_shorter );
                            tmp_truth_name_6 = CHECK_IF_TRUE( par_shorter );
                            if ( tmp_truth_name_6 == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 390;
                                type_description_1 = "ooooooooooooooooobo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_14 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                            {
                                goto condexpr_true_3;
                            }
                            else
                            {
                                goto condexpr_false_3;
                            }
                            condexpr_true_3:;
                            CHECK_OBJECT( var__ );
                            tmp_called_name_14 = var__;
                            frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 389;
                            tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_str_digest_9d183a9f7777afb52500f6cd3f94fde8_tuple, 0 ) );

                            if ( tmp_assign_source_18 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 389;
                                type_description_1 = "ooooooooooooooooobo";
                                goto frame_exception_exit_1;
                            }
                            goto condexpr_end_3;
                            condexpr_false_3:;
                            CHECK_OBJECT( var__ );
                            tmp_called_name_15 = var__;
                            frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 391;
                            tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_digest_e89810b6cdcb59d43c1349f24947e94c_tuple, 0 ) );

                            if ( tmp_assign_source_18 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 391;
                                type_description_1 = "ooooooooooooooooobo";
                                goto frame_exception_exit_1;
                            }
                            condexpr_end_3:;
                            {
                                PyObject *old = var_format;
                                assert( old != NULL );
                                var_format = tmp_assign_source_18;
                                Py_DECREF( old );
                            }

                        }
                        branch_no_11:;
                    }
                    branch_end_10:;
                }
                branch_end_9:;
            }
            branch_end_8:;
        }
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_compexpr_left_11;
        PyObject *tmp_compexpr_right_11;
        if ( var_format == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "format" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 394;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_11 = var_format;
        tmp_compexpr_right_11 = Py_None;
        tmp_condition_result_15 = ( tmp_compexpr_left_11 == tmp_compexpr_right_11 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            PyObject *tmp_assign_source_19;
            nuitka_bool tmp_condition_result_16;
            int tmp_truth_name_7;
            PyObject *tmp_called_name_16;
            PyObject *tmp_called_name_17;
            CHECK_OBJECT( par_shorter );
            tmp_truth_name_7 = CHECK_IF_TRUE( par_shorter );
            if ( tmp_truth_name_7 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 397;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_16 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            CHECK_OBJECT( var__ );
            tmp_called_name_16 = var__;
            frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 396;
            tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_str_digest_3574e84f2a7e015bd543ca78e33acf99_tuple, 0 ) );

            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 396;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            goto condexpr_end_4;
            condexpr_false_4:;
            CHECK_OBJECT( var__ );
            tmp_called_name_17 = var__;
            frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 398;
            tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_digest_c96f4f5d520728e597b7aaa11664f9e1_tuple, 0 ) );

            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 398;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            condexpr_end_4:;
            {
                PyObject *old = var_format;
                var_format = tmp_assign_source_19;
                Py_XDECREF( old );
            }

        }
        branch_no_12:;
    }
    {
        nuitka_bool tmp_assign_source_20;
        PyObject *tmp_compexpr_left_12;
        PyObject *tmp_compexpr_right_12;
        PyObject *tmp_source_name_12;
        CHECK_OBJECT( par_self );
        tmp_source_name_12 = par_self;
        tmp_compexpr_left_12 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_code );
        if ( tmp_compexpr_left_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 401;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_12 = const_tuple_str_plain_en_str_plain_en_US_str_plain_zh_CN_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_12, tmp_compexpr_left_12 );
        Py_DECREF( tmp_compexpr_left_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 401;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        var_tfhour_clock = tmp_assign_source_20;
    }
    {
        nuitka_bool tmp_condition_result_17;
        assert( var_tfhour_clock != NUITKA_BOOL_UNASSIGNED);
        tmp_condition_result_17 = var_tfhour_clock;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_left_name_11;
            PyObject *tmp_right_name_11;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_13;
            PyObject *tmp_source_name_14;
            tmp_left_name_11 = const_str_digest_c16be5bdd7c0f2cffe66ad43f1ff6e1c;
            CHECK_OBJECT( var_local_date );
            tmp_source_name_13 = var_local_date;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_hour );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 403;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_11 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_right_name_11, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_local_date );
            tmp_source_name_14 = var_local_date;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_minute );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_right_name_11 );

                exception_lineno = 403;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_right_name_11, 1, tmp_tuple_element_1 );
            tmp_assign_source_21 = BINARY_OPERATION_REMAINDER( tmp_left_name_11, tmp_right_name_11 );
            Py_DECREF( tmp_right_name_11 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 403;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            assert( var_str_time == NULL );
            var_str_time = tmp_assign_source_21;
        }
        goto branch_end_13;
        branch_no_13:;
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_compexpr_left_13;
            PyObject *tmp_compexpr_right_13;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( par_self );
            tmp_source_name_15 = par_self;
            tmp_compexpr_left_13 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_code );
            if ( tmp_compexpr_left_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 404;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_13 = const_str_plain_zh_CN;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
            Py_DECREF( tmp_compexpr_left_13 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 404;
                type_description_1 = "ooooooooooooooooobo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_14;
            }
            else
            {
                goto branch_no_14;
            }
            branch_yes_14:;
            {
                PyObject *tmp_assign_source_22;
                PyObject *tmp_left_name_12;
                PyObject *tmp_right_name_12;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                PyObject *tmp_compexpr_left_14;
                PyObject *tmp_compexpr_right_14;
                PyObject *tmp_source_name_16;
                int tmp_or_left_truth_1;
                PyObject *tmp_or_left_value_1;
                PyObject *tmp_or_right_value_1;
                PyObject *tmp_left_name_13;
                PyObject *tmp_source_name_17;
                PyObject *tmp_right_name_13;
                PyObject *tmp_source_name_18;
                tmp_left_name_12 = const_str_digest_1f98531c7ff1755fa56a767f09a0a7a0;
                tmp_subscribed_name_1 = const_tuple_4098b9f363c2cd529fe597c0aaf71d10_tuple;
                CHECK_OBJECT( var_local_date );
                tmp_source_name_16 = var_local_date;
                tmp_compexpr_left_14 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_hour );
                if ( tmp_compexpr_left_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 406;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_14 = const_int_pos_12;
                tmp_subscript_name_1 = RICH_COMPARE_GTE_OBJECT_OBJECT( tmp_compexpr_left_14, tmp_compexpr_right_14 );
                Py_DECREF( tmp_compexpr_left_14 );
                if ( tmp_subscript_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 406;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_tuple_element_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                Py_DECREF( tmp_subscript_name_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 406;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_12 = PyTuple_New( 3 );
                PyTuple_SET_ITEM( tmp_right_name_12, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( var_local_date );
                tmp_source_name_17 = var_local_date;
                tmp_left_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_hour );
                if ( tmp_left_name_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_12 );

                    exception_lineno = 407;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_13 = const_int_pos_12;
                tmp_or_left_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_13, tmp_right_name_13 );
                Py_DECREF( tmp_left_name_13 );
                if ( tmp_or_left_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_12 );

                    exception_lineno = 407;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
                if ( tmp_or_left_truth_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_12 );
                    Py_DECREF( tmp_or_left_value_1 );

                    exception_lineno = 407;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                if ( tmp_or_left_truth_1 == 1 )
                {
                    goto or_left_1;
                }
                else
                {
                    goto or_right_1;
                }
                or_right_1:;
                Py_DECREF( tmp_or_left_value_1 );
                tmp_or_right_value_1 = const_int_pos_12;
                Py_INCREF( tmp_or_right_value_1 );
                tmp_tuple_element_2 = tmp_or_right_value_1;
                goto or_end_1;
                or_left_1:;
                tmp_tuple_element_2 = tmp_or_left_value_1;
                or_end_1:;
                PyTuple_SET_ITEM( tmp_right_name_12, 1, tmp_tuple_element_2 );
                CHECK_OBJECT( var_local_date );
                tmp_source_name_18 = var_local_date;
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_minute );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_12 );

                    exception_lineno = 408;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_12, 2, tmp_tuple_element_2 );
                tmp_assign_source_22 = BINARY_OPERATION_REMAINDER( tmp_left_name_12, tmp_right_name_12 );
                Py_DECREF( tmp_right_name_12 );
                if ( tmp_assign_source_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 405;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                assert( var_str_time == NULL );
                var_str_time = tmp_assign_source_22;
            }
            goto branch_end_14;
            branch_no_14:;
            {
                PyObject *tmp_assign_source_23;
                PyObject *tmp_left_name_14;
                PyObject *tmp_right_name_14;
                PyObject *tmp_tuple_element_3;
                int tmp_or_left_truth_2;
                PyObject *tmp_or_left_value_2;
                PyObject *tmp_or_right_value_2;
                PyObject *tmp_left_name_15;
                PyObject *tmp_source_name_19;
                PyObject *tmp_right_name_15;
                PyObject *tmp_source_name_20;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_compexpr_left_15;
                PyObject *tmp_compexpr_right_15;
                PyObject *tmp_source_name_21;
                tmp_left_name_14 = const_str_digest_695283f572d9bc5821fb828221e07f56;
                CHECK_OBJECT( var_local_date );
                tmp_source_name_19 = var_local_date;
                tmp_left_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_hour );
                if ( tmp_left_name_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 412;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_15 = const_int_pos_12;
                tmp_or_left_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_15, tmp_right_name_15 );
                Py_DECREF( tmp_left_name_15 );
                if ( tmp_or_left_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 412;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
                if ( tmp_or_left_truth_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_or_left_value_2 );

                    exception_lineno = 412;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                if ( tmp_or_left_truth_2 == 1 )
                {
                    goto or_left_2;
                }
                else
                {
                    goto or_right_2;
                }
                or_right_2:;
                Py_DECREF( tmp_or_left_value_2 );
                tmp_or_right_value_2 = const_int_pos_12;
                Py_INCREF( tmp_or_right_value_2 );
                tmp_tuple_element_3 = tmp_or_right_value_2;
                goto or_end_2;
                or_left_2:;
                tmp_tuple_element_3 = tmp_or_left_value_2;
                or_end_2:;
                tmp_right_name_14 = PyTuple_New( 3 );
                PyTuple_SET_ITEM( tmp_right_name_14, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( var_local_date );
                tmp_source_name_20 = var_local_date;
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_minute );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_14 );

                    exception_lineno = 413;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_14, 1, tmp_tuple_element_3 );
                tmp_subscribed_name_2 = const_tuple_str_plain_am_str_plain_pm_tuple;
                CHECK_OBJECT( var_local_date );
                tmp_source_name_21 = var_local_date;
                tmp_compexpr_left_15 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_hour );
                if ( tmp_compexpr_left_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_14 );

                    exception_lineno = 414;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_15 = const_int_pos_12;
                tmp_subscript_name_2 = RICH_COMPARE_GTE_OBJECT_OBJECT( tmp_compexpr_left_15, tmp_compexpr_right_15 );
                Py_DECREF( tmp_compexpr_left_15 );
                if ( tmp_subscript_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_14 );

                    exception_lineno = 414;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                tmp_tuple_element_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                Py_DECREF( tmp_subscript_name_2 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_14 );

                    exception_lineno = 414;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_14, 2, tmp_tuple_element_3 );
                tmp_assign_source_23 = BINARY_OPERATION_REMAINDER( tmp_left_name_14, tmp_right_name_14 );
                Py_DECREF( tmp_right_name_14 );
                if ( tmp_assign_source_23 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 411;
                    type_description_1 = "ooooooooooooooooobo";
                    goto frame_exception_exit_1;
                }
                assert( var_str_time == NULL );
                var_str_time = tmp_assign_source_23;
            }
            branch_end_14:;
        }
        branch_end_13:;
    }
    {
        PyObject *tmp_left_name_16;
        PyObject *tmp_right_name_16;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_source_name_22;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_left_name_17;
        PyObject *tmp_source_name_23;
        PyObject *tmp_right_name_17;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_source_name_24;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_unicode_arg_1;
        PyObject *tmp_source_name_25;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_unicode_arg_2;
        PyObject *tmp_source_name_26;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        if ( var_format == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "format" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 417;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_16 = var_format;
        tmp_dict_key_6 = const_str_plain_month_name;
        CHECK_OBJECT( par_self );
        tmp_source_name_22 = par_self;
        tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain__months );
        if ( tmp_subscribed_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 418;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_local_date );
        tmp_source_name_23 = var_local_date;
        tmp_left_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_month );
        if ( tmp_left_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscribed_name_3 );

            exception_lineno = 418;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_17 = const_int_pos_1;
        tmp_subscript_name_3 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_17, tmp_right_name_17 );
        Py_DECREF( tmp_left_name_17 );
        if ( tmp_subscript_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscribed_name_3 );

            exception_lineno = 418;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        Py_DECREF( tmp_subscribed_name_3 );
        Py_DECREF( tmp_subscript_name_3 );
        if ( tmp_dict_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 418;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_16 = _PyDict_NewPresized( 5 );
        tmp_res = PyDict_SetItem( tmp_right_name_16, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_weekday;
        CHECK_OBJECT( par_self );
        tmp_source_name_24 = par_self;
        tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain__weekdays );
        if ( tmp_subscribed_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_16 );

            exception_lineno = 419;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_local_date );
        tmp_called_instance_3 = var_local_date;
        frame_3fab27a27092cb371e8ae54e29adbcff->m_frame.f_lineno = 419;
        tmp_subscript_name_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_weekday );
        if ( tmp_subscript_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_16 );
            Py_DECREF( tmp_subscribed_name_4 );

            exception_lineno = 419;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
        Py_DECREF( tmp_subscribed_name_4 );
        Py_DECREF( tmp_subscript_name_4 );
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_16 );

            exception_lineno = 419;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_right_name_16, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_day;
        CHECK_OBJECT( var_local_date );
        tmp_source_name_25 = var_local_date;
        tmp_unicode_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_day );
        if ( tmp_unicode_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_16 );

            exception_lineno = 420;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_8 = PyObject_Unicode( tmp_unicode_arg_1 );
        Py_DECREF( tmp_unicode_arg_1 );
        if ( tmp_dict_value_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_16 );

            exception_lineno = 420;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_right_name_16, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_year;
        CHECK_OBJECT( var_local_date );
        tmp_source_name_26 = var_local_date;
        tmp_unicode_arg_2 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_year );
        if ( tmp_unicode_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_16 );

            exception_lineno = 421;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_9 = PyObject_Unicode( tmp_unicode_arg_2 );
        Py_DECREF( tmp_unicode_arg_2 );
        if ( tmp_dict_value_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_16 );

            exception_lineno = 421;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_right_name_16, tmp_dict_key_9, tmp_dict_value_9 );
        Py_DECREF( tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_time;
        if ( var_str_time == NULL )
        {
            Py_DECREF( tmp_right_name_16 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "str_time" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 422;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_10 = var_str_time;
        tmp_res = PyDict_SetItem( tmp_right_name_16, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_16, tmp_right_name_16 );
        Py_DECREF( tmp_right_name_16 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 417;
            type_description_1 = "ooooooooooooooooobo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3fab27a27092cb371e8ae54e29adbcff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3fab27a27092cb371e8ae54e29adbcff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3fab27a27092cb371e8ae54e29adbcff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3fab27a27092cb371e8ae54e29adbcff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3fab27a27092cb371e8ae54e29adbcff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3fab27a27092cb371e8ae54e29adbcff, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3fab27a27092cb371e8ae54e29adbcff,
        type_description_1,
        par_self,
        par_date,
        par_gmt_offset,
        par_relative,
        par_shorter,
        par_full_format,
        var_now,
        var_local_date,
        var_local_now,
        var_local_yesterday,
        var_difference,
        var_seconds,
        var_days,
        var__,
        var_format,
        var_minutes,
        var_hours,
        (int)var_tfhour_clock,
        var_str_time
    );


    // Release cached frame.
    if ( frame_3fab27a27092cb371e8ae54e29adbcff == cache_frame_3fab27a27092cb371e8ae54e29adbcff )
    {
        Py_DECREF( frame_3fab27a27092cb371e8ae54e29adbcff );
    }
    cache_frame_3fab27a27092cb371e8ae54e29adbcff = NULL;

    assertFrameObject( frame_3fab27a27092cb371e8ae54e29adbcff );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_11_format_date );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_date );
    par_date = NULL;

    CHECK_OBJECT( (PyObject *)par_gmt_offset );
    Py_DECREF( par_gmt_offset );
    par_gmt_offset = NULL;

    CHECK_OBJECT( (PyObject *)par_relative );
    Py_DECREF( par_relative );
    par_relative = NULL;

    CHECK_OBJECT( (PyObject *)par_shorter );
    Py_DECREF( par_shorter );
    par_shorter = NULL;

    Py_XDECREF( par_full_format );
    par_full_format = NULL;

    CHECK_OBJECT( (PyObject *)var_now );
    Py_DECREF( var_now );
    var_now = NULL;

    CHECK_OBJECT( (PyObject *)var_local_date );
    Py_DECREF( var_local_date );
    var_local_date = NULL;

    CHECK_OBJECT( (PyObject *)var_local_now );
    Py_DECREF( var_local_now );
    var_local_now = NULL;

    CHECK_OBJECT( (PyObject *)var_local_yesterday );
    Py_DECREF( var_local_yesterday );
    var_local_yesterday = NULL;

    CHECK_OBJECT( (PyObject *)var_difference );
    Py_DECREF( var_difference );
    var_difference = NULL;

    CHECK_OBJECT( (PyObject *)var_seconds );
    Py_DECREF( var_seconds );
    var_seconds = NULL;

    CHECK_OBJECT( (PyObject *)var_days );
    Py_DECREF( var_days );
    var_days = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_format );
    var_format = NULL;

    Py_XDECREF( var_minutes );
    var_minutes = NULL;

    Py_XDECREF( var_hours );
    var_hours = NULL;

    Py_XDECREF( var_str_time );
    var_str_time = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_date );
    par_date = NULL;

    CHECK_OBJECT( (PyObject *)par_gmt_offset );
    Py_DECREF( par_gmt_offset );
    par_gmt_offset = NULL;

    CHECK_OBJECT( (PyObject *)par_relative );
    Py_DECREF( par_relative );
    par_relative = NULL;

    CHECK_OBJECT( (PyObject *)par_shorter );
    Py_DECREF( par_shorter );
    par_shorter = NULL;

    Py_XDECREF( par_full_format );
    par_full_format = NULL;

    Py_XDECREF( var_now );
    var_now = NULL;

    Py_XDECREF( var_local_date );
    var_local_date = NULL;

    Py_XDECREF( var_local_now );
    var_local_now = NULL;

    Py_XDECREF( var_local_yesterday );
    var_local_yesterday = NULL;

    Py_XDECREF( var_difference );
    var_difference = NULL;

    Py_XDECREF( var_seconds );
    var_seconds = NULL;

    Py_XDECREF( var_days );
    var_days = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_format );
    var_format = NULL;

    Py_XDECREF( var_minutes );
    var_minutes = NULL;

    Py_XDECREF( var_hours );
    var_hours = NULL;

    Py_XDECREF( var_str_time );
    var_str_time = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_11_format_date );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_12_format_day( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_date = python_pars[ 1 ];
    PyObject *par_gmt_offset = python_pars[ 2 ];
    PyObject *par_dow = python_pars[ 3 ];
    PyObject *var_local_date = NULL;
    PyObject *var__ = NULL;
    struct Nuitka_FrameObject *frame_c018dbe84ba4c914ac5efad120c78c2a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_c018dbe84ba4c914ac5efad120c78c2a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c018dbe84ba4c914ac5efad120c78c2a, codeobj_c018dbe84ba4c914ac5efad120c78c2a, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c018dbe84ba4c914ac5efad120c78c2a = cache_frame_c018dbe84ba4c914ac5efad120c78c2a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c018dbe84ba4c914ac5efad120c78c2a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c018dbe84ba4c914ac5efad120c78c2a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        CHECK_OBJECT( par_date );
        tmp_left_name_1 = par_date;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 433;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_timedelta );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 433;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_minutes;
        CHECK_OBJECT( par_gmt_offset );
        tmp_dict_value_1 = par_gmt_offset;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_c018dbe84ba4c914ac5efad120c78c2a->m_frame.f_lineno = 433;
        tmp_right_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 433;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 433;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_local_date == NULL );
        var_local_date = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_translate );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 434;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_dow );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_dow );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 435;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_left_name_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_left_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_right_name_3;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            PyObject *tmp_unicode_arg_1;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( var__ );
            tmp_called_name_2 = var__;
            frame_c018dbe84ba4c914ac5efad120c78c2a->m_frame.f_lineno = 436;
            tmp_left_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_digest_93b20dc5acd0f4060cb8bd8950d9b8cf_tuple, 0 ) );

            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 436;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_2 = const_str_plain_month_name;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__months );
            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 437;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_local_date );
            tmp_source_name_4 = var_local_date;
            tmp_left_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_month );
            if ( tmp_left_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );
                Py_DECREF( tmp_subscribed_name_1 );

                exception_lineno = 437;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_3 = const_int_pos_1;
            tmp_subscript_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_left_name_3 );
            if ( tmp_subscript_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );
                Py_DECREF( tmp_subscribed_name_1 );

                exception_lineno = 437;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscribed_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 437;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_right_name_2, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_3 = const_str_plain_weekday;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__weekdays );
            if ( tmp_subscribed_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );
                Py_DECREF( tmp_right_name_2 );

                exception_lineno = 438;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_local_date );
            tmp_called_instance_1 = var_local_date;
            frame_c018dbe84ba4c914ac5efad120c78c2a->m_frame.f_lineno = 438;
            tmp_subscript_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_weekday );
            if ( tmp_subscript_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );
                Py_DECREF( tmp_right_name_2 );
                Py_DECREF( tmp_subscribed_name_2 );

                exception_lineno = 438;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscribed_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_dict_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );
                Py_DECREF( tmp_right_name_2 );

                exception_lineno = 438;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = PyDict_SetItem( tmp_right_name_2, tmp_dict_key_3, tmp_dict_value_3 );
            Py_DECREF( tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_4 = const_str_plain_day;
            CHECK_OBJECT( var_local_date );
            tmp_source_name_6 = var_local_date;
            tmp_unicode_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_day );
            if ( tmp_unicode_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );
                Py_DECREF( tmp_right_name_2 );

                exception_lineno = 439;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_4 = PyObject_Unicode( tmp_unicode_arg_1 );
            Py_DECREF( tmp_unicode_arg_1 );
            if ( tmp_dict_value_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );
                Py_DECREF( tmp_right_name_2 );

                exception_lineno = 439;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = PyDict_SetItem( tmp_right_name_2, tmp_dict_key_4, tmp_dict_value_4 );
            Py_DECREF( tmp_dict_value_4 );
            assert( !(tmp_res != 0) );
            tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 436;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_left_name_4;
            PyObject *tmp_called_name_3;
            PyObject *tmp_right_name_4;
            PyObject *tmp_dict_key_5;
            PyObject *tmp_dict_value_5;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_left_name_5;
            PyObject *tmp_source_name_8;
            PyObject *tmp_right_name_5;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            PyObject *tmp_unicode_arg_2;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( var__ );
            tmp_called_name_3 = var__;
            frame_c018dbe84ba4c914ac5efad120c78c2a->m_frame.f_lineno = 442;
            tmp_left_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_digest_9d183a9f7777afb52500f6cd3f94fde8_tuple, 0 ) );

            if ( tmp_left_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 442;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_5 = const_str_plain_month_name;
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__months );
            if ( tmp_subscribed_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_4 );

                exception_lineno = 443;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_local_date );
            tmp_source_name_8 = var_local_date;
            tmp_left_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_month );
            if ( tmp_left_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_4 );
                Py_DECREF( tmp_subscribed_name_3 );

                exception_lineno = 443;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_5 = const_int_pos_1;
            tmp_subscript_name_3 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_5, tmp_right_name_5 );
            Py_DECREF( tmp_left_name_5 );
            if ( tmp_subscript_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_4 );
                Py_DECREF( tmp_subscribed_name_3 );

                exception_lineno = 443;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_3 );
            Py_DECREF( tmp_subscript_name_3 );
            if ( tmp_dict_value_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_4 );

                exception_lineno = 443;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_4 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_right_name_4, tmp_dict_key_5, tmp_dict_value_5 );
            Py_DECREF( tmp_dict_value_5 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_6 = const_str_plain_day;
            CHECK_OBJECT( var_local_date );
            tmp_source_name_9 = var_local_date;
            tmp_unicode_arg_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_day );
            if ( tmp_unicode_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_4 );
                Py_DECREF( tmp_right_name_4 );

                exception_lineno = 444;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_6 = PyObject_Unicode( tmp_unicode_arg_2 );
            Py_DECREF( tmp_unicode_arg_2 );
            if ( tmp_dict_value_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_4 );
                Py_DECREF( tmp_right_name_4 );

                exception_lineno = 444;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = PyDict_SetItem( tmp_right_name_4, tmp_dict_key_6, tmp_dict_value_6 );
            Py_DECREF( tmp_dict_value_6 );
            assert( !(tmp_res != 0) );
            tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
            Py_DECREF( tmp_left_name_4 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 442;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c018dbe84ba4c914ac5efad120c78c2a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c018dbe84ba4c914ac5efad120c78c2a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c018dbe84ba4c914ac5efad120c78c2a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c018dbe84ba4c914ac5efad120c78c2a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c018dbe84ba4c914ac5efad120c78c2a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c018dbe84ba4c914ac5efad120c78c2a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c018dbe84ba4c914ac5efad120c78c2a,
        type_description_1,
        par_self,
        par_date,
        par_gmt_offset,
        par_dow,
        var_local_date,
        var__
    );


    // Release cached frame.
    if ( frame_c018dbe84ba4c914ac5efad120c78c2a == cache_frame_c018dbe84ba4c914ac5efad120c78c2a )
    {
        Py_DECREF( frame_c018dbe84ba4c914ac5efad120c78c2a );
    }
    cache_frame_c018dbe84ba4c914ac5efad120c78c2a = NULL;

    assertFrameObject( frame_c018dbe84ba4c914ac5efad120c78c2a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_12_format_day );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_date );
    Py_DECREF( par_date );
    par_date = NULL;

    CHECK_OBJECT( (PyObject *)par_gmt_offset );
    Py_DECREF( par_gmt_offset );
    par_gmt_offset = NULL;

    CHECK_OBJECT( (PyObject *)par_dow );
    Py_DECREF( par_dow );
    par_dow = NULL;

    CHECK_OBJECT( (PyObject *)var_local_date );
    Py_DECREF( var_local_date );
    var_local_date = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_date );
    Py_DECREF( par_date );
    par_date = NULL;

    CHECK_OBJECT( (PyObject *)par_gmt_offset );
    Py_DECREF( par_gmt_offset );
    par_gmt_offset = NULL;

    CHECK_OBJECT( (PyObject *)par_dow );
    Py_DECREF( par_dow );
    par_dow = NULL;

    Py_XDECREF( var_local_date );
    var_local_date = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_12_format_day );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_13_list( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_parts = python_pars[ 1 ];
    PyObject *var__ = NULL;
    PyObject *var_comma = NULL;
    struct Nuitka_FrameObject *frame_764b37dccc915f0bcd96d4b5355bab86;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_764b37dccc915f0bcd96d4b5355bab86 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_764b37dccc915f0bcd96d4b5355bab86, codeobj_764b37dccc915f0bcd96d4b5355bab86, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_764b37dccc915f0bcd96d4b5355bab86 = cache_frame_764b37dccc915f0bcd96d4b5355bab86;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_764b37dccc915f0bcd96d4b5355bab86 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_764b37dccc915f0bcd96d4b5355bab86 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_translate );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 453;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_parts );
        tmp_len_arg_1 = par_parts;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 454;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_str_empty;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_2;
        CHECK_OBJECT( par_parts );
        tmp_len_arg_2 = par_parts;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 456;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_parts );
            tmp_subscribed_name_1 = par_parts;
            tmp_subscript_name_1 = const_int_0;
            tmp_return_value = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 457;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_2;
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_code );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 458;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_764b37dccc915f0bcd96d4b5355bab86->m_frame.f_lineno = 458;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_fa_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 458;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 458;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_assign_source_2 = const_str_digest_3505f180c48d7881a1fcd516b48114ef;
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_assign_source_2 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
        condexpr_end_1:;
        assert( var_comma == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_comma = tmp_assign_source_2;
    }
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_left_name_2;
        PyObject *tmp_len_arg_3;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( var__ );
        tmp_called_name_1 = var__;
        frame_764b37dccc915f0bcd96d4b5355bab86->m_frame.f_lineno = 459;
        tmp_left_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_str_digest_eb0f9b85ed1cef1f2122150e15335d0c_tuple, 0 ) );

        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 459;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_commas;
        CHECK_OBJECT( var_comma );
        tmp_source_name_3 = var_comma;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_join );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 460;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_parts );
        tmp_subscribed_name_2 = par_parts;
        tmp_subscript_name_2 = const_slice_none_int_neg_1_none;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 460;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_764b37dccc915f0bcd96d4b5355bab86->m_frame.f_lineno = 460;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 460;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_right_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_last;
        CHECK_OBJECT( par_parts );
        tmp_subscribed_name_3 = par_parts;
        CHECK_OBJECT( par_parts );
        tmp_len_arg_3 = par_parts;
        tmp_left_name_2 = BUILTIN_LEN( tmp_len_arg_3 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 461;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_subscript_name_3 = BINARY_OPERATION_SUB_LONG_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        assert( !(tmp_subscript_name_3 == NULL) );
        tmp_dict_value_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        Py_DECREF( tmp_subscript_name_3 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 461;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_right_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 459;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_764b37dccc915f0bcd96d4b5355bab86 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_764b37dccc915f0bcd96d4b5355bab86 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_764b37dccc915f0bcd96d4b5355bab86 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_764b37dccc915f0bcd96d4b5355bab86, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_764b37dccc915f0bcd96d4b5355bab86->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_764b37dccc915f0bcd96d4b5355bab86, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_764b37dccc915f0bcd96d4b5355bab86,
        type_description_1,
        par_self,
        par_parts,
        var__,
        var_comma
    );


    // Release cached frame.
    if ( frame_764b37dccc915f0bcd96d4b5355bab86 == cache_frame_764b37dccc915f0bcd96d4b5355bab86 )
    {
        Py_DECREF( frame_764b37dccc915f0bcd96d4b5355bab86 );
    }
    cache_frame_764b37dccc915f0bcd96d4b5355bab86 = NULL;

    assertFrameObject( frame_764b37dccc915f0bcd96d4b5355bab86 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_13_list );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_parts );
    Py_DECREF( par_parts );
    par_parts = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_comma );
    var_comma = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_parts );
    Py_DECREF( par_parts );
    par_parts = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_comma );
    var_comma = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_13_list );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_14_friendly_number( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *var_s = NULL;
    PyObject *var_parts = NULL;
    struct Nuitka_FrameObject *frame_f153662e8ed47c2118619d52e0b7d53b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f153662e8ed47c2118619d52e0b7d53b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f153662e8ed47c2118619d52e0b7d53b, codeobj_f153662e8ed47c2118619d52e0b7d53b, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f153662e8ed47c2118619d52e0b7d53b = cache_frame_f153662e8ed47c2118619d52e0b7d53b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f153662e8ed47c2118619d52e0b7d53b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f153662e8ed47c2118619d52e0b7d53b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_code );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 466;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_tuple_str_plain_en_str_plain_en_US_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 466;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_unicode_arg_1;
            CHECK_OBJECT( par_value );
            tmp_unicode_arg_1 = par_value;
            tmp_return_value = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 467;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_unicode_arg_2;
        CHECK_OBJECT( par_value );
        tmp_unicode_arg_2 = par_value;
        tmp_assign_source_1 = PyObject_Unicode( tmp_unicode_arg_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 468;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_s == NULL );
        var_s = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( var_parts == NULL );
        var_parts = tmp_assign_source_2;
    }
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_s );
        tmp_operand_name_1 = var_s;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 470;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        goto loop_end_1;
        branch_no_2:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( var_parts );
        tmp_source_name_2 = var_parts;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 471;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_s );
        tmp_subscribed_name_1 = var_s;
        tmp_subscript_name_1 = const_slice_int_neg_3_none_none;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 471;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_f153662e8ed47c2118619d52e0b7d53b->m_frame.f_lineno = 471;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 471;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( var_s );
        tmp_subscribed_name_2 = var_s;
        tmp_subscript_name_2 = const_slice_none_int_neg_3_none;
        tmp_assign_source_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 472;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_s;
            assert( old != NULL );
            var_s = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 470;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_3;
        tmp_source_name_3 = const_str_chr_44;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_join );
        assert( !(tmp_called_name_2 == NULL) );
        tmp_called_name_3 = (PyObject *)&PyReversed_Type;
        CHECK_OBJECT( var_parts );
        tmp_args_element_name_3 = var_parts;
        frame_f153662e8ed47c2118619d52e0b7d53b->m_frame.f_lineno = 473;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 473;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_f153662e8ed47c2118619d52e0b7d53b->m_frame.f_lineno = 473;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 473;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f153662e8ed47c2118619d52e0b7d53b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f153662e8ed47c2118619d52e0b7d53b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f153662e8ed47c2118619d52e0b7d53b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f153662e8ed47c2118619d52e0b7d53b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f153662e8ed47c2118619d52e0b7d53b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f153662e8ed47c2118619d52e0b7d53b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f153662e8ed47c2118619d52e0b7d53b,
        type_description_1,
        par_self,
        par_value,
        var_s,
        var_parts
    );


    // Release cached frame.
    if ( frame_f153662e8ed47c2118619d52e0b7d53b == cache_frame_f153662e8ed47c2118619d52e0b7d53b )
    {
        Py_DECREF( frame_f153662e8ed47c2118619d52e0b7d53b );
    }
    cache_frame_f153662e8ed47c2118619d52e0b7d53b = NULL;

    assertFrameObject( frame_f153662e8ed47c2118619d52e0b7d53b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_14_friendly_number );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_14_friendly_number );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_15___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_code = python_pars[ 1 ];
    PyObject *par_translations = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_a842a0780f04fb77527ad613fa687a53;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a842a0780f04fb77527ad613fa687a53 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a842a0780f04fb77527ad613fa687a53, codeobj_a842a0780f04fb77527ad613fa687a53, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a842a0780f04fb77527ad613fa687a53 = cache_frame_a842a0780f04fb77527ad613fa687a53;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a842a0780f04fb77527ad613fa687a53 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a842a0780f04fb77527ad613fa687a53 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_translations );
        tmp_assattr_name_1 = par_translations;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_translations, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 480;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CSVLocale );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CSVLocale );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CSVLocale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 481;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 481;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_code );
        tmp_args_element_name_1 = par_code;
        frame_a842a0780f04fb77527ad613fa687a53->m_frame.f_lineno = 481;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain___init__, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 481;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a842a0780f04fb77527ad613fa687a53 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a842a0780f04fb77527ad613fa687a53 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a842a0780f04fb77527ad613fa687a53, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a842a0780f04fb77527ad613fa687a53->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a842a0780f04fb77527ad613fa687a53, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a842a0780f04fb77527ad613fa687a53,
        type_description_1,
        par_self,
        par_code,
        par_translations,
        NULL
    );


    // Release cached frame.
    if ( frame_a842a0780f04fb77527ad613fa687a53 == cache_frame_a842a0780f04fb77527ad613fa687a53 )
    {
        Py_DECREF( frame_a842a0780f04fb77527ad613fa687a53 );
    }
    cache_frame_a842a0780f04fb77527ad613fa687a53 = NULL;

    assertFrameObject( frame_a842a0780f04fb77527ad613fa687a53 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_15___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_translations );
    Py_DECREF( par_translations );
    par_translations = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_translations );
    Py_DECREF( par_translations );
    par_translations = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_15___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_16_translate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_message = python_pars[ 1 ];
    PyObject *par_plural_message = python_pars[ 2 ];
    PyObject *par_count = python_pars[ 3 ];
    PyObject *var_message_dict = NULL;
    struct Nuitka_FrameObject *frame_6b09f54ae5c9f31517b15e2480fcf05f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_6b09f54ae5c9f31517b15e2480fcf05f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6b09f54ae5c9f31517b15e2480fcf05f, codeobj_6b09f54ae5c9f31517b15e2480fcf05f, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6b09f54ae5c9f31517b15e2480fcf05f = cache_frame_6b09f54ae5c9f31517b15e2480fcf05f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6b09f54ae5c9f31517b15e2480fcf05f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6b09f54ae5c9f31517b15e2480fcf05f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_plural_message );
        tmp_compexpr_left_1 = par_plural_message;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_count );
            tmp_compexpr_left_2 = par_count;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                tmp_raise_type_1 = PyExc_AssertionError;
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_lineno = 487;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par_count );
            tmp_compexpr_left_3 = par_count;
            tmp_compexpr_right_3 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 488;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_1;
                CHECK_OBJECT( par_plural_message );
                tmp_assign_source_1 = par_plural_message;
                {
                    PyObject *old = par_message;
                    assert( old != NULL );
                    par_message = tmp_assign_source_1;
                    Py_INCREF( par_message );
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_source_name_1;
                PyObject *tmp_call_arg_element_1;
                PyObject *tmp_call_arg_element_2;
                CHECK_OBJECT( par_self );
                tmp_source_name_1 = par_self;
                tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_translations );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 490;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_call_arg_element_1 = const_str_plain_plural;
                tmp_call_arg_element_2 = PyDict_New();
                frame_6b09f54ae5c9f31517b15e2480fcf05f->m_frame.f_lineno = 490;
                {
                    PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
                    tmp_assign_source_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
                }

                Py_DECREF( tmp_called_instance_1 );
                Py_DECREF( tmp_call_arg_element_2 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 490;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_message_dict == NULL );
                var_message_dict = tmp_assign_source_2;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_source_name_2;
                PyObject *tmp_call_arg_element_3;
                PyObject *tmp_call_arg_element_4;
                CHECK_OBJECT( par_self );
                tmp_source_name_2 = par_self;
                tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_translations );
                if ( tmp_called_instance_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 492;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_call_arg_element_3 = const_str_plain_singular;
                tmp_call_arg_element_4 = PyDict_New();
                frame_6b09f54ae5c9f31517b15e2480fcf05f->m_frame.f_lineno = 492;
                {
                    PyObject *call_args[] = { tmp_call_arg_element_3, tmp_call_arg_element_4 };
                    tmp_assign_source_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get, call_args );
                }

                Py_DECREF( tmp_called_instance_2 );
                Py_DECREF( tmp_call_arg_element_4 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 492;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_message_dict == NULL );
                var_message_dict = tmp_assign_source_3;
            }
            branch_end_3:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_arg_element_5;
            PyObject *tmp_call_arg_element_6;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_translations );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 494;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_call_arg_element_5 = const_str_plain_unknown;
            tmp_call_arg_element_6 = PyDict_New();
            frame_6b09f54ae5c9f31517b15e2480fcf05f->m_frame.f_lineno = 494;
            {
                PyObject *call_args[] = { tmp_call_arg_element_5, tmp_call_arg_element_6 };
                tmp_assign_source_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_3, const_str_plain_get, call_args );
            }

            Py_DECREF( tmp_called_instance_3 );
            Py_DECREF( tmp_call_arg_element_6 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 494;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_message_dict == NULL );
            var_message_dict = tmp_assign_source_4;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        if ( var_message_dict == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "message_dict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 495;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = var_message_dict;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_get );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 495;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        if ( par_message == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "message" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 495;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = par_message;
        if ( par_message == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "message" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 495;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = par_message;
        frame_6b09f54ae5c9f31517b15e2480fcf05f->m_frame.f_lineno = 495;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 495;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b09f54ae5c9f31517b15e2480fcf05f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b09f54ae5c9f31517b15e2480fcf05f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b09f54ae5c9f31517b15e2480fcf05f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6b09f54ae5c9f31517b15e2480fcf05f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6b09f54ae5c9f31517b15e2480fcf05f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6b09f54ae5c9f31517b15e2480fcf05f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6b09f54ae5c9f31517b15e2480fcf05f,
        type_description_1,
        par_self,
        par_message,
        par_plural_message,
        par_count,
        var_message_dict
    );


    // Release cached frame.
    if ( frame_6b09f54ae5c9f31517b15e2480fcf05f == cache_frame_6b09f54ae5c9f31517b15e2480fcf05f )
    {
        Py_DECREF( frame_6b09f54ae5c9f31517b15e2480fcf05f );
    }
    cache_frame_6b09f54ae5c9f31517b15e2480fcf05f = NULL;

    assertFrameObject( frame_6b09f54ae5c9f31517b15e2480fcf05f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_16_translate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    Py_XDECREF( var_message_dict );
    var_message_dict = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    Py_XDECREF( var_message_dict );
    var_message_dict = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_16_translate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_17_pgettext( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    PyObject *par_message = python_pars[ 2 ];
    PyObject *par_plural_message = python_pars[ 3 ];
    PyObject *par_count = python_pars[ 4 ];
    struct Nuitka_FrameObject *frame_541d72d224cccb1ded4070daa30e97b1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_541d72d224cccb1ded4070daa30e97b1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_541d72d224cccb1ded4070daa30e97b1, codeobj_541d72d224cccb1ded4070daa30e97b1, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_541d72d224cccb1ded4070daa30e97b1 = cache_frame_541d72d224cccb1ded4070daa30e97b1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_541d72d224cccb1ded4070daa30e97b1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_541d72d224cccb1ded4070daa30e97b1 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_translations );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 500;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 500;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gen_log );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen_log );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 501;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            frame_541d72d224cccb1ded4070daa30e97b1->m_frame.f_lineno = 501;
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_warning, &PyTuple_GET_ITEM( const_tuple_str_digest_4a0c6d12249351433c374c8e2dac95fa_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 501;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( par_message );
        tmp_args_element_name_1 = par_message;
        CHECK_OBJECT( par_plural_message );
        tmp_args_element_name_2 = par_plural_message;
        CHECK_OBJECT( par_count );
        tmp_args_element_name_3 = par_count;
        frame_541d72d224cccb1ded4070daa30e97b1->m_frame.f_lineno = 502;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain_translate, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 502;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_541d72d224cccb1ded4070daa30e97b1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_541d72d224cccb1ded4070daa30e97b1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_541d72d224cccb1ded4070daa30e97b1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_541d72d224cccb1ded4070daa30e97b1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_541d72d224cccb1ded4070daa30e97b1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_541d72d224cccb1ded4070daa30e97b1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_541d72d224cccb1ded4070daa30e97b1,
        type_description_1,
        par_self,
        par_context,
        par_message,
        par_plural_message,
        par_count
    );


    // Release cached frame.
    if ( frame_541d72d224cccb1ded4070daa30e97b1 == cache_frame_541d72d224cccb1ded4070daa30e97b1 )
    {
        Py_DECREF( frame_541d72d224cccb1ded4070daa30e97b1 );
    }
    cache_frame_541d72d224cccb1ded4070daa30e97b1 = NULL;

    assertFrameObject( frame_541d72d224cccb1ded4070daa30e97b1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_17_pgettext );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_17_pgettext );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_18___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_code = python_pars[ 1 ];
    PyObject *par_translations = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_87e572aedb8ed4c853df737392dc0d3b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_87e572aedb8ed4c853df737392dc0d3b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_87e572aedb8ed4c853df737392dc0d3b, codeobj_87e572aedb8ed4c853df737392dc0d3b, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_87e572aedb8ed4c853df737392dc0d3b = cache_frame_87e572aedb8ed4c853df737392dc0d3b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_87e572aedb8ed4c853df737392dc0d3b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_87e572aedb8ed4c853df737392dc0d3b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_translations );
        tmp_source_name_1 = par_translations;
        tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ngettext );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 509;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_ngettext, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 509;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_translations );
        tmp_source_name_2 = par_translations;
        tmp_assattr_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_gettext );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 510;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_gettext, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 510;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_GettextLocale );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_GettextLocale );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "GettextLocale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 513;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 513;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_code );
        tmp_args_element_name_1 = par_code;
        frame_87e572aedb8ed4c853df737392dc0d3b->m_frame.f_lineno = 513;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain___init__, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 513;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_87e572aedb8ed4c853df737392dc0d3b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_87e572aedb8ed4c853df737392dc0d3b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_87e572aedb8ed4c853df737392dc0d3b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_87e572aedb8ed4c853df737392dc0d3b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_87e572aedb8ed4c853df737392dc0d3b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_87e572aedb8ed4c853df737392dc0d3b,
        type_description_1,
        par_self,
        par_code,
        par_translations,
        NULL
    );


    // Release cached frame.
    if ( frame_87e572aedb8ed4c853df737392dc0d3b == cache_frame_87e572aedb8ed4c853df737392dc0d3b )
    {
        Py_DECREF( frame_87e572aedb8ed4c853df737392dc0d3b );
    }
    cache_frame_87e572aedb8ed4c853df737392dc0d3b = NULL;

    assertFrameObject( frame_87e572aedb8ed4c853df737392dc0d3b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_18___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_translations );
    Py_DECREF( par_translations );
    par_translations = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_translations );
    Py_DECREF( par_translations );
    par_translations = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_18___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_19_translate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_message = python_pars[ 1 ];
    PyObject *par_plural_message = python_pars[ 2 ];
    PyObject *par_count = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_067b26dca163587989ee9a2dbf5f8ae4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_067b26dca163587989ee9a2dbf5f8ae4 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_067b26dca163587989ee9a2dbf5f8ae4, codeobj_067b26dca163587989ee9a2dbf5f8ae4, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_067b26dca163587989ee9a2dbf5f8ae4 = cache_frame_067b26dca163587989ee9a2dbf5f8ae4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_067b26dca163587989ee9a2dbf5f8ae4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_067b26dca163587989ee9a2dbf5f8ae4 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_plural_message );
        tmp_compexpr_left_1 = par_plural_message;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_count );
            tmp_compexpr_left_2 = par_count;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                tmp_raise_type_1 = PyExc_AssertionError;
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_lineno = 519;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            CHECK_OBJECT( par_message );
            tmp_args_element_name_1 = par_message;
            CHECK_OBJECT( par_plural_message );
            tmp_args_element_name_2 = par_plural_message;
            CHECK_OBJECT( par_count );
            tmp_args_element_name_3 = par_count;
            frame_067b26dca163587989ee9a2dbf5f8ae4->m_frame.f_lineno = 520;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_return_value = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain_ngettext, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 520;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( par_self );
            tmp_called_instance_2 = par_self;
            CHECK_OBJECT( par_message );
            tmp_args_element_name_4 = par_message;
            frame_067b26dca163587989ee9a2dbf5f8ae4->m_frame.f_lineno = 522;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_gettext, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 522;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_067b26dca163587989ee9a2dbf5f8ae4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_067b26dca163587989ee9a2dbf5f8ae4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_067b26dca163587989ee9a2dbf5f8ae4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_067b26dca163587989ee9a2dbf5f8ae4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_067b26dca163587989ee9a2dbf5f8ae4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_067b26dca163587989ee9a2dbf5f8ae4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_067b26dca163587989ee9a2dbf5f8ae4,
        type_description_1,
        par_self,
        par_message,
        par_plural_message,
        par_count
    );


    // Release cached frame.
    if ( frame_067b26dca163587989ee9a2dbf5f8ae4 == cache_frame_067b26dca163587989ee9a2dbf5f8ae4 )
    {
        Py_DECREF( frame_067b26dca163587989ee9a2dbf5f8ae4 );
    }
    cache_frame_067b26dca163587989ee9a2dbf5f8ae4 = NULL;

    assertFrameObject( frame_067b26dca163587989ee9a2dbf5f8ae4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_19_translate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_19_translate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$locale$$$function_20_pgettext( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    PyObject *par_message = python_pars[ 2 ];
    PyObject *par_plural_message = python_pars[ 3 ];
    PyObject *par_count = python_pars[ 4 ];
    PyObject *var_msgs_with_ctxt = NULL;
    PyObject *var_result = NULL;
    PyObject *var_msg_with_ctxt = NULL;
    struct Nuitka_FrameObject *frame_58c754dd4825bf3ee9b8e8af194614f5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_58c754dd4825bf3ee9b8e8af194614f5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_58c754dd4825bf3ee9b8e8af194614f5, codeobj_58c754dd4825bf3ee9b8e8af194614f5, module_tornado$locale, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_58c754dd4825bf3ee9b8e8af194614f5 = cache_frame_58c754dd4825bf3ee9b8e8af194614f5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_58c754dd4825bf3ee9b8e8af194614f5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_58c754dd4825bf3ee9b8e8af194614f5 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_plural_message );
        tmp_compexpr_left_1 = par_plural_message;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_count );
            tmp_compexpr_left_2 = par_count;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                tmp_raise_type_1 = PyExc_AssertionError;
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_lineno = 547;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_mvar_value_2;
            tmp_left_name_1 = const_str_digest_83f99027874593222e45fae974a2895d;
            CHECK_OBJECT( par_context );
            tmp_tuple_element_2 = par_context;
            tmp_right_name_1 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_right_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONTEXT_SEPARATOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 549;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_2 = tmp_mvar_value_1;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( par_message );
            tmp_tuple_element_2 = par_message;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_2 );
            tmp_tuple_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 549;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_assign_source_1, 0, tmp_tuple_element_1 );
            tmp_left_name_2 = const_str_digest_83f99027874593222e45fae974a2895d;
            CHECK_OBJECT( par_context );
            tmp_tuple_element_3 = par_context;
            tmp_right_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_3 );
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_assign_source_1 );
                Py_DECREF( tmp_right_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONTEXT_SEPARATOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 550;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_3 = tmp_mvar_value_2;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_3 );
            CHECK_OBJECT( par_plural_message );
            tmp_tuple_element_3 = par_plural_message;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_right_name_2, 2, tmp_tuple_element_3 );
            tmp_tuple_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assign_source_1 );

                exception_lineno = 550;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_assign_source_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( par_count );
            tmp_tuple_element_1 = par_count;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_1, 2, tmp_tuple_element_1 );
            assert( var_msgs_with_ctxt == NULL );
            var_msgs_with_ctxt = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_dircall_arg2_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_1 = par_self;
            tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ngettext );
            if ( tmp_dircall_arg1_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 553;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_msgs_with_ctxt );
            tmp_dircall_arg2_1 = var_msgs_with_ctxt;
            Py_INCREF( tmp_dircall_arg2_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
                tmp_assign_source_2 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
            }
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 553;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_result == NULL );
            var_result = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_mvar_value_3;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONTEXT_SEPARATOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 554;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_3 = tmp_mvar_value_3;
            CHECK_OBJECT( var_result );
            tmp_compexpr_right_3 = var_result;
            tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 554;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                CHECK_OBJECT( par_self );
                tmp_called_instance_1 = par_self;
                CHECK_OBJECT( par_message );
                tmp_args_element_name_1 = par_message;
                CHECK_OBJECT( par_plural_message );
                tmp_args_element_name_2 = par_plural_message;
                CHECK_OBJECT( par_count );
                tmp_args_element_name_3 = par_count;
                frame_58c754dd4825bf3ee9b8e8af194614f5->m_frame.f_lineno = 556;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                    tmp_assign_source_3 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain_ngettext, call_args );
                }

                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 556;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_result;
                    assert( old != NULL );
                    var_result = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        CHECK_OBJECT( var_result );
        tmp_return_value = var_result;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_mvar_value_4;
            tmp_left_name_3 = const_str_digest_83f99027874593222e45fae974a2895d;
            CHECK_OBJECT( par_context );
            tmp_tuple_element_4 = par_context;
            tmp_right_name_3 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_4 );
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_right_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONTEXT_SEPARATOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 559;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_4 = tmp_mvar_value_4;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_4 );
            CHECK_OBJECT( par_message );
            tmp_tuple_element_4 = par_message;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_right_name_3, 2, tmp_tuple_element_4 );
            tmp_assign_source_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_msg_with_ctxt == NULL );
            var_msg_with_ctxt = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( par_self );
            tmp_called_instance_2 = par_self;
            CHECK_OBJECT( var_msg_with_ctxt );
            tmp_args_element_name_4 = var_msg_with_ctxt;
            frame_58c754dd4825bf3ee9b8e8af194614f5->m_frame.f_lineno = 560;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_gettext, call_args );
            }

            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 560;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_result == NULL );
            var_result = tmp_assign_source_5;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_mvar_value_5;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONTEXT_SEPARATOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 561;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_4 = tmp_mvar_value_5;
            CHECK_OBJECT( var_result );
            tmp_compexpr_right_4 = var_result;
            tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 561;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_6;
                CHECK_OBJECT( par_message );
                tmp_assign_source_6 = par_message;
                {
                    PyObject *old = var_result;
                    assert( old != NULL );
                    var_result = tmp_assign_source_6;
                    Py_INCREF( var_result );
                    Py_DECREF( old );
                }

            }
            branch_no_4:;
        }
        CHECK_OBJECT( var_result );
        tmp_return_value = var_result;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_58c754dd4825bf3ee9b8e8af194614f5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_58c754dd4825bf3ee9b8e8af194614f5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_58c754dd4825bf3ee9b8e8af194614f5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_58c754dd4825bf3ee9b8e8af194614f5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_58c754dd4825bf3ee9b8e8af194614f5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_58c754dd4825bf3ee9b8e8af194614f5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_58c754dd4825bf3ee9b8e8af194614f5,
        type_description_1,
        par_self,
        par_context,
        par_message,
        par_plural_message,
        par_count,
        var_msgs_with_ctxt,
        var_result,
        var_msg_with_ctxt
    );


    // Release cached frame.
    if ( frame_58c754dd4825bf3ee9b8e8af194614f5 == cache_frame_58c754dd4825bf3ee9b8e8af194614f5 )
    {
        Py_DECREF( frame_58c754dd4825bf3ee9b8e8af194614f5 );
    }
    cache_frame_58c754dd4825bf3ee9b8e8af194614f5 = NULL;

    assertFrameObject( frame_58c754dd4825bf3ee9b8e8af194614f5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_20_pgettext );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    Py_XDECREF( var_msgs_with_ctxt );
    var_msgs_with_ctxt = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_msg_with_ctxt );
    var_msg_with_ctxt = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_plural_message );
    Py_DECREF( par_plural_message );
    par_plural_message = NULL;

    CHECK_OBJECT( (PyObject *)par_count );
    Py_DECREF( par_count );
    par_count = NULL;

    Py_XDECREF( var_msgs_with_ctxt );
    var_msgs_with_ctxt = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_msg_with_ctxt );
    var_msg_with_ctxt = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$locale$$$function_20_pgettext );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_10_pgettext( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_10_pgettext,
        const_str_plain_pgettext,
#if PYTHON_VERSION >= 300
        const_str_digest_8aa1fb3e25bed2700e722135293009ed,
#endif
        codeobj_650c77969df137a567f8457a5a94a2f8,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_11_format_date( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_11_format_date,
        const_str_plain_format_date,
#if PYTHON_VERSION >= 300
        const_str_digest_c105a91b9f0e7aa7ab96931fa498bc8b,
#endif
        codeobj_3fab27a27092cb371e8ae54e29adbcff,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_0a7809271042678b67580f28ea6c38b7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_12_format_day( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_12_format_day,
        const_str_plain_format_day,
#if PYTHON_VERSION >= 300
        const_str_digest_9a4cda2f32b95b957c2fd58ebb08c92a,
#endif
        codeobj_c018dbe84ba4c914ac5efad120c78c2a,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_06670af157044a600ad097110ee7c367,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_13_list( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_13_list,
        const_str_plain_list,
#if PYTHON_VERSION >= 300
        const_str_digest_e157804d84d435f58356cd3da9c183e4,
#endif
        codeobj_764b37dccc915f0bcd96d4b5355bab86,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_eb2e4d4363a413350a3b55abcc716ba6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_14_friendly_number( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_14_friendly_number,
        const_str_plain_friendly_number,
#if PYTHON_VERSION >= 300
        const_str_digest_8bda2ae8a09ed125b91f60b17cc43541,
#endif
        codeobj_f153662e8ed47c2118619d52e0b7d53b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_bef59eea50ee627e5c737ba8285a3699,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_15___init__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_15___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_29385822239ba969122cbf138728dfbe,
#endif
        codeobj_a842a0780f04fb77527ad613fa687a53,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_16_translate( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_16_translate,
        const_str_plain_translate,
#if PYTHON_VERSION >= 300
        const_str_digest_9c0911e86d5a69f513ffa9986416d31c,
#endif
        codeobj_6b09f54ae5c9f31517b15e2480fcf05f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_17_pgettext( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_17_pgettext,
        const_str_plain_pgettext,
#if PYTHON_VERSION >= 300
        const_str_digest_1214c21596fee4366c3ea77fd73e2614,
#endif
        codeobj_541d72d224cccb1ded4070daa30e97b1,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_18___init__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_18___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_f499141fb159d3c1f4c2312c62754b04,
#endif
        codeobj_87e572aedb8ed4c853df737392dc0d3b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_19_translate( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_19_translate,
        const_str_plain_translate,
#if PYTHON_VERSION >= 300
        const_str_digest_6703d883ad26971c7886a4bc827d59fa,
#endif
        codeobj_067b26dca163587989ee9a2dbf5f8ae4,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_1_get( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_1_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6e9c9ec0fde4e3d3dfa88f4a417ad8df,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_915c1427e06b79b36242659fa2173cde,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_20_pgettext( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_20_pgettext,
        const_str_plain_pgettext,
#if PYTHON_VERSION >= 300
        const_str_digest_052d7566e9844fc118a7de62c1266c2d,
#endif
        codeobj_58c754dd4825bf3ee9b8e8af194614f5,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_f5737bc588770cc5264c9b81f8874d8c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_2_set_default_locale( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_2_set_default_locale,
        const_str_plain_set_default_locale,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d876f22011476a59bcd061b910cd5ccf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_e1dd5bde31ecab95c214424d92ae147d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_3_load_translations( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_3_load_translations,
        const_str_plain_load_translations,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ed18bd3f27453c02c18ebbeeb2000c16,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_a2593816d0f28e619453b3faa3652e96,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_4_load_gettext_translations( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_4_load_gettext_translations,
        const_str_plain_load_gettext_translations,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6e03f2d96809b9c77d314ed0447d982b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_556bc24351716bafdca56a89def05747,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_5_get_supported_locales( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_5_get_supported_locales,
        const_str_plain_get_supported_locales,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5af22b6e07c6e31626b3ccb031b771c5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_475df1123b507a27d0e67c96974cd632,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_6_get_closest( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_6_get_closest,
        const_str_plain_get_closest,
#if PYTHON_VERSION >= 300
        const_str_digest_0c060498692fd2cb6f8eedca3dc6fbab,
#endif
        codeobj_cbd06a4c08c800b733833a97ba1bb610,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_aa2fa87725efc213396ce7440d599ef0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_7_get( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_7_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        const_str_digest_2c507f37a18f95b53987a1891593f590,
#endif
        codeobj_173bd6178f556cc83e3014643cc70611,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_c2f26402c280cea934f7a4889cf0b12a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_8___init__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_8___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_7f7f8e2802dcb3f5d1279d9ba6ecfe7c,
#endif
        codeobj_61a8788ba2981b76b0667ee4f53933ac,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$locale$$$function_9_translate( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$locale$$$function_9_translate,
        const_str_plain_translate,
#if PYTHON_VERSION >= 300
        const_str_digest_b27e80db6e1cdf2aa0d1c5081ea4a866,
#endif
        codeobj_7b47ccf3d9329dc01e17f4b0c470baaf,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$locale,
        const_str_digest_eab034a4d05d723cadb02b46db203b41,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_tornado$locale =
{
    PyModuleDef_HEAD_INIT,
    "tornado.locale",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(tornado$locale)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(tornado$locale)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_tornado$locale );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("tornado.locale: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.locale: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.locale: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in inittornado$locale" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_tornado$locale = Py_InitModule4(
        "tornado.locale",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_tornado$locale = PyModule_Create( &mdef_tornado$locale );
#endif

    moduledict_tornado$locale = MODULE_DICT( module_tornado$locale );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_tornado$locale,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_tornado$locale,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$locale,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$locale,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_tornado$locale );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_cb6886bd8b859ac43aab4f50aae79db4, module_tornado$locale );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_5bd5a958260a4066c2f16356d7c027b8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_tornado$locale_228 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_6048e90e773f64ce9dbb0460f6546cc2_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_6048e90e773f64ce9dbb0460f6546cc2_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *locals_tornado$locale_476 = NULL;
    struct Nuitka_FrameObject *frame_2df287331323093443a3977055e987a6_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_2df287331323093443a3977055e987a6_3 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *locals_tornado$locale_505 = NULL;
    struct Nuitka_FrameObject *frame_3da1907eb5bd327c136ce25919a31dea_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3da1907eb5bd327c136ce25919a31dea_4 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_3ac0bb3f70842f105c27dddcb88c1d26;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_5bd5a958260a4066c2f16356d7c027b8 = MAKE_MODULE_FRAME( codeobj_5bd5a958260a4066c2f16356d7c027b8, module_tornado$locale );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_5bd5a958260a4066c2f16356d7c027b8 );
    assert( Py_REFCNT( frame_5bd5a958260a4066c2f16356d7c027b8 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_codecs;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 42;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_codecs, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_csv;
        tmp_globals_name_2 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 43;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_csv, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_datetime;
        tmp_globals_name_3 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 44;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_gettext;
        tmp_globals_name_4 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 45;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gettext, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_os;
        tmp_globals_name_5 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 46;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_re;
        tmp_globals_name_6 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 47;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_tornado;
        tmp_globals_name_7 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_escape_tuple;
        tmp_level_name_7 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 49;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_escape );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_escape, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_d19a843ecd797530113ead5e37e2513b;
        tmp_globals_name_8 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_gen_log_tuple;
        tmp_level_name_8 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 50;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_gen_log );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gen_log, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_03cf11698c63a9ec6d05e7924bb93e03;
        tmp_globals_name_9 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_LOCALE_NAMES_tuple;
        tmp_level_name_9 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 52;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_LOCALE_NAMES );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_LOCALE_NAMES, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_plain_typing;
        tmp_globals_name_10 = (PyObject *)moduledict_tornado$locale;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_32523c33b2c5f7d616fdfabb493279a2_tuple;
        tmp_level_name_10 = const_int_0;
        frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 54;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_13;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Iterable );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Iterable, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Any );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Any, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_Union );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Union, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_7 = tmp_import_from_1__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_Dict );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Dict, tmp_assign_source_17 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = const_str_plain_en_US;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__default_locale, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__translations, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_frozenset_arg_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__default_locale );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__default_locale );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_list_element_1 = tmp_mvar_value_3;
        tmp_frozenset_arg_1 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_frozenset_arg_1, 0, tmp_list_element_1 );
        tmp_assign_source_20 = PyFrozenSet_New( tmp_frozenset_arg_1 );
        Py_DECREF( tmp_frozenset_arg_1 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__supported_locales, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = Py_False;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain__use_gettext, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = const_str_chr_4;
        UPDATE_STRING_DICT0( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CONTEXT_SEPARATOR, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_annotations_1;
        tmp_annotations_1 = PyDict_Copy( const_dict_ba6737774c95763b85c660cd7c84b8a4 );
        tmp_assign_source_23 = MAKE_FUNCTION_tornado$locale$$$function_1_get( tmp_annotations_1 );



        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_get, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_annotations_2;
        tmp_annotations_2 = PyDict_Copy( const_dict_3fe11010327cbd4c16bbcf45efc3eecd );
        tmp_assign_source_24 = MAKE_FUNCTION_tornado$locale$$$function_2_set_default_locale( tmp_annotations_2 );



        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_set_default_locale, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_defaults_1;
        PyObject *tmp_annotations_3;
        tmp_defaults_1 = const_tuple_none_tuple;
        tmp_annotations_3 = PyDict_Copy( const_dict_dd2827a37a0ea3d35f64079aff2696f9 );
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_25 = MAKE_FUNCTION_tornado$locale$$$function_3_load_translations( tmp_defaults_1, tmp_annotations_3 );



        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_load_translations, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_annotations_4;
        tmp_annotations_4 = PyDict_Copy( const_dict_94af2e730a3b531141cb165a0fc9a983 );
        tmp_assign_source_26 = MAKE_FUNCTION_tornado$locale$$$function_4_load_gettext_translations( tmp_annotations_4 );



        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_load_gettext_translations, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_annotations_5;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_subscript_name_1;
        tmp_dict_key_1 = const_str_plain_return;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Iterable );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Iterable );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Iterable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 223;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_4;
        tmp_subscript_name_1 = (PyObject *)&PyUnicode_Type;
        tmp_dict_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;

            goto frame_exception_exit_1;
        }
        tmp_annotations_5 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_annotations_5, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_27 = MAKE_FUNCTION_tornado$locale$$$function_5_get_supported_locales( tmp_annotations_5 );



        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_get_supported_locales, tmp_assign_source_27 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_28 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_28;
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_29;
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_2 = tmp_class_creation_1__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_30 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_30;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;

                goto try_except_handler_2;
            }
            tmp_tuple_element_1 = const_str_plain_Locale;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 228;
            tmp_assign_source_31 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_31;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;

                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 228;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 228;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_32;
            tmp_assign_source_32 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_32;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_33;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_tornado$locale_228 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_cb6886bd8b859ac43aab4f50aae79db4;
        tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_05e07a22ce2c0ccc55eeafcae9e90317;
        tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_Locale;
        tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_6048e90e773f64ce9dbb0460f6546cc2_2, codeobj_6048e90e773f64ce9dbb0460f6546cc2, module_tornado$locale, sizeof(void *) );
        frame_6048e90e773f64ce9dbb0460f6546cc2_2 = cache_frame_6048e90e773f64ce9dbb0460f6546cc2_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_6048e90e773f64ce9dbb0460f6546cc2_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_6048e90e773f64ce9dbb0460f6546cc2_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = PyDict_New();
        tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain__cache, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 235;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_annotations_6;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_classmethod_arg_1;
            PyObject *tmp_annotations_7;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            PyObject *tmp_dict_key_5;
            PyObject *tmp_dict_value_5;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$locale_228, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_2 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_classmethod );

            if ( tmp_called_name_2 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dict_key_2 = const_str_plain_locale_codes;
            tmp_dict_value_2 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_2 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_2 );
                }
            }

            tmp_annotations_6 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_3 = const_str_plain_return;
            tmp_dict_value_3 = const_str_plain_Locale;
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_3, tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_1 = MAKE_FUNCTION_tornado$locale$$$function_6_get_closest( tmp_annotations_6 );



            frame_6048e90e773f64ce9dbb0460f6546cc2_2->m_frame.f_lineno = 237;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_dict_key_4 = const_str_plain_locale_codes;
            tmp_dict_value_4 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_4 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_4 );
                }
            }

            tmp_annotations_7 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_4, tmp_dict_value_4 );
            Py_DECREF( tmp_dict_value_4 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_5 = const_str_plain_return;
            tmp_dict_value_5 = const_str_plain_Locale;
            tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_5, tmp_dict_value_5 );
            assert( !(tmp_res != 0) );
            tmp_classmethod_arg_1 = MAKE_FUNCTION_tornado$locale$$$function_6_get_closest( tmp_annotations_7 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
            Py_DECREF( tmp_classmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_get_closest, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_annotations_8;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            PyObject *tmp_dict_key_7;
            PyObject *tmp_dict_value_7;
            PyObject *tmp_classmethod_arg_2;
            PyObject *tmp_annotations_9;
            PyObject *tmp_dict_key_8;
            PyObject *tmp_dict_value_8;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$locale_228, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            tmp_called_name_3 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_classmethod );

            if ( tmp_called_name_3 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dict_key_6 = const_str_plain_code;
            tmp_dict_value_6 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_6 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_6 );
                }
            }

            tmp_annotations_8 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_6, tmp_dict_value_6 );
            Py_DECREF( tmp_dict_value_6 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_7 = const_str_plain_return;
            tmp_dict_value_7 = const_str_plain_Locale;
            tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_7, tmp_dict_value_7 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_2 = MAKE_FUNCTION_tornado$locale$$$function_7_get( tmp_annotations_8 );



            frame_6048e90e773f64ce9dbb0460f6546cc2_2->m_frame.f_lineno = 255;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_4;
            condexpr_false_4:;
            tmp_dict_key_8 = const_str_plain_code;
            tmp_dict_value_8 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_8 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_8 );
                }
            }

            tmp_annotations_9 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_8, tmp_dict_value_8 );
            Py_DECREF( tmp_dict_value_8 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_9 = const_str_plain_return;
            tmp_dict_value_9 = const_str_plain_Locale;
            tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_9, tmp_dict_value_9 );
            assert( !(tmp_res != 0) );
            tmp_classmethod_arg_2 = MAKE_FUNCTION_tornado$locale$$$function_7_get( tmp_annotations_9 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_2 );
            Py_DECREF( tmp_classmethod_arg_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_4:;
            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_get, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_10;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            tmp_dict_key_10 = const_str_plain_code;
            tmp_dict_value_10 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_10 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_10 );
                }
            }

            tmp_annotations_10 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_10, tmp_dict_value_10 );
            Py_DECREF( tmp_dict_value_10 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_11 = const_str_plain_return;
            tmp_dict_value_11 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_11, tmp_dict_value_11 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_8___init__( tmp_annotations_10 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_2;
            PyObject *tmp_annotations_11;
            PyObject *tmp_dict_key_12;
            PyObject *tmp_dict_value_12;
            PyObject *tmp_dict_key_13;
            PyObject *tmp_dict_value_13;
            PyObject *tmp_dict_key_14;
            PyObject *tmp_dict_value_14;
            PyObject *tmp_dict_key_15;
            PyObject *tmp_dict_value_15;
            tmp_defaults_2 = const_tuple_none_none_tuple;
            tmp_dict_key_12 = const_str_plain_message;
            tmp_dict_value_12 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_12 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_12 );
                }
            }

            tmp_annotations_11 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_12, tmp_dict_value_12 );
            Py_DECREF( tmp_dict_value_12 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_13 = const_str_plain_plural_message;
            tmp_dict_value_13 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_13 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_13 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_13, tmp_dict_value_13 );
            Py_DECREF( tmp_dict_value_13 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_14 = const_str_plain_count;
            tmp_dict_value_14 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_int );

            if ( tmp_dict_value_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_14 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_14 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_14, tmp_dict_value_14 );
            Py_DECREF( tmp_dict_value_14 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_15 = const_str_plain_return;
            tmp_dict_value_15 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_15 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_15 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_15, tmp_dict_value_15 );
            Py_DECREF( tmp_dict_value_15 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_9_translate( tmp_defaults_2, tmp_annotations_11 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_translate, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 308;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_3;
            PyObject *tmp_annotations_12;
            PyObject *tmp_dict_key_16;
            PyObject *tmp_dict_value_16;
            PyObject *tmp_dict_key_17;
            PyObject *tmp_dict_value_17;
            PyObject *tmp_dict_key_18;
            PyObject *tmp_dict_value_18;
            PyObject *tmp_dict_key_19;
            PyObject *tmp_dict_value_19;
            PyObject *tmp_dict_key_20;
            PyObject *tmp_dict_value_20;
            tmp_defaults_3 = const_tuple_none_none_tuple;
            tmp_dict_key_16 = const_str_plain_context;
            tmp_dict_value_16 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_16 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_16 );
                }
            }

            tmp_annotations_12 = _PyDict_NewPresized( 5 );
            tmp_res = PyDict_SetItem( tmp_annotations_12, tmp_dict_key_16, tmp_dict_value_16 );
            Py_DECREF( tmp_dict_value_16 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_17 = const_str_plain_message;
            tmp_dict_value_17 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_17 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_17 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_12, tmp_dict_key_17, tmp_dict_value_17 );
            Py_DECREF( tmp_dict_value_17 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_18 = const_str_plain_plural_message;
            tmp_dict_value_18 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_18 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_18 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_12, tmp_dict_key_18, tmp_dict_value_18 );
            Py_DECREF( tmp_dict_value_18 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_19 = const_str_plain_count;
            tmp_dict_value_19 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_int );

            if ( tmp_dict_value_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_19 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_19 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_12, tmp_dict_key_19, tmp_dict_value_19 );
            Py_DECREF( tmp_dict_value_19 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_20 = const_str_plain_return;
            tmp_dict_value_20 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_20 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_20 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_12, tmp_dict_key_20, tmp_dict_value_20 );
            Py_DECREF( tmp_dict_value_20 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_10_pgettext( tmp_defaults_3, tmp_annotations_12 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_pgettext, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_4;
            PyObject *tmp_annotations_13;
            PyObject *tmp_dict_key_21;
            PyObject *tmp_dict_value_21;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_dict_key_22;
            PyObject *tmp_dict_value_22;
            PyObject *tmp_dict_key_23;
            PyObject *tmp_dict_value_23;
            PyObject *tmp_dict_key_24;
            PyObject *tmp_dict_value_24;
            PyObject *tmp_dict_key_25;
            PyObject *tmp_dict_value_25;
            PyObject *tmp_dict_key_26;
            PyObject *tmp_dict_value_26;
            tmp_defaults_4 = const_tuple_int_0_true_false_false_tuple;
            tmp_dict_key_21 = const_str_plain_date;
            tmp_subscribed_name_3 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_Union );

            if ( tmp_subscribed_name_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 327;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_3 = tmp_mvar_value_5;
                Py_INCREF( tmp_subscribed_name_3 );
                }
            }

            tmp_tuple_element_3 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_int );

            if ( tmp_tuple_element_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_3 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_tuple_element_3 );
                }
            }

            tmp_subscript_name_3 = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_subscript_name_3, 0, tmp_tuple_element_3 );
            tmp_tuple_element_3 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_float );

            if ( tmp_tuple_element_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_3 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_tuple_element_3 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_3, 1, tmp_tuple_element_3 );
            tmp_source_name_5 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_datetime );

            if ( tmp_source_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
                }

                if ( tmp_mvar_value_6 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_3 );
                    Py_DECREF( tmp_subscript_name_3 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 327;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_5 = tmp_mvar_value_6;
                Py_INCREF( tmp_source_name_5 );
                }
            }

            tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_datetime );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscribed_name_3 );
                Py_DECREF( tmp_subscript_name_3 );

                exception_lineno = 327;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_3, 2, tmp_tuple_element_3 );
            tmp_dict_value_21 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_3 );
            Py_DECREF( tmp_subscript_name_3 );
            if ( tmp_dict_value_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 327;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_13 = _PyDict_NewPresized( 6 );
            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_21, tmp_dict_value_21 );
            Py_DECREF( tmp_dict_value_21 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_22 = const_str_plain_gmt_offset;
            tmp_dict_value_22 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_int );

            if ( tmp_dict_value_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_22 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_22 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_22, tmp_dict_value_22 );
            Py_DECREF( tmp_dict_value_22 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_23 = const_str_plain_relative;
            tmp_dict_value_23 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_bool );

            if ( tmp_dict_value_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_23 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_23 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_23, tmp_dict_value_23 );
            Py_DECREF( tmp_dict_value_23 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_24 = const_str_plain_shorter;
            tmp_dict_value_24 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_bool );

            if ( tmp_dict_value_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_24 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_24 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_24, tmp_dict_value_24 );
            Py_DECREF( tmp_dict_value_24 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_25 = const_str_plain_full_format;
            tmp_dict_value_25 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_bool );

            if ( tmp_dict_value_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_25 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_25 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_25, tmp_dict_value_25 );
            Py_DECREF( tmp_dict_value_25 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_26 = const_str_plain_return;
            tmp_dict_value_26 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_26 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_26 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_26 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_13, tmp_dict_key_26, tmp_dict_value_26 );
            Py_DECREF( tmp_dict_value_26 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_4 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_11_format_date( tmp_defaults_4, tmp_annotations_13 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_format_date, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 325;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_5;
            PyObject *tmp_annotations_14;
            PyObject *tmp_dict_key_27;
            PyObject *tmp_dict_value_27;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_dict_key_28;
            PyObject *tmp_dict_value_28;
            PyObject *tmp_dict_key_29;
            PyObject *tmp_dict_value_29;
            PyObject *tmp_dict_key_30;
            PyObject *tmp_dict_value_30;
            tmp_defaults_5 = const_tuple_int_0_true_tuple;
            tmp_dict_key_27 = const_str_plain_date;
            tmp_source_name_6 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_datetime );

            if ( tmp_source_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_datetime );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 426;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_6 = tmp_mvar_value_7;
                Py_INCREF( tmp_source_name_6 );
                }
            }

            tmp_dict_value_27 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_datetime );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_dict_value_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 426;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_14 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_27, tmp_dict_value_27 );
            Py_DECREF( tmp_dict_value_27 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_28 = const_str_plain_gmt_offset;
            tmp_dict_value_28 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_int );

            if ( tmp_dict_value_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_28 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_28 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_28, tmp_dict_value_28 );
            Py_DECREF( tmp_dict_value_28 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_29 = const_str_plain_dow;
            tmp_dict_value_29 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_bool );

            if ( tmp_dict_value_29 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_29 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_29 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_29, tmp_dict_value_29 );
            Py_DECREF( tmp_dict_value_29 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_30 = const_str_plain_return;
            tmp_dict_value_30 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_bool );

            if ( tmp_dict_value_30 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_30 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_30 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_30, tmp_dict_value_30 );
            Py_DECREF( tmp_dict_value_30 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_5 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_12_format_day( tmp_defaults_5, tmp_annotations_14 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_format_day, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 425;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_15;
            PyObject *tmp_dict_key_31;
            PyObject *tmp_dict_value_31;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_dict_key_32;
            PyObject *tmp_dict_value_32;
            tmp_dict_key_31 = const_str_plain_parts;
            tmp_dict_value_31 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_Any );

            if ( tmp_dict_value_31 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 447;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_dict_value_31 = tmp_mvar_value_8;
                Py_INCREF( tmp_dict_value_31 );
                }
            }

            tmp_annotations_15 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_31, tmp_dict_value_31 );
            Py_DECREF( tmp_dict_value_31 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_32 = const_str_plain_return;
            tmp_dict_value_32 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_32 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_32 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_32, tmp_dict_value_32 );
            Py_DECREF( tmp_dict_value_32 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_13_list( tmp_annotations_15 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_list, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 447;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_16;
            PyObject *tmp_dict_key_33;
            PyObject *tmp_dict_value_33;
            PyObject *tmp_dict_key_34;
            PyObject *tmp_dict_value_34;
            tmp_dict_key_33 = const_str_plain_value;
            tmp_dict_value_33 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_int );

            if ( tmp_dict_value_33 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_33 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_33 );
                }
            }

            tmp_annotations_16 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_33, tmp_dict_value_33 );
            Py_DECREF( tmp_dict_value_33 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_34 = const_str_plain_return;
            tmp_dict_value_34 = PyObject_GetItem( locals_tornado$locale_228, const_str_plain_str );

            if ( tmp_dict_value_34 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_34 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_34 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_34, tmp_dict_value_34 );
            Py_DECREF( tmp_dict_value_34 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_14_friendly_number( tmp_annotations_16 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain_friendly_number, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 464;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6048e90e773f64ce9dbb0460f6546cc2_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6048e90e773f64ce9dbb0460f6546cc2_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_6048e90e773f64ce9dbb0460f6546cc2_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_6048e90e773f64ce9dbb0460f6546cc2_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_6048e90e773f64ce9dbb0460f6546cc2_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_6048e90e773f64ce9dbb0460f6546cc2_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_6048e90e773f64ce9dbb0460f6546cc2_2 == cache_frame_6048e90e773f64ce9dbb0460f6546cc2_2 )
        {
            Py_DECREF( frame_6048e90e773f64ce9dbb0460f6546cc2_2 );
        }
        cache_frame_6048e90e773f64ce9dbb0460f6546cc2_2 = NULL;

        assertFrameObject( frame_6048e90e773f64ce9dbb0460f6546cc2_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;

                goto try_except_handler_4;
            }
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$locale_228, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_4 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_Locale;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_tornado$locale_228;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 228;
            tmp_assign_source_34 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;

                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_34;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_33 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_33 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_tornado$locale_228 );
        locals_tornado$locale_228 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$locale_228 );
        locals_tornado$locale_228 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 228;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Locale, tmp_assign_source_33 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Locale );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Locale );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Locale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 476;

            goto try_except_handler_5;
        }

        tmp_tuple_element_5 = tmp_mvar_value_9;
        tmp_assign_source_35 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_assign_source_35, 0, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_35;
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_36 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_36;
    }
    {
        PyObject *tmp_assign_source_37;
        tmp_assign_source_37 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_37;
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_10;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        tmp_condition_result_10 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_4 = tmp_class_creation_2__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_6:;
        condexpr_end_5:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_38 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_38;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_5;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_7 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___prepare__ );
        tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_8;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_8 = tmp_class_creation_2__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 476;

                goto try_except_handler_5;
            }
            tmp_tuple_element_6 = const_str_plain_CSVLocale;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 476;
            tmp_assign_source_39 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 476;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_39;
        }
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_9 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 476;

                goto try_except_handler_5;
            }
            tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_10;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 476;

                    goto try_except_handler_5;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_10 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_10 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_10 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 476;

                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 476;

                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 476;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_5;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_40;
            tmp_assign_source_40 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_40;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_41;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_tornado$locale_476 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_cb6886bd8b859ac43aab4f50aae79db4;
        tmp_res = PyObject_SetItem( locals_tornado$locale_476, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_digest_bc4c3b36cdd00cb6e1d6ca8f27b40a93;
        tmp_res = PyObject_SetItem( locals_tornado$locale_476, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_plain_CSVLocale;
        tmp_res = PyObject_SetItem( locals_tornado$locale_476, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;

            goto try_except_handler_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_2df287331323093443a3977055e987a6_3, codeobj_2df287331323093443a3977055e987a6, module_tornado$locale, sizeof(void *) );
        frame_2df287331323093443a3977055e987a6_3 = cache_frame_2df287331323093443a3977055e987a6_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_2df287331323093443a3977055e987a6_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_2df287331323093443a3977055e987a6_3 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_annotations_17;
            PyObject *tmp_dict_key_35;
            PyObject *tmp_dict_value_35;
            PyObject *tmp_dict_key_36;
            PyObject *tmp_dict_value_36;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_subscribed_name_6;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_subscript_name_6;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_dict_key_37;
            PyObject *tmp_dict_value_37;
            tmp_dict_key_35 = const_str_plain_code;
            tmp_dict_value_35 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_35 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_35 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_35 );
                }
            }

            tmp_annotations_17 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_35, tmp_dict_value_35 );
            Py_DECREF( tmp_dict_value_35 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_36 = const_str_plain_translations;
            tmp_subscribed_name_5 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_Dict );

            if ( tmp_subscribed_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_10 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 479;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_5 = tmp_mvar_value_10;
                Py_INCREF( tmp_subscribed_name_5 );
                }
            }

            tmp_tuple_element_8 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_tuple_element_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_8 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_8 );
                }
            }

            tmp_subscript_name_5 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_5, 0, tmp_tuple_element_8 );
            tmp_subscribed_name_6 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_Dict );

            if ( tmp_subscribed_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_11 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    Py_DECREF( tmp_subscribed_name_5 );
                    Py_DECREF( tmp_subscript_name_5 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 479;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_6 = tmp_mvar_value_11;
                Py_INCREF( tmp_subscribed_name_6 );
                }
            }

            tmp_tuple_element_9 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_tuple_element_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_9 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_9 );
                }
            }

            tmp_subscript_name_6 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_6, 0, tmp_tuple_element_9 );
            tmp_tuple_element_9 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_tuple_element_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_9 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_9 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_6, 1, tmp_tuple_element_9 );
            tmp_tuple_element_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
            Py_DECREF( tmp_subscribed_name_6 );
            Py_DECREF( tmp_subscript_name_6 );
            if ( tmp_tuple_element_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_17 );
                Py_DECREF( tmp_subscribed_name_5 );
                Py_DECREF( tmp_subscript_name_5 );

                exception_lineno = 479;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_5, 1, tmp_tuple_element_8 );
            tmp_dict_value_36 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            Py_DECREF( tmp_subscribed_name_5 );
            Py_DECREF( tmp_subscript_name_5 );
            if ( tmp_dict_value_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_17 );

                exception_lineno = 479;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_36, tmp_dict_value_36 );
            Py_DECREF( tmp_dict_value_36 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_37 = const_str_plain_return;
            tmp_dict_value_37 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_37, tmp_dict_value_37 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_15___init__( tmp_annotations_17 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_476, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 479;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_6;
            PyObject *tmp_annotations_18;
            PyObject *tmp_dict_key_38;
            PyObject *tmp_dict_value_38;
            PyObject *tmp_dict_key_39;
            PyObject *tmp_dict_value_39;
            PyObject *tmp_dict_key_40;
            PyObject *tmp_dict_value_40;
            PyObject *tmp_dict_key_41;
            PyObject *tmp_dict_value_41;
            tmp_defaults_6 = const_tuple_none_none_tuple;
            tmp_dict_key_38 = const_str_plain_message;
            tmp_dict_value_38 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_38 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_38 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_38 );
                }
            }

            tmp_annotations_18 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_38, tmp_dict_value_38 );
            Py_DECREF( tmp_dict_value_38 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_39 = const_str_plain_plural_message;
            tmp_dict_value_39 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_39 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_39 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_39 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_39, tmp_dict_value_39 );
            Py_DECREF( tmp_dict_value_39 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_40 = const_str_plain_count;
            tmp_dict_value_40 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_int );

            if ( tmp_dict_value_40 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_40 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_40 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_40, tmp_dict_value_40 );
            Py_DECREF( tmp_dict_value_40 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_41 = const_str_plain_return;
            tmp_dict_value_41 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_41 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_41 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_41 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_41, tmp_dict_value_41 );
            Py_DECREF( tmp_dict_value_41 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_6 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_16_translate( tmp_defaults_6, tmp_annotations_18 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_476, const_str_plain_translate, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 483;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_7;
            PyObject *tmp_annotations_19;
            PyObject *tmp_dict_key_42;
            PyObject *tmp_dict_value_42;
            PyObject *tmp_dict_key_43;
            PyObject *tmp_dict_value_43;
            PyObject *tmp_dict_key_44;
            PyObject *tmp_dict_value_44;
            PyObject *tmp_dict_key_45;
            PyObject *tmp_dict_value_45;
            PyObject *tmp_dict_key_46;
            PyObject *tmp_dict_value_46;
            tmp_defaults_7 = const_tuple_none_none_tuple;
            tmp_dict_key_42 = const_str_plain_context;
            tmp_dict_value_42 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_42 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_42 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_42 );
                }
            }

            tmp_annotations_19 = _PyDict_NewPresized( 5 );
            tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_42, tmp_dict_value_42 );
            Py_DECREF( tmp_dict_value_42 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_43 = const_str_plain_message;
            tmp_dict_value_43 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_43 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_43 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_43 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_43, tmp_dict_value_43 );
            Py_DECREF( tmp_dict_value_43 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_44 = const_str_plain_plural_message;
            tmp_dict_value_44 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_44 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_44 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_44 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_44, tmp_dict_value_44 );
            Py_DECREF( tmp_dict_value_44 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_45 = const_str_plain_count;
            tmp_dict_value_45 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_int );

            if ( tmp_dict_value_45 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_45 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_45 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_45, tmp_dict_value_45 );
            Py_DECREF( tmp_dict_value_45 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_46 = const_str_plain_return;
            tmp_dict_value_46 = PyObject_GetItem( locals_tornado$locale_476, const_str_plain_str );

            if ( tmp_dict_value_46 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_46 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_46 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_46, tmp_dict_value_46 );
            Py_DECREF( tmp_dict_value_46 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_7 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_17_pgettext( tmp_defaults_7, tmp_annotations_19 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_476, const_str_plain_pgettext, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 497;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_2df287331323093443a3977055e987a6_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_2df287331323093443a3977055e987a6_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_2df287331323093443a3977055e987a6_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_2df287331323093443a3977055e987a6_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_2df287331323093443a3977055e987a6_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_2df287331323093443a3977055e987a6_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_2df287331323093443a3977055e987a6_3 == cache_frame_2df287331323093443a3977055e987a6_3 )
        {
            Py_DECREF( frame_2df287331323093443a3977055e987a6_3 );
        }
        cache_frame_2df287331323093443a3977055e987a6_3 = NULL;

        assertFrameObject( frame_2df287331323093443a3977055e987a6_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_7;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_14;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 476;

                goto try_except_handler_7;
            }
            tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_tornado$locale_476, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 476;

                goto try_except_handler_7;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_42;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_6 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_10 = const_str_plain_CSVLocale;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_10 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_10 );
            tmp_tuple_element_10 = locals_tornado$locale_476;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 476;
            tmp_assign_source_42 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_42 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 476;

                goto try_except_handler_7;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_42;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_41 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_41 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_tornado$locale_476 );
        locals_tornado$locale_476 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$locale_476 );
        locals_tornado$locale_476 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 476;
        goto try_except_handler_5;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_CSVLocale, tmp_assign_source_41 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_tuple_element_11;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_Locale );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Locale );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Locale" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 505;

            goto try_except_handler_8;
        }

        tmp_tuple_element_11 = tmp_mvar_value_12;
        tmp_assign_source_43 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_11 );
        PyTuple_SET_ITEM( tmp_assign_source_43, 0, tmp_tuple_element_11 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_43;
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_44 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_44;
    }
    {
        PyObject *tmp_assign_source_45;
        tmp_assign_source_45 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_45;
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_16;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_subscript_name_7;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        tmp_condition_result_16 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_8;
        }
        else
        {
            goto condexpr_false_8;
        }
        condexpr_true_8:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_7 = tmp_class_creation_3__bases;
        tmp_subscript_name_7 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        goto condexpr_end_8;
        condexpr_false_8:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_8:;
        condexpr_end_7:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_46 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_46;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_8;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_18;
        PyObject *tmp_source_name_11;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_11 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___prepare__ );
        tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_47;
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_12;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_12 = tmp_class_creation_3__metaclass;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___prepare__ );
            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 505;

                goto try_except_handler_8;
            }
            tmp_tuple_element_12 = const_str_plain_GettextLocale;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_12 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 505;
            tmp_assign_source_47 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 505;

                goto try_except_handler_8;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_47;
        }
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_13;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_13 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_13, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 505;

                goto try_except_handler_8;
            }
            tmp_condition_result_19 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_13;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_14;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_13 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 505;

                    goto try_except_handler_8;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_13 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_14 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_14 == NULL) );
                tmp_tuple_element_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_14 );
                if ( tmp_tuple_element_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 505;

                    goto try_except_handler_8;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_13 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 505;

                    goto try_except_handler_8;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 505;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_8;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_48;
            tmp_assign_source_48 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_48;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_49;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_tornado$locale_505 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_cb6886bd8b859ac43aab4f50aae79db4;
        tmp_res = PyObject_SetItem( locals_tornado$locale_505, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_digest_ff2e73506fa67da0b46f1c793668749b;
        tmp_res = PyObject_SetItem( locals_tornado$locale_505, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_plain_GettextLocale;
        tmp_res = PyObject_SetItem( locals_tornado$locale_505, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;

            goto try_except_handler_10;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3da1907eb5bd327c136ce25919a31dea_4, codeobj_3da1907eb5bd327c136ce25919a31dea, module_tornado$locale, sizeof(void *) );
        frame_3da1907eb5bd327c136ce25919a31dea_4 = cache_frame_3da1907eb5bd327c136ce25919a31dea_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3da1907eb5bd327c136ce25919a31dea_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3da1907eb5bd327c136ce25919a31dea_4 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_annotations_20;
            PyObject *tmp_dict_key_47;
            PyObject *tmp_dict_value_47;
            PyObject *tmp_dict_key_48;
            PyObject *tmp_dict_value_48;
            PyObject *tmp_source_name_15;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_dict_key_49;
            PyObject *tmp_dict_value_49;
            tmp_dict_key_47 = const_str_plain_code;
            tmp_dict_value_47 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_47 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_47 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_47 );
                }
            }

            tmp_annotations_20 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_47, tmp_dict_value_47 );
            Py_DECREF( tmp_dict_value_47 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_48 = const_str_plain_translations;
            tmp_source_name_15 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_gettext );

            if ( tmp_source_name_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_gettext );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gettext );
                }

                if ( tmp_mvar_value_13 == NULL )
                {
                    Py_DECREF( tmp_annotations_20 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gettext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 508;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_15 = tmp_mvar_value_13;
                Py_INCREF( tmp_source_name_15 );
                }
            }

            tmp_dict_value_48 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_NullTranslations );
            Py_DECREF( tmp_source_name_15 );
            if ( tmp_dict_value_48 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_20 );

                exception_lineno = 508;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_48, tmp_dict_value_48 );
            Py_DECREF( tmp_dict_value_48 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_49 = const_str_plain_return;
            tmp_dict_value_49 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_49, tmp_dict_value_49 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_18___init__( tmp_annotations_20 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_505, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 508;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_defaults_8;
            PyObject *tmp_annotations_21;
            PyObject *tmp_dict_key_50;
            PyObject *tmp_dict_value_50;
            PyObject *tmp_dict_key_51;
            PyObject *tmp_dict_value_51;
            PyObject *tmp_dict_key_52;
            PyObject *tmp_dict_value_52;
            PyObject *tmp_dict_key_53;
            PyObject *tmp_dict_value_53;
            tmp_defaults_8 = const_tuple_none_none_tuple;
            tmp_dict_key_50 = const_str_plain_message;
            tmp_dict_value_50 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_50 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_50 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_50 );
                }
            }

            tmp_annotations_21 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_50, tmp_dict_value_50 );
            Py_DECREF( tmp_dict_value_50 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_51 = const_str_plain_plural_message;
            tmp_dict_value_51 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_51 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_51 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_51 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_51, tmp_dict_value_51 );
            Py_DECREF( tmp_dict_value_51 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_52 = const_str_plain_count;
            tmp_dict_value_52 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_int );

            if ( tmp_dict_value_52 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_52 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_52 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_52, tmp_dict_value_52 );
            Py_DECREF( tmp_dict_value_52 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_53 = const_str_plain_return;
            tmp_dict_value_53 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_53 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_53 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_53 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_53, tmp_dict_value_53 );
            Py_DECREF( tmp_dict_value_53 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_8 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_19_translate( tmp_defaults_8, tmp_annotations_21 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_505, const_str_plain_translate, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 515;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_defaults_9;
            PyObject *tmp_annotations_22;
            PyObject *tmp_dict_key_54;
            PyObject *tmp_dict_value_54;
            PyObject *tmp_dict_key_55;
            PyObject *tmp_dict_value_55;
            PyObject *tmp_dict_key_56;
            PyObject *tmp_dict_value_56;
            PyObject *tmp_dict_key_57;
            PyObject *tmp_dict_value_57;
            PyObject *tmp_dict_key_58;
            PyObject *tmp_dict_value_58;
            tmp_defaults_9 = const_tuple_none_none_tuple;
            tmp_dict_key_54 = const_str_plain_context;
            tmp_dict_value_54 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_54 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_54 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_54 );
                }
            }

            tmp_annotations_22 = _PyDict_NewPresized( 5 );
            tmp_res = PyDict_SetItem( tmp_annotations_22, tmp_dict_key_54, tmp_dict_value_54 );
            Py_DECREF( tmp_dict_value_54 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_55 = const_str_plain_message;
            tmp_dict_value_55 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_55 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_55 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_55 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_22, tmp_dict_key_55, tmp_dict_value_55 );
            Py_DECREF( tmp_dict_value_55 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_56 = const_str_plain_plural_message;
            tmp_dict_value_56 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_56 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_56 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_56 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_22, tmp_dict_key_56, tmp_dict_value_56 );
            Py_DECREF( tmp_dict_value_56 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_57 = const_str_plain_count;
            tmp_dict_value_57 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_int );

            if ( tmp_dict_value_57 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_57 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_57 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_22, tmp_dict_key_57, tmp_dict_value_57 );
            Py_DECREF( tmp_dict_value_57 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_58 = const_str_plain_return;
            tmp_dict_value_58 = PyObject_GetItem( locals_tornado$locale_505, const_str_plain_str );

            if ( tmp_dict_value_58 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_58 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_58 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_22, tmp_dict_key_58, tmp_dict_value_58 );
            Py_DECREF( tmp_dict_value_58 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$locale$$$function_20_pgettext( tmp_defaults_9, tmp_annotations_22 );



            tmp_res = PyObject_SetItem( locals_tornado$locale_505, const_str_plain_pgettext, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 524;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3da1907eb5bd327c136ce25919a31dea_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3da1907eb5bd327c136ce25919a31dea_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3da1907eb5bd327c136ce25919a31dea_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3da1907eb5bd327c136ce25919a31dea_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3da1907eb5bd327c136ce25919a31dea_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3da1907eb5bd327c136ce25919a31dea_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_3da1907eb5bd327c136ce25919a31dea_4 == cache_frame_3da1907eb5bd327c136ce25919a31dea_4 )
        {
            Py_DECREF( frame_3da1907eb5bd327c136ce25919a31dea_4 );
        }
        cache_frame_3da1907eb5bd327c136ce25919a31dea_4 = NULL;

        assertFrameObject( frame_3da1907eb5bd327c136ce25919a31dea_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_10;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_20;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 505;

                goto try_except_handler_10;
            }
            tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_tornado$locale_505, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 505;

                goto try_except_handler_10;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_50;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_8 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_14 = const_str_plain_GettextLocale;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_14 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_14 );
            tmp_tuple_element_14 = locals_tornado$locale_505;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_5bd5a958260a4066c2f16356d7c027b8->m_frame.f_lineno = 505;
            tmp_assign_source_50 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_50 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 505;

                goto try_except_handler_10;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_50;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_49 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_49 );
        goto try_return_handler_10;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        Py_DECREF( locals_tornado$locale_505 );
        locals_tornado$locale_505 = NULL;
        goto try_return_handler_9;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$locale_505 );
        locals_tornado$locale_505 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto try_except_handler_9;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$locale );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 505;
        goto try_except_handler_8;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_tornado$locale, (Nuitka_StringObject *)const_str_plain_GettextLocale, tmp_assign_source_49 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5bd5a958260a4066c2f16356d7c027b8 );
#endif
    popFrameStack();

    assertFrameObject( frame_5bd5a958260a4066c2f16356d7c027b8 );

    goto frame_no_exception_4;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5bd5a958260a4066c2f16356d7c027b8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5bd5a958260a4066c2f16356d7c027b8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5bd5a958260a4066c2f16356d7c027b8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5bd5a958260a4066c2f16356d7c027b8, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;


    return MOD_RETURN_VALUE( module_tornado$locale );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
