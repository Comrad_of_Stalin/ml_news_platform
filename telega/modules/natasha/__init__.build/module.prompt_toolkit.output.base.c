/* Generated code for Python module 'prompt_toolkit.output.base'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$output$base" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$output$base;
PyDictObject *moduledict_prompt_toolkit$output$base;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_7829343aeb8baaea9dd1d74962f7fce5;
static PyObject *const_str_digest_dec178e9f07b8197db43bd257822dc51;
static PyObject *const_str_digest_c768843ed10151a01402694b3d1d371c;
extern PyObject *const_str_plain_enter_alternate_screen;
extern PyObject *const_tuple_str_plain_self_str_plain_amount_tuple;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain___name__;
extern PyObject *const_tuple_str_plain___class___tuple;
static PyObject *const_str_digest_14fb97c1142e2dcf0d4a21ead83ae1d4;
static PyObject *const_str_digest_9ba4feb51a956bffe5cea67a7c9377fa;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_hide_cursor;
extern PyObject *const_str_plain_color_depth;
extern PyObject *const_str_plain_erase_end_of_line;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_abc;
extern PyObject *const_str_plain_object;
extern PyObject *const_tuple_str_plain_self_str_plain_title_tuple;
static PyObject *const_str_digest_663ccffe3fe7c6718cc297d815b26fd9;
extern PyObject *const_str_plain_reset_attributes;
static PyObject *const_str_digest_c7450a192faf5ea8906fd2e9de0ebb6a;
extern PyObject *const_str_plain_fileno;
extern PyObject *const_str_plain_enable_autowrap;
static PyObject *const_str_digest_36176eb3d536f46685a6f161f51c68b6;
static PyObject *const_str_digest_999f695b95bca0840444542c96ea96e7;
static PyObject *const_str_digest_da8eb1012811f54209375770755941fe;
static PyObject *const_str_digest_8f0113edabf95528a7200f9a768b5976;
extern PyObject *const_str_plain_cursor_forward;
extern PyObject *const_int_pos_80;
static PyObject *const_str_digest_08fea1b4c475524cd4054ba7371dc896;
static PyObject *const_str_digest_50508c7979e0691cfeb38f9cbfb209ac;
extern PyObject *const_str_plain_abstractmethod;
extern PyObject *const_str_plain_cursor_up;
static PyObject *const_str_digest_ec0f046b7acb8ec2d2cc3c8de6176c30;
static PyObject *const_str_digest_dfd82adf71ab68a753acd566b6d38e20;
static PyObject *const_str_digest_2aed43cef6efc9c00dc4438856dad9fc;
static PyObject *const_str_digest_3e30e3b82b4323354a17341867b5d0d2;
static PyObject *const_str_digest_14927f470548c9a8c459f537c4d606e3;
static PyObject *const_str_digest_3bdcb6c0545f43479aa2924b95b6ba71;
extern PyObject *const_str_plain_flush;
extern PyObject *const_tuple_int_0_int_0_tuple;
extern PyObject *const_str_plain_cursor_down;
extern PyObject *const_str_digest_c1a89acdc0ab4f2bdb72558eadf3a14a;
static PyObject *const_str_digest_80efe96a33feb513e7b847a7bf3d0225;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_7b6588ddf1e62dbfaa95743ef0f5de28;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_attrs;
extern PyObject *const_str_plain_data;
static PyObject *const_tuple_str_plain_self_str_plain_attrs_str_plain_color_depth_tuple;
static PyObject *const_str_digest_5ebb06d0022985d2104685a594dd4ac0;
extern PyObject *const_str_plain_show_cursor;
extern PyObject *const_str_plain_Output;
static PyObject *const_str_digest_694e45dd9e5255c0bf1292133f76932a;
static PyObject *const_str_digest_09601a5c4deed6ab50fc30222e411887;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_1e597e02a9871accd1caf14ea21f1a48;
static PyObject *const_str_digest_202ccd6f07239f3c8cb827dba83fc76d;
static PyObject *const_str_digest_ac80ccef51bb506f114f80a65d36a419;
static PyObject *const_str_digest_89aa056be9c7e5ff2dc58fe6983d5187;
extern PyObject *const_str_plain_cursor_goto;
static PyObject *const_str_digest_1506b03c4467468b9a95d33f0f1b1dae;
static PyObject *const_str_digest_087dacde81663e3e0968e6783f2de514;
static PyObject *const_str_digest_6df94ed0037e26719e9d01e5a37deda6;
static PyObject *const_str_digest_0b30589eee2f4e96a743b3eb380d1297;
extern PyObject *const_str_plain_six;
static PyObject *const_str_digest_dcc217ed13a4a0a580b10adf5023f7bf;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_get_rows_below_cursor_position;
static PyObject *const_str_digest_f647240ea52ccde05f7be0d913a94199;
extern PyObject *const_str_plain_cursor_backward;
static PyObject *const_str_digest_2c8594080f72870e72c65e312495d6f4;
static PyObject *const_str_digest_8e5855ca7067098f9bf1bfd11fb956b2;
static PyObject *const_str_digest_25c0825942f1d35f9ce4eea4f3950df7;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_5d03614ccefe559d157d65485c0d1147;
extern PyObject *const_str_plain_disable_autowrap;
extern PyObject *const_str_plain_columns;
static PyObject *const_str_digest_932c3300ae1e51f5120bb0994ba59b63;
extern PyObject *const_str_digest_6c08e9b66c2dc0968961027756a22333;
extern PyObject *const_str_plain_title;
static PyObject *const_str_digest_ae525420a2e70f93183cb31e5deca5ae;
static PyObject *const_str_digest_a0706dad627a6637adfe4d54f0a3ce20;
static PyObject *const_str_digest_120b7720def5ff816ae3417246538854;
static PyObject *const_str_digest_bd3866bba06b7d7d3fc346efdc505c82;
extern PyObject *const_tuple_str_plain_Size_tuple;
static PyObject *const_str_digest_af2cf307d48d74333c6ef743ee7962d0;
extern PyObject *const_str_plain_enable_mouse_support;
static PyObject *const_str_digest_4ccfcaf5e2781b904b35481c559b1ac1;
static PyObject *const_str_digest_48473635da0b65ba98d195c930a9ab09;
static PyObject *const_str_digest_c99a84050f4f47a8ec5fac065680ea37;
extern PyObject *const_str_plain_clear_title;
static PyObject *const_str_digest_8221d521c8d7f0d3d18c29c3c746f407;
extern PyObject *const_str_plain_rows;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_scroll_buffer_to_prompt;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_tuple_str_plain_ABCMeta_str_plain_abstractmethod_tuple;
static PyObject *const_dict_e706279e75687480bbcc20e3645fe7d7;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_digest_f5a6294764d6f8fed159044754bd9b70;
extern PyObject *const_str_plain_write_raw;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_digest_4a27625dd31ca8c93050501996bd4790;
static PyObject *const_str_digest_e09c0c910bc0d0f005ed13b2b3ba596b;
static PyObject *const_str_digest_4eb05cb08b804072578aa7744c547fa3;
static PyObject *const_str_digest_b125201b2c3126ab088f75ee2a83d85c;
extern PyObject *const_int_0;
static PyObject *const_list_str_plain_Output_str_plain_DummyOutput_list;
extern PyObject *const_str_plain_bell;
extern PyObject *const_tuple_str_plain_self_str_plain_row_str_plain_column_tuple;
extern PyObject *const_str_digest_afb1dc9d409deb3b7fe4024e073b84b2;
extern PyObject *const_str_plain_erase_down;
static PyObject *const_str_digest_a100f5126b8ef6f34dabdc14e202fc86;
extern PyObject *const_str_plain_quit_alternate_screen;
extern PyObject *const_str_plain_ABCMeta;
static PyObject *const_str_digest_c068aebc67d49a9ef8fdae8b481115e8;
extern PyObject *const_str_plain_row;
static PyObject *const_str_digest_ecfdf1a34a369ffbdffb91a970a98862;
static PyObject *const_str_digest_fe3454b3d4da126f380099500352a60c;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_b631d30e30032dcfd220c349d5de8ec0;
static PyObject *const_str_digest_7300954f838f00ab5167029163f33db7;
extern PyObject *const_str_plain_disable_mouse_support;
static PyObject *const_str_digest_a7c36282f7b7721404bd3bfb13fa5999;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_bb8543d14c4bb77f19d46b486e3c133b;
static PyObject *const_str_digest_448df5a8a02a06f6f6b7ac861804077a;
static PyObject *const_str_digest_130c3cf83a3960d2838f0dd2bc8e82e0;
extern PyObject *const_str_plain_column;
static PyObject *const_str_digest_5e89fe18c23fd245f51f1f8a85ac516d;
extern PyObject *const_str_digest_96d4b7eba020383f6f6d437d0ddb1b2d;
static PyObject *const_str_digest_c755db6eead683e11ef95108e352544a;
static PyObject *const_str_digest_f0640ff90e887fde651b4ed933f49909;
static PyObject *const_str_digest_8fa8b0d4e0fdefa7e3d6500c783eed20;
extern PyObject *const_str_plain_type;
static PyObject *const_str_digest_862cd2f2b357b5c19ce21ab0b1590f2f;
static PyObject *const_str_digest_c6a5c2421584d9b57afbebc94bb68bdb;
static PyObject *const_str_digest_15082d24a9246f1671857ee111abe5b0;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_c8dab8d434ffb6e6e263482b2a16a42a;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_digest_f7df7c19f7473d40c9bf203bb1d70fca;
extern PyObject *const_str_plain_disable_bracketed_paste;
static PyObject *const_str_digest_9324ef0a3b911702053678f3cb613d02;
static PyObject *const_str_digest_e26d5477861662fbac8ffc0b322d5cdc;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_digest_06f3787b6a76fe89f837964c81cb9ebc;
extern PyObject *const_tuple_str_plain_with_metaclass_tuple;
extern PyObject *const_str_plain_DummyOutput;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_str_digest_91db96957842275a305c52c545265ca6;
static PyObject *const_str_digest_379da375eaf32853d9201cc53c888089;
extern PyObject *const_str_digest_66d43be44abed6e83a28713fad8e1523;
static PyObject *const_str_digest_c3c5a2fabec9802e5ea49418dee11362;
static PyObject *const_str_digest_85b9f78fb96a53ae591b91df451afe81;
static PyObject *const_str_digest_30a43304a778737b27b19b13e908dd60;
static PyObject *const_str_digest_6e6723d3292a3915e476469389a3dc2a;
static PyObject *const_str_digest_8c7a1a827931f70b446e5c5ea05db235;
extern PyObject *const_str_digest_3c06bc9b326bb91a7e78926c176ec572;
extern PyObject *const_str_plain_set_title;
static PyObject *const_str_digest_519035211152d76f3610ed2ad2933d98;
extern PyObject *const_str_plain_ask_for_cpr;
static PyObject *const_str_digest_4b09407213011b8c7b3a83571024c105;
static PyObject *const_str_digest_50012427db5e9c8ba7b6b597004b0640;
extern PyObject *const_str_plain_set_attributes;
extern PyObject *const_tuple_str_plain_self_str_plain_data_tuple;
static PyObject *const_str_digest_06f36d9cde3d6031d0eacda529e99e05;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_digest_22b5e94e13f35f8f2cc9c14e3ee867b2;
extern PyObject *const_str_plain_get_size;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_write;
extern PyObject *const_int_pos_40;
static PyObject *const_str_digest_1c836df9b8160765c58053b62d719192;
extern PyObject *const_str_plain_Size;
extern PyObject *const_str_plain_erase_screen;
extern PyObject *const_str_plain_amount;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_6eadfa176897a37e39c912dd49464fb8;
extern PyObject *const_str_plain_enable_bracketed_paste;
static PyObject *const_str_digest_2227bd60a6b597cbe124ec42ffd30777;
static PyObject *const_str_digest_8e644be611a1a5826ad846989b1e737e;
static PyObject *const_str_digest_1da7ccf968632b12e682947ab1515aba;
extern PyObject *const_str_plain_with_metaclass;
static PyObject *const_str_digest_a66b99ad40c978bd4dc144639f333cea;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_7829343aeb8baaea9dd1d74962f7fce5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809715 ], 21, 0 );
    const_str_digest_dec178e9f07b8197db43bd257822dc51 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809736 ], 21, 0 );
    const_str_digest_c768843ed10151a01402694b3d1d371c = UNSTREAM_STRING_ASCII( &constant_bin[ 4809757 ], 21, 0 );
    const_str_digest_14fb97c1142e2dcf0d4a21ead83ae1d4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809778 ], 23, 0 );
    const_str_digest_9ba4feb51a956bffe5cea67a7c9377fa = UNSTREAM_STRING_ASCII( &constant_bin[ 4809801 ], 35, 0 );
    const_str_digest_663ccffe3fe7c6718cc297d815b26fd9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809836 ], 27, 0 );
    const_str_digest_c7450a192faf5ea8906fd2e9de0ebb6a = UNSTREAM_STRING_ASCII( &constant_bin[ 4809863 ], 20, 0 );
    const_str_digest_36176eb3d536f46685a6f161f51c68b6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809883 ], 37, 0 );
    const_str_digest_999f695b95bca0840444542c96ea96e7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809920 ], 37, 0 );
    const_str_digest_da8eb1012811f54209375770755941fe = UNSTREAM_STRING_ASCII( &constant_bin[ 4809762 ], 16, 0 );
    const_str_digest_8f0113edabf95528a7200f9a768b5976 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809957 ], 80, 0 );
    const_str_digest_08fea1b4c475524cd4054ba7371dc896 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810037 ], 23, 0 );
    const_str_digest_50508c7979e0691cfeb38f9cbfb209ac = UNSTREAM_STRING_ASCII( &constant_bin[ 4810060 ], 17, 0 );
    const_str_digest_ec0f046b7acb8ec2d2cc3c8de6176c30 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810077 ], 16, 0 );
    const_str_digest_dfd82adf71ab68a753acd566b6d38e20 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810093 ], 13, 0 );
    const_str_digest_2aed43cef6efc9c00dc4438856dad9fc = UNSTREAM_STRING_ASCII( &constant_bin[ 4810106 ], 26, 0 );
    const_str_digest_3e30e3b82b4323354a17341867b5d0d2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810132 ], 17, 0 );
    const_str_digest_14927f470548c9a8c459f537c4d606e3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810042 ], 18, 0 );
    const_str_digest_3bdcb6c0545f43479aa2924b95b6ba71 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810149 ], 29, 0 );
    const_str_digest_80efe96a33feb513e7b847a7bf3d0225 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810178 ], 29, 0 );
    const_str_digest_7b6588ddf1e62dbfaa95743ef0f5de28 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810207 ], 65, 0 );
    const_tuple_str_plain_self_str_plain_attrs_str_plain_color_depth_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attrs_str_plain_color_depth_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attrs_str_plain_color_depth_tuple, 1, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attrs_str_plain_color_depth_tuple, 2, const_str_plain_color_depth ); Py_INCREF( const_str_plain_color_depth );
    const_str_digest_5ebb06d0022985d2104685a594dd4ac0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810272 ], 39, 0 );
    const_str_digest_694e45dd9e5255c0bf1292133f76932a = UNSTREAM_STRING_ASCII( &constant_bin[ 4810311 ], 30, 0 );
    const_str_digest_09601a5c4deed6ab50fc30222e411887 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809809 ], 26, 0 );
    const_str_digest_1e597e02a9871accd1caf14ea21f1a48 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810341 ], 24, 0 );
    const_str_digest_202ccd6f07239f3c8cb827dba83fc76d = UNSTREAM_STRING_ASCII( &constant_bin[ 4810365 ], 18, 0 );
    const_str_digest_ac80ccef51bb506f114f80a65d36a419 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810383 ], 23, 0 );
    const_str_digest_89aa056be9c7e5ff2dc58fe6983d5187 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810406 ], 42, 0 );
    const_str_digest_1506b03c4467468b9a95d33f0f1b1dae = UNSTREAM_STRING_ASCII( &constant_bin[ 4810448 ], 28, 0 );
    const_str_digest_087dacde81663e3e0968e6783f2de514 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810476 ], 68, 0 );
    const_str_digest_6df94ed0037e26719e9d01e5a37deda6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810544 ], 28, 0 );
    const_str_digest_0b30589eee2f4e96a743b3eb380d1297 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810572 ], 21, 0 );
    const_str_digest_dcc217ed13a4a0a580b10adf5023f7bf = UNSTREAM_STRING_ASCII( &constant_bin[ 4810593 ], 11, 0 );
    const_str_digest_f647240ea52ccde05f7be0d913a94199 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810604 ], 14, 0 );
    const_str_digest_2c8594080f72870e72c65e312495d6f4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810618 ], 17, 0 );
    const_str_digest_8e5855ca7067098f9bf1bfd11fb956b2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810635 ], 28, 0 );
    const_str_digest_25c0825942f1d35f9ce4eea4f3950df7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810663 ], 28, 0 );
    const_str_digest_5d03614ccefe559d157d65485c0d1147 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810691 ], 27, 0 );
    const_str_digest_932c3300ae1e51f5120bb0994ba59b63 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810718 ], 32, 0 );
    const_str_digest_ae525420a2e70f93183cb31e5deca5ae = UNSTREAM_STRING_ASCII( &constant_bin[ 4810750 ], 34, 0 );
    const_str_digest_a0706dad627a6637adfe4d54f0a3ce20 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810696 ], 22, 0 );
    const_str_digest_120b7720def5ff816ae3417246538854 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810784 ], 35, 0 );
    const_str_digest_bd3866bba06b7d7d3fc346efdc505c82 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810819 ], 18, 0 );
    const_str_digest_af2cf307d48d74333c6ef743ee7962d0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810837 ], 26, 0 );
    const_str_digest_4ccfcaf5e2781b904b35481c559b1ac1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810863 ], 28, 0 );
    const_str_digest_48473635da0b65ba98d195c930a9ab09 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810891 ], 22, 0 );
    const_str_digest_c99a84050f4f47a8ec5fac065680ea37 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810913 ], 18, 0 );
    const_str_digest_8221d521c8d7f0d3d18c29c3c746f407 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810931 ], 18, 0 );
    const_dict_e706279e75687480bbcc20e3645fe7d7 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_e706279e75687480bbcc20e3645fe7d7, const_str_plain_rows, const_int_pos_40 );
    PyDict_SetItem( const_dict_e706279e75687480bbcc20e3645fe7d7, const_str_plain_columns, const_int_pos_80 );
    assert( PyDict_Size( const_dict_e706279e75687480bbcc20e3645fe7d7 ) == 2 );
    const_str_digest_f5a6294764d6f8fed159044754bd9b70 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810077 ], 12, 0 );
    const_str_digest_4a27625dd31ca8c93050501996bd4790 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810949 ], 44, 0 );
    const_str_digest_e09c0c910bc0d0f005ed13b2b3ba596b = UNSTREAM_STRING_ASCII( &constant_bin[ 4810993 ], 29, 0 );
    const_str_digest_4eb05cb08b804072578aa7744c547fa3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811022 ], 29, 0 );
    const_str_digest_b125201b2c3126ab088f75ee2a83d85c = UNSTREAM_STRING_ASCII( &constant_bin[ 4811051 ], 34, 0 );
    const_list_str_plain_Output_str_plain_DummyOutput_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_Output_str_plain_DummyOutput_list, 0, const_str_plain_Output ); Py_INCREF( const_str_plain_Output );
    PyList_SET_ITEM( const_list_str_plain_Output_str_plain_DummyOutput_list, 1, const_str_plain_DummyOutput ); Py_INCREF( const_str_plain_DummyOutput );
    const_str_digest_a100f5126b8ef6f34dabdc14e202fc86 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811085 ], 38, 0 );
    const_str_digest_c068aebc67d49a9ef8fdae8b481115e8 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811123 ], 16, 0 );
    const_str_digest_ecfdf1a34a369ffbdffb91a970a98862 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810388 ], 18, 0 );
    const_str_digest_fe3454b3d4da126f380099500352a60c = UNSTREAM_STRING_ASCII( &constant_bin[ 4811139 ], 32, 0 );
    const_str_digest_b631d30e30032dcfd220c349d5de8ec0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4809868 ], 15, 0 );
    const_str_digest_7300954f838f00ab5167029163f33db7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811171 ], 35, 0 );
    const_str_digest_a7c36282f7b7721404bd3bfb13fa5999 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811206 ], 68, 0 );
    const_str_digest_bb8543d14c4bb77f19d46b486e3c133b = UNSTREAM_STRING_ASCII( &constant_bin[ 4811274 ], 22, 0 );
    const_str_digest_448df5a8a02a06f6f6b7ac861804077a = UNSTREAM_STRING_ASCII( &constant_bin[ 4811296 ], 21, 0 );
    const_str_digest_130c3cf83a3960d2838f0dd2bc8e82e0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811317 ], 66, 0 );
    const_str_digest_5e89fe18c23fd245f51f1f8a85ac516d = UNSTREAM_STRING_ASCII( &constant_bin[ 4810346 ], 19, 0 );
    const_str_digest_c755db6eead683e11ef95108e352544a = UNSTREAM_STRING_ASCII( &constant_bin[ 4809783 ], 18, 0 );
    const_str_digest_f0640ff90e887fde651b4ed933f49909 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811383 ], 27, 0 );
    const_str_digest_8fa8b0d4e0fdefa7e3d6500c783eed20 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811410 ], 23, 0 );
    const_str_digest_862cd2f2b357b5c19ce21ab0b1590f2f = UNSTREAM_STRING_ASCII( &constant_bin[ 4811433 ], 16, 0 );
    const_str_digest_c6a5c2421584d9b57afbebc94bb68bdb = UNSTREAM_STRING_ASCII( &constant_bin[ 4810453 ], 23, 0 );
    const_str_digest_15082d24a9246f1671857ee111abe5b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811449 ], 13, 0 );
    const_str_digest_c8dab8d434ffb6e6e263482b2a16a42a = UNSTREAM_STRING_ASCII( &constant_bin[ 4811462 ], 17, 0 );
    const_str_digest_f7df7c19f7473d40c9bf203bb1d70fca = UNSTREAM_STRING_ASCII( &constant_bin[ 4811479 ], 26, 0 );
    const_str_digest_9324ef0a3b911702053678f3cb613d02 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811505 ], 15, 0 );
    const_str_digest_e26d5477861662fbac8ffc0b322d5cdc = UNSTREAM_STRING_ASCII( &constant_bin[ 4811520 ], 29, 0 );
    const_str_digest_06f3787b6a76fe89f837964c81cb9ebc = UNSTREAM_STRING_ASCII( &constant_bin[ 4811484 ], 21, 0 );
    const_str_digest_91db96957842275a305c52c545265ca6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811549 ], 233, 0 );
    const_str_digest_379da375eaf32853d9201cc53c888089 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811782 ], 34, 0 );
    const_str_digest_c3c5a2fabec9802e5ea49418dee11362 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811816 ], 20, 0 );
    const_str_digest_85b9f78fb96a53ae591b91df451afe81 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811836 ], 33, 0 );
    const_str_digest_30a43304a778737b27b19b13e908dd60 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811869 ], 36, 0 );
    const_str_digest_6e6723d3292a3915e476469389a3dc2a = UNSTREAM_STRING_ASCII( &constant_bin[ 4811905 ], 268, 0 );
    const_str_digest_8c7a1a827931f70b446e5c5ea05db235 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810577 ], 16, 0 );
    const_str_digest_519035211152d76f3610ed2ad2933d98 = UNSTREAM_STRING_ASCII( &constant_bin[ 4812173 ], 23, 0 );
    const_str_digest_4b09407213011b8c7b3a83571024c105 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810868 ], 23, 0 );
    const_str_digest_50012427db5e9c8ba7b6b597004b0640 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810623 ], 12, 0 );
    const_str_digest_06f36d9cde3d6031d0eacda529e99e05 = UNSTREAM_STRING_ASCII( &constant_bin[ 4810183 ], 24, 0 );
    const_str_digest_22b5e94e13f35f8f2cc9c14e3ee867b2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4812196 ], 23, 0 );
    const_str_digest_1c836df9b8160765c58053b62d719192 = UNSTREAM_STRING_ASCII( &constant_bin[ 4811176 ], 30, 0 );
    const_str_digest_6eadfa176897a37e39c912dd49464fb8 = UNSTREAM_STRING_ASCII( &constant_bin[ 4812219 ], 35, 0 );
    const_str_digest_2227bd60a6b597cbe124ec42ffd30777 = UNSTREAM_STRING_ASCII( &constant_bin[ 4812254 ], 49, 0 );
    const_str_digest_8e644be611a1a5826ad846989b1e737e = UNSTREAM_STRING_ASCII( &constant_bin[ 4812303 ], 14, 0 );
    const_str_digest_1da7ccf968632b12e682947ab1515aba = UNSTREAM_STRING_ASCII( &constant_bin[ 4812317 ], 33, 0 );
    const_str_digest_a66b99ad40c978bd4dc144639f333cea = UNSTREAM_STRING_ASCII( &constant_bin[ 4811296 ], 17, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$output$base( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_ce5c8ac4e485bd676e5ac9603cd72d48;
static PyCodeObject *codeobj_aeb434831fa251de371090a9fa391d35;
static PyCodeObject *codeobj_254abf71eb493d3132563ebed18071ae;
static PyCodeObject *codeobj_2f226eac355cf606aef01081ceef38df;
static PyCodeObject *codeobj_da0565c3135b25d7048ec66c275153cf;
static PyCodeObject *codeobj_71a68e0c9a41cc71203d728a0c3db278;
static PyCodeObject *codeobj_0953f4ac58e385cb49ef351c3c93fc97;
static PyCodeObject *codeobj_0e345f0f5cde2f53d117ca777f573b98;
static PyCodeObject *codeobj_91708eecbd6ce4489d1ce35290f0fa63;
static PyCodeObject *codeobj_081308b8613d3af05ce041ed424f834d;
static PyCodeObject *codeobj_53dc9a37fa14435b082f1d9fb7b0ff45;
static PyCodeObject *codeobj_063dc02df1e212374f45f31ae0a93b63;
static PyCodeObject *codeobj_1de0706f989498f8b17e55b9c8a11992;
static PyCodeObject *codeobj_22e6dcd3275282d8bf9d0de593a39cac;
static PyCodeObject *codeobj_1940c85c1a621c998ffbe00eb3f98aa1;
static PyCodeObject *codeobj_d7bb4a39f51a60d35813ff7f4392ea55;
static PyCodeObject *codeobj_6c4553c06a7f340d93bc9beb03717154;
static PyCodeObject *codeobj_7ad180afbf8771a730cd5d18f44bff12;
static PyCodeObject *codeobj_e7286fddfc867cb5d719b00e8e23c239;
static PyCodeObject *codeobj_f905699b74aa33358883de041b5f868d;
static PyCodeObject *codeobj_7cc86b9aec552932832d18cebfbc7eaf;
static PyCodeObject *codeobj_ec27c99c7ecb2d866940493772a61cc9;
static PyCodeObject *codeobj_75981b7dfe8f8920682687c37e55cc8a;
static PyCodeObject *codeobj_9047c2e9c0ccd6ad989a444307d1eaf4;
static PyCodeObject *codeobj_802f9f1bd8e3602b5cbf930da9c850d7;
static PyCodeObject *codeobj_f2b6ed0d575673a5bc304f40faa88b28;
static PyCodeObject *codeobj_8a57ff80973390cf781b7db372be03eb;
static PyCodeObject *codeobj_ddd9036ae7c27bdc85cbf8fb6f7136f8;
static PyCodeObject *codeobj_c29fde4d337bb7ebe2e2fb50b59a990e;
static PyCodeObject *codeobj_64be358edc659b712da7963449c610a6;
static PyCodeObject *codeobj_722efa8dcc0af26adf2b9cd4f2c7b442;
static PyCodeObject *codeobj_f0dd3aa8b481776d0ce48e5ddbc90be3;
static PyCodeObject *codeobj_1a0db61985e041004b000279b9ef5263;
static PyCodeObject *codeobj_4fac2d7c0634e7dd431d8c2318419271;
static PyCodeObject *codeobj_214813b785a7b21ffe907e08660689ef;
static PyCodeObject *codeobj_110c724191b6827541128077114f86e6;
static PyCodeObject *codeobj_9b39b5eb3203a82979f16cd7648357cf;
static PyCodeObject *codeobj_783149b9e715b4f72621c9c91877fd11;
static PyCodeObject *codeobj_2bbd0dd9c02f11ca3055e5ac99906ca1;
static PyCodeObject *codeobj_e28c131d3d0314aa51dba709988e0372;
static PyCodeObject *codeobj_3c0b842e4c618a0fd13728cf994fa0e1;
static PyCodeObject *codeobj_7d765c2d7f87d7ea863f2150d512aa12;
static PyCodeObject *codeobj_627adccb3cafa32ad4c2b2725facbda4;
static PyCodeObject *codeobj_5534ee95eb38f2df65706af040d7a5da;
static PyCodeObject *codeobj_f2b502f80bbfe476da9ae187cf7a3c8e;
static PyCodeObject *codeobj_eb24fc153eef6bdff4f10de53235c0ea;
static PyCodeObject *codeobj_9454baddcbfaa4ccdee2fba583ac097e;
static PyCodeObject *codeobj_094db91c3a04aa12da8a95867c439878;
static PyCodeObject *codeobj_460d66156a7cfb4799ea5a8aa6cde4cb;
static PyCodeObject *codeobj_b519807b206511e86971910c09efabec;
static PyCodeObject *codeobj_e95e20930cb7f6bd34ba29e35340a336;
static PyCodeObject *codeobj_52070c36916889c6dc6e419e1be290a2;
static PyCodeObject *codeobj_ea892bbfddc309596de6c410acb24e79;
static PyCodeObject *codeobj_e1efc6f05791f243920141f3b019e568;
static PyCodeObject *codeobj_0bd9b76455f0f9d5c4defa55872bd6a6;
static PyCodeObject *codeobj_0f4339db7266107da48ebfd89a2b2725;
static PyCodeObject *codeobj_a630b13a4676042ca58ca27fe73ddcf4;
static PyCodeObject *codeobj_cf8bb8cce06357af92ad09877d769f21;
static PyCodeObject *codeobj_c4a40aa1ed72d18a203196b5718163fe;
static PyCodeObject *codeobj_8b834b939c011eb2e9296ac4eccac9ef;
static PyCodeObject *codeobj_7f8842a0462859a2a3eb86059cf9cd1e;
static PyCodeObject *codeobj_6e59e6c8f03af04081b5bd944df42d75;
static PyCodeObject *codeobj_dd5fa7e784d08c49904e7005d07a7fc4;
static PyCodeObject *codeobj_74f476f50532a3f007d99f412b840692;
static PyCodeObject *codeobj_d6414fd3a2dc6648949b92d8daab578d;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_4eb05cb08b804072578aa7744c547fa3 );
    codeobj_ce5c8ac4e485bd676e5ac9603cd72d48 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_9ba4feb51a956bffe5cea67a7c9377fa, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_aeb434831fa251de371090a9fa391d35 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_DummyOutput, 156, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_254abf71eb493d3132563ebed18071ae = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Output, 15, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2f226eac355cf606aef01081ceef38df = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ask_for_cpr, 137, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_da0565c3135b25d7048ec66c275153cf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ask_for_cpr, 190, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_71a68e0c9a41cc71203d728a0c3db278 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_bell, 143, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0953f4ac58e385cb49ef351c3c93fc97 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_bell, 191, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0e345f0f5cde2f53d117ca777f573b98 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_clear_title, 49, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_91708eecbd6ce4489d1ce35290f0fa63 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_clear_title, 170, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_081308b8613d3af05ce041ed424f834d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_backward, 125, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_53dc9a37fa14435b082f1d9fb7b0ff45 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_backward, 187, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_063dc02df1e212374f45f31ae0a93b63 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_down, 117, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1de0706f989498f8b17e55b9c8a11992 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_down, 185, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_22e6dcd3275282d8bf9d0de593a39cac = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_forward, 121, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1940c85c1a621c998ffbe00eb3f98aa1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_forward, 186, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d7bb4a39f51a60d35813ff7f4392ea55 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_goto, 109, const_tuple_str_plain_self_str_plain_row_str_plain_column_tuple, 3, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6c4553c06a7f340d93bc9beb03717154 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_goto, 183, const_tuple_str_plain_self_str_plain_row_str_plain_column_tuple, 3, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7ad180afbf8771a730cd5d18f44bff12 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_up, 113, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e7286fddfc867cb5d719b00e8e23c239 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cursor_up, 184, const_tuple_str_plain_self_str_plain_amount_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f905699b74aa33358883de041b5f868d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_disable_autowrap, 101, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7cc86b9aec552932832d18cebfbc7eaf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_disable_autowrap, 181, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ec27c99c7ecb2d866940493772a61cc9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_disable_bracketed_paste, 149, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_75981b7dfe8f8920682687c37e55cc8a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_disable_bracketed_paste, 193, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9047c2e9c0ccd6ad989a444307d1eaf4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_disable_mouse_support, 76, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_802f9f1bd8e3602b5cbf930da9c850d7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_disable_mouse_support, 176, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f2b6ed0d575673a5bc304f40faa88b28 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enable_autowrap, 105, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8a57ff80973390cf781b7db372be03eb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enable_autowrap, 182, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ddd9036ae7c27bdc85cbf8fb6f7136f8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enable_bracketed_paste, 146, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c29fde4d337bb7ebe2e2fb50b59a990e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enable_bracketed_paste, 192, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_64be358edc659b712da7963449c610a6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enable_mouse_support, 72, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_722efa8dcc0af26adf2b9cd4f2c7b442 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enable_mouse_support, 175, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f0dd3aa8b481776d0ce48e5ddbc90be3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_encoding, 28, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1a0db61985e041004b000279b9ef5263 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_encoding, 164, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4fac2d7c0634e7dd431d8c2318419271 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enter_alternate_screen, 64, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_214813b785a7b21ffe907e08660689ef = MAKE_CODEOBJ( module_filename_obj, const_str_plain_enter_alternate_screen, 173, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_110c724191b6827541128077114f86e6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_erase_down, 86, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9b39b5eb3203a82979f16cd7648357cf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_erase_down, 178, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_783149b9e715b4f72621c9c91877fd11 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_erase_end_of_line, 80, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2bbd0dd9c02f11ca3055e5ac99906ca1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_erase_end_of_line, 177, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e28c131d3d0314aa51dba709988e0372 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_erase_screen, 57, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3c0b842e4c618a0fd13728cf994fa0e1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_erase_screen, 172, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7d765c2d7f87d7ea863f2150d512aa12 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fileno, 24, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_627adccb3cafa32ad4c2b2725facbda4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fileno, 160, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5534ee95eb38f2df65706af040d7a5da = MAKE_CODEOBJ( module_filename_obj, const_str_plain_flush, 53, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f2b502f80bbfe476da9ae187cf7a3c8e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_flush, 171, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_eb24fc153eef6bdff4f10de53235c0ea = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_rows_below_cursor_position, 199, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9454baddcbfaa4ccdee2fba583ac097e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_size, 196, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_094db91c3a04aa12da8a95867c439878 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_hide_cursor, 129, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_460d66156a7cfb4799ea5a8aa6cde4cb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_hide_cursor, 188, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b519807b206511e86971910c09efabec = MAKE_CODEOBJ( module_filename_obj, const_str_plain_quit_alternate_screen, 68, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e95e20930cb7f6bd34ba29e35340a336 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_quit_alternate_screen, 174, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_52070c36916889c6dc6e419e1be290a2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reset_attributes, 93, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ea892bbfddc309596de6c410acb24e79 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reset_attributes, 179, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e1efc6f05791f243920141f3b019e568 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_buffer_to_prompt, 152, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0bd9b76455f0f9d5c4defa55872bd6a6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_buffer_to_prompt, 194, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0f4339db7266107da48ebfd89a2b2725 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_attributes, 97, const_tuple_str_plain_self_str_plain_attrs_str_plain_color_depth_tuple, 3, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a630b13a4676042ca58ca27fe73ddcf4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_attributes, 180, const_tuple_str_plain_self_str_plain_attrs_str_plain_color_depth_tuple, 3, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_cf8bb8cce06357af92ad09877d769f21 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_title, 45, const_tuple_str_plain_self_str_plain_title_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c4a40aa1ed72d18a203196b5718163fe = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_title, 169, const_tuple_str_plain_self_str_plain_title_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8b834b939c011eb2e9296ac4eccac9ef = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show_cursor, 133, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7f8842a0462859a2a3eb86059cf9cd1e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show_cursor, 189, const_tuple_str_plain_self_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6e59e6c8f03af04081b5bd944df42d75 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write, 37, const_tuple_str_plain_self_str_plain_data_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dd5fa7e784d08c49904e7005d07a7fc4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write, 167, const_tuple_str_plain_self_str_plain_data_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_74f476f50532a3f007d99f412b840692 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write_raw, 41, const_tuple_str_plain_self_str_plain_data_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d6414fd3a2dc6648949b92d8daab578d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_write_raw, 168, const_tuple_str_plain_self_str_plain_data_tuple, 2, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_10_quit_alternate_screen(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_11_enable_mouse_support(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_12_disable_mouse_support(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_13_erase_end_of_line(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_14_erase_down(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_15_reset_attributes(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_16_set_attributes(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_17_disable_autowrap(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_18_enable_autowrap(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_19_cursor_goto( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_1_fileno(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_20_cursor_up(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_21_cursor_down(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_22_cursor_forward(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_23_cursor_backward(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_24_hide_cursor(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_25_show_cursor(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_26_ask_for_cpr(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_27_bell(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_28_enable_bracketed_paste(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_29_disable_bracketed_paste(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_2_encoding(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_30_scroll_buffer_to_prompt(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_31_fileno(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_32_encoding(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_33_write(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_34_write_raw(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_35_set_title(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_36_clear_title(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_37_flush(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_38_erase_screen(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_39_enter_alternate_screen(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_3_write(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_40_quit_alternate_screen(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_41_enable_mouse_support(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_42_disable_mouse_support(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_43_erase_end_of_line(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_44_erase_down(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_45_reset_attributes(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_46_set_attributes(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_47_disable_autowrap(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_48_enable_autowrap(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_49_cursor_goto( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_4_write_raw(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_50_cursor_up(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_51_cursor_down(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_52_cursor_forward(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_53_cursor_backward(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_54_hide_cursor(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_55_show_cursor(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_56_ask_for_cpr(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_57_bell(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_58_enable_bracketed_paste(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_59_disable_bracketed_paste(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_5_set_title(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_60_scroll_buffer_to_prompt(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_61_get_size(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_62_get_rows_below_cursor_position(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_6_clear_title(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_7_flush(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_8_erase_screen(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_9_enter_alternate_screen(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$output$base$$$function_1_fileno( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_1_fileno );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_1_fileno );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_2_encoding( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_2_encoding );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_2_encoding );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_3_write( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_data = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_3_write );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_3_write );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_4_write_raw( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_data = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_4_write_raw );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_4_write_raw );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_5_set_title( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_title = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_5_set_title );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_5_set_title );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_6_clear_title( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_6_clear_title );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_6_clear_title );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_7_flush( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_7_flush );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_7_flush );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_8_erase_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_8_erase_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_8_erase_screen );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_9_enter_alternate_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_9_enter_alternate_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_9_enter_alternate_screen );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_10_quit_alternate_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_10_quit_alternate_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_10_quit_alternate_screen );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_11_enable_mouse_support( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_11_enable_mouse_support );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_11_enable_mouse_support );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_12_disable_mouse_support( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_12_disable_mouse_support );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_12_disable_mouse_support );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_13_erase_end_of_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_13_erase_end_of_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_13_erase_end_of_line );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_14_erase_down( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_14_erase_down );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_14_erase_down );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_15_reset_attributes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_15_reset_attributes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_15_reset_attributes );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_16_set_attributes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attrs = python_pars[ 1 ];
    PyObject *par_color_depth = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_16_set_attributes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    CHECK_OBJECT( (PyObject *)par_color_depth );
    Py_DECREF( par_color_depth );
    par_color_depth = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    CHECK_OBJECT( (PyObject *)par_color_depth );
    Py_DECREF( par_color_depth );
    par_color_depth = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_16_set_attributes );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_17_disable_autowrap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_17_disable_autowrap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_17_disable_autowrap );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_18_enable_autowrap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_18_enable_autowrap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_18_enable_autowrap );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_19_cursor_goto( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_row = python_pars[ 1 ];
    PyObject *par_column = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_19_cursor_goto );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_row );
    Py_DECREF( par_row );
    par_row = NULL;

    CHECK_OBJECT( (PyObject *)par_column );
    Py_DECREF( par_column );
    par_column = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_row );
    Py_DECREF( par_row );
    par_row = NULL;

    CHECK_OBJECT( (PyObject *)par_column );
    Py_DECREF( par_column );
    par_column = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_19_cursor_goto );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_20_cursor_up( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_20_cursor_up );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_20_cursor_up );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_21_cursor_down( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_21_cursor_down );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_21_cursor_down );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_22_cursor_forward( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_22_cursor_forward );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_22_cursor_forward );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_23_cursor_backward( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_23_cursor_backward );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_23_cursor_backward );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_24_hide_cursor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_24_hide_cursor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_24_hide_cursor );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_25_show_cursor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_25_show_cursor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_25_show_cursor );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_26_ask_for_cpr( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_26_ask_for_cpr );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_26_ask_for_cpr );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_27_bell( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_27_bell );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_27_bell );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_28_enable_bracketed_paste( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_28_enable_bracketed_paste );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_28_enable_bracketed_paste );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_29_disable_bracketed_paste( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_29_disable_bracketed_paste );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_29_disable_bracketed_paste );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_30_scroll_buffer_to_prompt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_30_scroll_buffer_to_prompt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_30_scroll_buffer_to_prompt );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_31_fileno( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_627adccb3cafa32ad4c2b2725facbda4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_627adccb3cafa32ad4c2b2725facbda4 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_627adccb3cafa32ad4c2b2725facbda4, codeobj_627adccb3cafa32ad4c2b2725facbda4, module_prompt_toolkit$output$base, sizeof(void *) );
    frame_627adccb3cafa32ad4c2b2725facbda4 = cache_frame_627adccb3cafa32ad4c2b2725facbda4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_627adccb3cafa32ad4c2b2725facbda4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_627adccb3cafa32ad4c2b2725facbda4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        tmp_raise_type_1 = PyExc_NotImplementedError;
        exception_type = tmp_raise_type_1;
        Py_INCREF( tmp_raise_type_1 );
        exception_lineno = 162;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_627adccb3cafa32ad4c2b2725facbda4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_627adccb3cafa32ad4c2b2725facbda4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_627adccb3cafa32ad4c2b2725facbda4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_627adccb3cafa32ad4c2b2725facbda4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_627adccb3cafa32ad4c2b2725facbda4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_627adccb3cafa32ad4c2b2725facbda4,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_627adccb3cafa32ad4c2b2725facbda4 == cache_frame_627adccb3cafa32ad4c2b2725facbda4 )
    {
        Py_DECREF( frame_627adccb3cafa32ad4c2b2725facbda4 );
    }
    cache_frame_627adccb3cafa32ad4c2b2725facbda4 = NULL;

    assertFrameObject( frame_627adccb3cafa32ad4c2b2725facbda4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_31_fileno );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_31_fileno );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_prompt_toolkit$output$base$$$function_32_encoding( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_str_digest_c075052d723d6707083e869a0e3659bb;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_32_encoding );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_32_encoding );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_33_write( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_data = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_33_write );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_33_write );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_34_write_raw( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_data = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_34_write_raw );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_34_write_raw );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_35_set_title( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_title = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_35_set_title );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_35_set_title );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_36_clear_title( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_36_clear_title );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_36_clear_title );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_37_flush( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_37_flush );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_37_flush );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_38_erase_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_38_erase_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_38_erase_screen );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_39_enter_alternate_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_39_enter_alternate_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_39_enter_alternate_screen );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_40_quit_alternate_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_40_quit_alternate_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_40_quit_alternate_screen );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_41_enable_mouse_support( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_41_enable_mouse_support );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_41_enable_mouse_support );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_42_disable_mouse_support( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_42_disable_mouse_support );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_42_disable_mouse_support );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_43_erase_end_of_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_43_erase_end_of_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_43_erase_end_of_line );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_44_erase_down( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_44_erase_down );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_44_erase_down );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_45_reset_attributes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_45_reset_attributes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_45_reset_attributes );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_46_set_attributes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attrs = python_pars[ 1 ];
    PyObject *par_color_depth = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_46_set_attributes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    CHECK_OBJECT( (PyObject *)par_color_depth );
    Py_DECREF( par_color_depth );
    par_color_depth = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    CHECK_OBJECT( (PyObject *)par_color_depth );
    Py_DECREF( par_color_depth );
    par_color_depth = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_46_set_attributes );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_47_disable_autowrap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_47_disable_autowrap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_47_disable_autowrap );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_48_enable_autowrap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_48_enable_autowrap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_48_enable_autowrap );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_49_cursor_goto( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_row = python_pars[ 1 ];
    PyObject *par_column = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_49_cursor_goto );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_row );
    Py_DECREF( par_row );
    par_row = NULL;

    CHECK_OBJECT( (PyObject *)par_column );
    Py_DECREF( par_column );
    par_column = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_row );
    Py_DECREF( par_row );
    par_row = NULL;

    CHECK_OBJECT( (PyObject *)par_column );
    Py_DECREF( par_column );
    par_column = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_49_cursor_goto );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_50_cursor_up( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_50_cursor_up );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_50_cursor_up );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_51_cursor_down( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_51_cursor_down );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_51_cursor_down );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_52_cursor_forward( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_52_cursor_forward );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_52_cursor_forward );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_53_cursor_backward( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_amount = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_53_cursor_backward );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_amount );
    Py_DECREF( par_amount );
    par_amount = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_53_cursor_backward );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_54_hide_cursor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_54_hide_cursor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_54_hide_cursor );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_55_show_cursor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_55_show_cursor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_55_show_cursor );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_56_ask_for_cpr( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_56_ask_for_cpr );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_56_ask_for_cpr );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_57_bell( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_57_bell );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_57_bell );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_58_enable_bracketed_paste( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_58_enable_bracketed_paste );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_58_enable_bracketed_paste );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_59_disable_bracketed_paste( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_59_disable_bracketed_paste );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_59_disable_bracketed_paste );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_60_scroll_buffer_to_prompt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_60_scroll_buffer_to_prompt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_60_scroll_buffer_to_prompt );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_61_get_size( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9454baddcbfaa4ccdee2fba583ac097e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9454baddcbfaa4ccdee2fba583ac097e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9454baddcbfaa4ccdee2fba583ac097e, codeobj_9454baddcbfaa4ccdee2fba583ac097e, module_prompt_toolkit$output$base, sizeof(void *) );
    frame_9454baddcbfaa4ccdee2fba583ac097e = cache_frame_9454baddcbfaa4ccdee2fba583ac097e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9454baddcbfaa4ccdee2fba583ac097e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9454baddcbfaa4ccdee2fba583ac097e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_Size );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Size );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Size" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_kw_name_1 = PyDict_Copy( const_dict_e706279e75687480bbcc20e3645fe7d7 );
        frame_9454baddcbfaa4ccdee2fba583ac097e->m_frame.f_lineno = 197;
        tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9454baddcbfaa4ccdee2fba583ac097e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9454baddcbfaa4ccdee2fba583ac097e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9454baddcbfaa4ccdee2fba583ac097e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9454baddcbfaa4ccdee2fba583ac097e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9454baddcbfaa4ccdee2fba583ac097e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9454baddcbfaa4ccdee2fba583ac097e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9454baddcbfaa4ccdee2fba583ac097e,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_9454baddcbfaa4ccdee2fba583ac097e == cache_frame_9454baddcbfaa4ccdee2fba583ac097e )
    {
        Py_DECREF( frame_9454baddcbfaa4ccdee2fba583ac097e );
    }
    cache_frame_9454baddcbfaa4ccdee2fba583ac097e = NULL;

    assertFrameObject( frame_9454baddcbfaa4ccdee2fba583ac097e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_61_get_size );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_61_get_size );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$output$base$$$function_62_get_rows_below_cursor_position( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_int_pos_40;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_62_get_rows_below_cursor_position );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base$$$function_62_get_rows_below_cursor_position );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_10_quit_alternate_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_10_quit_alternate_screen,
        const_str_plain_quit_alternate_screen,
#if PYTHON_VERSION >= 300
        const_str_digest_6df94ed0037e26719e9d01e5a37deda6,
#endif
        codeobj_b519807b206511e86971910c09efabec,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_30a43304a778737b27b19b13e908dd60,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_11_enable_mouse_support(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_11_enable_mouse_support,
        const_str_plain_enable_mouse_support,
#if PYTHON_VERSION >= 300
        const_str_digest_663ccffe3fe7c6718cc297d815b26fd9,
#endif
        codeobj_64be358edc659b712da7963449c610a6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_9324ef0a3b911702053678f3cb613d02,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_12_disable_mouse_support(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_12_disable_mouse_support,
        const_str_plain_disable_mouse_support,
#if PYTHON_VERSION >= 300
        const_str_digest_8e5855ca7067098f9bf1bfd11fb956b2,
#endif
        codeobj_9047c2e9c0ccd6ad989a444307d1eaf4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_862cd2f2b357b5c19ce21ab0b1590f2f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_13_erase_end_of_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_13_erase_end_of_line,
        const_str_plain_erase_end_of_line,
#if PYTHON_VERSION >= 300
        const_str_digest_06f36d9cde3d6031d0eacda529e99e05,
#endif
        codeobj_783149b9e715b4f72621c9c91877fd11,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_c1a89acdc0ab4f2bdb72558eadf3a14a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_14_erase_down(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_14_erase_down,
        const_str_plain_erase_down,
#if PYTHON_VERSION >= 300
        const_str_digest_3e30e3b82b4323354a17341867b5d0d2,
#endif
        codeobj_110c724191b6827541128077114f86e6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_96d4b7eba020383f6f6d437d0ddb1b2d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_15_reset_attributes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_15_reset_attributes,
        const_str_plain_reset_attributes,
#if PYTHON_VERSION >= 300
        const_str_digest_c6a5c2421584d9b57afbebc94bb68bdb,
#endif
        codeobj_52070c36916889c6dc6e419e1be290a2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_36176eb3d536f46685a6f161f51c68b6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_16_set_attributes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_16_set_attributes,
        const_str_plain_set_attributes,
#if PYTHON_VERSION >= 300
        const_str_digest_06f3787b6a76fe89f837964c81cb9ebc,
#endif
        codeobj_0f4339db7266107da48ebfd89a2b2725,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_5ebb06d0022985d2104685a594dd4ac0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_17_disable_autowrap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_17_disable_autowrap,
        const_str_plain_disable_autowrap,
#if PYTHON_VERSION >= 300
        const_str_digest_4b09407213011b8c7b3a83571024c105,
#endif
        codeobj_f905699b74aa33358883de041b5f868d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_3bdcb6c0545f43479aa2924b95b6ba71,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_18_enable_autowrap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_18_enable_autowrap,
        const_str_plain_enable_autowrap,
#if PYTHON_VERSION >= 300
        const_str_digest_bb8543d14c4bb77f19d46b486e3c133b,
#endif
        codeobj_f2b6ed0d575673a5bc304f40faa88b28,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_25c0825942f1d35f9ce4eea4f3950df7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_19_cursor_goto( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_19_cursor_goto,
        const_str_plain_cursor_goto,
#if PYTHON_VERSION >= 300
        const_str_digest_8221d521c8d7f0d3d18c29c3c746f407,
#endif
        codeobj_d7bb4a39f51a60d35813ff7f4392ea55,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_66d43be44abed6e83a28713fad8e1523,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_1_fileno(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_1_fileno,
        const_str_plain_fileno,
#if PYTHON_VERSION >= 300
        const_str_digest_dfd82adf71ab68a753acd566b6d38e20,
#endif
        codeobj_7d765c2d7f87d7ea863f2150d512aa12,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_130c3cf83a3960d2838f0dd2bc8e82e0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_20_cursor_up(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_20_cursor_up,
        const_str_plain_cursor_up,
#if PYTHON_VERSION >= 300
        const_str_digest_da8eb1012811f54209375770755941fe,
#endif
        codeobj_7ad180afbf8771a730cd5d18f44bff12,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_932c3300ae1e51f5120bb0994ba59b63,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_21_cursor_down(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_21_cursor_down,
        const_str_plain_cursor_down,
#if PYTHON_VERSION >= 300
        const_str_digest_bd3866bba06b7d7d3fc346efdc505c82,
#endif
        codeobj_063dc02df1e212374f45f31ae0a93b63,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_ae525420a2e70f93183cb31e5deca5ae,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_22_cursor_forward(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_22_cursor_forward,
        const_str_plain_cursor_forward,
#if PYTHON_VERSION >= 300
        const_str_digest_dec178e9f07b8197db43bd257822dc51,
#endif
        codeobj_22e6dcd3275282d8bf9d0de593a39cac,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_999f695b95bca0840444542c96ea96e7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_23_cursor_backward(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_23_cursor_backward,
        const_str_plain_cursor_backward,
#if PYTHON_VERSION >= 300
        const_str_digest_a0706dad627a6637adfe4d54f0a3ce20,
#endif
        codeobj_081308b8613d3af05ce041ed424f834d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_a100f5126b8ef6f34dabdc14e202fc86,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_24_hide_cursor(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_24_hide_cursor,
        const_str_plain_hide_cursor,
#if PYTHON_VERSION >= 300
        const_str_digest_14927f470548c9a8c459f537c4d606e3,
#endif
        codeobj_094db91c3a04aa12da8a95867c439878,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_8e644be611a1a5826ad846989b1e737e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_25_show_cursor(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_25_show_cursor,
        const_str_plain_show_cursor,
#if PYTHON_VERSION >= 300
        const_str_digest_c99a84050f4f47a8ec5fac065680ea37,
#endif
        codeobj_8b834b939c011eb2e9296ac4eccac9ef,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_f647240ea52ccde05f7be0d913a94199,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_26_ask_for_cpr(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_26_ask_for_cpr,
        const_str_plain_ask_for_cpr,
#if PYTHON_VERSION >= 300
        const_str_digest_ecfdf1a34a369ffbdffb91a970a98862,
#endif
        codeobj_2f226eac355cf606aef01081ceef38df,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_8f0113edabf95528a7200f9a768b5976,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_27_bell(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_27_bell,
        const_str_plain_bell,
#if PYTHON_VERSION >= 300
        const_str_digest_dcc217ed13a4a0a580b10adf5023f7bf,
#endif
        codeobj_71a68e0c9a41cc71203d728a0c3db278,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_afb1dc9d409deb3b7fe4024e073b84b2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_28_enable_bracketed_paste(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_28_enable_bracketed_paste,
        const_str_plain_enable_bracketed_paste,
#if PYTHON_VERSION >= 300
        const_str_digest_e09c0c910bc0d0f005ed13b2b3ba596b,
#endif
        codeobj_ddd9036ae7c27bdc85cbf8fb6f7136f8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_c8dab8d434ffb6e6e263482b2a16a42a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_29_disable_bracketed_paste(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_29_disable_bracketed_paste,
        const_str_plain_disable_bracketed_paste,
#if PYTHON_VERSION >= 300
        const_str_digest_1c836df9b8160765c58053b62d719192,
#endif
        codeobj_ec27c99c7ecb2d866940493772a61cc9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_c8dab8d434ffb6e6e263482b2a16a42a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_2_encoding(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_2_encoding,
        const_str_plain_encoding,
#if PYTHON_VERSION >= 300
        const_str_digest_b631d30e30032dcfd220c349d5de8ec0,
#endif
        codeobj_f0dd3aa8b481776d0ce48e5ddbc90be3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_91db96957842275a305c52c545265ca6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_30_scroll_buffer_to_prompt(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_30_scroll_buffer_to_prompt,
        const_str_plain_scroll_buffer_to_prompt,
#if PYTHON_VERSION >= 300
        const_str_digest_694e45dd9e5255c0bf1292133f76932a,
#endif
        codeobj_e1efc6f05791f243920141f3b019e568,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_50508c7979e0691cfeb38f9cbfb209ac,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_31_fileno(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_31_fileno,
        const_str_plain_fileno,
#if PYTHON_VERSION >= 300
        const_str_digest_202ccd6f07239f3c8cb827dba83fc76d,
#endif
        codeobj_627adccb3cafa32ad4c2b2725facbda4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_4a27625dd31ca8c93050501996bd4790,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_32_encoding(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_32_encoding,
        const_str_plain_encoding,
#if PYTHON_VERSION >= 300
        const_str_digest_c7450a192faf5ea8906fd2e9de0ebb6a,
#endif
        codeobj_1a0db61985e041004b000279b9ef5263,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_33_write(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_33_write,
        const_str_plain_write,
#if PYTHON_VERSION >= 300
        const_str_digest_a66b99ad40c978bd4dc144639f333cea,
#endif
        codeobj_dd5fa7e784d08c49904e7005d07a7fc4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_34_write_raw(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_34_write_raw,
        const_str_plain_write_raw,
#if PYTHON_VERSION >= 300
        const_str_digest_448df5a8a02a06f6f6b7ac861804077a,
#endif
        codeobj_d6414fd3a2dc6648949b92d8daab578d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_35_set_title(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_35_set_title,
        const_str_plain_set_title,
#if PYTHON_VERSION >= 300
        const_str_digest_0b30589eee2f4e96a743b3eb380d1297,
#endif
        codeobj_c4a40aa1ed72d18a203196b5718163fe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_36_clear_title(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_36_clear_title,
        const_str_plain_clear_title,
#if PYTHON_VERSION >= 300
        const_str_digest_14fb97c1142e2dcf0d4a21ead83ae1d4,
#endif
        codeobj_91708eecbd6ce4489d1ce35290f0fa63,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_37_flush(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_37_flush,
        const_str_plain_flush,
#if PYTHON_VERSION >= 300
        const_str_digest_2c8594080f72870e72c65e312495d6f4,
#endif
        codeobj_f2b502f80bbfe476da9ae187cf7a3c8e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_38_erase_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_38_erase_screen,
        const_str_plain_erase_screen,
#if PYTHON_VERSION >= 300
        const_str_digest_1e597e02a9871accd1caf14ea21f1a48,
#endif
        codeobj_3c0b842e4c618a0fd13728cf994fa0e1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_39_enter_alternate_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_39_enter_alternate_screen,
        const_str_plain_enter_alternate_screen,
#if PYTHON_VERSION >= 300
        const_str_digest_379da375eaf32853d9201cc53c888089,
#endif
        codeobj_214813b785a7b21ffe907e08660689ef,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_3_write(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_3_write,
        const_str_plain_write,
#if PYTHON_VERSION >= 300
        const_str_digest_f5a6294764d6f8fed159044754bd9b70,
#endif
        codeobj_6e59e6c8f03af04081b5bd944df42d75,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_7b6588ddf1e62dbfaa95743ef0f5de28,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_40_quit_alternate_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_40_quit_alternate_screen,
        const_str_plain_quit_alternate_screen,
#if PYTHON_VERSION >= 300
        const_str_digest_85b9f78fb96a53ae591b91df451afe81,
#endif
        codeobj_e95e20930cb7f6bd34ba29e35340a336,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_41_enable_mouse_support(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_41_enable_mouse_support,
        const_str_plain_enable_mouse_support,
#if PYTHON_VERSION >= 300
        const_str_digest_fe3454b3d4da126f380099500352a60c,
#endif
        codeobj_722efa8dcc0af26adf2b9cd4f2c7b442,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_42_disable_mouse_support(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_42_disable_mouse_support,
        const_str_plain_disable_mouse_support,
#if PYTHON_VERSION >= 300
        const_str_digest_1da7ccf968632b12e682947ab1515aba,
#endif
        codeobj_802f9f1bd8e3602b5cbf930da9c850d7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_43_erase_end_of_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_43_erase_end_of_line,
        const_str_plain_erase_end_of_line,
#if PYTHON_VERSION >= 300
        const_str_digest_80efe96a33feb513e7b847a7bf3d0225,
#endif
        codeobj_2bbd0dd9c02f11ca3055e5ac99906ca1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_44_erase_down(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_44_erase_down,
        const_str_plain_erase_down,
#if PYTHON_VERSION >= 300
        const_str_digest_48473635da0b65ba98d195c930a9ab09,
#endif
        codeobj_9b39b5eb3203a82979f16cd7648357cf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_45_reset_attributes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_45_reset_attributes,
        const_str_plain_reset_attributes,
#if PYTHON_VERSION >= 300
        const_str_digest_1506b03c4467468b9a95d33f0f1b1dae,
#endif
        codeobj_ea892bbfddc309596de6c410acb24e79,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_46_set_attributes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_46_set_attributes,
        const_str_plain_set_attributes,
#if PYTHON_VERSION >= 300
        const_str_digest_f7df7c19f7473d40c9bf203bb1d70fca,
#endif
        codeobj_a630b13a4676042ca58ca27fe73ddcf4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_47_disable_autowrap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_47_disable_autowrap,
        const_str_plain_disable_autowrap,
#if PYTHON_VERSION >= 300
        const_str_digest_4ccfcaf5e2781b904b35481c559b1ac1,
#endif
        codeobj_7cc86b9aec552932832d18cebfbc7eaf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_48_enable_autowrap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_48_enable_autowrap,
        const_str_plain_enable_autowrap,
#if PYTHON_VERSION >= 300
        const_str_digest_f0640ff90e887fde651b4ed933f49909,
#endif
        codeobj_8a57ff80973390cf781b7db372be03eb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_49_cursor_goto( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_49_cursor_goto,
        const_str_plain_cursor_goto,
#if PYTHON_VERSION >= 300
        const_str_digest_8fa8b0d4e0fdefa7e3d6500c783eed20,
#endif
        codeobj_6c4553c06a7f340d93bc9beb03717154,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_4_write_raw(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_4_write_raw,
        const_str_plain_write_raw,
#if PYTHON_VERSION >= 300
        const_str_digest_ec0f046b7acb8ec2d2cc3c8de6176c30,
#endif
        codeobj_74f476f50532a3f007d99f412b840692,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_15082d24a9246f1671857ee111abe5b0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_50_cursor_up(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_50_cursor_up,
        const_str_plain_cursor_up,
#if PYTHON_VERSION >= 300
        const_str_digest_c768843ed10151a01402694b3d1d371c,
#endif
        codeobj_e7286fddfc867cb5d719b00e8e23c239,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_51_cursor_down(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_51_cursor_down,
        const_str_plain_cursor_down,
#if PYTHON_VERSION >= 300
        const_str_digest_22b5e94e13f35f8f2cc9c14e3ee867b2,
#endif
        codeobj_1de0706f989498f8b17e55b9c8a11992,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_52_cursor_forward(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_52_cursor_forward,
        const_str_plain_cursor_forward,
#if PYTHON_VERSION >= 300
        const_str_digest_2aed43cef6efc9c00dc4438856dad9fc,
#endif
        codeobj_1940c85c1a621c998ffbe00eb3f98aa1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_53_cursor_backward(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_53_cursor_backward,
        const_str_plain_cursor_backward,
#if PYTHON_VERSION >= 300
        const_str_digest_5d03614ccefe559d157d65485c0d1147,
#endif
        codeobj_53dc9a37fa14435b082f1d9fb7b0ff45,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_54_hide_cursor(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_54_hide_cursor,
        const_str_plain_hide_cursor,
#if PYTHON_VERSION >= 300
        const_str_digest_08fea1b4c475524cd4054ba7371dc896,
#endif
        codeobj_460d66156a7cfb4799ea5a8aa6cde4cb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_55_show_cursor(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_55_show_cursor,
        const_str_plain_show_cursor,
#if PYTHON_VERSION >= 300
        const_str_digest_519035211152d76f3610ed2ad2933d98,
#endif
        codeobj_7f8842a0462859a2a3eb86059cf9cd1e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_56_ask_for_cpr(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_56_ask_for_cpr,
        const_str_plain_ask_for_cpr,
#if PYTHON_VERSION >= 300
        const_str_digest_ac80ccef51bb506f114f80a65d36a419,
#endif
        codeobj_da0565c3135b25d7048ec66c275153cf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_57_bell(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_57_bell,
        const_str_plain_bell,
#if PYTHON_VERSION >= 300
        const_str_digest_c068aebc67d49a9ef8fdae8b481115e8,
#endif
        codeobj_0953f4ac58e385cb49ef351c3c93fc97,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_58_enable_bracketed_paste(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_58_enable_bracketed_paste,
        const_str_plain_enable_bracketed_paste,
#if PYTHON_VERSION >= 300
        const_str_digest_b125201b2c3126ab088f75ee2a83d85c,
#endif
        codeobj_c29fde4d337bb7ebe2e2fb50b59a990e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_59_disable_bracketed_paste(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_59_disable_bracketed_paste,
        const_str_plain_disable_bracketed_paste,
#if PYTHON_VERSION >= 300
        const_str_digest_7300954f838f00ab5167029163f33db7,
#endif
        codeobj_75981b7dfe8f8920682687c37e55cc8a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_5_set_title(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_5_set_title,
        const_str_plain_set_title,
#if PYTHON_VERSION >= 300
        const_str_digest_8c7a1a827931f70b446e5c5ea05db235,
#endif
        codeobj_cf8bb8cce06357af92ad09877d769f21,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_7829343aeb8baaea9dd1d74962f7fce5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_60_scroll_buffer_to_prompt(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_60_scroll_buffer_to_prompt,
        const_str_plain_scroll_buffer_to_prompt,
#if PYTHON_VERSION >= 300
        const_str_digest_6eadfa176897a37e39c912dd49464fb8,
#endif
        codeobj_0bd9b76455f0f9d5c4defa55872bd6a6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_61_get_size(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_61_get_size,
        const_str_plain_get_size,
#if PYTHON_VERSION >= 300
        const_str_digest_c3c5a2fabec9802e5ea49418dee11362,
#endif
        codeobj_9454baddcbfaa4ccdee2fba583ac097e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_62_get_rows_below_cursor_position(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_62_get_rows_below_cursor_position,
        const_str_plain_get_rows_below_cursor_position,
#if PYTHON_VERSION >= 300
        const_str_digest_89aa056be9c7e5ff2dc58fe6983d5187,
#endif
        codeobj_eb24fc153eef6bdff4f10de53235c0ea,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_6_clear_title(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_6_clear_title,
        const_str_plain_clear_title,
#if PYTHON_VERSION >= 300
        const_str_digest_c755db6eead683e11ef95108e352544a,
#endif
        codeobj_0e345f0f5cde2f53d117ca777f573b98,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_2227bd60a6b597cbe124ec42ffd30777,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_7_flush(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_7_flush,
        const_str_plain_flush,
#if PYTHON_VERSION >= 300
        const_str_digest_50012427db5e9c8ba7b6b597004b0640,
#endif
        codeobj_5534ee95eb38f2df65706af040d7a5da,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_120b7720def5ff816ae3417246538854,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_8_erase_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_8_erase_screen,
        const_str_plain_erase_screen,
#if PYTHON_VERSION >= 300
        const_str_digest_5e89fe18c23fd245f51f1f8a85ac516d,
#endif
        codeobj_e28c131d3d0314aa51dba709988e0372,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_3c06bc9b326bb91a7e78926c176ec572,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$output$base$$$function_9_enter_alternate_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$output$base$$$function_9_enter_alternate_screen,
        const_str_plain_enter_alternate_screen,
#if PYTHON_VERSION >= 300
        const_str_digest_e26d5477861662fbac8ffc0b322d5cdc,
#endif
        codeobj_4fac2d7c0634e7dd431d8c2318419271,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$output$base,
        const_str_digest_087dacde81663e3e0968e6783f2de514,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$output$base =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.output.base",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$output$base)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$output$base)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$output$base );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.output.base: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.output.base: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.output.base: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$output$base" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$output$base = Py_InitModule4(
        "prompt_toolkit.output.base",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$output$base = PyModule_Create( &mdef_prompt_toolkit$output$base );
#endif

    moduledict_prompt_toolkit$output$base = MODULE_DICT( module_prompt_toolkit$output$base );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$output$base,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$output$base,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$output$base,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$output$base,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$output$base );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_09601a5c4deed6ab50fc30222e411887, module_prompt_toolkit$output$base );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_ce5c8ac4e485bd676e5ac9603cd72d48;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_prompt_toolkit$output$base_15 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_254abf71eb493d3132563ebed18071ae_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_254abf71eb493d3132563ebed18071ae_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *locals_prompt_toolkit$output$base_156 = NULL;
    struct Nuitka_FrameObject *frame_aeb434831fa251de371090a9fa391d35_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_aeb434831fa251de371090a9fa391d35_3 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_af2cf307d48d74333c6ef743ee7962d0;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_ce5c8ac4e485bd676e5ac9603cd72d48 = MAKE_MODULE_FRAME( codeobj_ce5c8ac4e485bd676e5ac9603cd72d48, module_prompt_toolkit$output$base );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_ce5c8ac4e485bd676e5ac9603cd72d48 );
    assert( Py_REFCNT( frame_ce5c8ac4e485bd676e5ac9603cd72d48 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 4;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_abc;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$output$base;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_ABCMeta_str_plain_abstractmethod_tuple;
        tmp_level_name_1 = const_int_0;
        frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 5;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_ABCMeta );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_ABCMeta, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_abstractmethod );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_six;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$output$base;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_with_metaclass_tuple;
        tmp_level_name_2 = const_int_0;
        frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 6;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_with_metaclass );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_with_metaclass, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_6c08e9b66c2dc0968961027756a22333;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$output$base;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_Size_tuple;
        tmp_level_name_3 = const_int_0;
        frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 7;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Size );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_Size, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = LIST_COPY( const_list_str_plain_Output_str_plain_DummyOutput_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_10 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_with_metaclass );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_with_metaclass );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "with_metaclass" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 15;

            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_ABCMeta );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ABCMeta );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ABCMeta" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 15;

            goto try_except_handler_2;
        }

        tmp_args_element_name_1 = tmp_mvar_value_4;
        tmp_args_element_name_2 = (PyObject *)&PyBaseObject_Type;
        frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 15;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        tmp_assign_source_11 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_11, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_12 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_14 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_14;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;

                goto try_except_handler_2;
            }
            tmp_tuple_element_2 = const_str_plain_Output;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 15;
            tmp_assign_source_15 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_15;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;

                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 15;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 15;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 15;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 15;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_16;
            tmp_assign_source_16 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_16;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_17;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_prompt_toolkit$output$base_15 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_09601a5c4deed6ab50fc30222e411887;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_6e6723d3292a3915e476469389a3dc2a;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_Output;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_254abf71eb493d3132563ebed18071ae_2, codeobj_254abf71eb493d3132563ebed18071ae, module_prompt_toolkit$output$base, sizeof(void *) );
        frame_254abf71eb493d3132563ebed18071ae_2 = cache_frame_254abf71eb493d3132563ebed18071ae_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_254abf71eb493d3132563ebed18071ae_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_254abf71eb493d3132563ebed18071ae_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_3;
            tmp_called_name_3 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 24;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_3 = tmp_mvar_value_5;
                Py_INCREF( tmp_called_name_3 );
                }
            }

            tmp_args_element_name_3 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_1_fileno(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 24;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_fileno, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_4;
            tmp_called_name_4 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 28;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_4 = tmp_mvar_value_6;
                Py_INCREF( tmp_called_name_4 );
                }
            }

            tmp_args_element_name_4 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_2_encoding(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 28;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 28;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_encoding, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 28;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_args_element_name_5;
            tmp_called_name_5 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 37;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_5 = tmp_mvar_value_7;
                Py_INCREF( tmp_called_name_5 );
                }
            }

            tmp_args_element_name_5 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_3_write(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 37;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_write, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_args_element_name_6;
            tmp_called_name_6 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 41;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_6 = tmp_mvar_value_8;
                Py_INCREF( tmp_called_name_6 );
                }
            }

            tmp_args_element_name_6 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_4_write_raw(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 41;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_write_raw, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_args_element_name_7;
            tmp_called_name_7 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 45;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_7 = tmp_mvar_value_9;
                Py_INCREF( tmp_called_name_7 );
                }
            }

            tmp_args_element_name_7 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_5_set_title(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 45;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_set_title, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_args_element_name_8;
            tmp_called_name_8 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_10 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 49;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_8 = tmp_mvar_value_10;
                Py_INCREF( tmp_called_name_8 );
                }
            }

            tmp_args_element_name_8 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_6_clear_title(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 49;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_clear_title, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_9;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_args_element_name_9;
            tmp_called_name_9 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 53;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_9 = tmp_mvar_value_11;
                Py_INCREF( tmp_called_name_9 );
                }
            }

            tmp_args_element_name_9 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_7_flush(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 53;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
            }

            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_flush, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_10;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_args_element_name_10;
            tmp_called_name_10 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_12 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 57;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_10 = tmp_mvar_value_12;
                Py_INCREF( tmp_called_name_10 );
                }
            }

            tmp_args_element_name_10 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_8_erase_screen(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 57;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_element_name_10 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 57;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_erase_screen, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 57;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_11;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_args_element_name_11;
            tmp_called_name_11 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_13 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_11 = tmp_mvar_value_13;
                Py_INCREF( tmp_called_name_11 );
                }
            }

            tmp_args_element_name_11 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_9_enter_alternate_screen(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 64;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_enter_alternate_screen, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_12;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_args_element_name_12;
            tmp_called_name_12 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_14 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 68;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_12 = tmp_mvar_value_14;
                Py_INCREF( tmp_called_name_12 );
                }
            }

            tmp_args_element_name_12 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_10_quit_alternate_screen(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 68;
            {
                PyObject *call_args[] = { tmp_args_element_name_12 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_args_element_name_12 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_quit_alternate_screen, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_13;
            PyObject *tmp_mvar_value_15;
            PyObject *tmp_args_element_name_13;
            tmp_called_name_13 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 72;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_13 = tmp_mvar_value_15;
                Py_INCREF( tmp_called_name_13 );
                }
            }

            tmp_args_element_name_13 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_11_enable_mouse_support(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 72;
            {
                PyObject *call_args[] = { tmp_args_element_name_13 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
            }

            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_element_name_13 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 72;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_enable_mouse_support, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 72;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_14;
            PyObject *tmp_mvar_value_16;
            PyObject *tmp_args_element_name_14;
            tmp_called_name_14 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_16 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 76;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_14 = tmp_mvar_value_16;
                Py_INCREF( tmp_called_name_14 );
                }
            }

            tmp_args_element_name_14 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_12_disable_mouse_support(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 76;
            {
                PyObject *call_args[] = { tmp_args_element_name_14 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_args_element_name_14 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_disable_mouse_support, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_15;
            PyObject *tmp_mvar_value_17;
            PyObject *tmp_args_element_name_15;
            tmp_called_name_15 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_17 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 80;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_15 = tmp_mvar_value_17;
                Py_INCREF( tmp_called_name_15 );
                }
            }

            tmp_args_element_name_15 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_13_erase_end_of_line(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 80;
            {
                PyObject *call_args[] = { tmp_args_element_name_15 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
            }

            Py_DECREF( tmp_called_name_15 );
            Py_DECREF( tmp_args_element_name_15 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_erase_end_of_line, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_16;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_args_element_name_16;
            tmp_called_name_16 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_18 == NULL ))
                {
                    tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_18 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 86;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_16 = tmp_mvar_value_18;
                Py_INCREF( tmp_called_name_16 );
                }
            }

            tmp_args_element_name_16 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_14_erase_down(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 86;
            {
                PyObject *call_args[] = { tmp_args_element_name_16 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
            }

            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_args_element_name_16 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_erase_down, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_17;
            PyObject *tmp_mvar_value_19;
            PyObject *tmp_args_element_name_17;
            tmp_called_name_17 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_19 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 93;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_17 = tmp_mvar_value_19;
                Py_INCREF( tmp_called_name_17 );
                }
            }

            tmp_args_element_name_17 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_15_reset_attributes(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 93;
            {
                PyObject *call_args[] = { tmp_args_element_name_17 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
            }

            Py_DECREF( tmp_called_name_17 );
            Py_DECREF( tmp_args_element_name_17 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_reset_attributes, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_18;
            PyObject *tmp_mvar_value_20;
            PyObject *tmp_args_element_name_18;
            tmp_called_name_18 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_20 == NULL ))
                {
                    tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_20 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 97;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_18 = tmp_mvar_value_20;
                Py_INCREF( tmp_called_name_18 );
                }
            }

            tmp_args_element_name_18 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_16_set_attributes(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 97;
            {
                PyObject *call_args[] = { tmp_args_element_name_18 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
            }

            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_args_element_name_18 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_set_attributes, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_19;
            PyObject *tmp_mvar_value_21;
            PyObject *tmp_args_element_name_19;
            tmp_called_name_19 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_21 == NULL ))
                {
                    tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_21 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 101;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_19 = tmp_mvar_value_21;
                Py_INCREF( tmp_called_name_19 );
                }
            }

            tmp_args_element_name_19 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_17_disable_autowrap(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 101;
            {
                PyObject *call_args[] = { tmp_args_element_name_19 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
            }

            Py_DECREF( tmp_called_name_19 );
            Py_DECREF( tmp_args_element_name_19 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_disable_autowrap, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_20;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_args_element_name_20;
            tmp_called_name_20 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_22 == NULL ))
                {
                    tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_22 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 105;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_20 = tmp_mvar_value_22;
                Py_INCREF( tmp_called_name_20 );
                }
            }

            tmp_args_element_name_20 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_18_enable_autowrap(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 105;
            {
                PyObject *call_args[] = { tmp_args_element_name_20 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
            }

            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_args_element_name_20 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_enable_autowrap, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_21;
            PyObject *tmp_mvar_value_23;
            PyObject *tmp_args_element_name_21;
            PyObject *tmp_defaults_1;
            tmp_called_name_21 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_23 == NULL ))
                {
                    tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_23 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 109;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_21 = tmp_mvar_value_23;
                Py_INCREF( tmp_called_name_21 );
                }
            }

            tmp_defaults_1 = const_tuple_int_0_int_0_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_args_element_name_21 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_19_cursor_goto( tmp_defaults_1 );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 109;
            {
                PyObject *call_args[] = { tmp_args_element_name_21 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
            }

            Py_DECREF( tmp_called_name_21 );
            Py_DECREF( tmp_args_element_name_21 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_cursor_goto, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_22;
            PyObject *tmp_mvar_value_24;
            PyObject *tmp_args_element_name_22;
            tmp_called_name_22 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_24 == NULL ))
                {
                    tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_24 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 113;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_22 = tmp_mvar_value_24;
                Py_INCREF( tmp_called_name_22 );
                }
            }

            tmp_args_element_name_22 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_20_cursor_up(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 113;
            {
                PyObject *call_args[] = { tmp_args_element_name_22 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
            }

            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_22 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_cursor_up, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_23;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_args_element_name_23;
            tmp_called_name_23 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_25 == NULL ))
                {
                    tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_25 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 117;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_23 = tmp_mvar_value_25;
                Py_INCREF( tmp_called_name_23 );
                }
            }

            tmp_args_element_name_23 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_21_cursor_down(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 117;
            {
                PyObject *call_args[] = { tmp_args_element_name_23 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
            }

            Py_DECREF( tmp_called_name_23 );
            Py_DECREF( tmp_args_element_name_23 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 117;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_cursor_down, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 117;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_24;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_args_element_name_24;
            tmp_called_name_24 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_26 == NULL ))
                {
                    tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_26 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 121;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_24 = tmp_mvar_value_26;
                Py_INCREF( tmp_called_name_24 );
                }
            }

            tmp_args_element_name_24 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_22_cursor_forward(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 121;
            {
                PyObject *call_args[] = { tmp_args_element_name_24 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
            }

            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_args_element_name_24 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 121;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_cursor_forward, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 121;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_25;
            PyObject *tmp_mvar_value_27;
            PyObject *tmp_args_element_name_25;
            tmp_called_name_25 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_27 == NULL ))
                {
                    tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_27 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 125;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_25 = tmp_mvar_value_27;
                Py_INCREF( tmp_called_name_25 );
                }
            }

            tmp_args_element_name_25 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_23_cursor_backward(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 125;
            {
                PyObject *call_args[] = { tmp_args_element_name_25 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
            }

            Py_DECREF( tmp_called_name_25 );
            Py_DECREF( tmp_args_element_name_25 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_cursor_backward, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_26;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_args_element_name_26;
            tmp_called_name_26 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_26 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_28 == NULL ))
                {
                    tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_28 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 129;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_26 = tmp_mvar_value_28;
                Py_INCREF( tmp_called_name_26 );
                }
            }

            tmp_args_element_name_26 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_24_hide_cursor(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 129;
            {
                PyObject *call_args[] = { tmp_args_element_name_26 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
            }

            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_args_element_name_26 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_hide_cursor, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_27;
            PyObject *tmp_mvar_value_29;
            PyObject *tmp_args_element_name_27;
            tmp_called_name_27 = PyObject_GetItem( locals_prompt_toolkit$output$base_15, const_str_plain_abstractmethod );

            if ( tmp_called_name_27 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_abstractmethod );

                if (unlikely( tmp_mvar_value_29 == NULL ))
                {
                    tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_abstractmethod );
                }

                if ( tmp_mvar_value_29 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "abstractmethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 133;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_27 = tmp_mvar_value_29;
                Py_INCREF( tmp_called_name_27 );
                }
            }

            tmp_args_element_name_27 = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_25_show_cursor(  );



            frame_254abf71eb493d3132563ebed18071ae_2->m_frame.f_lineno = 133;
            {
                PyObject *call_args[] = { tmp_args_element_name_27 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
            }

            Py_DECREF( tmp_called_name_27 );
            Py_DECREF( tmp_args_element_name_27 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_show_cursor, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_26_ask_for_cpr(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_ask_for_cpr, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_27_bell(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_bell, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_28_enable_bracketed_paste(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_enable_bracketed_paste, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_29_disable_bracketed_paste(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_disable_bracketed_paste, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_30_scroll_buffer_to_prompt(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain_scroll_buffer_to_prompt, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_254abf71eb493d3132563ebed18071ae_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_254abf71eb493d3132563ebed18071ae_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_254abf71eb493d3132563ebed18071ae_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_254abf71eb493d3132563ebed18071ae_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_254abf71eb493d3132563ebed18071ae_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_254abf71eb493d3132563ebed18071ae_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_254abf71eb493d3132563ebed18071ae_2 == cache_frame_254abf71eb493d3132563ebed18071ae_2 )
        {
            Py_DECREF( frame_254abf71eb493d3132563ebed18071ae_2 );
        }
        cache_frame_254abf71eb493d3132563ebed18071ae_2 = NULL;

        assertFrameObject( frame_254abf71eb493d3132563ebed18071ae_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;

                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_15, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_called_name_28;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_28 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_Output;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_prompt_toolkit$output$base_15;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 15;
            tmp_assign_source_18 = CALL_FUNCTION( tmp_called_name_28, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;

                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_18;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_17 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_17 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_prompt_toolkit$output$base_15 );
        locals_prompt_toolkit$output$base_15 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_prompt_toolkit$output$base_15 );
        locals_prompt_toolkit$output$base_15 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 15;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_Output, tmp_assign_source_17 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_30;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_Output );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Output );
        }

        if ( tmp_mvar_value_30 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Output" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;

            goto try_except_handler_5;
        }

        tmp_tuple_element_5 = tmp_mvar_value_30;
        tmp_assign_source_19 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_assign_source_19, 0, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_19;
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_20 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_20;
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_21;
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_22 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_22;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_5;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_called_name_29;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_29 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;

                goto try_except_handler_5;
            }
            tmp_tuple_element_6 = const_str_plain_DummyOutput;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 156;
            tmp_assign_source_23 = CALL_FUNCTION( tmp_called_name_29, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_29 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_23;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;

                goto try_except_handler_5;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 156;

                    goto try_except_handler_5;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 156;

                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 156;

                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 156;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_5;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_24;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_25;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_prompt_toolkit$output$base_156 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_09601a5c4deed6ab50fc30222e411887;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_digest_a7c36282f7b7721404bd3bfb13fa5999;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_plain_DummyOutput;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto try_except_handler_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_aeb434831fa251de371090a9fa391d35_3, codeobj_aeb434831fa251de371090a9fa391d35, module_prompt_toolkit$output$base, sizeof(void *) );
        frame_aeb434831fa251de371090a9fa391d35_3 = cache_frame_aeb434831fa251de371090a9fa391d35_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_aeb434831fa251de371090a9fa391d35_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_aeb434831fa251de371090a9fa391d35_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_31_fileno(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_fileno, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_32_encoding(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_encoding, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_33_write(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_write, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_34_write_raw(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_write_raw, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_35_set_title(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_set_title, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_36_clear_title(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_clear_title, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_37_flush(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_flush, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_38_erase_screen(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_erase_screen, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_39_enter_alternate_screen(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_enter_alternate_screen, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_40_quit_alternate_screen(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_quit_alternate_screen, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_41_enable_mouse_support(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_enable_mouse_support, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_42_disable_mouse_support(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_disable_mouse_support, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_43_erase_end_of_line(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_erase_end_of_line, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_44_erase_down(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_erase_down, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_45_reset_attributes(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_reset_attributes, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_46_set_attributes(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_set_attributes, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_47_disable_autowrap(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_disable_autowrap, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_48_enable_autowrap(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_enable_autowrap, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        {
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_int_0_int_0_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_49_cursor_goto( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_cursor_goto, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_50_cursor_up(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_cursor_up, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_51_cursor_down(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_cursor_down, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_52_cursor_forward(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_cursor_forward, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_53_cursor_backward(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_cursor_backward, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_54_hide_cursor(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_hide_cursor, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_55_show_cursor(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_show_cursor, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_56_ask_for_cpr(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_ask_for_cpr, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_57_bell(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_bell, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_58_enable_bracketed_paste(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_enable_bracketed_paste, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_59_disable_bracketed_paste(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_disable_bracketed_paste, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_60_scroll_buffer_to_prompt(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_scroll_buffer_to_prompt, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_61_get_size(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_get_size, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$output$base$$$function_62_get_rows_below_cursor_position(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain_get_rows_below_cursor_position, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_aeb434831fa251de371090a9fa391d35_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_aeb434831fa251de371090a9fa391d35_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_aeb434831fa251de371090a9fa391d35_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_aeb434831fa251de371090a9fa391d35_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_aeb434831fa251de371090a9fa391d35_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_aeb434831fa251de371090a9fa391d35_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_aeb434831fa251de371090a9fa391d35_3 == cache_frame_aeb434831fa251de371090a9fa391d35_3 )
        {
            Py_DECREF( frame_aeb434831fa251de371090a9fa391d35_3 );
        }
        cache_frame_aeb434831fa251de371090a9fa391d35_3 = NULL;

        assertFrameObject( frame_aeb434831fa251de371090a9fa391d35_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_7;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;

                goto try_except_handler_7;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$output$base_156, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;

                goto try_except_handler_7;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_called_name_30;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_30 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_DummyOutput;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_prompt_toolkit$output$base_156;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame.f_lineno = 156;
            tmp_assign_source_26 = CALL_FUNCTION( tmp_called_name_30, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;

                goto try_except_handler_7;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_26;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_25 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_25 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_prompt_toolkit$output$base_156 );
        locals_prompt_toolkit$output$base_156 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_prompt_toolkit$output$base_156 );
        locals_prompt_toolkit$output$base_156 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$output$base );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 156;
        goto try_except_handler_5;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$output$base, (Nuitka_StringObject *)const_str_plain_DummyOutput, tmp_assign_source_25 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce5c8ac4e485bd676e5ac9603cd72d48 );
#endif
    popFrameStack();

    assertFrameObject( frame_ce5c8ac4e485bd676e5ac9603cd72d48 );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce5c8ac4e485bd676e5ac9603cd72d48 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ce5c8ac4e485bd676e5ac9603cd72d48, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ce5c8ac4e485bd676e5ac9603cd72d48->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ce5c8ac4e485bd676e5ac9603cd72d48, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;


    return MOD_RETURN_VALUE( module_prompt_toolkit$output$base );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
