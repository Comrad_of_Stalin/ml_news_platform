/* Generated code for Python module 'prompt_toolkit.key_binding.bindings.completion'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$key_binding$bindings$completion" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$key_binding$bindings$completion;
PyDictObject *moduledict_prompt_toolkit$key_binding$bindings$completion;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_str_plain_run_coroutine_in_terminal_tuple;
extern PyObject *const_tuple_str_plain_event_str_plain_b_tuple;
static PyObject *const_tuple_str_plain_Q_tuple;
extern PyObject *const_str_plain_complete_state;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_4689ddb1b17648707196b6e484a13af6;
extern PyObject *const_str_plain_completer;
static PyObject *const_str_digest_fd643db9ad44eab8b8503c969940b902;
extern PyObject *const_tuple_str_plain_KeyBindings_tuple;
static PyObject *const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple;
extern PyObject *const_str_plain_i;
extern PyObject *const_tuple_str_space_tuple;
extern PyObject *const_tuple_str_plain_PromptSession_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_event_tuple;
extern PyObject *const_dict_bd9585a8208e1c25f6bc216c105a6e8f;
extern PyObject *const_str_plain_ControlJ;
extern PyObject *const_str_plain_CompleteEvent;
static PyObject *const_str_digest_b60299e041cdfd0035ec0a9bae28826f;
extern PyObject *const_str_plain_max;
static PyObject *const_str_digest_ac511e3a0ece86ea5b9308c7fa96f1a6;
extern PyObject *const_str_digest_ebac526994d665b2afb9731ed9b7a23c;
extern PyObject *const_str_plain_delete_before_cursor;
static PyObject *const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple;
static PyObject *const_str_digest_23527169a10f84527c194a07b93c92bb;
extern PyObject *const_str_plain_N;
extern PyObject *const_tuple_str_plain_Keys_tuple;
extern PyObject *const_str_digest_1259d6153a100ed296aefbba03a90b57;
static PyObject *const_tuple_035a3ecde940a9a239714735e1a66c46_tuple;
static PyObject *const_str_digest_9fc3e803d5f2a48b56f3f7313dc6e9e3;
extern PyObject *const_str_plain_confirm;
extern PyObject *const_str_plain_q;
static PyObject *const_str_digest_8e8c534fb43f44992cab65344621307f;
extern PyObject *const_str_plain_bindings;
extern PyObject *const_str_plain_flush;
extern PyObject *const_str_digest_ea27cb142036902290d97ce95d960d15;
extern PyObject *const_str_plain_Y;
extern PyObject *const_str_plain_create_confirm_session;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_extend;
extern PyObject *const_str_digest_6695b0a823c8cd6b75b68a2528261696;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_page;
extern PyObject *const_str_digest_8cff9047d0926d732c79d282e1f19e8e;
extern PyObject *const_str_plain_add;
extern PyObject *const_str_plain_padding;
extern PyObject *const_tuple_str_plain_to_formatted_text_tuple;
extern PyObject *const_str_plain_render_cli_done;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_str_digest_b01490f6432779cab5be9465691d156a;
extern PyObject *const_str_plain_n;
static PyObject *const_tuple_17f7024ca22b5db8adf768991274aca0_tuple;
extern PyObject *const_str_plain_print_text;
extern PyObject *const_str_digest_e371fcb47bb33b8f0db02b1ad9764d6e;
extern PyObject *const_str_plain_get_completions;
extern PyObject *const_str_plain_min;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_complete_next;
static PyObject *const_str_digest_ccb3493770ba2106c1d4478a247c7325;
static PyObject *const_str_plain_show_more;
static PyObject *const_dict_98480538e7bf8d3c83e001bed5d405e3;
extern PyObject *const_str_plain_prompt;
extern PyObject *const_str_plain_style;
extern PyObject *const_str_plain_ControlI;
extern PyObject *const_str_plain_display_text;
extern PyObject *const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
static PyObject *const_str_digest_34d8419347ad40f3624660c365ce6d87;
extern PyObject *const_tuple_str_plain_q_tuple;
extern PyObject *const_str_plain_message;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_c_tuple;
extern PyObject *const_str_plain_async_;
extern PyObject *const_str_plain_display;
extern PyObject *const_str_plain_get_cwidth;
extern PyObject *const_tuple_str_plain_y_tuple;
extern PyObject *const_tuple_str_plain_N_tuple;
extern PyObject *const_str_plain_start_position;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_get_common_complete_suffix;
extern PyObject *const_str_plain_columns;
extern PyObject *const_str_space;
extern PyObject *const_str_plain_run_coroutine_in_terminal;
extern PyObject *const_str_plain_event;
static PyObject *const_str_plain_page_completions;
static PyObject *const_tuple_fafbfce7477850ed9bd0672e936d80b6_tuple;
static PyObject *const_str_digest_1221d40c14a917ca64d9799e0fd8eb08;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_Keys;
static PyObject *const_str_plain_term_height;
extern PyObject *const_str_plain_session;
extern PyObject *const_str_plain_r;
extern PyObject *const_str_plain_document;
extern PyObject *const_str_plain_Any;
static PyObject *const_str_plain_max_compl_width;
extern PyObject *const_tuple_tuple_str_empty_str_newline_tuple_tuple;
extern PyObject *const_dict_d9f37cf1134b2ef5e7e66a4fa42a97ac;
extern PyObject *const_str_plain_b;
static PyObject *const_str_digest_632d63e94fd0db87b5234c4ce51d9369;
static PyObject *const_str_plain_page_count;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_rows;
extern PyObject *const_str_plain_Q;
extern PyObject *const_str_plain_KeyBindings;
extern PyObject *const_str_plain_False;
static PyObject *const_str_digest_eaa120300f50b01b6bc94cdc3a2117e6;
extern PyObject *const_str_plain_output;
extern PyObject *const_str_plain_column_count;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_plain_completion;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_plain_ControlC;
extern PyObject *const_str_plain_to_formatted_text;
static PyObject *const_str_digest_419d1721aaeb7914f7b6216d4ecba0b0;
extern PyObject *const_int_0;
static PyObject *const_str_plain_run_compl;
static PyObject *const_str_digest_072ff487f78b29b4a1d056547254a8d3;
extern PyObject *const_tuple_str_plain_get_cwidth_tuple;
static PyObject *const_str_plain_page_row_count;
extern PyObject *const_str_plain_start_completion;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_completions_per_page;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_dict_23ccf90c8ca98c47117f7093afc959ff;
extern PyObject *const_str_plain_c;
extern PyObject *const_tuple_str_plain_Y_tuple;
extern PyObject *const_str_plain_complete_event;
extern PyObject *const_str_digest_892d8dcf187eb812198632660c4fd2f6;
static PyObject *const_dict_3d35255aaf18052dfa7f86009d060cd4;
static PyObject *const_str_plain_term_size;
extern PyObject *const_tuple_str_plain_n_tuple;
static PyObject *const_list_4689138b69e94207c700df4456eb690b_list;
extern PyObject *const_str_plain_app;
extern PyObject *const_str_plain_ControlM;
extern PyObject *const_str_plain_current_buffer;
extern PyObject *const_str_plain_PromptSession;
extern PyObject *const_str_plain_key_bindings;
extern PyObject *const_str_plain_exit;
static PyObject *const_dict_c8d1ff05459685c2fb0696690e743b57;
extern PyObject *const_str_plain_math;
extern PyObject *const_str_plain__;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_generate_completions;
static PyObject *const_tuple_str_digest_23527169a10f84527c194a07b93c92bb_tuple;
extern PyObject *const_str_digest_f8312c22e9c7474f17f935ae54dbfeef;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_str_digest_8e9cb0df41bf6407b4d4434b9be1ef3d;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_insert_common_part;
static PyObject *const_str_plain__display_completions_like_readline;
extern PyObject *const_str_plain_y;
static PyObject *const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple;
static PyObject *const_tuple_str_plain_CompleteEvent_str_plain_get_common_complete_suffix_tuple;
extern PyObject *const_str_plain_display_completions_like_readline;
extern PyObject *const_str_newline;
extern PyObject *const_tuple_str_empty_str_newline_tuple;
extern PyObject *const_str_plain_get_size;
extern PyObject *const_str_plain_completion_requested;
static PyObject *const_tuple_str_plain_create_confirm_session_tuple;
static PyObject *const_str_plain__create_more_session;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_completions;
static PyObject *const_str_plain_page_columns;
static PyObject *const_str_plain_common_suffix;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_ceil;
extern PyObject *const_str_plain_insert_text;
extern PyObject *const_str_plain_erase_when_done;
static PyObject *const_str_plain_term_width;
extern PyObject *const_str_digest_01e95d2aac58530e0f23e21a1e50445c;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_plain_run_coroutine_in_terminal_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_run_coroutine_in_terminal_tuple, 0, const_str_plain_run_coroutine_in_terminal ); Py_INCREF( const_str_plain_run_coroutine_in_terminal );
    const_tuple_str_plain_Q_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Q_tuple, 0, const_str_plain_Q ); Py_INCREF( const_str_plain_Q );
    const_str_digest_4689ddb1b17648707196b6e484a13af6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678642 ], 50, 0 );
    const_str_digest_fd643db9ad44eab8b8503c969940b902 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678692 ], 49, 0 );
    const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 0, const_str_plain_confirm ); Py_INCREF( const_str_plain_confirm );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 1, const_str_plain_page ); Py_INCREF( const_str_plain_page );
    const_str_plain_show_more = UNSTREAM_STRING_ASCII( &constant_bin[ 4678741 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 2, const_str_plain_show_more ); Py_INCREF( const_str_plain_show_more );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 3, const_str_plain_completions ); Py_INCREF( const_str_plain_completions );
    const_str_plain_completions_per_page = UNSTREAM_STRING_ASCII( &constant_bin[ 4678750 ], 20, 1 );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 4, const_str_plain_completions_per_page ); Py_INCREF( const_str_plain_completions_per_page );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 5, const_str_plain_create_confirm_session ); Py_INCREF( const_str_plain_create_confirm_session );
    const_str_plain_page_count = UNSTREAM_STRING_ASCII( &constant_bin[ 4678770 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 6, const_str_plain_page_count ); Py_INCREF( const_str_plain_page_count );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 7, const_str_plain_display ); Py_INCREF( const_str_plain_display );
    PyTuple_SET_ITEM( const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 8, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_str_digest_b60299e041cdfd0035ec0a9bae28826f = UNSTREAM_STRING_ASCII( &constant_bin[ 4678780 ], 43, 0 );
    const_str_digest_ac511e3a0ece86ea5b9308c7fa96f1a6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678823 ], 410, 0 );
    const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple, 1, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple, 2, const_str_plain_complete_event ); Py_INCREF( const_str_plain_complete_event );
    PyTuple_SET_ITEM( const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple, 3, const_str_plain_completions ); Py_INCREF( const_str_plain_completions );
    const_str_plain_common_suffix = UNSTREAM_STRING_ASCII( &constant_bin[ 4679233 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple, 4, const_str_plain_common_suffix ); Py_INCREF( const_str_plain_common_suffix );
    const_str_digest_23527169a10f84527c194a07b93c92bb = UNSTREAM_STRING_ASCII( &constant_bin[ 4679246 ], 8, 0 );
    const_tuple_035a3ecde940a9a239714735e1a66c46_tuple = PyTuple_New( 16 );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 0, const_str_plain_page ); Py_INCREF( const_str_plain_page );
    const_str_plain_page_completions = UNSTREAM_STRING_ASCII( &constant_bin[ 4679254 ], 16, 1 );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 1, const_str_plain_page_completions ); Py_INCREF( const_str_plain_page_completions );
    const_str_plain_page_row_count = UNSTREAM_STRING_ASCII( &constant_bin[ 4679270 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 2, const_str_plain_page_row_count ); Py_INCREF( const_str_plain_page_row_count );
    const_str_plain_page_columns = UNSTREAM_STRING_ASCII( &constant_bin[ 4679284 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 3, const_str_plain_page_columns ); Py_INCREF( const_str_plain_page_columns );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 4, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 5, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 6, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 7, const_str_plain_completion ); Py_INCREF( const_str_plain_completion );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 8, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 9, const_str_plain_padding ); Py_INCREF( const_str_plain_padding );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 10, const_str_plain_completions ); Py_INCREF( const_str_plain_completions );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 11, const_str_plain_completions_per_page ); Py_INCREF( const_str_plain_completions_per_page );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 12, const_str_plain_column_count ); Py_INCREF( const_str_plain_column_count );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 13, const_str_plain_to_formatted_text ); Py_INCREF( const_str_plain_to_formatted_text );
    const_str_plain_max_compl_width = UNSTREAM_STRING_ASCII( &constant_bin[ 4679296 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 14, const_str_plain_max_compl_width ); Py_INCREF( const_str_plain_max_compl_width );
    PyTuple_SET_ITEM( const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 15, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_str_digest_9fc3e803d5f2a48b56f3f7313dc6e9e3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4679311 ], 215, 0 );
    const_str_digest_8e8c534fb43f44992cab65344621307f = UNSTREAM_STRING_ASCII( &constant_bin[ 4679526 ], 55, 0 );
    const_str_digest_b01490f6432779cab5be9465691d156a = UNSTREAM_STRING_ASCII( &constant_bin[ 4679581 ], 29, 0 );
    const_tuple_17f7024ca22b5db8adf768991274aca0_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_17f7024ca22b5db8adf768991274aca0_tuple, 0, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_17f7024ca22b5db8adf768991274aca0_tuple, 1, const_str_plain_PromptSession ); Py_INCREF( const_str_plain_PromptSession );
    PyTuple_SET_ITEM( const_tuple_17f7024ca22b5db8adf768991274aca0_tuple, 2, const_str_plain_bindings ); Py_INCREF( const_str_plain_bindings );
    PyTuple_SET_ITEM( const_tuple_17f7024ca22b5db8adf768991274aca0_tuple, 3, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    PyTuple_SET_ITEM( const_tuple_17f7024ca22b5db8adf768991274aca0_tuple, 4, const_str_plain_session ); Py_INCREF( const_str_plain_session );
    const_str_digest_ccb3493770ba2106c1d4478a247c7325 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678780 ], 31, 0 );
    const_dict_98480538e7bf8d3c83e001bed5d405e3 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_98480538e7bf8d3c83e001bed5d405e3, const_str_plain_insert_common_part, Py_True );
    assert( PyDict_Size( const_dict_98480538e7bf8d3c83e001bed5d405e3 ) == 1 );
    const_str_digest_34d8419347ad40f3624660c365ce6d87 = UNSTREAM_STRING_ASCII( &constant_bin[ 4679610 ], 53, 0 );
    const_tuple_fafbfce7477850ed9bd0672e936d80b6_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_fafbfce7477850ed9bd0672e936d80b6_tuple, 0, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_fafbfce7477850ed9bd0672e936d80b6_tuple, 1, const_str_plain_page_completions ); Py_INCREF( const_str_plain_page_completions );
    PyTuple_SET_ITEM( const_tuple_fafbfce7477850ed9bd0672e936d80b6_tuple, 2, const_str_plain_page_row_count ); Py_INCREF( const_str_plain_page_row_count );
    const_str_digest_1221d40c14a917ca64d9799e0fd8eb08 = UNSTREAM_STRING_ASCII( &constant_bin[ 4679663 ], 51, 0 );
    const_str_plain_term_height = UNSTREAM_STRING_ASCII( &constant_bin[ 4679714 ], 11, 1 );
    const_str_digest_632d63e94fd0db87b5234c4ce51d9369 = UNSTREAM_STRING_ASCII( &constant_bin[ 4679725 ], 28, 0 );
    const_str_digest_eaa120300f50b01b6bc94cdc3a2117e6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4679753 ], 123, 0 );
    const_str_digest_419d1721aaeb7914f7b6216d4ecba0b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4679876 ], 72, 0 );
    const_str_plain_run_compl = UNSTREAM_STRING_ASCII( &constant_bin[ 4679948 ], 9, 1 );
    const_str_digest_072ff487f78b29b4a1d056547254a8d3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4679957 ], 31, 0 );
    const_dict_3d35255aaf18052dfa7f86009d060cd4 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_3d35255aaf18052dfa7f86009d060cd4, const_str_plain_async_, Py_True );
    assert( PyDict_Size( const_dict_3d35255aaf18052dfa7f86009d060cd4 ) == 1 );
    const_str_plain_term_size = UNSTREAM_STRING_ASCII( &constant_bin[ 4679988 ], 9, 1 );
    const_list_4689138b69e94207c700df4456eb690b_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_4689138b69e94207c700df4456eb690b_list, 0, const_str_plain_generate_completions ); Py_INCREF( const_str_plain_generate_completions );
    PyList_SET_ITEM( const_list_4689138b69e94207c700df4456eb690b_list, 1, const_str_plain_display_completions_like_readline ); Py_INCREF( const_str_plain_display_completions_like_readline );
    const_dict_c8d1ff05459685c2fb0696690e743b57 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_c8d1ff05459685c2fb0696690e743b57, const_str_plain_render_cli_done, Py_True );
    assert( PyDict_Size( const_dict_c8d1ff05459685c2fb0696690e743b57 ) == 1 );
    const_tuple_str_digest_23527169a10f84527c194a07b93c92bb_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_23527169a10f84527c194a07b93c92bb_tuple, 0, const_str_digest_23527169a10f84527c194a07b93c92bb ); Py_INCREF( const_str_digest_23527169a10f84527c194a07b93c92bb );
    const_str_digest_8e9cb0df41bf6407b4d4434b9be1ef3d = UNSTREAM_STRING_ASCII( &constant_bin[ 4679997 ], 53, 0 );
    const_str_plain__display_completions_like_readline = UNSTREAM_STRING_ASCII( &constant_bin[ 4679610 ], 34, 1 );
    const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple = PyTuple_New( 13 );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 0, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 1, const_str_plain_completions ); Py_INCREF( const_str_plain_completions );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 2, const_str_plain_create_confirm_session ); Py_INCREF( const_str_plain_create_confirm_session );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 3, const_str_plain_to_formatted_text ); Py_INCREF( const_str_plain_to_formatted_text );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 4, const_str_plain_term_size ); Py_INCREF( const_str_plain_term_size );
    const_str_plain_term_width = UNSTREAM_STRING_ASCII( &constant_bin[ 4680050 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 5, const_str_plain_term_width ); Py_INCREF( const_str_plain_term_width );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 6, const_str_plain_term_height ); Py_INCREF( const_str_plain_term_height );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 7, const_str_plain_max_compl_width ); Py_INCREF( const_str_plain_max_compl_width );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 8, const_str_plain_column_count ); Py_INCREF( const_str_plain_column_count );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 9, const_str_plain_completions_per_page ); Py_INCREF( const_str_plain_completions_per_page );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 10, const_str_plain_page_count ); Py_INCREF( const_str_plain_page_count );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 11, const_str_plain_display ); Py_INCREF( const_str_plain_display );
    PyTuple_SET_ITEM( const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 12, const_str_plain_run_compl ); Py_INCREF( const_str_plain_run_compl );
    const_tuple_str_plain_CompleteEvent_str_plain_get_common_complete_suffix_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_CompleteEvent_str_plain_get_common_complete_suffix_tuple, 0, const_str_plain_CompleteEvent ); Py_INCREF( const_str_plain_CompleteEvent );
    PyTuple_SET_ITEM( const_tuple_str_plain_CompleteEvent_str_plain_get_common_complete_suffix_tuple, 1, const_str_plain_get_common_complete_suffix ); Py_INCREF( const_str_plain_get_common_complete_suffix );
    const_tuple_str_plain_create_confirm_session_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_create_confirm_session_tuple, 0, const_str_plain_create_confirm_session ); Py_INCREF( const_str_plain_create_confirm_session );
    const_str_plain__create_more_session = UNSTREAM_STRING_ASCII( &constant_bin[ 4679957 ], 20, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$key_binding$bindings$completion( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_869eef7eb6c43f75799efd7a1898cdea;
static PyCodeObject *codeobj_41b35b457c6378513a2a778231c348e0;
static PyCodeObject *codeobj_34e80bb0bbf944f22f9ecf062c53ca52;
static PyCodeObject *codeobj_6176dd9c65729e0528d8ef7c4fa87a2c;
static PyCodeObject *codeobj_762465262e16acce09534825e13dde4d;
static PyCodeObject *codeobj_a457fc0b896f4d20ec9850253c4f6bb0;
static PyCodeObject *codeobj_4fb91f90eee259ddfe98fafe720d6141;
static PyCodeObject *codeobj_6fd261609a458f9a8d6e6c0a22417c6c;
static PyCodeObject *codeobj_a6b1c03abe1fa3b0bd0bdb90477ece7f;
static PyCodeObject *codeobj_a643590f1bc39c9e6a3c29b1fd6e5859;
static PyCodeObject *codeobj_10e94fe3f0f3c0523e41f72f5e8b9f5d;
static PyCodeObject *codeobj_a76d00685e9c179822ab9bb2902ac15b;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_fd643db9ad44eab8b8503c969940b902 );
    codeobj_869eef7eb6c43f75799efd7a1898cdea = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 88, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_c_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_41b35b457c6378513a2a778231c348e0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 100, const_tuple_fafbfce7477850ed9bd0672e936d80b6_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_34e80bb0bbf944f22f9ecf062c53ca52 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8e8c534fb43f44992cab65344621307f, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6176dd9c65729e0528d8ef7c4fa87a2c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 158, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_762465262e16acce09534825e13dde4d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 167, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a457fc0b896f4d20ec9850253c4f6bb0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 175, const_tuple_str_plain_event_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4fb91f90eee259ddfe98fafe720d6141 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__create_more_session, 151, const_tuple_17f7024ca22b5db8adf768991274aca0_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6fd261609a458f9a8d6e6c0a22417c6c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__display_completions_like_readline, 69, const_tuple_cb9ec85f655830ca55af4387ae86540c_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a6b1c03abe1fa3b0bd0bdb90477ece7f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_display, 94, const_tuple_035a3ecde940a9a239714735e1a66c46_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a643590f1bc39c9e6a3c29b1fd6e5859 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_display_completions_like_readline, 33, const_tuple_f9d6de8fbcabf548c131c328d9a7f904_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_10e94fe3f0f3c0523e41f72f5e8b9f5d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_generate_completions, 19, const_tuple_str_plain_event_str_plain_b_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a76d00685e9c179822ab9bb2902ac15b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_run_compl, 123, const_tuple_a648d6ec2eadd004bc31172b3e4dda76_tuple, 0, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_maker( void );


static PyObject *prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_maker( void );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_1_generate_completions(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_2_display_completions_like_readline(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_1__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_2__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_3__(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_1_generate_completions( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_b = NULL;
    struct Nuitka_FrameObject *frame_10e94fe3f0f3c0523e41f72f5e8b9f5d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_10e94fe3f0f3c0523e41f72f5e8b9f5d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_10e94fe3f0f3c0523e41f72f5e8b9f5d, codeobj_10e94fe3f0f3c0523e41f72f5e8b9f5d, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *) );
    frame_10e94fe3f0f3c0523e41f72f5e8b9f5d = cache_frame_10e94fe3f0f3c0523e41f72f5e8b9f5d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_b );
        tmp_source_name_2 = var_b;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_complete_state );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( var_b );
            tmp_called_instance_1 = var_b;
            frame_10e94fe3f0f3c0523e41f72f5e8b9f5d->m_frame.f_lineno = 28;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_complete_next );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 28;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( var_b );
            tmp_source_name_3 = var_b;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_start_completion );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_98480538e7bf8d3c83e001bed5d405e3 );
            frame_10e94fe3f0f3c0523e41f72f5e8b9f5d->m_frame.f_lineno = 30;
            tmp_call_result_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_10e94fe3f0f3c0523e41f72f5e8b9f5d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_10e94fe3f0f3c0523e41f72f5e8b9f5d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_10e94fe3f0f3c0523e41f72f5e8b9f5d,
        type_description_1,
        par_event,
        var_b
    );


    // Release cached frame.
    if ( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d == cache_frame_10e94fe3f0f3c0523e41f72f5e8b9f5d )
    {
        Py_DECREF( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d );
    }
    cache_frame_10e94fe3f0f3c0523e41f72f5e8b9f5d = NULL;

    assertFrameObject( frame_10e94fe3f0f3c0523e41f72f5e8b9f5d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_1_generate_completions );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_1_generate_completions );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_2_display_completions_like_readline( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_b = NULL;
    PyObject *var_complete_event = NULL;
    PyObject *var_completions = NULL;
    PyObject *var_common_suffix = NULL;
    struct Nuitka_FrameObject *frame_a643590f1bc39c9e6a3c29b1fd6e5859;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_a643590f1bc39c9e6a3c29b1fd6e5859 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a643590f1bc39c9e6a3c29b1fd6e5859, codeobj_a643590f1bc39c9e6a3c29b1fd6e5859, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a643590f1bc39c9e6a3c29b1fd6e5859 = cache_frame_a643590f1bc39c9e6a3c29b1fd6e5859;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a643590f1bc39c9e6a3c29b1fd6e5859 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a643590f1bc39c9e6a3c29b1fd6e5859 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_b );
        tmp_source_name_2 = var_b;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_completer );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_CompleteEvent );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompleteEvent );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompleteEvent" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_kw_name_1 = PyDict_Copy( const_dict_bd9585a8208e1c25f6bc216c105a6e8f );
        frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame.f_lineno = 51;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_complete_event == NULL );
        var_complete_event = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_b );
        tmp_source_name_4 = var_b;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_completer );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_get_completions );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_b );
        tmp_source_name_5 = var_b;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_document );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 52;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_complete_event );
        tmp_args_element_name_2 = var_complete_event;
        frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame.f_lineno = 52;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_list_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_completions == NULL );
        var_completions = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_get_common_complete_suffix );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_common_complete_suffix );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_common_complete_suffix" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_2;
        CHECK_OBJECT( var_b );
        tmp_source_name_6 = var_b;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_document );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_completions );
        tmp_args_element_name_4 = var_completions;
        frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_common_suffix == NULL );
        var_common_suffix = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( var_completions );
        tmp_len_arg_1 = var_completions;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        assert( !(tmp_compexpr_left_2 == NULL) );
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_7;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_8;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_b );
            tmp_source_name_7 = var_b;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_delete_before_cursor );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_completions );
            tmp_subscribed_name_1 = var_completions;
            tmp_subscript_name_1 = const_int_0;
            tmp_source_name_8 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_source_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 59;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_start_position );
            Py_DECREF( tmp_source_name_8 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 59;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_5 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 59;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame.f_lineno = 59;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_9;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_source_name_10;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( var_b );
            tmp_source_name_9 = var_b;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_insert_text );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_completions );
            tmp_subscribed_name_2 = var_completions;
            tmp_subscript_name_2 = const_int_0;
            tmp_source_name_10 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
            if ( tmp_source_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 60;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_text );
            Py_DECREF( tmp_source_name_10 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 60;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame.f_lineno = 60;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_truth_name_1;
            CHECK_OBJECT( var_common_suffix );
            tmp_truth_name_1 = CHECK_IF_TRUE( var_common_suffix );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 62;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_7;
                CHECK_OBJECT( var_b );
                tmp_called_instance_1 = var_b;
                CHECK_OBJECT( var_common_suffix );
                tmp_args_element_name_7 = var_common_suffix;
                frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame.f_lineno = 63;
                {
                    PyObject *call_args[] = { tmp_args_element_name_7 };
                    tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_insert_text, call_args );
                }

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 63;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                int tmp_truth_name_2;
                CHECK_OBJECT( var_completions );
                tmp_truth_name_2 = CHECK_IF_TRUE( var_completions );
                assert( !(tmp_truth_name_2 == -1) );
                tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_mvar_value_3;
                    PyObject *tmp_call_result_4;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_source_name_11;
                    PyObject *tmp_args_element_name_9;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain__display_completions_like_readline );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__display_completions_like_readline );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_display_completions_like_readline" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 66;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_6 = tmp_mvar_value_3;
                    CHECK_OBJECT( par_event );
                    tmp_source_name_11 = par_event;
                    tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_app );
                    if ( tmp_args_element_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 66;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_completions );
                    tmp_args_element_name_9 = var_completions;
                    frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame.f_lineno = 66;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
                        tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_8 );
                    if ( tmp_call_result_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 66;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_4 );
                }
                branch_no_4:;
            }
            branch_end_3:;
        }
        branch_end_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a643590f1bc39c9e6a3c29b1fd6e5859 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a643590f1bc39c9e6a3c29b1fd6e5859 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a643590f1bc39c9e6a3c29b1fd6e5859 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a643590f1bc39c9e6a3c29b1fd6e5859, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a643590f1bc39c9e6a3c29b1fd6e5859->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a643590f1bc39c9e6a3c29b1fd6e5859, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a643590f1bc39c9e6a3c29b1fd6e5859,
        type_description_1,
        par_event,
        var_b,
        var_complete_event,
        var_completions,
        var_common_suffix
    );


    // Release cached frame.
    if ( frame_a643590f1bc39c9e6a3c29b1fd6e5859 == cache_frame_a643590f1bc39c9e6a3c29b1fd6e5859 )
    {
        Py_DECREF( frame_a643590f1bc39c9e6a3c29b1fd6e5859 );
    }
    cache_frame_a643590f1bc39c9e6a3c29b1fd6e5859 = NULL;

    assertFrameObject( frame_a643590f1bc39c9e6a3c29b1fd6e5859 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_2_display_completions_like_readline );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_complete_event );
    var_complete_event = NULL;

    Py_XDECREF( var_completions );
    var_completions = NULL;

    Py_XDECREF( var_common_suffix );
    var_common_suffix = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_complete_event );
    var_complete_event = NULL;

    Py_XDECREF( var_completions );
    var_completions = NULL;

    Py_XDECREF( var_common_suffix );
    var_common_suffix = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_2_display_completions_like_readline );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_app = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_completions = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *var_create_confirm_session = PyCell_EMPTY();
    struct Nuitka_CellObject *var_to_formatted_text = PyCell_EMPTY();
    PyObject *var_term_size = NULL;
    PyObject *var_term_width = NULL;
    PyObject *var_term_height = NULL;
    struct Nuitka_CellObject *var_max_compl_width = PyCell_EMPTY();
    struct Nuitka_CellObject *var_column_count = PyCell_EMPTY();
    struct Nuitka_CellObject *var_completions_per_page = PyCell_EMPTY();
    struct Nuitka_CellObject *var_page_count = PyCell_EMPTY();
    struct Nuitka_CellObject *var_display = PyCell_EMPTY();
    PyObject *var_run_compl = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_6fd261609a458f9a8d6e6c0a22417c6c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_6fd261609a458f9a8d6e6c0a22417c6c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6fd261609a458f9a8d6e6c0a22417c6c, codeobj_6fd261609a458f9a8d6e6c0a22417c6c, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6fd261609a458f9a8d6e6c0a22417c6c = cache_frame_6fd261609a458f9a8d6e6c0a22417c6c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6fd261609a458f9a8d6e6c0a22417c6c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6fd261609a458f9a8d6e6c0a22417c6c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_1259d6153a100ed296aefbba03a90b57;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_create_confirm_session_tuple;
        tmp_level_name_1 = const_int_0;
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 75;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_create_confirm_session );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_create_confirm_session ) == NULL );
        PyCell_SET( var_create_confirm_session, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_f8312c22e9c7474f17f935ae54dbfeef;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_to_formatted_text_tuple;
        tmp_level_name_2 = const_int_0;
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 76;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_to_formatted_text );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_to_formatted_text ) == NULL );
        PyCell_SET( var_to_formatted_text, tmp_assign_source_2 );

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( PyCell_GET( par_completions ) );
        tmp_isinstance_inst_1 = PyCell_GET( par_completions );
        tmp_isinstance_cls_1 = (PyObject *)&PyList_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 77;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( PyCell_GET( par_app ) );
        tmp_source_name_1 = PyCell_GET( par_app );
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_output );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 80;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_size );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( var_term_size == NULL );
        var_term_size = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_term_size );
        tmp_source_name_2 = var_term_size;
        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_columns );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( var_term_width == NULL );
        var_term_width = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_term_size );
        tmp_source_name_3 = var_term_size;
        tmp_assign_source_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_rows );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( var_term_height == NULL );
        var_term_height = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_right_name_1;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_min );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( var_term_width );
        tmp_args_element_name_1 = var_term_width;
        tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_max );
        assert( tmp_called_name_2 != NULL );
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( PyCell_GET( par_completions ) );
            tmp_iter_arg_1 = PyCell_GET( par_completions );
            tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;
                type_description_1 = "ccccoooccccco";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_7;
        }
        // Tried code:
        tmp_args_element_name_3 = prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_3)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline );
        return NULL;
        outline_result_1:;
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 88;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_left_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_1;
        tmp_args_element_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 87;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_max_compl_width ) == NULL );
        PyCell_SET( var_max_compl_width, tmp_assign_source_6 );

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_max );
        assert( tmp_called_name_3 != NULL );
        tmp_args_element_name_4 = const_int_pos_1;
        CHECK_OBJECT( var_term_width );
        tmp_left_name_2 = var_term_width;
        CHECK_OBJECT( PyCell_GET( var_max_compl_width ) );
        tmp_right_name_2 = PyCell_GET( var_max_compl_width );
        tmp_args_element_name_5 = BINARY_OPERATION_FLOORDIV_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_column_count ) == NULL );
        PyCell_SET( var_column_count, tmp_assign_source_8 );

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        CHECK_OBJECT( PyCell_GET( var_column_count ) );
        tmp_left_name_3 = PyCell_GET( var_column_count );
        CHECK_OBJECT( var_term_height );
        tmp_left_name_4 = var_term_height;
        tmp_right_name_4 = const_int_pos_1;
        tmp_right_name_3 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_4, tmp_right_name_4 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_completions_per_page ) == NULL );
        PyCell_SET( var_completions_per_page, tmp_assign_source_9 );

    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_int_arg_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_left_name_5;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_right_name_5;
        PyObject *tmp_float_arg_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_math );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_math );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "math" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_1;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_ceil );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_completions ) );
        tmp_len_arg_1 = PyCell_GET( par_completions );
        tmp_left_name_5 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 91;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( var_completions_per_page ) );
        tmp_float_arg_1 = PyCell_GET( var_completions_per_page );
        tmp_right_name_5 = TO_FLOAT( tmp_float_arg_1 );
        if ( tmp_right_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_left_name_5 );

            exception_lineno = 91;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_6 = BINARY_OPERATION_TRUEDIV_LONG_OBJECT( tmp_left_name_5, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_5 );
        Py_DECREF( tmp_right_name_5 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 91;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 91;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_int_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_int_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = PyNumber_Int( tmp_int_arg_1 );
        Py_DECREF( tmp_int_arg_1 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_page_count ) == NULL );
        PyCell_SET( var_page_count, tmp_assign_source_10 );

    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[0] = par_app;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[1] = var_column_count;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[2] = par_completions;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[3] = var_completions_per_page;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[3] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[4] = var_max_compl_width;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[4] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[5] = var_to_formatted_text;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_11)->m_closure[5] );


        assert( PyCell_GET( var_display ) == NULL );
        PyCell_SET( var_display, tmp_assign_source_11 );

    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[0] = par_app;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[1] = par_completions;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[2] = var_completions_per_page;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[3] = var_create_confirm_session;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[3] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[4] = var_display;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[4] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[5] = var_page_count;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[5] );


        assert( var_run_compl == NULL );
        var_run_compl = tmp_assign_source_12;
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_run_coroutine_in_terminal );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_run_coroutine_in_terminal );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "run_coroutine_in_terminal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_2;
        CHECK_OBJECT( var_run_compl );
        tmp_tuple_element_1 = var_run_compl;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_c8d1ff05459685c2fb0696690e743b57 );
        frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame.f_lineno = 148;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "ccccoooccccco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fd261609a458f9a8d6e6c0a22417c6c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fd261609a458f9a8d6e6c0a22417c6c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6fd261609a458f9a8d6e6c0a22417c6c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6fd261609a458f9a8d6e6c0a22417c6c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6fd261609a458f9a8d6e6c0a22417c6c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6fd261609a458f9a8d6e6c0a22417c6c,
        type_description_1,
        par_app,
        par_completions,
        var_create_confirm_session,
        var_to_formatted_text,
        var_term_size,
        var_term_width,
        var_term_height,
        var_max_compl_width,
        var_column_count,
        var_completions_per_page,
        var_page_count,
        var_display,
        var_run_compl
    );


    // Release cached frame.
    if ( frame_6fd261609a458f9a8d6e6c0a22417c6c == cache_frame_6fd261609a458f9a8d6e6c0a22417c6c )
    {
        Py_DECREF( frame_6fd261609a458f9a8d6e6c0a22417c6c );
    }
    cache_frame_6fd261609a458f9a8d6e6c0a22417c6c = NULL;

    assertFrameObject( frame_6fd261609a458f9a8d6e6c0a22417c6c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_app );
    Py_DECREF( par_app );
    par_app = NULL;

    CHECK_OBJECT( (PyObject *)par_completions );
    Py_DECREF( par_completions );
    par_completions = NULL;

    CHECK_OBJECT( (PyObject *)var_create_confirm_session );
    Py_DECREF( var_create_confirm_session );
    var_create_confirm_session = NULL;

    CHECK_OBJECT( (PyObject *)var_to_formatted_text );
    Py_DECREF( var_to_formatted_text );
    var_to_formatted_text = NULL;

    CHECK_OBJECT( (PyObject *)var_term_size );
    Py_DECREF( var_term_size );
    var_term_size = NULL;

    CHECK_OBJECT( (PyObject *)var_term_width );
    Py_DECREF( var_term_width );
    var_term_width = NULL;

    CHECK_OBJECT( (PyObject *)var_term_height );
    Py_DECREF( var_term_height );
    var_term_height = NULL;

    CHECK_OBJECT( (PyObject *)var_max_compl_width );
    Py_DECREF( var_max_compl_width );
    var_max_compl_width = NULL;

    CHECK_OBJECT( (PyObject *)var_column_count );
    Py_DECREF( var_column_count );
    var_column_count = NULL;

    CHECK_OBJECT( (PyObject *)var_completions_per_page );
    Py_DECREF( var_completions_per_page );
    var_completions_per_page = NULL;

    CHECK_OBJECT( (PyObject *)var_page_count );
    Py_DECREF( var_page_count );
    var_page_count = NULL;

    CHECK_OBJECT( (PyObject *)var_display );
    Py_DECREF( var_display );
    var_display = NULL;

    CHECK_OBJECT( (PyObject *)var_run_compl );
    Py_DECREF( var_run_compl );
    var_run_compl = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_app );
    Py_DECREF( par_app );
    par_app = NULL;

    CHECK_OBJECT( (PyObject *)par_completions );
    Py_DECREF( par_completions );
    par_completions = NULL;

    CHECK_OBJECT( (PyObject *)var_create_confirm_session );
    Py_DECREF( var_create_confirm_session );
    var_create_confirm_session = NULL;

    CHECK_OBJECT( (PyObject *)var_to_formatted_text );
    Py_DECREF( var_to_formatted_text );
    var_to_formatted_text = NULL;

    Py_XDECREF( var_term_size );
    var_term_size = NULL;

    Py_XDECREF( var_term_width );
    var_term_width = NULL;

    Py_XDECREF( var_term_height );
    var_term_height = NULL;

    CHECK_OBJECT( (PyObject *)var_max_compl_width );
    Py_DECREF( var_max_compl_width );
    var_max_compl_width = NULL;

    CHECK_OBJECT( (PyObject *)var_column_count );
    Py_DECREF( var_column_count );
    var_column_count = NULL;

    CHECK_OBJECT( (PyObject *)var_completions_per_page );
    Py_DECREF( var_completions_per_page );
    var_completions_per_page = NULL;

    CHECK_OBJECT( (PyObject *)var_page_count );
    Py_DECREF( var_page_count );
    var_page_count = NULL;

    CHECK_OBJECT( (PyObject *)var_display );
    Py_DECREF( var_display );
    var_display = NULL;

    Py_XDECREF( var_run_compl );
    var_run_compl = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_locals {
    PyObject *var_c;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_c = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_869eef7eb6c43f75799efd7a1898cdea, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 88;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_c;
            generator_heap->var_c = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_c );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_get_cwidth );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_cwidth );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_cwidth" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 88;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_c );
        tmp_source_name_1 = generator_heap->var_c;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_display_text );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 88;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 88;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_expression_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 88;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 88;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 88;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_c
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_34d8419347ad40f3624660c365ce6d87,
#endif
        codeobj_869eef7eb6c43f75799efd7a1898cdea,
        1,
        sizeof(struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_page = python_pars[ 0 ];
    PyObject *var_page_completions = NULL;
    PyObject *var_page_row_count = NULL;
    PyObject *var_page_columns = NULL;
    PyObject *var_result = NULL;
    PyObject *var_r = NULL;
    PyObject *var_c = NULL;
    PyObject *var_completion = NULL;
    PyObject *var_style = NULL;
    PyObject *var_padding = NULL;
    PyObject *outline_0_var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_a6b1c03abe1fa3b0bd0bdb90477ece7f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_41b35b457c6378513a2a778231c348e0_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_41b35b457c6378513a2a778231c348e0_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_a6b1c03abe1fa3b0bd0bdb90477ece7f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a6b1c03abe1fa3b0bd0bdb90477ece7f, codeobj_a6b1c03abe1fa3b0bd0bdb90477ece7f, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a6b1c03abe1fa3b0bd0bdb90477ece7f = cache_frame_a6b1c03abe1fa3b0bd0bdb90477ece7f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_step_name_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "completions" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = PyCell_GET( self->m_closure[2] );
        CHECK_OBJECT( par_page );
        tmp_left_name_1 = par_page;
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "completions_per_page" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_right_name_1 = PyCell_GET( self->m_closure[3] );
        tmp_start_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_start_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_page );
        tmp_left_name_3 = par_page;
        tmp_right_name_2 = const_int_pos_1;
        tmp_left_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_3, tmp_right_name_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_start_name_1 );

            exception_lineno = 97;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_start_name_1 );
            Py_DECREF( tmp_left_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "completions_per_page" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_right_name_3 = PyCell_GET( self->m_closure[3] );
        tmp_stop_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_stop_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_start_name_1 );

            exception_lineno = 97;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        Py_DECREF( tmp_start_name_1 );
        Py_DECREF( tmp_stop_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        assert( var_page_completions == NULL );
        var_page_completions = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_int_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_4;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_right_name_4;
        PyObject *tmp_float_arg_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_math );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_math );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "math" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ceil );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_page_completions );
        tmp_len_arg_1 = var_page_completions;
        tmp_left_name_4 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "column_count" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_float_arg_1 = PyCell_GET( self->m_closure[1] );
        tmp_right_name_4 = TO_FLOAT( tmp_float_arg_1 );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_4 );

            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_TRUEDIV_LONG_OBJECT( tmp_left_name_4, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_4 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_int_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_int_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = PyNumber_Int( tmp_int_arg_1 );
        Py_DECREF( tmp_int_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        assert( var_page_row_count == NULL );
        var_page_row_count = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_xrange_low_1;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "column_count" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 101;
                type_description_1 = "oooooooooocccccc";
                goto try_except_handler_2;
            }

            tmp_xrange_low_1 = PyCell_GET( self->m_closure[1] );
            tmp_iter_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_1 = "oooooooooocccccc";
                goto try_except_handler_2;
            }
            tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooooooooocccccc";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_41b35b457c6378513a2a778231c348e0_2, codeobj_41b35b457c6378513a2a778231c348e0, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *)+sizeof(void *) );
        frame_41b35b457c6378513a2a778231c348e0_2 = cache_frame_41b35b457c6378513a2a778231c348e0_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_41b35b457c6378513a2a778231c348e0_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_41b35b457c6378513a2a778231c348e0_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "ooo";
                    exception_lineno = 100;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_7 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_i;
                outline_0_var_i = tmp_assign_source_7;
                Py_INCREF( outline_0_var_i );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_start_name_2;
            PyObject *tmp_left_name_5;
            PyObject *tmp_right_name_5;
            PyObject *tmp_stop_name_2;
            PyObject *tmp_left_name_6;
            PyObject *tmp_left_name_7;
            PyObject *tmp_right_name_6;
            PyObject *tmp_right_name_7;
            PyObject *tmp_step_name_2;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( var_page_completions );
            tmp_subscribed_name_2 = var_page_completions;
            CHECK_OBJECT( outline_0_var_i );
            tmp_left_name_5 = outline_0_var_i;
            CHECK_OBJECT( var_page_row_count );
            tmp_right_name_5 = var_page_row_count;
            tmp_start_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_5 );
            if ( tmp_start_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( outline_0_var_i );
            tmp_left_name_7 = outline_0_var_i;
            tmp_right_name_6 = const_int_pos_1;
            tmp_left_name_6 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_7, tmp_right_name_6 );
            if ( tmp_left_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_start_name_2 );

                exception_lineno = 100;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_page_row_count );
            tmp_right_name_7 = var_page_row_count;
            tmp_stop_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_7 );
            Py_DECREF( tmp_left_name_6 );
            if ( tmp_stop_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_start_name_2 );

                exception_lineno = 100;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            tmp_step_name_2 = Py_None;
            tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
            Py_DECREF( tmp_start_name_2 );
            Py_DECREF( tmp_stop_name_2 );
            assert( !(tmp_subscript_name_2 == NULL) );
            tmp_append_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_2 = "ooo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_3 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_3 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_41b35b457c6378513a2a778231c348e0_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_41b35b457c6378513a2a778231c348e0_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_41b35b457c6378513a2a778231c348e0_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_41b35b457c6378513a2a778231c348e0_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_41b35b457c6378513a2a778231c348e0_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_41b35b457c6378513a2a778231c348e0_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_41b35b457c6378513a2a778231c348e0_2,
            type_description_2,
            outline_0_var_i,
            var_page_completions,
            var_page_row_count
        );


        // Release cached frame.
        if ( frame_41b35b457c6378513a2a778231c348e0_2 == cache_frame_41b35b457c6378513a2a778231c348e0_2 )
        {
            Py_DECREF( frame_41b35b457c6378513a2a778231c348e0_2 );
        }
        cache_frame_41b35b457c6378513a2a778231c348e0_2 = NULL;

        assertFrameObject( frame_41b35b457c6378513a2a778231c348e0_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oooooooooocccccc";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_i );
        outline_0_var_i = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_i );
        outline_0_var_i = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display );
        return NULL;
        outline_exception_1:;
        exception_lineno = 100;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_page_columns == NULL );
        var_page_columns = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = PyList_New( 0 );
        assert( var_result == NULL );
        var_result = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_xrange_low_2;
        CHECK_OBJECT( var_page_row_count );
        tmp_xrange_low_2 = var_page_row_count;
        tmp_iter_arg_2 = BUILTIN_XRANGE1( tmp_xrange_low_2 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_9;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_2 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooocccccc";
                exception_lineno = 105;
                goto try_except_handler_4;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_11 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_r;
            var_r = tmp_assign_source_11;
            Py_INCREF( var_r );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_xrange_low_3;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "column_count" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_4;
        }

        tmp_xrange_low_3 = PyCell_GET( self->m_closure[1] );
        tmp_iter_arg_3 = BUILTIN_XRANGE1( tmp_xrange_low_3 );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_4;
        }
        tmp_assign_source_12 = MAKE_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_3:;
    {
        PyObject *tmp_next_source_3;
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_3 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_13 = ITERATOR_NEXT( tmp_next_source_3 );
        if ( tmp_assign_source_13 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_3;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooocccccc";
                exception_lineno = 106;
                goto try_except_handler_5;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_13;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_14;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_14 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_c;
            var_c = tmp_assign_source_14;
            Py_INCREF( var_c );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_subscript_name_4;
        CHECK_OBJECT( var_page_columns );
        tmp_subscribed_name_4 = var_page_columns;
        CHECK_OBJECT( var_c );
        tmp_subscript_name_3 = var_c;
        tmp_subscribed_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_3 );
        if ( tmp_subscribed_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        CHECK_OBJECT( var_r );
        tmp_subscript_name_4 = var_r;
        tmp_assign_source_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_4 );
        Py_DECREF( tmp_subscribed_name_3 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        {
            PyObject *old = var_completion;
            var_completion = tmp_assign_source_15;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_left_name_8;
        PyObject *tmp_right_name_8;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_source_name_2;
        tmp_left_name_8 = const_str_digest_b60299e041cdfd0035ec0a9bae28826f;
        CHECK_OBJECT( var_completion );
        tmp_source_name_2 = var_completion;
        tmp_or_left_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_style );
        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 109;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        tmp_or_right_value_1 = const_str_empty;
        Py_INCREF( tmp_or_right_value_1 );
        tmp_right_name_8 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_right_name_8 = tmp_or_left_value_1;
        or_end_1:;
        tmp_assign_source_16 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_8, tmp_right_name_8 );
        Py_DECREF( tmp_right_name_8 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        {
            PyObject *old = var_style;
            var_style = tmp_assign_source_16;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        CHECK_OBJECT( var_result );
        tmp_source_name_3 = var_result;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_extend );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        if ( PyCell_GET( self->m_closure[5] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "to_formatted_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }

        tmp_called_name_3 = PyCell_GET( self->m_closure[5] );
        CHECK_OBJECT( var_completion );
        tmp_source_name_4 = var_completion;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_display );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 111;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_style;
        CHECK_OBJECT( var_style );
        tmp_dict_value_1 = var_style;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 111;
        tmp_args_element_name_2 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 111;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_left_name_9;
        PyObject *tmp_right_name_9;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_5;
        if ( PyCell_GET( self->m_closure[4] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "max_compl_width" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }

        tmp_left_name_9 = PyCell_GET( self->m_closure[4] );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_get_cwidth );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_cwidth );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_cwidth" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }

        tmp_called_name_4 = tmp_mvar_value_2;
        CHECK_OBJECT( var_completion );
        tmp_source_name_5 = var_completion;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_display_text );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 114;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_right_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_right_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        tmp_assign_source_17 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_9, tmp_right_name_9 );
        Py_DECREF( tmp_right_name_9 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        {
            PyObject *old = var_padding;
            var_padding = tmp_assign_source_17;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_source_name_7;
        PyObject *tmp_left_name_10;
        PyObject *tmp_right_name_10;
        CHECK_OBJECT( var_result );
        tmp_source_name_6 = var_result;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_append );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        CHECK_OBJECT( var_completion );
        tmp_source_name_7 = var_completion;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_style );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 115;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        tmp_args_element_name_4 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_args_element_name_4, 0, tmp_tuple_element_2 );
        tmp_left_name_10 = const_str_space;
        CHECK_OBJECT( var_padding );
        tmp_right_name_10 = var_padding;
        tmp_tuple_element_2 = BINARY_OPERATION_MUL_UNICODE_OBJECT( tmp_left_name_10, tmp_right_name_10 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 115;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        PyTuple_SET_ITEM( tmp_args_element_name_4, 1, tmp_tuple_element_2 );
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_6;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_a6b1c03abe1fa3b0bd0bdb90477ece7f, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_IndexError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_7;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_7;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 107;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame) frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooooocccccc";
        goto try_except_handler_7;
        branch_no_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_5;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display );
    return NULL;
    // End of try:
    try_end_1:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 106;
        type_description_1 = "oooooooooocccccc";
        goto try_except_handler_5;
    }
    goto loop_start_3;
    loop_end_3:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_4;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_3;
        CHECK_OBJECT( var_result );
        tmp_called_instance_1 = var_result;
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 118;
        tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_tuple_str_empty_str_newline_tuple_tuple, 0 ) );

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "oooooooooocccccc";
            goto try_except_handler_4;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;
        type_description_1 = "oooooooooocccccc";
        goto try_except_handler_4;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_8;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_7;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = PyCell_GET( self->m_closure[0] );
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_print_text );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[5] ) == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "to_formatted_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = PyCell_GET( self->m_closure[5] );
        CHECK_OBJECT( var_result );
        tmp_args_element_name_6 = var_result;
        tmp_args_element_name_7 = const_str_digest_ccb3493770ba2106c1d4478a247c7325;
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 120;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooooooocccccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a6b1c03abe1fa3b0bd0bdb90477ece7f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a6b1c03abe1fa3b0bd0bdb90477ece7f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a6b1c03abe1fa3b0bd0bdb90477ece7f,
        type_description_1,
        par_page,
        var_page_completions,
        var_page_row_count,
        var_page_columns,
        var_result,
        var_r,
        var_c,
        var_completion,
        var_style,
        var_padding,
        self->m_closure[2],
        self->m_closure[3],
        self->m_closure[1],
        self->m_closure[5],
        self->m_closure[4],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f == cache_frame_a6b1c03abe1fa3b0bd0bdb90477ece7f )
    {
        Py_DECREF( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f );
    }
    cache_frame_a6b1c03abe1fa3b0bd0bdb90477ece7f = NULL;

    assertFrameObject( frame_a6b1c03abe1fa3b0bd0bdb90477ece7f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_page );
    Py_DECREF( par_page );
    par_page = NULL;

    CHECK_OBJECT( (PyObject *)var_page_completions );
    Py_DECREF( var_page_completions );
    var_page_completions = NULL;

    CHECK_OBJECT( (PyObject *)var_page_row_count );
    Py_DECREF( var_page_row_count );
    var_page_row_count = NULL;

    CHECK_OBJECT( (PyObject *)var_page_columns );
    Py_DECREF( var_page_columns );
    var_page_columns = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    Py_XDECREF( var_completion );
    var_completion = NULL;

    Py_XDECREF( var_style );
    var_style = NULL;

    Py_XDECREF( var_padding );
    var_padding = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_page );
    Py_DECREF( par_page );
    par_page = NULL;

    Py_XDECREF( var_page_completions );
    var_page_completions = NULL;

    Py_XDECREF( var_page_row_count );
    var_page_row_count = NULL;

    Py_XDECREF( var_page_columns );
    var_page_columns = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    Py_XDECREF( var_completion );
    var_completion = NULL;

    Py_XDECREF( var_style );
    var_style = NULL;

    Py_XDECREF( var_padding );
    var_padding = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = self->m_closure[0];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = self->m_closure[1];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] = self->m_closure[2];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] = self->m_closure[3];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[4] = self->m_closure[4];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[4] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[5] = self->m_closure[5];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[5] );


    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_locals {
    PyObject *var_confirm;
    PyObject *var_page;
    PyObject *var_show_more;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_locals *generator_heap = (struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_confirm = NULL;
    generator_heap->var_page = NULL;
    generator_heap->var_show_more = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;
    generator_heap->tmp_return_value = NULL;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_a76d00685e9c179822ab9bb2902ac15b, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "completions" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 125;
            generator_heap->type_description_1 = "ooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_len_arg_1 = PyCell_GET( generator->m_closure[1] );
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 125;
            generator_heap->type_description_1 = "ooocccccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( generator->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_compexpr_left_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "completions_per_page" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 125;
            generator_heap->type_description_1 = "ooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = PyCell_GET( generator->m_closure[2] );
        generator_heap->tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 125;
            generator_heap->type_description_1 = "ooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_expression_name_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_len_arg_2;
            PyObject *tmp_kw_name_1;
            if ( PyCell_GET( generator->m_closure[3] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "create_confirm_session" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 127;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = PyCell_GET( generator->m_closure[3] );
            tmp_source_name_2 = const_str_digest_b01490f6432779cab5be9465691d156a;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_format );
            assert( !(tmp_called_name_3 == NULL) );
            if ( PyCell_GET( generator->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "completions" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 128;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }

            tmp_len_arg_2 = PyCell_GET( generator->m_closure[1] );
            tmp_args_element_name_2 = BUILTIN_LEN( tmp_len_arg_2 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_called_name_3 );

                generator_heap->exception_lineno = 128;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            generator->m_frame->m_frame.f_lineno = 128;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 128;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            generator->m_frame->m_frame.f_lineno = 127;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_source_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_source_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 127;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_prompt );
            Py_DECREF( tmp_source_name_1 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 127;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_3d35255aaf18052dfa7f86009d060cd4 );
            generator->m_frame->m_frame.f_lineno = 127;
            tmp_expression_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 127;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_len_arg_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_len_arg_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 127;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = yield_return_value;
            assert( generator_heap->var_confirm == NULL );
            Py_INCREF( tmp_assign_source_1 );
            generator_heap->var_confirm = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_1;
            CHECK_OBJECT( generator_heap->var_confirm );
            tmp_truth_name_1 = CHECK_IF_TRUE( generator_heap->var_confirm );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 131;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_xrange_low_1;
                if ( PyCell_GET( generator->m_closure[5] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "page_count" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 133;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto frame_exception_exit_1;
                }

                tmp_xrange_low_1 = PyCell_GET( generator->m_closure[5] );
                tmp_iter_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
                if ( tmp_iter_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 133;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 133;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto frame_exception_exit_1;
                }
                assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
                generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_2;
            }
            // Tried code:
            loop_start_1:;
            {
                PyObject *tmp_next_source_1;
                PyObject *tmp_assign_source_3;
                CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
                tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
                tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_1;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_1 = "ooocccccc";
                        generator_heap->exception_lineno = 133;
                        goto try_except_handler_2;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
                    generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_3;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_4;
                CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
                tmp_assign_source_4 = generator_heap->tmp_for_loop_1__iter_value;
                {
                    PyObject *old = generator_heap->var_page;
                    generator_heap->var_page = tmp_assign_source_4;
                    Py_INCREF( generator_heap->var_page );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_3;
                if ( PyCell_GET( generator->m_closure[4] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "display" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 134;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto try_except_handler_2;
                }

                tmp_called_name_4 = PyCell_GET( generator->m_closure[4] );
                CHECK_OBJECT( generator_heap->var_page );
                tmp_args_element_name_3 = generator_heap->var_page;
                generator->m_frame->m_frame.f_lineno = 134;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 134;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                CHECK_OBJECT( generator_heap->var_page );
                tmp_compexpr_left_2 = generator_heap->var_page;
                if ( PyCell_GET( generator->m_closure[5] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "page_count" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 136;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto try_except_handler_2;
                }

                tmp_left_name_1 = PyCell_GET( generator->m_closure[5] );
                tmp_right_name_1 = const_int_pos_1;
                tmp_compexpr_right_2 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_compexpr_right_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 136;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto try_except_handler_2;
                }
                generator_heap->tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                Py_DECREF( tmp_compexpr_right_2 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 136;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto try_except_handler_2;
                }
                tmp_condition_result_3 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_5;
                    PyObject *tmp_expression_name_2;
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_source_name_3;
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_mvar_value_1;
                    PyObject *tmp_kw_name_2;
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain__create_more_session );

                    if (unlikely( tmp_mvar_value_1 == NULL ))
                    {
                        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__create_more_session );
                    }

                    if ( tmp_mvar_value_1 == NULL )
                    {

                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_create_more_session" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 138;
                        generator_heap->type_description_1 = "ooocccccc";
                        goto try_except_handler_2;
                    }

                    tmp_called_name_6 = tmp_mvar_value_1;
                    generator->m_frame->m_frame.f_lineno = 138;
                    tmp_source_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_digest_23527169a10f84527c194a07b93c92bb_tuple, 0 ) );

                    if ( tmp_source_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 138;
                        generator_heap->type_description_1 = "ooocccccc";
                        goto try_except_handler_2;
                    }
                    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_prompt );
                    Py_DECREF( tmp_source_name_3 );
                    if ( tmp_called_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 138;
                        generator_heap->type_description_1 = "ooocccccc";
                        goto try_except_handler_2;
                    }
                    tmp_kw_name_2 = PyDict_Copy( const_dict_3d35255aaf18052dfa7f86009d060cd4 );
                    generator->m_frame->m_frame.f_lineno = 138;
                    tmp_expression_name_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_2 );
                    Py_DECREF( tmp_called_name_5 );
                    Py_DECREF( tmp_kw_name_2 );
                    if ( tmp_expression_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 138;
                        generator_heap->type_description_1 = "ooocccccc";
                        goto try_except_handler_2;
                    }
                    Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), &tmp_called_name_5, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_called_name_6, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_kw_name_2, sizeof(PyObject *), NULL );
                    generator->m_yield_return_index = 2;
                    return tmp_expression_name_2;
                    yield_return_2:
                    Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), &tmp_called_name_5, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_called_name_6, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_kw_name_2, sizeof(PyObject *), NULL );
                    if ( yield_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 138;
                        generator_heap->type_description_1 = "ooocccccc";
                        goto try_except_handler_2;
                    }
                    tmp_assign_source_5 = yield_return_value;
                    {
                        PyObject *old = generator_heap->var_show_more;
                        generator_heap->var_show_more = tmp_assign_source_5;
                        Py_INCREF( generator_heap->var_show_more );
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_operand_name_1;
                    CHECK_OBJECT( generator_heap->var_show_more );
                    tmp_operand_name_1 = generator_heap->var_show_more;
                    generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 140;
                        generator_heap->type_description_1 = "ooocccccc";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_4 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    generator_heap->tmp_return_value = Py_None;
                    Py_INCREF( generator_heap->tmp_return_value );
                    goto try_return_handler_2;
                    branch_no_4:;
                }
                branch_no_3:;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 133;
                generator_heap->type_description_1 = "ooocccccc";
                goto try_except_handler_2;
            }
            goto loop_start_1;
            loop_end_1:;
            goto try_end_1;
            // Return handler code:
            try_return_handler_2:;
            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__iter_value );
            Py_DECREF( generator_heap->tmp_for_loop_1__iter_value );
            generator_heap->tmp_for_loop_1__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
            generator_heap->tmp_for_loop_1__for_iterator = NULL;

            goto frame_return_exit_1;
            // Exception handler code:
            try_except_handler_2:;
            generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
            generator_heap->tmp_for_loop_1__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
            generator_heap->tmp_for_loop_1__for_iterator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_1;
            generator_heap->exception_value = generator_heap->exception_keeper_value_1;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

            goto frame_exception_exit_1;
            // End of try:
            try_end_1:;
            Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
            generator_heap->tmp_for_loop_1__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
            generator_heap->tmp_for_loop_1__for_iterator = NULL;

            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_call_result_2;
                if ( PyCell_GET( generator->m_closure[0] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 143;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_4 = PyCell_GET( generator->m_closure[0] );
                tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_output );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 143;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto frame_exception_exit_1;
                }
                generator->m_frame->m_frame.f_lineno = 143;
                tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_flush );
                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 143;
                    generator_heap->type_description_1 = "ooocccccc";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_end_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_call_result_3;
            if ( PyCell_GET( generator->m_closure[4] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "display" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 146;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_7 = PyCell_GET( generator->m_closure[4] );
            generator->m_frame->m_frame.f_lineno = 146;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 146;
                generator_heap->type_description_1 = "ooocccccc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_end_1:;
    }

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator_heap->var_confirm,
            generator_heap->var_page,
            generator_heap->var_show_more,
            generator->m_closure[1],
            generator->m_closure[2],
            generator->m_closure[3],
            generator->m_closure[5],
            generator->m_closure[4],
            generator->m_closure[0]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)generator_heap->var_confirm );
    Py_DECREF( generator_heap->var_confirm );
    generator_heap->var_confirm = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_page );
    Py_DECREF( generator_heap->var_page );
    generator_heap->var_page = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_show_more );
    Py_DECREF( generator_heap->var_show_more );
    generator_heap->var_show_more = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_confirm );
    generator_heap->var_confirm = NULL;

    Py_XDECREF( generator_heap->var_page );
    generator_heap->var_page = NULL;

    Py_XDECREF( generator_heap->var_show_more );
    generator_heap->var_show_more = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->var_confirm );
    generator_heap->var_confirm = NULL;

    Py_XDECREF( generator_heap->var_page );
    generator_heap->var_page = NULL;

    Py_XDECREF( generator_heap->var_show_more );
    generator_heap->var_show_more = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;
    // The above won't return, but we need to make it clear to the compiler
    // as well, or else it will complain and/or generate inferior code.
    assert(false);
    return NULL;

    function_return_exit:
#if PYTHON_VERSION >= 300
    generator->m_returned = generator_heap->tmp_return_value;
#endif

    return NULL;

}

static PyObject *prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_context,
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_plain_run_compl,
#if PYTHON_VERSION >= 350
        const_str_digest_8e9cb0df41bf6407b4d4434b9be1ef3d,
#endif
        codeobj_a76d00685e9c179822ab9bb2902ac15b,
        6,
        sizeof(struct prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl$$$genobj_1_run_compl_locals)
    );
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_message = python_pars[ 0 ];
    PyObject *var_PromptSession = NULL;
    PyObject *var_bindings = NULL;
    PyObject *var__ = NULL;
    PyObject *var_session = NULL;
    struct Nuitka_FrameObject *frame_4fb91f90eee259ddfe98fafe720d6141;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_4fb91f90eee259ddfe98fafe720d6141 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4fb91f90eee259ddfe98fafe720d6141, codeobj_4fb91f90eee259ddfe98fafe720d6141, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4fb91f90eee259ddfe98fafe720d6141 = cache_frame_4fb91f90eee259ddfe98fafe720d6141;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4fb91f90eee259ddfe98fafe720d6141 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4fb91f90eee259ddfe98fafe720d6141 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_892d8dcf187eb812198632660c4fd2f6;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_PromptSession_tuple;
        tmp_level_name_1 = const_int_0;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 155;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_PromptSession );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_PromptSession == NULL );
        var_PromptSession = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_KeyBindings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KeyBindings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KeyBindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 156;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_bindings == NULL );
        var_bindings = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_7;
        PyObject *tmp_called_name_8;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_9;
        PyObject *tmp_called_name_10;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_9;
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_1 = var_bindings;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 158;
        tmp_called_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_space_tuple, 0 ) );

        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_2 = var_bindings;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 159;
        tmp_called_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_plain_y_tuple, 0 ) );

        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 159;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_3 = var_bindings;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 160;
        tmp_called_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_plain_Y_tuple, 0 ) );

        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 160;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_source_name_1 = var_bindings;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_ControlJ );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 161;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_called_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_source_name_3 = var_bindings;
        tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_add );
        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_called_name_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_3;
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_ControlM );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_called_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_source_name_5 = var_bindings;
        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_add );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 163;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_called_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_4;
        tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_ControlI );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 163;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_called_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 163;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_9 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_1__(  );



        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 163;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 161;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 160;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 159;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 159;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 158;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_11;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_12;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_13;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_14;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_called_name_15;
        PyObject *tmp_called_name_16;
        PyObject *tmp_source_name_7;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_15;
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_4 = var_bindings;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 167;
        tmp_called_name_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_plain_n_tuple, 0 ) );

        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_5 = var_bindings;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 168;
        tmp_called_name_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_plain_N_tuple, 0 ) );

        if ( tmp_called_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );

            exception_lineno = 168;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_6 = var_bindings;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 169;
        tmp_called_name_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_plain_q_tuple, 0 ) );

        if ( tmp_called_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );

            exception_lineno = 169;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_called_instance_7 = var_bindings;
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 170;
        tmp_called_name_14 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_add, &PyTuple_GET_ITEM( const_tuple_str_plain_Q_tuple, 0 ) );

        if ( tmp_called_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_13 );

            exception_lineno = 170;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bindings );
        tmp_source_name_7 = var_bindings;
        tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_add );
        if ( tmp_called_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 171;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_5;
        tmp_args_element_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_ControlC );
        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );

            exception_lineno = 171;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 171;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_called_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_called_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 171;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_15 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_2__(  );



        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 171;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_called_name_15 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 171;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 170;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_13 );

            exception_lineno = 170;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 169;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_called_name_13 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_called_name_12 );

            exception_lineno = 169;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 168;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );

            exception_lineno = 168;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 167;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_4;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_17;
        PyObject *tmp_called_name_18;
        PyObject *tmp_source_name_9;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_17;
        CHECK_OBJECT( var_bindings );
        tmp_source_name_9 = var_bindings;
        tmp_called_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_add );
        if ( tmp_called_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_6;
        tmp_args_element_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_Any );
        if ( tmp_args_element_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_18 );

            exception_lineno = 175;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_called_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_called_name_18 );
        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_called_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_17 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_3__(  );



        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_17 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_called_name_17 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_5;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_19;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        CHECK_OBJECT( var_PromptSession );
        tmp_called_name_19 = var_PromptSession;
        CHECK_OBJECT( par_message );
        tmp_tuple_element_1 = par_message;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_key_bindings;
        CHECK_OBJECT( var_bindings );
        tmp_dict_value_1 = var_bindings;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_erase_when_done;
        tmp_dict_value_2 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_4fb91f90eee259ddfe98fafe720d6141->m_frame.f_lineno = 179;
        tmp_assign_source_6 = CALL_FUNCTION( tmp_called_name_19, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_session == NULL );
        var_session = tmp_assign_source_6;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fb91f90eee259ddfe98fafe720d6141 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fb91f90eee259ddfe98fafe720d6141 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4fb91f90eee259ddfe98fafe720d6141, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4fb91f90eee259ddfe98fafe720d6141->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4fb91f90eee259ddfe98fafe720d6141, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4fb91f90eee259ddfe98fafe720d6141,
        type_description_1,
        par_message,
        var_PromptSession,
        var_bindings,
        var__,
        var_session
    );


    // Release cached frame.
    if ( frame_4fb91f90eee259ddfe98fafe720d6141 == cache_frame_4fb91f90eee259ddfe98fafe720d6141 )
    {
        Py_DECREF( frame_4fb91f90eee259ddfe98fafe720d6141 );
    }
    cache_frame_4fb91f90eee259ddfe98fafe720d6141 = NULL;

    assertFrameObject( frame_4fb91f90eee259ddfe98fafe720d6141 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_session );
    tmp_return_value = var_session;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)var_PromptSession );
    Py_DECREF( var_PromptSession );
    var_PromptSession = NULL;

    CHECK_OBJECT( (PyObject *)var_bindings );
    Py_DECREF( var_bindings );
    var_bindings = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    CHECK_OBJECT( (PyObject *)var_session );
    Py_DECREF( var_session );
    var_session = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    Py_XDECREF( var_PromptSession );
    var_PromptSession = NULL;

    Py_XDECREF( var_bindings );
    var_bindings = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_1__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6176dd9c65729e0528d8ef7c4fa87a2c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6176dd9c65729e0528d8ef7c4fa87a2c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6176dd9c65729e0528d8ef7c4fa87a2c, codeobj_6176dd9c65729e0528d8ef7c4fa87a2c, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *) );
    frame_6176dd9c65729e0528d8ef7c4fa87a2c = cache_frame_6176dd9c65729e0528d8ef7c4fa87a2c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6176dd9c65729e0528d8ef7c4fa87a2c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6176dd9c65729e0528d8ef7c4fa87a2c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_23ccf90c8ca98c47117f7093afc959ff );
        frame_6176dd9c65729e0528d8ef7c4fa87a2c->m_frame.f_lineno = 165;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6176dd9c65729e0528d8ef7c4fa87a2c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6176dd9c65729e0528d8ef7c4fa87a2c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6176dd9c65729e0528d8ef7c4fa87a2c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6176dd9c65729e0528d8ef7c4fa87a2c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6176dd9c65729e0528d8ef7c4fa87a2c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6176dd9c65729e0528d8ef7c4fa87a2c,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_6176dd9c65729e0528d8ef7c4fa87a2c == cache_frame_6176dd9c65729e0528d8ef7c4fa87a2c )
    {
        Py_DECREF( frame_6176dd9c65729e0528d8ef7c4fa87a2c );
    }
    cache_frame_6176dd9c65729e0528d8ef7c4fa87a2c = NULL;

    assertFrameObject( frame_6176dd9c65729e0528d8ef7c4fa87a2c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_1__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_1__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_2__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_762465262e16acce09534825e13dde4d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_762465262e16acce09534825e13dde4d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_762465262e16acce09534825e13dde4d, codeobj_762465262e16acce09534825e13dde4d, module_prompt_toolkit$key_binding$bindings$completion, sizeof(void *) );
    frame_762465262e16acce09534825e13dde4d = cache_frame_762465262e16acce09534825e13dde4d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_762465262e16acce09534825e13dde4d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_762465262e16acce09534825e13dde4d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_exit );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_d9f37cf1134b2ef5e7e66a4fa42a97ac );
        frame_762465262e16acce09534825e13dde4d->m_frame.f_lineno = 173;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_762465262e16acce09534825e13dde4d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_762465262e16acce09534825e13dde4d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_762465262e16acce09534825e13dde4d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_762465262e16acce09534825e13dde4d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_762465262e16acce09534825e13dde4d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_762465262e16acce09534825e13dde4d,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_762465262e16acce09534825e13dde4d == cache_frame_762465262e16acce09534825e13dde4d )
    {
        Py_DECREF( frame_762465262e16acce09534825e13dde4d );
    }
    cache_frame_762465262e16acce09534825e13dde4d = NULL;

    assertFrameObject( frame_762465262e16acce09534825e13dde4d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_2__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_2__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_3__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_3__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_3__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_1_generate_completions(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_1_generate_completions,
        const_str_plain_generate_completions,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_10e94fe3f0f3c0523e41f72f5e8b9f5d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_digest_eaa120300f50b01b6bc94cdc3a2117e6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_2_display_completions_like_readline(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_2_display_completions_like_readline,
        const_str_plain_display_completions_like_readline,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a643590f1bc39c9e6a3c29b1fd6e5859,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_digest_ac511e3a0ece86ea5b9308c7fa96f1a6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline,
        const_str_plain__display_completions_like_readline,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6fd261609a458f9a8d6e6c0a22417c6c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_digest_9fc3e803d5f2a48b56f3f7313dc6e9e3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_1_display,
        const_str_plain_display,
#if PYTHON_VERSION >= 300
        const_str_digest_1221d40c14a917ca64d9799e0fd8eb08,
#endif
        codeobj_a6b1c03abe1fa3b0bd0bdb90477ece7f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        NULL,
        6
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline$$$function_2_run_compl,
        const_str_plain_run_compl,
#if PYTHON_VERSION >= 300
        const_str_digest_8e9cb0df41bf6407b4d4434b9be1ef3d,
#endif
        codeobj_a76d00685e9c179822ab9bb2902ac15b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_digest_ea27cb142036902290d97ce95d960d15,
        6
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session,
        const_str_plain__create_more_session,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4fb91f90eee259ddfe98fafe720d6141,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_digest_419d1721aaeb7914f7b6216d4ecba0b0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_1__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_1__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_072ff487f78b29b4a1d056547254a8d3,
#endif
        codeobj_6176dd9c65729e0528d8ef7c4fa87a2c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_2__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_2__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_072ff487f78b29b4a1d056547254a8d3,
#endif
        codeobj_762465262e16acce09534825e13dde4d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_3__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session$$$function_3__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_072ff487f78b29b4a1d056547254a8d3,
#endif
        codeobj_a457fc0b896f4d20ec9850253c4f6bb0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$completion,
        const_str_digest_632d63e94fd0db87b5234c4ce51d9369,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$key_binding$bindings$completion =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.key_binding.bindings.completion",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$completion)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$key_binding$bindings$completion)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$completion );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.completion: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.completion: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.completion: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$key_binding$bindings$completion" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$key_binding$bindings$completion = Py_InitModule4(
        "prompt_toolkit.key_binding.bindings.completion",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$key_binding$bindings$completion = PyModule_Create( &mdef_prompt_toolkit$key_binding$bindings$completion );
#endif

    moduledict_prompt_toolkit$key_binding$bindings$completion = MODULE_DICT( module_prompt_toolkit$key_binding$bindings$completion );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$key_binding$bindings$completion,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$key_binding$bindings$completion,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$completion,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$completion,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$key_binding$bindings$completion );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_01e95d2aac58530e0f23e21a1e50445c, module_prompt_toolkit$key_binding$bindings$completion );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_34e80bb0bbf944f22f9ecf062c53ca52;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_4689ddb1b17648707196b6e484a13af6;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_34e80bb0bbf944f22f9ecf062c53ca52 = MAKE_MODULE_FRAME( codeobj_34e80bb0bbf944f22f9ecf062c53ca52, module_prompt_toolkit$key_binding$bindings$completion );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_34e80bb0bbf944f22f9ecf062c53ca52 );
    assert( Py_REFCNT( frame_34e80bb0bbf944f22f9ecf062c53ca52 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame.f_lineno = 4;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_ebac526994d665b2afb9731ed9b7a23c;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_run_coroutine_in_terminal_tuple;
        tmp_level_name_1 = const_int_0;
        frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame.f_lineno = 5;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_run_coroutine_in_terminal );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_run_coroutine_in_terminal, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_e371fcb47bb33b8f0db02b1ad9764d6e;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_CompleteEvent_str_plain_get_common_complete_suffix_tuple;
        tmp_level_name_2 = const_int_0;
        frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame.f_lineno = 6;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_CompleteEvent );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_CompleteEvent, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_get_common_complete_suffix );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_get_common_complete_suffix, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_6695b0a823c8cd6b75b68a2528261696;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_KeyBindings_tuple;
        tmp_level_name_3 = const_int_0;
        frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame.f_lineno = 7;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_KeyBindings );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_KeyBindings, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Keys_tuple;
        tmp_level_name_4 = const_int_0;
        frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame.f_lineno = 8;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_Keys );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_Keys, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_8cff9047d0926d732c79d282e1f19e8e;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_get_cwidth_tuple;
        tmp_level_name_5 = const_int_0;
        frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame.f_lineno = 9;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_get_cwidth );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_get_cwidth, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_math;
        tmp_globals_name_6 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$completion;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame.f_lineno = 11;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_math, tmp_assign_source_12 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_34e80bb0bbf944f22f9ecf062c53ca52 );
#endif
    popFrameStack();

    assertFrameObject( frame_34e80bb0bbf944f22f9ecf062c53ca52 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_34e80bb0bbf944f22f9ecf062c53ca52 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_34e80bb0bbf944f22f9ecf062c53ca52, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_34e80bb0bbf944f22f9ecf062c53ca52->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_34e80bb0bbf944f22f9ecf062c53ca52, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = LIST_COPY( const_list_4689138b69e94207c700df4456eb690b_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_1_generate_completions(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_generate_completions, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_2_display_completions_like_readline(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain_display_completions_like_readline, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_3__display_completions_like_readline(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain__display_completions_like_readline, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_str_digest_23527169a10f84527c194a07b93c92bb_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_17 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$completion$$$function_4__create_more_session( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$completion, (Nuitka_StringObject *)const_str_plain__create_more_session, tmp_assign_source_17 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$completion );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
