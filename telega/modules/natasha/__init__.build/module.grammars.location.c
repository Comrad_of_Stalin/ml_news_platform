/* Generated code for Python module 'grammars.location'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_grammars$location" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_grammars$location;
PyDictObject *moduledict_grammars$location;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_is_title;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_length_eq;
extern PyObject *const_str_plain_is_single;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_58d7183d75f6a1148d1baaae89c0fb64;
static PyObject *const_tuple_str_plain_Geox_tuple;
extern PyObject *const_str_plain_CONJ;
static PyObject *const_str_digest_105cd69f89139c3140ccc57b9497c4d7;
static PyObject *const_set_5f71e6c8975fa3da89451c27416c4e06;
extern PyObject *const_str_digest_6ad5950c518515e785cce7ace5787ec3;
static PyObject *const_set_039f612f114bd28b9b8a304073fb8632;
extern PyObject *const_tuple_str_plain_gent_tuple;
extern PyObject *const_str_digest_aeaeed9a368b5ddbb90f1b491cb19740;
extern PyObject *const_str_plain_Location;
extern PyObject *const_str_digest_d2699fa4d5aaa8f388031a8a1920ebb6;
static PyObject *const_tuple_4893302eac92a978f6457e86015205d2_tuple;
extern PyObject *const_str_digest_5aaebc7bd751170c841a9d6f763cad87;
static PyObject *const_tuple_str_digest_58d7183d75f6a1148d1baaae89c0fb64_tuple;
extern PyObject *const_str_digest_b3bf94470e6c2f83224ac9ae046cba22;
static PyObject *const_str_plain_STATE;
static PyObject *const_set_9f951b67d5c3ab53cac8b22001165cd1;
static PyObject *const_str_plain_FEDERATION;
extern PyObject *const_tuple_str_plain_ADJF_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_digest_20d15687798de5d0430828d30f6e8af6;
static PyObject *const_str_plain_REGION;
extern PyObject *const_str_plain_ADJF;
extern PyObject *const_str_plain_Abbr;
extern PyObject *const_str_digest_f39e036dd1eb5cb79affd7cea5212569;
extern PyObject *const_tuple_str_plain_fact_tuple;
static PyObject *const_str_plain_ADJX_FEDERATION;
extern PyObject *const_str_plain_gnc_relation;
extern PyObject *const_str_digest_552e6024e3641f7a089a225aa1b64e6a;
extern PyObject *const_str_digest_8e02063b573c8bd408940bbe38f2dce8;
extern PyObject *const_tuple_str_plain_gnc_relation_tuple;
static PyObject *const_set_046d91ed660b9014185a469cd4bed016;
static PyObject *const_str_digest_2b8e4f9d141252bdd47aa057222221a7;
extern PyObject *const_str_chr_45;
extern PyObject *const_tuple_str_digest_5aaebc7bd751170c841a9d6f763cad87_tuple;
static PyObject *const_str_plain_FEDERAL_DISTRICT;
extern PyObject *const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
static PyObject *const_str_plain_Adjx;
extern PyObject *const_str_plain_inflected;
extern PyObject *const_str_digest_4f5d0f8f163b29527c9a53da3c52494b;
extern PyObject *const_str_plain_or_;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_str_plain_CONJ_tuple;
extern PyObject *const_str_plain_gnc;
extern PyObject *const_str_plain_interpretation;
extern PyObject *const_str_digest_72c8e055e9bf11147dcc3c338c5aacd7;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_digest_3b0d1c4c904989a80e46e8a766322a96;
static PyObject *const_set_28b50f6213755b1a8a3a45e044122aa3;
static PyObject *const_tuple_str_digest_969bab7e4d3b9481c0b52aa8d031c72e_tuple;
extern PyObject *const_str_digest_ea78dcde38a5066c777d6274d25657b3;
static PyObject *const_tuple_str_plain_Adjx_tuple;
extern PyObject *const_str_plain_match;
static PyObject *const_tuple_str_plain_PRCL_tuple;
extern PyObject *const_str_plain_eq;
extern PyObject *const_str_plain_PREP;
extern PyObject *const_str_plain_yargy;
extern PyObject *const_str_digest_bb189f61d7bc8cb91c15ff3f11da22de;
extern PyObject *const_str_plain_gent;
extern PyObject *const_tuple_str_plain_rule_str_plain_and__str_plain_or__str_plain_not__tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_repeatable;
extern PyObject *const_str_digest_693d07d7cb25daa6f515f4392fdfe8a4;
static PyObject *const_tuple_str_plain_PREP_tuple;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_f54cf53eca31457d375c91970494f530;
extern PyObject *const_str_plain_fact;
extern PyObject *const_list_str_plain_name_list;
extern PyObject *const_str_plain_normalized;
static PyObject *const_str_digest_b1ab38dabe8f46f8b417e0907f33cbbc;
extern PyObject *const_tuple_str_plain_NOUN_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_caseless;
extern PyObject *const_str_digest_4260e6aa736e8889464d5ccf957cf64b;
extern PyObject *const_str_digest_d2a7a56a4d20853487c008ece870c4f1;
extern PyObject *const_str_plain_rule;
extern PyObject *const_str_digest_9b9f88a69761058092e67cd95c9e309c;
static PyObject *const_str_digest_31d95b046758b208685ce1121dbdf802;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_str_digest_969bab7e4d3b9481c0b52aa8d031c72e;
extern PyObject *const_str_plain_NOUN;
extern PyObject *const_str_plain_optional;
static PyObject *const_set_46db9eadb527a32db54c196b1a27bcac;
extern PyObject *const_str_digest_dd034e7ef4d7378a1261be73253b9ee7;
static PyObject *const_str_plain_AUTONOMOUS_DISTRICT;
extern PyObject *const_str_digest_a4a7cb20a721b4c6b06742cf329b73de;
extern PyObject *const_str_digest_1ec96010e167c03844fcda56ebdc4429;
extern PyObject *const_str_digest_3ab0ee19f367bbde60612dbeae3f5202;
static PyObject *const_str_digest_76b6e52ef0115a84aa2d6cf78b3c6ea6;
static PyObject *const_str_plain_LOCALITY;
extern PyObject *const_str_digest_ef3b803edd99f0b6e81c6dffa028f608;
static PyObject *const_str_plain_Geox;
extern PyObject *const_str_plain_not_;
static PyObject *const_set_a20ea88b302ba22117ebe215fbbc0485;
extern PyObject *const_str_plain_dictionary;
extern PyObject *const_str_plain_LOCATION;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_tuple_str_plain_Abbr_tuple;
static PyObject *const_str_digest_cbeec13121112163ee7509f1ff2dbd09;
static PyObject *const_set_5476be352a05047fdbdfb7cc2bdbc6ea;
extern PyObject *const_str_plain_and_;
extern PyObject *const_str_plain_PRCL;
static PyObject *const_set_52abd00c494ef6649d2050f530a5ed8a;
extern PyObject *const_str_plain_gram;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_58d7183d75f6a1148d1baaae89c0fb64 = UNSTREAM_STRING( &constant_bin[ 678493 ], 4, 0 );
    const_tuple_str_plain_Geox_tuple = PyTuple_New( 1 );
    const_str_plain_Geox = UNSTREAM_STRING_ASCII( &constant_bin[ 678497 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Geox_tuple, 0, const_str_plain_Geox ); Py_INCREF( const_str_plain_Geox );
    const_str_digest_105cd69f89139c3140ccc57b9497c4d7 = UNSTREAM_STRING_ASCII( &constant_bin[ 678501 ], 26, 0 );
    const_set_5f71e6c8975fa3da89451c27416c4e06 = PySet_New( NULL );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_6ad5950c518515e785cce7ace5787ec3 );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_4f5d0f8f163b29527c9a53da3c52494b );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_ef3b803edd99f0b6e81c6dffa028f608 );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_dd034e7ef4d7378a1261be73253b9ee7 );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_1ec96010e167c03844fcda56ebdc4429 );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_693d07d7cb25daa6f515f4392fdfe8a4 );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_9b9f88a69761058092e67cd95c9e309c );
    PySet_Add( const_set_5f71e6c8975fa3da89451c27416c4e06, const_str_digest_d2699fa4d5aaa8f388031a8a1920ebb6 );
    assert( PySet_Size( const_set_5f71e6c8975fa3da89451c27416c4e06 ) == 8 );
    const_set_039f612f114bd28b9b8a304073fb8632 = PySet_New( NULL );
    const_str_digest_76b6e52ef0115a84aa2d6cf78b3c6ea6 = UNSTREAM_STRING( &constant_bin[ 678527 ], 8, 0 );
    PySet_Add( const_set_039f612f114bd28b9b8a304073fb8632, const_str_digest_76b6e52ef0115a84aa2d6cf78b3c6ea6 );
    const_str_digest_31d95b046758b208685ce1121dbdf802 = UNSTREAM_STRING( &constant_bin[ 678535 ], 16, 0 );
    PySet_Add( const_set_039f612f114bd28b9b8a304073fb8632, const_str_digest_31d95b046758b208685ce1121dbdf802 );
    assert( PySet_Size( const_set_039f612f114bd28b9b8a304073fb8632 ) == 2 );
    const_tuple_4893302eac92a978f6457e86015205d2_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 0, const_str_plain_caseless ); Py_INCREF( const_str_plain_caseless );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 1, const_str_plain_normalized ); Py_INCREF( const_str_plain_normalized );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 2, const_str_plain_eq ); Py_INCREF( const_str_plain_eq );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 3, const_str_plain_length_eq ); Py_INCREF( const_str_plain_length_eq );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 4, const_str_plain_gram ); Py_INCREF( const_str_plain_gram );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 5, const_str_plain_dictionary ); Py_INCREF( const_str_plain_dictionary );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 6, const_str_plain_is_single ); Py_INCREF( const_str_plain_is_single );
    PyTuple_SET_ITEM( const_tuple_4893302eac92a978f6457e86015205d2_tuple, 7, const_str_plain_is_title ); Py_INCREF( const_str_plain_is_title );
    const_tuple_str_digest_58d7183d75f6a1148d1baaae89c0fb64_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_58d7183d75f6a1148d1baaae89c0fb64_tuple, 0, const_str_digest_58d7183d75f6a1148d1baaae89c0fb64 ); Py_INCREF( const_str_digest_58d7183d75f6a1148d1baaae89c0fb64 );
    const_str_plain_STATE = UNSTREAM_STRING_ASCII( &constant_bin[ 158971 ], 5, 1 );
    const_set_9f951b67d5c3ab53cac8b22001165cd1 = PySet_New( NULL );
    PySet_Add( const_set_9f951b67d5c3ab53cac8b22001165cd1, const_str_digest_76b6e52ef0115a84aa2d6cf78b3c6ea6 );
    const_str_digest_2b8e4f9d141252bdd47aa057222221a7 = UNSTREAM_STRING( &constant_bin[ 678551 ], 12, 0 );
    PySet_Add( const_set_9f951b67d5c3ab53cac8b22001165cd1, const_str_digest_2b8e4f9d141252bdd47aa057222221a7 );
    assert( PySet_Size( const_set_9f951b67d5c3ab53cac8b22001165cd1 ) == 2 );
    const_str_plain_FEDERATION = UNSTREAM_STRING_ASCII( &constant_bin[ 678563 ], 10, 1 );
    const_str_plain_REGION = UNSTREAM_STRING_ASCII( &constant_bin[ 678573 ], 6, 1 );
    const_str_plain_ADJX_FEDERATION = UNSTREAM_STRING_ASCII( &constant_bin[ 678579 ], 15, 1 );
    const_set_046d91ed660b9014185a469cd4bed016 = PySet_New( NULL );
    PySet_Add( const_set_046d91ed660b9014185a469cd4bed016, const_str_digest_bb189f61d7bc8cb91c15ff3f11da22de );
    const_str_digest_b1ab38dabe8f46f8b417e0907f33cbbc = UNSTREAM_STRING( &constant_bin[ 678594 ], 16, 0 );
    PySet_Add( const_set_046d91ed660b9014185a469cd4bed016, const_str_digest_b1ab38dabe8f46f8b417e0907f33cbbc );
    PySet_Add( const_set_046d91ed660b9014185a469cd4bed016, const_str_digest_4260e6aa736e8889464d5ccf957cf64b );
    PySet_Add( const_set_046d91ed660b9014185a469cd4bed016, const_str_digest_552e6024e3641f7a089a225aa1b64e6a );
    const_str_digest_f54cf53eca31457d375c91970494f530 = UNSTREAM_STRING( &constant_bin[ 678610 ], 8, 0 );
    PySet_Add( const_set_046d91ed660b9014185a469cd4bed016, const_str_digest_f54cf53eca31457d375c91970494f530 );
    assert( PySet_Size( const_set_046d91ed660b9014185a469cd4bed016 ) == 5 );
    const_str_plain_FEDERAL_DISTRICT = UNSTREAM_STRING_ASCII( &constant_bin[ 678618 ], 16, 1 );
    const_str_plain_Adjx = UNSTREAM_STRING_ASCII( &constant_bin[ 678634 ], 4, 1 );
    const_tuple_str_plain_CONJ_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_CONJ_tuple, 0, const_str_plain_CONJ ); Py_INCREF( const_str_plain_CONJ );
    const_set_28b50f6213755b1a8a3a45e044122aa3 = PySet_New( NULL );
    PySet_Add( const_set_28b50f6213755b1a8a3a45e044122aa3, const_str_digest_3ab0ee19f367bbde60612dbeae3f5202 );
    assert( PySet_Size( const_set_28b50f6213755b1a8a3a45e044122aa3 ) == 1 );
    const_tuple_str_digest_969bab7e4d3b9481c0b52aa8d031c72e_tuple = PyTuple_New( 1 );
    const_str_digest_969bab7e4d3b9481c0b52aa8d031c72e = UNSTREAM_STRING( &constant_bin[ 678638 ], 4, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_969bab7e4d3b9481c0b52aa8d031c72e_tuple, 0, const_str_digest_969bab7e4d3b9481c0b52aa8d031c72e ); Py_INCREF( const_str_digest_969bab7e4d3b9481c0b52aa8d031c72e );
    const_tuple_str_plain_Adjx_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Adjx_tuple, 0, const_str_plain_Adjx ); Py_INCREF( const_str_plain_Adjx );
    const_tuple_str_plain_PRCL_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PRCL_tuple, 0, const_str_plain_PRCL ); Py_INCREF( const_str_plain_PRCL );
    const_tuple_str_plain_PREP_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PREP_tuple, 0, const_str_plain_PREP ); Py_INCREF( const_str_plain_PREP );
    const_set_46db9eadb527a32db54c196b1a27bcac = PySet_New( NULL );
    PySet_Add( const_set_46db9eadb527a32db54c196b1a27bcac, const_str_digest_72c8e055e9bf11147dcc3c338c5aacd7 );
    PySet_Add( const_set_46db9eadb527a32db54c196b1a27bcac, const_str_digest_a4a7cb20a721b4c6b06742cf329b73de );
    PySet_Add( const_set_46db9eadb527a32db54c196b1a27bcac, const_str_digest_aeaeed9a368b5ddbb90f1b491cb19740 );
    assert( PySet_Size( const_set_46db9eadb527a32db54c196b1a27bcac ) == 3 );
    const_str_plain_AUTONOMOUS_DISTRICT = UNSTREAM_STRING_ASCII( &constant_bin[ 678642 ], 19, 1 );
    const_str_plain_LOCALITY = UNSTREAM_STRING_ASCII( &constant_bin[ 678661 ], 8, 1 );
    const_set_a20ea88b302ba22117ebe215fbbc0485 = PySet_New( NULL );
    PySet_Add( const_set_a20ea88b302ba22117ebe215fbbc0485, const_str_digest_8e02063b573c8bd408940bbe38f2dce8 );
    assert( PySet_Size( const_set_a20ea88b302ba22117ebe215fbbc0485 ) == 1 );
    const_str_digest_cbeec13121112163ee7509f1ff2dbd09 = UNSTREAM_STRING_ASCII( &constant_bin[ 678669 ], 20, 0 );
    const_set_5476be352a05047fdbdfb7cc2bdbc6ea = PySet_New( NULL );
    PySet_Add( const_set_5476be352a05047fdbdfb7cc2bdbc6ea, const_str_digest_f39e036dd1eb5cb79affd7cea5212569 );
    assert( PySet_Size( const_set_5476be352a05047fdbdfb7cc2bdbc6ea ) == 1 );
    const_set_52abd00c494ef6649d2050f530a5ed8a = PySet_New( NULL );
    PySet_Add( const_set_52abd00c494ef6649d2050f530a5ed8a, const_str_digest_b3bf94470e6c2f83224ac9ae046cba22 );
    PySet_Add( const_set_52abd00c494ef6649d2050f530a5ed8a, const_str_digest_20d15687798de5d0430828d30f6e8af6 );
    assert( PySet_Size( const_set_52abd00c494ef6649d2050f530a5ed8a ) == 2 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_grammars$location( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_c4b888b9d7f40a84ef779411b50c49e7;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_cbeec13121112163ee7509f1ff2dbd09 );
    codeobj_c4b888b9d7f40a84ef779411b50c49e7 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_105cd69f89139c3140ccc57b9497c4d7, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_grammars$location =
{
    PyModuleDef_HEAD_INIT,
    "grammars.location",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(grammars$location)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(grammars$location)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_grammars$location );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("grammars.location: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.location: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.location: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initgrammars$location" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_grammars$location = Py_InitModule4(
        "grammars.location",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_grammars$location = PyModule_Create( &mdef_grammars$location );
#endif

    moduledict_grammars$location = MODULE_DICT( module_grammars$location );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_grammars$location,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_grammars$location,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$location,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$location,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_grammars$location );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_d2a7a56a4d20853487c008ece870c4f1, module_grammars$location );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_c4b888b9d7f40a84ef779411b50c49e7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_c4b888b9d7f40a84ef779411b50c49e7 = MAKE_MODULE_FRAME( codeobj_c4b888b9d7f40a84ef779411b50c49e7, module_grammars$location );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_c4b888b9d7f40a84ef779411b50c49e7 );
    assert( Py_REFCNT( frame_c4b888b9d7f40a84ef779411b50c49e7 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_yargy;
        tmp_globals_name_1 = (PyObject *)moduledict_grammars$location;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_rule_str_plain_and__str_plain_or__str_plain_not__tuple;
        tmp_level_name_1 = const_int_0;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 4;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_rule );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_and_ );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_and_, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_or_ );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_or_, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_not_ );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_not_, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
        tmp_globals_name_2 = (PyObject *)moduledict_grammars$location;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_fact_tuple;
        tmp_level_name_2 = const_int_0;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 8;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_fact );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_fact, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_ea78dcde38a5066c777d6274d25657b3;
        tmp_globals_name_3 = (PyObject *)moduledict_grammars$location;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_4893302eac92a978f6457e86015205d2_tuple;
        tmp_level_name_3 = const_int_0;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 9;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_11;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_caseless );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_caseless, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_normalized );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_normalized, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_eq );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_eq, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_10 = tmp_import_from_2__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_length_eq );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_length_eq, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_11 = tmp_import_from_2__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_gram );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_12 = tmp_import_from_2__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_dictionary );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_13 = tmp_import_from_2__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_is_single );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_is_single, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_14 = tmp_import_from_2__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_is_title );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_is_title, tmp_assign_source_19 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_15;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_3b0d1c4c904989a80e46e8a766322a96;
        tmp_globals_name_4 = (PyObject *)moduledict_grammars$location;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_gnc_relation_tuple;
        tmp_level_name_4 = const_int_0;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 15;
        tmp_import_name_from_15 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_gnc_relation );
        Py_DECREF( tmp_import_name_from_15 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_fact );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fact );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fact" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 18;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_call_arg_element_1 = const_str_plain_Location;
        tmp_call_arg_element_2 = LIST_COPY( const_list_str_plain_name_list );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 18;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_21 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 24;
        tmp_assign_source_22 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_7;
        PyObject *tmp_source_name_3;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_call_arg_element_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 26;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_6;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 27;
        tmp_source_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_match );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_7;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 27;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 28;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_8;
        tmp_call_arg_element_3 = PySet_New( const_set_046d91ed660b9014185a469cd4bed016 );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 28;
        {
            PyObject *call_args[] = { tmp_call_arg_element_3 };
            tmp_source_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_call_arg_element_3 );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_match );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_called_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 34;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = tmp_mvar_value_9;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 28;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 26;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_3 };
            tmp_source_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 35;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_10;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 35;
        tmp_args_element_name_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 26;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_REGION, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 37;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_11;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 37;
        tmp_assign_source_24 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_name_10;
        PyObject *tmp_source_name_5;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_14;
        PyObject *tmp_source_name_6;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_call_arg_element_4;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_called_name_18;
        PyObject *tmp_source_name_7;
        PyObject *tmp_called_name_19;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_call_arg_element_5;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_name_20;
        PyObject *tmp_source_name_8;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_call_arg_element_6;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_24;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_12;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_13;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_14;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 40;
        tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_digest_5aaebc7bd751170c841a9d6f763cad87_tuple, 0 ) );

        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_8 = const_str_chr_45;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_called_instance_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 40;
        tmp_args_element_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_15;
        tmp_call_arg_element_4 = PySet_New( const_set_5f71e6c8975fa3da89451c27416c4e06 );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_call_arg_element_4 };
            tmp_source_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_call_arg_element_4 );
        if ( tmp_source_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_called_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_match );
        Py_DECREF( tmp_source_name_6 );
        if ( tmp_called_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_called_name_14 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_10 = tmp_mvar_value_16;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_called_name_14 );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_17;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_18;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;

            goto frame_exception_exit_1;
        }

        tmp_called_name_19 = tmp_mvar_value_19;
        tmp_call_arg_element_5 = PySet_New( const_set_5476be352a05047fdbdfb7cc2bdbc6ea );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_call_arg_element_5 };
            tmp_source_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_call_arg_element_5 );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 53;

            goto frame_exception_exit_1;
        }
        tmp_called_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_match );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_called_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 53;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_20 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_called_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_14 = tmp_mvar_value_20;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_called_name_18 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 53;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_args_element_name_13 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;

            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_21;
        tmp_call_arg_element_6 = PySet_New( const_set_a20ea88b302ba22117ebe215fbbc0485 );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 54;
        {
            PyObject *call_args[] = { tmp_call_arg_element_6 };
            tmp_source_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
        }

        Py_DECREF( tmp_call_arg_element_6 );
        if ( tmp_source_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_args_element_name_13 );

            exception_lineno = 54;

            goto frame_exception_exit_1;
        }
        tmp_called_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_match );
        Py_DECREF( tmp_source_name_8 );
        if ( tmp_called_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_args_element_name_13 );

            exception_lineno = 54;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_args_element_name_13 );
            Py_DECREF( tmp_called_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_16 = tmp_mvar_value_22;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 54;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_called_name_20 );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_args_element_name_13 );

            exception_lineno = 54;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 52;
        {
            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_15 };
            tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_args_element_name_13 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_args_element_name_12 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_23;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 56;
        tmp_args_element_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, &PyTuple_GET_ITEM( const_tuple_str_digest_58d7183d75f6a1148d1baaae89c0fb64_tuple, 0 ) );

        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_args_element_name_12 );

            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 51;
        {
            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_17 };
            tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 51;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 39;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_9, tmp_args_element_name_11 };
            tmp_source_name_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        Py_DECREF( tmp_args_element_name_9 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_called_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_24;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_name );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 58;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 58;
        tmp_args_element_name_18 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 58;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 39;
        {
            PyObject *call_args[] = { tmp_args_element_name_18 };
            tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_18 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_FEDERAL_DISTRICT, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_called_name_23;
        PyObject *tmp_mvar_value_25;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;

            goto frame_exception_exit_1;
        }

        tmp_called_name_23 = tmp_mvar_value_25;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 60;
        tmp_assign_source_26 = CALL_FUNCTION_NO_ARGS( tmp_called_name_23 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_name_24;
        PyObject *tmp_source_name_10;
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_called_name_26;
        PyObject *tmp_source_name_11;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_called_name_28;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_called_name_30;
        PyObject *tmp_source_name_12;
        PyObject *tmp_called_name_31;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_call_arg_element_7;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_called_name_32;
        PyObject *tmp_source_name_13;
        PyObject *tmp_called_name_33;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_call_arg_element_8;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_called_name_34;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_36;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_26;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_27;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 63;
        tmp_source_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_source_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        tmp_called_name_26 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_match );
        Py_DECREF( tmp_source_name_11 );
        if ( tmp_called_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_called_name_26 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_20 = tmp_mvar_value_28;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_20 };
            tmp_called_instance_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
        }

        Py_DECREF( tmp_called_name_26 );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 63;
        tmp_args_element_name_19 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_args_element_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_called_name_28 = tmp_mvar_value_29;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_args_element_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_30;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_args_element_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;

            goto frame_exception_exit_1;
        }

        tmp_called_name_31 = tmp_mvar_value_31;
        tmp_call_arg_element_7 = PySet_New( const_set_28b50f6213755b1a8a3a45e044122aa3 );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_call_arg_element_7 };
            tmp_source_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, call_args );
        }

        Py_DECREF( tmp_call_arg_element_7 );
        if ( tmp_source_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 66;

            goto frame_exception_exit_1;
        }
        tmp_called_name_30 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_match );
        Py_DECREF( tmp_source_name_12 );
        if ( tmp_called_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 66;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_called_name_30 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_24 = tmp_mvar_value_32;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_args_element_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, call_args );
        }

        Py_DECREF( tmp_called_name_30 );
        if ( tmp_args_element_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 66;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }

        tmp_called_name_33 = tmp_mvar_value_33;
        tmp_call_arg_element_8 = PySet_New( const_set_a20ea88b302ba22117ebe215fbbc0485 );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_call_arg_element_8 };
            tmp_source_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, call_args );
        }

        Py_DECREF( tmp_call_arg_element_8 );
        if ( tmp_source_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_23 );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        tmp_called_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_match );
        Py_DECREF( tmp_source_name_13 );
        if ( tmp_called_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_23 );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_23 );
            Py_DECREF( tmp_called_name_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_26 = tmp_mvar_value_34;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_26 };
            tmp_args_element_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
        }

        Py_DECREF( tmp_called_name_32 );
        if ( tmp_args_element_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_23 );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 65;
        {
            PyObject *call_args[] = { tmp_args_element_name_23, tmp_args_element_name_25 };
            tmp_args_element_name_22 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_29, call_args );
        }

        Py_DECREF( tmp_args_element_name_23 );
        Py_DECREF( tmp_args_element_name_25 );
        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 65;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_called_name_34 = tmp_mvar_value_35;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 69;
        tmp_args_element_name_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, &PyTuple_GET_ITEM( const_tuple_str_digest_969bab7e4d3b9481c0b52aa8d031c72e_tuple, 0 ) );

        if ( tmp_args_element_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_22 );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_22, tmp_args_element_name_27 };
            tmp_args_element_name_21 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_args_element_name_22 );
        Py_DECREF( tmp_args_element_name_27 );
        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 62;
        {
            PyObject *call_args[] = { tmp_args_element_name_19, tmp_args_element_name_21 };
            tmp_source_name_10 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_args_element_name_19 );
        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_source_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        tmp_called_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_10 );
        if ( tmp_called_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_called_name_24 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_36;
        tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_name );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_24 );

            exception_lineno = 71;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 71;
        tmp_args_element_name_28 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_24 );

            exception_lineno = 71;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 62;
        {
            PyObject *call_args[] = { tmp_args_element_name_28 };
            tmp_assign_source_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_called_name_24 );
        Py_DECREF( tmp_args_element_name_28 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_AUTONOMOUS_DISTRICT, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_35;
        PyObject *tmp_mvar_value_37;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_37 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;

            goto frame_exception_exit_1;
        }

        tmp_called_name_35 = tmp_mvar_value_37;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 73;
        tmp_assign_source_28 = CALL_FUNCTION_NO_ARGS( tmp_called_name_35 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_name_36;
        PyObject *tmp_source_name_15;
        PyObject *tmp_called_name_37;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_called_name_38;
        PyObject *tmp_source_name_16;
        PyObject *tmp_called_name_39;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_args_element_name_30;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_args_element_name_31;
        PyObject *tmp_called_name_40;
        PyObject *tmp_source_name_17;
        PyObject *tmp_called_name_41;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_call_arg_element_9;
        PyObject *tmp_args_element_name_32;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_args_element_name_33;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_source_name_18;
        PyObject *tmp_mvar_value_43;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_38 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 75;

            goto frame_exception_exit_1;
        }

        tmp_called_name_37 = tmp_mvar_value_38;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_39 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto frame_exception_exit_1;
        }

        tmp_called_name_39 = tmp_mvar_value_39;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 76;
        tmp_source_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_39, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_source_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        tmp_called_name_38 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_match );
        Py_DECREF( tmp_source_name_16 );
        if ( tmp_called_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_called_name_38 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_30 = tmp_mvar_value_40;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 76;
        {
            PyObject *call_args[] = { tmp_args_element_name_30 };
            tmp_called_instance_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_38, call_args );
        }

        Py_DECREF( tmp_called_name_38 );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 76;
        tmp_args_element_name_29 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_args_element_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_args_element_name_29 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_called_name_41 = tmp_mvar_value_41;
        tmp_call_arg_element_9 = PySet_New( const_set_52abd00c494ef6649d2050f530a5ed8a );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_call_arg_element_9 };
            tmp_source_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_41, call_args );
        }

        Py_DECREF( tmp_call_arg_element_9 );
        if ( tmp_source_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        tmp_called_name_40 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_match );
        Py_DECREF( tmp_source_name_17 );
        if ( tmp_called_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_args_element_name_29 );
            Py_DECREF( tmp_called_name_40 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_32 = tmp_mvar_value_42;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_32 };
            tmp_args_element_name_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_40, call_args );
        }

        Py_DECREF( tmp_called_name_40 );
        if ( tmp_args_element_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 75;
        {
            PyObject *call_args[] = { tmp_args_element_name_29, tmp_args_element_name_31 };
            tmp_source_name_15 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_37, call_args );
        }

        Py_DECREF( tmp_args_element_name_29 );
        Py_DECREF( tmp_args_element_name_31 );
        if ( tmp_source_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        tmp_called_name_36 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_15 );
        if ( tmp_called_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_called_name_36 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }

        tmp_source_name_18 = tmp_mvar_value_43;
        tmp_called_instance_7 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_name );
        if ( tmp_called_instance_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_36 );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 81;
        tmp_args_element_name_33 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_7 );
        if ( tmp_args_element_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_36 );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 75;
        {
            PyObject *call_args[] = { tmp_args_element_name_33 };
            tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_36, call_args );
        }

        Py_DECREF( tmp_called_name_36 );
        Py_DECREF( tmp_args_element_name_33 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_FEDERATION, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_name_42;
        PyObject *tmp_mvar_value_44;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_44 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_called_name_42 = tmp_mvar_value_44;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 83;
        tmp_assign_source_30 = CALL_FUNCTION_NO_ARGS( tmp_called_name_42 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_name_43;
        PyObject *tmp_source_name_19;
        PyObject *tmp_called_name_44;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_args_element_name_34;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_called_name_45;
        PyObject *tmp_source_name_20;
        PyObject *tmp_called_name_46;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_args_element_name_35;
        PyObject *tmp_called_name_47;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_args_element_name_36;
        PyObject *tmp_called_name_48;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_args_element_name_38;
        PyObject *tmp_called_name_49;
        PyObject *tmp_source_name_21;
        PyObject *tmp_called_name_50;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_call_arg_element_10;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_called_instance_9;
        PyObject *tmp_called_instance_10;
        PyObject *tmp_called_name_51;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_called_instance_11;
        PyObject *tmp_source_name_22;
        PyObject *tmp_mvar_value_53;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_45 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }

        tmp_called_name_44 = tmp_mvar_value_45;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_46 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }

        tmp_called_name_46 = tmp_mvar_value_46;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_47 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;

            goto frame_exception_exit_1;
        }

        tmp_called_name_47 = tmp_mvar_value_47;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 87;
        tmp_args_element_name_35 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_47, &PyTuple_GET_ITEM( const_tuple_str_plain_Adjx_tuple, 0 ) );

        if ( tmp_args_element_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_args_element_name_35 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 88;

            goto frame_exception_exit_1;
        }

        tmp_called_name_48 = tmp_mvar_value_48;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 88;
        tmp_args_element_name_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_48, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_args_element_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_35 );

            exception_lineno = 88;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 86;
        {
            PyObject *call_args[] = { tmp_args_element_name_35, tmp_args_element_name_36 };
            tmp_source_name_20 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_46, call_args );
        }

        Py_DECREF( tmp_args_element_name_35 );
        Py_DECREF( tmp_args_element_name_36 );
        if ( tmp_source_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        tmp_called_name_45 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_match );
        Py_DECREF( tmp_source_name_20 );
        if ( tmp_called_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_49 == NULL )
        {
            Py_DECREF( tmp_called_name_45 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_37 = tmp_mvar_value_49;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 86;
        {
            PyObject *call_args[] = { tmp_args_element_name_37 };
            tmp_called_instance_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_45, call_args );
        }

        Py_DECREF( tmp_called_name_45 );
        if ( tmp_called_instance_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 86;
        tmp_args_element_name_34 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_8 );
        if ( tmp_args_element_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_50 == NULL )
        {
            Py_DECREF( tmp_args_element_name_34 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 90;

            goto frame_exception_exit_1;
        }

        tmp_called_name_50 = tmp_mvar_value_50;
        tmp_call_arg_element_10 = PySet_New( const_set_9f951b67d5c3ab53cac8b22001165cd1 );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_call_arg_element_10 };
            tmp_source_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_50, call_args );
        }

        Py_DECREF( tmp_call_arg_element_10 );
        if ( tmp_source_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_34 );

            exception_lineno = 90;

            goto frame_exception_exit_1;
        }
        tmp_called_name_49 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_match );
        Py_DECREF( tmp_source_name_21 );
        if ( tmp_called_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_34 );

            exception_lineno = 90;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_51 == NULL )
        {
            Py_DECREF( tmp_args_element_name_34 );
            Py_DECREF( tmp_called_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 93;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_39 = tmp_mvar_value_51;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_args_element_name_39 };
            tmp_args_element_name_38 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_49, call_args );
        }

        Py_DECREF( tmp_called_name_49 );
        if ( tmp_args_element_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_34 );

            exception_lineno = 90;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_52 == NULL )
        {
            Py_DECREF( tmp_args_element_name_34 );
            Py_DECREF( tmp_args_element_name_38 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }

        tmp_called_name_51 = tmp_mvar_value_52;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 94;
        tmp_called_instance_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_51, &PyTuple_GET_ITEM( const_tuple_str_plain_gent_tuple, 0 ) );

        if ( tmp_called_instance_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_34 );
            Py_DECREF( tmp_args_element_name_38 );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 94;
        tmp_called_instance_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_10, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_10 );
        if ( tmp_called_instance_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_34 );
            Py_DECREF( tmp_args_element_name_38 );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 94;
        tmp_args_element_name_40 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_9 );
        if ( tmp_args_element_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_34 );
            Py_DECREF( tmp_args_element_name_38 );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_34, tmp_args_element_name_38, tmp_args_element_name_40 };
            tmp_source_name_19 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_44, call_args );
        }

        Py_DECREF( tmp_args_element_name_34 );
        Py_DECREF( tmp_args_element_name_38 );
        Py_DECREF( tmp_args_element_name_40 );
        if ( tmp_source_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        tmp_called_name_43 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_19 );
        if ( tmp_called_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_called_name_43 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 95;

            goto frame_exception_exit_1;
        }

        tmp_source_name_22 = tmp_mvar_value_53;
        tmp_called_instance_11 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_name );
        if ( tmp_called_instance_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_43 );

            exception_lineno = 95;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 95;
        tmp_args_element_name_41 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_11 );
        if ( tmp_args_element_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_43 );

            exception_lineno = 95;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_41 };
            tmp_assign_source_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_43, call_args );
        }

        Py_DECREF( tmp_called_name_43 );
        Py_DECREF( tmp_args_element_name_41 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_ADJX_FEDERATION, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_called_name_52;
        PyObject *tmp_mvar_value_54;
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_54 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;

            goto frame_exception_exit_1;
        }

        tmp_called_name_52 = tmp_mvar_value_54;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 97;
        tmp_assign_source_32 = CALL_FUNCTION_NO_ARGS( tmp_called_name_52 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_called_name_53;
        PyObject *tmp_source_name_23;
        PyObject *tmp_called_name_54;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_args_element_name_42;
        PyObject *tmp_called_name_55;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_call_arg_element_11;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_called_instance_12;
        PyObject *tmp_called_name_56;
        PyObject *tmp_source_name_24;
        PyObject *tmp_called_name_57;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_args_element_name_44;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_called_name_58;
        PyObject *tmp_source_name_25;
        PyObject *tmp_called_name_59;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_args_element_name_46;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_called_instance_13;
        PyObject *tmp_source_name_26;
        PyObject *tmp_mvar_value_61;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_55 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;

            goto frame_exception_exit_1;
        }

        tmp_called_name_54 = tmp_mvar_value_55;
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_56 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;

            goto frame_exception_exit_1;
        }

        tmp_called_name_55 = tmp_mvar_value_56;
        tmp_call_arg_element_11 = PySet_New( const_set_039f612f114bd28b9b8a304073fb8632 );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_call_arg_element_11 };
            tmp_args_element_name_42 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_55, call_args );
        }

        Py_DECREF( tmp_call_arg_element_11 );
        if ( tmp_args_element_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }

        tmp_called_name_57 = tmp_mvar_value_57;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 104;
        tmp_source_name_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_57, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_source_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        tmp_called_name_56 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_match );
        Py_DECREF( tmp_source_name_24 );
        if ( tmp_called_name_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_58 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_called_name_56 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_44 = tmp_mvar_value_58;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_44 };
            tmp_called_instance_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_56, call_args );
        }

        Py_DECREF( tmp_called_name_56 );
        if ( tmp_called_instance_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 104;
        tmp_args_element_name_43 = CALL_METHOD_NO_ARGS( tmp_called_instance_12, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_12 );
        if ( tmp_args_element_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_59 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_43 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }

        tmp_called_name_59 = tmp_mvar_value_59;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 105;
        tmp_source_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_59, &PyTuple_GET_ITEM( const_tuple_str_plain_NOUN_tuple, 0 ) );

        if ( tmp_source_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_43 );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        tmp_called_name_58 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_match );
        Py_DECREF( tmp_source_name_25 );
        if ( tmp_called_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_43 );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_60 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_43 );
            Py_DECREF( tmp_called_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_46 = tmp_mvar_value_60;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_46 };
            tmp_args_element_name_45 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_58, call_args );
        }

        Py_DECREF( tmp_called_name_58 );
        if ( tmp_args_element_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_43 );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_42, tmp_args_element_name_43, tmp_args_element_name_45 };
            tmp_source_name_23 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_54, call_args );
        }

        Py_DECREF( tmp_args_element_name_42 );
        Py_DECREF( tmp_args_element_name_43 );
        Py_DECREF( tmp_args_element_name_45 );
        if ( tmp_source_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;

            goto frame_exception_exit_1;
        }
        tmp_called_name_53 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_23 );
        if ( tmp_called_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_called_name_53 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }

        tmp_source_name_26 = tmp_mvar_value_61;
        tmp_called_instance_13 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_name );
        if ( tmp_called_instance_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_53 );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 106;
        tmp_args_element_name_47 = CALL_METHOD_NO_ARGS( tmp_called_instance_13, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_13 );
        if ( tmp_args_element_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_53 );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_47 };
            tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_53, call_args );
        }

        Py_DECREF( tmp_called_name_53 );
        Py_DECREF( tmp_args_element_name_47 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_STATE, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_called_name_60;
        PyObject *tmp_mvar_value_62;
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_62 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;

            goto frame_exception_exit_1;
        }

        tmp_called_name_60 = tmp_mvar_value_62;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 108;
        tmp_assign_source_34 = CALL_FUNCTION_NO_ARGS( tmp_called_name_60 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_called_name_61;
        PyObject *tmp_source_name_27;
        PyObject *tmp_called_name_62;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_args_element_name_48;
        PyObject *tmp_called_instance_14;
        PyObject *tmp_called_name_63;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_args_element_name_49;
        PyObject *tmp_called_name_64;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_call_arg_element_12;
        PyObject *tmp_args_element_name_50;
        PyObject *tmp_called_name_65;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_args_element_name_51;
        PyObject *tmp_called_name_66;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_args_element_name_52;
        PyObject *tmp_called_name_67;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_args_element_name_53;
        PyObject *tmp_called_name_68;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_args_element_name_54;
        PyObject *tmp_called_name_69;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_args_element_name_55;
        PyObject *tmp_called_name_70;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_args_element_name_56;
        PyObject *tmp_called_instance_15;
        PyObject *tmp_called_name_71;
        PyObject *tmp_source_name_28;
        PyObject *tmp_called_name_72;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_args_element_name_57;
        PyObject *tmp_called_name_73;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_args_element_name_58;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_args_element_name_59;
        PyObject *tmp_called_name_74;
        PyObject *tmp_source_name_29;
        PyObject *tmp_called_name_75;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_args_element_name_60;
        PyObject *tmp_called_name_76;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_args_element_name_61;
        PyObject *tmp_called_name_77;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_args_element_name_62;
        PyObject *tmp_called_name_78;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_args_element_name_63;
        PyObject *tmp_called_name_79;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_args_element_name_64;
        PyObject *tmp_called_name_80;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_args_element_name_65;
        PyObject *tmp_called_name_81;
        PyObject *tmp_mvar_value_81;
        PyObject *tmp_args_element_name_66;
        PyObject *tmp_called_name_82;
        PyObject *tmp_mvar_value_82;
        PyObject *tmp_args_element_name_67;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_args_element_name_68;
        PyObject *tmp_called_instance_16;
        PyObject *tmp_source_name_30;
        PyObject *tmp_mvar_value_84;
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_63 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;

            goto frame_exception_exit_1;
        }

        tmp_called_name_62 = tmp_mvar_value_63;
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_64 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;

            goto frame_exception_exit_1;
        }

        tmp_called_name_63 = tmp_mvar_value_64;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_65 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;

            goto frame_exception_exit_1;
        }

        tmp_called_name_64 = tmp_mvar_value_65;
        tmp_call_arg_element_12 = PySet_New( const_set_46db9eadb527a32db54c196b1a27bcac );
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_call_arg_element_12 };
            tmp_args_element_name_49 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_64, call_args );
        }

        Py_DECREF( tmp_call_arg_element_12 );
        if ( tmp_args_element_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_not_ );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_not_ );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "not_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }

        tmp_called_name_65 = tmp_mvar_value_66;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }

        tmp_called_name_66 = tmp_mvar_value_67;
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }

        tmp_called_name_67 = tmp_mvar_value_68;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 119;
        tmp_args_element_name_52 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_67, &PyTuple_GET_ITEM( const_tuple_str_plain_Abbr_tuple, 0 ) );

        if ( tmp_args_element_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            Py_DECREF( tmp_args_element_name_52 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }

        tmp_called_name_68 = tmp_mvar_value_69;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 120;
        tmp_args_element_name_53 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_68, &PyTuple_GET_ITEM( const_tuple_str_plain_PREP_tuple, 0 ) );

        if ( tmp_args_element_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );
            Py_DECREF( tmp_args_element_name_52 );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            Py_DECREF( tmp_args_element_name_52 );
            Py_DECREF( tmp_args_element_name_53 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }

        tmp_called_name_69 = tmp_mvar_value_70;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 121;
        tmp_args_element_name_54 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_69, &PyTuple_GET_ITEM( const_tuple_str_plain_CONJ_tuple, 0 ) );

        if ( tmp_args_element_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );
            Py_DECREF( tmp_args_element_name_52 );
            Py_DECREF( tmp_args_element_name_53 );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            Py_DECREF( tmp_args_element_name_52 );
            Py_DECREF( tmp_args_element_name_53 );
            Py_DECREF( tmp_args_element_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;

            goto frame_exception_exit_1;
        }

        tmp_called_name_70 = tmp_mvar_value_71;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 122;
        tmp_args_element_name_55 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_70, &PyTuple_GET_ITEM( const_tuple_str_plain_PRCL_tuple, 0 ) );

        if ( tmp_args_element_name_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );
            Py_DECREF( tmp_args_element_name_52 );
            Py_DECREF( tmp_args_element_name_53 );
            Py_DECREF( tmp_args_element_name_54 );

            exception_lineno = 122;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_52, tmp_args_element_name_53, tmp_args_element_name_54, tmp_args_element_name_55 };
            tmp_args_element_name_51 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_66, call_args );
        }

        Py_DECREF( tmp_args_element_name_52 );
        Py_DECREF( tmp_args_element_name_53 );
        Py_DECREF( tmp_args_element_name_54 );
        Py_DECREF( tmp_args_element_name_55 );
        if ( tmp_args_element_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 117;
        {
            PyObject *call_args[] = { tmp_args_element_name_51 };
            tmp_args_element_name_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_65, call_args );
        }

        Py_DECREF( tmp_args_element_name_51 );
        if ( tmp_args_element_name_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_49, tmp_args_element_name_50 };
            tmp_called_instance_14 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_63, call_args );
        }

        Py_DECREF( tmp_args_element_name_49 );
        Py_DECREF( tmp_args_element_name_50 );
        if ( tmp_called_instance_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 111;
        tmp_args_element_name_48 = CALL_METHOD_NO_ARGS( tmp_called_instance_14, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_14 );
        if ( tmp_args_element_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_72 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }

        tmp_called_name_72 = tmp_mvar_value_72;
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_73 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;

            goto frame_exception_exit_1;
        }

        tmp_called_name_73 = tmp_mvar_value_73;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 127;
        tmp_args_element_name_57 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_73, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_args_element_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 127;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 126;
        {
            PyObject *call_args[] = { tmp_args_element_name_57 };
            tmp_source_name_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_72, call_args );
        }

        Py_DECREF( tmp_args_element_name_57 );
        if ( tmp_source_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }
        tmp_called_name_71 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_match );
        Py_DECREF( tmp_source_name_28 );
        if ( tmp_called_name_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_74 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_called_name_71 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_58 = tmp_mvar_value_74;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 126;
        {
            PyObject *call_args[] = { tmp_args_element_name_58 };
            tmp_called_instance_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_71, call_args );
        }

        Py_DECREF( tmp_called_name_71 );
        if ( tmp_called_instance_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 126;
        tmp_args_element_name_56 = CALL_METHOD_NO_ARGS( tmp_called_instance_15, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_15 );
        if ( tmp_args_element_name_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_75 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }

        tmp_called_name_75 = tmp_mvar_value_75;
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_76 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;

            goto frame_exception_exit_1;
        }

        tmp_called_name_76 = tmp_mvar_value_76;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 130;
        tmp_args_element_name_60 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_76, &PyTuple_GET_ITEM( const_tuple_str_plain_Geox_tuple, 0 ) );

        if ( tmp_args_element_name_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );

            exception_lineno = 130;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_not_ );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_not_ );
        }

        if ( tmp_mvar_value_77 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "not_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }

        tmp_called_name_77 = tmp_mvar_value_77;
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_78 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;

            goto frame_exception_exit_1;
        }

        tmp_called_name_78 = tmp_mvar_value_78;
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_79 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }

        tmp_called_name_79 = tmp_mvar_value_79;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 133;
        tmp_args_element_name_63 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_79, &PyTuple_GET_ITEM( const_tuple_str_plain_Abbr_tuple, 0 ) );

        if ( tmp_args_element_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_80 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            Py_DECREF( tmp_args_element_name_63 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;

            goto frame_exception_exit_1;
        }

        tmp_called_name_80 = tmp_mvar_value_80;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 134;
        tmp_args_element_name_64 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_80, &PyTuple_GET_ITEM( const_tuple_str_plain_PREP_tuple, 0 ) );

        if ( tmp_args_element_name_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            Py_DECREF( tmp_args_element_name_63 );

            exception_lineno = 134;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_81 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            Py_DECREF( tmp_args_element_name_63 );
            Py_DECREF( tmp_args_element_name_64 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;

            goto frame_exception_exit_1;
        }

        tmp_called_name_81 = tmp_mvar_value_81;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 135;
        tmp_args_element_name_65 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_81, &PyTuple_GET_ITEM( const_tuple_str_plain_CONJ_tuple, 0 ) );

        if ( tmp_args_element_name_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            Py_DECREF( tmp_args_element_name_63 );
            Py_DECREF( tmp_args_element_name_64 );

            exception_lineno = 135;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_82 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            Py_DECREF( tmp_args_element_name_63 );
            Py_DECREF( tmp_args_element_name_64 );
            Py_DECREF( tmp_args_element_name_65 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }

        tmp_called_name_82 = tmp_mvar_value_82;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 136;
        tmp_args_element_name_66 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_82, &PyTuple_GET_ITEM( const_tuple_str_plain_PRCL_tuple, 0 ) );

        if ( tmp_args_element_name_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );
            Py_DECREF( tmp_args_element_name_63 );
            Py_DECREF( tmp_args_element_name_64 );
            Py_DECREF( tmp_args_element_name_65 );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_63, tmp_args_element_name_64, tmp_args_element_name_65, tmp_args_element_name_66 };
            tmp_args_element_name_62 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_78, call_args );
        }

        Py_DECREF( tmp_args_element_name_63 );
        Py_DECREF( tmp_args_element_name_64 );
        Py_DECREF( tmp_args_element_name_65 );
        Py_DECREF( tmp_args_element_name_66 );
        if ( tmp_args_element_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );

            exception_lineno = 132;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_62 };
            tmp_args_element_name_61 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_77, call_args );
        }

        Py_DECREF( tmp_args_element_name_62 );
        if ( tmp_args_element_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_args_element_name_60 );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_60, tmp_args_element_name_61 };
            tmp_source_name_29 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_75, call_args );
        }

        Py_DECREF( tmp_args_element_name_60 );
        Py_DECREF( tmp_args_element_name_61 );
        if ( tmp_source_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }
        tmp_called_name_74 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_match );
        Py_DECREF( tmp_source_name_29 );
        if ( tmp_called_name_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_83 == NULL )
        {
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );
            Py_DECREF( tmp_called_name_74 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_67 = tmp_mvar_value_83;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_67 };
            tmp_args_element_name_59 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_74, call_args );
        }

        Py_DECREF( tmp_called_name_74 );
        if ( tmp_args_element_name_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_56 );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_48, tmp_args_element_name_56, tmp_args_element_name_59 };
            tmp_source_name_27 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_62, call_args );
        }

        Py_DECREF( tmp_args_element_name_48 );
        Py_DECREF( tmp_args_element_name_56 );
        Py_DECREF( tmp_args_element_name_59 );
        if ( tmp_source_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        tmp_called_name_61 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_27 );
        if ( tmp_called_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_84 == NULL )
        {
            Py_DECREF( tmp_called_name_61 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;

            goto frame_exception_exit_1;
        }

        tmp_source_name_30 = tmp_mvar_value_84;
        tmp_called_instance_16 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_name );
        if ( tmp_called_instance_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_61 );

            exception_lineno = 140;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 140;
        tmp_args_element_name_68 = CALL_METHOD_NO_ARGS( tmp_called_instance_16, const_str_plain_inflected );
        Py_DECREF( tmp_called_instance_16 );
        if ( tmp_args_element_name_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_61 );

            exception_lineno = 140;

            goto frame_exception_exit_1;
        }
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_68 };
            tmp_assign_source_35 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_61, call_args );
        }

        Py_DECREF( tmp_called_name_61 );
        Py_DECREF( tmp_args_element_name_68 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_LOCALITY, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_called_name_83;
        PyObject *tmp_source_name_31;
        PyObject *tmp_called_name_84;
        PyObject *tmp_mvar_value_85;
        PyObject *tmp_args_element_name_69;
        PyObject *tmp_mvar_value_86;
        PyObject *tmp_args_element_name_70;
        PyObject *tmp_mvar_value_87;
        PyObject *tmp_args_element_name_71;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_args_element_name_72;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_args_element_name_73;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_args_element_name_74;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_args_element_name_75;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_args_element_name_76;
        PyObject *tmp_mvar_value_93;
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_85 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_called_name_84 = tmp_mvar_value_85;
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_REGION );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_REGION );
        }

        if ( tmp_mvar_value_86 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "REGION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_69 = tmp_mvar_value_86;
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_FEDERAL_DISTRICT );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FEDERAL_DISTRICT );
        }

        if ( tmp_mvar_value_87 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FEDERAL_DISTRICT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_70 = tmp_mvar_value_87;
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_AUTONOMOUS_DISTRICT );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AUTONOMOUS_DISTRICT );
        }

        if ( tmp_mvar_value_88 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AUTONOMOUS_DISTRICT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_71 = tmp_mvar_value_88;
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_FEDERATION );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FEDERATION );
        }

        if ( tmp_mvar_value_89 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FEDERATION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_72 = tmp_mvar_value_89;
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_ADJX_FEDERATION );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ADJX_FEDERATION );
        }

        if ( tmp_mvar_value_90 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ADJX_FEDERATION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_73 = tmp_mvar_value_90;
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_STATE );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STATE );
        }

        if ( tmp_mvar_value_91 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STATE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_74 = tmp_mvar_value_91;
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_LOCALITY );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LOCALITY );
        }

        CHECK_OBJECT( tmp_mvar_value_92 );
        tmp_args_element_name_75 = tmp_mvar_value_92;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 142;
        {
            PyObject *call_args[] = { tmp_args_element_name_69, tmp_args_element_name_70, tmp_args_element_name_71, tmp_args_element_name_72, tmp_args_element_name_73, tmp_args_element_name_74, tmp_args_element_name_75 };
            tmp_source_name_31 = CALL_FUNCTION_WITH_ARGS7( tmp_called_name_84, call_args );
        }

        if ( tmp_source_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto frame_exception_exit_1;
        }
        tmp_called_name_83 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_31 );
        if ( tmp_called_name_83 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_Location );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Location );
        }

        if ( tmp_mvar_value_93 == NULL )
        {
            Py_DECREF( tmp_called_name_83 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Location" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_76 = tmp_mvar_value_93;
        frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame.f_lineno = 142;
        {
            PyObject *call_args[] = { tmp_args_element_name_76 };
            tmp_assign_source_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_83, call_args );
        }

        Py_DECREF( tmp_called_name_83 );
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$location, (Nuitka_StringObject *)const_str_plain_LOCATION, tmp_assign_source_36 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c4b888b9d7f40a84ef779411b50c49e7 );
#endif
    popFrameStack();

    assertFrameObject( frame_c4b888b9d7f40a84ef779411b50c49e7 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c4b888b9d7f40a84ef779411b50c49e7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c4b888b9d7f40a84ef779411b50c49e7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c4b888b9d7f40a84ef779411b50c49e7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c4b888b9d7f40a84ef779411b50c49e7, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_grammars$location );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
