/* Generated code for Python module 'jinja2.asyncfilters'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jinja2$asyncfilters" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jinja2$asyncfilters;
PyDictObject *moduledict_jinja2$asyncfilters;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_seq;
extern PyObject *const_str_plain_environmentfilter;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_ASYNC_FILTERS;
extern PyObject *const_str_plain_groupby;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_str_plain_iterable;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_digest_8bf2f6f70c5293998a720f2bf96ac69e;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_sum;
extern PyObject *const_tuple_str_plain_value_tuple;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_digest_cc84abbcaaba95671b2d7356ba8cc675;
extern PyObject *const_str_plain_rv;
extern PyObject *const_str_plain_do_slice;
static PyObject *const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple;
extern PyObject *const_str_plain_prepare_map;
extern PyObject *const_str_plain__GroupTuple;
extern PyObject *const_str_plain_None;
static PyObject *const_tuple_21d06e6b197a99895e647b626188cd6b_tuple;
extern PyObject *const_str_plain_lookup_attr;
extern PyObject *const_tuple_str_plain_key_str_plain_values_tuple;
extern PyObject *const_str_plain_func;
extern PyObject *const_tuple_052dd9f13adb5de12f330ff7442179bf_tuple;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_is_async;
extern PyObject *const_str_plain_start;
static PyObject *const_str_digest_abec06806e1eccd913d2cfa6a475442c;
extern PyObject *const_str_plain_wrapper;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_async_filter;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_slice;
extern PyObject *const_str_digest_37aca85c698d791dc4bb639deca6348b;
extern PyObject *const_str_plain_do_first;
static PyObject *const_tuple_str_plain_value_str_plain_seq_str_plain_item_tuple;
extern PyObject *const_str_plain_evalcontextfilter;
extern PyObject *const_str_plain_wraps;
extern PyObject *const_str_plain_do_map;
extern PyObject *const_str_plain_item;
extern PyObject *const_str_plain_value;
extern PyObject *const_tuple_str_plain_args_str_plain_kwargs_tuple;
extern PyObject *const_tuple_846e08d8492d9227f9ba945b310292e8_tuple;
extern PyObject *const_tuple_str_plain_environment_str_plain_seq_tuple;
extern PyObject *const_str_plain_environment;
static PyObject *const_str_plain_dualfilter;
extern PyObject *const_str_plain_expr;
static PyObject *const_str_plain_async_select_or_reject;
static PyObject *const_str_digest_957539f8c5a0c569c341556fc0b67be9;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_digest_bd661d31362901e16224d7891f93b39b;
static PyObject *const_str_plain_auto_to_seq;
extern PyObject *const_str_plain_attribute;
extern PyObject *const_str_plain_append;
static PyObject *const_tuple_str_plain_original_str_plain_decorator_tuple;
extern PyObject *const_str_plain_modfunc;
extern PyObject *const_str_plain_contextfilter;
extern PyObject *const_str_plain_rejectattr;
extern PyObject *const_tuple_str_digest_0b73b59fa5aedb66ee40bf6034307534_tuple;
extern PyObject *const_str_plain_do_selectattr;
static PyObject *const_tuple_str_plain_auto_aiter_tuple;
extern PyObject *const_tuple_str_plain_x_tuple;
extern PyObject *const_str_plain_reject;
extern PyObject *const_str_plain_b;
extern PyObject *const_str_digest_e9c0aba9ba51d097a6babda6163af2fe;
extern PyObject *const_str_digest_0b73b59fa5aedb66ee40bf6034307534;
extern PyObject *const_str_plain_map;
extern PyObject *const_tuple_str_plain_args_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_do_list;
extern PyObject *const_str_plain_filters;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_plain_do_sum;
extern PyObject *const_tuple_str_plain_wraps_tuple;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_plain_auto_aiter;
static PyObject *const_tuple_str_plain_f_str_plain_original_tuple;
extern PyObject *const_str_plain_first;
extern PyObject *const_str_plain_prepare_select_or_reject;
extern PyObject *const_int_0;
static PyObject *const_str_digest_60ea0382f7ce60012086bc4ceb8337d8;
extern PyObject *const_str_plain_slices;
static PyObject *const_str_digest_c253dd25652dae487d0fca8cd2eccf85;
extern PyObject *const_str_plain_do_select;
static PyObject *const_str_plain_normal_filter;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_undefined;
extern PyObject *const_tuple_none_int_0_tuple;
extern PyObject *const_str_plain_asyncfiltervariant;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain___anext__;
extern PyObject *const_str_plain_make_attrgetter;
static PyObject *const_str_digest_8bef79adb6a0c630d5dd7876e9d1fb34;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_digest_7ae79598d0477268ed3786ba124fb740;
extern PyObject *const_str_plain_select;
static PyObject *const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple;
extern PyObject *const_str_plain_d;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_eval_ctx;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_functools;
extern PyObject *const_str_plain_do_reject;
extern PyObject *const_int_pos_1;
extern PyObject *const_tuple_str_empty_none_tuple;
extern PyObject *const_str_plain_values;
static PyObject *const_tuple_str_plain_value_str_plain_slices_str_plain_fill_with_tuple;
static PyObject *const_str_plain_wrap_evalctx;
extern PyObject *const_str_plain_jinja2;
extern PyObject *const_str_plain_key;
extern PyObject *const_tuple_6ed83c1c0052f794a7be75d57cbd8cd2_tuple;
static PyObject *const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple;
extern PyObject *const_tuple_str_plain_filters_tuple;
extern PyObject *const_str_plain_selectattr;
extern PyObject *const_str_plain_original;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_do_join;
extern PyObject *const_str_plain_fill_with;
extern PyObject *const_str_plain___aiter__;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_do_groupby;
extern PyObject *const_str_plain_do_rejectattr;
extern PyObject *const_str_plain_decorator;
static PyObject *const_str_digest_7471b673d6fd023f57af67fd216eb43f;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 0, const_str_plain_environment ); Py_INCREF( const_str_plain_environment );
    PyTuple_SET_ITEM( const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 1, const_str_plain_iterable ); Py_INCREF( const_str_plain_iterable );
    PyTuple_SET_ITEM( const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 2, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 3, const_str_plain_start ); Py_INCREF( const_str_plain_start );
    PyTuple_SET_ITEM( const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 4, const_str_plain_rv ); Py_INCREF( const_str_plain_rv );
    PyTuple_SET_ITEM( const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 5, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 6, const_str_plain_item ); Py_INCREF( const_str_plain_item );
    const_tuple_21d06e6b197a99895e647b626188cd6b_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_21d06e6b197a99895e647b626188cd6b_tuple, 0, const_str_plain_eval_ctx ); Py_INCREF( const_str_plain_eval_ctx );
    PyTuple_SET_ITEM( const_tuple_21d06e6b197a99895e647b626188cd6b_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_21d06e6b197a99895e647b626188cd6b_tuple, 2, const_str_plain_d ); Py_INCREF( const_str_plain_d );
    PyTuple_SET_ITEM( const_tuple_21d06e6b197a99895e647b626188cd6b_tuple, 3, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    const_str_digest_abec06806e1eccd913d2cfa6a475442c = UNSTREAM_STRING_ASCII( &constant_bin[ 1030771 ], 27, 0 );
    const_str_plain_async_filter = UNSTREAM_STRING_ASCII( &constant_bin[ 1030798 ], 12, 1 );
    const_tuple_str_plain_value_str_plain_seq_str_plain_item_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_seq_str_plain_item_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_seq_str_plain_item_tuple, 1, const_str_plain_seq ); Py_INCREF( const_str_plain_seq );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_seq_str_plain_item_tuple, 2, const_str_plain_item ); Py_INCREF( const_str_plain_item );
    const_str_plain_dualfilter = UNSTREAM_STRING_ASCII( &constant_bin[ 1030771 ], 10, 1 );
    const_str_plain_async_select_or_reject = UNSTREAM_STRING_ASCII( &constant_bin[ 1030810 ], 22, 1 );
    const_str_digest_957539f8c5a0c569c341556fc0b67be9 = UNSTREAM_STRING_ASCII( &constant_bin[ 1030832 ], 22, 0 );
    const_str_plain_auto_to_seq = UNSTREAM_STRING_ASCII( &constant_bin[ 1030854 ], 11, 1 );
    const_tuple_str_plain_original_str_plain_decorator_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_original_str_plain_decorator_tuple, 0, const_str_plain_original ); Py_INCREF( const_str_plain_original );
    PyTuple_SET_ITEM( const_tuple_str_plain_original_str_plain_decorator_tuple, 1, const_str_plain_decorator ); Py_INCREF( const_str_plain_decorator );
    const_tuple_str_plain_auto_aiter_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_auto_aiter_tuple, 0, const_str_plain_auto_aiter ); Py_INCREF( const_str_plain_auto_aiter );
    const_tuple_str_plain_f_str_plain_original_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f_str_plain_original_tuple, 0, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_str_plain_f_str_plain_original_tuple, 1, const_str_plain_original ); Py_INCREF( const_str_plain_original );
    const_str_digest_60ea0382f7ce60012086bc4ceb8337d8 = UNSTREAM_STRING_ASCII( &constant_bin[ 1030865 ], 24, 0 );
    const_str_digest_c253dd25652dae487d0fca8cd2eccf85 = UNSTREAM_STRING_ASCII( &constant_bin[ 1030889 ], 37, 0 );
    const_str_plain_normal_filter = UNSTREAM_STRING_ASCII( &constant_bin[ 1030926 ], 13, 1 );
    const_str_digest_8bef79adb6a0c630d5dd7876e9d1fb34 = UNSTREAM_STRING_ASCII( &constant_bin[ 1030939 ], 28, 0 );
    const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 2, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 3, const_str_plain_is_async ); Py_INCREF( const_str_plain_is_async );
    const_str_plain_wrap_evalctx = UNSTREAM_STRING_ASCII( &constant_bin[ 1030967 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 4, const_str_plain_wrap_evalctx ); Py_INCREF( const_str_plain_wrap_evalctx );
    PyTuple_SET_ITEM( const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 5, const_str_plain_async_filter ); Py_INCREF( const_str_plain_async_filter );
    PyTuple_SET_ITEM( const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 6, const_str_plain_normal_filter ); Py_INCREF( const_str_plain_normal_filter );
    const_tuple_str_plain_value_str_plain_slices_str_plain_fill_with_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_slices_str_plain_fill_with_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_slices_str_plain_fill_with_tuple, 1, const_str_plain_slices ); Py_INCREF( const_str_plain_slices );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_slices_str_plain_fill_with_tuple, 2, const_str_plain_fill_with ); Py_INCREF( const_str_plain_fill_with );
    const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple, 0, const_str_plain_normal_filter ); Py_INCREF( const_str_plain_normal_filter );
    PyTuple_SET_ITEM( const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple, 1, const_str_plain_async_filter ); Py_INCREF( const_str_plain_async_filter );
    PyTuple_SET_ITEM( const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple, 2, const_str_plain_wrap_evalctx ); Py_INCREF( const_str_plain_wrap_evalctx );
    PyTuple_SET_ITEM( const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple, 3, const_str_plain_is_async ); Py_INCREF( const_str_plain_is_async );
    PyTuple_SET_ITEM( const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple, 4, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    const_str_digest_7471b673d6fd023f57af67fd216eb43f = UNSTREAM_STRING_ASCII( &constant_bin[ 1030979 ], 28, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jinja2$asyncfilters( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_f23b53a98b30354af9fd158782cd46e9;
static PyCodeObject *codeobj_f35e7c1722912117ea0f4a28f2e33f3a;
static PyCodeObject *codeobj_2319d197081a7cadbd4501c95edde3e8;
static PyCodeObject *codeobj_af4ddcb98bc56d0debe98c79d1794ec8;
static PyCodeObject *codeobj_20c43f26e71a6e024337f6b06bbf6fb5;
static PyCodeObject *codeobj_c06bfb27b3331172ff1849bab432d7da;
static PyCodeObject *codeobj_62dbde7746b177fd8206848f593ed530;
static PyCodeObject *codeobj_3a80fa0cd4b0b177736dab985cf22565;
static PyCodeObject *codeobj_a38a9cd0db8f5b0f9297fa38ba849984;
static PyCodeObject *codeobj_d0d572e6a1be4063384dd873b2005de9;
static PyCodeObject *codeobj_01d5a0773486b7a69b0e25409f159e1b;
static PyCodeObject *codeobj_1c53d575fcbe01c3ae447f6a9b40ae01;
static PyCodeObject *codeobj_94c4672f873eaf481092de964a189029;
static PyCodeObject *codeobj_95bdcaa04bbdebf08501f0a839b84489;
static PyCodeObject *codeobj_4d8a94c87df659cfd7277e613af56ba5;
static PyCodeObject *codeobj_0d831141192fa8a7efce569e9785bc7a;
static PyCodeObject *codeobj_fc49021adb810318671fa076f3ff2e6c;
static PyCodeObject *codeobj_a5f30cb86b153f7dad62e406091ac311;
static PyCodeObject *codeobj_23012cc63c9bc71d68d6f0504e6f7792;
static PyCodeObject *codeobj_b965da213ccdd2016ff765647bb022b5;
static PyCodeObject *codeobj_38981df49bd903f46e9a0bf6e41c9e1d;
static PyCodeObject *codeobj_cf88715a9b709f7dec9525d4a74a115f;
static PyCodeObject *codeobj_679b68139ff05bbcf536dcf2b682828d;
static PyCodeObject *codeobj_c40038b6156fbef9efcde8da2a92444c;
static PyCodeObject *codeobj_ebdd32db41cd24fd1f38bd9f050e42de;
static PyCodeObject *codeobj_5fc135b126a3190ee290dbf20de8f8ca;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_957539f8c5a0c569c341556fc0b67be9 );
    codeobj_f23b53a98b30354af9fd158782cd46e9 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 30, const_tuple_str_plain_args_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f35e7c1722912117ea0f4a28f2e33f3a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 36, const_tuple_str_plain_args_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2319d197081a7cadbd4501c95edde3e8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 89, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_af4ddcb98bc56d0debe98c79d1794ec8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 94, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_20c43f26e71a6e024337f6b06bbf6fb5 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 99, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c06bfb27b3331172ff1849bab432d7da = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 104, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_62dbde7746b177fd8206848f593ed530 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 121, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3a80fa0cd4b0b177736dab985cf22565 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 72, const_tuple_str_plain_key_str_plain_values_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a38a9cd0db8f5b0f9297fa38ba849984 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8bef79adb6a0c630d5dd7876e9d1fb34, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_d0d572e6a1be4063384dd873b2005de9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_async_select_or_reject, 18, const_tuple_846e08d8492d9227f9ba945b310292e8_tuple, 4, 0, CO_ASYNC_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_01d5a0773486b7a69b0e25409f159e1b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_asyncfiltervariant, 55, const_tuple_str_plain_original_str_plain_decorator_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1c53d575fcbe01c3ae447f6a9b40ae01 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_auto_to_seq, 7, const_tuple_str_plain_value_str_plain_seq_str_plain_item_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_94c4672f873eaf481092de964a189029 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_decorator, 56, const_tuple_str_plain_f_str_plain_original_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_95bdcaa04bbdebf08501f0a839b84489 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_first, 61, const_tuple_str_plain_environment_str_plain_seq_tuple, 2, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4d8a94c87df659cfd7277e613af56ba5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_groupby, 69, const_tuple_052dd9f13adb5de12f330ff7442179bf_tuple, 3, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0d831141192fa8a7efce569e9785bc7a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_join, 77, const_tuple_21d06e6b197a99895e647b626188cd6b_tuple, 4, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fc49021adb810318671fa076f3ff2e6c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_list, 82, const_tuple_str_plain_value_tuple, 1, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a5f30cb86b153f7dad62e406091ac311 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_map, 107, const_tuple_6ed83c1c0052f794a7be75d57cbd8cd2_tuple, 0, 0, CO_ASYNC_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_23012cc63c9bc71d68d6f0504e6f7792 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_reject, 87, const_tuple_str_plain_args_str_plain_kwargs_tuple, 0, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_b965da213ccdd2016ff765647bb022b5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_rejectattr, 92, const_tuple_str_plain_args_str_plain_kwargs_tuple, 0, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_38981df49bd903f46e9a0bf6e41c9e1d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_select, 97, const_tuple_str_plain_args_str_plain_kwargs_tuple, 0, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_cf88715a9b709f7dec9525d4a74a115f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_selectattr, 102, const_tuple_str_plain_args_str_plain_kwargs_tuple, 0, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_679b68139ff05bbcf536dcf2b682828d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_slice, 127, const_tuple_str_plain_value_str_plain_slices_str_plain_fill_with_tuple, 3, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c40038b6156fbef9efcde8da2a92444c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_do_sum, 115, const_tuple_0082c6de4b1f21379b2af5074b389ae0_tuple, 4, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ebdd32db41cd24fd1f38bd9f050e42de = MAKE_CODEOBJ( module_filename_obj, const_str_plain_dualfilter, 27, const_tuple_3ec0dcf8ca175ddd448caf41134e8073_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5fc135b126a3190ee290dbf20de8f8ca = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 38, const_tuple_73d8e85834b6f8b0788bd89fc35e87b6_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
}

// The module function declarations.
static PyObject *jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_maker( void );


static PyObject *jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_maker( void );


static PyObject *jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_maker( void );


static PyObject *jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_maker( void );


static PyObject *jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_maker( void );


static PyObject *jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_maker( void );


static PyObject *jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_maker( void );


static PyObject *jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_maker( void );


static PyObject *jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_maker( void );


static PyObject *jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_maker( void );


static PyObject *jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_maker( void );


static PyObject *jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_maker( void );


static PyObject *jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_10_do_rejectattr(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_11_do_select(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_12_do_selectattr(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_13_do_map(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_14_do_sum( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_15_do_slice( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_1_auto_to_seq(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_2_async_select_or_reject(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_3_wrapper(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_4_asyncfiltervariant(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_4_asyncfiltervariant$$$function_1_decorator(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_5_do_first(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_6_do_groupby(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_7_do_join( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_8_do_list(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_9_do_reject(  );


static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject$$$function_1_lambda(  );


// The module function definitions.
static PyObject *impl_jinja2$asyncfilters$$$function_1_auto_to_seq( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_value = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_value;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_1_auto_to_seq );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_1_auto_to_seq );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_locals {
    PyObject *var_seq;
    PyObject *var_item;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_for_loop_2__for_iterator;
    PyObject *tmp_for_loop_2__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_seq = NULL;
    coroutine_heap->var_item = NULL;
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;
    coroutine_heap->tmp_for_loop_2__for_iterator = NULL;
    coroutine_heap->tmp_for_loop_2__iter_value = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( coroutine_heap->var_seq == NULL );
        coroutine_heap->var_seq = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_1c53d575fcbe01c3ae447f6a9b40ae01, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_name_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 9;
            coroutine_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( coroutine->m_closure[0] );
        tmp_attribute_name_1 = const_str_plain___aiter__;
        coroutine_heap->tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 9;
            coroutine_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_expression_name_1;
            PyObject *tmp_value_name_1;
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }

            tmp_value_name_1 = PyCell_GET( coroutine->m_closure[0] );
            tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_1, sizeof(PyObject *), NULL );
            coroutine->m_yield_return_index = 1;
            coroutine->m_yieldfrom = tmp_expression_name_1;
            coroutine->m_awaiting = true;
            return NULL;

            yield_return_1:
            Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_1, sizeof(PyObject *), NULL );
            coroutine->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = yield_return_value;
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }
            assert( coroutine_heap->tmp_for_loop_1__for_iterator == NULL );
            coroutine_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_2;
        }
        // Tried code:
        loop_start_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_expression_name_2;
            PyObject *tmp_value_name_2;
            CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__for_iterator );
            tmp_value_name_2 = coroutine_heap->tmp_for_loop_1__for_iterator;
            tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
            if ( tmp_expression_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto try_except_handler_3;
            }
            Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_2, sizeof(PyObject *), NULL );
            coroutine->m_yield_return_index = 2;
            coroutine->m_yieldfrom = tmp_expression_name_2;
            coroutine->m_awaiting = true;
            return NULL;

            yield_return_2:
            Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_name_1, sizeof(PyObject *), &tmp_value_name_2, sizeof(PyObject *), NULL );
            coroutine->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto try_except_handler_3;
            }
            tmp_assign_source_3 = yield_return_value;
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = coroutine_heap->tmp_for_loop_1__iter_value;
                coroutine_heap->tmp_for_loop_1__iter_value = tmp_assign_source_3;
                Py_XDECREF( old );
            }

        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
        coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
        coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
        coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
        coroutine_heap->exception_type = NULL;
        coroutine_heap->exception_value = NULL;
        coroutine_heap->exception_tb = NULL;
        coroutine_heap->exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = coroutine_heap->exception_keeper_type_1;
            tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
            coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( coroutine_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

                Py_DECREF( coroutine_heap->exception_keeper_type_1 );
                Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
                Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );

                coroutine_heap->exception_lineno = 10;
                coroutine_heap->type_description_1 = "coo";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            Py_DECREF( coroutine_heap->exception_keeper_type_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );
            goto loop_end_1;
            goto branch_end_2;
            branch_no_2:;
            // Re-raise.
            coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
            coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
            coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
            coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

            goto try_except_handler_2;
            branch_end_2:;
        }
        // End of try:
        try_end_1:;
        {
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__iter_value );
            tmp_assign_source_4 = coroutine_heap->tmp_for_loop_1__iter_value;
            {
                PyObject *old = coroutine_heap->var_item;
                coroutine_heap->var_item = tmp_assign_source_4;
                Py_INCREF( coroutine_heap->var_item );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( coroutine_heap->var_seq );
            tmp_called_instance_1 = coroutine_heap->var_seq;
            CHECK_OBJECT( coroutine_heap->var_item );
            tmp_args_element_name_1 = coroutine_heap->var_item;
            coroutine->m_frame->m_frame.f_lineno = 11;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 11;
                coroutine_heap->type_description_1 = "coo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 10;
            coroutine_heap->type_description_1 = "coo";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
        coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
        coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
        coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
        coroutine_heap->exception_type = NULL;
        coroutine_heap->exception_value = NULL;
        coroutine_heap->exception_tb = NULL;
        coroutine_heap->exception_lineno = 0;

        Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
        coroutine_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
        coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
        coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
        coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
        coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
        coroutine_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
        coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_iter_arg_1;
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 13;
                coroutine_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }

            tmp_iter_arg_1 = PyCell_GET( coroutine->m_closure[0] );
            tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 13;
                coroutine_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }
            assert( coroutine_heap->tmp_for_loop_2__for_iterator == NULL );
            coroutine_heap->tmp_for_loop_2__for_iterator = tmp_assign_source_5;
        }
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( coroutine_heap->tmp_for_loop_2__for_iterator );
            tmp_next_source_1 = coroutine_heap->tmp_for_loop_2__for_iterator;
            tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    coroutine_heap->type_description_1 = "coo";
                    coroutine_heap->exception_lineno = 13;
                    goto try_except_handler_4;
                }
            }

            {
                PyObject *old = coroutine_heap->tmp_for_loop_2__iter_value;
                coroutine_heap->tmp_for_loop_2__iter_value = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( coroutine_heap->tmp_for_loop_2__iter_value );
            tmp_assign_source_7 = coroutine_heap->tmp_for_loop_2__iter_value;
            {
                PyObject *old = coroutine_heap->var_item;
                coroutine_heap->var_item = tmp_assign_source_7;
                Py_INCREF( coroutine_heap->var_item );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( coroutine_heap->var_seq );
            tmp_called_instance_2 = coroutine_heap->var_seq;
            CHECK_OBJECT( coroutine_heap->var_item );
            tmp_args_element_name_2 = coroutine_heap->var_item;
            coroutine->m_frame->m_frame.f_lineno = 14;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 14;
                coroutine_heap->type_description_1 = "coo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 13;
            coroutine_heap->type_description_1 = "coo";
            goto try_except_handler_4;
        }
        goto loop_start_2;
        loop_end_2:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_4:;
        coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
        coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
        coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
        coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
        coroutine_heap->exception_type = NULL;
        coroutine_heap->exception_value = NULL;
        coroutine_heap->exception_tb = NULL;
        coroutine_heap->exception_lineno = 0;

        Py_XDECREF( coroutine_heap->tmp_for_loop_2__iter_value );
        coroutine_heap->tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_2__for_iterator );
        Py_DECREF( coroutine_heap->tmp_for_loop_2__for_iterator );
        coroutine_heap->tmp_for_loop_2__for_iterator = NULL;

        // Re-raise.
        coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_3;
        coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_3;
        coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_3;
        coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_3;

        goto frame_exception_exit_1;
        // End of try:
        try_end_3:;
        Py_XDECREF( coroutine_heap->tmp_for_loop_2__iter_value );
        coroutine_heap->tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_2__for_iterator );
        Py_DECREF( coroutine_heap->tmp_for_loop_2__for_iterator );
        coroutine_heap->tmp_for_loop_2__for_iterator = NULL;

        branch_end_1:;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine_heap->var_seq,
            coroutine_heap->var_item
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( coroutine_heap->var_seq );
    coroutine_heap->tmp_return_value = coroutine_heap->var_seq;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_seq );
    Py_DECREF( coroutine_heap->var_seq );
    coroutine_heap->var_seq = NULL;

    Py_XDECREF( coroutine_heap->var_item );
    coroutine_heap->var_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_4 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_4 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_4 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_4 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_seq );
    Py_DECREF( coroutine_heap->var_seq );
    coroutine_heap->var_seq = NULL;

    Py_XDECREF( coroutine_heap->var_item );
    coroutine_heap->var_item = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_4;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_4;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_4;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_context,
        module_jinja2$asyncfilters,
        const_str_plain_auto_to_seq,
        NULL,
        codeobj_1c53d575fcbe01c3ae447f6a9b40ae01,
        1,
        sizeof(struct jinja2$asyncfilters$$$function_1_auto_to_seq$$$coroutine_1_auto_to_seq_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_2_async_select_or_reject( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_modfunc = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_lookup_attr = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_maker();

    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[2] = par_lookup_attr;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[3] = par_modfunc;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_2_async_select_or_reject );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)par_modfunc );
    Py_DECREF( par_modfunc );
    par_modfunc = NULL;

    CHECK_OBJECT( (PyObject *)par_lookup_attr );
    Py_DECREF( par_lookup_attr );
    par_lookup_attr = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)par_modfunc );
    Py_DECREF( par_modfunc );
    par_modfunc = NULL;

    CHECK_OBJECT( (PyObject *)par_lookup_attr );
    Py_DECREF( par_lookup_attr );
    par_lookup_attr = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_2_async_select_or_reject );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_locals {
    PyObject *var_seq;
    PyObject *var_func;
    PyObject *var_item;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    int tmp_res;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
};

static PyObject *jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_context( struct Nuitka_AsyncgenObject *asyncgen, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)asyncgen );
    assert( Nuitka_Asyncgen_Check( (PyObject *)asyncgen ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_locals *asyncgen_heap = (struct jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_locals *)asyncgen->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(asyncgen->m_yield_return_index) {
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    asyncgen_heap->var_seq = NULL;
    asyncgen_heap->var_func = NULL;
    asyncgen_heap->var_item = NULL;
    asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;
    asyncgen_heap->tmp_for_loop_1__iter_value = NULL;
    asyncgen_heap->tmp_tuple_unpack_1__element_1 = NULL;
    asyncgen_heap->tmp_tuple_unpack_1__element_2 = NULL;
    asyncgen_heap->tmp_tuple_unpack_1__source_iter = NULL;
    asyncgen_heap->type_description_1 = NULL;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    // Actual asyngen body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_d0d572e6a1be4063384dd873b2005de9, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    asyncgen->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( asyncgen->m_frame );
    assert( Py_REFCNT( asyncgen->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    asyncgen->m_frame->m_frame.f_gen = (PyObject *)asyncgen;
#endif

    Py_CLEAR( asyncgen->m_frame->m_frame.f_back );

    asyncgen->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( asyncgen->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &asyncgen->m_frame->m_frame;
    Py_INCREF( asyncgen->m_frame );

    Nuitka_Frame_MarkAsExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        asyncgen->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( asyncgen->m_frame->m_frame.f_exc_type == Py_None ) asyncgen->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_type );
    asyncgen->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_value );
    asyncgen->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_traceback );
#else
        asyncgen->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( asyncgen->m_exc_state.exc_type == Py_None ) asyncgen->m_exc_state.exc_type = NULL;
        Py_XINCREF( asyncgen->m_exc_state.exc_type );
        asyncgen->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_value );
        asyncgen->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 19;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_prepare_select_or_reject );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 19;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( asyncgen->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 20;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_1 = PyCell_GET( asyncgen->m_closure[0] );
        if ( PyCell_GET( asyncgen->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 20;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_2 = PyCell_GET( asyncgen->m_closure[1] );
        if ( PyCell_GET( asyncgen->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "modfunc" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 20;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_3 = PyCell_GET( asyncgen->m_closure[3] );
        if ( PyCell_GET( asyncgen->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "lookup_attr" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 20;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_4 = PyCell_GET( asyncgen->m_closure[2] );
        asyncgen->m_frame->m_frame.f_lineno = 19;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 19;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 19;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        assert( asyncgen_heap->tmp_tuple_unpack_1__source_iter == NULL );
        asyncgen_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = asyncgen_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                asyncgen_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = NULL;
                asyncgen_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            }


            asyncgen_heap->type_description_1 = "ccccooo";
            asyncgen_heap->exception_lineno = 19;
            goto try_except_handler_3;
        }
        assert( asyncgen_heap->tmp_tuple_unpack_1__element_1 == NULL );
        asyncgen_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = asyncgen_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                asyncgen_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = NULL;
                asyncgen_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            }


            asyncgen_heap->type_description_1 = "ccccooo";
            asyncgen_heap->exception_lineno = 19;
            goto try_except_handler_3;
        }
        assert( asyncgen_heap->tmp_tuple_unpack_1__element_2 == NULL );
        asyncgen_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = asyncgen_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        asyncgen_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( asyncgen_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

                    asyncgen_heap->type_description_1 = "ccccooo";
                    asyncgen_heap->exception_lineno = 19;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( asyncgen_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

            asyncgen_heap->type_description_1 = "ccccooo";
            asyncgen_heap->exception_lineno = 19;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    asyncgen_heap->exception_keeper_type_1 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_1 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_1 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_1 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    asyncgen_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_1;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_1;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_1;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    asyncgen_heap->exception_keeper_type_2 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_2 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_2 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_2 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_1 );
    asyncgen_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_2 );
    asyncgen_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_2;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_2;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_2;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    asyncgen_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = asyncgen_heap->tmp_tuple_unpack_1__element_1;
        assert( asyncgen_heap->var_seq == NULL );
        Py_INCREF( tmp_assign_source_4 );
        asyncgen_heap->var_seq = tmp_assign_source_4;
    }
    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_1 );
    asyncgen_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = asyncgen_heap->tmp_tuple_unpack_1__element_2;
        assert( asyncgen_heap->var_func == NULL );
        Py_INCREF( tmp_assign_source_5 );
        asyncgen_heap->var_func = tmp_assign_source_5;
    }
    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_2 );
    asyncgen_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( asyncgen_heap->var_seq );
        tmp_truth_name_1 = CHECK_IF_TRUE( asyncgen_heap->var_seq );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 21;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_expression_name_1;
            PyObject *tmp_value_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_5;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_aiter );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_aiter );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                asyncgen_heap->exception_type = PyExc_NameError;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_aiter" );
                asyncgen_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
                CHAIN_EXCEPTION( asyncgen_heap->exception_value );

                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( asyncgen_heap->var_seq );
            tmp_args_element_name_5 = asyncgen_heap->var_seq;
            asyncgen->m_frame->m_frame.f_lineno = 22;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_value_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_value_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }
            tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
            Py_DECREF( tmp_value_name_1 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 1;
            asyncgen->m_yieldfrom = tmp_expression_name_1;
            asyncgen->m_awaiting = true;
            return NULL;

            yield_return_1:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), NULL );
            asyncgen->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_6 = yield_return_value;
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }
            assert( asyncgen_heap->tmp_for_loop_1__for_iterator == NULL );
            asyncgen_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_6;
        }
        // Tried code:
        loop_start_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_expression_name_2;
            PyObject *tmp_value_name_2;
            CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__for_iterator );
            tmp_value_name_2 = asyncgen_heap->tmp_for_loop_1__for_iterator;
            tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
            if ( tmp_expression_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto try_except_handler_5;
            }
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_2, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 2;
            asyncgen->m_yieldfrom = tmp_expression_name_2;
            asyncgen->m_awaiting = true;
            return NULL;

            yield_return_2:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_2, sizeof(PyObject *), NULL );
            asyncgen->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto try_except_handler_5;
            }
            tmp_assign_source_7 = yield_return_value;
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = asyncgen_heap->tmp_for_loop_1__iter_value;
                asyncgen_heap->tmp_for_loop_1__iter_value = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_5:;
        asyncgen_heap->exception_keeper_type_3 = asyncgen_heap->exception_type;
        asyncgen_heap->exception_keeper_value_3 = asyncgen_heap->exception_value;
        asyncgen_heap->exception_keeper_tb_3 = asyncgen_heap->exception_tb;
        asyncgen_heap->exception_keeper_lineno_3 = asyncgen_heap->exception_lineno;
        asyncgen_heap->exception_type = NULL;
        asyncgen_heap->exception_value = NULL;
        asyncgen_heap->exception_tb = NULL;
        asyncgen_heap->exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = asyncgen_heap->exception_keeper_type_3;
            tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
            asyncgen_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( asyncgen_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

                Py_DECREF( asyncgen_heap->exception_keeper_type_3 );
                Py_XDECREF( asyncgen_heap->exception_keeper_value_3 );
                Py_XDECREF( asyncgen_heap->exception_keeper_tb_3 );

                asyncgen_heap->exception_lineno = 22;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( asyncgen_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            Py_DECREF( asyncgen_heap->exception_keeper_type_3 );
            Py_XDECREF( asyncgen_heap->exception_keeper_value_3 );
            Py_XDECREF( asyncgen_heap->exception_keeper_tb_3 );
            goto loop_end_1;
            goto branch_end_2;
            branch_no_2:;
            // Re-raise.
            asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_3;
            asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_3;
            asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_3;
            asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_3;

            goto try_except_handler_4;
            branch_end_2:;
        }
        // End of try:
        try_end_3:;
        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__iter_value );
            tmp_assign_source_8 = asyncgen_heap->tmp_for_loop_1__iter_value;
            {
                PyObject *old = asyncgen_heap->var_item;
                asyncgen_heap->var_item = tmp_assign_source_8;
                Py_INCREF( asyncgen_heap->var_item );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_called_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_6;
            int tmp_truth_name_2;
            CHECK_OBJECT( asyncgen_heap->var_func );
            tmp_called_name_3 = asyncgen_heap->var_func;
            CHECK_OBJECT( asyncgen_heap->var_item );
            tmp_args_element_name_6 = asyncgen_heap->var_item;
            asyncgen->m_frame->m_frame.f_lineno = 23;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 23;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto try_except_handler_4;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
                Py_DECREF( tmp_call_result_1 );

                asyncgen_heap->exception_lineno = 23;
                asyncgen_heap->type_description_1 = "ccccooo";
                goto try_except_handler_4;
            }
            tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_expression_name_3;
                NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
                CHECK_OBJECT( asyncgen_heap->var_item );
                tmp_expression_name_3 = asyncgen_heap->var_item;
                Py_INCREF( tmp_expression_name_3 );
                Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_called_name_3, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), NULL );
                asyncgen->m_yield_return_index = 3;
                return tmp_expression_name_3;
                yield_return_3:
                Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_called_name_3, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), NULL );
                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                    asyncgen_heap->exception_lineno = 24;
                    asyncgen_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }
                tmp_yield_result_1 = yield_return_value;
            }
            branch_no_3:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 22;
            asyncgen_heap->type_description_1 = "ccccooo";
            goto try_except_handler_4;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_4:;
        asyncgen_heap->exception_keeper_type_4 = asyncgen_heap->exception_type;
        asyncgen_heap->exception_keeper_value_4 = asyncgen_heap->exception_value;
        asyncgen_heap->exception_keeper_tb_4 = asyncgen_heap->exception_tb;
        asyncgen_heap->exception_keeper_lineno_4 = asyncgen_heap->exception_lineno;
        asyncgen_heap->exception_type = NULL;
        asyncgen_heap->exception_value = NULL;
        asyncgen_heap->exception_tb = NULL;
        asyncgen_heap->exception_lineno = 0;

        Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
        asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
        asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_4;
        asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_4;
        asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_4;
        asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
        asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
        asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

        branch_no_1:;
    }

    Nuitka_Frame_MarkAsNotExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( asyncgen->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( asyncgen_heap->exception_type ) )
    {
        if ( asyncgen_heap->exception_tb == NULL )
        {
            asyncgen_heap->exception_tb = MAKE_TRACEBACK( asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }
        else if ( asyncgen_heap->exception_tb->tb_frame != &asyncgen->m_frame->m_frame )
        {
            asyncgen_heap->exception_tb = ADD_TRACEBACK( asyncgen_heap->exception_tb, asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)asyncgen->m_frame,
            asyncgen_heap->type_description_1,
            asyncgen->m_closure[0],
            asyncgen->m_closure[1],
            asyncgen->m_closure[3],
            asyncgen->m_closure[2],
            asyncgen_heap->var_seq,
            asyncgen_heap->var_func,
            asyncgen_heap->var_item
        );


        // Release cached frame.
        if ( asyncgen->m_frame == cache_m_frame )
        {
            Py_DECREF( asyncgen->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( asyncgen->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( asyncgen->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)asyncgen_heap->var_seq );
    Py_DECREF( asyncgen_heap->var_seq );
    asyncgen_heap->var_seq = NULL;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->var_func );
    Py_DECREF( asyncgen_heap->var_func );
    asyncgen_heap->var_func = NULL;

    Py_XDECREF( asyncgen_heap->var_item );
    asyncgen_heap->var_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    asyncgen_heap->exception_keeper_type_5 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_5 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_5 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_5 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->var_seq );
    asyncgen_heap->var_seq = NULL;

    Py_XDECREF( asyncgen_heap->var_func );
    asyncgen_heap->var_func = NULL;

    Py_XDECREF( asyncgen_heap->var_item );
    asyncgen_heap->var_item = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_5;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_5;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_5;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject );

    function_exception_exit:
    assert( asyncgen_heap->exception_type );
    RESTORE_ERROR_OCCURRED( asyncgen_heap->exception_type, asyncgen_heap->exception_value, asyncgen_heap->exception_tb );
    return NULL;
    function_return_exit:;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_maker( void )
{
    return Nuitka_Asyncgen_New(
        jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_context,
        module_jinja2$asyncfilters,
        const_str_plain_async_select_or_reject,
        NULL,
        codeobj_d0d572e6a1be4063384dd873b2005de9,
        4,
        sizeof(struct jinja2$asyncfilters$$$function_2_async_select_or_reject$$$asyncgen_1_async_select_or_reject_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_3_dualfilter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_normal_filter = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_async_filter = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *var_wrap_evalctx = PyCell_EMPTY();
    struct Nuitka_CellObject *var_is_async = PyCell_EMPTY();
    PyObject *var_wrapper = NULL;
    struct Nuitka_FrameObject *frame_ebdd32db41cd24fd1f38bd9f050e42de;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_ebdd32db41cd24fd1f38bd9f050e42de = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_False;
        assert( PyCell_GET( var_wrap_evalctx ) == NULL );
        Py_INCREF( tmp_assign_source_1 );
        PyCell_SET( var_wrap_evalctx, tmp_assign_source_1 );

    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ebdd32db41cd24fd1f38bd9f050e42de, codeobj_ebdd32db41cd24fd1f38bd9f050e42de, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ebdd32db41cd24fd1f38bd9f050e42de = cache_frame_ebdd32db41cd24fd1f38bd9f050e42de;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ebdd32db41cd24fd1f38bd9f050e42de );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ebdd32db41cd24fd1f38bd9f050e42de ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_getattr_default_1;
        PyObject *tmp_capi_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( PyCell_GET( par_normal_filter ) );
        tmp_getattr_target_1 = PyCell_GET( par_normal_filter );
        tmp_getattr_attr_1 = const_str_plain_environmentfilter;
        tmp_getattr_default_1 = Py_False;
        tmp_capi_result_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
        if ( tmp_capi_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_capi_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_capi_result_1 );

            exception_lineno = 29;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_capi_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_1_lambda(  );



            assert( PyCell_GET( var_is_async ) == NULL );
            PyCell_SET( var_is_async, tmp_assign_source_2 );

        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = Py_False;
            {
                PyObject *old = PyCell_GET( var_wrap_evalctx );
                PyCell_SET( var_wrap_evalctx, tmp_assign_source_3 );
                Py_INCREF( tmp_assign_source_3 );
                Py_XDECREF( old );
            }

        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_getattr_target_2;
            PyObject *tmp_getattr_attr_2;
            PyObject *tmp_getattr_default_2;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_getattr_target_3;
            PyObject *tmp_getattr_attr_3;
            PyObject *tmp_getattr_default_3;
            CHECK_OBJECT( PyCell_GET( par_normal_filter ) );
            tmp_getattr_target_2 = PyCell_GET( par_normal_filter );
            tmp_getattr_attr_2 = const_str_plain_evalcontextfilter;
            tmp_getattr_default_2 = Py_False;
            tmp_operand_name_1 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "cccco";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "cccco";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( PyCell_GET( par_normal_filter ) );
            tmp_getattr_target_3 = PyCell_GET( par_normal_filter );
            tmp_getattr_attr_3 = const_str_plain_contextfilter;
            tmp_getattr_default_3 = Py_False;
            tmp_operand_name_2 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "cccco";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "cccco";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_2 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_4;
                tmp_assign_source_4 = Py_True;
                {
                    PyObject *old = PyCell_GET( var_wrap_evalctx );
                    PyCell_SET( var_wrap_evalctx, tmp_assign_source_4 );
                    Py_INCREF( tmp_assign_source_4 );
                    Py_XDECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_2_lambda(  );



            assert( PyCell_GET( var_is_async ) == NULL );
            PyCell_SET( var_is_async, tmp_assign_source_5 );

        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_wraps );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wraps );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wraps" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( PyCell_GET( par_normal_filter ) );
        tmp_args_element_name_1 = PyCell_GET( par_normal_filter );
        frame_ebdd32db41cd24fd1f38bd9f050e42de->m_frame.f_lineno = 38;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_3_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] = par_async_filter;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] = var_is_async;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[2] = par_normal_filter;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[3] = var_wrap_evalctx;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[3] );


        frame_ebdd32db41cd24fd1f38bd9f050e42de->m_frame.f_lineno = 38;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_2;
        if ( PyCell_GET( var_wrap_evalctx ) == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "wrap_evalctx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_2 = CHECK_IF_TRUE( PyCell_GET( var_wrap_evalctx ) );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            tmp_assattr_name_1 = Py_True;
            CHECK_OBJECT( var_wrapper );
            tmp_assattr_target_1 = var_wrapper;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_evalcontextfilter, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;
                type_description_1 = "cccco";
                goto frame_exception_exit_1;
            }
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_True;
        CHECK_OBJECT( var_wrapper );
        tmp_assattr_target_2 = var_wrapper;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_asyncfiltervariant, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "cccco";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ebdd32db41cd24fd1f38bd9f050e42de );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ebdd32db41cd24fd1f38bd9f050e42de );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ebdd32db41cd24fd1f38bd9f050e42de, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ebdd32db41cd24fd1f38bd9f050e42de->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ebdd32db41cd24fd1f38bd9f050e42de, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ebdd32db41cd24fd1f38bd9f050e42de,
        type_description_1,
        par_normal_filter,
        par_async_filter,
        var_wrap_evalctx,
        var_is_async,
        var_wrapper
    );


    // Release cached frame.
    if ( frame_ebdd32db41cd24fd1f38bd9f050e42de == cache_frame_ebdd32db41cd24fd1f38bd9f050e42de )
    {
        Py_DECREF( frame_ebdd32db41cd24fd1f38bd9f050e42de );
    }
    cache_frame_ebdd32db41cd24fd1f38bd9f050e42de = NULL;

    assertFrameObject( frame_ebdd32db41cd24fd1f38bd9f050e42de );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_normal_filter );
    Py_DECREF( par_normal_filter );
    par_normal_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_async_filter );
    Py_DECREF( par_async_filter );
    par_async_filter = NULL;

    CHECK_OBJECT( (PyObject *)var_wrap_evalctx );
    Py_DECREF( var_wrap_evalctx );
    var_wrap_evalctx = NULL;

    CHECK_OBJECT( (PyObject *)var_is_async );
    Py_DECREF( var_is_async );
    var_is_async = NULL;

    CHECK_OBJECT( (PyObject *)var_wrapper );
    Py_DECREF( var_wrapper );
    var_wrapper = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_normal_filter );
    Py_DECREF( par_normal_filter );
    par_normal_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_async_filter );
    Py_DECREF( par_async_filter );
    par_async_filter = NULL;

    CHECK_OBJECT( (PyObject *)var_wrap_evalctx );
    Py_DECREF( var_wrap_evalctx );
    var_wrap_evalctx = NULL;

    CHECK_OBJECT( (PyObject *)var_is_async );
    Py_DECREF( var_is_async );
    var_is_async = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_3_dualfilter$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f23b53a98b30354af9fd158782cd46e9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f23b53a98b30354af9fd158782cd46e9 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f23b53a98b30354af9fd158782cd46e9, codeobj_f23b53a98b30354af9fd158782cd46e9, module_jinja2$asyncfilters, sizeof(void *) );
    frame_f23b53a98b30354af9fd158782cd46e9 = cache_frame_f23b53a98b30354af9fd158782cd46e9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f23b53a98b30354af9fd158782cd46e9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f23b53a98b30354af9fd158782cd46e9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_args );
        tmp_subscribed_name_1 = par_args;
        tmp_subscript_name_1 = const_int_0;
        tmp_source_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f23b53a98b30354af9fd158782cd46e9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f23b53a98b30354af9fd158782cd46e9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f23b53a98b30354af9fd158782cd46e9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f23b53a98b30354af9fd158782cd46e9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f23b53a98b30354af9fd158782cd46e9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f23b53a98b30354af9fd158782cd46e9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f23b53a98b30354af9fd158782cd46e9,
        type_description_1,
        par_args
    );


    // Release cached frame.
    if ( frame_f23b53a98b30354af9fd158782cd46e9 == cache_frame_f23b53a98b30354af9fd158782cd46e9 )
    {
        Py_DECREF( frame_f23b53a98b30354af9fd158782cd46e9 );
    }
    cache_frame_f23b53a98b30354af9fd158782cd46e9 = NULL;

    assertFrameObject( frame_f23b53a98b30354af9fd158782cd46e9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_3_dualfilter$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f35e7c1722912117ea0f4a28f2e33f3a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f35e7c1722912117ea0f4a28f2e33f3a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f35e7c1722912117ea0f4a28f2e33f3a, codeobj_f35e7c1722912117ea0f4a28f2e33f3a, module_jinja2$asyncfilters, sizeof(void *) );
    frame_f35e7c1722912117ea0f4a28f2e33f3a = cache_frame_f35e7c1722912117ea0f4a28f2e33f3a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f35e7c1722912117ea0f4a28f2e33f3a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f35e7c1722912117ea0f4a28f2e33f3a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_args );
        tmp_subscribed_name_1 = par_args;
        tmp_subscript_name_1 = const_int_0;
        tmp_source_name_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_environment );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_async );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f35e7c1722912117ea0f4a28f2e33f3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f35e7c1722912117ea0f4a28f2e33f3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f35e7c1722912117ea0f4a28f2e33f3a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f35e7c1722912117ea0f4a28f2e33f3a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f35e7c1722912117ea0f4a28f2e33f3a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f35e7c1722912117ea0f4a28f2e33f3a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f35e7c1722912117ea0f4a28f2e33f3a,
        type_description_1,
        par_args
    );


    // Release cached frame.
    if ( frame_f35e7c1722912117ea0f4a28f2e33f3a == cache_frame_f35e7c1722912117ea0f4a28f2e33f3a )
    {
        Py_DECREF( frame_f35e7c1722912117ea0f4a28f2e33f3a );
    }
    cache_frame_f35e7c1722912117ea0f4a28f2e33f3a = NULL;

    assertFrameObject( frame_f35e7c1722912117ea0f4a28f2e33f3a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter$$$function_2_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter$$$function_2_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_3_dualfilter$$$function_3_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_b = NULL;
    struct Nuitka_FrameObject *frame_5fc135b126a3190ee290dbf20de8f8ca;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5fc135b126a3190ee290dbf20de8f8ca = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5fc135b126a3190ee290dbf20de8f8ca, codeobj_5fc135b126a3190ee290dbf20de8f8ca, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5fc135b126a3190ee290dbf20de8f8ca = cache_frame_5fc135b126a3190ee290dbf20de8f8ca;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5fc135b126a3190ee290dbf20de8f8ca );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5fc135b126a3190ee290dbf20de8f8ca ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "is_async" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( par_args );
        tmp_args_element_name_1 = par_args;
        frame_5fc135b126a3190ee290dbf20de8f8ca->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "wrap_evalctx" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[3] ) );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_args );
            tmp_subscribed_name_1 = par_args;
            tmp_subscript_name_1 = const_slice_int_pos_1_none_none;
            tmp_assign_source_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_args;
                assert( old != NULL );
                par_args = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_b );
        tmp_truth_name_2 = CHECK_IF_TRUE( var_b );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_dircall_arg2_1;
            PyObject *tmp_dircall_arg3_1;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "async_filter" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 44;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }

            tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( par_args );
            tmp_dircall_arg2_1 = par_args;
            CHECK_OBJECT( par_kwargs );
            tmp_dircall_arg3_1 = par_kwargs;
            Py_INCREF( tmp_dircall_arg1_1 );
            Py_INCREF( tmp_dircall_arg2_1 );
            Py_INCREF( tmp_dircall_arg3_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
                tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
            }
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_dircall_arg3_2;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "normal_filter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_2 = PyCell_GET( self->m_closure[2] );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_2 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_2 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_2 );
        Py_INCREF( tmp_dircall_arg2_2 );
        Py_INCREF( tmp_dircall_arg3_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5fc135b126a3190ee290dbf20de8f8ca );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5fc135b126a3190ee290dbf20de8f8ca );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5fc135b126a3190ee290dbf20de8f8ca );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5fc135b126a3190ee290dbf20de8f8ca, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5fc135b126a3190ee290dbf20de8f8ca->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5fc135b126a3190ee290dbf20de8f8ca, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5fc135b126a3190ee290dbf20de8f8ca,
        type_description_1,
        par_args,
        par_kwargs,
        var_b,
        self->m_closure[1],
        self->m_closure[3],
        self->m_closure[0],
        self->m_closure[2]
    );


    // Release cached frame.
    if ( frame_5fc135b126a3190ee290dbf20de8f8ca == cache_frame_5fc135b126a3190ee290dbf20de8f8ca )
    {
        Py_DECREF( frame_5fc135b126a3190ee290dbf20de8f8ca );
    }
    cache_frame_5fc135b126a3190ee290dbf20de8f8ca = NULL;

    assertFrameObject( frame_5fc135b126a3190ee290dbf20de8f8ca );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter$$$function_3_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_3_dualfilter$$$function_3_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_4_asyncfiltervariant( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_original = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_decorator = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_4_asyncfiltervariant$$$function_1_decorator(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_original;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_decorator == NULL );
        var_decorator = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_decorator );
    tmp_return_value = var_decorator;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_4_asyncfiltervariant );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_original );
    Py_DECREF( par_original );
    par_original = NULL;

    CHECK_OBJECT( (PyObject *)var_decorator );
    Py_DECREF( var_decorator );
    var_decorator = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_4_asyncfiltervariant );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_4_asyncfiltervariant$$$function_1_decorator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_f = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_94c4672f873eaf481092de964a189029;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_94c4672f873eaf481092de964a189029 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_94c4672f873eaf481092de964a189029, codeobj_94c4672f873eaf481092de964a189029, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *) );
    frame_94c4672f873eaf481092de964a189029 = cache_frame_94c4672f873eaf481092de964a189029;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_94c4672f873eaf481092de964a189029 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_94c4672f873eaf481092de964a189029 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_dualfilter );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dualfilter );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dualfilter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "original" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_f );
        tmp_args_element_name_2 = par_f;
        frame_94c4672f873eaf481092de964a189029->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_94c4672f873eaf481092de964a189029 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_94c4672f873eaf481092de964a189029 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_94c4672f873eaf481092de964a189029 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_94c4672f873eaf481092de964a189029, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_94c4672f873eaf481092de964a189029->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_94c4672f873eaf481092de964a189029, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_94c4672f873eaf481092de964a189029,
        type_description_1,
        par_f,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_94c4672f873eaf481092de964a189029 == cache_frame_94c4672f873eaf481092de964a189029 )
    {
        Py_DECREF( frame_94c4672f873eaf481092de964a189029 );
    }
    cache_frame_94c4672f873eaf481092de964a189029 = NULL;

    assertFrameObject( frame_94c4672f873eaf481092de964a189029 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_4_asyncfiltervariant$$$function_1_decorator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_f );
    Py_DECREF( par_f );
    par_f = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_f );
    Py_DECREF( par_f );
    par_f = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_4_asyncfiltervariant$$$function_1_decorator );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_5_do_first( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_environment = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_seq = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_environment;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_seq;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_5_do_first );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_seq );
    Py_DECREF( par_seq );
    par_seq = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_seq );
    Py_DECREF( par_seq );
    par_seq = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_5_do_first );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_95bdcaa04bbdebf08501f0a839b84489, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        coroutine->m_frame->m_frame.f_lineno = 64;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_aiter );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_aiter );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_aiter" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "seq" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[1] );
        coroutine->m_frame->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_1;
        }
        coroutine->m_frame->m_frame.f_lineno = 64;
        tmp_expression_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain___anext__ );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_1;
        }
        coroutine_heap->tmp_return_value = yield_return_value;
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 64;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_1;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Preserve existing published exception.
    coroutine_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_type_1 );
    coroutine_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_value_1 );
    coroutine_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( coroutine_heap->exception_preserved_tb_1 );

    if ( coroutine_heap->exception_keeper_tb_1 == NULL )
    {
        coroutine_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_keeper_lineno_1 );
    }
    else if ( coroutine_heap->exception_keeper_lineno_1 != 0 )
    {
        coroutine_heap->exception_keeper_tb_1 = ADD_TRACEBACK( coroutine_heap->exception_keeper_tb_1, coroutine->m_frame, coroutine_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &coroutine_heap->exception_keeper_type_1, &coroutine_heap->exception_keeper_value_1, &coroutine_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( coroutine_heap->exception_keeper_value_1, (PyObject *)coroutine_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &coroutine_heap->exception_keeper_type_1, &coroutine_heap->exception_keeper_value_1, &coroutine_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 65;
            coroutine_heap->type_description_1 = "cc";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_2;
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "environment" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 66;
                coroutine_heap->type_description_1 = "cc";
                goto try_except_handler_2;
            }

            tmp_called_instance_2 = PyCell_GET( coroutine->m_closure[0] );
            coroutine->m_frame->m_frame.f_lineno = 66;
            coroutine_heap->tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_undefined, &PyTuple_GET_ITEM( const_tuple_str_digest_0b73b59fa5aedb66ee40bf6034307534_tuple, 0 ) );

            if ( coroutine_heap->tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 66;
                coroutine_heap->type_description_1 = "cc";
                goto try_except_handler_2;
            }
            goto try_return_handler_2;
        }
        goto branch_end_1;
        branch_no_1:;
        coroutine_heap->tmp_result = RERAISE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
        if (unlikely( coroutine_heap->tmp_result == false ))
        {
            coroutine_heap->exception_lineno = 63;
        }

        if (coroutine_heap->exception_tb && coroutine_heap->exception_tb->tb_frame == &coroutine->m_frame->m_frame) coroutine->m_frame->m_frame.f_lineno = coroutine_heap->exception_tb->tb_lineno;
        coroutine_heap->type_description_1 = "cc";
        goto try_except_handler_2;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( coroutine_heap->exception_preserved_type_1, coroutine_heap->exception_preserved_value_1, coroutine_heap->exception_preserved_tb_1 );
    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine->m_closure[1]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_first,
        NULL,
        codeobj_95bdcaa04bbdebf08501f0a839b84489,
        2,
        sizeof(struct jinja2$asyncfilters$$$function_5_do_first$$$coroutine_1_do_first_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_6_do_groupby( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_environment = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_value = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_attribute = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_attribute;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_environment;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_value;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_6_do_groupby );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_6_do_groupby );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_locals {
    PyObject *var_expr;
    PyObject *outline_0_var_key;
    PyObject *outline_0_var_values;
    PyObject *tmp_listcomp$tuple_unpack_1__element_1;
    PyObject *tmp_listcomp$tuple_unpack_1__element_2;
    PyObject *tmp_listcomp$tuple_unpack_1__source_iter;
    PyObject *tmp_listcomp_1__$0;
    PyObject *tmp_listcomp_1__contraction;
    PyObject *tmp_listcomp_1__iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_return_value;
    char yield_tmps[1024];
    int tmp_res;
    struct Nuitka_FrameObject *frame_3a80fa0cd4b0b177736dab985cf22565_2;
    char const *type_description_2;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
};

static PyObject *jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_frame_3a80fa0cd4b0b177736dab985cf22565_2 = NULL;
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_expr = NULL;
    coroutine_heap->outline_0_var_key = NULL;
    coroutine_heap->outline_0_var_values = NULL;
    coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1 = NULL;
    coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2 = NULL;
    coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter = NULL;
    coroutine_heap->tmp_listcomp_1__$0 = NULL;
    coroutine_heap->tmp_listcomp_1__contraction = NULL;
    coroutine_heap->tmp_listcomp_1__iter_value_0 = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->type_description_2 = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_4d8a94c87df659cfd7277e613af56ba5, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 71;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_make_attrgetter );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 71;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "environment" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 71;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[1] );
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "attribute" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 71;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 71;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 71;
            coroutine_heap->type_description_1 = "ccco";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_expr == NULL );
        coroutine_heap->var_expr = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 73;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_groupby );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 73;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_3 != NULL );
        coroutine->m_frame->m_frame.f_lineno = 74;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_to_seq );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_to_seq );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_to_seq" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 74;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }

        tmp_called_name_4 = tmp_mvar_value_3;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 74;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }

        tmp_args_element_name_4 = PyCell_GET( coroutine->m_closure[2] );
        coroutine->m_frame->m_frame.f_lineno = 74;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_expression_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            coroutine_heap->exception_lineno = 74;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            coroutine_heap->exception_lineno = 74;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_assign_source_2, sizeof(PyObject *), &tmp_iter_arg_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_4, sizeof(PyObject *), &tmp_mvar_value_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_assign_source_2, sizeof(PyObject *), &tmp_iter_arg_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_4, sizeof(PyObject *), &tmp_mvar_value_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            coroutine_heap->exception_lineno = 74;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        tmp_tuple_element_1 = yield_return_value;
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            coroutine_heap->exception_lineno = 74;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_key;
        CHECK_OBJECT( coroutine_heap->var_expr );
        tmp_dict_value_1 = coroutine_heap->var_expr;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(coroutine_heap->tmp_res != 0) );
        coroutine->m_frame->m_frame.f_lineno = 73;
        tmp_args_element_name_3 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            coroutine_heap->exception_lineno = 73;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( coroutine_heap->var_expr );
        tmp_args_element_name_5 = coroutine_heap->var_expr;
        coroutine->m_frame->m_frame.f_lineno = 73;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_5 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 73;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_1 = "ccco";
            goto try_except_handler_2;
        }
        assert( coroutine_heap->tmp_listcomp_1__$0 == NULL );
        coroutine_heap->tmp_listcomp_1__$0 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyList_New( 0 );
        assert( coroutine_heap->tmp_listcomp_1__contraction == NULL );
        coroutine_heap->tmp_listcomp_1__contraction = tmp_assign_source_3;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_3a80fa0cd4b0b177736dab985cf22565_2, codeobj_3a80fa0cd4b0b177736dab985cf22565, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *) );
    coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 = cache_frame_3a80fa0cd4b0b177736dab985cf22565_2;

    // Push the new frame as the currently active one.
    pushFrameStack( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp_1__$0 );
        tmp_next_source_1 = coroutine_heap->tmp_listcomp_1__$0;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                coroutine_heap->type_description_2 = "oo";
                coroutine_heap->exception_lineno = 72;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = coroutine_heap->tmp_listcomp_1__iter_value_0;
            coroutine_heap->tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp_1__iter_value_0 );
        tmp_iter_arg_2 = coroutine_heap->tmp_listcomp_1__iter_value_0;
        tmp_assign_source_5 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_4;
        }
        {
            PyObject *old = coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter;
            coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter );
        tmp_unpack_1 = coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                coroutine_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = NULL;
                coroutine_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            }


            coroutine_heap->type_description_2 = "oo";
            coroutine_heap->exception_lineno = 72;
            goto try_except_handler_5;
        }
        {
            PyObject *old = coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1;
            coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter );
        tmp_unpack_2 = coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                coroutine_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = NULL;
                coroutine_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            }


            coroutine_heap->type_description_2 = "oo";
            coroutine_heap->exception_lineno = 72;
            goto try_except_handler_5;
        }
        {
            PyObject *old = coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2;
            coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        coroutine_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( coroutine_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

                    coroutine_heap->type_description_2 = "oo";
                    coroutine_heap->exception_lineno = 72;
                    goto try_except_handler_5;
                }
            }
        }
        else
        {
            Py_DECREF( coroutine_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

            coroutine_heap->type_description_2 = "oo";
            coroutine_heap->exception_lineno = 72;
            goto try_except_handler_5;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_5:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter );
    Py_DECREF( coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter );
    coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

    goto try_except_handler_4;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1 );
    coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2 );
    coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter );
    Py_DECREF( coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter );
    coroutine_heap->tmp_listcomp$tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1 );
        tmp_assign_source_8 = coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1;
        {
            PyObject *old = coroutine_heap->outline_0_var_key;
            coroutine_heap->outline_0_var_key = tmp_assign_source_8;
            Py_INCREF( coroutine_heap->outline_0_var_key );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1 );
    coroutine_heap->tmp_listcomp$tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2 );
        tmp_assign_source_9 = coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2;
        {
            PyObject *old = coroutine_heap->outline_0_var_values;
            coroutine_heap->outline_0_var_values = tmp_assign_source_9;
            Py_INCREF( coroutine_heap->outline_0_var_values );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2 );
    coroutine_heap->tmp_listcomp$tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_append_list_1;
        PyObject *tmp_append_value_1;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_expression_name_3;
        PyObject *tmp_expression_name_4;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_8;
        CHECK_OBJECT( coroutine_heap->tmp_listcomp_1__contraction );
        tmp_append_list_1 = coroutine_heap->tmp_listcomp_1__contraction;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }

        tmp_source_name_3 = tmp_mvar_value_4;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__GroupTuple );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }
        CHECK_OBJECT( coroutine_heap->outline_0_var_key );
        tmp_args_element_name_6 = coroutine_heap->outline_0_var_key;
        coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2->m_frame.f_lineno = 72;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_to_seq );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_to_seq );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_to_seq" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }

        tmp_called_name_6 = tmp_mvar_value_5;
        CHECK_OBJECT( coroutine_heap->outline_0_var_values );
        tmp_args_element_name_8 = coroutine_heap->outline_0_var_values;
        coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_expression_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_expression_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_5 );

            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }
        tmp_expression_name_3 = ASYNC_AWAIT( tmp_expression_name_4, await_normal );
        Py_DECREF( tmp_expression_name_4 );
        if ( tmp_expression_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_5 );

            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_append_list_1, sizeof(PyObject *), &tmp_append_value_1, sizeof(PyObject *), &tmp_called_name_5, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_expression_name_4, sizeof(PyObject *), &tmp_called_name_6, sizeof(PyObject *), &tmp_mvar_value_5, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 2;
        coroutine->m_yieldfrom = tmp_expression_name_3;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_2:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_append_list_1, sizeof(PyObject *), &tmp_append_value_1, sizeof(PyObject *), &tmp_called_name_5, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_expression_name_4, sizeof(PyObject *), &tmp_called_name_6, sizeof(PyObject *), &tmp_mvar_value_5, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_5 );

            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }
        tmp_args_element_name_7 = yield_return_value;
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_5 );

            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }
        coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_append_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }
        assert( PyList_Check( tmp_append_list_1 ) );
        coroutine_heap->tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
        Py_DECREF( tmp_append_value_1 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 72;
            coroutine_heap->type_description_2 = "oo";
            goto try_except_handler_3;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


        coroutine_heap->exception_lineno = 72;
        coroutine_heap->type_description_2 = "oo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( coroutine_heap->tmp_listcomp_1__contraction );
    coroutine_heap->tmp_return_value = coroutine_heap->tmp_listcomp_1__contraction;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_listcomp_1__$0 );
    Py_DECREF( coroutine_heap->tmp_listcomp_1__$0 );
    coroutine_heap->tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_listcomp_1__contraction );
    Py_DECREF( coroutine_heap->tmp_listcomp_1__contraction );
    coroutine_heap->tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( coroutine_heap->tmp_listcomp_1__iter_value_0 );
    coroutine_heap->tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_listcomp_1__$0 );
    Py_DECREF( coroutine_heap->tmp_listcomp_1__$0 );
    coroutine_heap->tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_listcomp_1__contraction );
    Py_DECREF( coroutine_heap->tmp_listcomp_1__contraction );
    coroutine_heap->tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( coroutine_heap->tmp_listcomp_1__iter_value_0 );
    coroutine_heap->tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_3;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_3;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_3;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 );
#endif

    if ( coroutine_heap->exception_tb == NULL )
    {
        coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2, coroutine_heap->exception_lineno );
    }
    else if ( coroutine_heap->exception_tb->tb_frame != &coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2->m_frame )
    {
        coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2, coroutine_heap->exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2,
        coroutine_heap->type_description_2,
        coroutine_heap->outline_0_var_key,
        coroutine_heap->outline_0_var_values
    );


    // Release cached frame.
    if ( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 == cache_frame_3a80fa0cd4b0b177736dab985cf22565_2 )
    {
        Py_DECREF( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 );
    }
    cache_frame_3a80fa0cd4b0b177736dab985cf22565_2 = NULL;

    assertFrameObject( coroutine_heap->frame_3a80fa0cd4b0b177736dab985cf22565_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    coroutine_heap->type_description_1 = "ccco";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( coroutine_heap->outline_0_var_key );
    coroutine_heap->outline_0_var_key = NULL;

    Py_XDECREF( coroutine_heap->outline_0_var_values );
    coroutine_heap->outline_0_var_values = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_4 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_4 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_4 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_4 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->outline_0_var_key );
    coroutine_heap->outline_0_var_key = NULL;

    Py_XDECREF( coroutine_heap->outline_0_var_values );
    coroutine_heap->outline_0_var_values = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_4;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_4;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_4;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_4;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby );
    return NULL;
    outline_exception_1:;
    coroutine_heap->exception_lineno = 72;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_2;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[1],
            coroutine->m_closure[2],
            coroutine->m_closure[0],
            coroutine_heap->var_expr
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_expr );
    Py_DECREF( coroutine_heap->var_expr );
    coroutine_heap->var_expr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_5 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_5 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_5 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_5 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_expr );
    coroutine_heap->var_expr = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_5;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_5;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_5;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_groupby,
        NULL,
        codeobj_4d8a94c87df659cfd7277e613af56ba5,
        3,
        sizeof(struct jinja2$asyncfilters$$$function_6_do_groupby$$$coroutine_1_do_groupby_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_7_do_join( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_eval_ctx = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_value = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_d = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_attribute = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_attribute;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_d;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_eval_ctx;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] = par_value;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_7_do_join );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_eval_ctx );
    Py_DECREF( par_eval_ctx );
    par_eval_ctx = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_eval_ctx );
    Py_DECREF( par_eval_ctx );
    par_eval_ctx = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_d );
    Py_DECREF( par_d );
    par_d = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_7_do_join );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
};

static PyObject *jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_0d831141192fa8a7efce569e9785bc7a, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_do_join );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "eval_ctx" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[2] );
        coroutine->m_frame->m_frame.f_lineno = 79;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_to_seq );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_to_seq );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_to_seq" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        if ( PyCell_GET( coroutine->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( coroutine->m_closure[3] );
        coroutine->m_frame->m_frame.f_lineno = 79;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_expression_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = yield_return_value;
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "d" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = PyCell_GET( coroutine->m_closure[1] );
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "attribute" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 79;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_4, tmp_args_element_name_5 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 79;
            coroutine_heap->type_description_1 = "cccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[2],
            coroutine->m_closure[3],
            coroutine->m_closure[1],
            coroutine->m_closure[0]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_join,
        NULL,
        codeobj_0d831141192fa8a7efce569e9785bc7a,
        4,
        sizeof(struct jinja2$asyncfilters$$$function_7_do_join$$$coroutine_1_do_join_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_8_do_list( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_value = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_value;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_8_do_list );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_8_do_list );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
};

static PyObject *jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_fc49021adb810318671fa076f3ff2e6c, module_jinja2$asyncfilters, sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        coroutine->m_frame->m_frame.f_lineno = 84;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_to_seq );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_to_seq );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_to_seq" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 84;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 84;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 84;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_expression_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 84;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 84;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 84;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        coroutine_heap->tmp_return_value = yield_return_value;
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 84;
            coroutine_heap->type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_list,
        NULL,
        codeobj_fc49021adb810318671fa076f3ff2e6c,
        1,
        sizeof(struct jinja2$asyncfilters$$$function_8_do_list$$$coroutine_1_do_list_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_9_do_reject( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_9_do_reject );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_9_do_reject );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
};

static PyObject *jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:


    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_23012cc63c9bc71d68d6f0504e6f7792, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "async_select_or_reject" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 89;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 89;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[0] );
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 89;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[1] );
        tmp_args_element_name_3 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject$$$function_1_lambda(  );



        tmp_args_element_name_4 = Py_False;
        coroutine->m_frame->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 89;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine->m_closure[1]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_reject,
        NULL,
        codeobj_23012cc63c9bc71d68d6f0504e6f7792,
        2,
        sizeof(struct jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2319d197081a7cadbd4501c95edde3e8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2319d197081a7cadbd4501c95edde3e8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2319d197081a7cadbd4501c95edde3e8, codeobj_2319d197081a7cadbd4501c95edde3e8, module_jinja2$asyncfilters, sizeof(void *) );
    frame_2319d197081a7cadbd4501c95edde3e8 = cache_frame_2319d197081a7cadbd4501c95edde3e8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2319d197081a7cadbd4501c95edde3e8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2319d197081a7cadbd4501c95edde3e8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_x );
        tmp_operand_name_1 = par_x;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2319d197081a7cadbd4501c95edde3e8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2319d197081a7cadbd4501c95edde3e8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2319d197081a7cadbd4501c95edde3e8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2319d197081a7cadbd4501c95edde3e8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2319d197081a7cadbd4501c95edde3e8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2319d197081a7cadbd4501c95edde3e8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2319d197081a7cadbd4501c95edde3e8,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_2319d197081a7cadbd4501c95edde3e8 == cache_frame_2319d197081a7cadbd4501c95edde3e8 )
    {
        Py_DECREF( frame_2319d197081a7cadbd4501c95edde3e8 );
    }
    cache_frame_2319d197081a7cadbd4501c95edde3e8 = NULL;

    assertFrameObject( frame_2319d197081a7cadbd4501c95edde3e8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_10_do_rejectattr( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_10_do_rejectattr );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_10_do_rejectattr );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
};

static PyObject *jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:


    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_b965da213ccdd2016ff765647bb022b5, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "async_select_or_reject" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 94;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 94;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[0] );
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 94;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[1] );
        tmp_args_element_name_3 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr$$$function_1_lambda(  );



        tmp_args_element_name_4 = Py_True;
        coroutine->m_frame->m_frame.f_lineno = 94;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 94;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine->m_closure[1]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_rejectattr,
        NULL,
        codeobj_b965da213ccdd2016ff765647bb022b5,
        2,
        sizeof(struct jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_af4ddcb98bc56d0debe98c79d1794ec8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_af4ddcb98bc56d0debe98c79d1794ec8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_af4ddcb98bc56d0debe98c79d1794ec8, codeobj_af4ddcb98bc56d0debe98c79d1794ec8, module_jinja2$asyncfilters, sizeof(void *) );
    frame_af4ddcb98bc56d0debe98c79d1794ec8 = cache_frame_af4ddcb98bc56d0debe98c79d1794ec8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_af4ddcb98bc56d0debe98c79d1794ec8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_af4ddcb98bc56d0debe98c79d1794ec8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_x );
        tmp_operand_name_1 = par_x;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af4ddcb98bc56d0debe98c79d1794ec8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_af4ddcb98bc56d0debe98c79d1794ec8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af4ddcb98bc56d0debe98c79d1794ec8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_af4ddcb98bc56d0debe98c79d1794ec8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_af4ddcb98bc56d0debe98c79d1794ec8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_af4ddcb98bc56d0debe98c79d1794ec8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_af4ddcb98bc56d0debe98c79d1794ec8,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_af4ddcb98bc56d0debe98c79d1794ec8 == cache_frame_af4ddcb98bc56d0debe98c79d1794ec8 )
    {
        Py_DECREF( frame_af4ddcb98bc56d0debe98c79d1794ec8 );
    }
    cache_frame_af4ddcb98bc56d0debe98c79d1794ec8 = NULL;

    assertFrameObject( frame_af4ddcb98bc56d0debe98c79d1794ec8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_11_do_select( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_11_do_select );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_11_do_select );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
};

static PyObject *jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:


    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_38981df49bd903f46e9a0bf6e41c9e1d, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "async_select_or_reject" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 99;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 99;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[0] );
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 99;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[1] );
        tmp_args_element_name_3 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select$$$function_1_lambda(  );



        tmp_args_element_name_4 = Py_False;
        coroutine->m_frame->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 99;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine->m_closure[1]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_select,
        NULL,
        codeobj_38981df49bd903f46e9a0bf6e41c9e1d,
        2,
        sizeof(struct jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_x );
    tmp_return_value = par_x;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select$$$function_1_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_12_do_selectattr( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_12_do_selectattr );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_12_do_selectattr );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
};

static PyObject *jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:


    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_cf88715a9b709f7dec9525d4a74a115f, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_async_select_or_reject );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "async_select_or_reject" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 104;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 104;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[0] );
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 104;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[1] );
        tmp_args_element_name_3 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr$$$function_1_lambda(  );



        tmp_args_element_name_4 = Py_True;
        coroutine->m_frame->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 104;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine->m_closure[1]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_selectattr,
        NULL,
        codeobj_cf88715a9b709f7dec9525d4a74a115f,
        2,
        sizeof(struct jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_x );
    tmp_return_value = par_x;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr$$$function_1_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_13_do_map( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_args = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_maker();

    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] = par_args;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[1] = par_kwargs;
    Py_INCREF( ((struct Nuitka_AsyncgenObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_13_do_map );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_13_do_map );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_locals {
    PyObject *var_seq;
    PyObject *var_func;
    PyObject *var_item;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    int tmp_res;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
};

static PyObject *jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_context( struct Nuitka_AsyncgenObject *asyncgen, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)asyncgen );
    assert( Nuitka_Asyncgen_Check( (PyObject *)asyncgen ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_locals *asyncgen_heap = (struct jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_locals *)asyncgen->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(asyncgen->m_yield_return_index) {
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    asyncgen_heap->var_seq = NULL;
    asyncgen_heap->var_func = NULL;
    asyncgen_heap->var_item = NULL;
    asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;
    asyncgen_heap->tmp_for_loop_1__iter_value = NULL;
    asyncgen_heap->tmp_tuple_unpack_1__element_1 = NULL;
    asyncgen_heap->tmp_tuple_unpack_1__element_2 = NULL;
    asyncgen_heap->tmp_tuple_unpack_1__source_iter = NULL;
    asyncgen_heap->type_description_1 = NULL;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    // Actual asyngen body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_a5f30cb86b153f7dad62e406091ac311, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    asyncgen->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( asyncgen->m_frame );
    assert( Py_REFCNT( asyncgen->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    asyncgen->m_frame->m_frame.f_gen = (PyObject *)asyncgen;
#endif

    Py_CLEAR( asyncgen->m_frame->m_frame.f_back );

    asyncgen->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( asyncgen->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &asyncgen->m_frame->m_frame;
    Py_INCREF( asyncgen->m_frame );

    Nuitka_Frame_MarkAsExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        asyncgen->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( asyncgen->m_frame->m_frame.f_exc_type == Py_None ) asyncgen->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_type );
    asyncgen->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_value );
    asyncgen->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_frame->m_frame.f_exc_traceback );
#else
        asyncgen->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( asyncgen->m_exc_state.exc_type == Py_None ) asyncgen->m_exc_state.exc_type = NULL;
        Py_XINCREF( asyncgen->m_exc_state.exc_type );
        asyncgen->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_value );
        asyncgen->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( asyncgen->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 109;
            asyncgen_heap->type_description_1 = "ccooo";
            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_prepare_map );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 109;
            asyncgen_heap->type_description_1 = "ccooo";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( asyncgen->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "args" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 109;
            asyncgen_heap->type_description_1 = "ccooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_1 = PyCell_GET( asyncgen->m_closure[0] );
        if ( PyCell_GET( asyncgen->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            asyncgen_heap->exception_type = PyExc_NameError;
            Py_INCREF( asyncgen_heap->exception_type );
            asyncgen_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            asyncgen_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            CHAIN_EXCEPTION( asyncgen_heap->exception_value );

            asyncgen_heap->exception_lineno = 109;
            asyncgen_heap->type_description_1 = "ccooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_2 = PyCell_GET( asyncgen->m_closure[1] );
        asyncgen->m_frame->m_frame.f_lineno = 109;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 109;
            asyncgen_heap->type_description_1 = "ccooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 109;
            asyncgen_heap->type_description_1 = "ccooo";
            goto try_except_handler_2;
        }
        assert( asyncgen_heap->tmp_tuple_unpack_1__source_iter == NULL );
        asyncgen_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = asyncgen_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                asyncgen_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = NULL;
                asyncgen_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            }


            asyncgen_heap->type_description_1 = "ccooo";
            asyncgen_heap->exception_lineno = 109;
            goto try_except_handler_3;
        }
        assert( asyncgen_heap->tmp_tuple_unpack_1__element_1 == NULL );
        asyncgen_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = asyncgen_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                asyncgen_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = NULL;
                asyncgen_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
            }


            asyncgen_heap->type_description_1 = "ccooo";
            asyncgen_heap->exception_lineno = 109;
            goto try_except_handler_3;
        }
        assert( asyncgen_heap->tmp_tuple_unpack_1__element_2 == NULL );
        asyncgen_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = asyncgen_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        asyncgen_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( asyncgen_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

                    asyncgen_heap->type_description_1 = "ccooo";
                    asyncgen_heap->exception_lineno = 109;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( asyncgen_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

            asyncgen_heap->type_description_1 = "ccooo";
            asyncgen_heap->exception_lineno = 109;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    asyncgen_heap->exception_keeper_type_1 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_1 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_1 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_1 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    asyncgen_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_1;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_1;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_1;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    asyncgen_heap->exception_keeper_type_2 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_2 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_2 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_2 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_1 );
    asyncgen_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_2 );
    asyncgen_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_2;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_2;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_2;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( asyncgen_heap->tmp_tuple_unpack_1__source_iter );
    asyncgen_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = asyncgen_heap->tmp_tuple_unpack_1__element_1;
        assert( asyncgen_heap->var_seq == NULL );
        Py_INCREF( tmp_assign_source_4 );
        asyncgen_heap->var_seq = tmp_assign_source_4;
    }
    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_1 );
    asyncgen_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( asyncgen_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = asyncgen_heap->tmp_tuple_unpack_1__element_2;
        assert( asyncgen_heap->var_func == NULL );
        Py_INCREF( tmp_assign_source_5 );
        asyncgen_heap->var_func = tmp_assign_source_5;
    }
    Py_XDECREF( asyncgen_heap->tmp_tuple_unpack_1__element_2 );
    asyncgen_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( asyncgen_heap->var_seq );
        tmp_truth_name_1 = CHECK_IF_TRUE( asyncgen_heap->var_seq );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 110;
            asyncgen_heap->type_description_1 = "ccooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_expression_name_1;
            PyObject *tmp_value_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_aiter );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_aiter );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                asyncgen_heap->exception_type = PyExc_NameError;
                Py_INCREF( asyncgen_heap->exception_type );
                asyncgen_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_aiter" );
                asyncgen_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );
                CHAIN_EXCEPTION( asyncgen_heap->exception_value );

                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( asyncgen_heap->var_seq );
            tmp_args_element_name_3 = asyncgen_heap->var_seq;
            asyncgen->m_frame->m_frame.f_lineno = 111;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_value_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_value_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto frame_exception_exit_1;
            }
            tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
            Py_DECREF( tmp_value_name_1 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 1;
            asyncgen->m_yieldfrom = tmp_expression_name_1;
            asyncgen->m_awaiting = true;
            return NULL;

            yield_return_1:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
            asyncgen->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_6 = yield_return_value;
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto frame_exception_exit_1;
            }
            assert( asyncgen_heap->tmp_for_loop_1__for_iterator == NULL );
            asyncgen_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_6;
        }
        // Tried code:
        loop_start_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_expression_name_2;
            PyObject *tmp_value_name_2;
            CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__for_iterator );
            tmp_value_name_2 = asyncgen_heap->tmp_for_loop_1__for_iterator;
            tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
            if ( tmp_expression_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto try_except_handler_5;
            }
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_2, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 2;
            asyncgen->m_yieldfrom = tmp_expression_name_2;
            asyncgen->m_awaiting = true;
            return NULL;

            yield_return_2:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_value_name_2, sizeof(PyObject *), NULL );
            asyncgen->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto try_except_handler_5;
            }
            tmp_assign_source_7 = yield_return_value;
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = asyncgen_heap->tmp_for_loop_1__iter_value;
                asyncgen_heap->tmp_for_loop_1__iter_value = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_5:;
        asyncgen_heap->exception_keeper_type_3 = asyncgen_heap->exception_type;
        asyncgen_heap->exception_keeper_value_3 = asyncgen_heap->exception_value;
        asyncgen_heap->exception_keeper_tb_3 = asyncgen_heap->exception_tb;
        asyncgen_heap->exception_keeper_lineno_3 = asyncgen_heap->exception_lineno;
        asyncgen_heap->exception_type = NULL;
        asyncgen_heap->exception_value = NULL;
        asyncgen_heap->exception_tb = NULL;
        asyncgen_heap->exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = asyncgen_heap->exception_keeper_type_3;
            tmp_compexpr_right_1 = PyExc_StopAsyncIteration;
            asyncgen_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( asyncgen_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );

                Py_DECREF( asyncgen_heap->exception_keeper_type_3 );
                Py_XDECREF( asyncgen_heap->exception_keeper_value_3 );
                Py_XDECREF( asyncgen_heap->exception_keeper_tb_3 );

                asyncgen_heap->exception_lineno = 111;
                asyncgen_heap->type_description_1 = "ccooo";
                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( asyncgen_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            Py_DECREF( asyncgen_heap->exception_keeper_type_3 );
            Py_XDECREF( asyncgen_heap->exception_keeper_value_3 );
            Py_XDECREF( asyncgen_heap->exception_keeper_tb_3 );
            goto loop_end_1;
            goto branch_end_2;
            branch_no_2:;
            // Re-raise.
            asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_3;
            asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_3;
            asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_3;
            asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_3;

            goto try_except_handler_4;
            branch_end_2:;
        }
        // End of try:
        try_end_3:;
        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( asyncgen_heap->tmp_for_loop_1__iter_value );
            tmp_assign_source_8 = asyncgen_heap->tmp_for_loop_1__iter_value;
            {
                PyObject *old = asyncgen_heap->var_item;
                asyncgen_heap->var_item = tmp_assign_source_8;
                Py_INCREF( asyncgen_heap->var_item );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_expression_name_3;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_4;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            CHECK_OBJECT( asyncgen_heap->var_func );
            tmp_called_name_3 = asyncgen_heap->var_func;
            CHECK_OBJECT( asyncgen_heap->var_item );
            tmp_args_element_name_4 = asyncgen_heap->var_item;
            asyncgen->m_frame->m_frame.f_lineno = 112;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_expression_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_expression_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 112;
                asyncgen_heap->type_description_1 = "ccooo";
                goto try_except_handler_4;
            }
            Nuitka_PreserveHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_called_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
            asyncgen->m_yield_return_index = 3;
            return tmp_expression_name_3;
            yield_return_3:
            Nuitka_RestoreHeap( asyncgen_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), &tmp_called_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


                asyncgen_heap->exception_lineno = 112;
                asyncgen_heap->type_description_1 = "ccooo";
                goto try_except_handler_4;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &asyncgen_heap->exception_type, &asyncgen_heap->exception_value, &asyncgen_heap->exception_tb );


            asyncgen_heap->exception_lineno = 111;
            asyncgen_heap->type_description_1 = "ccooo";
            goto try_except_handler_4;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_4:;
        asyncgen_heap->exception_keeper_type_4 = asyncgen_heap->exception_type;
        asyncgen_heap->exception_keeper_value_4 = asyncgen_heap->exception_value;
        asyncgen_heap->exception_keeper_tb_4 = asyncgen_heap->exception_tb;
        asyncgen_heap->exception_keeper_lineno_4 = asyncgen_heap->exception_lineno;
        asyncgen_heap->exception_type = NULL;
        asyncgen_heap->exception_value = NULL;
        asyncgen_heap->exception_tb = NULL;
        asyncgen_heap->exception_lineno = 0;

        Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
        asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
        asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_4;
        asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_4;
        asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_4;
        asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        Py_XDECREF( asyncgen_heap->tmp_for_loop_1__iter_value );
        asyncgen_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)asyncgen_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( asyncgen_heap->tmp_for_loop_1__for_iterator );
        asyncgen_heap->tmp_for_loop_1__for_iterator = NULL;

        branch_no_1:;
    }

    Nuitka_Frame_MarkAsNotExecuting( asyncgen->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( asyncgen->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( asyncgen_heap->exception_type ) )
    {
        if ( asyncgen_heap->exception_tb == NULL )
        {
            asyncgen_heap->exception_tb = MAKE_TRACEBACK( asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }
        else if ( asyncgen_heap->exception_tb->tb_frame != &asyncgen->m_frame->m_frame )
        {
            asyncgen_heap->exception_tb = ADD_TRACEBACK( asyncgen_heap->exception_tb, asyncgen->m_frame, asyncgen_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)asyncgen->m_frame,
            asyncgen_heap->type_description_1,
            asyncgen->m_closure[0],
            asyncgen->m_closure[1],
            asyncgen_heap->var_seq,
            asyncgen_heap->var_func,
            asyncgen_heap->var_item
        );


        // Release cached frame.
        if ( asyncgen->m_frame == cache_m_frame )
        {
            Py_DECREF( asyncgen->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( asyncgen->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( asyncgen->m_exc_state.exc_type );
    Py_CLEAR( asyncgen->m_exc_state.exc_value );
    Py_CLEAR( asyncgen->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_type );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_value );
    Py_CLEAR( asyncgen->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( asyncgen->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)asyncgen_heap->var_seq );
    Py_DECREF( asyncgen_heap->var_seq );
    asyncgen_heap->var_seq = NULL;

    CHECK_OBJECT( (PyObject *)asyncgen_heap->var_func );
    Py_DECREF( asyncgen_heap->var_func );
    asyncgen_heap->var_func = NULL;

    Py_XDECREF( asyncgen_heap->var_item );
    asyncgen_heap->var_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    asyncgen_heap->exception_keeper_type_5 = asyncgen_heap->exception_type;
    asyncgen_heap->exception_keeper_value_5 = asyncgen_heap->exception_value;
    asyncgen_heap->exception_keeper_tb_5 = asyncgen_heap->exception_tb;
    asyncgen_heap->exception_keeper_lineno_5 = asyncgen_heap->exception_lineno;
    asyncgen_heap->exception_type = NULL;
    asyncgen_heap->exception_value = NULL;
    asyncgen_heap->exception_tb = NULL;
    asyncgen_heap->exception_lineno = 0;

    Py_XDECREF( asyncgen_heap->var_seq );
    asyncgen_heap->var_seq = NULL;

    Py_XDECREF( asyncgen_heap->var_func );
    asyncgen_heap->var_func = NULL;

    Py_XDECREF( asyncgen_heap->var_item );
    asyncgen_heap->var_item = NULL;

    // Re-raise.
    asyncgen_heap->exception_type = asyncgen_heap->exception_keeper_type_5;
    asyncgen_heap->exception_value = asyncgen_heap->exception_keeper_value_5;
    asyncgen_heap->exception_tb = asyncgen_heap->exception_keeper_tb_5;
    asyncgen_heap->exception_lineno = asyncgen_heap->exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map );

    function_exception_exit:
    assert( asyncgen_heap->exception_type );
    RESTORE_ERROR_OCCURRED( asyncgen_heap->exception_type, asyncgen_heap->exception_value, asyncgen_heap->exception_tb );
    return NULL;
    function_return_exit:;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_maker( void )
{
    return Nuitka_Asyncgen_New(
        jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_map,
        NULL,
        codeobj_a5f30cb86b153f7dad62e406091ac311,
        2,
        sizeof(struct jinja2$asyncfilters$$$function_13_do_map$$$asyncgen_1_do_map_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_14_do_sum( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_environment = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_iterable = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_attribute = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_start = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_attribute;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_environment;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_iterable;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] = par_start;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_14_do_sum );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_iterable );
    Py_DECREF( par_iterable );
    par_iterable = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_iterable );
    Py_DECREF( par_iterable );
    par_iterable = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_14_do_sum );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_locals {
    PyObject *var_rv;
    PyObject *var_func;
    PyObject *var_item;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_rv = NULL;
    coroutine_heap->var_func = NULL;
    coroutine_heap->var_item = NULL;
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_c40038b6156fbef9efcde8da2a92444c, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        if ( PyCell_GET( coroutine->m_closure[3] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "start" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 117;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_1 = PyCell_GET( coroutine->m_closure[3] );
        assert( coroutine_heap->var_rv == NULL );
        Py_INCREF( tmp_assign_source_1 );
        coroutine_heap->var_rv = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "attribute" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 118;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( coroutine->m_closure[0] );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 119;
                coroutine_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_make_attrgetter );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 119;
                coroutine_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }
            if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "environment" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 119;
                coroutine_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[1] );
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "attribute" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 119;
                coroutine_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[0] );
            coroutine->m_frame->m_frame.f_lineno = 119;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 119;
                coroutine_heap->type_description_1 = "ccccooo";
                goto frame_exception_exit_1;
            }
            assert( coroutine_heap->var_func == NULL );
            coroutine_heap->var_func = tmp_assign_source_2;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum$$$function_1_lambda(  );



            assert( coroutine_heap->var_func == NULL );
            coroutine_heap->var_func = tmp_assign_source_3;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_value_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_aiter );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_aiter );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_aiter" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "iterable" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( coroutine->m_closure[2] );
        coroutine->m_frame->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_value_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_MAKE_ITERATOR( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_value_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = yield_return_value;
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->tmp_for_loop_1__for_iterator == NULL );
        coroutine_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_value_name_2;
        CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__for_iterator );
        tmp_value_name_2 = coroutine_heap->tmp_for_loop_1__for_iterator;
        tmp_expression_name_2 = ASYNC_ITERATOR_NEXT( tmp_value_name_2 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 2;
        coroutine->m_yieldfrom = tmp_expression_name_2;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_2:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_value_name_2, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_5 = yield_return_value;
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = coroutine_heap->tmp_for_loop_1__iter_value;
            coroutine_heap->tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = coroutine_heap->exception_keeper_type_1;
        tmp_compexpr_right_2 = PyExc_StopAsyncIteration;
        coroutine_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( coroutine_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

            Py_DECREF( coroutine_heap->exception_keeper_type_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
            Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );

            coroutine_heap->exception_lineno = 122;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        Py_DECREF( coroutine_heap->exception_keeper_type_1 );
        Py_XDECREF( coroutine_heap->exception_keeper_value_1 );
        Py_XDECREF( coroutine_heap->exception_keeper_tb_1 );
        goto loop_end_1;
        goto branch_end_2;
        branch_no_2:;
        // Re-raise.
        coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
        coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
        coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
        coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

        goto try_except_handler_2;
        branch_end_2:;
    }
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( coroutine_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_6 = coroutine_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = coroutine_heap->var_item;
            coroutine_heap->var_item = tmp_assign_source_6;
            Py_INCREF( coroutine_heap->var_item );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( coroutine_heap->var_rv );
        tmp_left_name_1 = coroutine_heap->var_rv;
        CHECK_OBJECT( coroutine_heap->var_func );
        tmp_called_name_3 = coroutine_heap->var_func;
        CHECK_OBJECT( coroutine_heap->var_item );
        tmp_args_element_name_4 = coroutine_heap->var_item;
        coroutine->m_frame->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 123;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        coroutine_heap->tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( coroutine_heap->tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 123;
            coroutine_heap->type_description_1 = "ccccooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_7 = tmp_left_name_1;
        coroutine_heap->var_rv = tmp_assign_source_7;

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


        coroutine_heap->exception_lineno = 122;
        coroutine_heap->type_description_1 = "ccccooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[1],
            coroutine->m_closure[2],
            coroutine->m_closure[0],
            coroutine->m_closure[3],
            coroutine_heap->var_rv,
            coroutine_heap->var_func,
            coroutine_heap->var_item
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( coroutine_heap->tmp_for_loop_1__iter_value );
    coroutine_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( coroutine_heap->tmp_for_loop_1__for_iterator );
    coroutine_heap->tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( coroutine_heap->var_rv );
    coroutine_heap->tmp_return_value = coroutine_heap->var_rv;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_rv );
    Py_DECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_func );
    Py_DECREF( coroutine_heap->var_func );
    coroutine_heap->var_func = NULL;

    Py_XDECREF( coroutine_heap->var_item );
    coroutine_heap->var_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_rv );
    coroutine_heap->var_rv = NULL;

    Py_XDECREF( coroutine_heap->var_func );
    coroutine_heap->var_func = NULL;

    Py_XDECREF( coroutine_heap->var_item );
    coroutine_heap->var_item = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_3;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_3;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_3;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_sum,
        NULL,
        codeobj_c40038b6156fbef9efcde8da2a92444c,
        4,
        sizeof(struct jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum_locals)
    );
}


static PyObject *impl_jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_x );
    tmp_return_value = par_x;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum$$$function_1_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jinja2$asyncfilters$$$function_15_do_slice( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_value = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_slices = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_fill_with = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_fill_with;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_slices;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_value;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_15_do_slice );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_slices );
    Py_DECREF( par_slices );
    par_slices = NULL;

    CHECK_OBJECT( (PyObject *)par_fill_with );
    Py_DECREF( par_fill_with );
    par_fill_with = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_slices );
    Py_DECREF( par_slices );
    par_slices = NULL;

    CHECK_OBJECT( (PyObject *)par_fill_with );
    Py_DECREF( par_fill_with );
    par_fill_with = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_15_do_slice );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_locals {
    char const *type_description_1;
    PyObject *tmp_return_value;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
};

static PyObject *jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_locals *coroutine_heap = (struct jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->tmp_return_value = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_679b68139ff05bbcf536dcf2b682828d, module_jinja2$asyncfilters, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_do_slice );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        coroutine->m_frame->m_frame.f_lineno = 129;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_to_seq );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_auto_to_seq );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "auto_to_seq" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "value" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( coroutine->m_closure[2] );
        coroutine->m_frame->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_expression_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = yield_return_value;
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "slices" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( coroutine->m_closure[1] );
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fill_with" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = PyCell_GET( coroutine->m_closure[0] );
        coroutine->m_frame->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_3, tmp_args_element_name_4 };
            coroutine_heap->tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 129;
            coroutine_heap->type_description_1 = "ccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[2],
            coroutine->m_closure[1],
            coroutine->m_closure[0]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_maker( void )
{
    return Nuitka_Coroutine_New(
        jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_context,
        module_jinja2$asyncfilters,
        const_str_plain_do_slice,
        NULL,
        codeobj_679b68139ff05bbcf536dcf2b682828d,
        3,
        sizeof(struct jinja2$asyncfilters$$$function_15_do_slice$$$coroutine_1_do_slice_locals)
    );
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_10_do_rejectattr(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_10_do_rejectattr,
        const_str_plain_do_rejectattr,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b965da213ccdd2016ff765647bb022b5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_10_do_rejectattr$$$coroutine_1_do_rejectattr$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_cc84abbcaaba95671b2d7356ba8cc675,
#endif
        codeobj_af4ddcb98bc56d0debe98c79d1794ec8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_11_do_select(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_11_do_select,
        const_str_plain_do_select,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_38981df49bd903f46e9a0bf6e41c9e1d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_11_do_select$$$coroutine_1_do_select$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_7ae79598d0477268ed3786ba124fb740,
#endif
        codeobj_20c43f26e71a6e024337f6b06bbf6fb5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_12_do_selectattr(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_12_do_selectattr,
        const_str_plain_do_selectattr,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cf88715a9b709f7dec9525d4a74a115f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_12_do_selectattr$$$coroutine_1_do_selectattr$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_8bf2f6f70c5293998a720f2bf96ac69e,
#endif
        codeobj_c06bfb27b3331172ff1849bab432d7da,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_13_do_map(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_13_do_map,
        const_str_plain_do_map,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a5f30cb86b153f7dad62e406091ac311,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_14_do_sum( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_14_do_sum,
        const_str_plain_do_sum,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c40038b6156fbef9efcde8da2a92444c,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_14_do_sum$$$coroutine_1_do_sum$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_60ea0382f7ce60012086bc4ceb8337d8,
#endif
        codeobj_62dbde7746b177fd8206848f593ed530,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_15_do_slice( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_15_do_slice,
        const_str_plain_do_slice,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_679b68139ff05bbcf536dcf2b682828d,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_1_auto_to_seq(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_1_auto_to_seq,
        const_str_plain_auto_to_seq,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1c53d575fcbe01c3ae447f6a9b40ae01,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_2_async_select_or_reject(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_2_async_select_or_reject,
        const_str_plain_async_select_or_reject,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d0d572e6a1be4063384dd873b2005de9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_3_dualfilter,
        const_str_plain_dualfilter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ebdd32db41cd24fd1f38bd9f050e42de,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_3_dualfilter$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_7471b673d6fd023f57af67fd216eb43f,
#endif
        codeobj_f23b53a98b30354af9fd158782cd46e9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_3_dualfilter$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_7471b673d6fd023f57af67fd216eb43f,
#endif
        codeobj_f35e7c1722912117ea0f4a28f2e33f3a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter$$$function_3_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_3_dualfilter$$$function_3_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_abec06806e1eccd913d2cfa6a475442c,
#endif
        codeobj_5fc135b126a3190ee290dbf20de8f8ca,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        4
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_4_asyncfiltervariant(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_4_asyncfiltervariant,
        const_str_plain_asyncfiltervariant,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_01d5a0773486b7a69b0e25409f159e1b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_4_asyncfiltervariant$$$function_1_decorator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_4_asyncfiltervariant$$$function_1_decorator,
        const_str_plain_decorator,
#if PYTHON_VERSION >= 300
        const_str_digest_c253dd25652dae487d0fca8cd2eccf85,
#endif
        codeobj_94c4672f873eaf481092de964a189029,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_5_do_first(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_5_do_first,
        const_str_plain_do_first,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_95bdcaa04bbdebf08501f0a839b84489,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_6_do_groupby(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_6_do_groupby,
        const_str_plain_do_groupby,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4d8a94c87df659cfd7277e613af56ba5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_7_do_join( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_7_do_join,
        const_str_plain_do_join,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0d831141192fa8a7efce569e9785bc7a,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_8_do_list(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_8_do_list,
        const_str_plain_do_list,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_fc49021adb810318671fa076f3ff2e6c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_9_do_reject(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_9_do_reject,
        const_str_plain_do_reject,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_23012cc63c9bc71d68d6f0504e6f7792,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jinja2$asyncfilters$$$function_9_do_reject$$$coroutine_1_do_reject$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_bd661d31362901e16224d7891f93b39b,
#endif
        codeobj_2319d197081a7cadbd4501c95edde3e8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jinja2$asyncfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jinja2$asyncfilters =
{
    PyModuleDef_HEAD_INIT,
    "jinja2.asyncfilters",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jinja2$asyncfilters)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jinja2$asyncfilters)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jinja2$asyncfilters );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jinja2.asyncfilters: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2.asyncfilters: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jinja2.asyncfilters: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjinja2$asyncfilters" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jinja2$asyncfilters = Py_InitModule4(
        "jinja2.asyncfilters",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jinja2$asyncfilters = PyModule_Create( &mdef_jinja2$asyncfilters );
#endif

    moduledict_jinja2$asyncfilters = MODULE_DICT( module_jinja2$asyncfilters );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jinja2$asyncfilters,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jinja2$asyncfilters,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$asyncfilters,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jinja2$asyncfilters,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jinja2$asyncfilters );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_e9c0aba9ba51d097a6babda6163af2fe, module_jinja2$asyncfilters );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_a38a9cd0db8f5b0f9297fa38ba849984;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_a38a9cd0db8f5b0f9297fa38ba849984 = MAKE_MODULE_FRAME( codeobj_a38a9cd0db8f5b0f9297fa38ba849984, module_jinja2$asyncfilters );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_a38a9cd0db8f5b0f9297fa38ba849984 );
    assert( Py_REFCNT( frame_a38a9cd0db8f5b0f9297fa38ba849984 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_functools;
        tmp_globals_name_1 = (PyObject *)moduledict_jinja2$asyncfilters;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_wraps_tuple;
        tmp_level_name_1 = const_int_0;
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 1;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_wraps );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_wraps, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_37aca85c698d791dc4bb639deca6348b;
        tmp_globals_name_2 = (PyObject *)moduledict_jinja2$asyncfilters;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_auto_aiter_tuple;
        tmp_level_name_2 = const_int_0;
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 3;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_auto_aiter );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_aiter, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_jinja2;
        tmp_globals_name_3 = (PyObject *)moduledict_jinja2$asyncfilters;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_filters_tuple;
        tmp_level_name_3 = const_int_0;
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 4;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_filters );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_1_auto_to_seq(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_auto_to_seq, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_2_async_select_or_reject(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_async_select_or_reject, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_3_dualfilter(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_dualfilter, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_4_asyncfiltervariant(  );



        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_2 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_source_name_1 = tmp_mvar_value_4;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_do_first );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 61;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_5_do_first(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 61;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_first, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_6;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_do_groupby );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_called_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_6_do_groupby(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_groupby, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_name_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_defaults_1;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_7;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_8;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_do_join );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_called_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        tmp_defaults_1 = const_tuple_str_empty_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_args_element_name_6 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_7_do_join( tmp_defaults_1 );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_join, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_called_name_7;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_9;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_10;
        tmp_args_element_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_do_list );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 82;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_called_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_8 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_8_do_list(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 82;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_assign_source_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_list, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_called_name_9;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;

            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_11;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_12;
        tmp_args_element_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_do_reject );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 87;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_called_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_10 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_9_do_reject(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 87;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_assign_source_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_reject, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_called_name_11;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_13;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_14;
        tmp_args_element_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_do_rejectattr );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_called_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_12 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_10_do_rejectattr(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_rejectattr, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_name_13;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_args_element_name_14;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_15;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_16;
        tmp_args_element_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_do_select );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_called_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_called_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_14 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_11_do_select(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_called_name_13 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_select, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_called_name_15;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_args_element_name_16;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_17;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_18;
        tmp_args_element_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_do_selectattr );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_called_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_called_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_16 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_12_do_selectattr(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_called_name_15 );
        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_selectattr, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_called_name_17;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_18;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_19;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_20;
        tmp_args_element_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_do_map );
        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 107;
        {
            PyObject *call_args[] = { tmp_args_element_name_17 };
            tmp_called_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_called_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_18 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_13_do_map(  );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 107;
        {
            PyObject *call_args[] = { tmp_args_element_name_18 };
            tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_called_name_17 );
        Py_DECREF( tmp_args_element_name_18 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_map, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_called_name_19;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_defaults_2;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_21;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_22;
        tmp_args_element_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_do_sum );
        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_called_name_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_called_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        tmp_defaults_2 = const_tuple_none_int_0_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_args_element_name_20 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_14_do_sum( tmp_defaults_2 );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_20 };
            tmp_assign_source_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_element_name_20 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_sum, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_21;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_defaults_3;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asyncfiltervariant );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asyncfiltervariant" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_23;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_filters );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_filters );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "filters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_24;
        tmp_args_element_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_do_slice );
        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;

            goto frame_exception_exit_1;
        }
        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_21 };
            tmp_called_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_called_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;

            goto frame_exception_exit_1;
        }
        tmp_defaults_3 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_args_element_name_22 = MAKE_FUNCTION_jinja2$asyncfilters$$$function_15_do_slice( tmp_defaults_3 );



        frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_22 };
            tmp_assign_source_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
        }

        Py_DECREF( tmp_called_name_21 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_slice, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_mvar_value_35;
        tmp_dict_key_1 = const_str_plain_first;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_first );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_first );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_first" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_1 = tmp_mvar_value_25;
        tmp_assign_source_22 = _PyDict_NewPresized( 11 );
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_groupby;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_groupby );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_groupby );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_groupby" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_2 = tmp_mvar_value_26;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_join;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_join );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_join );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_join" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_3 = tmp_mvar_value_27;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_list;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_list );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_list );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_list" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_4 = tmp_mvar_value_28;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_reject;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_reject );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_reject );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_reject" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_5 = tmp_mvar_value_29;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_rejectattr;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_rejectattr );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_rejectattr );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_rejectattr" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_6 = tmp_mvar_value_30;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_map;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_map );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_map );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_map" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_7 = tmp_mvar_value_31;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_select;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_select );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_select );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_select" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_8 = tmp_mvar_value_32;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_selectattr;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_selectattr );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_selectattr );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_selectattr" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_9 = tmp_mvar_value_33;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_sum;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_sum );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_sum );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_assign_source_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "do_sum" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_10 = tmp_mvar_value_34;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_plain_slice;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_do_slice );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_do_slice );
        }

        CHECK_OBJECT( tmp_mvar_value_35 );
        tmp_dict_value_11 = tmp_mvar_value_35;
        tmp_res = PyDict_SetItem( tmp_assign_source_22, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_jinja2$asyncfilters, (Nuitka_StringObject *)const_str_plain_ASYNC_FILTERS, tmp_assign_source_22 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a38a9cd0db8f5b0f9297fa38ba849984 );
#endif
    popFrameStack();

    assertFrameObject( frame_a38a9cd0db8f5b0f9297fa38ba849984 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a38a9cd0db8f5b0f9297fa38ba849984 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a38a9cd0db8f5b0f9297fa38ba849984, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a38a9cd0db8f5b0f9297fa38ba849984->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a38a9cd0db8f5b0f9297fa38ba849984, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_jinja2$asyncfilters );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
