/* Generated code for Python module 'grammars.date'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_grammars$date" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_grammars$date;
PyDictObject *moduledict_grammars$date;

/* The declarations of module constants used, if any. */
extern PyObject *const_int_pos_12;
static PyObject *const_str_plain_YEAR_SHORT;
extern PyObject *const_str_plain_YEAR;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_DAY;
extern PyObject *const_str_plain_length_eq;
extern PyObject *const_tuple_int_pos_2100_tuple;
extern PyObject *const_tuple_str_plain_fact_str_plain_attribute_tuple;
static PyObject *const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_month;
extern PyObject *const_tuple_int_pos_31_tuple;
extern PyObject *const_str_digest_b869d9b1f0243a03bd697608af4d0e39;
extern PyObject *const_tuple_str_plain___tuple;
extern PyObject *const_str_chr_1075;
extern PyObject *const_int_pos_31;
extern PyObject *const_tuple_str_plain_rule_str_plain_and__str_plain_or__tuple;
static PyObject *const_tuple_str_plain_current_era_true_tuple;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___debug__;
static PyObject *const_tuple_str_digest_8f141c194bc565da997cf47a8956e393_tuple;
extern PyObject *const_int_pos_99;
extern PyObject *const_str_digest_138ff90aa646afd822e94590024c804a;
extern PyObject *const_tuple_int_pos_1000_tuple;
static PyObject *const_tuple_int_pos_99_tuple;
static PyObject *const_str_digest_8f141c194bc565da997cf47a8956e393;
static PyObject *const_tuple_str_digest_295632cff31d071fab8d66f837bda99e_tuple;
extern PyObject *const_str_plain_DATE;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
extern PyObject *const_tuple_str_digest_138ff90aa646afd822e94590024c804a_tuple;
extern PyObject *const_str_plain_MONTHS;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_str_plain_or_;
static PyObject *const_str_plain_current_era;
extern PyObject *const_str_plain_const;
extern PyObject *const_tuple_empty;
extern PyObject *const_tuple_str_digest_947f7a6ac444bb1ddd86a5723bc9579c_tuple;
extern PyObject *const_str_plain_attribute;
extern PyObject *const_str_plain_interpretation;
extern PyObject *const_tuple_int_pos_100000_tuple;
extern PyObject *const_str_digest_ea78dcde38a5066c777d6274d25657b3;
static PyObject *const_str_plain_ERA_YEAR;
extern PyObject *const_int_pos_100000;
extern PyObject *const_int_pos_2100;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_eq;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_yargy;
static PyObject *const_str_plain_MONTH;
extern PyObject *const_int_0;
extern PyObject *const_int_pos_1900;
static PyObject *const_str_digest_295632cff31d071fab8d66f837bda99e;
extern PyObject *const_int_pos_1000;
extern PyObject *const_tuple_int_pos_2_tuple;
extern PyObject *const_str_plain_origin;
extern PyObject *const_tuple_type_int_tuple;
static PyObject *const_str_chr_1101;
extern PyObject *const_str_chr_1085;
extern PyObject *const_str_plain_year;
extern PyObject *const_str_angle_lambda;
static PyObject *const_str_digest_4ccd3f27d78e349b6b1fee28e188f1cf;
static PyObject *const_str_plain_ERA_WORD;
extern PyObject *const_str_plain_fact;
extern PyObject *const_str_plain_gte;
extern PyObject *const_str_plain_Date;
extern PyObject *const_str_plain_normalized;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain__;
extern PyObject *const_str_plain_rule;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_optional;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_day;
extern PyObject *const_str_plain_dictionary;
extern PyObject *const_tuple_int_pos_12_tuple;
static PyObject *const_str_plain_YEAR_WORD;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_lte;
extern PyObject *const_str_plain_and_;
extern PyObject *const_str_plain_custom;
extern PyObject *const_str_digest_947f7a6ac444bb1ddd86a5723bc9579c;
static PyObject *const_dict_33e6f9bba9be98fe2a8e54e7efa221bf;
static PyObject *const_str_plain_MONTH_NAME;
static PyObject *const_str_digest_61ccfb2a7e0befa42852471cb3888af8;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_YEAR_SHORT = UNSTREAM_STRING_ASCII( &constant_bin[ 678136 ], 10, 1 );
    const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple, 0, const_str_plain_eq ); Py_INCREF( const_str_plain_eq );
    PyTuple_SET_ITEM( const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple, 1, const_str_plain_gte ); Py_INCREF( const_str_plain_gte );
    PyTuple_SET_ITEM( const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple, 2, const_str_plain_lte ); Py_INCREF( const_str_plain_lte );
    PyTuple_SET_ITEM( const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple, 3, const_str_plain_length_eq ); Py_INCREF( const_str_plain_length_eq );
    PyTuple_SET_ITEM( const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple, 4, const_str_plain_dictionary ); Py_INCREF( const_str_plain_dictionary );
    PyTuple_SET_ITEM( const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple, 5, const_str_plain_normalized ); Py_INCREF( const_str_plain_normalized );
    const_tuple_str_plain_current_era_true_tuple = PyTuple_New( 2 );
    const_str_plain_current_era = UNSTREAM_STRING_ASCII( &constant_bin[ 678146 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_current_era_true_tuple, 0, const_str_plain_current_era ); Py_INCREF( const_str_plain_current_era );
    PyTuple_SET_ITEM( const_tuple_str_plain_current_era_true_tuple, 1, Py_True ); Py_INCREF( Py_True );
    const_tuple_str_digest_8f141c194bc565da997cf47a8956e393_tuple = PyTuple_New( 1 );
    const_str_digest_8f141c194bc565da997cf47a8956e393 = UNSTREAM_STRING( &constant_bin[ 678157 ], 6, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_8f141c194bc565da997cf47a8956e393_tuple, 0, const_str_digest_8f141c194bc565da997cf47a8956e393 ); Py_INCREF( const_str_digest_8f141c194bc565da997cf47a8956e393 );
    const_tuple_int_pos_99_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_99_tuple, 0, const_int_pos_99 ); Py_INCREF( const_int_pos_99 );
    const_tuple_str_digest_295632cff31d071fab8d66f837bda99e_tuple = PyTuple_New( 1 );
    const_str_digest_295632cff31d071fab8d66f837bda99e = UNSTREAM_STRING( &constant_bin[ 678163 ], 8, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_295632cff31d071fab8d66f837bda99e_tuple, 0, const_str_digest_295632cff31d071fab8d66f837bda99e ); Py_INCREF( const_str_digest_295632cff31d071fab8d66f837bda99e );
    const_str_plain_ERA_YEAR = UNSTREAM_STRING_ASCII( &constant_bin[ 678171 ], 8, 1 );
    const_str_plain_MONTH = UNSTREAM_STRING_ASCII( &constant_bin[ 602325 ], 5, 1 );
    const_str_chr_1101 = UNSTREAM_STRING( &constant_bin[ 668233 ], 2, 0 );
    const_str_digest_4ccd3f27d78e349b6b1fee28e188f1cf = UNSTREAM_STRING_ASCII( &constant_bin[ 678179 ], 16, 0 );
    const_str_plain_ERA_WORD = UNSTREAM_STRING_ASCII( &constant_bin[ 678195 ], 8, 1 );
    const_str_plain_YEAR_WORD = UNSTREAM_STRING_ASCII( &constant_bin[ 677919 ], 9, 1 );
    const_dict_33e6f9bba9be98fe2a8e54e7efa221bf = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 678203 ], 258 );
    const_str_plain_MONTH_NAME = UNSTREAM_STRING_ASCII( &constant_bin[ 678461 ], 10, 1 );
    const_str_digest_61ccfb2a7e0befa42852471cb3888af8 = UNSTREAM_STRING_ASCII( &constant_bin[ 678471 ], 22, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_grammars$date( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_d0e6875cb47313e74fc079de2f2e86d8;
static PyCodeObject *codeobj_993954414d8f34f0527f3be818b8ad36;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_4ccd3f27d78e349b6b1fee28e188f1cf );
    codeobj_d0e6875cb47313e74fc079de2f2e86d8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 72, const_tuple_str_plain___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_993954414d8f34f0527f3be818b8ad36 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_61ccfb2a7e0befa42852471cb3888af8, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_grammars$date$$$function_1_lambda(  );


// The module function definitions.
static PyObject *impl_grammars$date$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par__ = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d0e6875cb47313e74fc079de2f2e86d8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d0e6875cb47313e74fc079de2f2e86d8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d0e6875cb47313e74fc079de2f2e86d8, codeobj_d0e6875cb47313e74fc079de2f2e86d8, module_grammars$date, sizeof(void *) );
    frame_d0e6875cb47313e74fc079de2f2e86d8 = cache_frame_d0e6875cb47313e74fc079de2f2e86d8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d0e6875cb47313e74fc079de2f2e86d8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d0e6875cb47313e74fc079de2f2e86d8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_int_arg_1;
        tmp_left_name_1 = const_int_pos_1900;
        CHECK_OBJECT( par__ );
        tmp_int_arg_1 = par__;
        tmp_right_name_1 = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0e6875cb47313e74fc079de2f2e86d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0e6875cb47313e74fc079de2f2e86d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0e6875cb47313e74fc079de2f2e86d8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d0e6875cb47313e74fc079de2f2e86d8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d0e6875cb47313e74fc079de2f2e86d8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d0e6875cb47313e74fc079de2f2e86d8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d0e6875cb47313e74fc079de2f2e86d8,
        type_description_1,
        par__
    );


    // Release cached frame.
    if ( frame_d0e6875cb47313e74fc079de2f2e86d8 == cache_frame_d0e6875cb47313e74fc079de2f2e86d8 )
    {
        Py_DECREF( frame_d0e6875cb47313e74fc079de2f2e86d8 );
    }
    cache_frame_d0e6875cb47313e74fc079de2f2e86d8 = NULL;

    assertFrameObject( frame_d0e6875cb47313e74fc079de2f2e86d8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$date$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$date$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_grammars$date$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$date$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d0e6875cb47313e74fc079de2f2e86d8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$date,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_grammars$date =
{
    PyModuleDef_HEAD_INIT,
    "grammars.date",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(grammars$date)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(grammars$date)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_grammars$date );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("grammars.date: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.date: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.date: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initgrammars$date" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_grammars$date = Py_InitModule4(
        "grammars.date",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_grammars$date = PyModule_Create( &mdef_grammars$date );
#endif

    moduledict_grammars$date = MODULE_DICT( module_grammars$date );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_grammars$date,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_grammars$date,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$date,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$date,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_grammars$date );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_b869d9b1f0243a03bd697608af4d0e39, module_grammars$date );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_993954414d8f34f0527f3be818b8ad36;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_993954414d8f34f0527f3be818b8ad36 = MAKE_MODULE_FRAME( codeobj_993954414d8f34f0527f3be818b8ad36, module_grammars$date );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_993954414d8f34f0527f3be818b8ad36 );
    assert( Py_REFCNT( frame_993954414d8f34f0527f3be818b8ad36 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_yargy;
        tmp_globals_name_1 = (PyObject *)moduledict_grammars$date;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_rule_str_plain_and__str_plain_or__tuple;
        tmp_level_name_1 = const_int_0;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 4;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_rule );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_and_ );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_and_, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_or_ );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_or_, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
        tmp_globals_name_2 = (PyObject *)moduledict_grammars$date;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_fact_str_plain_attribute_tuple;
        tmp_level_name_2 = const_int_0;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 8;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_9;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_fact );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_fact, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_attribute );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_attribute, tmp_assign_source_11 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_ea78dcde38a5066c777d6274d25657b3;
        tmp_globals_name_3 = (PyObject *)moduledict_grammars$date;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_d7e0230fba91b09d4e673b82abf4b844_tuple;
        tmp_level_name_3 = const_int_0;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 9;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_12;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_7 = tmp_import_from_3__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_eq );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_eq, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_8 = tmp_import_from_3__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_gte );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_gte, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_9 = tmp_import_from_3__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_lte );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_lte, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_10 = tmp_import_from_3__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_length_eq );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_length_eq, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_11 = tmp_import_from_3__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_dictionary );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_dictionary, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_12 = tmp_import_from_3__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_normalized );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_normalized, tmp_assign_source_18 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_fact );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fact );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fact" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 15;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_args_element_name_1 = const_str_plain_Date;
        tmp_list_element_1 = const_str_plain_year;
        tmp_args_element_name_2 = PyList_New( 4 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_2, 0, tmp_list_element_1 );
        tmp_list_element_1 = const_str_plain_month;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_2, 1, tmp_list_element_1 );
        tmp_list_element_1 = const_str_plain_day;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_2, 2, tmp_list_element_1 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_attribute );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_attribute );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_args_element_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "attribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 17;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 17;
        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_plain_current_era_true_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_2, 3, tmp_list_element_1 );
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 15;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = PyDict_Copy( const_dict_33e6f9bba9be98fe2a8e54e7efa221bf );
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTHS, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 37;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTHS );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONTHS );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_args_element_name_3 = tmp_mvar_value_6;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 37;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_source_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_7;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_month );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 38;
        tmp_source_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_normalized );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_custom );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTHS );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONTHS );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONTHS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_8;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___getitem__ );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 38;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 37;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTH_NAME, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_5;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_9;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_gte );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gte );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_10;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 42;
        tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_lte );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_lte );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_args_element_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "lte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_11;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 43;
        tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_int_pos_12_tuple, 0 ) );

        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_6 );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_source_name_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_12;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_month );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 45;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 45;
        tmp_args_element_name_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_custom, &PyTuple_GET_ITEM( const_tuple_type_int_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 45;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTH, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_name_10;
        PyObject *tmp_source_name_7;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_16;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_13;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_gte );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gte );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 49;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_14;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 49;
        tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_lte );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_lte );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_args_element_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "lte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_15;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 50;
        tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_int_pos_31_tuple, 0 ) );

        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 50;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_source_name_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_args_element_name_9 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto frame_exception_exit_1;
        }
        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_called_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_16;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_day );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 52;
        tmp_args_element_name_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_custom, &PyTuple_GET_ITEM( const_tuple_type_int_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_assign_source_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_DAY, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_21;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_17;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_18;
        tmp_args_element_name_13 = const_str_chr_1075;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_19;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 56;
        tmp_called_instance_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 56;
        tmp_args_element_name_14 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 56;
        {
            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_20 == NULL )
        {
            Py_DECREF( tmp_args_element_name_12 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_20;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_args_element_name_12 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_21;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 57;
        tmp_args_element_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_digest_138ff90aa646afd822e94590024c804a_tuple, 0 ) );

        if ( tmp_args_element_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_12 );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_12 );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_15 };
            tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_WORD, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_name_19;
        PyObject *tmp_source_name_9;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_25;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_22;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_gte );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gte );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;

            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_23;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 61;
        tmp_args_element_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_int_pos_1000_tuple, 0 ) );

        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_lte );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_lte );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_args_element_name_17 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "lte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_24;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 62;
        tmp_args_element_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, &PyTuple_GET_ITEM( const_tuple_int_pos_2100_tuple, 0 ) );

        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_17 );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_17, tmp_args_element_name_18 };
            tmp_source_name_9 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_args_element_name_17 );
        Py_DECREF( tmp_args_element_name_18 );
        if ( tmp_source_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto frame_exception_exit_1;
        }
        tmp_called_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_9 );
        if ( tmp_called_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_25 == NULL )
        {
            Py_DECREF( tmp_called_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_25;
        tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_year );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_19 );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 64;
        tmp_args_element_name_19 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_custom, &PyTuple_GET_ITEM( const_tuple_type_int_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_19 );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_called_name_23;
        PyObject *tmp_source_name_11;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_args_element_name_24;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_26;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_length_eq );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_length_eq );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "length_eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 68;

            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_27;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 68;
        tmp_args_element_name_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_args_element_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_gte );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gte );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_args_element_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_28;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 69;
        tmp_args_element_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_20 );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_lte );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_lte );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_args_element_name_20 );
            Py_DECREF( tmp_args_element_name_21 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "lte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 70;

            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_29;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 70;
        tmp_args_element_name_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, &PyTuple_GET_ITEM( const_tuple_int_pos_99_tuple, 0 ) );

        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_20 );
            Py_DECREF( tmp_args_element_name_21 );

            exception_lineno = 70;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_20, tmp_args_element_name_21, tmp_args_element_name_22 };
            tmp_source_name_11 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_args_element_name_20 );
        Py_DECREF( tmp_args_element_name_21 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_source_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_11 );
        if ( tmp_called_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_called_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_30;
        tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_year );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_23 );

            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_24 = MAKE_FUNCTION_grammars$date$$$function_1_lambda(  );



        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_args_element_name_23 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_custom, call_args );
        }

        Py_DECREF( tmp_called_instance_6 );
        Py_DECREF( tmp_args_element_name_24 );
        if ( tmp_args_element_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_23 );

            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_23 };
            tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_called_name_23 );
        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_SHORT, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_name_28;
        PyObject *tmp_source_name_13;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_called_name_31;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_34;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 75;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_31;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_gte );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gte );
        }

        if ( tmp_mvar_value_32 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_32;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 76;
        tmp_args_element_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_args_element_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_lte );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_lte );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_args_element_name_25 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "lte" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_called_name_31 = tmp_mvar_value_33;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 77;
        tmp_args_element_name_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, &PyTuple_GET_ITEM( const_tuple_int_pos_100000_tuple, 0 ) );

        if ( tmp_args_element_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_25 );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 75;
        {
            PyObject *call_args[] = { tmp_args_element_name_25, tmp_args_element_name_26 };
            tmp_source_name_13 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_29, call_args );
        }

        Py_DECREF( tmp_args_element_name_25 );
        Py_DECREF( tmp_args_element_name_26 );
        if ( tmp_source_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        tmp_called_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_13 );
        if ( tmp_called_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_called_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_34;
        tmp_called_instance_7 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_year );
        if ( tmp_called_instance_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 79;
        tmp_args_element_name_27 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_custom, &PyTuple_GET_ITEM( const_tuple_type_int_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_7 );
        if ( tmp_args_element_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 75;
        {
            PyObject *call_args[] = { tmp_args_element_name_27 };
            tmp_assign_source_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_element_name_27 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_ERA_YEAR, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_32;
        PyObject *tmp_source_name_15;
        PyObject *tmp_called_name_33;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_called_name_34;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_called_name_35;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_args_element_name_30;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_args_element_name_31;
        PyObject *tmp_args_element_name_32;
        PyObject *tmp_called_name_37;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_args_element_name_33;
        PyObject *tmp_args_element_name_34;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_called_name_38;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_args_element_name_35;
        PyObject *tmp_called_name_39;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_args_element_name_36;
        PyObject *tmp_called_name_40;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_called_name_41;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_args_element_name_38;
        PyObject *tmp_called_instance_9;
        PyObject *tmp_source_name_16;
        PyObject *tmp_mvar_value_44;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }

        tmp_called_name_33 = tmp_mvar_value_35;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_36 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_called_name_34 = tmp_mvar_value_36;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 83;
        tmp_args_element_name_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, &PyTuple_GET_ITEM( const_tuple_str_digest_947f7a6ac444bb1ddd86a5723bc9579c_tuple, 0 ) );

        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_37 == NULL )
        {
            Py_DECREF( tmp_args_element_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }

        tmp_called_name_35 = tmp_mvar_value_37;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_38 == NULL )
        {
            Py_DECREF( tmp_args_element_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_38;
        tmp_args_element_name_31 = const_str_chr_1085;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_args_element_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }

        tmp_called_name_37 = tmp_mvar_value_39;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 85;
        tmp_args_element_name_32 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_37, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_args_element_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_33 = const_str_chr_1101;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }

        tmp_called_name_38 = tmp_mvar_value_40;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 85;
        tmp_called_instance_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_38, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_called_instance_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_32 );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 85;
        tmp_args_element_name_34 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_8 );
        if ( tmp_args_element_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_32 );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_31, tmp_args_element_name_32, tmp_args_element_name_33, tmp_args_element_name_34 };
            tmp_args_element_name_30 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_36, call_args );
        }

        Py_DECREF( tmp_args_element_name_32 );
        Py_DECREF( tmp_args_element_name_34 );
        if ( tmp_args_element_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_30 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }

        tmp_called_name_39 = tmp_mvar_value_41;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_30 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }

        tmp_called_name_40 = tmp_mvar_value_42;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 86;
        tmp_args_element_name_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_40, &PyTuple_GET_ITEM( const_tuple_str_digest_295632cff31d071fab8d66f837bda99e_tuple, 0 ) );

        if ( tmp_args_element_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_30 );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_30 );
            Py_DECREF( tmp_args_element_name_36 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }

        tmp_called_name_41 = tmp_mvar_value_43;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 86;
        tmp_args_element_name_37 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_41, &PyTuple_GET_ITEM( const_tuple_str_digest_8f141c194bc565da997cf47a8956e393_tuple, 0 ) );

        if ( tmp_args_element_name_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_30 );
            Py_DECREF( tmp_args_element_name_36 );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 86;
        {
            PyObject *call_args[] = { tmp_args_element_name_36, tmp_args_element_name_37 };
            tmp_args_element_name_35 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_39, call_args );
        }

        Py_DECREF( tmp_args_element_name_36 );
        Py_DECREF( tmp_args_element_name_37 );
        if ( tmp_args_element_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_30 );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 84;
        {
            PyObject *call_args[] = { tmp_args_element_name_30, tmp_args_element_name_35 };
            tmp_args_element_name_29 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_35, call_args );
        }

        Py_DECREF( tmp_args_element_name_30 );
        Py_DECREF( tmp_args_element_name_35 );
        if ( tmp_args_element_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_28 );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 82;
        {
            PyObject *call_args[] = { tmp_args_element_name_28, tmp_args_element_name_29 };
            tmp_source_name_15 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_33, call_args );
        }

        Py_DECREF( tmp_args_element_name_28 );
        Py_DECREF( tmp_args_element_name_29 );
        if ( tmp_source_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        tmp_called_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_15 );
        if ( tmp_called_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_44 == NULL )
        {
            Py_DECREF( tmp_called_name_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;

            goto frame_exception_exit_1;
        }

        tmp_source_name_16 = tmp_mvar_value_44;
        tmp_called_instance_9 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_current_era );
        if ( tmp_called_instance_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_32 );

            exception_lineno = 89;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 89;
        tmp_args_element_name_38 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_const, &PyTuple_GET_ITEM( const_tuple_false_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_9 );
        if ( tmp_args_element_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_32 );

            exception_lineno = 89;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 82;
        {
            PyObject *call_args[] = { tmp_args_element_name_38 };
            tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
        }

        Py_DECREF( tmp_called_name_32 );
        Py_DECREF( tmp_args_element_name_38 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_ERA_WORD, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_name_42;
        PyObject *tmp_source_name_17;
        PyObject *tmp_called_name_43;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_called_name_44;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_args_element_name_42;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_args_element_name_44;
        PyObject *tmp_called_name_45;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_args_element_name_46;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_called_instance_10;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_args_element_name_48;
        PyObject *tmp_called_name_46;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_args_element_name_49;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_args_element_name_50;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_args_element_name_51;
        PyObject *tmp_called_name_47;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_args_element_name_52;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_args_element_name_53;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_args_element_name_54;
        PyObject *tmp_called_name_48;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_args_element_name_55;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_args_element_name_56;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_args_element_name_57;
        PyObject *tmp_called_instance_11;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_args_element_name_58;
        PyObject *tmp_called_name_49;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_args_element_name_59;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_args_element_name_60;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_args_element_name_61;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_args_element_name_62;
        PyObject *tmp_called_instance_12;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_args_element_name_63;
        PyObject *tmp_called_name_50;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_args_element_name_64;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_args_element_name_65;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_args_element_name_66;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_args_element_name_67;
        PyObject *tmp_mvar_value_72;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_45 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;

            goto frame_exception_exit_1;
        }

        tmp_called_name_43 = tmp_mvar_value_45;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_46 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 93;

            goto frame_exception_exit_1;
        }

        tmp_called_name_44 = tmp_mvar_value_46;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_DAY );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DAY );
        }

        if ( tmp_mvar_value_47 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DAY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_40 = tmp_mvar_value_47;
        tmp_args_element_name_41 = const_str_dot;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTH );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONTH );
        }

        if ( tmp_mvar_value_48 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONTH" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_42 = tmp_mvar_value_48;
        tmp_args_element_name_43 = const_str_dot;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_49 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;

            goto frame_exception_exit_1;
        }

        tmp_called_name_45 = tmp_mvar_value_49;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR );
        }

        if ( tmp_mvar_value_50 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_45 = tmp_mvar_value_50;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_SHORT );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR_SHORT );
        }

        if ( tmp_mvar_value_51 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR_SHORT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_46 = tmp_mvar_value_51;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_45, tmp_args_element_name_46 };
            tmp_args_element_name_44 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_45, call_args );
        }

        if ( tmp_args_element_name_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );
        }

        if ( tmp_mvar_value_52 == NULL )
        {
            Py_DECREF( tmp_args_element_name_44 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR_WORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_10 = tmp_mvar_value_52;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 102;
        tmp_args_element_name_47 = CALL_METHOD_NO_ARGS( tmp_called_instance_10, const_str_plain_optional );
        if ( tmp_args_element_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_44 );

            exception_lineno = 102;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 93;
        {
            PyObject *call_args[] = { tmp_args_element_name_40, tmp_args_element_name_41, tmp_args_element_name_42, tmp_args_element_name_43, tmp_args_element_name_44, tmp_args_element_name_47 };
            tmp_args_element_name_39 = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_44, call_args );
        }

        Py_DECREF( tmp_args_element_name_44 );
        Py_DECREF( tmp_args_element_name_47 );
        if ( tmp_args_element_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }

        tmp_called_name_46 = tmp_mvar_value_53;
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR );
        }

        if ( tmp_mvar_value_54 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_49 = tmp_mvar_value_54;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );
        }

        if ( tmp_mvar_value_55 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR_WORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_50 = tmp_mvar_value_55;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_49, tmp_args_element_name_50 };
            tmp_args_element_name_48 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_46, call_args );
        }

        if ( tmp_args_element_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_56 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;

            goto frame_exception_exit_1;
        }

        tmp_called_name_47 = tmp_mvar_value_56;
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_DAY );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DAY );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DAY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 109;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_52 = tmp_mvar_value_57;
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTH_NAME );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONTH_NAME );
        }

        if ( tmp_mvar_value_58 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONTH_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_53 = tmp_mvar_value_58;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 108;
        {
            PyObject *call_args[] = { tmp_args_element_name_52, tmp_args_element_name_53 };
            tmp_args_element_name_51 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_47, call_args );
        }

        if ( tmp_args_element_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 108;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_59 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;

            goto frame_exception_exit_1;
        }

        tmp_called_name_48 = tmp_mvar_value_59;
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTH_NAME );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONTH_NAME );
        }

        if ( tmp_mvar_value_60 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONTH_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_55 = tmp_mvar_value_60;
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_56 = tmp_mvar_value_61;
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR_WORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_11 = tmp_mvar_value_62;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 115;
        tmp_args_element_name_57 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_optional );
        if ( tmp_args_element_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_55, tmp_args_element_name_56, tmp_args_element_name_57 };
            tmp_args_element_name_54 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_48, call_args );
        }

        Py_DECREF( tmp_args_element_name_57 );
        if ( tmp_args_element_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );

            exception_lineno = 112;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }

        tmp_called_name_49 = tmp_mvar_value_63;
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_DAY );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DAY );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DAY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_59 = tmp_mvar_value_64;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_MONTH_NAME );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONTH_NAME );
        }

        if ( tmp_mvar_value_65 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONTH_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_60 = tmp_mvar_value_65;
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_61 = tmp_mvar_value_66;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR_WORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_12 = tmp_mvar_value_67;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 121;
        tmp_args_element_name_62 = CALL_METHOD_NO_ARGS( tmp_called_instance_12, const_str_plain_optional );
        if ( tmp_args_element_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );

            exception_lineno = 121;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 117;
        {
            PyObject *call_args[] = { tmp_args_element_name_59, tmp_args_element_name_60, tmp_args_element_name_61, tmp_args_element_name_62 };
            tmp_args_element_name_58 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_49, call_args );
        }

        Py_DECREF( tmp_args_element_name_62 );
        if ( tmp_args_element_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );

            exception_lineno = 117;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;

            goto frame_exception_exit_1;
        }

        tmp_called_name_50 = tmp_mvar_value_68;
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_ERA_YEAR );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ERA_YEAR );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ERA_YEAR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_64 = tmp_mvar_value_69;
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_YEAR_WORD );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "YEAR_WORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_65 = tmp_mvar_value_70;
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_ERA_WORD );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ERA_WORD );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ERA_WORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_66 = tmp_mvar_value_71;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_64, tmp_args_element_name_65, tmp_args_element_name_66 };
            tmp_args_element_name_63 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_50, call_args );
        }

        if ( tmp_args_element_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            Py_DECREF( tmp_args_element_name_54 );
            Py_DECREF( tmp_args_element_name_58 );

            exception_lineno = 123;

            goto frame_exception_exit_1;
        }
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_39, tmp_args_element_name_48, tmp_args_element_name_51, tmp_args_element_name_54, tmp_args_element_name_58, tmp_args_element_name_63 };
            tmp_source_name_17 = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_43, call_args );
        }

        Py_DECREF( tmp_args_element_name_39 );
        Py_DECREF( tmp_args_element_name_48 );
        Py_DECREF( tmp_args_element_name_51 );
        Py_DECREF( tmp_args_element_name_54 );
        Py_DECREF( tmp_args_element_name_58 );
        Py_DECREF( tmp_args_element_name_63 );
        if ( tmp_source_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        tmp_called_name_42 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_17 );
        if ( tmp_called_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_Date );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Date );
        }

        if ( tmp_mvar_value_72 == NULL )
        {
            Py_DECREF( tmp_called_name_42 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Date" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_67 = tmp_mvar_value_72;
        frame_993954414d8f34f0527f3be818b8ad36->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_67 };
            tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_42, call_args );
        }

        Py_DECREF( tmp_called_name_42 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$date, (Nuitka_StringObject *)const_str_plain_DATE, tmp_assign_source_29 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_993954414d8f34f0527f3be818b8ad36 );
#endif
    popFrameStack();

    assertFrameObject( frame_993954414d8f34f0527f3be818b8ad36 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_993954414d8f34f0527f3be818b8ad36 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_993954414d8f34f0527f3be818b8ad36, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_993954414d8f34f0527f3be818b8ad36->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_993954414d8f34f0527f3be818b8ad36, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_grammars$date );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
