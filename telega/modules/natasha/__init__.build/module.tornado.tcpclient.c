/* Generated code for Python module 'tornado.tcpclient'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_tornado$tcpclient" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_tornado$tcpclient;
PyDictObject *moduledict_tornado$tcpclient;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_TimeoutError;
static PyObject *const_str_digest_a5ec5f7583a10182afead56cee5545a7;
extern PyObject *const_str_plain_resolve;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_digest_a9749d20124866074eb2f4f5b5d6dda5;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_addr;
static PyObject *const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple;
extern PyObject *const_str_plain_Set;
static PyObject *const_str_plain_source_port;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_plain_last_error;
extern PyObject *const_str_plain_timedelta;
extern PyObject *const_str_plain_IOLoop;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_dict_056a293e2058d56276328e53ff09a8b9;
extern PyObject *const_str_plain_Resolver;
static PyObject *const_str_digest_dbf320d6ab8c292ec06eb15c20d40242;
extern PyObject *const_str_plain_TCPClient;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_digest_c56ac0b712595d363e21033bdbaf2d31;
extern PyObject *const_str_plain_on_timeout;
extern PyObject *const_str_plain_gen;
extern PyObject *const_str_plain_fileno;
extern PyObject *const_str_plain_af;
extern PyObject *const_str_plain_Callable;
extern PyObject *const_str_plain_max_buffer_size;
extern PyObject *const_str_plain_Real;
extern PyObject *const_tuple_str_plain_self_str_plain_stream_tuple;
extern PyObject *const_str_digest_66ef772b0f62991fd20497da33b24b9b;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_Dict;
extern PyObject *const_str_plain_Optional;
static PyObject *const_tuple_str_plain_Future_str_plain_future_add_done_callback_tuple;
extern PyObject *const_str_plain_return;
extern PyObject *const_str_plain_staticmethod;
extern PyObject *const_tuple_str_plain_gen_tuple;
extern PyObject *const_str_plain_List;
static PyObject *const_str_digest_3f478350567dda4f59bb11821d2188f3;
static PyObject *const_str_digest_9c35a83a41df634bfe1f1aeb9487e326;
extern PyObject *const_str_plain_with_timeout;
static PyObject *const_str_digest_39bf746e7e6fa9081cb27981301d6497;
extern PyObject *const_str_plain_fu;
static PyObject *const_str_plain__INITIAL_CONNECT_TIMEOUT;
extern PyObject *const_str_plain_connect_timeout;
extern PyObject *const_str_plain_bind;
extern PyObject *const_str_plain_start;
static PyObject *const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple;
extern PyObject *const_tuple_str_plain_set_close_exec_tuple;
extern PyObject *const_str_plain_typing;
extern PyObject *const_str_plain_connect;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_set_connect_timeout;
extern PyObject *const_str_plain_timeout;
extern PyObject *const_tuple_str_plain_TimeoutError_tuple;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_plain_primary_af;
extern PyObject *const_str_plain_time;
extern PyObject *const_str_plain_Future;
extern PyObject *const_str_plain_close;
static PyObject *const_str_digest_f3e84bb2c0ba624424fca914b1a06f4e;
extern PyObject *const_str_plain_remaining;
extern PyObject *const_str_plain_ssl_options;
extern PyObject *const_str_plain_add;
static PyObject *const_tuple_str_plain_Resolver_tuple;
extern PyObject *const_str_plain_str;
static PyObject *const_str_plain_on_connect_done;
extern PyObject *const_str_plain_primary;
static PyObject *const_tuple_str_plain_self_str_plain_addrinfo_str_plain_connect_tuple;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_host;
static PyObject *const_str_digest_f60549bbcd4e8e24830aa6307857816b;
static PyObject *const_tuple_deb336c834b9248726d3a56f69093fe2_tuple;
static PyObject *const_str_digest_e0643bd494e012b9157e7c29953e4eba;
extern PyObject *const_str_plain_SSLContext;
extern PyObject *const_str_plain_set_close_exec;
static PyObject *const_str_digest_63220ccc51be15c30b069d2428e0ba3e;
static PyObject *const_str_digest_a6f3b7f5cf87c6768a6b5fa2a4394168;
extern PyObject *const_str_plain_int;
static PyObject *const_str_plain_source_ip_bind;
extern PyObject *const_str_plain_connector;
static PyObject *const_str_plain_secondary_addrs;
static PyObject *const_str_plain_clear_timeouts;
extern PyObject *const_str_plain_float;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_Tuple;
static PyObject *const_str_plain_on_connect_timeout;
extern PyObject *const_str_plain_TYPE_CHECKING;
extern PyObject *const_str_plain_e;
static PyObject *const_str_digest_6c2f84cc90af0505120aaa5f893a0d8a;
static PyObject *const_str_plain__create_stream;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_numbers;
static PyObject *const_str_digest_6fc24d07893c3730673b1956f99df558;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_source_ip;
extern PyObject *const_str_plain_datetime;
extern PyObject *const_str_digest_9b98666360eab9bd5e6093808f6c2588;
static PyObject *const_str_digest_c24ffa9ac487f488583e3aa88e1a7fde;
static PyObject *const_str_plain_secondary;
static PyObject *const_str_digest_117aea24be92adc94cba16e3bc01c3a1;
extern PyObject *const_str_plain_Any;
extern PyObject *const_float_0_3;
static PyObject *const_tuple_str_plain_Optional_str_plain_Set_tuple;
extern PyObject *const_str_plain_addrinfo;
extern PyObject *const_str_plain_error;
extern PyObject *const_str_digest_a78b083c1c6e4171ab199ac37817d480;
extern PyObject *const_str_plain_split;
static PyObject *const_str_plain_close_streams;
extern PyObject *const_str_plain_AddressFamily;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_digest_d057d99810b1c539102c2397e91f2552;
extern PyObject *const_str_plain_remove_timeout;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_stream;
static PyObject *const_str_digest_c5af78c28f1e6b1a5b6fa585d863557d;
extern PyObject *const_str_plain_start_tls;
static PyObject *const_tuple_str_plain_self_str_plain_timeout_str_plain_connect_timeout_tuple;
extern PyObject *const_str_plain_tornado;
extern PyObject *const_str_plain_ssl;
extern PyObject *const_str_digest_0ababed7bda89496db964fae880aa905;
static PyObject *const_tuple_str_plain_self_str_plain_resolver_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_server_hostname;
extern PyObject *const_str_digest_3f9fec55820f857461da4f1e12840da5;
extern PyObject *const_str_digest_c5a6a5dde27777d16fa4978b5cf11756;
static PyObject *const_str_plain__Connector;
extern PyObject *const_str_plain_current;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_d77c8dba2c1752ad707d97d40a958563;
extern PyObject *const_str_plain_resolver;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_tuple_a81590143c265f724e87fff796eb5136_tuple;
static PyObject *const_str_plain_set_timeout;
static PyObject *const_str_digest_60c529acd196cbf00ea282122ccf4eb2;
extern PyObject *const_str_plain_Iterator;
static PyObject *const_str_digest_b50dec95435ce18f9dbeb8564c432fb2;
extern PyObject *const_str_plain_type;
extern PyObject *const_tuple_str_plain_IOLoop_tuple;
static PyObject *const_str_digest_c89da60025fa7fade027312fa03fc3ff;
extern PyObject *const_str_plain_AF_UNSPEC;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_digest_a054dc237cb6ddb4265000e5728b4749;
static PyObject *const_str_plain_source_port_bind;
extern PyObject *const_str_plain_IOStream;
static PyObject *const_tuple_str_plain_IOStream_tuple;
extern PyObject *const_str_plain_Union;
static PyObject *const_str_plain_clear_timeout;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_functools;
static PyObject *const_str_plain_try_connect;
static PyObject *const_str_digest_17375146e8fa326c089317143a43d610;
extern PyObject *const_str_digest_a78b79c1406c2b3f78fc8a3a0c110b46;
extern PyObject *const_str_plain_AF_INET6;
static PyObject *const_str_digest_fe7119b909401f80d9f429ed075d30c3;
static PyObject *const_str_digest_3028aebf52cc3ba379a11963de1a1b88;
extern PyObject *const_str_plain_partial;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_socket;
extern PyObject *const_str_plain_set_exception;
extern PyObject *const_str_plain_port;
static PyObject *const_str_digest_ba5dc92d7e0677efa71a10e00e7065fd;
extern PyObject *const_str_plain_add_timeout;
extern PyObject *const_str_plain_future_add_done_callback;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
static PyObject *const_tuple_a60f734589ef15f46084be8283528eac_tuple;
static PyObject *const_str_plain__own_resolver;
static PyObject *const_str_digest_4a46cceca4b3639158c4818f0c5676c6;
static PyObject *const_str_digest_75b86f3d3c6c651ab3dc77de6dea87e9;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_io_loop;
extern PyObject *const_str_plain_total_seconds;
extern PyObject *const_str_plain_done;
static PyObject *const_str_digest_45c81ba291760ce276d329a5c153168e;
extern PyObject *const_str_plain_future;
static PyObject *const_tuple_str_plain_self_str_plain_connect_timeout_tuple;
static PyObject *const_str_plain_socket_obj;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_discard;
extern PyObject *const_tuple_str_plain_self_str_plain_timeout_tuple;
static PyObject *const_tuple_e97fa1f008637d53159139904fbee086_tuple;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_str_plain_addrs;
static PyObject *const_str_plain_primary_addrs;
extern PyObject *const_str_plain_set_result;
static PyObject *const_str_plain_streams;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_a5ec5f7583a10182afead56cee5545a7 = UNSTREAM_STRING_ASCII( &constant_bin[ 5493223 ], 52, 0 );
    const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple, 0, const_str_plain_addrinfo ); Py_INCREF( const_str_plain_addrinfo );
    PyTuple_SET_ITEM( const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple, 1, const_str_plain_primary ); Py_INCREF( const_str_plain_primary );
    const_str_plain_secondary = UNSTREAM_STRING_ASCII( &constant_bin[ 851992 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple, 2, const_str_plain_secondary ); Py_INCREF( const_str_plain_secondary );
    const_str_plain_primary_af = UNSTREAM_STRING_ASCII( &constant_bin[ 5493275 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple, 3, const_str_plain_primary_af ); Py_INCREF( const_str_plain_primary_af );
    PyTuple_SET_ITEM( const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple, 4, const_str_plain_af ); Py_INCREF( const_str_plain_af );
    PyTuple_SET_ITEM( const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple, 5, const_str_plain_addr ); Py_INCREF( const_str_plain_addr );
    const_str_plain_source_port = UNSTREAM_STRING_ASCII( &constant_bin[ 5493285 ], 11, 1 );
    const_str_plain_last_error = UNSTREAM_STRING_ASCII( &constant_bin[ 5493296 ], 10, 1 );
    const_str_digest_dbf320d6ab8c292ec06eb15c20d40242 = UNSTREAM_STRING_ASCII( &constant_bin[ 5493306 ], 24, 0 );
    const_tuple_str_plain_Future_str_plain_future_add_done_callback_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Future_str_plain_future_add_done_callback_tuple, 0, const_str_plain_Future ); Py_INCREF( const_str_plain_Future );
    PyTuple_SET_ITEM( const_tuple_str_plain_Future_str_plain_future_add_done_callback_tuple, 1, const_str_plain_future_add_done_callback ); Py_INCREF( const_str_plain_future_add_done_callback );
    const_str_digest_3f478350567dda4f59bb11821d2188f3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5493330 ], 18, 0 );
    const_str_digest_9c35a83a41df634bfe1f1aeb9487e326 = UNSTREAM_STRING_ASCII( &constant_bin[ 5493348 ], 17, 0 );
    const_str_digest_39bf746e7e6fa9081cb27981301d6497 = UNSTREAM_STRING_ASCII( &constant_bin[ 5493365 ], 22, 0 );
    const_str_plain__INITIAL_CONNECT_TIMEOUT = UNSTREAM_STRING_ASCII( &constant_bin[ 5493387 ], 24, 1 );
    const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple, 1, const_str_plain_addrs ); Py_INCREF( const_str_plain_addrs );
    PyTuple_SET_ITEM( const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple, 2, const_str_plain_af ); Py_INCREF( const_str_plain_af );
    PyTuple_SET_ITEM( const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple, 3, const_str_plain_addr ); Py_INCREF( const_str_plain_addr );
    PyTuple_SET_ITEM( const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple, 4, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple, 5, const_str_plain_future ); Py_INCREF( const_str_plain_future );
    const_str_plain_set_connect_timeout = UNSTREAM_STRING_ASCII( &constant_bin[ 5493411 ], 19, 1 );
    const_str_digest_f3e84bb2c0ba624424fca914b1a06f4e = UNSTREAM_STRING_ASCII( &constant_bin[ 5493430 ], 25, 0 );
    const_tuple_str_plain_Resolver_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Resolver_tuple, 0, const_str_plain_Resolver ); Py_INCREF( const_str_plain_Resolver );
    const_str_plain_on_connect_done = UNSTREAM_STRING_ASCII( &constant_bin[ 5493455 ], 15, 1 );
    const_tuple_str_plain_self_str_plain_addrinfo_str_plain_connect_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_addrinfo_str_plain_connect_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_addrinfo_str_plain_connect_tuple, 1, const_str_plain_addrinfo ); Py_INCREF( const_str_plain_addrinfo );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_addrinfo_str_plain_connect_tuple, 2, const_str_plain_connect ); Py_INCREF( const_str_plain_connect );
    const_str_digest_f60549bbcd4e8e24830aa6307857816b = UNSTREAM_STRING_ASCII( &constant_bin[ 5493470 ], 24, 0 );
    const_tuple_deb336c834b9248726d3a56f69093fe2_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 1, const_str_plain_max_buffer_size ); Py_INCREF( const_str_plain_max_buffer_size );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 2, const_str_plain_af ); Py_INCREF( const_str_plain_af );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 3, const_str_plain_addr ); Py_INCREF( const_str_plain_addr );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 4, const_str_plain_source_ip ); Py_INCREF( const_str_plain_source_ip );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 5, const_str_plain_source_port ); Py_INCREF( const_str_plain_source_port );
    const_str_plain_source_port_bind = UNSTREAM_STRING_ASCII( &constant_bin[ 5493494 ], 16, 1 );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 6, const_str_plain_source_port_bind ); Py_INCREF( const_str_plain_source_port_bind );
    const_str_plain_source_ip_bind = UNSTREAM_STRING_ASCII( &constant_bin[ 5493510 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 7, const_str_plain_source_ip_bind ); Py_INCREF( const_str_plain_source_ip_bind );
    const_str_plain_socket_obj = UNSTREAM_STRING_ASCII( &constant_bin[ 5493524 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 8, const_str_plain_socket_obj ); Py_INCREF( const_str_plain_socket_obj );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 9, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 10, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    PyTuple_SET_ITEM( const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 11, const_str_plain_fu ); Py_INCREF( const_str_plain_fu );
    const_str_digest_e0643bd494e012b9157e7c29953e4eba = UNSTREAM_STRING_ASCII( &constant_bin[ 5493534 ], 672, 0 );
    const_str_digest_63220ccc51be15c30b069d2428e0ba3e = UNSTREAM_STRING_ASCII( &constant_bin[ 5494206 ], 22, 0 );
    const_str_digest_a6f3b7f5cf87c6768a6b5fa2a4394168 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494228 ], 21, 0 );
    const_str_plain_secondary_addrs = UNSTREAM_STRING_ASCII( &constant_bin[ 5494249 ], 15, 1 );
    const_str_plain_clear_timeouts = UNSTREAM_STRING_ASCII( &constant_bin[ 5493441 ], 14, 1 );
    const_str_plain_on_connect_timeout = UNSTREAM_STRING_ASCII( &constant_bin[ 5494264 ], 18, 1 );
    const_str_digest_6c2f84cc90af0505120aaa5f893a0d8a = UNSTREAM_STRING_ASCII( &constant_bin[ 5494282 ], 50, 0 );
    const_str_plain__create_stream = UNSTREAM_STRING_ASCII( &constant_bin[ 5493480 ], 14, 1 );
    const_str_digest_6fc24d07893c3730673b1956f99df558 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494332 ], 29, 0 );
    const_str_digest_c24ffa9ac487f488583e3aa88e1a7fde = UNSTREAM_STRING_ASCII( &constant_bin[ 5494361 ], 153, 0 );
    const_str_digest_117aea24be92adc94cba16e3bc01c3a1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494514 ], 19, 0 );
    const_tuple_str_plain_Optional_str_plain_Set_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Optional_str_plain_Set_tuple, 0, const_str_plain_Optional ); Py_INCREF( const_str_plain_Optional );
    PyTuple_SET_ITEM( const_tuple_str_plain_Optional_str_plain_Set_tuple, 1, const_str_plain_Set ); Py_INCREF( const_str_plain_Set );
    const_str_plain_close_streams = UNSTREAM_STRING_ASCII( &constant_bin[ 5493317 ], 13, 1 );
    const_str_digest_c5af78c28f1e6b1a5b6fa585d863557d = UNSTREAM_STRING_ASCII( &constant_bin[ 5494533 ], 15, 0 );
    const_tuple_str_plain_self_str_plain_timeout_str_plain_connect_timeout_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_timeout_str_plain_connect_timeout_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_timeout_str_plain_connect_timeout_tuple, 1, const_str_plain_timeout ); Py_INCREF( const_str_plain_timeout );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_timeout_str_plain_connect_timeout_tuple, 2, const_str_plain_connect_timeout ); Py_INCREF( const_str_plain_connect_timeout );
    const_tuple_str_plain_self_str_plain_resolver_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_resolver_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_resolver_tuple, 1, const_str_plain_resolver ); Py_INCREF( const_str_plain_resolver );
    const_str_plain__Connector = UNSTREAM_STRING_ASCII( &constant_bin[ 5493306 ], 10, 1 );
    const_str_digest_d77c8dba2c1752ad707d97d40a958563 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494548 ], 16, 0 );
    const_tuple_a81590143c265f724e87fff796eb5136_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_a81590143c265f724e87fff796eb5136_tuple, 0, const_str_plain_Any ); Py_INCREF( const_str_plain_Any );
    PyTuple_SET_ITEM( const_tuple_a81590143c265f724e87fff796eb5136_tuple, 1, const_str_plain_Union ); Py_INCREF( const_str_plain_Union );
    PyTuple_SET_ITEM( const_tuple_a81590143c265f724e87fff796eb5136_tuple, 2, const_str_plain_Dict ); Py_INCREF( const_str_plain_Dict );
    PyTuple_SET_ITEM( const_tuple_a81590143c265f724e87fff796eb5136_tuple, 3, const_str_plain_Tuple ); Py_INCREF( const_str_plain_Tuple );
    PyTuple_SET_ITEM( const_tuple_a81590143c265f724e87fff796eb5136_tuple, 4, const_str_plain_List ); Py_INCREF( const_str_plain_List );
    PyTuple_SET_ITEM( const_tuple_a81590143c265f724e87fff796eb5136_tuple, 5, const_str_plain_Callable ); Py_INCREF( const_str_plain_Callable );
    PyTuple_SET_ITEM( const_tuple_a81590143c265f724e87fff796eb5136_tuple, 6, const_str_plain_Iterator ); Py_INCREF( const_str_plain_Iterator );
    const_str_plain_set_timeout = UNSTREAM_STRING_ASCII( &constant_bin[ 5470438 ], 11, 1 );
    const_str_digest_60c529acd196cbf00ea282122ccf4eb2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5493430 ], 24, 0 );
    const_str_digest_b50dec95435ce18f9dbeb8564c432fb2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494564 ], 30, 0 );
    const_str_digest_c89da60025fa7fade027312fa03fc3ff = UNSTREAM_STRING_ASCII( &constant_bin[ 5494594 ], 22, 0 );
    const_str_digest_a054dc237cb6ddb4265000e5728b4749 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494616 ], 26, 0 );
    const_tuple_str_plain_IOStream_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_IOStream_tuple, 0, const_str_plain_IOStream ); Py_INCREF( const_str_plain_IOStream );
    const_str_plain_clear_timeout = UNSTREAM_STRING_ASCII( &constant_bin[ 5493441 ], 13, 1 );
    const_str_plain_try_connect = UNSTREAM_STRING_ASCII( &constant_bin[ 5493376 ], 11, 1 );
    const_str_digest_17375146e8fa326c089317143a43d610 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494642 ], 20, 0 );
    const_str_digest_fe7119b909401f80d9f429ed075d30c3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494662 ], 16, 0 );
    const_str_digest_3028aebf52cc3ba379a11963de1a1b88 = UNSTREAM_STRING_ASCII( &constant_bin[ 5494678 ], 1007, 0 );
    const_str_digest_ba5dc92d7e0677efa71a10e00e7065fd = UNSTREAM_STRING_ASCII( &constant_bin[ 5495685 ], 17, 0 );
    const_tuple_a60f734589ef15f46084be8283528eac_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_a60f734589ef15f46084be8283528eac_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_a60f734589ef15f46084be8283528eac_tuple, 1, const_str_plain_addrs ); Py_INCREF( const_str_plain_addrs );
    PyTuple_SET_ITEM( const_tuple_a60f734589ef15f46084be8283528eac_tuple, 2, const_str_plain_af ); Py_INCREF( const_str_plain_af );
    PyTuple_SET_ITEM( const_tuple_a60f734589ef15f46084be8283528eac_tuple, 3, const_str_plain_addr ); Py_INCREF( const_str_plain_addr );
    PyTuple_SET_ITEM( const_tuple_a60f734589ef15f46084be8283528eac_tuple, 4, const_str_plain_future ); Py_INCREF( const_str_plain_future );
    PyTuple_SET_ITEM( const_tuple_a60f734589ef15f46084be8283528eac_tuple, 5, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    PyTuple_SET_ITEM( const_tuple_a60f734589ef15f46084be8283528eac_tuple, 6, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_str_plain__own_resolver = UNSTREAM_STRING_ASCII( &constant_bin[ 5495702 ], 13, 1 );
    const_str_digest_4a46cceca4b3639158c4818f0c5676c6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5495715 ], 388, 0 );
    const_str_digest_75b86f3d3c6c651ab3dc77de6dea87e9 = UNSTREAM_STRING_ASCII( &constant_bin[ 5496103 ], 26, 0 );
    const_str_digest_45c81ba291760ce276d329a5c153168e = UNSTREAM_STRING_ASCII( &constant_bin[ 5494361 ], 39, 0 );
    const_tuple_str_plain_self_str_plain_connect_timeout_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_connect_timeout_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_connect_timeout_tuple, 1, const_str_plain_connect_timeout ); Py_INCREF( const_str_plain_connect_timeout );
    const_tuple_e97fa1f008637d53159139904fbee086_tuple = PyTuple_New( 13 );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 1, const_str_plain_host ); Py_INCREF( const_str_plain_host );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 2, const_str_plain_port ); Py_INCREF( const_str_plain_port );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 3, const_str_plain_af ); Py_INCREF( const_str_plain_af );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 4, const_str_plain_ssl_options ); Py_INCREF( const_str_plain_ssl_options );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 5, const_str_plain_max_buffer_size ); Py_INCREF( const_str_plain_max_buffer_size );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 6, const_str_plain_source_ip ); Py_INCREF( const_str_plain_source_ip );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 7, const_str_plain_source_port ); Py_INCREF( const_str_plain_source_port );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 8, const_str_plain_timeout ); Py_INCREF( const_str_plain_timeout );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 9, const_str_plain_addrinfo ); Py_INCREF( const_str_plain_addrinfo );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 10, const_str_plain_connector ); Py_INCREF( const_str_plain_connector );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 11, const_str_plain_addr ); Py_INCREF( const_str_plain_addr );
    PyTuple_SET_ITEM( const_tuple_e97fa1f008637d53159139904fbee086_tuple, 12, const_str_plain_stream ); Py_INCREF( const_str_plain_stream );
    const_str_plain_primary_addrs = UNSTREAM_STRING_ASCII( &constant_bin[ 5496129 ], 13, 1 );
    const_str_plain_streams = UNSTREAM_STRING_ASCII( &constant_bin[ 867103 ], 7, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_tornado$tcpclient( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_7b8ffbaad84d3ceef21adcb2f3812e3f;
static PyCodeObject *codeobj_1ae114085666de15d483afe5f0f6984a;
static PyCodeObject *codeobj_c865cfb03d605666e7822b4eee0d8ac9;
static PyCodeObject *codeobj_d613320d0df2ebc5c52af6c928e1c130;
static PyCodeObject *codeobj_d68f8ca13a9673b9114ba90152a28042;
static PyCodeObject *codeobj_27460344c266d7b4afdcb1503875504d;
static PyCodeObject *codeobj_6e221219ab71e145074fd5a56615b3ef;
static PyCodeObject *codeobj_793f9e350275ba265bef7ec1b3f9d348;
static PyCodeObject *codeobj_b84f5a07a552cb21cef2f0fdfdb94933;
static PyCodeObject *codeobj_9337544eeae4fefac5b0f2e4de7c8d8e;
static PyCodeObject *codeobj_767c3d3c0821babfafc5da5d82408a00;
static PyCodeObject *codeobj_8ad4a72b5abeb6760f8bd6620dc8fb80;
static PyCodeObject *codeobj_47c6a2ad74230fcb9dcc1362760e0766;
static PyCodeObject *codeobj_1294c5e4eefa4f961a3eff38dd5d48bc;
static PyCodeObject *codeobj_8e0a19119d5caf5484e0b42a72c69ed6;
static PyCodeObject *codeobj_390b695c20ddbaaf72df3d190f411f82;
static PyCodeObject *codeobj_96716989f5f9ecb67ad282bdae3e0449;
static PyCodeObject *codeobj_8fbd1c7c6633ceaf0faf23969f9070be;
static PyCodeObject *codeobj_7a49bd1c33abeae8183a066ba3d45db5;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_17375146e8fa326c089317143a43d610 );
    codeobj_7b8ffbaad84d3ceef21adcb2f3812e3f = MAKE_CODEOBJ( module_filename_obj, const_str_digest_75b86f3d3c6c651ab3dc77de6dea87e9, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_1ae114085666de15d483afe5f0f6984a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_TCPClient, 203, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_c865cfb03d605666e7822b4eee0d8ac9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__Connector, 42, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_d613320d0df2ebc5c52af6c928e1c130 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 60, const_tuple_str_plain_self_str_plain_addrinfo_str_plain_connect_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d68f8ca13a9673b9114ba90152a28042 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 210, const_tuple_str_plain_self_str_plain_resolver_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_27460344c266d7b4afdcb1503875504d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__create_stream, 298, const_tuple_deb336c834b9248726d3a56f69093fe2_tuple, 6, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6e221219ab71e145074fd5a56615b3ef = MAKE_CODEOBJ( module_filename_obj, const_str_plain_clear_timeout, 176, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_793f9e350275ba265bef7ec1b3f9d348 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_clear_timeouts, 192, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b84f5a07a552cb21cef2f0fdfdb94933 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_close, 218, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9337544eeae4fefac5b0f2e4de7c8d8e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_close_streams, 198, const_tuple_str_plain_self_str_plain_stream_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_767c3d3c0821babfafc5da5d82408a00 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_connect, 222, const_tuple_e97fa1f008637d53159139904fbee086_tuple, 9, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8ad4a72b5abeb6760f8bd6620dc8fb80 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_on_connect_done, 134, const_tuple_a60f734589ef15f46084be8283528eac_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_47c6a2ad74230fcb9dcc1362760e0766 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_on_connect_timeout, 187, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1294c5e4eefa4f961a3eff38dd5d48bc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_on_timeout, 171, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8e0a19119d5caf5484e0b42a72c69ed6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_connect_timeout, 180, const_tuple_str_plain_self_str_plain_connect_timeout_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_390b695c20ddbaaf72df3d190f411f82 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_timeout, 166, const_tuple_str_plain_self_str_plain_timeout_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_96716989f5f9ecb67ad282bdae3e0449 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_split, 80, const_tuple_fc7014205dbbd85a516e13f6b62f87b9_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8fbd1c7c6633ceaf0faf23969f9070be = MAKE_CODEOBJ( module_filename_obj, const_str_plain_start, 105, const_tuple_str_plain_self_str_plain_timeout_str_plain_connect_timeout_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7a49bd1c33abeae8183a066ba3d45db5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_try_connect, 116, const_tuple_7a65676f748a5448b8b09fce7c79c531_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_10_on_connect_timeout( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_11_clear_timeouts( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_12_close_streams( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_13___init__( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_14_close( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_15_connect( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_16__create_stream( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_1___init__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_2_split( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_3_start( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_4_try_connect( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_5_on_connect_done( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_6_set_timeout( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_7_on_timeout( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_8_clear_timeout( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_9_set_connect_timeout( PyObject *annotations );


// The module function definitions.
static PyObject *impl_tornado$tcpclient$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_addrinfo = python_pars[ 1 ];
    PyObject *par_connect = python_pars[ 2 ];
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_d613320d0df2ebc5c52af6c928e1c130;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_d613320d0df2ebc5c52af6c928e1c130 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d613320d0df2ebc5c52af6c928e1c130, codeobj_d613320d0df2ebc5c52af6c928e1c130, module_tornado$tcpclient, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d613320d0df2ebc5c52af6c928e1c130 = cache_frame_d613320d0df2ebc5c52af6c928e1c130;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d613320d0df2ebc5c52af6c928e1c130 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d613320d0df2ebc5c52af6c928e1c130 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_d613320d0df2ebc5c52af6c928e1c130->m_frame.f_lineno = 67;
        tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_current );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_io_loop, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_connect );
        tmp_assattr_name_2 = par_connect;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_connect, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_assattr_target_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Future );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Future );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Future" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        frame_d613320d0df2ebc5c52af6c928e1c130->m_frame.f_lineno = 71;
        tmp_assattr_name_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_future, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        tmp_assattr_name_4 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_timeout, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        tmp_assattr_name_5 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_connect_timeout, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        tmp_assattr_name_6 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_last_error, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_assattr_target_7;
        CHECK_OBJECT( par_addrinfo );
        tmp_len_arg_1 = par_addrinfo;
        tmp_assattr_name_7 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_assattr_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_remaining, tmp_assattr_name_7 );
        Py_DECREF( tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( par_addrinfo );
        tmp_args_element_name_1 = par_addrinfo;
        frame_d613320d0df2ebc5c52af6c928e1c130->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_split, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooo";
            exception_lineno = 77;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooo";
            exception_lineno = 77;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooo";
                    exception_lineno = 77;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooo";
            exception_lineno = 77;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_assattr_target_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assattr_name_8 = tmp_tuple_unpack_1__element_1;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain_primary_addrs, tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assattr_name_9;
        PyObject *tmp_assattr_target_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assattr_name_9 = tmp_tuple_unpack_1__element_2;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_9 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain_secondary_addrs, tmp_assattr_name_9 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assattr_name_10;
        PyObject *tmp_assattr_target_10;
        tmp_assattr_name_10 = PySet_New( NULL );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_10 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain_streams, tmp_assattr_name_10 );
        Py_DECREF( tmp_assattr_name_10 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d613320d0df2ebc5c52af6c928e1c130 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d613320d0df2ebc5c52af6c928e1c130 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d613320d0df2ebc5c52af6c928e1c130, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d613320d0df2ebc5c52af6c928e1c130->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d613320d0df2ebc5c52af6c928e1c130, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d613320d0df2ebc5c52af6c928e1c130,
        type_description_1,
        par_self,
        par_addrinfo,
        par_connect
    );


    // Release cached frame.
    if ( frame_d613320d0df2ebc5c52af6c928e1c130 == cache_frame_d613320d0df2ebc5c52af6c928e1c130 )
    {
        Py_DECREF( frame_d613320d0df2ebc5c52af6c928e1c130 );
    }
    cache_frame_d613320d0df2ebc5c52af6c928e1c130 = NULL;

    assertFrameObject( frame_d613320d0df2ebc5c52af6c928e1c130 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_addrinfo );
    Py_DECREF( par_addrinfo );
    par_addrinfo = NULL;

    CHECK_OBJECT( (PyObject *)par_connect );
    Py_DECREF( par_connect );
    par_connect = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_addrinfo );
    Py_DECREF( par_addrinfo );
    par_addrinfo = NULL;

    CHECK_OBJECT( (PyObject *)par_connect );
    Py_DECREF( par_connect );
    par_connect = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_2_split( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_addrinfo = python_pars[ 0 ];
    PyObject *var_primary = NULL;
    PyObject *var_secondary = NULL;
    PyObject *var_primary_af = NULL;
    PyObject *var_af = NULL;
    PyObject *var_addr = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_96716989f5f9ecb67ad282bdae3e0449;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_96716989f5f9ecb67ad282bdae3e0449 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_primary == NULL );
        var_primary = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( var_secondary == NULL );
        var_secondary = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_96716989f5f9ecb67ad282bdae3e0449, codeobj_96716989f5f9ecb67ad282bdae3e0449, module_tornado$tcpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_96716989f5f9ecb67ad282bdae3e0449 = cache_frame_96716989f5f9ecb67ad282bdae3e0449;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_96716989f5f9ecb67ad282bdae3e0449 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_96716989f5f9ecb67ad282bdae3e0449 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( par_addrinfo );
        tmp_subscribed_name_2 = par_addrinfo;
        tmp_subscript_name_1 = const_int_0;
        tmp_subscribed_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_1, 0 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_assign_source_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_primary_af == NULL );
        var_primary_af = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_addrinfo );
        tmp_iter_arg_1 = par_addrinfo;
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                exception_lineno = 98;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 98;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 98;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 98;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 98;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_af;
            var_af = tmp_assign_source_9;
            Py_INCREF( var_af );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_10 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_addr;
            var_addr = tmp_assign_source_10;
            Py_INCREF( var_addr );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_af );
        tmp_compexpr_left_1 = var_af;
        CHECK_OBJECT( var_primary_af );
        tmp_compexpr_right_1 = var_primary_af;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( var_primary );
            tmp_called_instance_1 = var_primary;
            CHECK_OBJECT( var_af );
            tmp_tuple_element_1 = var_af;
            tmp_args_element_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_element_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_addr );
            tmp_tuple_element_1 = var_addr;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_element_name_1, 1, tmp_tuple_element_1 );
            frame_96716989f5f9ecb67ad282bdae3e0449->m_frame.f_lineno = 100;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_tuple_element_2;
            CHECK_OBJECT( var_secondary );
            tmp_called_instance_2 = var_secondary;
            CHECK_OBJECT( var_af );
            tmp_tuple_element_2 = var_af;
            tmp_args_element_name_2 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_element_name_2, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( var_addr );
            tmp_tuple_element_2 = var_addr;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_element_name_2, 1, tmp_tuple_element_2 );
            frame_96716989f5f9ecb67ad282bdae3e0449->m_frame.f_lineno = 102;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 98;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96716989f5f9ecb67ad282bdae3e0449 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96716989f5f9ecb67ad282bdae3e0449 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_96716989f5f9ecb67ad282bdae3e0449, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_96716989f5f9ecb67ad282bdae3e0449->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_96716989f5f9ecb67ad282bdae3e0449, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_96716989f5f9ecb67ad282bdae3e0449,
        type_description_1,
        par_addrinfo,
        var_primary,
        var_secondary,
        var_primary_af,
        var_af,
        var_addr
    );


    // Release cached frame.
    if ( frame_96716989f5f9ecb67ad282bdae3e0449 == cache_frame_96716989f5f9ecb67ad282bdae3e0449 )
    {
        Py_DECREF( frame_96716989f5f9ecb67ad282bdae3e0449 );
    }
    cache_frame_96716989f5f9ecb67ad282bdae3e0449 = NULL;

    assertFrameObject( frame_96716989f5f9ecb67ad282bdae3e0449 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_tuple_element_3;
        CHECK_OBJECT( var_primary );
        tmp_tuple_element_3 = var_primary;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( var_secondary );
        tmp_tuple_element_3 = var_secondary;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_3 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_2_split );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_addrinfo );
    Py_DECREF( par_addrinfo );
    par_addrinfo = NULL;

    CHECK_OBJECT( (PyObject *)var_primary );
    Py_DECREF( var_primary );
    var_primary = NULL;

    CHECK_OBJECT( (PyObject *)var_secondary );
    Py_DECREF( var_secondary );
    var_secondary = NULL;

    CHECK_OBJECT( (PyObject *)var_primary_af );
    Py_DECREF( var_primary_af );
    var_primary_af = NULL;

    Py_XDECREF( var_af );
    var_af = NULL;

    Py_XDECREF( var_addr );
    var_addr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_addrinfo );
    Py_DECREF( par_addrinfo );
    par_addrinfo = NULL;

    CHECK_OBJECT( (PyObject *)var_primary );
    Py_DECREF( var_primary );
    var_primary = NULL;

    CHECK_OBJECT( (PyObject *)var_secondary );
    Py_DECREF( var_secondary );
    var_secondary = NULL;

    Py_XDECREF( var_primary_af );
    var_primary_af = NULL;

    Py_XDECREF( var_af );
    var_af = NULL;

    Py_XDECREF( var_addr );
    var_addr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_2_split );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_3_start( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_timeout = python_pars[ 1 ];
    PyObject *par_connect_timeout = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_8fbd1c7c6633ceaf0faf23969f9070be;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_8fbd1c7c6633ceaf0faf23969f9070be = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8fbd1c7c6633ceaf0faf23969f9070be, codeobj_8fbd1c7c6633ceaf0faf23969f9070be, module_tornado$tcpclient, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8fbd1c7c6633ceaf0faf23969f9070be = cache_frame_8fbd1c7c6633ceaf0faf23969f9070be;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8fbd1c7c6633ceaf0faf23969f9070be );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8fbd1c7c6633ceaf0faf23969f9070be ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_try_connect );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_primary_addrs );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 110;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 110;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_8fbd1c7c6633ceaf0faf23969f9070be->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_timeout );
        tmp_args_element_name_2 = par_timeout;
        frame_8fbd1c7c6633ceaf0faf23969f9070be->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_set_timeout, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_connect_timeout );
        tmp_compexpr_left_1 = par_connect_timeout;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( par_self );
            tmp_called_instance_2 = par_self;
            CHECK_OBJECT( par_connect_timeout );
            tmp_args_element_name_3 = par_connect_timeout;
            frame_8fbd1c7c6633ceaf0faf23969f9070be->m_frame.f_lineno = 113;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_connect_timeout, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_future );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8fbd1c7c6633ceaf0faf23969f9070be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8fbd1c7c6633ceaf0faf23969f9070be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8fbd1c7c6633ceaf0faf23969f9070be );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8fbd1c7c6633ceaf0faf23969f9070be, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8fbd1c7c6633ceaf0faf23969f9070be->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8fbd1c7c6633ceaf0faf23969f9070be, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8fbd1c7c6633ceaf0faf23969f9070be,
        type_description_1,
        par_self,
        par_timeout,
        par_connect_timeout
    );


    // Release cached frame.
    if ( frame_8fbd1c7c6633ceaf0faf23969f9070be == cache_frame_8fbd1c7c6633ceaf0faf23969f9070be )
    {
        Py_DECREF( frame_8fbd1c7c6633ceaf0faf23969f9070be );
    }
    cache_frame_8fbd1c7c6633ceaf0faf23969f9070be = NULL;

    assertFrameObject( frame_8fbd1c7c6633ceaf0faf23969f9070be );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_3_start );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_timeout );
    Py_DECREF( par_timeout );
    par_timeout = NULL;

    CHECK_OBJECT( (PyObject *)par_connect_timeout );
    Py_DECREF( par_connect_timeout );
    par_connect_timeout = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_timeout );
    Py_DECREF( par_timeout );
    par_timeout = NULL;

    CHECK_OBJECT( (PyObject *)par_connect_timeout );
    Py_DECREF( par_connect_timeout );
    par_connect_timeout = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_3_start );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_4_try_connect( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_addrs = python_pars[ 1 ];
    PyObject *var_af = NULL;
    PyObject *var_addr = NULL;
    PyObject *var_stream = NULL;
    PyObject *var_future = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_7a49bd1c33abeae8183a066ba3d45db5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_7a49bd1c33abeae8183a066ba3d45db5 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7a49bd1c33abeae8183a066ba3d45db5, codeobj_7a49bd1c33abeae8183a066ba3d45db5, module_tornado$tcpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7a49bd1c33abeae8183a066ba3d45db5 = cache_frame_7a49bd1c33abeae8183a066ba3d45db5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7a49bd1c33abeae8183a066ba3d45db5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7a49bd1c33abeae8183a066ba3d45db5 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_value_name_1;
        CHECK_OBJECT( par_addrs );
        tmp_value_name_1 = par_addrs;
        tmp_iter_arg_1 = ITERATOR_NEXT( tmp_value_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 118;
            goto try_except_handler_3;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 118;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 118;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 118;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 118;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_7a49bd1c33abeae8183a066ba3d45db5, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_7a49bd1c33abeae8183a066ba3d45db5, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_StopIteration;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "oooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_1 = par_self;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_remaining );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_right_2 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_future );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = 123;
            tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_done );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_2 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_3;
                PyObject *tmp_source_name_4;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_1;
                int tmp_or_left_truth_1;
                PyObject *tmp_or_left_value_1;
                PyObject *tmp_or_right_value_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_make_exception_arg_1;
                CHECK_OBJECT( par_self );
                tmp_source_name_4 = par_self;
                tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_future );
                if ( tmp_source_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_set_exception );
                Py_DECREF( tmp_source_name_3 );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                CHECK_OBJECT( par_self );
                tmp_source_name_5 = par_self;
                tmp_or_left_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_last_error );
                if ( tmp_or_left_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 125;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
                if ( tmp_or_left_truth_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_or_left_value_1 );

                    exception_lineno = 125;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                if ( tmp_or_left_truth_1 == 1 )
                {
                    goto or_left_1;
                }
                else
                {
                    goto or_right_1;
                }
                or_right_1:;
                Py_DECREF( tmp_or_left_value_1 );
                tmp_make_exception_arg_1 = const_str_digest_ba5dc92d7e0677efa71a10e00e7065fd;
                frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = 125;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_or_right_value_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
                }

                assert( !(tmp_or_right_value_1 == NULL) );
                tmp_args_element_name_1 = tmp_or_right_value_1;
                goto or_end_1;
                or_left_1:;
                tmp_args_element_name_1 = tmp_or_left_value_1;
                or_end_1:;
                frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = 124;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_1 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_2:;
        }
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_5;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 117;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame) frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooo";
        goto try_except_handler_5;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_4_try_connect );
    return NULL;
    // Return handler code:
    try_return_handler_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_af == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_af = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_addr == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_addr = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( var_af );
        tmp_args_element_name_2 = var_af;
        CHECK_OBJECT( var_addr );
        tmp_args_element_name_3 = var_addr;
        frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = 128;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_iter_arg_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_connect, call_args );
        }

        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "oooooo";
            goto try_except_handler_6;
        }
        tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "oooooo";
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_2__source_iter == NULL );
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 128;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_2__element_1 == NULL );
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 128;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_2__element_2 == NULL );
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_8;
    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 128;
                    goto try_except_handler_7;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 128;
            goto try_except_handler_7;
        }
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_6;
    // End of try:
    try_end_4:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_9 = tmp_tuple_unpack_2__element_1;
        assert( var_stream == NULL );
        Py_INCREF( tmp_assign_source_9 );
        var_stream = tmp_assign_source_9;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_10 = tmp_tuple_unpack_2__element_2;
        assert( var_future == NULL );
        Py_INCREF( tmp_assign_source_10 );
        var_future = tmp_assign_source_10;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_streams );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_stream );
        tmp_args_element_name_4 = var_stream;
        frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_add, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_source_name_8;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_future_add_done_callback );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_future_add_done_callback );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "future_add_done_callback" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( var_future );
        tmp_args_element_name_5 = var_future;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_2;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_partial );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_args_element_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_on_connect_done );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 131;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_addrs );
        tmp_args_element_name_8 = par_addrs;
        CHECK_OBJECT( var_af );
        tmp_args_element_name_9 = var_af;
        CHECK_OBJECT( var_addr );
        tmp_args_element_name_10 = var_addr;
        frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame.f_lineno = 130;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a49bd1c33abeae8183a066ba3d45db5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a49bd1c33abeae8183a066ba3d45db5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a49bd1c33abeae8183a066ba3d45db5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7a49bd1c33abeae8183a066ba3d45db5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7a49bd1c33abeae8183a066ba3d45db5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7a49bd1c33abeae8183a066ba3d45db5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7a49bd1c33abeae8183a066ba3d45db5,
        type_description_1,
        par_self,
        par_addrs,
        var_af,
        var_addr,
        var_stream,
        var_future
    );


    // Release cached frame.
    if ( frame_7a49bd1c33abeae8183a066ba3d45db5 == cache_frame_7a49bd1c33abeae8183a066ba3d45db5 )
    {
        Py_DECREF( frame_7a49bd1c33abeae8183a066ba3d45db5 );
    }
    cache_frame_7a49bd1c33abeae8183a066ba3d45db5 = NULL;

    assertFrameObject( frame_7a49bd1c33abeae8183a066ba3d45db5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_4_try_connect );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_addrs );
    Py_DECREF( par_addrs );
    par_addrs = NULL;

    Py_XDECREF( var_af );
    var_af = NULL;

    Py_XDECREF( var_addr );
    var_addr = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_future );
    var_future = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_addrs );
    Py_DECREF( par_addrs );
    par_addrs = NULL;

    Py_XDECREF( var_af );
    var_af = NULL;

    Py_XDECREF( var_addr );
    var_addr = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_future );
    var_future = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_4_try_connect );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_5_on_connect_done( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_addrs = python_pars[ 1 ];
    PyObject *par_af = python_pars[ 2 ];
    PyObject *par_addr = python_pars[ 3 ];
    PyObject *par_future = python_pars[ 4 ];
    PyObject *var_stream = NULL;
    PyObject *var_e = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_8ad4a72b5abeb6760f8bd6620dc8fb80;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_8ad4a72b5abeb6760f8bd6620dc8fb80 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8ad4a72b5abeb6760f8bd6620dc8fb80, codeobj_8ad4a72b5abeb6760f8bd6620dc8fb80, module_tornado$tcpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8ad4a72b5abeb6760f8bd6620dc8fb80 = cache_frame_8ad4a72b5abeb6760f8bd6620dc8fb80;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_remaining );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        tmp_right_name_1 = const_int_pos_1;
        tmp_assign_source_2 = BINARY_OPERATION( PyNumber_InPlaceSubtract, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_remaining, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooooo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_future );
        tmp_called_instance_1 = par_future;
        frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 143;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_result );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        assert( var_stream == NULL );
        var_stream = tmp_assign_source_3;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_8ad4a72b5abeb6760f8bd6620dc8fb80, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_8ad4a72b5abeb6760f8bd6620dc8fb80, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_Exception;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_4;
            tmp_assign_source_4 = EXC_VALUE(PyThreadState_GET());
            assert( var_e == NULL );
            Py_INCREF( tmp_assign_source_4 );
            var_e = tmp_assign_source_4;
        }
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_future );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 145;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 145;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_done );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 145;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_1 );

                exception_lineno = 145;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_6;
            branch_no_2:;
        }
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            CHECK_OBJECT( var_e );
            tmp_assattr_name_2 = var_e;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_2 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_last_error, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
        }
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_self );
            tmp_called_instance_3 = par_self;
            CHECK_OBJECT( par_addrs );
            tmp_args_element_name_1 = par_addrs;
            frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 150;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_try_connect, call_args );
            }

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 150;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_timeout );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_3 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_source_name_5;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_source_name_6;
                CHECK_OBJECT( par_self );
                tmp_source_name_5 = par_self;
                tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_io_loop );
                if ( tmp_source_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_6;
                }
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_remove_timeout );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_6;
                }
                CHECK_OBJECT( par_self );
                tmp_source_name_6 = par_self;
                tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_timeout );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 154;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_6;
                }
                frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 154;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_6;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            {
                PyObject *tmp_called_instance_4;
                PyObject *tmp_call_result_4;
                CHECK_OBJECT( par_self );
                tmp_called_instance_4 = par_self;
                frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 155;
                tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_on_timeout );
                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 155;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_6;
                }
                Py_DECREF( tmp_call_result_4 );
            }
            branch_no_3:;
        }
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_5_on_connect_done );
        return NULL;
        // Return handler code:
        try_return_handler_6:;
        Py_XDECREF( var_e );
        var_e = NULL;

        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 142;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame) frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooo";
        goto try_except_handler_5;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_5_on_connect_done );
    return NULL;
    // Return handler code:
    try_return_handler_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_3:;
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_call_result_5;
        CHECK_OBJECT( par_self );
        tmp_called_instance_5 = par_self;
        frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 157;
        tmp_call_result_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_clear_timeouts );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_6;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_future );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 158;
        tmp_call_result_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_done );
        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_6 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_6 );

            exception_lineno = 158;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_6 );
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_instance_7;
            PyObject *tmp_call_result_7;
            CHECK_OBJECT( var_stream );
            tmp_called_instance_7 = var_stream;
            frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 160;
            tmp_call_result_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_close );
            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        goto branch_end_4;
        branch_no_4:;
        {
            PyObject *tmp_called_instance_8;
            PyObject *tmp_source_name_8;
            PyObject *tmp_call_result_8;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_8 = par_self;
            tmp_called_instance_8 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_streams );
            if ( tmp_called_instance_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_stream );
            tmp_args_element_name_3 = var_stream;
            frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 162;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_discard, call_args );
            }

            Py_DECREF( tmp_called_instance_8 );
            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        {
            PyObject *tmp_called_instance_9;
            PyObject *tmp_source_name_9;
            PyObject *tmp_call_result_9;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_9 = par_self;
            tmp_called_instance_9 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_future );
            if ( tmp_called_instance_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 163;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_af );
            tmp_tuple_element_1 = par_af;
            tmp_args_element_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_element_name_4, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( par_addr );
            tmp_tuple_element_1 = par_addr;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_element_name_4, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( var_stream );
            tmp_tuple_element_1 = var_stream;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_element_name_4, 2, tmp_tuple_element_1 );
            frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 163;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_set_result, call_args );
            }

            Py_DECREF( tmp_called_instance_9 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 163;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_9 );
        }
        {
            PyObject *tmp_called_instance_10;
            PyObject *tmp_call_result_10;
            CHECK_OBJECT( par_self );
            tmp_called_instance_10 = par_self;
            frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame.f_lineno = 164;
            tmp_call_result_10 = CALL_METHOD_NO_ARGS( tmp_called_instance_10, const_str_plain_close_streams );
            if ( tmp_call_result_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_10 );
        }
        branch_end_4:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8ad4a72b5abeb6760f8bd6620dc8fb80, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8ad4a72b5abeb6760f8bd6620dc8fb80->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8ad4a72b5abeb6760f8bd6620dc8fb80, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8ad4a72b5abeb6760f8bd6620dc8fb80,
        type_description_1,
        par_self,
        par_addrs,
        par_af,
        par_addr,
        par_future,
        var_stream,
        var_e
    );


    // Release cached frame.
    if ( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 == cache_frame_8ad4a72b5abeb6760f8bd6620dc8fb80 )
    {
        Py_DECREF( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 );
    }
    cache_frame_8ad4a72b5abeb6760f8bd6620dc8fb80 = NULL;

    assertFrameObject( frame_8ad4a72b5abeb6760f8bd6620dc8fb80 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_5_on_connect_done );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_addrs );
    Py_DECREF( par_addrs );
    par_addrs = NULL;

    CHECK_OBJECT( (PyObject *)par_af );
    Py_DECREF( par_af );
    par_af = NULL;

    CHECK_OBJECT( (PyObject *)par_addr );
    Py_DECREF( par_addr );
    par_addr = NULL;

    CHECK_OBJECT( (PyObject *)par_future );
    Py_DECREF( par_future );
    par_future = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_addrs );
    Py_DECREF( par_addrs );
    par_addrs = NULL;

    CHECK_OBJECT( (PyObject *)par_af );
    Py_DECREF( par_af );
    par_af = NULL;

    CHECK_OBJECT( (PyObject *)par_addr );
    Py_DECREF( par_addr );
    par_addr = NULL;

    CHECK_OBJECT( (PyObject *)par_future );
    Py_DECREF( par_future );
    par_future = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_5_on_connect_done );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_6_set_timeout( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_timeout = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_390b695c20ddbaaf72df3d190f411f82;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_390b695c20ddbaaf72df3d190f411f82 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_390b695c20ddbaaf72df3d190f411f82, codeobj_390b695c20ddbaaf72df3d190f411f82, module_tornado$tcpclient, sizeof(void *)+sizeof(void *) );
    frame_390b695c20ddbaaf72df3d190f411f82 = cache_frame_390b695c20ddbaaf72df3d190f411f82;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_390b695c20ddbaaf72df3d190f411f82 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_390b695c20ddbaaf72df3d190f411f82 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_io_loop );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add_timeout );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_io_loop );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 168;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_390b695c20ddbaaf72df3d190f411f82->m_frame.f_lineno = 168;
        tmp_left_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_time );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 168;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_timeout );
        tmp_right_name_1 = par_timeout;
        tmp_args_element_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 168;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_on_timeout );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 168;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_390b695c20ddbaaf72df3d190f411f82->m_frame.f_lineno = 167;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_timeout, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_390b695c20ddbaaf72df3d190f411f82 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_390b695c20ddbaaf72df3d190f411f82 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_390b695c20ddbaaf72df3d190f411f82, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_390b695c20ddbaaf72df3d190f411f82->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_390b695c20ddbaaf72df3d190f411f82, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_390b695c20ddbaaf72df3d190f411f82,
        type_description_1,
        par_self,
        par_timeout
    );


    // Release cached frame.
    if ( frame_390b695c20ddbaaf72df3d190f411f82 == cache_frame_390b695c20ddbaaf72df3d190f411f82 )
    {
        Py_DECREF( frame_390b695c20ddbaaf72df3d190f411f82 );
    }
    cache_frame_390b695c20ddbaaf72df3d190f411f82 = NULL;

    assertFrameObject( frame_390b695c20ddbaaf72df3d190f411f82 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_6_set_timeout );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_timeout );
    Py_DECREF( par_timeout );
    par_timeout = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_timeout );
    Py_DECREF( par_timeout );
    par_timeout = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_6_set_timeout );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_7_on_timeout( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1294c5e4eefa4f961a3eff38dd5d48bc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_1294c5e4eefa4f961a3eff38dd5d48bc = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1294c5e4eefa4f961a3eff38dd5d48bc, codeobj_1294c5e4eefa4f961a3eff38dd5d48bc, module_tornado$tcpclient, sizeof(void *) );
    frame_1294c5e4eefa4f961a3eff38dd5d48bc = cache_frame_1294c5e4eefa4f961a3eff38dd5d48bc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1294c5e4eefa4f961a3eff38dd5d48bc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1294c5e4eefa4f961a3eff38dd5d48bc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_timeout, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_future );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_1294c5e4eefa4f961a3eff38dd5d48bc->m_frame.f_lineno = 173;
        tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_done );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_try_connect );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_secondary_addrs );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 174;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 174;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_1294c5e4eefa4f961a3eff38dd5d48bc->m_frame.f_lineno = 174;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1294c5e4eefa4f961a3eff38dd5d48bc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1294c5e4eefa4f961a3eff38dd5d48bc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1294c5e4eefa4f961a3eff38dd5d48bc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1294c5e4eefa4f961a3eff38dd5d48bc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1294c5e4eefa4f961a3eff38dd5d48bc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1294c5e4eefa4f961a3eff38dd5d48bc,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_1294c5e4eefa4f961a3eff38dd5d48bc == cache_frame_1294c5e4eefa4f961a3eff38dd5d48bc )
    {
        Py_DECREF( frame_1294c5e4eefa4f961a3eff38dd5d48bc );
    }
    cache_frame_1294c5e4eefa4f961a3eff38dd5d48bc = NULL;

    assertFrameObject( frame_1294c5e4eefa4f961a3eff38dd5d48bc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_7_on_timeout );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_7_on_timeout );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_8_clear_timeout( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6e221219ab71e145074fd5a56615b3ef;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6e221219ab71e145074fd5a56615b3ef = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6e221219ab71e145074fd5a56615b3ef, codeobj_6e221219ab71e145074fd5a56615b3ef, module_tornado$tcpclient, sizeof(void *) );
    frame_6e221219ab71e145074fd5a56615b3ef = cache_frame_6e221219ab71e145074fd5a56615b3ef;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6e221219ab71e145074fd5a56615b3ef );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6e221219ab71e145074fd5a56615b3ef ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_timeout );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_io_loop );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 178;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_remove_timeout );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 178;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_timeout );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 178;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_6e221219ab71e145074fd5a56615b3ef->m_frame.f_lineno = 178;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 178;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e221219ab71e145074fd5a56615b3ef );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e221219ab71e145074fd5a56615b3ef );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6e221219ab71e145074fd5a56615b3ef, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6e221219ab71e145074fd5a56615b3ef->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6e221219ab71e145074fd5a56615b3ef, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6e221219ab71e145074fd5a56615b3ef,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_6e221219ab71e145074fd5a56615b3ef == cache_frame_6e221219ab71e145074fd5a56615b3ef )
    {
        Py_DECREF( frame_6e221219ab71e145074fd5a56615b3ef );
    }
    cache_frame_6e221219ab71e145074fd5a56615b3ef = NULL;

    assertFrameObject( frame_6e221219ab71e145074fd5a56615b3ef );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_8_clear_timeout );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_8_clear_timeout );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_9_set_connect_timeout( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_connect_timeout = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_8e0a19119d5caf5484e0b42a72c69ed6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_8e0a19119d5caf5484e0b42a72c69ed6 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8e0a19119d5caf5484e0b42a72c69ed6, codeobj_8e0a19119d5caf5484e0b42a72c69ed6, module_tornado$tcpclient, sizeof(void *)+sizeof(void *) );
    frame_8e0a19119d5caf5484e0b42a72c69ed6 = cache_frame_8e0a19119d5caf5484e0b42a72c69ed6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8e0a19119d5caf5484e0b42a72c69ed6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8e0a19119d5caf5484e0b42a72c69ed6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_io_loop );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add_timeout );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_connect_timeout );
        tmp_args_element_name_1 = par_connect_timeout;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_on_connect_timeout );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 184;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_8e0a19119d5caf5484e0b42a72c69ed6->m_frame.f_lineno = 183;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_connect_timeout, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e0a19119d5caf5484e0b42a72c69ed6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e0a19119d5caf5484e0b42a72c69ed6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8e0a19119d5caf5484e0b42a72c69ed6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8e0a19119d5caf5484e0b42a72c69ed6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8e0a19119d5caf5484e0b42a72c69ed6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8e0a19119d5caf5484e0b42a72c69ed6,
        type_description_1,
        par_self,
        par_connect_timeout
    );


    // Release cached frame.
    if ( frame_8e0a19119d5caf5484e0b42a72c69ed6 == cache_frame_8e0a19119d5caf5484e0b42a72c69ed6 )
    {
        Py_DECREF( frame_8e0a19119d5caf5484e0b42a72c69ed6 );
    }
    cache_frame_8e0a19119d5caf5484e0b42a72c69ed6 = NULL;

    assertFrameObject( frame_8e0a19119d5caf5484e0b42a72c69ed6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_9_set_connect_timeout );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_connect_timeout );
    Py_DECREF( par_connect_timeout );
    par_connect_timeout = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_connect_timeout );
    Py_DECREF( par_connect_timeout );
    par_connect_timeout = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_9_set_connect_timeout );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_10_on_connect_timeout( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_47c6a2ad74230fcb9dcc1362760e0766;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_47c6a2ad74230fcb9dcc1362760e0766 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_47c6a2ad74230fcb9dcc1362760e0766, codeobj_47c6a2ad74230fcb9dcc1362760e0766, module_tornado$tcpclient, sizeof(void *) );
    frame_47c6a2ad74230fcb9dcc1362760e0766 = cache_frame_47c6a2ad74230fcb9dcc1362760e0766;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_47c6a2ad74230fcb9dcc1362760e0766 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_47c6a2ad74230fcb9dcc1362760e0766 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_future );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_47c6a2ad74230fcb9dcc1362760e0766->m_frame.f_lineno = 188;
        tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_done );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_future );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_set_exception );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_TimeoutError );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TimeoutError );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TimeoutError" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 189;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_1;
            frame_47c6a2ad74230fcb9dcc1362760e0766->m_frame.f_lineno = 189;
            tmp_args_element_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 189;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_47c6a2ad74230fcb9dcc1362760e0766->m_frame.f_lineno = 189;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        frame_47c6a2ad74230fcb9dcc1362760e0766->m_frame.f_lineno = 190;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_close_streams );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_47c6a2ad74230fcb9dcc1362760e0766 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_47c6a2ad74230fcb9dcc1362760e0766 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_47c6a2ad74230fcb9dcc1362760e0766, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_47c6a2ad74230fcb9dcc1362760e0766->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_47c6a2ad74230fcb9dcc1362760e0766, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_47c6a2ad74230fcb9dcc1362760e0766,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_47c6a2ad74230fcb9dcc1362760e0766 == cache_frame_47c6a2ad74230fcb9dcc1362760e0766 )
    {
        Py_DECREF( frame_47c6a2ad74230fcb9dcc1362760e0766 );
    }
    cache_frame_47c6a2ad74230fcb9dcc1362760e0766 = NULL;

    assertFrameObject( frame_47c6a2ad74230fcb9dcc1362760e0766 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_10_on_connect_timeout );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_10_on_connect_timeout );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_11_clear_timeouts( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_793f9e350275ba265bef7ec1b3f9d348;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_793f9e350275ba265bef7ec1b3f9d348 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_793f9e350275ba265bef7ec1b3f9d348, codeobj_793f9e350275ba265bef7ec1b3f9d348, module_tornado$tcpclient, sizeof(void *) );
    frame_793f9e350275ba265bef7ec1b3f9d348 = cache_frame_793f9e350275ba265bef7ec1b3f9d348;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_793f9e350275ba265bef7ec1b3f9d348 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_793f9e350275ba265bef7ec1b3f9d348 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_timeout );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_io_loop );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 194;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_remove_timeout );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 194;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_timeout );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 194;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_793f9e350275ba265bef7ec1b3f9d348->m_frame.f_lineno = 194;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 194;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_connect_timeout );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 195;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_6;
            PyObject *tmp_source_name_7;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_io_loop );
            if ( tmp_source_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_remove_timeout );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_8 = par_self;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_connect_timeout );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 196;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_793f9e350275ba265bef7ec1b3f9d348->m_frame.f_lineno = 196;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_793f9e350275ba265bef7ec1b3f9d348 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_793f9e350275ba265bef7ec1b3f9d348 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_793f9e350275ba265bef7ec1b3f9d348, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_793f9e350275ba265bef7ec1b3f9d348->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_793f9e350275ba265bef7ec1b3f9d348, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_793f9e350275ba265bef7ec1b3f9d348,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_793f9e350275ba265bef7ec1b3f9d348 == cache_frame_793f9e350275ba265bef7ec1b3f9d348 )
    {
        Py_DECREF( frame_793f9e350275ba265bef7ec1b3f9d348 );
    }
    cache_frame_793f9e350275ba265bef7ec1b3f9d348 = NULL;

    assertFrameObject( frame_793f9e350275ba265bef7ec1b3f9d348 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_11_clear_timeouts );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_11_clear_timeouts );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_12_close_streams( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_stream = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_9337544eeae4fefac5b0f2e4de7c8d8e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_9337544eeae4fefac5b0f2e4de7c8d8e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9337544eeae4fefac5b0f2e4de7c8d8e, codeobj_9337544eeae4fefac5b0f2e4de7c8d8e, module_tornado$tcpclient, sizeof(void *)+sizeof(void *) );
    frame_9337544eeae4fefac5b0f2e4de7c8d8e = cache_frame_9337544eeae4fefac5b0f2e4de7c8d8e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9337544eeae4fefac5b0f2e4de7c8d8e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9337544eeae4fefac5b0f2e4de7c8d8e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_streams );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oo";
                exception_lineno = 199;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_stream;
            var_stream = tmp_assign_source_3;
            Py_INCREF( var_stream );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_stream );
        tmp_called_instance_1 = var_stream;
        frame_9337544eeae4fefac5b0f2e4de7c8d8e->m_frame.f_lineno = 200;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_close );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 199;
        type_description_1 = "oo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9337544eeae4fefac5b0f2e4de7c8d8e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9337544eeae4fefac5b0f2e4de7c8d8e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9337544eeae4fefac5b0f2e4de7c8d8e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9337544eeae4fefac5b0f2e4de7c8d8e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9337544eeae4fefac5b0f2e4de7c8d8e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9337544eeae4fefac5b0f2e4de7c8d8e,
        type_description_1,
        par_self,
        var_stream
    );


    // Release cached frame.
    if ( frame_9337544eeae4fefac5b0f2e4de7c8d8e == cache_frame_9337544eeae4fefac5b0f2e4de7c8d8e )
    {
        Py_DECREF( frame_9337544eeae4fefac5b0f2e4de7c8d8e );
    }
    cache_frame_9337544eeae4fefac5b0f2e4de7c8d8e = NULL;

    assertFrameObject( frame_9337544eeae4fefac5b0f2e4de7c8d8e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_12_close_streams );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_12_close_streams );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_13___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_resolver = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_d68f8ca13a9673b9114ba90152a28042;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d68f8ca13a9673b9114ba90152a28042 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d68f8ca13a9673b9114ba90152a28042, codeobj_d68f8ca13a9673b9114ba90152a28042, module_tornado$tcpclient, sizeof(void *)+sizeof(void *) );
    frame_d68f8ca13a9673b9114ba90152a28042 = cache_frame_d68f8ca13a9673b9114ba90152a28042;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d68f8ca13a9673b9114ba90152a28042 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d68f8ca13a9673b9114ba90152a28042 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_resolver );
        tmp_compexpr_left_1 = par_resolver;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( par_resolver );
            tmp_assattr_name_1 = par_resolver;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_resolver, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            tmp_assattr_name_2 = Py_False;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_2 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__own_resolver, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 213;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assattr_name_3;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_assattr_target_3;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Resolver );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Resolver );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Resolver" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 215;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            frame_d68f8ca13a9673b9114ba90152a28042->m_frame.f_lineno = 215;
            tmp_assattr_name_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_assattr_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_3 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_resolver, tmp_assattr_name_3 );
            Py_DECREF( tmp_assattr_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_assattr_name_4;
            PyObject *tmp_assattr_target_4;
            tmp_assattr_name_4 = Py_True;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_4 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__own_resolver, tmp_assattr_name_4 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d68f8ca13a9673b9114ba90152a28042 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d68f8ca13a9673b9114ba90152a28042 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d68f8ca13a9673b9114ba90152a28042, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d68f8ca13a9673b9114ba90152a28042->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d68f8ca13a9673b9114ba90152a28042, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d68f8ca13a9673b9114ba90152a28042,
        type_description_1,
        par_self,
        par_resolver
    );


    // Release cached frame.
    if ( frame_d68f8ca13a9673b9114ba90152a28042 == cache_frame_d68f8ca13a9673b9114ba90152a28042 )
    {
        Py_DECREF( frame_d68f8ca13a9673b9114ba90152a28042 );
    }
    cache_frame_d68f8ca13a9673b9114ba90152a28042 = NULL;

    assertFrameObject( frame_d68f8ca13a9673b9114ba90152a28042 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_13___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_resolver );
    Py_DECREF( par_resolver );
    par_resolver = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_resolver );
    Py_DECREF( par_resolver );
    par_resolver = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_13___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_14_close( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b84f5a07a552cb21cef2f0fdfdb94933;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b84f5a07a552cb21cef2f0fdfdb94933 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b84f5a07a552cb21cef2f0fdfdb94933, codeobj_b84f5a07a552cb21cef2f0fdfdb94933, module_tornado$tcpclient, sizeof(void *) );
    frame_b84f5a07a552cb21cef2f0fdfdb94933 = cache_frame_b84f5a07a552cb21cef2f0fdfdb94933;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b84f5a07a552cb21cef2f0fdfdb94933 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b84f5a07a552cb21cef2f0fdfdb94933 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__own_resolver );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 219;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_resolver );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 220;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_b84f5a07a552cb21cef2f0fdfdb94933->m_frame.f_lineno = 220;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_close );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 220;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b84f5a07a552cb21cef2f0fdfdb94933 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b84f5a07a552cb21cef2f0fdfdb94933 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b84f5a07a552cb21cef2f0fdfdb94933, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b84f5a07a552cb21cef2f0fdfdb94933->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b84f5a07a552cb21cef2f0fdfdb94933, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b84f5a07a552cb21cef2f0fdfdb94933,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_b84f5a07a552cb21cef2f0fdfdb94933 == cache_frame_b84f5a07a552cb21cef2f0fdfdb94933 )
    {
        Py_DECREF( frame_b84f5a07a552cb21cef2f0fdfdb94933 );
    }
    cache_frame_b84f5a07a552cb21cef2f0fdfdb94933 = NULL;

    assertFrameObject( frame_b84f5a07a552cb21cef2f0fdfdb94933 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_14_close );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_14_close );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$tcpclient$$$function_15_connect( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_host = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_port = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_af = PyCell_NEW1( python_pars[ 3 ] );
    struct Nuitka_CellObject *par_ssl_options = PyCell_NEW1( python_pars[ 4 ] );
    struct Nuitka_CellObject *par_max_buffer_size = PyCell_NEW1( python_pars[ 5 ] );
    struct Nuitka_CellObject *par_source_ip = PyCell_NEW1( python_pars[ 6 ] );
    struct Nuitka_CellObject *par_source_port = PyCell_NEW1( python_pars[ 7 ] );
    struct Nuitka_CellObject *par_timeout = PyCell_NEW1( python_pars[ 8 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = par_af;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = par_host;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] = par_max_buffer_size;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] = par_port;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[3] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[4] = par_self;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[4] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[5] = par_source_ip;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[5] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[6] = par_source_port;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[6] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[7] = par_ssl_options;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[7] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[8] = par_timeout;
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[8] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_15_connect );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_host );
    Py_DECREF( par_host );
    par_host = NULL;

    CHECK_OBJECT( (PyObject *)par_port );
    Py_DECREF( par_port );
    par_port = NULL;

    CHECK_OBJECT( (PyObject *)par_af );
    Py_DECREF( par_af );
    par_af = NULL;

    CHECK_OBJECT( (PyObject *)par_ssl_options );
    Py_DECREF( par_ssl_options );
    par_ssl_options = NULL;

    CHECK_OBJECT( (PyObject *)par_max_buffer_size );
    Py_DECREF( par_max_buffer_size );
    par_max_buffer_size = NULL;

    CHECK_OBJECT( (PyObject *)par_source_ip );
    Py_DECREF( par_source_ip );
    par_source_ip = NULL;

    CHECK_OBJECT( (PyObject *)par_source_port );
    Py_DECREF( par_source_port );
    par_source_port = NULL;

    CHECK_OBJECT( (PyObject *)par_timeout );
    Py_DECREF( par_timeout );
    par_timeout = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_host );
    Py_DECREF( par_host );
    par_host = NULL;

    CHECK_OBJECT( (PyObject *)par_port );
    Py_DECREF( par_port );
    par_port = NULL;

    CHECK_OBJECT( (PyObject *)par_af );
    Py_DECREF( par_af );
    par_af = NULL;

    CHECK_OBJECT( (PyObject *)par_ssl_options );
    Py_DECREF( par_ssl_options );
    par_ssl_options = NULL;

    CHECK_OBJECT( (PyObject *)par_max_buffer_size );
    Py_DECREF( par_max_buffer_size );
    par_max_buffer_size = NULL;

    CHECK_OBJECT( (PyObject *)par_source_ip );
    Py_DECREF( par_source_ip );
    par_source_ip = NULL;

    CHECK_OBJECT( (PyObject *)par_source_port );
    Py_DECREF( par_source_port );
    par_source_port = NULL;

    CHECK_OBJECT( (PyObject *)par_timeout );
    Py_DECREF( par_timeout );
    par_timeout = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_15_connect );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_locals {
    PyObject *var_addrinfo;
    PyObject *var_connector;
    PyObject *var_addr;
    PyObject *var_stream;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__element_3;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *tmp_return_value;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_locals *coroutine_heap = (struct tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 5: goto yield_return_5;
    case 4: goto yield_return_4;
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->var_addrinfo = NULL;
    coroutine_heap->var_connector = NULL;
    coroutine_heap->var_addr = NULL;
    coroutine_heap->var_stream = NULL;
    coroutine_heap->tmp_tuple_unpack_1__element_1 = NULL;
    coroutine_heap->tmp_tuple_unpack_1__element_2 = NULL;
    coroutine_heap->tmp_tuple_unpack_1__element_3 = NULL;
    coroutine_heap->tmp_tuple_unpack_1__source_iter = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_767c3d3c0821babfafc5da5d82408a00, module_tornado$tcpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 258;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( coroutine->m_closure[8] );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 259;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_inst_1 = PyCell_GET( coroutine->m_closure[8] );
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_numbers );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_numbers );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "numbers" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 259;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Real );
            if ( tmp_isinstance_cls_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 259;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            coroutine_heap->tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            Py_DECREF( tmp_isinstance_cls_1 );
            if ( coroutine_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 259;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_right_name_1;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 260;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_2 = tmp_mvar_value_2;
                coroutine->m_frame->m_frame.f_lineno = 260;
                tmp_called_instance_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_current );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 260;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                coroutine->m_frame->m_frame.f_lineno = 260;
                tmp_left_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_time );
                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_left_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 260;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
                {
                    Py_DECREF( tmp_left_name_1 );
                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 260;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_right_name_1 = PyCell_GET( coroutine->m_closure[8] );
                tmp_assign_source_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_left_name_1 );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 260;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = PyCell_GET( coroutine->m_closure[8] );
                    PyCell_SET( coroutine->m_closure[8], tmp_assign_source_1 );
                    Py_XDECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_isinstance_inst_2;
                PyObject *tmp_isinstance_cls_2;
                PyObject *tmp_source_name_2;
                PyObject *tmp_mvar_value_3;
                if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
                {

                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 261;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_isinstance_inst_2 = PyCell_GET( coroutine->m_closure[8] );
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_datetime );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 261;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_2 = tmp_mvar_value_3;
                tmp_isinstance_cls_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_timedelta );
                if ( tmp_isinstance_cls_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 261;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                coroutine_heap->tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
                Py_DECREF( tmp_isinstance_cls_2 );
                if ( coroutine_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 261;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( coroutine_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_2;
                    PyObject *tmp_left_name_2;
                    PyObject *tmp_called_instance_3;
                    PyObject *tmp_called_instance_4;
                    PyObject *tmp_mvar_value_4;
                    PyObject *tmp_right_name_2;
                    PyObject *tmp_called_instance_5;
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

                    if (unlikely( tmp_mvar_value_4 == NULL ))
                    {
                        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
                    }

                    if ( tmp_mvar_value_4 == NULL )
                    {

                        coroutine_heap->exception_type = PyExc_NameError;
                        Py_INCREF( coroutine_heap->exception_type );
                        coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
                        coroutine_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                        CHAIN_EXCEPTION( coroutine_heap->exception_value );

                        coroutine_heap->exception_lineno = 262;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_instance_4 = tmp_mvar_value_4;
                    coroutine->m_frame->m_frame.f_lineno = 262;
                    tmp_called_instance_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_current );
                    if ( tmp_called_instance_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                        coroutine_heap->exception_lineno = 262;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }
                    coroutine->m_frame->m_frame.f_lineno = 262;
                    tmp_left_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_time );
                    Py_DECREF( tmp_called_instance_3 );
                    if ( tmp_left_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                        coroutine_heap->exception_lineno = 262;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }
                    if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
                    {
                        Py_DECREF( tmp_left_name_2 );
                        coroutine_heap->exception_type = PyExc_NameError;
                        Py_INCREF( coroutine_heap->exception_type );
                        coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                        coroutine_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                        CHAIN_EXCEPTION( coroutine_heap->exception_value );

                        coroutine_heap->exception_lineno = 262;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_instance_5 = PyCell_GET( coroutine->m_closure[8] );
                    coroutine->m_frame->m_frame.f_lineno = 262;
                    tmp_right_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_total_seconds );
                    if ( tmp_right_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                        Py_DECREF( tmp_left_name_2 );

                        coroutine_heap->exception_lineno = 262;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
                    Py_DECREF( tmp_left_name_2 );
                    Py_DECREF( tmp_right_name_2 );
                    if ( tmp_assign_source_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                        coroutine_heap->exception_lineno = 262;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = PyCell_GET( coroutine->m_closure[8] );
                        PyCell_SET( coroutine->m_closure[8], tmp_assign_source_2 );
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_3;
                branch_no_3:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_make_exception_arg_1;
                    PyObject *tmp_left_name_3;
                    PyObject *tmp_right_name_3;
                    tmp_left_name_3 = const_str_digest_c89da60025fa7fade027312fa03fc3ff;
                    if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
                    {

                        coroutine_heap->exception_type = PyExc_NameError;
                        Py_INCREF( coroutine_heap->exception_type );
                        coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                        coroutine_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                        CHAIN_EXCEPTION( coroutine_heap->exception_value );

                        coroutine_heap->exception_lineno = 264;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_right_name_3 = PyCell_GET( coroutine->m_closure[8] );
                    tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                    if ( tmp_make_exception_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                        coroutine_heap->exception_lineno = 264;
                        coroutine_heap->type_description_1 = "cccccccccoooo";
                        goto frame_exception_exit_1;
                    }
                    coroutine->m_frame->m_frame.f_lineno = 264;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_1 };
                        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                    }

                    Py_DECREF( tmp_make_exception_arg_1 );
                    assert( !(tmp_raise_type_1 == NULL) );
                    coroutine_heap->exception_type = tmp_raise_type_1;
                    coroutine_heap->exception_lineno = 264;
                    RAISE_EXCEPTION_WITH_TYPE( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 265;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_2 = PyCell_GET( coroutine->m_closure[8] );
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_4 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_expression_name_1;
            PyObject *tmp_expression_name_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            coroutine->m_frame->m_frame.f_lineno = 266;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_gen );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 266;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_5;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_with_timeout );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 266;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = PyCell_GET( coroutine->m_closure[8] );
            if ( PyCell_GET( coroutine->m_closure[4] ) == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_5 = PyCell_GET( coroutine->m_closure[4] );
            tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_resolver );
            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                Py_DECREF( tmp_called_name_1 );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_resolve );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                Py_DECREF( tmp_called_name_1 );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_called_name_2 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "host" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_3 = PyCell_GET( coroutine->m_closure[1] );
            if ( PyCell_GET( coroutine->m_closure[3] ) == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_called_name_2 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "port" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_4 = PyCell_GET( coroutine->m_closure[3] );
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_called_name_2 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "af" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_5 = PyCell_GET( coroutine->m_closure[0] );
            coroutine->m_frame->m_frame.f_lineno = 267;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                Py_DECREF( tmp_called_name_1 );

                coroutine_heap->exception_lineno = 267;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            coroutine->m_frame->m_frame.f_lineno = 266;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_expression_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_expression_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 266;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
            Py_DECREF( tmp_expression_name_2 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 266;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_mvar_value_5, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), NULL );
            coroutine->m_yield_return_index = 1;
            coroutine->m_yieldfrom = tmp_expression_name_1;
            coroutine->m_awaiting = true;
            return NULL;

            yield_return_1:
            Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_mvar_value_5, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), NULL );
            coroutine->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 266;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_3 = yield_return_value;
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 266;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            assert( coroutine_heap->var_addrinfo == NULL );
            coroutine_heap->var_addrinfo = tmp_assign_source_3;
        }
        goto branch_end_4;
        branch_no_4:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_expression_name_3;
            PyObject *tmp_expression_name_4;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_source_name_7;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            coroutine->m_frame->m_frame.f_lineno = 270;
            if ( PyCell_GET( coroutine->m_closure[4] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_7 = PyCell_GET( coroutine->m_closure[4] );
            tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_resolver );
            if ( tmp_source_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_resolve );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "host" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_6 = PyCell_GET( coroutine->m_closure[1] );
            if ( PyCell_GET( coroutine->m_closure[3] ) == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "port" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_7 = PyCell_GET( coroutine->m_closure[3] );
            if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "af" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_8 = PyCell_GET( coroutine->m_closure[0] );
            coroutine->m_frame->m_frame.f_lineno = 270;
            {
                PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
                tmp_expression_name_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            if ( tmp_expression_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            tmp_expression_name_3 = ASYNC_AWAIT( tmp_expression_name_4, await_normal );
            Py_DECREF( tmp_expression_name_4 );
            if ( tmp_expression_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_expression_name_4, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_source_name_6, sizeof(PyObject *), &tmp_source_name_7, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
            coroutine->m_yield_return_index = 2;
            coroutine->m_yieldfrom = tmp_expression_name_3;
            coroutine->m_awaiting = true;
            return NULL;

            yield_return_2:
            Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_expression_name_4, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_source_name_6, sizeof(PyObject *), &tmp_source_name_7, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
            coroutine->m_awaiting = false;

            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_4 = yield_return_value;
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                coroutine_heap->exception_lineno = 270;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }
            assert( coroutine_heap->var_addrinfo == NULL );
            coroutine_heap->var_addrinfo = tmp_assign_source_4;
        }
        branch_end_4:;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_9;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain__Connector );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Connector );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Connector" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 271;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_6;
        CHECK_OBJECT( coroutine_heap->var_addrinfo );
        tmp_args_element_name_9 = coroutine_heap->var_addrinfo;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 273;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_7;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_partial );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 273;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( coroutine->m_closure[4] ) == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 274;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = PyCell_GET( coroutine->m_closure[4] );
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__create_stream );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            Py_DECREF( tmp_called_name_5 );

            coroutine_heap->exception_lineno = 274;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        if ( PyCell_GET( coroutine->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "max_buffer_size" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 275;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = PyCell_GET( coroutine->m_closure[2] );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_source_ip;
        if ( PyCell_GET( coroutine->m_closure[5] ) == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "source_ip" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 276;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_1 = PyCell_GET( coroutine->m_closure[5] );
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(coroutine_heap->tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_source_port;
        if ( PyCell_GET( coroutine->m_closure[6] ) == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "source_port" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 277;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_2 = PyCell_GET( coroutine->m_closure[6] );
        coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(coroutine_heap->tmp_res != 0) );
        coroutine->m_frame->m_frame.f_lineno = 273;
        tmp_args_element_name_10 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 273;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }
        coroutine->m_frame->m_frame.f_lineno = 271;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 271;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }
        assert( coroutine_heap->var_connector == NULL );
        coroutine_heap->var_connector = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_expression_name_5;
        PyObject *tmp_expression_name_6;
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_10;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        coroutine->m_frame->m_frame.f_lineno = 280;
        CHECK_OBJECT( coroutine_heap->var_connector );
        tmp_source_name_10 = coroutine_heap->var_connector;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_start );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 280;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto try_except_handler_2;
        }
        tmp_dict_key_3 = const_str_plain_connect_timeout;
        if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 280;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto try_except_handler_2;
        }

        tmp_dict_value_3 = PyCell_GET( coroutine->m_closure[8] );
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(coroutine_heap->tmp_res != 0) );
        coroutine->m_frame->m_frame.f_lineno = 280;
        tmp_expression_name_6 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_6, tmp_kw_name_2 );
        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_expression_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 280;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto try_except_handler_2;
        }
        tmp_expression_name_5 = ASYNC_AWAIT( tmp_expression_name_6, await_normal );
        Py_DECREF( tmp_expression_name_6 );
        if ( tmp_expression_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 280;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_assign_source_6, sizeof(PyObject *), &tmp_expression_name_6, sizeof(PyObject *), &tmp_called_name_6, sizeof(PyObject *), &tmp_source_name_10, sizeof(PyObject *), &tmp_kw_name_2, sizeof(PyObject *), &tmp_dict_key_3, sizeof(PyObject *), &tmp_dict_value_3, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 3;
        coroutine->m_yieldfrom = tmp_expression_name_5;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_3:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_assign_source_6, sizeof(PyObject *), &tmp_expression_name_6, sizeof(PyObject *), &tmp_called_name_6, sizeof(PyObject *), &tmp_source_name_10, sizeof(PyObject *), &tmp_kw_name_2, sizeof(PyObject *), &tmp_dict_key_3, sizeof(PyObject *), &tmp_dict_value_3, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 280;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto try_except_handler_2;
        }
        tmp_iter_arg_1 = yield_return_value;
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 280;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 280;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto try_except_handler_2;
        }
        assert( coroutine_heap->tmp_tuple_unpack_1__source_iter == NULL );
        coroutine_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( coroutine_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = coroutine_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                coroutine_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = NULL;
                coroutine_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            }


            coroutine_heap->type_description_1 = "cccccccccoooo";
            coroutine_heap->exception_lineno = 280;
            goto try_except_handler_3;
        }
        assert( coroutine_heap->tmp_tuple_unpack_1__element_1 == NULL );
        coroutine_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( coroutine_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = coroutine_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                coroutine_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = NULL;
                coroutine_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            }


            coroutine_heap->type_description_1 = "cccccccccoooo";
            coroutine_heap->exception_lineno = 280;
            goto try_except_handler_3;
        }
        assert( coroutine_heap->tmp_tuple_unpack_1__element_2 == NULL );
        coroutine_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( coroutine_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = coroutine_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                coroutine_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = NULL;
                coroutine_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            }


            coroutine_heap->type_description_1 = "cccccccccoooo";
            coroutine_heap->exception_lineno = 280;
            goto try_except_handler_3;
        }
        assert( coroutine_heap->tmp_tuple_unpack_1__element_3 == NULL );
        coroutine_heap->tmp_tuple_unpack_1__element_3 = tmp_assign_source_9;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( coroutine_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = coroutine_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        coroutine_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( coroutine_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    coroutine_heap->exception_lineno = 280;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( coroutine_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );

            coroutine_heap->type_description_1 = "cccccccccoooo";
            coroutine_heap->exception_lineno = 280;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    coroutine_heap->exception_keeper_type_1 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_1 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_1 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_1 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( coroutine_heap->tmp_tuple_unpack_1__source_iter );
    coroutine_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_1;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_1;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_1;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    coroutine_heap->exception_keeper_type_2 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_2 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_2 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_2 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->tmp_tuple_unpack_1__element_1 );
    coroutine_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( coroutine_heap->tmp_tuple_unpack_1__element_2 );
    coroutine_heap->tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( coroutine_heap->tmp_tuple_unpack_1__element_3 );
    coroutine_heap->tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_2;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_2;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_2;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( coroutine_heap->tmp_tuple_unpack_1__source_iter );
    coroutine_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( coroutine_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_10 = coroutine_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = PyCell_GET( coroutine->m_closure[0] );
            PyCell_SET( coroutine->m_closure[0], tmp_assign_source_10 );
            Py_INCREF( tmp_assign_source_10 );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( coroutine_heap->tmp_tuple_unpack_1__element_1 );
    coroutine_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( coroutine_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_11 = coroutine_heap->tmp_tuple_unpack_1__element_2;
        assert( coroutine_heap->var_addr == NULL );
        Py_INCREF( tmp_assign_source_11 );
        coroutine_heap->var_addr = tmp_assign_source_11;
    }
    Py_XDECREF( coroutine_heap->tmp_tuple_unpack_1__element_2 );
    coroutine_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( coroutine_heap->tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_12 = coroutine_heap->tmp_tuple_unpack_1__element_3;
        assert( coroutine_heap->var_stream == NULL );
        Py_INCREF( tmp_assign_source_12 );
        coroutine_heap->var_stream = tmp_assign_source_12;
    }
    Py_XDECREF( coroutine_heap->tmp_tuple_unpack_1__element_3 );
    coroutine_heap->tmp_tuple_unpack_1__element_3 = NULL;

    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        if ( PyCell_GET( coroutine->m_closure[7] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ssl_options" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 284;
            coroutine_heap->type_description_1 = "cccccccccoooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_3 = PyCell_GET( coroutine->m_closure[7] );
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_5 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
            {

                coroutine_heap->exception_type = PyExc_NameError;
                Py_INCREF( coroutine_heap->exception_type );
                coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                coroutine_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                CHAIN_EXCEPTION( coroutine_heap->exception_value );

                coroutine_heap->exception_lineno = 285;
                coroutine_heap->type_description_1 = "cccccccccoooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_4 = PyCell_GET( coroutine->m_closure[8] );
            tmp_compexpr_right_4 = Py_None;
            tmp_condition_result_6 = ( tmp_compexpr_left_4 != tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assign_source_13;
                PyObject *tmp_expression_name_7;
                PyObject *tmp_expression_name_8;
                PyObject *tmp_called_name_7;
                PyObject *tmp_source_name_11;
                PyObject *tmp_mvar_value_8;
                PyObject *tmp_args_element_name_11;
                PyObject *tmp_args_element_name_12;
                PyObject *tmp_called_name_8;
                PyObject *tmp_source_name_12;
                PyObject *tmp_args_name_2;
                PyObject *tmp_kw_name_3;
                PyObject *tmp_dict_key_4;
                PyObject *tmp_dict_value_4;
                PyObject *tmp_dict_key_5;
                PyObject *tmp_dict_value_5;
                coroutine->m_frame->m_frame.f_lineno = 286;
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_gen );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 286;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_11 = tmp_mvar_value_8;
                tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_with_timeout );
                if ( tmp_called_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 286;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                if ( PyCell_GET( coroutine->m_closure[8] ) == NULL )
                {
                    Py_DECREF( tmp_called_name_7 );
                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "timeout" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 287;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_args_element_name_11 = PyCell_GET( coroutine->m_closure[8] );
                CHECK_OBJECT( coroutine_heap->var_stream );
                tmp_source_name_12 = coroutine_heap->var_stream;
                tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_start_tls );
                if ( tmp_called_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    Py_DECREF( tmp_called_name_7 );

                    coroutine_heap->exception_lineno = 288;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_name_2 = const_tuple_false_tuple;
                tmp_dict_key_4 = const_str_plain_ssl_options;
                if ( PyCell_GET( coroutine->m_closure[7] ) == NULL )
                {
                    Py_DECREF( tmp_called_name_7 );
                    Py_DECREF( tmp_called_name_8 );
                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ssl_options" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 289;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_dict_value_4 = PyCell_GET( coroutine->m_closure[7] );
                tmp_kw_name_3 = _PyDict_NewPresized( 2 );
                coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_4, tmp_dict_value_4 );
                assert( !(coroutine_heap->tmp_res != 0) );
                tmp_dict_key_5 = const_str_plain_server_hostname;
                if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
                {
                    Py_DECREF( tmp_called_name_7 );
                    Py_DECREF( tmp_called_name_8 );
                    Py_DECREF( tmp_kw_name_3 );
                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "host" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 289;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_dict_value_5 = PyCell_GET( coroutine->m_closure[1] );
                coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_5, tmp_dict_value_5 );
                assert( !(coroutine_heap->tmp_res != 0) );
                coroutine->m_frame->m_frame.f_lineno = 288;
                tmp_args_element_name_12 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_2, tmp_kw_name_3 );
                Py_DECREF( tmp_called_name_8 );
                Py_DECREF( tmp_kw_name_3 );
                if ( tmp_args_element_name_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    Py_DECREF( tmp_called_name_7 );

                    coroutine_heap->exception_lineno = 288;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                coroutine->m_frame->m_frame.f_lineno = 286;
                {
                    PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12 };
                    tmp_expression_name_8 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
                }

                Py_DECREF( tmp_called_name_7 );
                Py_DECREF( tmp_args_element_name_12 );
                if ( tmp_expression_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 286;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                tmp_expression_name_7 = ASYNC_AWAIT( tmp_expression_name_8, await_normal );
                Py_DECREF( tmp_expression_name_8 );
                if ( tmp_expression_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 286;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_6, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_expression_name_8, sizeof(PyObject *), &tmp_called_name_7, sizeof(PyObject *), &tmp_source_name_11, sizeof(PyObject *), &tmp_mvar_value_8, sizeof(PyObject *), &tmp_args_element_name_11, sizeof(PyObject *), &tmp_args_element_name_12, sizeof(PyObject *), &tmp_called_name_8, sizeof(PyObject *), &tmp_source_name_12, sizeof(PyObject *), &tmp_args_name_2, sizeof(PyObject *), &tmp_kw_name_3, sizeof(PyObject *), &tmp_dict_key_4, sizeof(PyObject *), &tmp_dict_value_4, sizeof(PyObject *), &tmp_dict_key_5, sizeof(PyObject *), &tmp_dict_value_5, sizeof(PyObject *), NULL );
                coroutine->m_yield_return_index = 4;
                coroutine->m_yieldfrom = tmp_expression_name_7;
                coroutine->m_awaiting = true;
                return NULL;

                yield_return_4:
                Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_6, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_expression_name_8, sizeof(PyObject *), &tmp_called_name_7, sizeof(PyObject *), &tmp_source_name_11, sizeof(PyObject *), &tmp_mvar_value_8, sizeof(PyObject *), &tmp_args_element_name_11, sizeof(PyObject *), &tmp_args_element_name_12, sizeof(PyObject *), &tmp_called_name_8, sizeof(PyObject *), &tmp_source_name_12, sizeof(PyObject *), &tmp_args_name_2, sizeof(PyObject *), &tmp_kw_name_3, sizeof(PyObject *), &tmp_dict_key_4, sizeof(PyObject *), &tmp_dict_value_4, sizeof(PyObject *), &tmp_dict_key_5, sizeof(PyObject *), &tmp_dict_value_5, sizeof(PyObject *), NULL );
                coroutine->m_awaiting = false;

                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 286;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_13 = yield_return_value;
                if ( tmp_assign_source_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 286;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = coroutine_heap->var_stream;
                    assert( old != NULL );
                    coroutine_heap->var_stream = tmp_assign_source_13;
                    Py_DECREF( old );
                }

            }
            goto branch_end_6;
            branch_no_6:;
            {
                PyObject *tmp_assign_source_14;
                PyObject *tmp_expression_name_9;
                PyObject *tmp_expression_name_10;
                PyObject *tmp_called_name_9;
                PyObject *tmp_source_name_13;
                PyObject *tmp_args_name_3;
                PyObject *tmp_kw_name_4;
                PyObject *tmp_dict_key_6;
                PyObject *tmp_dict_value_6;
                PyObject *tmp_dict_key_7;
                PyObject *tmp_dict_value_7;
                coroutine->m_frame->m_frame.f_lineno = 293;
                CHECK_OBJECT( coroutine_heap->var_stream );
                tmp_source_name_13 = coroutine_heap->var_stream;
                tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_start_tls );
                if ( tmp_called_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 293;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_name_3 = const_tuple_false_tuple;
                tmp_dict_key_6 = const_str_plain_ssl_options;
                if ( PyCell_GET( coroutine->m_closure[7] ) == NULL )
                {
                    Py_DECREF( tmp_called_name_9 );
                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ssl_options" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 294;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_dict_value_6 = PyCell_GET( coroutine->m_closure[7] );
                tmp_kw_name_4 = _PyDict_NewPresized( 2 );
                coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_6, tmp_dict_value_6 );
                assert( !(coroutine_heap->tmp_res != 0) );
                tmp_dict_key_7 = const_str_plain_server_hostname;
                if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
                {
                    Py_DECREF( tmp_called_name_9 );
                    Py_DECREF( tmp_kw_name_4 );
                    coroutine_heap->exception_type = PyExc_NameError;
                    Py_INCREF( coroutine_heap->exception_type );
                    coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "host" );
                    coroutine_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
                    CHAIN_EXCEPTION( coroutine_heap->exception_value );

                    coroutine_heap->exception_lineno = 294;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }

                tmp_dict_value_7 = PyCell_GET( coroutine->m_closure[1] );
                coroutine_heap->tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_7, tmp_dict_value_7 );
                assert( !(coroutine_heap->tmp_res != 0) );
                coroutine->m_frame->m_frame.f_lineno = 293;
                tmp_expression_name_10 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_3, tmp_kw_name_4 );
                Py_DECREF( tmp_called_name_9 );
                Py_DECREF( tmp_kw_name_4 );
                if ( tmp_expression_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 293;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                tmp_expression_name_9 = ASYNC_AWAIT( tmp_expression_name_10, await_normal );
                Py_DECREF( tmp_expression_name_10 );
                if ( tmp_expression_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 293;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_6, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_expression_name_10, sizeof(PyObject *), &tmp_called_name_9, sizeof(PyObject *), &tmp_source_name_13, sizeof(PyObject *), &tmp_args_name_3, sizeof(PyObject *), &tmp_kw_name_4, sizeof(PyObject *), &tmp_dict_key_6, sizeof(PyObject *), &tmp_dict_value_6, sizeof(PyObject *), &tmp_dict_key_7, sizeof(PyObject *), &tmp_dict_value_7, sizeof(PyObject *), NULL );
                coroutine->m_yield_return_index = 5;
                coroutine->m_yieldfrom = tmp_expression_name_9;
                coroutine->m_awaiting = true;
                return NULL;

                yield_return_5:
                Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_6, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_expression_name_10, sizeof(PyObject *), &tmp_called_name_9, sizeof(PyObject *), &tmp_source_name_13, sizeof(PyObject *), &tmp_args_name_3, sizeof(PyObject *), &tmp_kw_name_4, sizeof(PyObject *), &tmp_dict_key_6, sizeof(PyObject *), &tmp_dict_value_6, sizeof(PyObject *), &tmp_dict_key_7, sizeof(PyObject *), &tmp_dict_value_7, sizeof(PyObject *), NULL );
                coroutine->m_awaiting = false;

                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 293;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_14 = yield_return_value;
                if ( tmp_assign_source_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


                    coroutine_heap->exception_lineno = 293;
                    coroutine_heap->type_description_1 = "cccccccccoooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = coroutine_heap->var_stream;
                    assert( old != NULL );
                    coroutine_heap->var_stream = tmp_assign_source_14;
                    Py_DECREF( old );
                }

            }
            branch_end_6:;
        }
        branch_no_5:;
    }
    if ( coroutine_heap->var_stream == NULL )
    {

        coroutine_heap->exception_type = PyExc_UnboundLocalError;
        Py_INCREF( coroutine_heap->exception_type );
        coroutine_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "stream" );
        coroutine_heap->exception_tb = NULL;
        NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
        CHAIN_EXCEPTION( coroutine_heap->exception_value );

        coroutine_heap->exception_lineno = 296;
        coroutine_heap->type_description_1 = "cccccccccoooo";
        goto frame_exception_exit_1;
    }

    coroutine_heap->tmp_return_value = coroutine_heap->var_stream;
    Py_INCREF( coroutine_heap->tmp_return_value );
    goto frame_return_exit_1;

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto try_return_handler_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[4],
            coroutine->m_closure[1],
            coroutine->m_closure[3],
            coroutine->m_closure[0],
            coroutine->m_closure[7],
            coroutine->m_closure[2],
            coroutine->m_closure[5],
            coroutine->m_closure[6],
            coroutine->m_closure[8],
            coroutine_heap->var_addrinfo,
            coroutine_heap->var_connector,
            coroutine_heap->var_addr,
            coroutine_heap->var_stream
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)coroutine_heap->var_addrinfo );
    Py_DECREF( coroutine_heap->var_addrinfo );
    coroutine_heap->var_addrinfo = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_connector );
    Py_DECREF( coroutine_heap->var_connector );
    coroutine_heap->var_connector = NULL;

    CHECK_OBJECT( (PyObject *)coroutine_heap->var_addr );
    Py_DECREF( coroutine_heap->var_addr );
    coroutine_heap->var_addr = NULL;

    Py_XDECREF( coroutine_heap->var_stream );
    coroutine_heap->var_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    coroutine_heap->exception_keeper_type_3 = coroutine_heap->exception_type;
    coroutine_heap->exception_keeper_value_3 = coroutine_heap->exception_value;
    coroutine_heap->exception_keeper_tb_3 = coroutine_heap->exception_tb;
    coroutine_heap->exception_keeper_lineno_3 = coroutine_heap->exception_lineno;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;

    Py_XDECREF( coroutine_heap->var_addrinfo );
    coroutine_heap->var_addrinfo = NULL;

    Py_XDECREF( coroutine_heap->var_connector );
    coroutine_heap->var_connector = NULL;

    Py_XDECREF( coroutine_heap->var_addr );
    coroutine_heap->var_addr = NULL;

    Py_XDECREF( coroutine_heap->var_stream );
    coroutine_heap->var_stream = NULL;

    // Re-raise.
    coroutine_heap->exception_type = coroutine_heap->exception_keeper_type_3;
    coroutine_heap->exception_value = coroutine_heap->exception_keeper_value_3;
    coroutine_heap->exception_tb = coroutine_heap->exception_keeper_tb_3;
    coroutine_heap->exception_lineno = coroutine_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_maker( void )
{
    return Nuitka_Coroutine_New(
        tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_context,
        module_tornado$tcpclient,
        const_str_plain_connect,
        const_str_digest_9c35a83a41df634bfe1f1aeb9487e326,
        codeobj_767c3d3c0821babfafc5da5d82408a00,
        9,
        sizeof(struct tornado$tcpclient$$$function_15_connect$$$coroutine_1_connect_locals)
    );
}


static PyObject *impl_tornado$tcpclient$$$function_16__create_stream( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_max_buffer_size = python_pars[ 1 ];
    PyObject *par_af = python_pars[ 2 ];
    PyObject *par_addr = python_pars[ 3 ];
    PyObject *par_source_ip = python_pars[ 4 ];
    PyObject *par_source_port = python_pars[ 5 ];
    PyObject *var_source_port_bind = NULL;
    PyObject *var_source_ip_bind = NULL;
    PyObject *var_socket_obj = NULL;
    PyObject *var_stream = NULL;
    PyObject *var_e = NULL;
    PyObject *var_fu = NULL;
    struct Nuitka_FrameObject *frame_27460344c266d7b4afdcb1503875504d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_27460344c266d7b4afdcb1503875504d = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_27460344c266d7b4afdcb1503875504d, codeobj_27460344c266d7b4afdcb1503875504d, module_tornado$tcpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_27460344c266d7b4afdcb1503875504d = cache_frame_27460344c266d7b4afdcb1503875504d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_27460344c266d7b4afdcb1503875504d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_27460344c266d7b4afdcb1503875504d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_source_port );
        tmp_isinstance_inst_1 = par_source_port;
        tmp_isinstance_cls_1 = (PyObject *)&PyLong_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 308;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( par_source_port );
        tmp_assign_source_1 = par_source_port;
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_assign_source_1 = const_int_0;
        condexpr_end_1:;
        assert( var_source_port_bind == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_source_port_bind = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( par_source_ip );
        tmp_assign_source_2 = par_source_ip;
        assert( var_source_ip_bind == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_source_ip_bind = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_source_port_bind );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_source_port_bind );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_source_ip );
        tmp_operand_name_1 = par_source_ip;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            CHECK_OBJECT( par_af );
            tmp_compexpr_left_1 = par_af;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 313;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_AF_INET6 );
            if ( tmp_compexpr_right_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            Py_DECREF( tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 313;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_2;
            }
            else
            {
                goto condexpr_false_2;
            }
            condexpr_true_2:;
            tmp_assign_source_3 = const_str_digest_3f9fec55820f857461da4f1e12840da5;
            goto condexpr_end_2;
            condexpr_false_2:;
            tmp_assign_source_3 = const_str_digest_a78b083c1c6e4171ab199ac37817d480;
            condexpr_end_2:;
            {
                PyObject *old = var_source_ip_bind;
                assert( old != NULL );
                var_source_ip_bind = tmp_assign_source_3;
                Py_INCREF( var_source_ip_bind );
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 317;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_af );
        tmp_args_element_name_1 = par_af;
        frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 317;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_socket, call_args );
        }

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_socket_obj == NULL );
        var_socket_obj = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_set_close_exec );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_set_close_exec );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "set_close_exec" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 318;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        CHECK_OBJECT( var_socket_obj );
        tmp_called_instance_2 = var_socket_obj;
        frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 318;
        tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_fileno );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 318;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_4;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        int tmp_truth_name_2;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_source_port_bind );
        tmp_truth_name_2 = CHECK_IF_TRUE( var_source_port_bind );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( var_source_ip_bind );
        tmp_truth_name_3 = CHECK_IF_TRUE( var_source_ip_bind );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_4 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_4 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        // Tried code:
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( var_socket_obj );
            tmp_called_instance_3 = var_socket_obj;
            CHECK_OBJECT( var_source_ip_bind );
            tmp_tuple_element_1 = var_source_ip_bind;
            tmp_args_element_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_element_name_3, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_source_port_bind );
            tmp_tuple_element_1 = var_source_port_bind;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_element_name_3, 1, tmp_tuple_element_1 );
            frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 322;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_bind, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 322;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_27460344c266d7b4afdcb1503875504d, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_27460344c266d7b4afdcb1503875504d, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_4;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 323;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_3;
            }

            tmp_source_name_2 = tmp_mvar_value_4;
            tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_error );
            if ( tmp_compexpr_right_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 323;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_3;
            }
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 323;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_called_instance_4;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( var_socket_obj );
                tmp_called_instance_4 = var_socket_obj;
                frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 324;
                tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_close );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 324;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_3;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 326;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_27460344c266d7b4afdcb1503875504d->m_frame) frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_3;
            goto branch_end_3;
            branch_no_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 321;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_27460344c266d7b4afdcb1503875504d->m_frame) frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_3;
            branch_end_3:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_16__create_stream );
        return NULL;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        try_end_1:;
        branch_no_2:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOStream );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOStream );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOStream" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 328;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_4;
        }

        tmp_called_name_2 = tmp_mvar_value_5;
        CHECK_OBJECT( var_socket_obj );
        tmp_tuple_element_2 = var_socket_obj;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
        tmp_dict_key_1 = const_str_plain_max_buffer_size;
        CHECK_OBJECT( par_max_buffer_size );
        tmp_dict_value_1 = par_max_buffer_size;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 328;
        tmp_assign_source_5 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 328;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_4;
        }
        assert( var_stream == NULL );
        var_stream = tmp_assign_source_5;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_27460344c266d7b4afdcb1503875504d, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_27460344c266d7b4afdcb1503875504d, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_6;
        tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 329;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_5;
        }

        tmp_source_name_3 = tmp_mvar_value_6;
        tmp_compexpr_right_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_error );
        if ( tmp_compexpr_right_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_5;
        }
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = EXC_VALUE(PyThreadState_GET());
            assert( var_e == NULL );
            Py_INCREF( tmp_assign_source_6 );
            var_e = tmp_assign_source_6;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_7;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Future );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Future );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Future" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 330;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_6;
            }

            tmp_called_name_3 = tmp_mvar_value_7;
            frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 330;
            tmp_assign_source_7 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 330;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_6;
            }
            assert( var_fu == NULL );
            var_fu = tmp_assign_source_7;
        }
        {
            PyObject *tmp_called_instance_5;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( var_fu );
            tmp_called_instance_5 = var_fu;
            CHECK_OBJECT( var_e );
            tmp_args_element_name_4 = var_e;
            frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 331;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_set_exception, call_args );
            }

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 331;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_6;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_raise_value_1;
            tmp_raise_type_1 = PyExc_UnboundLocalError;
            tmp_raise_value_1 = const_str_digest_a5ec5f7583a10182afead56cee5545a7;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_value = tmp_raise_value_1;
            Py_INCREF( tmp_raise_value_1 );
            exception_lineno = 332;
            RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_16__create_stream );
        return NULL;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        goto branch_end_4;
        branch_no_4:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 327;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_27460344c266d7b4afdcb1503875504d->m_frame) frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooooooo";
        goto try_except_handler_5;
        branch_end_4:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_16__create_stream );
    return NULL;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_2:;
    {
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( var_stream );
        tmp_tuple_element_3 = var_stream;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( var_stream );
        tmp_called_instance_6 = var_stream;
        CHECK_OBJECT( par_addr );
        tmp_args_element_name_5 = par_addr;
        frame_27460344c266d7b4afdcb1503875504d->m_frame.f_lineno = 334;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_tuple_element_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_connect, call_args );
        }

        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 334;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_3 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27460344c266d7b4afdcb1503875504d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_27460344c266d7b4afdcb1503875504d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27460344c266d7b4afdcb1503875504d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_27460344c266d7b4afdcb1503875504d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_27460344c266d7b4afdcb1503875504d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_27460344c266d7b4afdcb1503875504d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_27460344c266d7b4afdcb1503875504d,
        type_description_1,
        par_self,
        par_max_buffer_size,
        par_af,
        par_addr,
        par_source_ip,
        par_source_port,
        var_source_port_bind,
        var_source_ip_bind,
        var_socket_obj,
        var_stream,
        var_e,
        var_fu
    );


    // Release cached frame.
    if ( frame_27460344c266d7b4afdcb1503875504d == cache_frame_27460344c266d7b4afdcb1503875504d )
    {
        Py_DECREF( frame_27460344c266d7b4afdcb1503875504d );
    }
    cache_frame_27460344c266d7b4afdcb1503875504d = NULL;

    assertFrameObject( frame_27460344c266d7b4afdcb1503875504d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_16__create_stream );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_max_buffer_size );
    Py_DECREF( par_max_buffer_size );
    par_max_buffer_size = NULL;

    CHECK_OBJECT( (PyObject *)par_af );
    Py_DECREF( par_af );
    par_af = NULL;

    CHECK_OBJECT( (PyObject *)par_addr );
    Py_DECREF( par_addr );
    par_addr = NULL;

    CHECK_OBJECT( (PyObject *)par_source_ip );
    Py_DECREF( par_source_ip );
    par_source_ip = NULL;

    CHECK_OBJECT( (PyObject *)par_source_port );
    Py_DECREF( par_source_port );
    par_source_port = NULL;

    CHECK_OBJECT( (PyObject *)var_source_port_bind );
    Py_DECREF( var_source_port_bind );
    var_source_port_bind = NULL;

    CHECK_OBJECT( (PyObject *)var_source_ip_bind );
    Py_DECREF( var_source_ip_bind );
    var_source_ip_bind = NULL;

    CHECK_OBJECT( (PyObject *)var_socket_obj );
    Py_DECREF( var_socket_obj );
    var_socket_obj = NULL;

    CHECK_OBJECT( (PyObject *)var_stream );
    Py_DECREF( var_stream );
    var_stream = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_max_buffer_size );
    Py_DECREF( par_max_buffer_size );
    par_max_buffer_size = NULL;

    CHECK_OBJECT( (PyObject *)par_af );
    Py_DECREF( par_af );
    par_af = NULL;

    CHECK_OBJECT( (PyObject *)par_addr );
    Py_DECREF( par_addr );
    par_addr = NULL;

    CHECK_OBJECT( (PyObject *)par_source_ip );
    Py_DECREF( par_source_ip );
    par_source_ip = NULL;

    CHECK_OBJECT( (PyObject *)par_source_port );
    Py_DECREF( par_source_port );
    par_source_port = NULL;

    Py_XDECREF( var_source_port_bind );
    var_source_port_bind = NULL;

    Py_XDECREF( var_source_ip_bind );
    var_source_ip_bind = NULL;

    Py_XDECREF( var_socket_obj );
    var_socket_obj = NULL;

    Py_XDECREF( var_stream );
    var_stream = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_fu );
    var_fu = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$tcpclient$$$function_16__create_stream );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_10_on_connect_timeout( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_10_on_connect_timeout,
        const_str_plain_on_connect_timeout,
#if PYTHON_VERSION >= 300
        const_str_digest_6fc24d07893c3730673b1956f99df558,
#endif
        codeobj_47c6a2ad74230fcb9dcc1362760e0766,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_11_clear_timeouts( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_11_clear_timeouts,
        const_str_plain_clear_timeouts,
#if PYTHON_VERSION >= 300
        const_str_digest_f3e84bb2c0ba624424fca914b1a06f4e,
#endif
        codeobj_793f9e350275ba265bef7ec1b3f9d348,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_12_close_streams( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_12_close_streams,
        const_str_plain_close_streams,
#if PYTHON_VERSION >= 300
        const_str_digest_dbf320d6ab8c292ec06eb15c20d40242,
#endif
        codeobj_9337544eeae4fefac5b0f2e4de7c8d8e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_13___init__( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_13___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_3f478350567dda4f59bb11821d2188f3,
#endif
        codeobj_d68f8ca13a9673b9114ba90152a28042,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_14_close( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_14_close,
        const_str_plain_close,
#if PYTHON_VERSION >= 300
        const_str_digest_c5af78c28f1e6b1a5b6fa585d863557d,
#endif
        codeobj_b84f5a07a552cb21cef2f0fdfdb94933,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_15_connect( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_15_connect,
        const_str_plain_connect,
#if PYTHON_VERSION >= 300
        const_str_digest_9c35a83a41df634bfe1f1aeb9487e326,
#endif
        codeobj_767c3d3c0821babfafc5da5d82408a00,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        const_str_digest_3028aebf52cc3ba379a11963de1a1b88,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_16__create_stream( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_16__create_stream,
        const_str_plain__create_stream,
#if PYTHON_VERSION >= 300
        const_str_digest_f60549bbcd4e8e24830aa6307857816b,
#endif
        codeobj_27460344c266d7b4afdcb1503875504d,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_1___init__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_117aea24be92adc94cba16e3bc01c3a1,
#endif
        codeobj_d613320d0df2ebc5c52af6c928e1c130,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_2_split( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_2_split,
        const_str_plain_split,
#if PYTHON_VERSION >= 300
        const_str_digest_fe7119b909401f80d9f429ed075d30c3,
#endif
        codeobj_96716989f5f9ecb67ad282bdae3e0449,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        const_str_digest_4a46cceca4b3639158c4818f0c5676c6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_3_start( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_3_start,
        const_str_plain_start,
#if PYTHON_VERSION >= 300
        const_str_digest_d77c8dba2c1752ad707d97d40a958563,
#endif
        codeobj_8fbd1c7c6633ceaf0faf23969f9070be,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_4_try_connect( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_4_try_connect,
        const_str_plain_try_connect,
#if PYTHON_VERSION >= 300
        const_str_digest_39bf746e7e6fa9081cb27981301d6497,
#endif
        codeobj_7a49bd1c33abeae8183a066ba3d45db5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_5_on_connect_done( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_5_on_connect_done,
        const_str_plain_on_connect_done,
#if PYTHON_VERSION >= 300
        const_str_digest_a054dc237cb6ddb4265000e5728b4749,
#endif
        codeobj_8ad4a72b5abeb6760f8bd6620dc8fb80,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_6_set_timeout( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_6_set_timeout,
        const_str_plain_set_timeout,
#if PYTHON_VERSION >= 300
        const_str_digest_63220ccc51be15c30b069d2428e0ba3e,
#endif
        codeobj_390b695c20ddbaaf72df3d190f411f82,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_7_on_timeout( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_7_on_timeout,
        const_str_plain_on_timeout,
#if PYTHON_VERSION >= 300
        const_str_digest_a6f3b7f5cf87c6768a6b5fa2a4394168,
#endif
        codeobj_1294c5e4eefa4f961a3eff38dd5d48bc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_8_clear_timeout( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_8_clear_timeout,
        const_str_plain_clear_timeout,
#if PYTHON_VERSION >= 300
        const_str_digest_60c529acd196cbf00ea282122ccf4eb2,
#endif
        codeobj_6e221219ab71e145074fd5a56615b3ef,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$tcpclient$$$function_9_set_connect_timeout( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$tcpclient$$$function_9_set_connect_timeout,
        const_str_plain_set_connect_timeout,
#if PYTHON_VERSION >= 300
        const_str_digest_b50dec95435ce18f9dbeb8564c432fb2,
#endif
        codeobj_8e0a19119d5caf5484e0b42a72c69ed6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$tcpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_tornado$tcpclient =
{
    PyModuleDef_HEAD_INIT,
    "tornado.tcpclient",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(tornado$tcpclient)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(tornado$tcpclient)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_tornado$tcpclient );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("tornado.tcpclient: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.tcpclient: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.tcpclient: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in inittornado$tcpclient" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_tornado$tcpclient = Py_InitModule4(
        "tornado.tcpclient",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_tornado$tcpclient = PyModule_Create( &mdef_tornado$tcpclient );
#endif

    moduledict_tornado$tcpclient = MODULE_DICT( module_tornado$tcpclient );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_tornado$tcpclient,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_tornado$tcpclient,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$tcpclient,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$tcpclient,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_tornado$tcpclient );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_c5a6a5dde27777d16fa4978b5cf11756, module_tornado$tcpclient );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_7b8ffbaad84d3ceef21adcb2f3812e3f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_tornado$tcpclient_42 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_c865cfb03d605666e7822b4eee0d8ac9_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_c865cfb03d605666e7822b4eee0d8ac9_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *locals_tornado$tcpclient_203 = NULL;
    struct Nuitka_FrameObject *frame_1ae114085666de15d483afe5f0f6984a_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_1ae114085666de15d483afe5f0f6984a_3 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_45c81ba291760ce276d329a5c153168e;
        UPDATE_STRING_DICT0( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_7b8ffbaad84d3ceef21adcb2f3812e3f = MAKE_MODULE_FRAME( codeobj_7b8ffbaad84d3ceef21adcb2f3812e3f, module_tornado$tcpclient );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_7b8ffbaad84d3ceef21adcb2f3812e3f );
    assert( Py_REFCNT( frame_7b8ffbaad84d3ceef21adcb2f3812e3f ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_functools;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 19;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_functools, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_socket;
        tmp_globals_name_2 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 20;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_numbers;
        tmp_globals_name_3 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 21;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_numbers, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_datetime;
        tmp_globals_name_4 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 22;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_datetime, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_ssl;
        tmp_globals_name_5 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 23;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_ssl, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_9b98666360eab9bd5e6093808f6c2588;
        tmp_globals_name_6 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_Future_str_plain_future_add_done_callback_tuple;
        tmp_level_name_6 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 25;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_9;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Future );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Future, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_future_add_done_callback );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_future_add_done_callback, tmp_assign_source_11 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_66ef772b0f62991fd20497da33b24b9b;
        tmp_globals_name_7 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_IOLoop_tuple;
        tmp_level_name_7 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 26;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_IOLoop );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOLoop, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_0ababed7bda89496db964fae880aa905;
        tmp_globals_name_8 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_IOStream_tuple;
        tmp_level_name_8 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 27;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_IOStream );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOStream, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_tornado;
        tmp_globals_name_9 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_gen_tuple;
        tmp_level_name_9 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 28;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_gen );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_gen, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_a78b79c1406c2b3f78fc8a3a0c110b46;
        tmp_globals_name_10 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_Resolver_tuple;
        tmp_level_name_10 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 29;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_Resolver );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Resolver, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_a9749d20124866074eb2f4f5b5d6dda5;
        tmp_globals_name_11 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_set_close_exec_tuple;
        tmp_level_name_11 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 30;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_set_close_exec );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_set_close_exec, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_c56ac0b712595d363e21033bdbaf2d31;
        tmp_globals_name_12 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_TimeoutError_tuple;
        tmp_level_name_12 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 31;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_TimeoutError );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_TimeoutError, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_plain_typing;
        tmp_globals_name_13 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = Py_None;
        tmp_level_name_13 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 33;
        tmp_assign_source_18 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_typing, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_name_name_14;
        PyObject *tmp_globals_name_14;
        PyObject *tmp_locals_name_14;
        PyObject *tmp_fromlist_name_14;
        PyObject *tmp_level_name_14;
        tmp_name_name_14 = const_str_plain_typing;
        tmp_globals_name_14 = (PyObject *)moduledict_tornado$tcpclient;
        tmp_locals_name_14 = Py_None;
        tmp_fromlist_name_14 = const_tuple_a81590143c265f724e87fff796eb5136_tuple;
        tmp_level_name_14 = const_int_0;
        frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 34;
        tmp_assign_source_19 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_19;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_Any );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Any, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_10 = tmp_import_from_2__module;
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_Union );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Union, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_11 = tmp_import_from_2__module;
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_Dict );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Dict, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_12 = tmp_import_from_2__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_Tuple );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_13 = tmp_import_from_2__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_List );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_14 = tmp_import_from_2__module;
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_Callable );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Callable, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_15 = tmp_import_from_2__module;
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_Iterator );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Iterator, tmp_assign_source_26 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_typing );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_typing );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "typing" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_TYPE_CHECKING );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_name_name_15;
            PyObject *tmp_globals_name_15;
            PyObject *tmp_locals_name_15;
            PyObject *tmp_fromlist_name_15;
            PyObject *tmp_level_name_15;
            tmp_name_name_15 = const_str_plain_typing;
            tmp_globals_name_15 = (PyObject *)moduledict_tornado$tcpclient;
            tmp_locals_name_15 = Py_None;
            tmp_fromlist_name_15 = const_tuple_str_plain_Optional_str_plain_Set_tuple;
            tmp_level_name_15 = const_int_0;
            frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 37;
            tmp_assign_source_27 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;

                goto frame_exception_exit_1;
            }
            assert( tmp_import_from_3__module == NULL );
            tmp_import_from_3__module = tmp_assign_source_27;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_import_name_from_16;
            CHECK_OBJECT( tmp_import_from_3__module );
            tmp_import_name_from_16 = tmp_import_from_3__module;
            tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_Optional );
            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;

                goto try_except_handler_3;
            }
            UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Optional, tmp_assign_source_28 );
        }
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_import_name_from_17;
            CHECK_OBJECT( tmp_import_from_3__module );
            tmp_import_name_from_17 = tmp_import_from_3__module;
            tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_Set );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;

                goto try_except_handler_3;
            }
            UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Set, tmp_assign_source_29 );
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
        Py_DECREF( tmp_import_from_3__module );
        tmp_import_from_3__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_1;
        // End of try:
        try_end_3:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
        Py_DECREF( tmp_import_from_3__module );
        tmp_import_from_3__module = NULL;

        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = const_float_0_3;
        UPDATE_STRING_DICT0( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain__INITIAL_CONNECT_TIMEOUT, tmp_assign_source_30 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_31 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_33 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_33;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_4;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_2 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_2, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_4;
            }
            tmp_tuple_element_1 = const_str_plain__Connector;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 42;
            tmp_assign_source_34 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_34;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_4 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 42;

                    goto try_except_handler_4;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_5 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_5 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 42;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 42;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 42;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_35;
            tmp_assign_source_35 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_35;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_36;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_tornado$tcpclient_42 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c5a6a5dde27777d16fa4978b5cf11756;
        tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_digest_e0643bd494e012b9157e7c29953e4eba;
        tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain__Connector;
        tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_c865cfb03d605666e7822b4eee0d8ac9_2, codeobj_c865cfb03d605666e7822b4eee0d8ac9, module_tornado$tcpclient, sizeof(void *) );
        frame_c865cfb03d605666e7822b4eee0d8ac9_2 = cache_frame_c865cfb03d605666e7822b4eee0d8ac9_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_c865cfb03d605666e7822b4eee0d8ac9_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_c865cfb03d605666e7822b4eee0d8ac9_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_annotations_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_list_element_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_subscript_name_4;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            tmp_dict_key_1 = const_str_plain_addrinfo;
            tmp_subscribed_name_2 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_List );

            if ( tmp_subscribed_name_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 62;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_2 = tmp_mvar_value_4;
                Py_INCREF( tmp_subscribed_name_2 );
                }
            }

            tmp_subscript_name_2 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscript_name_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_5 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 62;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscript_name_2 = tmp_mvar_value_5;
                Py_INCREF( tmp_subscript_name_2 );
                }
            }

            tmp_dict_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscribed_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 62;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_1 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_connect;
            tmp_subscribed_name_3 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Callable );

            if ( tmp_subscribed_name_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_6 == NULL )
                {
                    Py_DECREF( tmp_annotations_1 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 63;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_3 = tmp_mvar_value_6;
                Py_INCREF( tmp_subscribed_name_3 );
                }
            }

            tmp_source_name_6 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_7 == NULL )
                {
                    Py_DECREF( tmp_annotations_1 );
                    Py_DECREF( tmp_subscribed_name_3 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_6 = tmp_mvar_value_7;
                Py_INCREF( tmp_source_name_6 );
                }
            }

            tmp_list_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_list_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_1 );
                Py_DECREF( tmp_subscribed_name_3 );

                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_tuple_element_3 = PyList_New( 2 );
            PyList_SET_ITEM( tmp_tuple_element_3, 0, tmp_list_element_1 );
            tmp_list_element_1 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_list_element_1 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_8 == NULL )
                {
                    Py_DECREF( tmp_annotations_1 );
                    Py_DECREF( tmp_subscribed_name_3 );
                    Py_DECREF( tmp_tuple_element_3 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_list_element_1 = tmp_mvar_value_8;
                Py_INCREF( tmp_list_element_1 );
                }
            }

            PyList_SET_ITEM( tmp_tuple_element_3, 1, tmp_list_element_1 );
            tmp_subscript_name_3 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_3, 0, tmp_tuple_element_3 );
            tmp_subscribed_name_4 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_9 == NULL )
                {
                    Py_DECREF( tmp_annotations_1 );
                    Py_DECREF( tmp_subscribed_name_3 );
                    Py_DECREF( tmp_subscript_name_3 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_4 = tmp_mvar_value_9;
                Py_INCREF( tmp_subscribed_name_4 );
                }
            }

            tmp_tuple_element_4 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_IOStream );

            if ( tmp_tuple_element_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOStream );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOStream );
                }

                if ( tmp_mvar_value_10 == NULL )
                {
                    Py_DECREF( tmp_annotations_1 );
                    Py_DECREF( tmp_subscribed_name_3 );
                    Py_DECREF( tmp_subscript_name_3 );
                    Py_DECREF( tmp_subscribed_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOStream" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_4 = tmp_mvar_value_10;
                Py_INCREF( tmp_tuple_element_4 );
                }
            }

            tmp_subscript_name_4 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_4, 0, tmp_tuple_element_4 );
            tmp_tuple_element_4 = const_str_digest_d057d99810b1c539102c2397e91f2552;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_subscript_name_4, 1, tmp_tuple_element_4 );
            tmp_tuple_element_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            Py_DECREF( tmp_subscribed_name_4 );
            Py_DECREF( tmp_subscript_name_4 );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_1 );
                Py_DECREF( tmp_subscribed_name_3 );
                Py_DECREF( tmp_subscript_name_3 );

                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_3, 1, tmp_tuple_element_3 );
            tmp_dict_value_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_3 );
            Py_DECREF( tmp_subscript_name_3 );
            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_1 );

                exception_lineno = 63;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_3 = const_str_plain_return;
            tmp_dict_value_3 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_3, tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_1___init__( tmp_annotations_1 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_annotations_2;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_dict_key_5;
            PyObject *tmp_dict_value_5;
            PyObject *tmp_subscribed_name_6;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_subscript_name_6;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_subscribed_name_7;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_subscript_name_7;
            PyObject *tmp_subscribed_name_8;
            PyObject *tmp_mvar_value_15;
            PyObject *tmp_subscript_name_8;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_16;
            PyObject *tmp_mvar_value_17;
            PyObject *tmp_subscribed_name_9;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_subscript_name_9;
            PyObject *tmp_subscribed_name_10;
            PyObject *tmp_mvar_value_19;
            PyObject *tmp_subscript_name_10;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_source_name_8;
            PyObject *tmp_mvar_value_20;
            PyObject *tmp_mvar_value_21;
            PyObject *tmp_staticmethod_arg_1;
            PyObject *tmp_annotations_3;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            PyObject *tmp_subscribed_name_11;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_subscript_name_11;
            PyObject *tmp_mvar_value_23;
            PyObject *tmp_dict_key_7;
            PyObject *tmp_dict_value_7;
            PyObject *tmp_subscribed_name_12;
            PyObject *tmp_mvar_value_24;
            PyObject *tmp_subscript_name_12;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_subscribed_name_13;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_subscript_name_13;
            PyObject *tmp_subscribed_name_14;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_subscript_name_14;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_source_name_9;
            PyObject *tmp_mvar_value_27;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_subscribed_name_15;
            PyObject *tmp_mvar_value_29;
            PyObject *tmp_subscript_name_15;
            PyObject *tmp_subscribed_name_16;
            PyObject *tmp_mvar_value_30;
            PyObject *tmp_subscript_name_16;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_source_name_10;
            PyObject *tmp_mvar_value_31;
            PyObject *tmp_mvar_value_32;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$tcpclient_42, const_str_plain_staticmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_2 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_staticmethod );

            if ( tmp_called_name_2 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "staticmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dict_key_4 = const_str_plain_addrinfo;
            tmp_subscribed_name_5 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_List );

            if ( tmp_subscribed_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_11 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 82;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_5 = tmp_mvar_value_11;
                Py_INCREF( tmp_subscribed_name_5 );
                }
            }

            tmp_subscript_name_5 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscript_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_12 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_subscribed_name_5 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 82;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscript_name_5 = tmp_mvar_value_12;
                Py_INCREF( tmp_subscript_name_5 );
                }
            }

            tmp_dict_value_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            Py_DECREF( tmp_subscribed_name_5 );
            Py_DECREF( tmp_subscript_name_5 );
            if ( tmp_dict_value_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 82;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_2 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_2, tmp_dict_key_4, tmp_dict_value_4 );
            Py_DECREF( tmp_dict_value_4 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_5 = const_str_plain_return;
            tmp_subscribed_name_6 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_13 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 83;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_6 = tmp_mvar_value_13;
                Py_INCREF( tmp_subscribed_name_6 );
                }
            }

            tmp_subscribed_name_7 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_List );

            if ( tmp_subscribed_name_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_14 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_7 = tmp_mvar_value_14;
                Py_INCREF( tmp_subscribed_name_7 );
                }
            }

            tmp_subscribed_name_8 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_15 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    Py_DECREF( tmp_subscribed_name_7 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_8 = tmp_mvar_value_15;
                Py_INCREF( tmp_subscribed_name_8 );
                }
            }

            tmp_source_name_7 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_16 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    Py_DECREF( tmp_subscribed_name_7 );
                    Py_DECREF( tmp_subscribed_name_8 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_7 = tmp_mvar_value_16;
                Py_INCREF( tmp_source_name_7 );
                }
            }

            tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_7 );
            if ( tmp_tuple_element_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_annotations_2 );
                Py_DECREF( tmp_subscribed_name_6 );
                Py_DECREF( tmp_subscribed_name_7 );
                Py_DECREF( tmp_subscribed_name_8 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_8 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_8, 0, tmp_tuple_element_6 );
            tmp_tuple_element_6 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_tuple_element_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_17 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    Py_DECREF( tmp_subscribed_name_7 );
                    Py_DECREF( tmp_subscribed_name_8 );
                    Py_DECREF( tmp_subscript_name_8 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_6 = tmp_mvar_value_17;
                Py_INCREF( tmp_tuple_element_6 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_8, 1, tmp_tuple_element_6 );
            tmp_subscript_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
            Py_DECREF( tmp_subscribed_name_8 );
            Py_DECREF( tmp_subscript_name_8 );
            if ( tmp_subscript_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_annotations_2 );
                Py_DECREF( tmp_subscribed_name_6 );
                Py_DECREF( tmp_subscribed_name_7 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_tuple_element_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
            Py_DECREF( tmp_subscribed_name_7 );
            Py_DECREF( tmp_subscript_name_7 );
            if ( tmp_tuple_element_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_annotations_2 );
                Py_DECREF( tmp_subscribed_name_6 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_6 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_6, 0, tmp_tuple_element_5 );
            tmp_subscribed_name_9 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_List );

            if ( tmp_subscribed_name_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_18 == NULL ))
                {
                    tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_18 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    Py_DECREF( tmp_subscript_name_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_9 = tmp_mvar_value_18;
                Py_INCREF( tmp_subscribed_name_9 );
                }
            }

            tmp_subscribed_name_10 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_19 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    Py_DECREF( tmp_subscript_name_6 );
                    Py_DECREF( tmp_subscribed_name_9 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_10 = tmp_mvar_value_19;
                Py_INCREF( tmp_subscribed_name_10 );
                }
            }

            tmp_source_name_8 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_20 == NULL ))
                {
                    tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_20 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    Py_DECREF( tmp_subscript_name_6 );
                    Py_DECREF( tmp_subscribed_name_9 );
                    Py_DECREF( tmp_subscribed_name_10 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_8 = tmp_mvar_value_20;
                Py_INCREF( tmp_source_name_8 );
                }
            }

            tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_8 );
            if ( tmp_tuple_element_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_annotations_2 );
                Py_DECREF( tmp_subscribed_name_6 );
                Py_DECREF( tmp_subscript_name_6 );
                Py_DECREF( tmp_subscribed_name_9 );
                Py_DECREF( tmp_subscribed_name_10 );

                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_10 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_10, 0, tmp_tuple_element_7 );
            tmp_tuple_element_7 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_tuple_element_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_21 == NULL ))
                {
                    tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_21 == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_annotations_2 );
                    Py_DECREF( tmp_subscribed_name_6 );
                    Py_DECREF( tmp_subscript_name_6 );
                    Py_DECREF( tmp_subscribed_name_9 );
                    Py_DECREF( tmp_subscribed_name_10 );
                    Py_DECREF( tmp_subscript_name_10 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_7 = tmp_mvar_value_21;
                Py_INCREF( tmp_tuple_element_7 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_10, 1, tmp_tuple_element_7 );
            tmp_subscript_name_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_10, tmp_subscript_name_10 );
            Py_DECREF( tmp_subscribed_name_10 );
            Py_DECREF( tmp_subscript_name_10 );
            if ( tmp_subscript_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_annotations_2 );
                Py_DECREF( tmp_subscribed_name_6 );
                Py_DECREF( tmp_subscript_name_6 );
                Py_DECREF( tmp_subscribed_name_9 );

                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_tuple_element_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_9 );
            Py_DECREF( tmp_subscribed_name_9 );
            Py_DECREF( tmp_subscript_name_9 );
            if ( tmp_tuple_element_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_annotations_2 );
                Py_DECREF( tmp_subscribed_name_6 );
                Py_DECREF( tmp_subscript_name_6 );

                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_6, 1, tmp_tuple_element_5 );
            tmp_dict_value_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
            Py_DECREF( tmp_subscribed_name_6 );
            Py_DECREF( tmp_subscript_name_6 );
            if ( tmp_dict_value_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_annotations_2 );

                exception_lineno = 83;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_2, tmp_dict_key_5, tmp_dict_value_5 );
            Py_DECREF( tmp_dict_value_5 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_1 = MAKE_FUNCTION_tornado$tcpclient$$$function_2_split( tmp_annotations_2 );



            frame_c865cfb03d605666e7822b4eee0d8ac9_2->m_frame.f_lineno = 80;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_dict_key_6 = const_str_plain_addrinfo;
            tmp_subscribed_name_11 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_List );

            if ( tmp_subscribed_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_22 == NULL ))
                {
                    tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_22 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 82;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_11 = tmp_mvar_value_22;
                Py_INCREF( tmp_subscribed_name_11 );
                }
            }

            tmp_subscript_name_11 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscript_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_23 == NULL ))
                {
                    tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_23 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_11 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 82;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscript_name_11 = tmp_mvar_value_23;
                Py_INCREF( tmp_subscript_name_11 );
                }
            }

            tmp_dict_value_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_11 );
            Py_DECREF( tmp_subscribed_name_11 );
            Py_DECREF( tmp_subscript_name_11 );
            if ( tmp_dict_value_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 82;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_3 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_3, tmp_dict_key_6, tmp_dict_value_6 );
            Py_DECREF( tmp_dict_value_6 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_7 = const_str_plain_return;
            tmp_subscribed_name_12 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_24 == NULL ))
                {
                    tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_24 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 83;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_12 = tmp_mvar_value_24;
                Py_INCREF( tmp_subscribed_name_12 );
                }
            }

            tmp_subscribed_name_13 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_List );

            if ( tmp_subscribed_name_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_25 == NULL ))
                {
                    tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_25 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_13 = tmp_mvar_value_25;
                Py_INCREF( tmp_subscribed_name_13 );
                }
            }

            tmp_subscribed_name_14 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_26 == NULL ))
                {
                    tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_26 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    Py_DECREF( tmp_subscribed_name_13 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_14 = tmp_mvar_value_26;
                Py_INCREF( tmp_subscribed_name_14 );
                }
            }

            tmp_source_name_9 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_27 == NULL ))
                {
                    tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_27 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    Py_DECREF( tmp_subscribed_name_13 );
                    Py_DECREF( tmp_subscribed_name_14 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_9 = tmp_mvar_value_27;
                Py_INCREF( tmp_source_name_9 );
                }
            }

            tmp_tuple_element_9 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_9 );
            if ( tmp_tuple_element_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_3 );
                Py_DECREF( tmp_subscribed_name_12 );
                Py_DECREF( tmp_subscribed_name_13 );
                Py_DECREF( tmp_subscribed_name_14 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_14 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_14, 0, tmp_tuple_element_9 );
            tmp_tuple_element_9 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_tuple_element_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_28 == NULL ))
                {
                    tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_28 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    Py_DECREF( tmp_subscribed_name_13 );
                    Py_DECREF( tmp_subscribed_name_14 );
                    Py_DECREF( tmp_subscript_name_14 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 84;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_9 = tmp_mvar_value_28;
                Py_INCREF( tmp_tuple_element_9 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_14, 1, tmp_tuple_element_9 );
            tmp_subscript_name_13 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_14, tmp_subscript_name_14 );
            Py_DECREF( tmp_subscribed_name_14 );
            Py_DECREF( tmp_subscript_name_14 );
            if ( tmp_subscript_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_3 );
                Py_DECREF( tmp_subscribed_name_12 );
                Py_DECREF( tmp_subscribed_name_13 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_tuple_element_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_13, tmp_subscript_name_13 );
            Py_DECREF( tmp_subscribed_name_13 );
            Py_DECREF( tmp_subscript_name_13 );
            if ( tmp_tuple_element_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_3 );
                Py_DECREF( tmp_subscribed_name_12 );

                exception_lineno = 84;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_12 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_12, 0, tmp_tuple_element_8 );
            tmp_subscribed_name_15 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_List );

            if ( tmp_subscribed_name_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_List );

                if (unlikely( tmp_mvar_value_29 == NULL ))
                {
                    tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_List );
                }

                if ( tmp_mvar_value_29 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    Py_DECREF( tmp_subscript_name_12 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "List" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_15 = tmp_mvar_value_29;
                Py_INCREF( tmp_subscribed_name_15 );
                }
            }

            tmp_subscribed_name_16 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_30 == NULL ))
                {
                    tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_30 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    Py_DECREF( tmp_subscript_name_12 );
                    Py_DECREF( tmp_subscribed_name_15 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_16 = tmp_mvar_value_30;
                Py_INCREF( tmp_subscribed_name_16 );
                }
            }

            tmp_source_name_10 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_31 == NULL ))
                {
                    tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_31 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    Py_DECREF( tmp_subscript_name_12 );
                    Py_DECREF( tmp_subscribed_name_15 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_10 = tmp_mvar_value_31;
                Py_INCREF( tmp_source_name_10 );
                }
            }

            tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_10 );
            if ( tmp_tuple_element_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_3 );
                Py_DECREF( tmp_subscribed_name_12 );
                Py_DECREF( tmp_subscript_name_12 );
                Py_DECREF( tmp_subscribed_name_15 );
                Py_DECREF( tmp_subscribed_name_16 );

                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_16 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_16, 0, tmp_tuple_element_10 );
            tmp_tuple_element_10 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_tuple_element_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_32 == NULL ))
                {
                    tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_32 == NULL )
                {
                    Py_DECREF( tmp_annotations_3 );
                    Py_DECREF( tmp_subscribed_name_12 );
                    Py_DECREF( tmp_subscript_name_12 );
                    Py_DECREF( tmp_subscribed_name_15 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    Py_DECREF( tmp_subscript_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_10 = tmp_mvar_value_32;
                Py_INCREF( tmp_tuple_element_10 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_16, 1, tmp_tuple_element_10 );
            tmp_subscript_name_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_16, tmp_subscript_name_16 );
            Py_DECREF( tmp_subscribed_name_16 );
            Py_DECREF( tmp_subscript_name_16 );
            if ( tmp_subscript_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_3 );
                Py_DECREF( tmp_subscribed_name_12 );
                Py_DECREF( tmp_subscript_name_12 );
                Py_DECREF( tmp_subscribed_name_15 );

                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_tuple_element_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_15, tmp_subscript_name_15 );
            Py_DECREF( tmp_subscribed_name_15 );
            Py_DECREF( tmp_subscript_name_15 );
            if ( tmp_tuple_element_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_3 );
                Py_DECREF( tmp_subscribed_name_12 );
                Py_DECREF( tmp_subscript_name_12 );

                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_12, 1, tmp_tuple_element_8 );
            tmp_dict_value_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_12, tmp_subscript_name_12 );
            Py_DECREF( tmp_subscribed_name_12 );
            Py_DECREF( tmp_subscript_name_12 );
            if ( tmp_dict_value_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_3 );

                exception_lineno = 83;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_3, tmp_dict_key_7, tmp_dict_value_7 );
            Py_DECREF( tmp_dict_value_7 );
            assert( !(tmp_res != 0) );
            tmp_staticmethod_arg_1 = MAKE_FUNCTION_tornado$tcpclient$$$function_2_split( tmp_annotations_3 );



            tmp_dictset_value = BUILTIN_STATICMETHOD( tmp_staticmethod_arg_1 );
            Py_DECREF( tmp_staticmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_split, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_1;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_mvar_value_33;
            PyObject *tmp_annotations_4;
            PyObject *tmp_dict_key_8;
            PyObject *tmp_dict_value_8;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            PyObject *tmp_subscribed_name_17;
            PyObject *tmp_mvar_value_34;
            PyObject *tmp_subscript_name_17;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_source_name_11;
            PyObject *tmp_mvar_value_35;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            tmp_tuple_element_11 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain__INITIAL_CONNECT_TIMEOUT );

            if ( tmp_tuple_element_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain__INITIAL_CONNECT_TIMEOUT );

                if (unlikely( tmp_mvar_value_33 == NULL ))
                {
                    tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__INITIAL_CONNECT_TIMEOUT );
                }

                if ( tmp_mvar_value_33 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_INITIAL_CONNECT_TIMEOUT" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 107;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_11 = tmp_mvar_value_33;
                Py_INCREF( tmp_tuple_element_11 );
                }
            }

            tmp_defaults_1 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_11 );
            tmp_tuple_element_11 = Py_None;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_defaults_1, 1, tmp_tuple_element_11 );
            tmp_dict_key_8 = const_str_plain_timeout;
            tmp_dict_value_8 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_float );

            if ( tmp_dict_value_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_8 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_dict_value_8 );
                }
            }

            tmp_annotations_4 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_8, tmp_dict_value_8 );
            Py_DECREF( tmp_dict_value_8 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_9 = const_str_plain_connect_timeout;
            tmp_subscribed_name_17 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Union );

            if ( tmp_subscribed_name_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_34 == NULL ))
                {
                    tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_34 == NULL )
                {
                    Py_DECREF( tmp_defaults_1 );
                    Py_DECREF( tmp_annotations_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 108;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_17 = tmp_mvar_value_34;
                Py_INCREF( tmp_subscribed_name_17 );
                }
            }

            tmp_tuple_element_12 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_float );

            if ( tmp_tuple_element_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_12 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_tuple_element_12 );
                }
            }

            tmp_subscript_name_17 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_17, 0, tmp_tuple_element_12 );
            tmp_source_name_11 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_datetime );

            if ( tmp_source_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_datetime );

                if (unlikely( tmp_mvar_value_35 == NULL ))
                {
                    tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
                }

                if ( tmp_mvar_value_35 == NULL )
                {
                    Py_DECREF( tmp_defaults_1 );
                    Py_DECREF( tmp_annotations_4 );
                    Py_DECREF( tmp_subscribed_name_17 );
                    Py_DECREF( tmp_subscript_name_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 108;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_11 = tmp_mvar_value_35;
                Py_INCREF( tmp_source_name_11 );
                }
            }

            tmp_tuple_element_12 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_timedelta );
            Py_DECREF( tmp_source_name_11 );
            if ( tmp_tuple_element_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_1 );
                Py_DECREF( tmp_annotations_4 );
                Py_DECREF( tmp_subscribed_name_17 );
                Py_DECREF( tmp_subscript_name_17 );

                exception_lineno = 108;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_17, 1, tmp_tuple_element_12 );
            tmp_dict_value_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_17, tmp_subscript_name_17 );
            Py_DECREF( tmp_subscribed_name_17 );
            Py_DECREF( tmp_subscript_name_17 );
            if ( tmp_dict_value_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_1 );
                Py_DECREF( tmp_annotations_4 );

                exception_lineno = 108;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_9, tmp_dict_value_9 );
            Py_DECREF( tmp_dict_value_9 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_10 = const_str_plain_return;
            tmp_dict_value_10 = const_str_digest_6c2f84cc90af0505120aaa5f893a0d8a;
            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_10, tmp_dict_value_10 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_3_start( tmp_defaults_1, tmp_annotations_4 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_start, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_5;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            PyObject *tmp_subscribed_name_18;
            PyObject *tmp_mvar_value_36;
            PyObject *tmp_subscript_name_18;
            PyObject *tmp_subscribed_name_19;
            PyObject *tmp_mvar_value_37;
            PyObject *tmp_subscript_name_19;
            PyObject *tmp_tuple_element_13;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_38;
            PyObject *tmp_mvar_value_39;
            PyObject *tmp_dict_key_12;
            PyObject *tmp_dict_value_12;
            tmp_dict_key_11 = const_str_plain_addrs;
            tmp_subscribed_name_18 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Iterator );

            if ( tmp_subscribed_name_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Iterator );

                if (unlikely( tmp_mvar_value_36 == NULL ))
                {
                    tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Iterator );
                }

                if ( tmp_mvar_value_36 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Iterator" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 116;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_18 = tmp_mvar_value_36;
                Py_INCREF( tmp_subscribed_name_18 );
                }
            }

            tmp_subscribed_name_19 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_37 == NULL ))
                {
                    tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_37 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 116;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_19 = tmp_mvar_value_37;
                Py_INCREF( tmp_subscribed_name_19 );
                }
            }

            tmp_source_name_12 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_38 == NULL ))
                {
                    tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_38 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_18 );
                    Py_DECREF( tmp_subscribed_name_19 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 116;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_12 = tmp_mvar_value_38;
                Py_INCREF( tmp_source_name_12 );
                }
            }

            tmp_tuple_element_13 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_12 );
            if ( tmp_tuple_element_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscribed_name_18 );
                Py_DECREF( tmp_subscribed_name_19 );

                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_19 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_19, 0, tmp_tuple_element_13 );
            tmp_tuple_element_13 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_tuple_element_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_39 == NULL ))
                {
                    tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_39 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_18 );
                    Py_DECREF( tmp_subscribed_name_19 );
                    Py_DECREF( tmp_subscript_name_19 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 116;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_13 = tmp_mvar_value_39;
                Py_INCREF( tmp_tuple_element_13 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_19, 1, tmp_tuple_element_13 );
            tmp_subscript_name_18 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_19, tmp_subscript_name_19 );
            Py_DECREF( tmp_subscribed_name_19 );
            Py_DECREF( tmp_subscript_name_19 );
            if ( tmp_subscript_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscribed_name_18 );

                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dict_value_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_18, tmp_subscript_name_18 );
            Py_DECREF( tmp_subscribed_name_18 );
            Py_DECREF( tmp_subscript_name_18 );
            if ( tmp_dict_value_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_5 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_5, tmp_dict_key_11, tmp_dict_value_11 );
            Py_DECREF( tmp_dict_value_11 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_12 = const_str_plain_return;
            tmp_dict_value_12 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_5, tmp_dict_key_12, tmp_dict_value_12 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_4_try_connect( tmp_annotations_5 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_try_connect, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_6;
            PyObject *tmp_dict_key_13;
            PyObject *tmp_dict_value_13;
            PyObject *tmp_subscribed_name_20;
            PyObject *tmp_mvar_value_40;
            PyObject *tmp_subscript_name_20;
            PyObject *tmp_subscribed_name_21;
            PyObject *tmp_mvar_value_41;
            PyObject *tmp_subscript_name_21;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_source_name_13;
            PyObject *tmp_mvar_value_42;
            PyObject *tmp_mvar_value_43;
            PyObject *tmp_dict_key_14;
            PyObject *tmp_dict_value_14;
            PyObject *tmp_source_name_14;
            PyObject *tmp_mvar_value_44;
            PyObject *tmp_dict_key_15;
            PyObject *tmp_dict_value_15;
            PyObject *tmp_mvar_value_45;
            PyObject *tmp_dict_key_16;
            PyObject *tmp_dict_value_16;
            PyObject *tmp_dict_key_17;
            PyObject *tmp_dict_value_17;
            tmp_dict_key_13 = const_str_plain_addrs;
            tmp_subscribed_name_20 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Iterator );

            if ( tmp_subscribed_name_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Iterator );

                if (unlikely( tmp_mvar_value_40 == NULL ))
                {
                    tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Iterator );
                }

                if ( tmp_mvar_value_40 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Iterator" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 136;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_20 = tmp_mvar_value_40;
                Py_INCREF( tmp_subscribed_name_20 );
                }
            }

            tmp_subscribed_name_21 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_subscribed_name_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_41 == NULL ))
                {
                    tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_41 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_20 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 136;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_21 = tmp_mvar_value_41;
                Py_INCREF( tmp_subscribed_name_21 );
                }
            }

            tmp_source_name_13 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_42 == NULL ))
                {
                    tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_42 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_20 );
                    Py_DECREF( tmp_subscribed_name_21 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 136;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_13 = tmp_mvar_value_42;
                Py_INCREF( tmp_source_name_13 );
                }
            }

            tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_13 );
            if ( tmp_tuple_element_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscribed_name_20 );
                Py_DECREF( tmp_subscribed_name_21 );

                exception_lineno = 136;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_subscript_name_21 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_21, 0, tmp_tuple_element_14 );
            tmp_tuple_element_14 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_tuple_element_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_43 == NULL ))
                {
                    tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_43 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_20 );
                    Py_DECREF( tmp_subscribed_name_21 );
                    Py_DECREF( tmp_subscript_name_21 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 136;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_14 = tmp_mvar_value_43;
                Py_INCREF( tmp_tuple_element_14 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_21, 1, tmp_tuple_element_14 );
            tmp_subscript_name_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_21, tmp_subscript_name_21 );
            Py_DECREF( tmp_subscribed_name_21 );
            Py_DECREF( tmp_subscript_name_21 );
            if ( tmp_subscript_name_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscribed_name_20 );

                exception_lineno = 136;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dict_value_13 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_20, tmp_subscript_name_20 );
            Py_DECREF( tmp_subscribed_name_20 );
            Py_DECREF( tmp_subscript_name_20 );
            if ( tmp_dict_value_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_6 = _PyDict_NewPresized( 5 );
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_13, tmp_dict_value_13 );
            Py_DECREF( tmp_dict_value_13 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_14 = const_str_plain_af;
            tmp_source_name_14 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_socket );

            if ( tmp_source_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_44 == NULL ))
                {
                    tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_44 == NULL )
                {
                    Py_DECREF( tmp_annotations_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 137;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_14 = tmp_mvar_value_44;
                Py_INCREF( tmp_source_name_14 );
                }
            }

            tmp_dict_value_14 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_14 );
            if ( tmp_dict_value_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_6 );

                exception_lineno = 137;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_14, tmp_dict_value_14 );
            Py_DECREF( tmp_dict_value_14 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_15 = const_str_plain_addr;
            tmp_dict_value_15 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Tuple );

            if ( tmp_dict_value_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_45 == NULL ))
                {
                    tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_45 == NULL )
                {
                    Py_DECREF( tmp_annotations_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 138;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_dict_value_15 = tmp_mvar_value_45;
                Py_INCREF( tmp_dict_value_15 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_15, tmp_dict_value_15 );
            Py_DECREF( tmp_dict_value_15 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_16 = const_str_plain_future;
            tmp_dict_value_16 = const_str_digest_d057d99810b1c539102c2397e91f2552;
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_16, tmp_dict_value_16 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_17 = const_str_plain_return;
            tmp_dict_value_17 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_17, tmp_dict_value_17 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_5_on_connect_done( tmp_annotations_6 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_on_connect_done, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_7;
            PyObject *tmp_dict_key_18;
            PyObject *tmp_dict_value_18;
            PyObject *tmp_dict_key_19;
            PyObject *tmp_dict_value_19;
            tmp_dict_key_18 = const_str_plain_timeout;
            tmp_dict_value_18 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_float );

            if ( tmp_dict_value_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_18 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_dict_value_18 );
                }
            }

            tmp_annotations_7 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_18, tmp_dict_value_18 );
            Py_DECREF( tmp_dict_value_18 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_19 = const_str_plain_return;
            tmp_dict_value_19 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_19, tmp_dict_value_19 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_6_set_timeout( tmp_annotations_7 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_set_timeout, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_8;
            tmp_annotations_8 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_7_on_timeout( tmp_annotations_8 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_on_timeout, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_9;
            tmp_annotations_9 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_8_clear_timeout( tmp_annotations_9 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_clear_timeout, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_10;
            PyObject *tmp_dict_key_20;
            PyObject *tmp_dict_value_20;
            PyObject *tmp_subscribed_name_22;
            PyObject *tmp_mvar_value_46;
            PyObject *tmp_subscript_name_22;
            PyObject *tmp_tuple_element_15;
            PyObject *tmp_source_name_15;
            PyObject *tmp_mvar_value_47;
            PyObject *tmp_dict_key_21;
            PyObject *tmp_dict_value_21;
            tmp_dict_key_20 = const_str_plain_connect_timeout;
            tmp_subscribed_name_22 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_Union );

            if ( tmp_subscribed_name_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_46 == NULL ))
                {
                    tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_46 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 181;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_22 = tmp_mvar_value_46;
                Py_INCREF( tmp_subscribed_name_22 );
                }
            }

            tmp_tuple_element_15 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_float );

            if ( tmp_tuple_element_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_15 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_tuple_element_15 );
                }
            }

            tmp_subscript_name_22 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_22, 0, tmp_tuple_element_15 );
            tmp_source_name_15 = PyObject_GetItem( locals_tornado$tcpclient_42, const_str_plain_datetime );

            if ( tmp_source_name_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_datetime );

                if (unlikely( tmp_mvar_value_47 == NULL ))
                {
                    tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
                }

                if ( tmp_mvar_value_47 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_22 );
                    Py_DECREF( tmp_subscript_name_22 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 181;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_15 = tmp_mvar_value_47;
                Py_INCREF( tmp_source_name_15 );
                }
            }

            tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_timedelta );
            Py_DECREF( tmp_source_name_15 );
            if ( tmp_tuple_element_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscribed_name_22 );
                Py_DECREF( tmp_subscript_name_22 );

                exception_lineno = 181;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_22, 1, tmp_tuple_element_15 );
            tmp_dict_value_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_22, tmp_subscript_name_22 );
            Py_DECREF( tmp_subscribed_name_22 );
            Py_DECREF( tmp_subscript_name_22 );
            if ( tmp_dict_value_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 181;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_10 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_20, tmp_dict_value_20 );
            Py_DECREF( tmp_dict_value_20 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_21 = const_str_plain_return;
            tmp_dict_value_21 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_21, tmp_dict_value_21 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_9_set_connect_timeout( tmp_annotations_10 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_set_connect_timeout, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_11;
            tmp_annotations_11 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_10_on_connect_timeout( tmp_annotations_11 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_on_connect_timeout, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_12;
            tmp_annotations_12 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_11_clear_timeouts( tmp_annotations_12 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_clear_timeouts, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_13;
            tmp_annotations_13 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_12_close_streams( tmp_annotations_13 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain_close_streams, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 198;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c865cfb03d605666e7822b4eee0d8ac9_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c865cfb03d605666e7822b4eee0d8ac9_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_c865cfb03d605666e7822b4eee0d8ac9_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_c865cfb03d605666e7822b4eee0d8ac9_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_c865cfb03d605666e7822b4eee0d8ac9_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_c865cfb03d605666e7822b4eee0d8ac9_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_c865cfb03d605666e7822b4eee0d8ac9_2 == cache_frame_c865cfb03d605666e7822b4eee0d8ac9_2 )
        {
            Py_DECREF( frame_c865cfb03d605666e7822b4eee0d8ac9_2 );
        }
        cache_frame_c865cfb03d605666e7822b4eee0d8ac9_2 = NULL;

        assertFrameObject( frame_c865cfb03d605666e7822b4eee0d8ac9_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_42, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_3 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_16 = const_str_plain__Connector;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_16 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_16 );
            tmp_tuple_element_16 = locals_tornado$tcpclient_42;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 42;
            tmp_assign_source_37 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_37;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_36 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_36 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_tornado$tcpclient_42 );
        locals_tornado$tcpclient_42 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$tcpclient_42 );
        locals_tornado$tcpclient_42 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 42;
        goto try_except_handler_4;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain__Connector, tmp_assign_source_36 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_dircall_arg1_2;
        tmp_dircall_arg1_2 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_38 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_38;
    }
    {
        PyObject *tmp_assign_source_39;
        tmp_assign_source_39 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_39;
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_10;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_23;
        PyObject *tmp_subscript_name_23;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        tmp_condition_result_10 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_23 = tmp_class_creation_2__bases;
        tmp_subscript_name_23 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_23, tmp_subscript_name_23, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_5:;
        condexpr_end_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_40 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_40;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_7;
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_source_name_16;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_16 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_16, const_str_plain___prepare__ );
        tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_17;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_17;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_17 = tmp_class_creation_2__metaclass;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain___prepare__ );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;

                goto try_except_handler_7;
            }
            tmp_tuple_element_17 = const_str_plain_TCPClient;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_17 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_17 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_17 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_17 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 203;
            tmp_assign_source_41 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_41 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;

                goto try_except_handler_7;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_41;
        }
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_18;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_18 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_18, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;

                goto try_except_handler_7;
            }
            tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_18;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_19;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_18 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 203;

                    goto try_except_handler_7;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_18 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_19 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_19 == NULL) );
                tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_19 );
                if ( tmp_tuple_element_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 203;

                    goto try_except_handler_7;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_18 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 203;

                    goto try_except_handler_7;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 203;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_7;
            }
            branch_no_8:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_assign_source_42;
            tmp_assign_source_42 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_42;
        }
        branch_end_7:;
    }
    {
        PyObject *tmp_assign_source_43;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_tornado$tcpclient_203 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c5a6a5dde27777d16fa4978b5cf11756;
        tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_digest_c24ffa9ac487f488583e3aa88e1a7fde;
        tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_plain_TCPClient;
        tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto try_except_handler_9;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_1ae114085666de15d483afe5f0f6984a_3, codeobj_1ae114085666de15d483afe5f0f6984a, module_tornado$tcpclient, sizeof(void *) );
        frame_1ae114085666de15d483afe5f0f6984a_3 = cache_frame_1ae114085666de15d483afe5f0f6984a_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_1ae114085666de15d483afe5f0f6984a_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_1ae114085666de15d483afe5f0f6984a_3 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_2;
            PyObject *tmp_annotations_14;
            PyObject *tmp_dict_key_22;
            PyObject *tmp_dict_value_22;
            PyObject *tmp_mvar_value_48;
            PyObject *tmp_dict_key_23;
            PyObject *tmp_dict_value_23;
            tmp_defaults_2 = const_tuple_none_tuple;
            tmp_dict_key_22 = const_str_plain_resolver;
            tmp_dict_value_22 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_Resolver );

            if ( tmp_dict_value_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Resolver );

                if (unlikely( tmp_mvar_value_48 == NULL ))
                {
                    tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Resolver );
                }

                if ( tmp_mvar_value_48 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Resolver" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 210;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_dict_value_22 = tmp_mvar_value_48;
                Py_INCREF( tmp_dict_value_22 );
                }
            }

            tmp_annotations_14 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_22, tmp_dict_value_22 );
            Py_DECREF( tmp_dict_value_22 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_23 = const_str_plain_return;
            tmp_dict_value_23 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_23, tmp_dict_value_23 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_13___init__( tmp_defaults_2, tmp_annotations_14 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_annotations_15;
            tmp_annotations_15 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_14_close( tmp_annotations_15 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain_close, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 218;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_3;
            PyObject *tmp_tuple_element_19;
            PyObject *tmp_source_name_20;
            PyObject *tmp_mvar_value_49;
            PyObject *tmp_annotations_16;
            PyObject *tmp_dict_key_24;
            PyObject *tmp_dict_value_24;
            PyObject *tmp_dict_key_25;
            PyObject *tmp_dict_value_25;
            PyObject *tmp_dict_key_26;
            PyObject *tmp_dict_value_26;
            PyObject *tmp_source_name_21;
            PyObject *tmp_mvar_value_50;
            PyObject *tmp_dict_key_27;
            PyObject *tmp_dict_value_27;
            PyObject *tmp_subscribed_name_24;
            PyObject *tmp_mvar_value_51;
            PyObject *tmp_subscript_name_24;
            PyObject *tmp_tuple_element_20;
            PyObject *tmp_subscribed_name_25;
            PyObject *tmp_mvar_value_52;
            PyObject *tmp_subscript_name_25;
            PyObject *tmp_tuple_element_21;
            PyObject *tmp_mvar_value_53;
            PyObject *tmp_source_name_22;
            PyObject *tmp_mvar_value_54;
            PyObject *tmp_dict_key_28;
            PyObject *tmp_dict_value_28;
            PyObject *tmp_dict_key_29;
            PyObject *tmp_dict_value_29;
            PyObject *tmp_dict_key_30;
            PyObject *tmp_dict_value_30;
            PyObject *tmp_dict_key_31;
            PyObject *tmp_dict_value_31;
            PyObject *tmp_subscribed_name_26;
            PyObject *tmp_mvar_value_55;
            PyObject *tmp_subscript_name_26;
            PyObject *tmp_tuple_element_22;
            PyObject *tmp_source_name_23;
            PyObject *tmp_mvar_value_56;
            PyObject *tmp_dict_key_32;
            PyObject *tmp_dict_value_32;
            PyObject *tmp_mvar_value_57;
            tmp_source_name_20 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_socket );

            if ( tmp_source_name_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_49 == NULL ))
                {
                    tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_49 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 226;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_source_name_20 = tmp_mvar_value_49;
                Py_INCREF( tmp_source_name_20 );
                }
            }

            tmp_tuple_element_19 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_AF_UNSPEC );
            Py_DECREF( tmp_source_name_20 );
            if ( tmp_tuple_element_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 226;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_defaults_3 = PyTuple_New( 6 );
            PyTuple_SET_ITEM( tmp_defaults_3, 0, tmp_tuple_element_19 );
            tmp_tuple_element_19 = Py_None;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_defaults_3, 1, tmp_tuple_element_19 );
            tmp_tuple_element_19 = Py_None;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_defaults_3, 2, tmp_tuple_element_19 );
            tmp_tuple_element_19 = Py_None;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_defaults_3, 3, tmp_tuple_element_19 );
            tmp_tuple_element_19 = Py_None;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_defaults_3, 4, tmp_tuple_element_19 );
            tmp_tuple_element_19 = Py_None;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_defaults_3, 5, tmp_tuple_element_19 );
            tmp_dict_key_24 = const_str_plain_host;
            tmp_dict_value_24 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_str );

            if ( tmp_dict_value_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_24 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_24 );
                }
            }

            tmp_annotations_16 = _PyDict_NewPresized( 9 );
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_24, tmp_dict_value_24 );
            Py_DECREF( tmp_dict_value_24 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_25 = const_str_plain_port;
            tmp_dict_value_25 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_int );

            if ( tmp_dict_value_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_25 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_25 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_25, tmp_dict_value_25 );
            Py_DECREF( tmp_dict_value_25 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_26 = const_str_plain_af;
            tmp_source_name_21 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_socket );

            if ( tmp_source_name_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_50 == NULL ))
                {
                    tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_50 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 226;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_source_name_21 = tmp_mvar_value_50;
                Py_INCREF( tmp_source_name_21 );
                }
            }

            tmp_dict_value_26 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_21 );
            if ( tmp_dict_value_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_3 );
                Py_DECREF( tmp_annotations_16 );

                exception_lineno = 226;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_26, tmp_dict_value_26 );
            Py_DECREF( tmp_dict_value_26 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_27 = const_str_plain_ssl_options;
            tmp_subscribed_name_24 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_Union );

            if ( tmp_subscribed_name_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_51 == NULL ))
                {
                    tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_51 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 227;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_24 = tmp_mvar_value_51;
                Py_INCREF( tmp_subscribed_name_24 );
                }
            }

            tmp_subscribed_name_25 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_Dict );

            if ( tmp_subscribed_name_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_52 == NULL ))
                {
                    tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_52 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    Py_DECREF( tmp_subscribed_name_24 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 227;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_25 = tmp_mvar_value_52;
                Py_INCREF( tmp_subscribed_name_25 );
                }
            }

            tmp_tuple_element_21 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_str );

            if ( tmp_tuple_element_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_21 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_21 );
                }
            }

            tmp_subscript_name_25 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_25, 0, tmp_tuple_element_21 );
            tmp_tuple_element_21 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_Any );

            if ( tmp_tuple_element_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_53 == NULL ))
                {
                    tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_53 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    Py_DECREF( tmp_subscribed_name_24 );
                    Py_DECREF( tmp_subscribed_name_25 );
                    Py_DECREF( tmp_subscript_name_25 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 227;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_21 = tmp_mvar_value_53;
                Py_INCREF( tmp_tuple_element_21 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_25, 1, tmp_tuple_element_21 );
            tmp_tuple_element_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_25, tmp_subscript_name_25 );
            Py_DECREF( tmp_subscribed_name_25 );
            Py_DECREF( tmp_subscript_name_25 );
            if ( tmp_tuple_element_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_3 );
                Py_DECREF( tmp_annotations_16 );
                Py_DECREF( tmp_subscribed_name_24 );

                exception_lineno = 227;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_subscript_name_24 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_24, 0, tmp_tuple_element_20 );
            tmp_source_name_22 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_ssl );

            if ( tmp_source_name_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_ssl );

                if (unlikely( tmp_mvar_value_54 == NULL ))
                {
                    tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ssl );
                }

                if ( tmp_mvar_value_54 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    Py_DECREF( tmp_subscribed_name_24 );
                    Py_DECREF( tmp_subscript_name_24 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ssl" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 227;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_source_name_22 = tmp_mvar_value_54;
                Py_INCREF( tmp_source_name_22 );
                }
            }

            tmp_tuple_element_20 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_SSLContext );
            Py_DECREF( tmp_source_name_22 );
            if ( tmp_tuple_element_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_3 );
                Py_DECREF( tmp_annotations_16 );
                Py_DECREF( tmp_subscribed_name_24 );
                Py_DECREF( tmp_subscript_name_24 );

                exception_lineno = 227;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_24, 1, tmp_tuple_element_20 );
            tmp_dict_value_27 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_24, tmp_subscript_name_24 );
            Py_DECREF( tmp_subscribed_name_24 );
            Py_DECREF( tmp_subscript_name_24 );
            if ( tmp_dict_value_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_3 );
                Py_DECREF( tmp_annotations_16 );

                exception_lineno = 227;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_27, tmp_dict_value_27 );
            Py_DECREF( tmp_dict_value_27 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_28 = const_str_plain_max_buffer_size;
            tmp_dict_value_28 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_int );

            if ( tmp_dict_value_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_28 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_28 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_28, tmp_dict_value_28 );
            Py_DECREF( tmp_dict_value_28 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_29 = const_str_plain_source_ip;
            tmp_dict_value_29 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_str );

            if ( tmp_dict_value_29 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_29 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_29 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_29, tmp_dict_value_29 );
            Py_DECREF( tmp_dict_value_29 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_30 = const_str_plain_source_port;
            tmp_dict_value_30 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_int );

            if ( tmp_dict_value_30 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_30 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_30 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_30, tmp_dict_value_30 );
            Py_DECREF( tmp_dict_value_30 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_31 = const_str_plain_timeout;
            tmp_subscribed_name_26 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_Union );

            if ( tmp_subscribed_name_26 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_55 == NULL ))
                {
                    tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_55 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 231;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_26 = tmp_mvar_value_55;
                Py_INCREF( tmp_subscribed_name_26 );
                }
            }

            tmp_tuple_element_22 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_float );

            if ( tmp_tuple_element_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_22 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_tuple_element_22 );
                }
            }

            tmp_subscript_name_26 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_26, 0, tmp_tuple_element_22 );
            tmp_source_name_23 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_datetime );

            if ( tmp_source_name_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_datetime );

                if (unlikely( tmp_mvar_value_56 == NULL ))
                {
                    tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
                }

                if ( tmp_mvar_value_56 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    Py_DECREF( tmp_subscribed_name_26 );
                    Py_DECREF( tmp_subscript_name_26 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 231;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_source_name_23 = tmp_mvar_value_56;
                Py_INCREF( tmp_source_name_23 );
                }
            }

            tmp_tuple_element_22 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_timedelta );
            Py_DECREF( tmp_source_name_23 );
            if ( tmp_tuple_element_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_3 );
                Py_DECREF( tmp_annotations_16 );
                Py_DECREF( tmp_subscribed_name_26 );
                Py_DECREF( tmp_subscript_name_26 );

                exception_lineno = 231;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_26, 1, tmp_tuple_element_22 );
            tmp_dict_value_31 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_26, tmp_subscript_name_26 );
            Py_DECREF( tmp_subscribed_name_26 );
            Py_DECREF( tmp_subscript_name_26 );
            if ( tmp_dict_value_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_defaults_3 );
                Py_DECREF( tmp_annotations_16 );

                exception_lineno = 231;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_31, tmp_dict_value_31 );
            Py_DECREF( tmp_dict_value_31 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_32 = const_str_plain_return;
            tmp_dict_value_32 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_IOStream );

            if ( tmp_dict_value_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOStream );

                if (unlikely( tmp_mvar_value_57 == NULL ))
                {
                    tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOStream );
                }

                if ( tmp_mvar_value_57 == NULL )
                {
                    Py_DECREF( tmp_defaults_3 );
                    Py_DECREF( tmp_annotations_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOStream" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 232;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_dict_value_32 = tmp_mvar_value_57;
                Py_INCREF( tmp_dict_value_32 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_32, tmp_dict_value_32 );
            Py_DECREF( tmp_dict_value_32 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_15_connect( tmp_defaults_3, tmp_annotations_16 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain_connect, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_4;
            PyObject *tmp_annotations_17;
            PyObject *tmp_dict_key_33;
            PyObject *tmp_dict_value_33;
            PyObject *tmp_dict_key_34;
            PyObject *tmp_dict_value_34;
            PyObject *tmp_source_name_24;
            PyObject *tmp_mvar_value_58;
            PyObject *tmp_dict_key_35;
            PyObject *tmp_dict_value_35;
            PyObject *tmp_mvar_value_59;
            PyObject *tmp_dict_key_36;
            PyObject *tmp_dict_value_36;
            PyObject *tmp_dict_key_37;
            PyObject *tmp_dict_value_37;
            PyObject *tmp_dict_key_38;
            PyObject *tmp_dict_value_38;
            PyObject *tmp_subscribed_name_27;
            PyObject *tmp_mvar_value_60;
            PyObject *tmp_subscript_name_27;
            PyObject *tmp_tuple_element_23;
            PyObject *tmp_mvar_value_61;
            tmp_defaults_4 = const_tuple_none_none_tuple;
            tmp_dict_key_33 = const_str_plain_max_buffer_size;
            tmp_dict_value_33 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_int );

            if ( tmp_dict_value_33 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_33 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_33 );
                }
            }

            tmp_annotations_17 = _PyDict_NewPresized( 6 );
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_33, tmp_dict_value_33 );
            Py_DECREF( tmp_dict_value_33 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_34 = const_str_plain_af;
            tmp_source_name_24 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_socket );

            if ( tmp_source_name_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_socket );

                if (unlikely( tmp_mvar_value_58 == NULL ))
                {
                    tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
                }

                if ( tmp_mvar_value_58 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 301;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_source_name_24 = tmp_mvar_value_58;
                Py_INCREF( tmp_source_name_24 );
                }
            }

            tmp_dict_value_34 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_AddressFamily );
            Py_DECREF( tmp_source_name_24 );
            if ( tmp_dict_value_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_17 );

                exception_lineno = 301;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_34, tmp_dict_value_34 );
            Py_DECREF( tmp_dict_value_34 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_35 = const_str_plain_addr;
            tmp_dict_value_35 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_Tuple );

            if ( tmp_dict_value_35 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_59 == NULL ))
                {
                    tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_59 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 302;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_dict_value_35 = tmp_mvar_value_59;
                Py_INCREF( tmp_dict_value_35 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_35, tmp_dict_value_35 );
            Py_DECREF( tmp_dict_value_35 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_36 = const_str_plain_source_ip;
            tmp_dict_value_36 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_str );

            if ( tmp_dict_value_36 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_36 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_36 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_36, tmp_dict_value_36 );
            Py_DECREF( tmp_dict_value_36 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_37 = const_str_plain_source_port;
            tmp_dict_value_37 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_int );

            if ( tmp_dict_value_37 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_37 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_37 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_37, tmp_dict_value_37 );
            Py_DECREF( tmp_dict_value_37 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_38 = const_str_plain_return;
            tmp_subscribed_name_27 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_Tuple );

            if ( tmp_subscribed_name_27 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_Tuple );

                if (unlikely( tmp_mvar_value_60 == NULL ))
                {
                    tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Tuple );
                }

                if ( tmp_mvar_value_60 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Tuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 305;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_27 = tmp_mvar_value_60;
                Py_INCREF( tmp_subscribed_name_27 );
                }
            }

            tmp_tuple_element_23 = PyObject_GetItem( locals_tornado$tcpclient_203, const_str_plain_IOStream );

            if ( tmp_tuple_element_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_IOStream );

                if (unlikely( tmp_mvar_value_61 == NULL ))
                {
                    tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOStream );
                }

                if ( tmp_mvar_value_61 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    Py_DECREF( tmp_subscribed_name_27 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOStream" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 305;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_23 = tmp_mvar_value_61;
                Py_INCREF( tmp_tuple_element_23 );
                }
            }

            tmp_subscript_name_27 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_27, 0, tmp_tuple_element_23 );
            tmp_tuple_element_23 = const_str_digest_d057d99810b1c539102c2397e91f2552;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_subscript_name_27, 1, tmp_tuple_element_23 );
            tmp_dict_value_38 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_27, tmp_subscript_name_27 );
            Py_DECREF( tmp_subscribed_name_27 );
            Py_DECREF( tmp_subscript_name_27 );
            if ( tmp_dict_value_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_17 );

                exception_lineno = 305;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_38, tmp_dict_value_38 );
            Py_DECREF( tmp_dict_value_38 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_4 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$tcpclient$$$function_16__create_stream( tmp_defaults_4, tmp_annotations_17 );



            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain__create_stream, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 298;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1ae114085666de15d483afe5f0f6984a_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1ae114085666de15d483afe5f0f6984a_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_1ae114085666de15d483afe5f0f6984a_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_1ae114085666de15d483afe5f0f6984a_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_1ae114085666de15d483afe5f0f6984a_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_1ae114085666de15d483afe5f0f6984a_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_1ae114085666de15d483afe5f0f6984a_3 == cache_frame_1ae114085666de15d483afe5f0f6984a_3 )
        {
            Py_DECREF( frame_1ae114085666de15d483afe5f0f6984a_3 );
        }
        cache_frame_1ae114085666de15d483afe5f0f6984a_3 = NULL;

        assertFrameObject( frame_1ae114085666de15d483afe5f0f6984a_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_9;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_14;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            tmp_compexpr_right_2 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;

                goto try_except_handler_9;
            }
            tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$tcpclient_203, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;

                goto try_except_handler_9;
            }
            branch_no_9:;
        }
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_24;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_5 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_24 = const_str_plain_TCPClient;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_24 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_24 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_24 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_24 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_24 );
            tmp_tuple_element_24 = locals_tornado$tcpclient_203;
            Py_INCREF( tmp_tuple_element_24 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_24 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame.f_lineno = 203;
            tmp_assign_source_44 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;

                goto try_except_handler_9;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_44;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_43 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_43 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        Py_DECREF( locals_tornado$tcpclient_203 );
        locals_tornado$tcpclient_203 = NULL;
        goto try_return_handler_8;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$tcpclient_203 );
        locals_tornado$tcpclient_203 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$tcpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 203;
        goto try_except_handler_7;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_tornado$tcpclient, (Nuitka_StringObject *)const_str_plain_TCPClient, tmp_assign_source_43 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b8ffbaad84d3ceef21adcb2f3812e3f );
#endif
    popFrameStack();

    assertFrameObject( frame_7b8ffbaad84d3ceef21adcb2f3812e3f );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b8ffbaad84d3ceef21adcb2f3812e3f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7b8ffbaad84d3ceef21adcb2f3812e3f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7b8ffbaad84d3ceef21adcb2f3812e3f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7b8ffbaad84d3ceef21adcb2f3812e3f, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;


    return MOD_RETURN_VALUE( module_tornado$tcpclient );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
