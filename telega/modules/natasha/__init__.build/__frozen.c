// This provides the frozen (compiled bytecode) files that are included if
// any.
#include <Python.h>

#include "nuitka/constants_blob.h"

// Blob from which modules are unstreamed.
#define stream_data constant_bin

// These modules should be loaded as bytecode. They may e.g. have to be loadable
// during "Py_Initialize" already, or for irrelevance, they are only included
// in this un-optimized form. These are not compiled by Nuitka, and therefore
// are not accelerated at all, merely bundled with the binary or module, so
// that CPython library can start out finding them.

struct frozen_desc {
    char const *name;
    ssize_t start;
    int size;
};

void copyFrozenModulesTo( struct _frozen *destination )
{
    struct frozen_desc frozen_modules[] = {
        { "_collections_abc", 7155578, 28975 },
        { "_compression", 15697684, 4157 },
        { "_weakrefset", 15701841, 7495 },
        { "abc", 7184553, 6484 },
        { "base64", 7265819, 17021 },
        { "bz2", 7309998, 11214 },
        { "codecs", 7380477, 33931 },
        { "collections", 7420731, -46643 },
        { "collections.abc", 7155578, 28975 },
        { "copyreg", 7544982, 4277 },
        { "dis", 7703669, 15238 },
        { "encodings", 8121421, -3980 },
        { "encodings.aliases", 15709336, 6329 },
        { "encodings.ascii", 15715665, 1917 },
        { "encodings.base64_codec", 15717582, 2456 },
        { "encodings.big5", 15720038, 1477 },
        { "encodings.big5hkscs", 15721515, 1487 },
        { "encodings.bz2_codec", 15723002, 3318 },
        { "encodings.charmap", 15726320, 2970 },
        { "encodings.cp037", 15729290, 2462 },
        { "encodings.cp1006", 15731752, 2538 },
        { "encodings.cp1026", 15734290, 2466 },
        { "encodings.cp1125", 15736756, 8159 },
        { "encodings.cp1140", 15744915, 2452 },
        { "encodings.cp1250", 15747367, 2489 },
        { "encodings.cp1251", 15749856, 2486 },
        { "encodings.cp1252", 15752342, 2489 },
        { "encodings.cp1253", 15754831, 2502 },
        { "encodings.cp1254", 15757333, 2491 },
        { "encodings.cp1255", 15759824, 2510 },
        { "encodings.cp1256", 15762334, 2488 },
        { "encodings.cp1257", 15764822, 2496 },
        { "encodings.cp1258", 15767318, 2494 },
        { "encodings.cp273", 15769812, 2448 },
        { "encodings.cp424", 15772260, 2492 },
        { "encodings.cp437", 15774752, 7876 },
        { "encodings.cp500", 15782628, 2462 },
        { "encodings.cp720", 15785090, 2559 },
        { "encodings.cp737", 15787649, 8198 },
        { "encodings.cp775", 15795847, 7906 },
        { "encodings.cp850", 15803753, 7537 },
        { "encodings.cp852", 15811290, 7914 },
        { "encodings.cp855", 15819204, 8167 },
        { "encodings.cp856", 15827371, 2524 },
        { "encodings.cp857", 15829895, 7519 },
        { "encodings.cp858", 15837414, 7507 },
        { "encodings.cp860", 15844921, 7855 },
        { "encodings.cp861", 15852776, 7870 },
        { "encodings.cp862", 15860646, 8059 },
        { "encodings.cp863", 15868705, 7870 },
        { "encodings.cp864", 15876575, 8016 },
        { "encodings.cp865", 15884591, 7870 },
        { "encodings.cp866", 15892461, 8203 },
        { "encodings.cp869", 15900664, 7896 },
        { "encodings.cp874", 15908560, 2590 },
        { "encodings.cp875", 15911150, 2459 },
        { "encodings.cp932", 15913609, 1479 },
        { "encodings.cp949", 15915088, 1479 },
        { "encodings.cp950", 15916567, 1479 },
        { "encodings.euc_jis_2004", 15918046, 1493 },
        { "encodings.euc_jisx0213", 15919539, 1493 },
        { "encodings.euc_jp", 15921032, 1481 },
        { "encodings.euc_kr", 15922513, 1481 },
        { "encodings.gb18030", 15923994, 1483 },
        { "encodings.gb2312", 15925477, 1481 },
        { "encodings.gbk", 15926958, 1475 },
        { "encodings.hex_codec", 15928433, 2443 },
        { "encodings.hp_roman8", 15930876, 2663 },
        { "encodings.hz", 15933539, 1473 },
        { "encodings.idna", 8125401, 5757 },
        { "encodings.iso2022_jp", 15935012, 1494 },
        { "encodings.iso2022_jp_1", 15936506, 1498 },
        { "encodings.iso2022_jp_2", 15938004, 1498 },
        { "encodings.iso2022_jp_2004", 15939502, 1504 },
        { "encodings.iso2022_jp_3", 15941006, 1498 },
        { "encodings.iso2022_jp_ext", 15942504, 1502 },
        { "encodings.iso2022_kr", 15944006, 1494 },
        { "encodings.iso8859_1", 15945500, 2461 },
        { "encodings.iso8859_10", 15947961, 2466 },
        { "encodings.iso8859_11", 15950427, 2560 },
        { "encodings.iso8859_13", 15952987, 2469 },
        { "encodings.iso8859_14", 15955456, 2487 },
        { "encodings.iso8859_15", 15957943, 2466 },
        { "encodings.iso8859_16", 15960409, 2468 },
        { "encodings.iso8859_2", 15962877, 2461 },
        { "encodings.iso8859_3", 15965338, 2468 },
        { "encodings.iso8859_4", 15967806, 2461 },
        { "encodings.iso8859_5", 15970267, 2462 },
        { "encodings.iso8859_6", 15972729, 2506 },
        { "encodings.iso8859_7", 15975235, 2469 },
        { "encodings.iso8859_8", 15977704, 2500 },
        { "encodings.iso8859_9", 15980204, 2461 },
        { "encodings.johab", 15982665, 1479 },
        { "encodings.koi8_r", 15984144, 2513 },
        { "encodings.koi8_t", 15986657, 2424 },
        { "encodings.koi8_u", 15989081, 2499 },
        { "encodings.kz1048", 15991580, 2476 },
        { "encodings.latin_1", 15994056, 1929 },
        { "encodings.mac_arabic", 15995985, 7770 },
        { "encodings.mac_centeuro", 16003755, 2500 },
        { "encodings.mac_croatian", 16006255, 2508 },
        { "encodings.mac_cyrillic", 16008763, 2498 },
        { "encodings.mac_farsi", 16011261, 2442 },
        { "encodings.mac_greek", 16013703, 2482 },
        { "encodings.mac_iceland", 16016185, 2501 },
        { "encodings.mac_latin2", 16018686, 2642 },
        { "encodings.mac_roman", 16021328, 2499 },
        { "encodings.mac_romanian", 16023827, 2509 },
        { "encodings.mac_turkish", 16026336, 2502 },
        { "encodings.palmos", 16028838, 2489 },
        { "encodings.ptcp154", 16031327, 2583 },
        { "encodings.punycode", 16033910, 6450 },
        { "encodings.quopri_codec", 16040360, 2476 },
        { "encodings.raw_unicode_escape", 16042836, 1802 },
        { "encodings.rot_13", 16044638, 3062 },
        { "encodings.shift_jis", 16047700, 1487 },
        { "encodings.shift_jis_2004", 16049187, 1497 },
        { "encodings.shift_jisx0213", 16050684, 1497 },
        { "encodings.tis_620", 16052181, 2551 },
        { "encodings.undefined", 16054732, 2196 },
        { "encodings.unicode_escape", 16056928, 1782 },
        { "encodings.unicode_internal", 16058710, 1792 },
        { "encodings.utf_16", 16060502, 4866 },
        { "encodings.utf_16_be", 16065368, 1667 },
        { "encodings.utf_16_le", 16067035, 1667 },
        { "encodings.utf_32", 16068702, 4759 },
        { "encodings.utf_32_be", 16073461, 1560 },
        { "encodings.utf_32_le", 16075021, 1560 },
        { "encodings.utf_7", 16076581, 1588 },
        { "encodings.utf_8", 16078169, 1647 },
        { "encodings.utf_8_sig", 16079816, 4549 },
        { "encodings.uu_codec", 16084365, 3258 },
        { "encodings.zlib_codec", 16087623, 3156 },
        { "enum", 8131158, 24304 },
        { "functools", 8198864, 23988 },
        { "genericpath", 16090779, 3781 },
        { "heapq", 8275656, 14395 },
        { "importlib", 8426368, -3765 },
        { "importlib._bootstrap", 8430133, 29211 },
        { "importlib._bootstrap_external", 16094560, 41851 },
        { "importlib.machinery", 8459344, 1005 },
        { "inspect", 8469738, 80065 },
        { "io", 8549803, 3442 },
        { "keyword", 8628643, 1842 },
        { "linecache", 8630485, 3822 },
        { "locale", 8634307, 34588 },
        { "opcode", 9606857, 5411 },
        { "operator", 9612268, 13933 },
        { "os", 9674127, 29726 },
        { "posixpath", 10387509, 10463 },
        { "quopri", 16136411, 5804 },
        { "re", 10911462, 13837 },
        { "reprlib", 10925299, 5383 },
        { "sre_compile", 16142215, 15236 },
        { "sre_constants", 11359937, 6324 },
        { "sre_parse", 16157451, 21390 },
        { "stat", 11406065, 3906 },
        { "stringprep", 16178841, 10065 },
        { "struct", 11417839, 367 },
        { "threading", 11570178, 37735 },
        { "token", 11824768, 3632 },
        { "tokenize", 11828400, 17864 },
        { "traceback", 11846264, 19656 },
        { "types", 11867047, 9009 },
        { "warnings", 12169462, 13973 },
        { NULL, 0, 0 }
    };

    struct frozen_desc *current = frozen_modules;

    for(;;)
    {
        destination->name = (char *)current->name;
        destination->code = (unsigned char *)&constant_bin[ current->start ];
        destination->size = current->size;

        if (destination->name == NULL) break;

        current += 1;
        destination += 1;
    };
}
