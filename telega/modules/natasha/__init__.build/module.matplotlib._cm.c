/* Generated code for Python module 'matplotlib._cm'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$_cm" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$_cm;
PyDictObject *moduledict_matplotlib$_cm;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain_RdPu;
static PyObject *const_dict_3adc72b697f1d7d162e0d950131b4b5a;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain__g33;
static PyObject *const_str_plain__YlGnBu_data;
static PyObject *const_float_1_78277;
static PyObject *const_str_plain__PiYG_data;
extern PyObject *const_str_plain_gamma;
extern PyObject *const_str_plain_i;
static PyObject *const_dict_d6323100fc0b68b2862861068aa9dfb4;
static PyObject *const_str_plain__wistia_data;
static PyObject *const_str_plain_BuGn;
static PyObject *const_str_plain__gist_yarg_data;
extern PyObject *const_float_minus_1_5;
static PyObject *const_tuple_str_plain_gamma_str_plain_s_str_plain_r_str_plain_h_tuple;
static PyObject *const_str_plain__jet_data;
static PyObject *const_str_plain__flag_data;
static PyObject *const_tuple_db9a981db24a26ea90b278665de2841d_tuple;
static PyObject *const_str_plain__g5;
extern PyObject *const_str_plain_m;
static PyObject *const_str_plain_ocean;
static PyObject *const_dict_ac09b61043b30f22f2e956405ad79658;
extern PyObject *const_int_pos_5;
static PyObject *const_str_plain__RdGy_data;
extern PyObject *const_str_plain_cool;
extern PyObject *const_int_pos_32;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_plain__g6;
static PyObject *const_str_plain_YlOrBr;
static PyObject *const_dict_8f097861f42f3010bd8f6de6022ffc3c;
static PyObject *const_str_plain__brg_data;
static PyObject *const_str_plain__g12;
static PyObject *const_str_plain__g26;
static PyObject *const_str_plain__gist_gray_data;
extern PyObject *const_str_plain_numpy;
extern PyObject *const_str_plain_red;
static PyObject *const_str_plain_OrRd;
extern PyObject *const_str_plain_listed;
static PyObject *const_str_plain_tab10;
extern PyObject *const_int_neg_2;
extern PyObject *const_float_1_0;
static PyObject *const_str_digest_0870d49f1cf88348cb56a7f4adc73408;
static PyObject *const_str_plain_gist_stern;
static PyObject *const_str_plain__g22;
static PyObject *const_str_plain__gist_heat_green;
extern PyObject *const_float_0_67;
static PyObject *const_str_plain__Pastel1_data;
static PyObject *const_tuple_ac83b7f87b65db8bc61c88a0c684aae6_tuple;
static PyObject *const_str_plain__flag_red;
static PyObject *const_str_plain__BuPu_data;
static PyObject *const_str_plain_PuBu;
static PyObject *const_str_plain__g23;
static PyObject *const_tuple_a1c2e0d0fdb72592d26c53db2b423104_tuple;
static PyObject *const_str_plain__autumn_data;
extern PyObject *const_str_plain_False;
static PyObject *const_str_plain__g16;
static PyObject *const_str_plain__BuGn_data;
extern PyObject *const_str_plain_green;
static PyObject *const_str_plain__g9;
static PyObject *const_str_plain__bwr_data;
static PyObject *const_str_plain__CMRmap_data;
static PyObject *const_dict_13dcc09e8b14458a914dc22a80eeee13;
extern PyObject *const_int_0;
static PyObject *const_str_plain__g4;
extern PyObject *const_str_plain_p1;
static PyObject *const_dict_1b20bf7e6394a197db2b925f2ae9f1f2;
static PyObject *const_str_plain_gist_heat;
static PyObject *const_tuple_dbd02ca2a43f79960ecaa20263657c10_tuple;
static PyObject *const_str_plain__gist_earth_data;
static PyObject *const_str_plain__tab20_data;
static PyObject *const_str_plain_terrain;
static PyObject *const_str_plain__Oranges_data;
extern PyObject *const_int_pos_36;
extern PyObject *const_int_pos_4;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_functools;
static PyObject *const_float_minus_1_1;
static PyObject *const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple;
extern PyObject *const_str_plain_partial;
extern PyObject *const_int_pos_1;
static PyObject *const_dict_7ba6c40e971dab085fcf28da729f7167;
static PyObject *const_tuple_float_1_0_float_0_5_float_minus_1_5_float_1_0_tuple;
static PyObject *const_str_plain__ch_helper;
static PyObject *const_float_0_33;
static PyObject *const_tuple_9e6a97cefbfc93e4dbd6fe4790754edf_tuple;
static PyObject *const_str_plain__afmhot_data;
static PyObject *const_float_20_9;
extern PyObject *const_str_plain_hsv;
extern PyObject *const_str_plain_flag;
extern PyObject *const_str_plain_format;
static PyObject *const_str_plain_gnuplot;
static PyObject *const_str_plain__YlOrBr_data;
static PyObject *const_str_plain__tab20c_data;
extern PyObject *const_str_plain_blue;
static PyObject *const_str_plain_tab20;
static PyObject *const_str_plain_Pastel1;
static PyObject *const_str_plain_coolwarm;
static PyObject *const_str_plain_Set3;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_h;
static PyObject *const_str_plain__cool_data;
static PyObject *const_str_plain__prism_green;
extern PyObject *const_str_digest_70f25990f64eb086931fae485976330d;
static PyObject *const_str_plain__prism_red;
static PyObject *const_float_1_84;
static PyObject *const_tuple_9b04358fc83d8c29f6d13e55a485e086_tuple;
static PyObject *const_tuple_e22ad1a6fd2b90375e84cff21f64303a_tuple;
static PyObject *const_str_plain__g19;
static PyObject *const_tuple_ef7272d3c529510edf3b0815822dbaa3_tuple;
static PyObject *const_dict_46fdda9c2313b830d68498039918be48;
static PyObject *const_tuple_122f654ebd563953484696204c943f8b_tuple;
static PyObject *const_str_plain_gist_yarg;
static PyObject *const_str_plain__gnuplot2_data;
extern PyObject *const_float_0_25;
static PyObject *const_dict_26510cda7d1dc75818c57eac9563d1d1;
extern PyObject *const_str_plain_Accent;
extern PyObject *const_float_0_5;
static PyObject *const_str_plain__g13;
static PyObject *const_tuple_6eb90b29c730cddab6ab1d936156a49c_tuple;
static PyObject *const_str_plain__g31;
static PyObject *const_tuple_de81471e775d0db13ccc93277369c0bb_tuple;
static PyObject *const_str_plain__flag_green;
static PyObject *const_tuple_2a4c4f5b12a7944706ab0b855d978f15_tuple;
static PyObject *const_tuple_4d8a3db908fea5ac787eae57a231d107_tuple;
extern PyObject *const_int_pos_33;
static PyObject *const_tuple_fb0dd03a0e957d10a507ea76b9c3ad27_tuple;
static PyObject *const_str_plain__g30;
static PyObject *const_str_plain_gist_gray;
extern PyObject *const_str_plain_bone;
static PyObject *const_tuple_1cf08670e58213d33381f121283ddef7_tuple;
static PyObject *const_str_plain__seismic_data;
static PyObject *const_str_plain__gist_yarg;
static PyObject *const_str_plain__PRGn_data;
static PyObject *const_str_plain_RdGy;
static PyObject *const_str_plain__bone_data;
static PyObject *const_str_plain_Paired;
static PyObject *const_str_plain__g0;
extern PyObject *const_tuple_float_1_0_float_1_0_float_1_0_tuple;
static PyObject *const_str_plain__g15;
static PyObject *const_str_plain__winter_data;
static PyObject *const_str_plain_bwr;
extern PyObject *const_int_pos_34;
static PyObject *const_str_plain__Set1_data;
static PyObject *const_float_minus_0_29227;
static PyObject *const_str_plain__g2;
static PyObject *const_dict_61e4a4a8cd2b4181694d4cfb1751640d;
static PyObject *const_str_plain_Set1;
static PyObject *const_str_plain__gist_heat_data;
extern PyObject *const_str_plain_abs;
static PyObject *const_str_plain_YlGn;
static PyObject *const_str_plain__BrBG_data;
static PyObject *const_str_plain_gfunc;
static PyObject *const_tuple_13493111d1a4f8c8a058ebd7f95c09b1_tuple;
static PyObject *const_tuple_9fa91c64747bb6ff4492053856cc06e7_tuple;
static PyObject *const_str_digest_46ac0c36bf895f540a2118bdf4f973be;
static PyObject *const_str_plain__Set3_data;
static PyObject *const_str_plain__terrain_data;
extern PyObject *const_int_pos_15;
static PyObject *const_dict_a6becc14d04a47becd62ae70b4c4482c;
static PyObject *const_str_plain__Purples_data;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_prism;
static PyObject *const_dict_79ecbf763b054a3ad895cae3fb89c58e;
static PyObject *const_tuple_d6bde7eec8df9cf0ee10c9abbaa88fc9_tuple;
static PyObject *const_str_plain_PRGn;
extern PyObject *const_str_plain_gray;
extern PyObject *const_str_plain_autumn;
extern PyObject *const_str_plain_copper;
static PyObject *const_tuple_float_0_5_float_0_0_float_0_0_tuple;
static PyObject *const_str_plain__gist_rainbow_data;
static PyObject *const_dict_cc81f270cbb7633b5af45d15d4c591ec;
static PyObject *const_str_plain_RdYlGn;
static PyObject *const_float_1_97294;
static PyObject *const_str_digest_36fea1563816aa379d3cd2c1ac36c445;
static PyObject *const_str_plain_gnuplot2;
extern PyObject *const_str_plain_zeros;
static PyObject *const_tuple_float_0_0_float_0_0_float_0_3_tuple;
static PyObject *const_str_digest_0ec62d994d1e365c7efad4dabaaba134;
extern PyObject *const_str_plain_xg;
static PyObject *const_str_plain_Oranges;
static PyObject *const_float_0_78125;
static PyObject *const_str_plain__prism_data;
static PyObject *const_dict_e5f8cd4b8b884f01b9dbdb6c0cf95972;
static PyObject *const_str_plain__Spectral_data;
extern PyObject *const_str_plain_hot;
static PyObject *const_str_plain__g36;
static PyObject *const_xrange_0_37;
extern PyObject *const_str_plain_has_location;
static PyObject *const_tuple_b84fc382b1c73eb24052e00184983189_tuple;
extern PyObject *const_float_0_0;
static PyObject *const_str_plain__YlGn_data;
static PyObject *const_str_plain__g21;
static PyObject *const_str_plain__copper_data;
static PyObject *const_tuple_4f66db73ea9fdbfdceb9e620df1e4919_tuple;
static PyObject *const_str_plain_Purples;
extern PyObject *const_tuple_str_plain_i_tuple;
static PyObject *const_tuple_20bd1f5c0a0f9a93d11e666cd7289af4_tuple;
extern PyObject *const_float_1_5;
static PyObject *const_str_plain_phi;
extern PyObject *const_str_plain_winter;
static PyObject *const_str_plain__summer_data;
static PyObject *const_str_plain__RdYlGn_data;
static PyObject *const_tuple_8a21be97bcec74887e2b70da615f70d9_tuple;
static PyObject *const_tuple_440d7807917857d4ae4d4cd0c76093e7_tuple;
static PyObject *const_str_plain_Greys;
extern PyObject *const_int_pos_31;
extern PyObject *const_int_pos_23;
static PyObject *const_tuple_194951aabc8c4d0f914fa1c18e921dac_tuple;
static PyObject *const_str_plain__g28;
static PyObject *const_str_plain_YlGnBu;
static PyObject *const_tuple_58dec871a15dc46004a9c29f566fd8e9_tuple;
static PyObject *const_str_plain_PiYG;
static PyObject *const_str_plain_seismic;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain__Greens_data;
static PyObject *const_dict_e67366b36b736b2b1824e4124103e620;
extern PyObject *const_float_0_08;
extern PyObject *const_str_plain_s;
static PyObject *const_str_plain_gist_earth;
static PyObject *const_str_plain__Reds_data;
extern PyObject *const_str_plain_pi;
static PyObject *const_str_plain__gist_stern_data;
static PyObject *const_str_plain__nipy_spectral_data;
static PyObject *const_str_plain__tab20b_data;
static PyObject *const_str_plain_CMRmap;
static PyObject *const_str_plain_PuBuGn;
static PyObject *const_str_plain__PuOr_data;
extern PyObject *const_tuple_empty;
extern PyObject *const_int_pos_10;
static PyObject *const_tuple_7eb55817b097308a4aa82a88a7a374a0_tuple;
static PyObject *const_dict_139fa967a21793ecdbc3e590120916a2;
static PyObject *const_float_0_92;
static PyObject *const_str_plain__g25;
static PyObject *const_tuple_d09dc699a96e56cc6c61b2a9d13ae443_tuple;
static PyObject *const_str_plain__rainbow_data;
static PyObject *const_str_plain__g27;
static PyObject *const_str_plain__gist_heat_blue;
static PyObject *const_str_plain__g24;
static PyObject *const_float_11_5;
static PyObject *const_tuple_6430e1188db9556163de382028b9d704_tuple;
static PyObject *const_tuple_float_0_0_float_0_0_float_1_0_tuple;
extern PyObject *const_int_pos_37;
static PyObject *const_str_plain_PuRd;
static PyObject *const_str_plain_RdBu;
static PyObject *const_str_plain__spring_data;
static PyObject *const_dict_a065b631920800bd13e7ea9419838858;
extern PyObject *const_str_plain_x;
static PyObject *const_str_plain__g17;
static PyObject *const_str_digest_f73618ed65447e45c026300593ea43e2;
static PyObject *const_str_plain_PuOr;
static PyObject *const_str_plain__Paired_data;
extern PyObject *const_int_pos_13;
static PyObject *const_str_plain__hsv_data;
static PyObject *const_str_plain__RdBu_data;
static PyObject *const_tuple_d0062c44ff73c1c48f3f7d27f6672421_tuple;
extern PyObject *const_str_plain_p0;
static PyObject *const_float_31_5;
static PyObject *const_tuple_259735cef93a8d5cd32533f90f8ce8d3_tuple;
extern PyObject *const_str_plain_Spectral;
static PyObject *const_str_plain_Wistia;
static PyObject *const_float_minus_0_14861;
static PyObject *const_str_plain__binary_data;
static PyObject *const_dict_fd4a49377d1d658f7d30b821b6352628;
static PyObject *const_tuple_float_0_0_float_1_0_float_0_0_tuple;
static PyObject *const_tuple_391173ce944bd5a033152d725addac81_tuple;
extern PyObject *const_str_plain_sqrt;
static PyObject *const_str_plain__g10;
static PyObject *const_tuple_dd7b62811603d0af86732a1f25835a50_tuple;
extern PyObject *const_str_plain_binary;
static PyObject *const_str_plain_Greens;
static PyObject *const_str_plain__ocean_data;
extern PyObject *const_str_plain_np;
static PyObject *const_str_plain__prism_blue;
extern PyObject *const_int_pos_35;
static PyObject *const_str_plain__g20;
static PyObject *const_str_plain__RdPu_data;
static PyObject *const_str_plain__g14;
extern PyObject *const_str_plain_summer;
static PyObject *const_str_plain__tab10_data;
extern PyObject *const_str_angle_dictcontraction;
static PyObject *const_tuple_cc9102d236a92569966c4ebd4c8fbcf5_tuple;
extern PyObject *const_float_0_75;
static PyObject *const_tuple_b19a69e04a28b726d8b7d59807ef8eef_tuple;
static PyObject *const_str_plain__g8;
static PyObject *const_str_plain__PuBu_data;
static PyObject *const_tuple_2fe5f075a061fd18cc310f1b06eb16ae_tuple;
extern PyObject *const_str_plain_ret;
static PyObject *const_str_plain_Reds;
static PyObject *const_str_plain__g11;
extern PyObject *const_str_plain_a;
extern PyObject *const_str_plain_sin;
static PyObject *const_tuple_16581877f4601a2ddb7154cb6281285a_tuple;
extern PyObject *const_str_plain_spring;
extern PyObject *const_tuple_str_plain_partial_tuple;
static PyObject *const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple;
static PyObject *const_tuple_eb5d9c64a7345357330502cba765a133_tuple;
extern PyObject *const_str_plain_cos;
static PyObject *const_tuple_float_1_0_float_0_0_float_0_0_tuple;
static PyObject *const_float_0_32;
static PyObject *const_tuple_db58afb74f702702860a0cdf1356a649_tuple;
static PyObject *const_str_plain__g35;
static PyObject *const_str_plain_brg;
extern PyObject *const_int_pos_30;
static PyObject *const_str_plain__flag_blue;
static PyObject *const_tuple_3f501f1fed43a18bc40d5702c0cd69db_tuple;
static PyObject *const_str_plain__Blues_data;
static PyObject *const_str_plain_afmhot;
static PyObject *const_str_plain__PuRd_data;
static PyObject *const_str_plain_BuPu;
static PyObject *const_str_plain__g34;
static PyObject *const_str_plain__Set2_data;
static PyObject *const_str_plain_Pastel2;
extern PyObject *const_str_plain_jet;
static PyObject *const_str_plain_GnBu;
static PyObject *const_str_plain__PuBuGn_data;
extern PyObject *const_str_plain_nipy_spectral;
static PyObject *const_str_plain__YlOrRd_data;
static PyObject *const_str_plain__Greys_data;
static PyObject *const_str_plain_gist_rainbow;
static PyObject *const_tuple_str_plain_x_str_plain_ret_str_plain_m_tuple;
static PyObject *const_str_plain_gist_ncar;
static PyObject *const_str_plain__g29;
static PyObject *const_str_plain__gray_data;
static PyObject *const_str_plain__gnuplot_data;
static PyObject *const_str_plain_tab20c;
extern PyObject *const_str_plain_r;
static PyObject *const_str_digest_68bcfeed91a2fbe6bb71f773db17210e;
extern PyObject *const_tuple_str_plain_x_tuple;
extern PyObject *const_float_0_3;
static PyObject *const_str_plain__g3;
static PyObject *const_str_plain__pink_data;
static PyObject *const_dict_61b96e51e18659e8ba92a9cb886c68eb;
static PyObject *const_str_plain_Blues;
static PyObject *const_str_plain_rainbow;
static PyObject *const_str_plain_cubehelix;
static PyObject *const_tuple_2bea485d544db289d067c788ebf78b62_tuple;
extern PyObject *const_str_plain_pink;
static PyObject *const_dict_d876a613e78a88b4cea47cbcba7077b2;
static PyObject *const_str_plain__hot_data;
static PyObject *const_str_plain__g7;
static PyObject *const_tuple_6e6f4e9a37231f81f1e456047fd8c742_tuple;
static PyObject *const_str_plain_YlOrRd;
static PyObject *const_float_0_84;
static PyObject *const_str_plain__RdYlBu_data;
static PyObject *const_str_plain__cubehelix_data;
static PyObject *const_str_plain__Pastel2_data;
static PyObject *const_str_plain__gist_ncar_data;
extern PyObject *const_str_plain_datad;
static PyObject *const_str_plain_tab20b;
static PyObject *const_str_plain__OrRd_data;
static PyObject *const_str_plain__gist_heat_red;
static PyObject *const_str_plain__g18;
static PyObject *const_tuple_53dc862f8960bed320d0251ce04aeaf0_tuple;
static PyObject *const_str_plain__Accent_data;
static PyObject *const_tuple_a22c772c730dca9240495aa06353c5e0_tuple;
static PyObject *const_str_plain__coolwarm_data;
static PyObject *const_str_plain_Set2;
extern PyObject *const_int_pos_3;
static PyObject *const_float_minus_0_90649;
extern PyObject *const_int_pos_7;
extern PyObject *const_int_pos_28;
static PyObject *const_str_plain_RdYlBu;
static PyObject *const_str_plain__GnBu_data;
extern PyObject *const_int_pos_2;
static PyObject *const_str_plain__Dark2_data;
static PyObject *const_str_plain_BrBG;
static PyObject *const_str_plain_Dark2;
static PyObject *const_str_plain__g1;
static PyObject *const_str_plain__g32;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_RdPu = UNSTREAM_STRING_ASCII( &constant_bin[ 1269485 ], 4, 1 );
    const_dict_3adc72b697f1d7d162e0d950131b4b5a = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1269489 ], 200 );
    const_str_plain__g33 = UNSTREAM_STRING_ASCII( &constant_bin[ 1269689 ], 4, 1 );
    const_str_plain__YlGnBu_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1269693 ], 12, 1 );
    const_float_1_78277 = UNSTREAM_FLOAT( &constant_bin[ 1269705 ] );
    const_str_plain__PiYG_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1269713 ], 10, 1 );
    const_dict_d6323100fc0b68b2862861068aa9dfb4 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1269723 ], 200 );
    const_str_plain__wistia_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1269923 ], 12, 1 );
    const_str_plain_BuGn = UNSTREAM_STRING_ASCII( &constant_bin[ 1269935 ], 4, 1 );
    const_str_plain__gist_yarg_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1269939 ], 15, 1 );
    const_tuple_str_plain_gamma_str_plain_s_str_plain_r_str_plain_h_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gamma_str_plain_s_str_plain_r_str_plain_h_tuple, 0, const_str_plain_gamma ); Py_INCREF( const_str_plain_gamma );
    PyTuple_SET_ITEM( const_tuple_str_plain_gamma_str_plain_s_str_plain_r_str_plain_h_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_str_plain_gamma_str_plain_s_str_plain_r_str_plain_h_tuple, 2, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_str_plain_gamma_str_plain_s_str_plain_r_str_plain_h_tuple, 3, const_str_plain_h ); Py_INCREF( const_str_plain_h );
    const_str_plain__jet_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1269954 ], 9, 1 );
    const_str_plain__flag_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1269963 ], 10, 1 );
    const_tuple_db9a981db24a26ea90b278665de2841d_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1269973 ], 321 );
    const_str_plain__g5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1270294 ], 3, 1 );
    const_str_plain_ocean = UNSTREAM_STRING_ASCII( &constant_bin[ 1270297 ], 5, 1 );
    const_dict_ac09b61043b30f22f2e956405ad79658 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1270302 ], 2906 );
    const_str_plain__RdGy_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1273208 ], 10, 1 );
    const_str_plain__g6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273218 ], 3, 1 );
    const_str_plain_YlOrBr = UNSTREAM_STRING_ASCII( &constant_bin[ 1273221 ], 6, 1 );
    const_dict_8f097861f42f3010bd8f6de6022ffc3c = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1273227 ], 229 );
    const_str_plain__brg_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1273456 ], 9, 1 );
    const_str_plain__g12 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273465 ], 4, 1 );
    const_str_plain__g26 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273469 ], 4, 1 );
    const_str_plain__gist_gray_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1273473 ], 15, 1 );
    const_str_plain_OrRd = UNSTREAM_STRING_ASCII( &constant_bin[ 1273488 ], 4, 1 );
    const_str_plain_tab10 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273492 ], 5, 1 );
    const_str_digest_0870d49f1cf88348cb56a7f4adc73408 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273497 ], 62, 0 );
    const_str_plain_gist_stern = UNSTREAM_STRING_ASCII( &constant_bin[ 1273559 ], 10, 1 );
    const_str_plain__g22 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273569 ], 4, 1 );
    const_str_plain__gist_heat_green = UNSTREAM_STRING_ASCII( &constant_bin[ 1273573 ], 16, 1 );
    const_str_plain__Pastel1_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1273589 ], 13, 1 );
    const_tuple_ac83b7f87b65db8bc61c88a0c684aae6_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1273602 ], 263 );
    const_str_plain__flag_red = UNSTREAM_STRING_ASCII( &constant_bin[ 1273865 ], 9, 1 );
    const_str_plain__BuPu_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1273874 ], 10, 1 );
    const_str_plain_PuBu = UNSTREAM_STRING_ASCII( &constant_bin[ 1273884 ], 4, 1 );
    const_str_plain__g23 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273888 ], 4, 1 );
    const_tuple_a1c2e0d0fdb72592d26c53db2b423104_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1273892 ], 321 );
    const_str_plain__autumn_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1274213 ], 12, 1 );
    const_str_plain__g16 = UNSTREAM_STRING_ASCII( &constant_bin[ 1274225 ], 4, 1 );
    const_str_plain__BuGn_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1274229 ], 10, 1 );
    const_str_plain__g9 = UNSTREAM_STRING_ASCII( &constant_bin[ 1274239 ], 3, 1 );
    const_str_plain__bwr_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1274242 ], 9, 1 );
    const_str_plain__CMRmap_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1274251 ], 12, 1 );
    const_dict_13dcc09e8b14458a914dc22a80eeee13 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1274263 ], 470 );
    const_str_plain__g4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1274733 ], 3, 1 );
    const_dict_1b20bf7e6394a197db2b925f2ae9f1f2 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1274736 ], 5594 );
    const_str_plain_gist_heat = UNSTREAM_STRING_ASCII( &constant_bin[ 1273574 ], 9, 1 );
    const_tuple_dbd02ca2a43f79960ecaa20263657c10_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1280330 ], 263 );
    const_str_plain__gist_earth_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1280593 ], 16, 1 );
    const_str_plain__tab20_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1280609 ], 11, 1 );
    const_str_plain_terrain = UNSTREAM_STRING_ASCII( &constant_bin[ 1280620 ], 7, 1 );
    const_str_plain__Oranges_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1280627 ], 13, 1 );
    const_float_minus_1_1 = UNSTREAM_FLOAT( &constant_bin[ 1280640 ] );
    const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 0, const_str_plain_gamma ); Py_INCREF( const_str_plain_gamma );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 2, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 3, const_str_plain_h ); Py_INCREF( const_str_plain_h );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 4, const_str_plain_p0 ); Py_INCREF( const_str_plain_p0 );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 5, const_str_plain_p1 ); Py_INCREF( const_str_plain_p1 );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 6, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 7, const_str_plain_xg ); Py_INCREF( const_str_plain_xg );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 8, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    const_str_plain_phi = UNSTREAM_STRING_ASCII( &constant_bin[ 90567 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 9, const_str_plain_phi ); Py_INCREF( const_str_plain_phi );
    const_dict_7ba6c40e971dab085fcf28da729f7167 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1280648 ], 1215 );
    const_tuple_float_1_0_float_0_5_float_minus_1_5_float_1_0_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_5_float_minus_1_5_float_1_0_tuple, 0, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_5_float_minus_1_5_float_1_0_tuple, 1, const_float_0_5 ); Py_INCREF( const_float_0_5 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_5_float_minus_1_5_float_1_0_tuple, 2, const_float_minus_1_5 ); Py_INCREF( const_float_minus_1_5 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_5_float_minus_1_5_float_1_0_tuple, 3, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    const_str_plain__ch_helper = UNSTREAM_STRING_ASCII( &constant_bin[ 1281863 ], 10, 1 );
    const_float_0_33 = UNSTREAM_FLOAT( &constant_bin[ 1281873 ] );
    const_tuple_9e6a97cefbfc93e4dbd6fe4790754edf_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1281881 ], 263 );
    const_str_plain__afmhot_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1282144 ], 12, 1 );
    const_float_20_9 = UNSTREAM_FLOAT( &constant_bin[ 1282156 ] );
    const_str_plain_gnuplot = UNSTREAM_STRING_ASCII( &constant_bin[ 1282164 ], 7, 1 );
    const_str_plain__YlOrBr_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1282171 ], 12, 1 );
    const_str_plain__tab20c_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1282183 ], 12, 1 );
    const_str_plain_tab20 = UNSTREAM_STRING_ASCII( &constant_bin[ 1280610 ], 5, 1 );
    const_str_plain_Pastel1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273590 ], 7, 1 );
    const_str_plain_coolwarm = UNSTREAM_STRING_ASCII( &constant_bin[ 1282195 ], 8, 1 );
    const_str_plain_Set3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1282203 ], 4, 1 );
    const_str_plain__cool_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1282207 ], 10, 1 );
    const_str_plain__prism_green = UNSTREAM_STRING_ASCII( &constant_bin[ 1282217 ], 12, 1 );
    const_str_plain__prism_red = UNSTREAM_STRING_ASCII( &constant_bin[ 1282229 ], 10, 1 );
    const_float_1_84 = UNSTREAM_FLOAT( &constant_bin[ 1282239 ] );
    const_tuple_9b04358fc83d8c29f6d13e55a485e086_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1282247 ], 321 );
    const_tuple_e22ad1a6fd2b90375e84cff21f64303a_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1282568 ], 321 );
    const_str_plain__g19 = UNSTREAM_STRING_ASCII( &constant_bin[ 1282889 ], 4, 1 );
    const_tuple_ef7272d3c529510edf3b0815822dbaa3_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1282893 ], 582 );
    const_dict_46fdda9c2313b830d68498039918be48 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1283475 ], 809 );
    const_tuple_122f654ebd563953484696204c943f8b_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1284284 ], 263 );
    const_str_plain_gist_yarg = UNSTREAM_STRING_ASCII( &constant_bin[ 1269940 ], 9, 1 );
    const_str_plain__gnuplot2_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1284547 ], 14, 1 );
    const_dict_26510cda7d1dc75818c57eac9563d1d1 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1284561 ], 292 );
    const_str_plain__g13 = UNSTREAM_STRING_ASCII( &constant_bin[ 1284853 ], 4, 1 );
    const_tuple_6eb90b29c730cddab6ab1d936156a49c_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1284857 ], 263 );
    const_str_plain__g31 = UNSTREAM_STRING_ASCII( &constant_bin[ 1285120 ], 4, 1 );
    const_tuple_de81471e775d0db13ccc93277369c0bb_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1285124 ], 263 );
    const_str_plain__flag_green = UNSTREAM_STRING_ASCII( &constant_bin[ 1285387 ], 11, 1 );
    const_tuple_2a4c4f5b12a7944706ab0b855d978f15_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1285398 ], 321 );
    const_tuple_4d8a3db908fea5ac787eae57a231d107_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1285719 ], 263 );
    const_tuple_fb0dd03a0e957d10a507ea76b9c3ad27_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1285982 ], 321 );
    const_str_plain__g30 = UNSTREAM_STRING_ASCII( &constant_bin[ 1286303 ], 4, 1 );
    const_str_plain_gist_gray = UNSTREAM_STRING_ASCII( &constant_bin[ 1273474 ], 9, 1 );
    const_tuple_1cf08670e58213d33381f121283ddef7_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1286307 ], 263 );
    const_str_plain__seismic_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1286570 ], 13, 1 );
    const_str_plain__gist_yarg = UNSTREAM_STRING_ASCII( &constant_bin[ 1269939 ], 10, 1 );
    const_str_plain__PRGn_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1286583 ], 10, 1 );
    const_str_plain_RdGy = UNSTREAM_STRING_ASCII( &constant_bin[ 1273209 ], 4, 1 );
    const_str_plain__bone_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1286593 ], 10, 1 );
    const_str_plain_Paired = UNSTREAM_STRING_ASCII( &constant_bin[ 1286603 ], 6, 1 );
    const_str_plain__g0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1286609 ], 3, 1 );
    const_str_plain__g15 = UNSTREAM_STRING_ASCII( &constant_bin[ 1286612 ], 4, 1 );
    const_str_plain__winter_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1286616 ], 12, 1 );
    const_str_plain_bwr = UNSTREAM_STRING_ASCII( &constant_bin[ 1274243 ], 3, 1 );
    const_str_plain__Set1_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1286628 ], 10, 1 );
    const_float_minus_0_29227 = UNSTREAM_FLOAT( &constant_bin[ 1286638 ] );
    const_str_plain__g2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273469 ], 3, 1 );
    const_dict_61e4a4a8cd2b4181694d4cfb1751640d = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1286646 ], 200 );
    const_str_plain_Set1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1286629 ], 4, 1 );
    const_str_plain__gist_heat_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1286846 ], 15, 1 );
    const_str_plain_YlGn = UNSTREAM_STRING_ASCII( &constant_bin[ 1269694 ], 4, 1 );
    const_str_plain__BrBG_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1286861 ], 10, 1 );
    const_str_plain_gfunc = UNSTREAM_STRING_ASCII( &constant_bin[ 1286871 ], 5, 1 );
    const_tuple_13493111d1a4f8c8a058ebd7f95c09b1_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1286876 ], 263 );
    const_tuple_9fa91c64747bb6ff4492053856cc06e7_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1287139 ], 234 );
    const_str_digest_46ac0c36bf895f540a2118bdf4f973be = UNSTREAM_STRING_ASCII( &constant_bin[ 1287373 ], 17, 0 );
    const_str_plain__Set3_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1287390 ], 10, 1 );
    const_str_plain__terrain_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1287400 ], 13, 1 );
    const_dict_a6becc14d04a47becd62ae70b4c4482c = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1287413 ], 200 );
    const_str_plain__Purples_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1287613 ], 13, 1 );
    const_dict_79ecbf763b054a3ad895cae3fb89c58e = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1287626 ], 152 );
    const_tuple_d6bde7eec8df9cf0ee10c9abbaa88fc9_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1287778 ], 234 );
    const_str_plain_PRGn = UNSTREAM_STRING_ASCII( &constant_bin[ 1286584 ], 4, 1 );
    const_tuple_float_0_5_float_0_0_float_0_0_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_float_0_5_float_0_0_float_0_0_tuple, 0, const_float_0_5 ); Py_INCREF( const_float_0_5 );
    PyTuple_SET_ITEM( const_tuple_float_0_5_float_0_0_float_0_0_tuple, 1, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_float_0_5_float_0_0_float_0_0_tuple, 2, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    const_str_plain__gist_rainbow_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1288012 ], 18, 1 );
    const_dict_cc81f270cbb7633b5af45d15d4c591ec = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1288030 ], 1389 );
    const_str_plain_RdYlGn = UNSTREAM_STRING_ASCII( &constant_bin[ 1289419 ], 6, 1 );
    const_float_1_97294 = UNSTREAM_FLOAT( &constant_bin[ 1289425 ] );
    const_str_digest_36fea1563816aa379d3cd2c1ac36c445 = UNSTREAM_STRING_ASCII( &constant_bin[ 1289433 ], 23, 0 );
    const_str_plain_gnuplot2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1284548 ], 8, 1 );
    const_tuple_float_0_0_float_0_0_float_0_3_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_0_float_0_3_tuple, 0, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_0_float_0_3_tuple, 1, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_0_float_0_3_tuple, 2, const_float_0_3 ); Py_INCREF( const_float_0_3 );
    const_str_digest_0ec62d994d1e365c7efad4dabaaba134 = UNSTREAM_STRING_ASCII( &constant_bin[ 1289456 ], 255, 0 );
    const_str_plain_Oranges = UNSTREAM_STRING_ASCII( &constant_bin[ 1280628 ], 7, 1 );
    const_float_0_78125 = UNSTREAM_FLOAT( &constant_bin[ 1271041 ] );
    const_str_plain__prism_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1289711 ], 11, 1 );
    const_dict_e5f8cd4b8b884f01b9dbdb6c0cf95972 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1289722 ], 1862 );
    const_str_plain__Spectral_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1291584 ], 14, 1 );
    const_str_plain__g36 = UNSTREAM_STRING_ASCII( &constant_bin[ 1291598 ], 4, 1 );
    const_xrange_0_37 = BUILTIN_XRANGE3( const_int_0, const_int_pos_37, const_int_pos_1 );
    const_tuple_b84fc382b1c73eb24052e00184983189_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1291602 ], 350 );
    const_str_plain__YlGn_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1291952 ], 10, 1 );
    const_str_plain__g21 = UNSTREAM_STRING_ASCII( &constant_bin[ 1291962 ], 4, 1 );
    const_str_plain__copper_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1291966 ], 12, 1 );
    const_tuple_4f66db73ea9fdbfdceb9e620df1e4919_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1291978 ], 263 );
    const_str_plain_Purples = UNSTREAM_STRING_ASCII( &constant_bin[ 1287614 ], 7, 1 );
    const_tuple_20bd1f5c0a0f9a93d11e666cd7289af4_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1292241 ], 321 );
    const_str_plain__summer_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1292562 ], 12, 1 );
    const_str_plain__RdYlGn_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1292574 ], 12, 1 );
    const_tuple_8a21be97bcec74887e2b70da615f70d9_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1292586 ], 263 );
    const_tuple_440d7807917857d4ae4d4cd0c76093e7_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1292849 ], 321 );
    const_str_plain_Greys = UNSTREAM_STRING_ASCII( &constant_bin[ 1293170 ], 5, 1 );
    const_tuple_194951aabc8c4d0f914fa1c18e921dac_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1293175 ], 263 );
    const_str_plain__g28 = UNSTREAM_STRING_ASCII( &constant_bin[ 1293438 ], 4, 1 );
    const_str_plain_YlGnBu = UNSTREAM_STRING_ASCII( &constant_bin[ 1269694 ], 6, 1 );
    const_tuple_58dec871a15dc46004a9c29f566fd8e9_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1293442 ], 292 );
    const_str_plain_PiYG = UNSTREAM_STRING_ASCII( &constant_bin[ 1269714 ], 4, 1 );
    const_str_plain_seismic = UNSTREAM_STRING_ASCII( &constant_bin[ 1286571 ], 7, 1 );
    const_str_plain__Greens_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1293734 ], 12, 1 );
    const_dict_e67366b36b736b2b1824e4124103e620 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1293746 ], 316 );
    const_str_plain_gist_earth = UNSTREAM_STRING_ASCII( &constant_bin[ 1280594 ], 10, 1 );
    const_str_plain__Reds_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1294062 ], 10, 1 );
    const_str_plain__gist_stern_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1294072 ], 16, 1 );
    const_str_plain__nipy_spectral_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1294088 ], 19, 1 );
    const_str_plain__tab20b_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1294107 ], 12, 1 );
    const_str_plain_CMRmap = UNSTREAM_STRING_ASCII( &constant_bin[ 1274252 ], 6, 1 );
    const_str_plain_PuBuGn = UNSTREAM_STRING_ASCII( &constant_bin[ 1294119 ], 6, 1 );
    const_str_plain__PuOr_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1294125 ], 10, 1 );
    const_tuple_7eb55817b097308a4aa82a88a7a374a0_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1294135 ], 263 );
    const_dict_139fa967a21793ecdbc3e590120916a2 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1294398 ], 200 );
    const_float_0_92 = UNSTREAM_FLOAT( &constant_bin[ 1294598 ] );
    const_str_plain__g25 = UNSTREAM_STRING_ASCII( &constant_bin[ 1294606 ], 4, 1 );
    const_tuple_d09dc699a96e56cc6c61b2a9d13ae443_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1294610 ], 322 );
    const_str_plain__rainbow_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1288017 ], 13, 1 );
    const_str_plain__g27 = UNSTREAM_STRING_ASCII( &constant_bin[ 1294932 ], 4, 1 );
    const_str_plain__gist_heat_blue = UNSTREAM_STRING_ASCII( &constant_bin[ 1294936 ], 15, 1 );
    const_str_plain__g24 = UNSTREAM_STRING_ASCII( &constant_bin[ 1294951 ], 4, 1 );
    const_float_11_5 = UNSTREAM_FLOAT( &constant_bin[ 1294955 ] );
    const_tuple_6430e1188db9556163de382028b9d704_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1294963 ], 234 );
    const_tuple_float_0_0_float_0_0_float_1_0_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_0_float_1_0_tuple, 0, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_0_float_1_0_tuple, 1, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_0_float_1_0_tuple, 2, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    const_str_plain_PuRd = UNSTREAM_STRING_ASCII( &constant_bin[ 1295197 ], 4, 1 );
    const_str_plain_RdBu = UNSTREAM_STRING_ASCII( &constant_bin[ 1295201 ], 4, 1 );
    const_str_plain__spring_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1295205 ], 12, 1 );
    const_dict_a065b631920800bd13e7ea9419838858 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1295217 ], 722 );
    const_str_plain__g17 = UNSTREAM_STRING_ASCII( &constant_bin[ 1295939 ], 4, 1 );
    const_str_digest_f73618ed65447e45c026300593ea43e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1295943 ], 4, 0 );
    const_str_plain_PuOr = UNSTREAM_STRING_ASCII( &constant_bin[ 1294126 ], 4, 1 );
    const_str_plain__Paired_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1295947 ], 12, 1 );
    const_str_plain__hsv_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1295959 ], 9, 1 );
    const_str_plain__RdBu_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1295968 ], 10, 1 );
    const_tuple_d0062c44ff73c1c48f3f7d27f6672421_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1295978 ], 321 );
    const_float_31_5 = UNSTREAM_FLOAT( &constant_bin[ 1296299 ] );
    const_tuple_259735cef93a8d5cd32533f90f8ce8d3_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1296307 ], 263 );
    const_str_plain_Wistia = UNSTREAM_STRING_ASCII( &constant_bin[ 1296570 ], 6, 1 );
    const_float_minus_0_14861 = UNSTREAM_FLOAT( &constant_bin[ 1296576 ] );
    const_str_plain__binary_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1296584 ], 12, 1 );
    const_dict_fd4a49377d1d658f7d30b821b6352628 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1296596 ], 366 );
    const_tuple_float_0_0_float_1_0_float_0_0_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_1_0_float_0_0_tuple, 0, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_1_0_float_0_0_tuple, 1, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_1_0_float_0_0_tuple, 2, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    const_tuple_391173ce944bd5a033152d725addac81_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1296962 ], 582 );
    const_str_plain__g10 = UNSTREAM_STRING_ASCII( &constant_bin[ 1297544 ], 4, 1 );
    const_tuple_dd7b62811603d0af86732a1f25835a50_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1297548 ], 234 );
    const_str_plain_Greens = UNSTREAM_STRING_ASCII( &constant_bin[ 1293735 ], 6, 1 );
    const_str_plain__ocean_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1297782 ], 11, 1 );
    const_str_plain__prism_blue = UNSTREAM_STRING_ASCII( &constant_bin[ 1297793 ], 11, 1 );
    const_str_plain__g20 = UNSTREAM_STRING_ASCII( &constant_bin[ 1297804 ], 4, 1 );
    const_str_plain__RdPu_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1297808 ], 10, 1 );
    const_str_plain__g14 = UNSTREAM_STRING_ASCII( &constant_bin[ 1297818 ], 4, 1 );
    const_str_plain__tab10_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1297822 ], 11, 1 );
    const_tuple_cc9102d236a92569966c4ebd4c8fbcf5_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1297833 ], 263 );
    const_tuple_b19a69e04a28b726d8b7d59807ef8eef_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1298096 ], 263 );
    const_str_plain__g8 = UNSTREAM_STRING_ASCII( &constant_bin[ 1298359 ], 3, 1 );
    const_str_plain__PuBu_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1298362 ], 10, 1 );
    const_tuple_2fe5f075a061fd18cc310f1b06eb16ae_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1298372 ], 350 );
    const_str_plain_Reds = UNSTREAM_STRING_ASCII( &constant_bin[ 1294063 ], 4, 1 );
    const_str_plain__g11 = UNSTREAM_STRING_ASCII( &constant_bin[ 1298722 ], 4, 1 );
    const_tuple_16581877f4601a2ddb7154cb6281285a_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1298726 ], 263 );
    const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple, 0, const_tuple_float_0_0_float_0_0_float_0_3_tuple ); Py_INCREF( const_tuple_float_0_0_float_0_0_float_0_3_tuple );
    PyTuple_SET_ITEM( const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple, 1, const_tuple_float_0_0_float_0_0_float_1_0_tuple ); Py_INCREF( const_tuple_float_0_0_float_0_0_float_1_0_tuple );
    PyTuple_SET_ITEM( const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple, 2, const_tuple_float_1_0_float_1_0_float_1_0_tuple ); Py_INCREF( const_tuple_float_1_0_float_1_0_float_1_0_tuple );
    const_tuple_float_1_0_float_0_0_float_0_0_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_0_float_0_0_tuple, 0, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_0_float_0_0_tuple, 1, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_0_float_0_0_tuple, 2, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    PyTuple_SET_ITEM( const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple, 3, const_tuple_float_1_0_float_0_0_float_0_0_tuple ); Py_INCREF( const_tuple_float_1_0_float_0_0_float_0_0_tuple );
    PyTuple_SET_ITEM( const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple, 4, const_tuple_float_0_5_float_0_0_float_0_0_tuple ); Py_INCREF( const_tuple_float_0_5_float_0_0_float_0_0_tuple );
    const_tuple_eb5d9c64a7345357330502cba765a133_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1298989 ], 263 );
    const_float_0_32 = UNSTREAM_FLOAT( &constant_bin[ 1299252 ] );
    const_tuple_db58afb74f702702860a0cdf1356a649_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1299260 ], 263 );
    const_str_plain__g35 = UNSTREAM_STRING_ASCII( &constant_bin[ 1299523 ], 4, 1 );
    const_str_plain_brg = UNSTREAM_STRING_ASCII( &constant_bin[ 1273457 ], 3, 1 );
    const_str_plain__flag_blue = UNSTREAM_STRING_ASCII( &constant_bin[ 1299527 ], 10, 1 );
    const_tuple_3f501f1fed43a18bc40d5702c0cd69db_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1299537 ], 263 );
    const_str_plain__Blues_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1299800 ], 11, 1 );
    const_str_plain_afmhot = UNSTREAM_STRING_ASCII( &constant_bin[ 1282145 ], 6, 1 );
    const_str_plain__PuRd_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1299811 ], 10, 1 );
    const_str_plain_BuPu = UNSTREAM_STRING_ASCII( &constant_bin[ 1273875 ], 4, 1 );
    const_str_plain__g34 = UNSTREAM_STRING_ASCII( &constant_bin[ 1299821 ], 4, 1 );
    const_str_plain__Set2_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1299825 ], 10, 1 );
    const_str_plain_Pastel2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1299835 ], 7, 1 );
    const_str_plain_GnBu = UNSTREAM_STRING_ASCII( &constant_bin[ 1269696 ], 4, 1 );
    const_str_plain__PuBuGn_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1299842 ], 12, 1 );
    const_str_plain__YlOrRd_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1299854 ], 12, 1 );
    const_str_plain__Greys_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1299866 ], 11, 1 );
    const_str_plain_gist_rainbow = UNSTREAM_STRING_ASCII( &constant_bin[ 1288013 ], 12, 1 );
    const_tuple_str_plain_x_str_plain_ret_str_plain_m_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_ret_str_plain_m_tuple, 0, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_ret_str_plain_m_tuple, 1, const_str_plain_ret ); Py_INCREF( const_str_plain_ret );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_ret_str_plain_m_tuple, 2, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    const_str_plain_gist_ncar = UNSTREAM_STRING_ASCII( &constant_bin[ 1299877 ], 9, 1 );
    const_str_plain__g29 = UNSTREAM_STRING_ASCII( &constant_bin[ 1299886 ], 4, 1 );
    const_str_plain__gray_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1273478 ], 10, 1 );
    const_str_plain__gnuplot_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1299890 ], 13, 1 );
    const_str_plain_tab20c = UNSTREAM_STRING_ASCII( &constant_bin[ 1282184 ], 6, 1 );
    const_str_digest_68bcfeed91a2fbe6bb71f773db17210e = UNSTREAM_STRING_ASCII( &constant_bin[ 1299903 ], 2036, 0 );
    const_str_plain__g3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1269689 ], 3, 1 );
    const_str_plain__pink_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1301939 ], 10, 1 );
    const_dict_61b96e51e18659e8ba92a9cb886c68eb = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1301949 ], 200 );
    const_str_plain_Blues = UNSTREAM_STRING_ASCII( &constant_bin[ 1299801 ], 5, 1 );
    const_str_plain_rainbow = UNSTREAM_STRING_ASCII( &constant_bin[ 1288018 ], 7, 1 );
    const_str_plain_cubehelix = UNSTREAM_STRING_ASCII( &constant_bin[ 1273538 ], 9, 1 );
    const_tuple_2bea485d544db289d067c788ebf78b62_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_2bea485d544db289d067c788ebf78b62_tuple, 0, const_tuple_float_0_0_float_0_0_float_1_0_tuple ); Py_INCREF( const_tuple_float_0_0_float_0_0_float_1_0_tuple );
    PyTuple_SET_ITEM( const_tuple_2bea485d544db289d067c788ebf78b62_tuple, 1, const_tuple_float_1_0_float_1_0_float_1_0_tuple ); Py_INCREF( const_tuple_float_1_0_float_1_0_float_1_0_tuple );
    PyTuple_SET_ITEM( const_tuple_2bea485d544db289d067c788ebf78b62_tuple, 2, const_tuple_float_1_0_float_0_0_float_0_0_tuple ); Py_INCREF( const_tuple_float_1_0_float_0_0_float_0_0_tuple );
    const_dict_d876a613e78a88b4cea47cbcba7077b2 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1302149 ], 316 );
    const_str_plain__hot_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1302465 ], 9, 1 );
    const_str_plain__g7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1302474 ], 3, 1 );
    const_tuple_6e6f4e9a37231f81f1e456047fd8c742_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1302477 ], 242 );
    const_str_plain_YlOrRd = UNSTREAM_STRING_ASCII( &constant_bin[ 1299855 ], 6, 1 );
    const_float_0_84 = UNSTREAM_FLOAT( &constant_bin[ 1302719 ] );
    const_str_plain__RdYlBu_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1302727 ], 12, 1 );
    const_str_plain__cubehelix_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1302739 ], 15, 1 );
    const_str_plain__Pastel2_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1302754 ], 13, 1 );
    const_str_plain__gist_ncar_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1302767 ], 15, 1 );
    const_str_plain_tab20b = UNSTREAM_STRING_ASCII( &constant_bin[ 1294108 ], 6, 1 );
    const_str_plain__OrRd_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1302782 ], 10, 1 );
    const_str_plain__gist_heat_red = UNSTREAM_STRING_ASCII( &constant_bin[ 1302792 ], 14, 1 );
    const_str_plain__g18 = UNSTREAM_STRING_ASCII( &constant_bin[ 1302806 ], 4, 1 );
    const_tuple_53dc862f8960bed320d0251ce04aeaf0_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1302810 ], 582 );
    const_str_plain__Accent_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1303392 ], 12, 1 );
    const_tuple_a22c772c730dca9240495aa06353c5e0_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_a22c772c730dca9240495aa06353c5e0_tuple, 0, const_tuple_float_0_0_float_0_0_float_1_0_tuple ); Py_INCREF( const_tuple_float_0_0_float_0_0_float_1_0_tuple );
    PyTuple_SET_ITEM( const_tuple_a22c772c730dca9240495aa06353c5e0_tuple, 1, const_tuple_float_1_0_float_0_0_float_0_0_tuple ); Py_INCREF( const_tuple_float_1_0_float_0_0_float_0_0_tuple );
    PyTuple_SET_ITEM( const_tuple_a22c772c730dca9240495aa06353c5e0_tuple, 2, const_tuple_float_0_0_float_1_0_float_0_0_tuple ); Py_INCREF( const_tuple_float_0_0_float_1_0_float_0_0_tuple );
    const_str_plain__coolwarm_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1303404 ], 14, 1 );
    const_str_plain_Set2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1299826 ], 4, 1 );
    const_float_minus_0_90649 = UNSTREAM_FLOAT( &constant_bin[ 1303418 ] );
    const_str_plain_RdYlBu = UNSTREAM_STRING_ASCII( &constant_bin[ 1302728 ], 6, 1 );
    const_str_plain__GnBu_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1303426 ], 10, 1 );
    const_str_plain__Dark2_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1303436 ], 11, 1 );
    const_str_plain_BrBG = UNSTREAM_STRING_ASCII( &constant_bin[ 1286862 ], 4, 1 );
    const_str_plain_Dark2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1303437 ], 5, 1 );
    const_str_plain__g1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1273465 ], 3, 1 );
    const_str_plain__g32 = UNSTREAM_STRING_ASCII( &constant_bin[ 1303447 ], 4, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$_cm( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_eced98829c6d9c04ad2c5fcef3e36c2c;
static PyCodeObject *codeobj_dc635218d6bfe5f1da89e2d1d276c5ff;
static PyCodeObject *codeobj_80e296267b0748a2b363ed54c4de79e8;
static PyCodeObject *codeobj_96dc18310950969d5f48576ce081a466;
static PyCodeObject *codeobj_bc3e536fbfa165ee58d9a0d82198ce1d;
static PyCodeObject *codeobj_9da89fbbcfc0335d1bbafb14a8117620;
static PyCodeObject *codeobj_9cbe26b9a96f59538bb1963f17e7d388;
static PyCodeObject *codeobj_9b3cd153a3f8975c8cbb5bdac89db94e;
static PyCodeObject *codeobj_6f001a3c707cfa288f8adae11f30a1d9;
static PyCodeObject *codeobj_a43ef645600be41fb6834b58933b4683;
static PyCodeObject *codeobj_3d07f3dd6ca66b2aecfeae9495e79601;
static PyCodeObject *codeobj_8b0e4f93545329d1d94e42993abf7216;
static PyCodeObject *codeobj_c09d6f101ce2957484c6ba3b60f2f5cf;
static PyCodeObject *codeobj_8cff908993bcdf98dabe0c3736dcf388;
static PyCodeObject *codeobj_3318b94fe7114ea97fc057bb2890d2d6;
static PyCodeObject *codeobj_2ed3244b65d5d34826d10d89ba234077;
static PyCodeObject *codeobj_acf507c7209ba5bcc78d816026bbdd7b;
static PyCodeObject *codeobj_642906f415e89b8021acbbc2acfc4bb5;
static PyCodeObject *codeobj_da1e2d0577c3f33d39ce13d722082e7c;
static PyCodeObject *codeobj_b185d50514175a783314284b7e1c8664;
static PyCodeObject *codeobj_b04b846f655acf9210cbb390b3a144ec;
static PyCodeObject *codeobj_bb72c4ebde9e34e2e57d29944502cdb1;
static PyCodeObject *codeobj_74adb45065271f93643eb2787503f6ff;
static PyCodeObject *codeobj_7a5c1be07c6c8bf106a51f3f3f4989f5;
static PyCodeObject *codeobj_ec2392149c4f5985c8e0e8e598cd76cf;
static PyCodeObject *codeobj_d65caf7c0a8973481ee4e27a9b0f6129;
static PyCodeObject *codeobj_510a5e3fafa7208dd155230ea37e1486;
static PyCodeObject *codeobj_ed6b474b87a1b9e43d1ffcd2f2821ef9;
static PyCodeObject *codeobj_b86579b6ac33f39ef296b2058171f200;
static PyCodeObject *codeobj_9a00559ab1fce59fdfb7a48ac0fbd836;
static PyCodeObject *codeobj_566444d1d4c291dccae414e188dab726;
static PyCodeObject *codeobj_45a7fe08d52f929db27e5920e6e2f85b;
static PyCodeObject *codeobj_93383a9ac06d4ec237b25dd6b51aa254;
static PyCodeObject *codeobj_6d5891a73a626ffe1449c0a32bde90be;
static PyCodeObject *codeobj_ca4ef550c0f985f1de07b531fab6582c;
static PyCodeObject *codeobj_147ee6952ad69b667578f6330066de5c;
static PyCodeObject *codeobj_0f545769d1a2556f56803e324fae7085;
static PyCodeObject *codeobj_5cf23711c93867864876412ea15953a6;
static PyCodeObject *codeobj_e6223b72b1e3a56299cfc38b154bc21e;
static PyCodeObject *codeobj_857da5f6463870875e79355e4ffcc4e0;
static PyCodeObject *codeobj_f91be58d49ac067f82c34fb0c522641c;
static PyCodeObject *codeobj_572b6279086edfe79cc5d4b59b9d3a78;
static PyCodeObject *codeobj_0665c21023d00b9af46ab168ecffda2d;
static PyCodeObject *codeobj_298269a309931b4e28d196e53b52b9d7;
static PyCodeObject *codeobj_ff54ad51aa14d088b02f2d24e41ac145;
static PyCodeObject *codeobj_ca6a378878e823427eba3f7dfa3f4a5f;
static PyCodeObject *codeobj_7ab5ebee71076c5d9b0e3985e77a7554;
static PyCodeObject *codeobj_71c9beb9d5e02f5ecc46acc3901775d6;
static PyCodeObject *codeobj_a5d59bf3a308aedf73adad4d6f80043d;
static PyCodeObject *codeobj_f1d46b9cdedca619e4f3c486cb441557;
static PyCodeObject *codeobj_32d77dfbf1973b6159d4f408ea9d29f7;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_46ac0c36bf895f540a2118bdf4f973be );
    codeobj_eced98829c6d9c04ad2c5fcef3e36c2c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_dictcontraction, 160, const_tuple_str_plain_i_tuple, 1, 0, CO_NEWLOCALS | CO_NOFREE );
    codeobj_dc635218d6bfe5f1da89e2d1d276c5ff = MAKE_CODEOBJ( module_filename_obj, const_str_digest_36fea1563816aa379d3cd2c1ac36c445, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_80e296267b0748a2b363ed54c4de79e8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__ch_helper, 56, const_tuple_a8e4d90ae8ea456e67dffd07b85de560_tuple, 7, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_96dc18310950969d5f48576ce081a466 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__flag_blue, 48, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bc3e536fbfa165ee58d9a0d82198ce1d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__flag_green, 47, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9da89fbbcfc0335d1bbafb14a8117620 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__flag_red, 46, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9cbe26b9a96f59538bb1963f17e7d388 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g0, 114, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9b3cd153a3f8975c8cbb5bdac89db94e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g1, 115, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6f001a3c707cfa288f8adae11f30a1d9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g10, 124, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a43ef645600be41fb6834b58933b4683 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g11, 125, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3d07f3dd6ca66b2aecfeae9495e79601 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g12, 126, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8b0e4f93545329d1d94e42993abf7216 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g13, 127, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c09d6f101ce2957484c6ba3b60f2f5cf = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g14, 128, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8cff908993bcdf98dabe0c3736dcf388 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g15, 129, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3318b94fe7114ea97fc057bb2890d2d6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g16, 130, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ed3244b65d5d34826d10d89ba234077 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g17, 131, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_acf507c7209ba5bcc78d816026bbdd7b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g18, 132, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_642906f415e89b8021acbbc2acfc4bb5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g19, 133, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_da1e2d0577c3f33d39ce13d722082e7c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g2, 116, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b185d50514175a783314284b7e1c8664 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g20, 134, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b04b846f655acf9210cbb390b3a144ec = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g21, 135, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bb72c4ebde9e34e2e57d29944502cdb1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g22, 136, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_74adb45065271f93643eb2787503f6ff = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g23, 137, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7a5c1be07c6c8bf106a51f3f3f4989f5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g24, 138, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ec2392149c4f5985c8e0e8e598cd76cf = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g25, 139, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d65caf7c0a8973481ee4e27a9b0f6129 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g26, 140, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_510a5e3fafa7208dd155230ea37e1486 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g27, 141, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ed6b474b87a1b9e43d1ffcd2f2821ef9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g28, 142, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b86579b6ac33f39ef296b2058171f200 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g29, 143, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9a00559ab1fce59fdfb7a48ac0fbd836 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g3, 117, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_566444d1d4c291dccae414e188dab726 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g30, 144, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_45a7fe08d52f929db27e5920e6e2f85b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g31, 145, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_93383a9ac06d4ec237b25dd6b51aa254 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g32, 146, const_tuple_str_plain_x_str_plain_ret_str_plain_m_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6d5891a73a626ffe1449c0a32bde90be = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g33, 155, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ca4ef550c0f985f1de07b531fab6582c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g34, 156, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_147ee6952ad69b667578f6330066de5c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g35, 157, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0f545769d1a2556f56803e324fae7085 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g36, 158, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5cf23711c93867864876412ea15953a6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g4, 118, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e6223b72b1e3a56299cfc38b154bc21e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g5, 119, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_857da5f6463870875e79355e4ffcc4e0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g6, 120, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f91be58d49ac067f82c34fb0c522641c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g7, 121, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_572b6279086edfe79cc5d4b59b9d3a78 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g8, 122, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0665c21023d00b9af46ab168ecffda2d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__g9, 123, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_298269a309931b4e28d196e53b52b9d7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__gist_heat_blue, 1009, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ff54ad51aa14d088b02f2d24e41ac145 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__gist_heat_green, 1008, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ca6a378878e823427eba3f7dfa3f4a5f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__gist_heat_red, 1007, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7ab5ebee71076c5d9b0e3985e77a7554 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__gist_yarg, 1088, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_71c9beb9d5e02f5ecc46acc3901775d6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__prism_blue, 53, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a5d59bf3a308aedf73adad4d6f80043d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__prism_green, 52, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f1d46b9cdedca619e4f3c486cb441557 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__prism_red, 51, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_32d77dfbf1973b6159d4f408ea9d29f7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cubehelix, 66, const_tuple_str_plain_gamma_str_plain_s_str_plain_r_str_plain_h_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_10__g1(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_11__g2(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_12__g3(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_13__g4(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_14__g5(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_15__g6(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_16__g7(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_17__g8(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_18__g9(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_19__g10(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_1__flag_red(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_20__g11(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_21__g12(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_22__g13(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_23__g14(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_24__g15(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_25__g16(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_26__g17(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_27__g18(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_28__g19(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_29__g20(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_2__flag_green(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_30__g21(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_31__g22(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_32__g23(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_33__g24(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_34__g25(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_35__g26(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_36__g27(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_37__g28(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_38__g29(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_39__g30(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_3__flag_blue(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_40__g31(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_41__g32(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_42__g33(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_43__g34(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_44__g35(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_45__g36(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_46__gist_heat_red(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_47__gist_heat_green(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_48__gist_heat_blue(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_49__gist_yarg(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_4__prism_red(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_5__prism_green(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_6__prism_blue(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_7__ch_helper(  );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_8_cubehelix( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_9__g0(  );


// The module function definitions.
static PyObject *impl_matplotlib$_cm$$$function_1__flag_red( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9da89fbbcfc0335d1bbafb14a8117620;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9da89fbbcfc0335d1bbafb14a8117620 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9da89fbbcfc0335d1bbafb14a8117620, codeobj_9da89fbbcfc0335d1bbafb14a8117620, module_matplotlib$_cm, sizeof(void *) );
    frame_9da89fbbcfc0335d1bbafb14a8117620 = cache_frame_9da89fbbcfc0335d1bbafb14a8117620;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9da89fbbcfc0335d1bbafb14a8117620 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9da89fbbcfc0335d1bbafb14a8117620 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_right_name_4;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_5;
        tmp_left_name_2 = const_float_0_75;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_5 = par_x;
        tmp_right_name_2 = const_float_31_5;
        tmp_left_name_4 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_5, tmp_right_name_2 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_float_0_25;
        tmp_left_name_3 = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_4, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );

            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_3 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_9da89fbbcfc0335d1bbafb14a8117620->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_MUL_FLOAT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_float_0_5;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9da89fbbcfc0335d1bbafb14a8117620 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9da89fbbcfc0335d1bbafb14a8117620 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9da89fbbcfc0335d1bbafb14a8117620 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9da89fbbcfc0335d1bbafb14a8117620, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9da89fbbcfc0335d1bbafb14a8117620->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9da89fbbcfc0335d1bbafb14a8117620, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9da89fbbcfc0335d1bbafb14a8117620,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_9da89fbbcfc0335d1bbafb14a8117620 == cache_frame_9da89fbbcfc0335d1bbafb14a8117620 )
    {
        Py_DECREF( frame_9da89fbbcfc0335d1bbafb14a8117620 );
    }
    cache_frame_9da89fbbcfc0335d1bbafb14a8117620 = NULL;

    assertFrameObject( frame_9da89fbbcfc0335d1bbafb14a8117620 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_1__flag_red );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_1__flag_red );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_2__flag_green( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_bc3e536fbfa165ee58d9a0d82198ce1d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bc3e536fbfa165ee58d9a0d82198ce1d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bc3e536fbfa165ee58d9a0d82198ce1d, codeobj_bc3e536fbfa165ee58d9a0d82198ce1d, module_matplotlib$_cm, sizeof(void *) );
    frame_bc3e536fbfa165ee58d9a0d82198ce1d = cache_frame_bc3e536fbfa165ee58d9a0d82198ce1d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bc3e536fbfa165ee58d9a0d82198ce1d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bc3e536fbfa165ee58d9a0d82198ce1d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_float_31_5;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_bc3e536fbfa165ee58d9a0d82198ce1d->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc3e536fbfa165ee58d9a0d82198ce1d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc3e536fbfa165ee58d9a0d82198ce1d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc3e536fbfa165ee58d9a0d82198ce1d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bc3e536fbfa165ee58d9a0d82198ce1d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bc3e536fbfa165ee58d9a0d82198ce1d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bc3e536fbfa165ee58d9a0d82198ce1d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bc3e536fbfa165ee58d9a0d82198ce1d,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_bc3e536fbfa165ee58d9a0d82198ce1d == cache_frame_bc3e536fbfa165ee58d9a0d82198ce1d )
    {
        Py_DECREF( frame_bc3e536fbfa165ee58d9a0d82198ce1d );
    }
    cache_frame_bc3e536fbfa165ee58d9a0d82198ce1d = NULL;

    assertFrameObject( frame_bc3e536fbfa165ee58d9a0d82198ce1d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_2__flag_green );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_2__flag_green );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_3__flag_blue( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_96dc18310950969d5f48576ce081a466;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_96dc18310950969d5f48576ce081a466 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_96dc18310950969d5f48576ce081a466, codeobj_96dc18310950969d5f48576ce081a466, module_matplotlib$_cm, sizeof(void *) );
    frame_96dc18310950969d5f48576ce081a466 = cache_frame_96dc18310950969d5f48576ce081a466;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_96dc18310950969d5f48576ce081a466 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_96dc18310950969d5f48576ce081a466 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_right_name_4;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_5;
        tmp_left_name_2 = const_float_0_75;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_5 = par_x;
        tmp_right_name_2 = const_float_31_5;
        tmp_left_name_4 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_5, tmp_right_name_2 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_float_0_25;
        tmp_left_name_3 = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_4, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );

            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_3 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_96dc18310950969d5f48576ce081a466->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_MUL_FLOAT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_float_0_5;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96dc18310950969d5f48576ce081a466 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_96dc18310950969d5f48576ce081a466 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96dc18310950969d5f48576ce081a466 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_96dc18310950969d5f48576ce081a466, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_96dc18310950969d5f48576ce081a466->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_96dc18310950969d5f48576ce081a466, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_96dc18310950969d5f48576ce081a466,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_96dc18310950969d5f48576ce081a466 == cache_frame_96dc18310950969d5f48576ce081a466 )
    {
        Py_DECREF( frame_96dc18310950969d5f48576ce081a466 );
    }
    cache_frame_96dc18310950969d5f48576ce081a466 = NULL;

    assertFrameObject( frame_96dc18310950969d5f48576ce081a466 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_3__flag_blue );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_3__flag_blue );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_4__prism_red( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f1d46b9cdedca619e4f3c486cb441557;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f1d46b9cdedca619e4f3c486cb441557 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f1d46b9cdedca619e4f3c486cb441557, codeobj_f1d46b9cdedca619e4f3c486cb441557, module_matplotlib$_cm, sizeof(void *) );
    frame_f1d46b9cdedca619e4f3c486cb441557 = cache_frame_f1d46b9cdedca619e4f3c486cb441557;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f1d46b9cdedca619e4f3c486cb441557 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f1d46b9cdedca619e4f3c486cb441557 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_right_name_4;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_5;
        tmp_left_name_2 = const_float_0_75;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_5 = par_x;
        tmp_right_name_2 = const_float_20_9;
        tmp_left_name_4 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_5, tmp_right_name_2 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_float_0_25;
        tmp_left_name_3 = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_4, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_3 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_f1d46b9cdedca619e4f3c486cb441557->m_frame.f_lineno = 51;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_MUL_FLOAT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_float_0_67;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f1d46b9cdedca619e4f3c486cb441557 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f1d46b9cdedca619e4f3c486cb441557 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f1d46b9cdedca619e4f3c486cb441557 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f1d46b9cdedca619e4f3c486cb441557, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f1d46b9cdedca619e4f3c486cb441557->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f1d46b9cdedca619e4f3c486cb441557, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f1d46b9cdedca619e4f3c486cb441557,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_f1d46b9cdedca619e4f3c486cb441557 == cache_frame_f1d46b9cdedca619e4f3c486cb441557 )
    {
        Py_DECREF( frame_f1d46b9cdedca619e4f3c486cb441557 );
    }
    cache_frame_f1d46b9cdedca619e4f3c486cb441557 = NULL;

    assertFrameObject( frame_f1d46b9cdedca619e4f3c486cb441557 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_4__prism_red );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_4__prism_red );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_5__prism_green( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a5d59bf3a308aedf73adad4d6f80043d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a5d59bf3a308aedf73adad4d6f80043d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a5d59bf3a308aedf73adad4d6f80043d, codeobj_a5d59bf3a308aedf73adad4d6f80043d, module_matplotlib$_cm, sizeof(void *) );
    frame_a5d59bf3a308aedf73adad4d6f80043d = cache_frame_a5d59bf3a308aedf73adad4d6f80043d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a5d59bf3a308aedf73adad4d6f80043d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a5d59bf3a308aedf73adad4d6f80043d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_right_name_4;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_5;
        tmp_left_name_2 = const_float_0_75;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_5 = par_x;
        tmp_right_name_2 = const_float_20_9;
        tmp_left_name_4 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_5, tmp_right_name_2 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_float_0_25;
        tmp_left_name_3 = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_4, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_3 );

            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_3 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_a5d59bf3a308aedf73adad4d6f80043d->m_frame.f_lineno = 52;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_MUL_FLOAT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_float_0_33;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5d59bf3a308aedf73adad4d6f80043d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5d59bf3a308aedf73adad4d6f80043d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5d59bf3a308aedf73adad4d6f80043d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a5d59bf3a308aedf73adad4d6f80043d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a5d59bf3a308aedf73adad4d6f80043d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a5d59bf3a308aedf73adad4d6f80043d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a5d59bf3a308aedf73adad4d6f80043d,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_a5d59bf3a308aedf73adad4d6f80043d == cache_frame_a5d59bf3a308aedf73adad4d6f80043d )
    {
        Py_DECREF( frame_a5d59bf3a308aedf73adad4d6f80043d );
    }
    cache_frame_a5d59bf3a308aedf73adad4d6f80043d = NULL;

    assertFrameObject( frame_a5d59bf3a308aedf73adad4d6f80043d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_5__prism_green );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_5__prism_green );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_6__prism_blue( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_71c9beb9d5e02f5ecc46acc3901775d6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_71c9beb9d5e02f5ecc46acc3901775d6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_71c9beb9d5e02f5ecc46acc3901775d6, codeobj_71c9beb9d5e02f5ecc46acc3901775d6, module_matplotlib$_cm, sizeof(void *) );
    frame_71c9beb9d5e02f5ecc46acc3901775d6 = cache_frame_71c9beb9d5e02f5ecc46acc3901775d6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_71c9beb9d5e02f5ecc46acc3901775d6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_71c9beb9d5e02f5ecc46acc3901775d6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_left_name_1 = const_float_minus_1_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_3 = par_x;
        tmp_right_name_2 = const_float_20_9;
        tmp_left_name_2 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_3, tmp_right_name_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_71c9beb9d5e02f5ecc46acc3901775d6->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_MUL_FLOAT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_71c9beb9d5e02f5ecc46acc3901775d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_71c9beb9d5e02f5ecc46acc3901775d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_71c9beb9d5e02f5ecc46acc3901775d6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_71c9beb9d5e02f5ecc46acc3901775d6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_71c9beb9d5e02f5ecc46acc3901775d6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_71c9beb9d5e02f5ecc46acc3901775d6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_71c9beb9d5e02f5ecc46acc3901775d6,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_71c9beb9d5e02f5ecc46acc3901775d6 == cache_frame_71c9beb9d5e02f5ecc46acc3901775d6 )
    {
        Py_DECREF( frame_71c9beb9d5e02f5ecc46acc3901775d6 );
    }
    cache_frame_71c9beb9d5e02f5ecc46acc3901775d6 = NULL;

    assertFrameObject( frame_71c9beb9d5e02f5ecc46acc3901775d6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_6__prism_blue );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_6__prism_blue );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_7__ch_helper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_gamma = python_pars[ 0 ];
    PyObject *par_s = python_pars[ 1 ];
    PyObject *par_r = python_pars[ 2 ];
    PyObject *par_h = python_pars[ 3 ];
    PyObject *par_p0 = python_pars[ 4 ];
    PyObject *par_p1 = python_pars[ 5 ];
    PyObject *par_x = python_pars[ 6 ];
    PyObject *var_xg = NULL;
    PyObject *var_a = NULL;
    PyObject *var_phi = NULL;
    struct Nuitka_FrameObject *frame_80e296267b0748a2b363ed54c4de79e8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_80e296267b0748a2b363ed54c4de79e8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_80e296267b0748a2b363ed54c4de79e8, codeobj_80e296267b0748a2b363ed54c4de79e8, module_matplotlib$_cm, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_80e296267b0748a2b363ed54c4de79e8 = cache_frame_80e296267b0748a2b363ed54c4de79e8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_80e296267b0748a2b363ed54c4de79e8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_80e296267b0748a2b363ed54c4de79e8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_x );
        tmp_left_name_1 = par_x;
        CHECK_OBJECT( par_gamma );
        tmp_right_name_1 = par_gamma;
        tmp_assign_source_1 = POWER_OPERATION( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_xg == NULL );
        var_xg = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_4;
        PyObject *tmp_right_name_5;
        CHECK_OBJECT( par_h );
        tmp_left_name_4 = par_h;
        CHECK_OBJECT( var_xg );
        tmp_right_name_2 = var_xg;
        tmp_left_name_3 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_2 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_5 = const_int_pos_1;
        CHECK_OBJECT( var_xg );
        tmp_right_name_4 = var_xg;
        tmp_right_name_3 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_5, tmp_right_name_4 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_3 );

            exception_lineno = 62;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_int_pos_2;
        tmp_assign_source_2 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_2, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_a == NULL );
        var_a = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_6;
        PyObject *tmp_left_name_7;
        PyObject *tmp_right_name_6;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_right_name_7;
        PyObject *tmp_left_name_8;
        PyObject *tmp_left_name_9;
        PyObject *tmp_right_name_8;
        PyObject *tmp_right_name_9;
        PyObject *tmp_left_name_10;
        PyObject *tmp_right_name_10;
        tmp_left_name_7 = const_int_pos_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_right_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_pi );
        if ( tmp_right_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_6 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_7, tmp_right_name_6 );
        Py_DECREF( tmp_right_name_6 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_s );
        tmp_left_name_9 = par_s;
        tmp_right_name_8 = const_int_pos_3;
        tmp_left_name_8 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_9, tmp_right_name_8 );
        if ( tmp_left_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_6 );

            exception_lineno = 63;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_r );
        tmp_left_name_10 = par_r;
        CHECK_OBJECT( par_x );
        tmp_right_name_10 = par_x;
        tmp_right_name_9 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_10, tmp_right_name_10 );
        if ( tmp_right_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_6 );
            Py_DECREF( tmp_left_name_8 );

            exception_lineno = 63;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_7 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_8, tmp_right_name_9 );
        Py_DECREF( tmp_left_name_8 );
        Py_DECREF( tmp_right_name_9 );
        if ( tmp_right_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_6 );

            exception_lineno = 63;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_7 );
        Py_DECREF( tmp_left_name_6 );
        Py_DECREF( tmp_right_name_7 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_phi == NULL );
        var_phi = tmp_assign_source_3;
    }
    {
        PyObject *tmp_left_name_11;
        PyObject *tmp_right_name_11;
        PyObject *tmp_left_name_12;
        PyObject *tmp_right_name_12;
        PyObject *tmp_left_name_13;
        PyObject *tmp_left_name_14;
        PyObject *tmp_right_name_13;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_right_name_14;
        PyObject *tmp_left_name_15;
        PyObject *tmp_right_name_15;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_xg );
        tmp_left_name_11 = var_xg;
        CHECK_OBJECT( var_a );
        tmp_left_name_12 = var_a;
        CHECK_OBJECT( par_p0 );
        tmp_left_name_14 = par_p0;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        CHECK_OBJECT( var_phi );
        tmp_args_element_name_1 = var_phi;
        frame_80e296267b0748a2b363ed54c4de79e8->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_cos, call_args );
        }

        if ( tmp_right_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_13 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_14, tmp_right_name_13 );
        Py_DECREF( tmp_right_name_13 );
        if ( tmp_left_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_p1 );
        tmp_left_name_15 = par_p1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_left_name_13 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_3;
        CHECK_OBJECT( var_phi );
        tmp_args_element_name_2 = var_phi;
        frame_80e296267b0748a2b363ed54c4de79e8->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_right_name_15 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_sin, call_args );
        }

        if ( tmp_right_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_13 );

            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_14 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_15, tmp_right_name_15 );
        Py_DECREF( tmp_right_name_15 );
        if ( tmp_right_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_13 );

            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_12 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_13, tmp_right_name_14 );
        Py_DECREF( tmp_left_name_13 );
        Py_DECREF( tmp_right_name_14 );
        if ( tmp_right_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_11 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_12, tmp_right_name_12 );
        Py_DECREF( tmp_right_name_12 );
        if ( tmp_right_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_11, tmp_right_name_11 );
        Py_DECREF( tmp_right_name_11 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_80e296267b0748a2b363ed54c4de79e8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_80e296267b0748a2b363ed54c4de79e8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_80e296267b0748a2b363ed54c4de79e8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_80e296267b0748a2b363ed54c4de79e8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_80e296267b0748a2b363ed54c4de79e8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_80e296267b0748a2b363ed54c4de79e8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_80e296267b0748a2b363ed54c4de79e8,
        type_description_1,
        par_gamma,
        par_s,
        par_r,
        par_h,
        par_p0,
        par_p1,
        par_x,
        var_xg,
        var_a,
        var_phi
    );


    // Release cached frame.
    if ( frame_80e296267b0748a2b363ed54c4de79e8 == cache_frame_80e296267b0748a2b363ed54c4de79e8 )
    {
        Py_DECREF( frame_80e296267b0748a2b363ed54c4de79e8 );
    }
    cache_frame_80e296267b0748a2b363ed54c4de79e8 = NULL;

    assertFrameObject( frame_80e296267b0748a2b363ed54c4de79e8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_7__ch_helper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_gamma );
    Py_DECREF( par_gamma );
    par_gamma = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_h );
    Py_DECREF( par_h );
    par_h = NULL;

    CHECK_OBJECT( (PyObject *)par_p0 );
    Py_DECREF( par_p0 );
    par_p0 = NULL;

    CHECK_OBJECT( (PyObject *)par_p1 );
    Py_DECREF( par_p1 );
    par_p1 = NULL;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)var_xg );
    Py_DECREF( var_xg );
    var_xg = NULL;

    CHECK_OBJECT( (PyObject *)var_a );
    Py_DECREF( var_a );
    var_a = NULL;

    CHECK_OBJECT( (PyObject *)var_phi );
    Py_DECREF( var_phi );
    var_phi = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_gamma );
    Py_DECREF( par_gamma );
    par_gamma = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_h );
    Py_DECREF( par_h );
    par_h = NULL;

    CHECK_OBJECT( (PyObject *)par_p0 );
    Py_DECREF( par_p0 );
    par_p0 = NULL;

    CHECK_OBJECT( (PyObject *)par_p1 );
    Py_DECREF( par_p1 );
    par_p1 = NULL;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    Py_XDECREF( var_xg );
    var_xg = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_phi );
    var_phi = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_7__ch_helper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_8_cubehelix( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_gamma = python_pars[ 0 ];
    PyObject *par_s = python_pars[ 1 ];
    PyObject *par_r = python_pars[ 2 ];
    PyObject *par_h = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_32d77dfbf1973b6159d4f408ea9d29f7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_32d77dfbf1973b6159d4f408ea9d29f7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_32d77dfbf1973b6159d4f408ea9d29f7, codeobj_32d77dfbf1973b6159d4f408ea9d29f7, module_matplotlib$_cm, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_32d77dfbf1973b6159d4f408ea9d29f7 = cache_frame_32d77dfbf1973b6159d4f408ea9d29f7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_32d77dfbf1973b6159d4f408ea9d29f7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_32d77dfbf1973b6159d4f408ea9d29f7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_args_element_name_21;
        tmp_dict_key_1 = const_str_plain_red;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_partial );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_partial );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "partial" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__ch_helper );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ch_helper );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ch_helper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_gamma );
        tmp_args_element_name_2 = par_gamma;
        CHECK_OBJECT( par_s );
        tmp_args_element_name_3 = par_s;
        CHECK_OBJECT( par_r );
        tmp_args_element_name_4 = par_r;
        CHECK_OBJECT( par_h );
        tmp_args_element_name_5 = par_h;
        tmp_args_element_name_6 = const_float_minus_0_14861;
        tmp_args_element_name_7 = const_float_1_78277;
        frame_32d77dfbf1973b6159d4f408ea9d29f7->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS7( tmp_called_name_1, call_args );
        }

        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_green;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_partial );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_partial );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "partial" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__ch_helper );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ch_helper );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ch_helper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_8 = tmp_mvar_value_4;
        CHECK_OBJECT( par_gamma );
        tmp_args_element_name_9 = par_gamma;
        CHECK_OBJECT( par_s );
        tmp_args_element_name_10 = par_s;
        CHECK_OBJECT( par_r );
        tmp_args_element_name_11 = par_r;
        CHECK_OBJECT( par_h );
        tmp_args_element_name_12 = par_h;
        tmp_args_element_name_13 = const_float_minus_0_29227;
        tmp_args_element_name_14 = const_float_minus_0_90649;
        frame_32d77dfbf1973b6159d4f408ea9d29f7->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_dict_value_2 = CALL_FUNCTION_WITH_ARGS7( tmp_called_name_2, call_args );
        }

        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 105;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_blue;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_partial );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_partial );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "partial" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_5;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__ch_helper );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ch_helper );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ch_helper" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_15 = tmp_mvar_value_6;
        CHECK_OBJECT( par_gamma );
        tmp_args_element_name_16 = par_gamma;
        CHECK_OBJECT( par_s );
        tmp_args_element_name_17 = par_s;
        CHECK_OBJECT( par_r );
        tmp_args_element_name_18 = par_r;
        CHECK_OBJECT( par_h );
        tmp_args_element_name_19 = par_h;
        tmp_args_element_name_20 = const_float_1_97294;
        tmp_args_element_name_21 = const_float_0_0;
        frame_32d77dfbf1973b6159d4f408ea9d29f7->m_frame.f_lineno = 106;
        {
            PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16, tmp_args_element_name_17, tmp_args_element_name_18, tmp_args_element_name_19, tmp_args_element_name_20, tmp_args_element_name_21 };
            tmp_dict_value_3 = CALL_FUNCTION_WITH_ARGS7( tmp_called_name_3, call_args );
        }

        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 106;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32d77dfbf1973b6159d4f408ea9d29f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_32d77dfbf1973b6159d4f408ea9d29f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32d77dfbf1973b6159d4f408ea9d29f7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_32d77dfbf1973b6159d4f408ea9d29f7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_32d77dfbf1973b6159d4f408ea9d29f7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_32d77dfbf1973b6159d4f408ea9d29f7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_32d77dfbf1973b6159d4f408ea9d29f7,
        type_description_1,
        par_gamma,
        par_s,
        par_r,
        par_h
    );


    // Release cached frame.
    if ( frame_32d77dfbf1973b6159d4f408ea9d29f7 == cache_frame_32d77dfbf1973b6159d4f408ea9d29f7 )
    {
        Py_DECREF( frame_32d77dfbf1973b6159d4f408ea9d29f7 );
    }
    cache_frame_32d77dfbf1973b6159d4f408ea9d29f7 = NULL;

    assertFrameObject( frame_32d77dfbf1973b6159d4f408ea9d29f7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_8_cubehelix );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_gamma );
    Py_DECREF( par_gamma );
    par_gamma = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_h );
    Py_DECREF( par_h );
    par_h = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_gamma );
    Py_DECREF( par_gamma );
    par_gamma = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_h );
    Py_DECREF( par_h );
    par_h = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_8_cubehelix );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_9__g0( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_int_0;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_9__g0 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_9__g0 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_10__g1( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_float_0_5;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_10__g1 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_10__g1 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_11__g2( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_int_pos_1;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_11__g2 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_11__g2 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_12__g3( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_x );
    tmp_return_value = par_x;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_12__g3 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_12__g3 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_13__g4( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5cf23711c93867864876412ea15953a6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5cf23711c93867864876412ea15953a6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5cf23711c93867864876412ea15953a6, codeobj_5cf23711c93867864876412ea15953a6, module_matplotlib$_cm, sizeof(void *) );
    frame_5cf23711c93867864876412ea15953a6 = cache_frame_5cf23711c93867864876412ea15953a6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5cf23711c93867864876412ea15953a6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5cf23711c93867864876412ea15953a6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_x );
        tmp_left_name_1 = par_x;
        tmp_right_name_1 = const_int_pos_2;
        tmp_return_value = POWER_OPERATION( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5cf23711c93867864876412ea15953a6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5cf23711c93867864876412ea15953a6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5cf23711c93867864876412ea15953a6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5cf23711c93867864876412ea15953a6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5cf23711c93867864876412ea15953a6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5cf23711c93867864876412ea15953a6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5cf23711c93867864876412ea15953a6,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_5cf23711c93867864876412ea15953a6 == cache_frame_5cf23711c93867864876412ea15953a6 )
    {
        Py_DECREF( frame_5cf23711c93867864876412ea15953a6 );
    }
    cache_frame_5cf23711c93867864876412ea15953a6 = NULL;

    assertFrameObject( frame_5cf23711c93867864876412ea15953a6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_13__g4 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_13__g4 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_14__g5( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e6223b72b1e3a56299cfc38b154bc21e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e6223b72b1e3a56299cfc38b154bc21e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e6223b72b1e3a56299cfc38b154bc21e, codeobj_e6223b72b1e3a56299cfc38b154bc21e, module_matplotlib$_cm, sizeof(void *) );
    frame_e6223b72b1e3a56299cfc38b154bc21e = cache_frame_e6223b72b1e3a56299cfc38b154bc21e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e6223b72b1e3a56299cfc38b154bc21e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e6223b72b1e3a56299cfc38b154bc21e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_x );
        tmp_left_name_1 = par_x;
        tmp_right_name_1 = const_int_pos_3;
        tmp_return_value = POWER_OPERATION( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e6223b72b1e3a56299cfc38b154bc21e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e6223b72b1e3a56299cfc38b154bc21e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e6223b72b1e3a56299cfc38b154bc21e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e6223b72b1e3a56299cfc38b154bc21e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e6223b72b1e3a56299cfc38b154bc21e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e6223b72b1e3a56299cfc38b154bc21e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e6223b72b1e3a56299cfc38b154bc21e,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_e6223b72b1e3a56299cfc38b154bc21e == cache_frame_e6223b72b1e3a56299cfc38b154bc21e )
    {
        Py_DECREF( frame_e6223b72b1e3a56299cfc38b154bc21e );
    }
    cache_frame_e6223b72b1e3a56299cfc38b154bc21e = NULL;

    assertFrameObject( frame_e6223b72b1e3a56299cfc38b154bc21e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_14__g5 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_14__g5 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_15__g6( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_857da5f6463870875e79355e4ffcc4e0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_857da5f6463870875e79355e4ffcc4e0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_857da5f6463870875e79355e4ffcc4e0, codeobj_857da5f6463870875e79355e4ffcc4e0, module_matplotlib$_cm, sizeof(void *) );
    frame_857da5f6463870875e79355e4ffcc4e0 = cache_frame_857da5f6463870875e79355e4ffcc4e0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_857da5f6463870875e79355e4ffcc4e0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_857da5f6463870875e79355e4ffcc4e0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_x );
        tmp_left_name_1 = par_x;
        tmp_right_name_1 = const_int_pos_4;
        tmp_return_value = POWER_OPERATION( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_857da5f6463870875e79355e4ffcc4e0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_857da5f6463870875e79355e4ffcc4e0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_857da5f6463870875e79355e4ffcc4e0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_857da5f6463870875e79355e4ffcc4e0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_857da5f6463870875e79355e4ffcc4e0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_857da5f6463870875e79355e4ffcc4e0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_857da5f6463870875e79355e4ffcc4e0,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_857da5f6463870875e79355e4ffcc4e0 == cache_frame_857da5f6463870875e79355e4ffcc4e0 )
    {
        Py_DECREF( frame_857da5f6463870875e79355e4ffcc4e0 );
    }
    cache_frame_857da5f6463870875e79355e4ffcc4e0 = NULL;

    assertFrameObject( frame_857da5f6463870875e79355e4ffcc4e0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_15__g6 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_15__g6 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_16__g7( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f91be58d49ac067f82c34fb0c522641c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f91be58d49ac067f82c34fb0c522641c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f91be58d49ac067f82c34fb0c522641c, codeobj_f91be58d49ac067f82c34fb0c522641c, module_matplotlib$_cm, sizeof(void *) );
    frame_f91be58d49ac067f82c34fb0c522641c = cache_frame_f91be58d49ac067f82c34fb0c522641c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f91be58d49ac067f82c34fb0c522641c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f91be58d49ac067f82c34fb0c522641c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_x );
        tmp_args_element_name_1 = par_x;
        frame_f91be58d49ac067f82c34fb0c522641c->m_frame.f_lineno = 121;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_sqrt, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f91be58d49ac067f82c34fb0c522641c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f91be58d49ac067f82c34fb0c522641c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f91be58d49ac067f82c34fb0c522641c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f91be58d49ac067f82c34fb0c522641c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f91be58d49ac067f82c34fb0c522641c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f91be58d49ac067f82c34fb0c522641c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f91be58d49ac067f82c34fb0c522641c,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_f91be58d49ac067f82c34fb0c522641c == cache_frame_f91be58d49ac067f82c34fb0c522641c )
    {
        Py_DECREF( frame_f91be58d49ac067f82c34fb0c522641c );
    }
    cache_frame_f91be58d49ac067f82c34fb0c522641c = NULL;

    assertFrameObject( frame_f91be58d49ac067f82c34fb0c522641c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_16__g7 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_16__g7 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_17__g8( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_572b6279086edfe79cc5d4b59b9d3a78;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_572b6279086edfe79cc5d4b59b9d3a78 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_572b6279086edfe79cc5d4b59b9d3a78, codeobj_572b6279086edfe79cc5d4b59b9d3a78, module_matplotlib$_cm, sizeof(void *) );
    frame_572b6279086edfe79cc5d4b59b9d3a78 = cache_frame_572b6279086edfe79cc5d4b59b9d3a78;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_572b6279086edfe79cc5d4b59b9d3a78 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_572b6279086edfe79cc5d4b59b9d3a78 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sqrt );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_x );
        tmp_args_element_name_2 = par_x;
        frame_572b6279086edfe79cc5d4b59b9d3a78->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_sqrt, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_572b6279086edfe79cc5d4b59b9d3a78->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_572b6279086edfe79cc5d4b59b9d3a78 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_572b6279086edfe79cc5d4b59b9d3a78 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_572b6279086edfe79cc5d4b59b9d3a78 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_572b6279086edfe79cc5d4b59b9d3a78, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_572b6279086edfe79cc5d4b59b9d3a78->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_572b6279086edfe79cc5d4b59b9d3a78, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_572b6279086edfe79cc5d4b59b9d3a78,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_572b6279086edfe79cc5d4b59b9d3a78 == cache_frame_572b6279086edfe79cc5d4b59b9d3a78 )
    {
        Py_DECREF( frame_572b6279086edfe79cc5d4b59b9d3a78 );
    }
    cache_frame_572b6279086edfe79cc5d4b59b9d3a78 = NULL;

    assertFrameObject( frame_572b6279086edfe79cc5d4b59b9d3a78 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_17__g8 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_17__g8 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_18__g9( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0665c21023d00b9af46ab168ecffda2d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0665c21023d00b9af46ab168ecffda2d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0665c21023d00b9af46ab168ecffda2d, codeobj_0665c21023d00b9af46ab168ecffda2d, module_matplotlib$_cm, sizeof(void *) );
    frame_0665c21023d00b9af46ab168ecffda2d = cache_frame_0665c21023d00b9af46ab168ecffda2d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0665c21023d00b9af46ab168ecffda2d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0665c21023d00b9af46ab168ecffda2d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 123;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 123;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_2;
        tmp_args_element_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 123;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_0665c21023d00b9af46ab168ecffda2d->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0665c21023d00b9af46ab168ecffda2d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0665c21023d00b9af46ab168ecffda2d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0665c21023d00b9af46ab168ecffda2d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0665c21023d00b9af46ab168ecffda2d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0665c21023d00b9af46ab168ecffda2d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0665c21023d00b9af46ab168ecffda2d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0665c21023d00b9af46ab168ecffda2d,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_0665c21023d00b9af46ab168ecffda2d == cache_frame_0665c21023d00b9af46ab168ecffda2d )
    {
        Py_DECREF( frame_0665c21023d00b9af46ab168ecffda2d );
    }
    cache_frame_0665c21023d00b9af46ab168ecffda2d = NULL;

    assertFrameObject( frame_0665c21023d00b9af46ab168ecffda2d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_18__g9 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_18__g9 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_19__g10( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6f001a3c707cfa288f8adae11f30a1d9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6f001a3c707cfa288f8adae11f30a1d9 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6f001a3c707cfa288f8adae11f30a1d9, codeobj_6f001a3c707cfa288f8adae11f30a1d9, module_matplotlib$_cm, sizeof(void *) );
    frame_6f001a3c707cfa288f8adae11f30a1d9 = cache_frame_6f001a3c707cfa288f8adae11f30a1d9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6f001a3c707cfa288f8adae11f30a1d9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6f001a3c707cfa288f8adae11f30a1d9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_cos );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 124;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 124;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_2;
        tmp_args_element_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 124;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_6f001a3c707cfa288f8adae11f30a1d9->m_frame.f_lineno = 124;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6f001a3c707cfa288f8adae11f30a1d9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6f001a3c707cfa288f8adae11f30a1d9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6f001a3c707cfa288f8adae11f30a1d9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6f001a3c707cfa288f8adae11f30a1d9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6f001a3c707cfa288f8adae11f30a1d9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6f001a3c707cfa288f8adae11f30a1d9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6f001a3c707cfa288f8adae11f30a1d9,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_6f001a3c707cfa288f8adae11f30a1d9 == cache_frame_6f001a3c707cfa288f8adae11f30a1d9 )
    {
        Py_DECREF( frame_6f001a3c707cfa288f8adae11f30a1d9 );
    }
    cache_frame_6f001a3c707cfa288f8adae11f30a1d9 = NULL;

    assertFrameObject( frame_6f001a3c707cfa288f8adae11f30a1d9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_19__g10 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_19__g10 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_20__g11( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a43ef645600be41fb6834b58933b4683;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a43ef645600be41fb6834b58933b4683 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a43ef645600be41fb6834b58933b4683, codeobj_a43ef645600be41fb6834b58933b4683, module_matplotlib$_cm, sizeof(void *) );
    frame_a43ef645600be41fb6834b58933b4683 = cache_frame_a43ef645600be41fb6834b58933b4683;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a43ef645600be41fb6834b58933b4683 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a43ef645600be41fb6834b58933b4683 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_1 = par_x;
        tmp_right_name_1 = const_float_0_5;
        tmp_args_element_name_1 = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 125;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_a43ef645600be41fb6834b58933b4683->m_frame.f_lineno = 125;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a43ef645600be41fb6834b58933b4683 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a43ef645600be41fb6834b58933b4683 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a43ef645600be41fb6834b58933b4683 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a43ef645600be41fb6834b58933b4683, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a43ef645600be41fb6834b58933b4683->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a43ef645600be41fb6834b58933b4683, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a43ef645600be41fb6834b58933b4683,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_a43ef645600be41fb6834b58933b4683 == cache_frame_a43ef645600be41fb6834b58933b4683 )
    {
        Py_DECREF( frame_a43ef645600be41fb6834b58933b4683 );
    }
    cache_frame_a43ef645600be41fb6834b58933b4683 = NULL;

    assertFrameObject( frame_a43ef645600be41fb6834b58933b4683 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_20__g11 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_20__g11 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_21__g12( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3d07f3dd6ca66b2aecfeae9495e79601;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3d07f3dd6ca66b2aecfeae9495e79601 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3d07f3dd6ca66b2aecfeae9495e79601, codeobj_3d07f3dd6ca66b2aecfeae9495e79601, module_matplotlib$_cm, sizeof(void *) );
    frame_3d07f3dd6ca66b2aecfeae9495e79601 = cache_frame_3d07f3dd6ca66b2aecfeae9495e79601;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3d07f3dd6ca66b2aecfeae9495e79601 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3d07f3dd6ca66b2aecfeae9495e79601 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        tmp_left_name_3 = const_int_pos_2;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_2 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_3, tmp_right_name_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_left_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_2;
        tmp_return_value = POWER_OPERATION( tmp_left_name_1, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d07f3dd6ca66b2aecfeae9495e79601 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d07f3dd6ca66b2aecfeae9495e79601 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d07f3dd6ca66b2aecfeae9495e79601 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3d07f3dd6ca66b2aecfeae9495e79601, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3d07f3dd6ca66b2aecfeae9495e79601->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3d07f3dd6ca66b2aecfeae9495e79601, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3d07f3dd6ca66b2aecfeae9495e79601,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_3d07f3dd6ca66b2aecfeae9495e79601 == cache_frame_3d07f3dd6ca66b2aecfeae9495e79601 )
    {
        Py_DECREF( frame_3d07f3dd6ca66b2aecfeae9495e79601 );
    }
    cache_frame_3d07f3dd6ca66b2aecfeae9495e79601 = NULL;

    assertFrameObject( frame_3d07f3dd6ca66b2aecfeae9495e79601 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_21__g12 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_21__g12 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_22__g13( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_8b0e4f93545329d1d94e42993abf7216;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8b0e4f93545329d1d94e42993abf7216 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8b0e4f93545329d1d94e42993abf7216, codeobj_8b0e4f93545329d1d94e42993abf7216, module_matplotlib$_cm, sizeof(void *) );
    frame_8b0e4f93545329d1d94e42993abf7216 = cache_frame_8b0e4f93545329d1d94e42993abf7216;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8b0e4f93545329d1d94e42993abf7216 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8b0e4f93545329d1d94e42993abf7216 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_1 = par_x;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 127;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 127;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_8b0e4f93545329d1d94e42993abf7216->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b0e4f93545329d1d94e42993abf7216 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b0e4f93545329d1d94e42993abf7216 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b0e4f93545329d1d94e42993abf7216 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8b0e4f93545329d1d94e42993abf7216, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8b0e4f93545329d1d94e42993abf7216->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8b0e4f93545329d1d94e42993abf7216, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8b0e4f93545329d1d94e42993abf7216,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_8b0e4f93545329d1d94e42993abf7216 == cache_frame_8b0e4f93545329d1d94e42993abf7216 )
    {
        Py_DECREF( frame_8b0e4f93545329d1d94e42993abf7216 );
    }
    cache_frame_8b0e4f93545329d1d94e42993abf7216 = NULL;

    assertFrameObject( frame_8b0e4f93545329d1d94e42993abf7216 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_22__g13 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_22__g13 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_23__g14( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c09d6f101ce2957484c6ba3b60f2f5cf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c09d6f101ce2957484c6ba3b60f2f5cf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c09d6f101ce2957484c6ba3b60f2f5cf, codeobj_c09d6f101ce2957484c6ba3b60f2f5cf, module_matplotlib$_cm, sizeof(void *) );
    frame_c09d6f101ce2957484c6ba3b60f2f5cf = cache_frame_c09d6f101ce2957484c6ba3b60f2f5cf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c09d6f101ce2957484c6ba3b60f2f5cf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c09d6f101ce2957484c6ba3b60f2f5cf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cos );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_1 = par_x;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pi );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_c09d6f101ce2957484c6ba3b60f2f5cf->m_frame.f_lineno = 128;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_c09d6f101ce2957484c6ba3b60f2f5cf->m_frame.f_lineno = 128;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c09d6f101ce2957484c6ba3b60f2f5cf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c09d6f101ce2957484c6ba3b60f2f5cf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c09d6f101ce2957484c6ba3b60f2f5cf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c09d6f101ce2957484c6ba3b60f2f5cf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c09d6f101ce2957484c6ba3b60f2f5cf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c09d6f101ce2957484c6ba3b60f2f5cf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c09d6f101ce2957484c6ba3b60f2f5cf,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_c09d6f101ce2957484c6ba3b60f2f5cf == cache_frame_c09d6f101ce2957484c6ba3b60f2f5cf )
    {
        Py_DECREF( frame_c09d6f101ce2957484c6ba3b60f2f5cf );
    }
    cache_frame_c09d6f101ce2957484c6ba3b60f2f5cf = NULL;

    assertFrameObject( frame_c09d6f101ce2957484c6ba3b60f2f5cf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_23__g14 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_23__g14 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_24__g15( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_8cff908993bcdf98dabe0c3736dcf388;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8cff908993bcdf98dabe0c3736dcf388 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8cff908993bcdf98dabe0c3736dcf388, codeobj_8cff908993bcdf98dabe0c3736dcf388, module_matplotlib$_cm, sizeof(void *) );
    frame_8cff908993bcdf98dabe0c3736dcf388 = cache_frame_8cff908993bcdf98dabe0c3736dcf388;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8cff908993bcdf98dabe0c3736dcf388 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8cff908993bcdf98dabe0c3736dcf388 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sin );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_int_pos_2;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_8cff908993bcdf98dabe0c3736dcf388->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cff908993bcdf98dabe0c3736dcf388 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cff908993bcdf98dabe0c3736dcf388 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cff908993bcdf98dabe0c3736dcf388 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8cff908993bcdf98dabe0c3736dcf388, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8cff908993bcdf98dabe0c3736dcf388->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8cff908993bcdf98dabe0c3736dcf388, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8cff908993bcdf98dabe0c3736dcf388,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_8cff908993bcdf98dabe0c3736dcf388 == cache_frame_8cff908993bcdf98dabe0c3736dcf388 )
    {
        Py_DECREF( frame_8cff908993bcdf98dabe0c3736dcf388 );
    }
    cache_frame_8cff908993bcdf98dabe0c3736dcf388 = NULL;

    assertFrameObject( frame_8cff908993bcdf98dabe0c3736dcf388 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_24__g15 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_24__g15 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_25__g16( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3318b94fe7114ea97fc057bb2890d2d6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3318b94fe7114ea97fc057bb2890d2d6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3318b94fe7114ea97fc057bb2890d2d6, codeobj_3318b94fe7114ea97fc057bb2890d2d6, module_matplotlib$_cm, sizeof(void *) );
    frame_3318b94fe7114ea97fc057bb2890d2d6 = cache_frame_3318b94fe7114ea97fc057bb2890d2d6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3318b94fe7114ea97fc057bb2890d2d6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3318b94fe7114ea97fc057bb2890d2d6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_cos );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_int_pos_2;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 130;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pi );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 130;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 130;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_3318b94fe7114ea97fc057bb2890d2d6->m_frame.f_lineno = 130;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3318b94fe7114ea97fc057bb2890d2d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3318b94fe7114ea97fc057bb2890d2d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3318b94fe7114ea97fc057bb2890d2d6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3318b94fe7114ea97fc057bb2890d2d6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3318b94fe7114ea97fc057bb2890d2d6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3318b94fe7114ea97fc057bb2890d2d6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3318b94fe7114ea97fc057bb2890d2d6,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_3318b94fe7114ea97fc057bb2890d2d6 == cache_frame_3318b94fe7114ea97fc057bb2890d2d6 )
    {
        Py_DECREF( frame_3318b94fe7114ea97fc057bb2890d2d6 );
    }
    cache_frame_3318b94fe7114ea97fc057bb2890d2d6 = NULL;

    assertFrameObject( frame_3318b94fe7114ea97fc057bb2890d2d6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_25__g16 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_25__g16 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_26__g17( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2ed3244b65d5d34826d10d89ba234077;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2ed3244b65d5d34826d10d89ba234077 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ed3244b65d5d34826d10d89ba234077, codeobj_2ed3244b65d5d34826d10d89ba234077, module_matplotlib$_cm, sizeof(void *) );
    frame_2ed3244b65d5d34826d10d89ba234077 = cache_frame_2ed3244b65d5d34826d10d89ba234077;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ed3244b65d5d34826d10d89ba234077 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ed3244b65d5d34826d10d89ba234077 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_sin );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_int_pos_2;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pi );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_2ed3244b65d5d34826d10d89ba234077->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_2ed3244b65d5d34826d10d89ba234077->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ed3244b65d5d34826d10d89ba234077 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ed3244b65d5d34826d10d89ba234077 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ed3244b65d5d34826d10d89ba234077 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ed3244b65d5d34826d10d89ba234077, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ed3244b65d5d34826d10d89ba234077->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ed3244b65d5d34826d10d89ba234077, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ed3244b65d5d34826d10d89ba234077,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_2ed3244b65d5d34826d10d89ba234077 == cache_frame_2ed3244b65d5d34826d10d89ba234077 )
    {
        Py_DECREF( frame_2ed3244b65d5d34826d10d89ba234077 );
    }
    cache_frame_2ed3244b65d5d34826d10d89ba234077 = NULL;

    assertFrameObject( frame_2ed3244b65d5d34826d10d89ba234077 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_26__g17 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_26__g17 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_27__g18( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_acf507c7209ba5bcc78d816026bbdd7b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_acf507c7209ba5bcc78d816026bbdd7b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_acf507c7209ba5bcc78d816026bbdd7b, codeobj_acf507c7209ba5bcc78d816026bbdd7b, module_matplotlib$_cm, sizeof(void *) );
    frame_acf507c7209ba5bcc78d816026bbdd7b = cache_frame_acf507c7209ba5bcc78d816026bbdd7b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_acf507c7209ba5bcc78d816026bbdd7b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_acf507c7209ba5bcc78d816026bbdd7b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cos );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_int_pos_2;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pi );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_acf507c7209ba5bcc78d816026bbdd7b->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_acf507c7209ba5bcc78d816026bbdd7b->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_acf507c7209ba5bcc78d816026bbdd7b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_acf507c7209ba5bcc78d816026bbdd7b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_acf507c7209ba5bcc78d816026bbdd7b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_acf507c7209ba5bcc78d816026bbdd7b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_acf507c7209ba5bcc78d816026bbdd7b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_acf507c7209ba5bcc78d816026bbdd7b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_acf507c7209ba5bcc78d816026bbdd7b,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_acf507c7209ba5bcc78d816026bbdd7b == cache_frame_acf507c7209ba5bcc78d816026bbdd7b )
    {
        Py_DECREF( frame_acf507c7209ba5bcc78d816026bbdd7b );
    }
    cache_frame_acf507c7209ba5bcc78d816026bbdd7b = NULL;

    assertFrameObject( frame_acf507c7209ba5bcc78d816026bbdd7b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_27__g18 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_27__g18 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_28__g19( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_642906f415e89b8021acbbc2acfc4bb5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_642906f415e89b8021acbbc2acfc4bb5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_642906f415e89b8021acbbc2acfc4bb5, codeobj_642906f415e89b8021acbbc2acfc4bb5, module_matplotlib$_cm, sizeof(void *) );
    frame_642906f415e89b8021acbbc2acfc4bb5 = cache_frame_642906f415e89b8021acbbc2acfc4bb5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_642906f415e89b8021acbbc2acfc4bb5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_642906f415e89b8021acbbc2acfc4bb5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_sin );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_int_pos_4;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pi );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_642906f415e89b8021acbbc2acfc4bb5->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_642906f415e89b8021acbbc2acfc4bb5->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_642906f415e89b8021acbbc2acfc4bb5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_642906f415e89b8021acbbc2acfc4bb5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_642906f415e89b8021acbbc2acfc4bb5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_642906f415e89b8021acbbc2acfc4bb5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_642906f415e89b8021acbbc2acfc4bb5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_642906f415e89b8021acbbc2acfc4bb5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_642906f415e89b8021acbbc2acfc4bb5,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_642906f415e89b8021acbbc2acfc4bb5 == cache_frame_642906f415e89b8021acbbc2acfc4bb5 )
    {
        Py_DECREF( frame_642906f415e89b8021acbbc2acfc4bb5 );
    }
    cache_frame_642906f415e89b8021acbbc2acfc4bb5 = NULL;

    assertFrameObject( frame_642906f415e89b8021acbbc2acfc4bb5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_28__g19 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_28__g19 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_29__g20( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b185d50514175a783314284b7e1c8664;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b185d50514175a783314284b7e1c8664 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b185d50514175a783314284b7e1c8664, codeobj_b185d50514175a783314284b7e1c8664, module_matplotlib$_cm, sizeof(void *) );
    frame_b185d50514175a783314284b7e1c8664 = cache_frame_b185d50514175a783314284b7e1c8664;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b185d50514175a783314284b7e1c8664 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b185d50514175a783314284b7e1c8664 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cos );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_int_pos_4;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pi );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_b185d50514175a783314284b7e1c8664->m_frame.f_lineno = 134;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_b185d50514175a783314284b7e1c8664->m_frame.f_lineno = 134;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b185d50514175a783314284b7e1c8664 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b185d50514175a783314284b7e1c8664 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b185d50514175a783314284b7e1c8664 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b185d50514175a783314284b7e1c8664, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b185d50514175a783314284b7e1c8664->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b185d50514175a783314284b7e1c8664, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b185d50514175a783314284b7e1c8664,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_b185d50514175a783314284b7e1c8664 == cache_frame_b185d50514175a783314284b7e1c8664 )
    {
        Py_DECREF( frame_b185d50514175a783314284b7e1c8664 );
    }
    cache_frame_b185d50514175a783314284b7e1c8664 = NULL;

    assertFrameObject( frame_b185d50514175a783314284b7e1c8664 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_29__g20 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_29__g20 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_30__g21( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b04b846f655acf9210cbb390b3a144ec;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b04b846f655acf9210cbb390b3a144ec = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b04b846f655acf9210cbb390b3a144ec, codeobj_b04b846f655acf9210cbb390b3a144ec, module_matplotlib$_cm, sizeof(void *) );
    frame_b04b846f655acf9210cbb390b3a144ec = cache_frame_b04b846f655acf9210cbb390b3a144ec;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b04b846f655acf9210cbb390b3a144ec );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b04b846f655acf9210cbb390b3a144ec ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_return_value = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b04b846f655acf9210cbb390b3a144ec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b04b846f655acf9210cbb390b3a144ec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b04b846f655acf9210cbb390b3a144ec );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b04b846f655acf9210cbb390b3a144ec, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b04b846f655acf9210cbb390b3a144ec->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b04b846f655acf9210cbb390b3a144ec, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b04b846f655acf9210cbb390b3a144ec,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_b04b846f655acf9210cbb390b3a144ec == cache_frame_b04b846f655acf9210cbb390b3a144ec )
    {
        Py_DECREF( frame_b04b846f655acf9210cbb390b3a144ec );
    }
    cache_frame_b04b846f655acf9210cbb390b3a144ec = NULL;

    assertFrameObject( frame_b04b846f655acf9210cbb390b3a144ec );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_30__g21 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_30__g21 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_31__g22( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_bb72c4ebde9e34e2e57d29944502cdb1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bb72c4ebde9e34e2e57d29944502cdb1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bb72c4ebde9e34e2e57d29944502cdb1, codeobj_bb72c4ebde9e34e2e57d29944502cdb1, module_matplotlib$_cm, sizeof(void *) );
    frame_bb72c4ebde9e34e2e57d29944502cdb1 = cache_frame_bb72c4ebde9e34e2e57d29944502cdb1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bb72c4ebde9e34e2e57d29944502cdb1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bb72c4ebde9e34e2e57d29944502cdb1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bb72c4ebde9e34e2e57d29944502cdb1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bb72c4ebde9e34e2e57d29944502cdb1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bb72c4ebde9e34e2e57d29944502cdb1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bb72c4ebde9e34e2e57d29944502cdb1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bb72c4ebde9e34e2e57d29944502cdb1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bb72c4ebde9e34e2e57d29944502cdb1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bb72c4ebde9e34e2e57d29944502cdb1,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_bb72c4ebde9e34e2e57d29944502cdb1 == cache_frame_bb72c4ebde9e34e2e57d29944502cdb1 )
    {
        Py_DECREF( frame_bb72c4ebde9e34e2e57d29944502cdb1 );
    }
    cache_frame_bb72c4ebde9e34e2e57d29944502cdb1 = NULL;

    assertFrameObject( frame_bb72c4ebde9e34e2e57d29944502cdb1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_31__g22 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_31__g22 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_32__g23( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_74adb45065271f93643eb2787503f6ff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_74adb45065271f93643eb2787503f6ff = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_74adb45065271f93643eb2787503f6ff, codeobj_74adb45065271f93643eb2787503f6ff, module_matplotlib$_cm, sizeof(void *) );
    frame_74adb45065271f93643eb2787503f6ff = cache_frame_74adb45065271f93643eb2787503f6ff;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_74adb45065271f93643eb2787503f6ff );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_74adb45065271f93643eb2787503f6ff ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_2;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_74adb45065271f93643eb2787503f6ff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_74adb45065271f93643eb2787503f6ff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_74adb45065271f93643eb2787503f6ff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_74adb45065271f93643eb2787503f6ff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_74adb45065271f93643eb2787503f6ff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_74adb45065271f93643eb2787503f6ff, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_74adb45065271f93643eb2787503f6ff,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_74adb45065271f93643eb2787503f6ff == cache_frame_74adb45065271f93643eb2787503f6ff )
    {
        Py_DECREF( frame_74adb45065271f93643eb2787503f6ff );
    }
    cache_frame_74adb45065271f93643eb2787503f6ff = NULL;

    assertFrameObject( frame_74adb45065271f93643eb2787503f6ff );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_32__g23 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_32__g23 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_33__g24( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7a5c1be07c6c8bf106a51f3f3f4989f5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7a5c1be07c6c8bf106a51f3f3f4989f5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7a5c1be07c6c8bf106a51f3f3f4989f5, codeobj_7a5c1be07c6c8bf106a51f3f3f4989f5, module_matplotlib$_cm, sizeof(void *) );
    frame_7a5c1be07c6c8bf106a51f3f3f4989f5 = cache_frame_7a5c1be07c6c8bf106a51f3f3f4989f5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 138;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_args_element_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 138;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_7a5c1be07c6c8bf106a51f3f3f4989f5->m_frame.f_lineno = 138;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7a5c1be07c6c8bf106a51f3f3f4989f5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7a5c1be07c6c8bf106a51f3f3f4989f5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7a5c1be07c6c8bf106a51f3f3f4989f5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7a5c1be07c6c8bf106a51f3f3f4989f5,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 == cache_frame_7a5c1be07c6c8bf106a51f3f3f4989f5 )
    {
        Py_DECREF( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 );
    }
    cache_frame_7a5c1be07c6c8bf106a51f3f3f4989f5 = NULL;

    assertFrameObject( frame_7a5c1be07c6c8bf106a51f3f3f4989f5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_33__g24 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_33__g24 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_34__g25( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ec2392149c4f5985c8e0e8e598cd76cf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ec2392149c4f5985c8e0e8e598cd76cf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ec2392149c4f5985c8e0e8e598cd76cf, codeobj_ec2392149c4f5985c8e0e8e598cd76cf, module_matplotlib$_cm, sizeof(void *) );
    frame_ec2392149c4f5985c8e0e8e598cd76cf = cache_frame_ec2392149c4f5985c8e0e8e598cd76cf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ec2392149c4f5985c8e0e8e598cd76cf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ec2392149c4f5985c8e0e8e598cd76cf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_2;
        tmp_args_element_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_ec2392149c4f5985c8e0e8e598cd76cf->m_frame.f_lineno = 139;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec2392149c4f5985c8e0e8e598cd76cf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec2392149c4f5985c8e0e8e598cd76cf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec2392149c4f5985c8e0e8e598cd76cf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ec2392149c4f5985c8e0e8e598cd76cf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ec2392149c4f5985c8e0e8e598cd76cf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ec2392149c4f5985c8e0e8e598cd76cf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ec2392149c4f5985c8e0e8e598cd76cf,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_ec2392149c4f5985c8e0e8e598cd76cf == cache_frame_ec2392149c4f5985c8e0e8e598cd76cf )
    {
        Py_DECREF( frame_ec2392149c4f5985c8e0e8e598cd76cf );
    }
    cache_frame_ec2392149c4f5985c8e0e8e598cd76cf = NULL;

    assertFrameObject( frame_ec2392149c4f5985c8e0e8e598cd76cf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_34__g25 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_34__g25 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_35__g26( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d65caf7c0a8973481ee4e27a9b0f6129;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d65caf7c0a8973481ee4e27a9b0f6129 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d65caf7c0a8973481ee4e27a9b0f6129, codeobj_d65caf7c0a8973481ee4e27a9b0f6129, module_matplotlib$_cm, sizeof(void *) );
    frame_d65caf7c0a8973481ee4e27a9b0f6129 = cache_frame_d65caf7c0a8973481ee4e27a9b0f6129;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d65caf7c0a8973481ee4e27a9b0f6129 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d65caf7c0a8973481ee4e27a9b0f6129 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        tmp_left_name_3 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_2 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_3, tmp_right_name_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_left_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_2;
        tmp_return_value = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d65caf7c0a8973481ee4e27a9b0f6129 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d65caf7c0a8973481ee4e27a9b0f6129 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d65caf7c0a8973481ee4e27a9b0f6129 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d65caf7c0a8973481ee4e27a9b0f6129, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d65caf7c0a8973481ee4e27a9b0f6129->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d65caf7c0a8973481ee4e27a9b0f6129, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d65caf7c0a8973481ee4e27a9b0f6129,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_d65caf7c0a8973481ee4e27a9b0f6129 == cache_frame_d65caf7c0a8973481ee4e27a9b0f6129 )
    {
        Py_DECREF( frame_d65caf7c0a8973481ee4e27a9b0f6129 );
    }
    cache_frame_d65caf7c0a8973481ee4e27a9b0f6129 = NULL;

    assertFrameObject( frame_d65caf7c0a8973481ee4e27a9b0f6129 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_35__g26 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_35__g26 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_36__g27( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_510a5e3fafa7208dd155230ea37e1486;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_510a5e3fafa7208dd155230ea37e1486 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_510a5e3fafa7208dd155230ea37e1486, codeobj_510a5e3fafa7208dd155230ea37e1486, module_matplotlib$_cm, sizeof(void *) );
    frame_510a5e3fafa7208dd155230ea37e1486 = cache_frame_510a5e3fafa7208dd155230ea37e1486;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_510a5e3fafa7208dd155230ea37e1486 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_510a5e3fafa7208dd155230ea37e1486 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        tmp_left_name_3 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_2 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_3, tmp_right_name_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_2;
        tmp_left_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_2;
        tmp_return_value = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_510a5e3fafa7208dd155230ea37e1486 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_510a5e3fafa7208dd155230ea37e1486 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_510a5e3fafa7208dd155230ea37e1486 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_510a5e3fafa7208dd155230ea37e1486, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_510a5e3fafa7208dd155230ea37e1486->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_510a5e3fafa7208dd155230ea37e1486, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_510a5e3fafa7208dd155230ea37e1486,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_510a5e3fafa7208dd155230ea37e1486 == cache_frame_510a5e3fafa7208dd155230ea37e1486 )
    {
        Py_DECREF( frame_510a5e3fafa7208dd155230ea37e1486 );
    }
    cache_frame_510a5e3fafa7208dd155230ea37e1486 = NULL;

    assertFrameObject( frame_510a5e3fafa7208dd155230ea37e1486 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_36__g27 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_36__g27 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_37__g28( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ed6b474b87a1b9e43d1ffcd2f2821ef9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ed6b474b87a1b9e43d1ffcd2f2821ef9, codeobj_ed6b474b87a1b9e43d1ffcd2f2821ef9, module_matplotlib$_cm, sizeof(void *) );
    frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 = cache_frame_ed6b474b87a1b9e43d1ffcd2f2821ef9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_3 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_2 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_3, tmp_right_name_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_left_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_2;
        tmp_args_element_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_ed6b474b87a1b9e43d1ffcd2f2821ef9->m_frame.f_lineno = 142;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ed6b474b87a1b9e43d1ffcd2f2821ef9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ed6b474b87a1b9e43d1ffcd2f2821ef9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ed6b474b87a1b9e43d1ffcd2f2821ef9,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 == cache_frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 )
    {
        Py_DECREF( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 );
    }
    cache_frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 = NULL;

    assertFrameObject( frame_ed6b474b87a1b9e43d1ffcd2f2821ef9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_37__g28 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_37__g28 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_38__g29( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b86579b6ac33f39ef296b2058171f200;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b86579b6ac33f39ef296b2058171f200 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b86579b6ac33f39ef296b2058171f200, codeobj_b86579b6ac33f39ef296b2058171f200, module_matplotlib$_cm, sizeof(void *) );
    frame_b86579b6ac33f39ef296b2058171f200 = cache_frame_b86579b6ac33f39ef296b2058171f200;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b86579b6ac33f39ef296b2058171f200 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b86579b6ac33f39ef296b2058171f200 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_3 = const_int_pos_3;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_2 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_3, tmp_right_name_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_2;
        tmp_left_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_2;
        tmp_args_element_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_b86579b6ac33f39ef296b2058171f200->m_frame.f_lineno = 143;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b86579b6ac33f39ef296b2058171f200 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b86579b6ac33f39ef296b2058171f200 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b86579b6ac33f39ef296b2058171f200 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b86579b6ac33f39ef296b2058171f200, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b86579b6ac33f39ef296b2058171f200->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b86579b6ac33f39ef296b2058171f200, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b86579b6ac33f39ef296b2058171f200,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_b86579b6ac33f39ef296b2058171f200 == cache_frame_b86579b6ac33f39ef296b2058171f200 )
    {
        Py_DECREF( frame_b86579b6ac33f39ef296b2058171f200 );
    }
    cache_frame_b86579b6ac33f39ef296b2058171f200 = NULL;

    assertFrameObject( frame_b86579b6ac33f39ef296b2058171f200 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_38__g29 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_38__g29 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_39__g30( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_566444d1d4c291dccae414e188dab726;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_566444d1d4c291dccae414e188dab726 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_566444d1d4c291dccae414e188dab726, codeobj_566444d1d4c291dccae414e188dab726, module_matplotlib$_cm, sizeof(void *) );
    frame_566444d1d4c291dccae414e188dab726 = cache_frame_566444d1d4c291dccae414e188dab726;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_566444d1d4c291dccae414e188dab726 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_566444d1d4c291dccae414e188dab726 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_float_0_32;
        tmp_left_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_FLOAT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_float_0_78125;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_566444d1d4c291dccae414e188dab726 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_566444d1d4c291dccae414e188dab726 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_566444d1d4c291dccae414e188dab726 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_566444d1d4c291dccae414e188dab726, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_566444d1d4c291dccae414e188dab726->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_566444d1d4c291dccae414e188dab726, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_566444d1d4c291dccae414e188dab726,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_566444d1d4c291dccae414e188dab726 == cache_frame_566444d1d4c291dccae414e188dab726 )
    {
        Py_DECREF( frame_566444d1d4c291dccae414e188dab726 );
    }
    cache_frame_566444d1d4c291dccae414e188dab726 = NULL;

    assertFrameObject( frame_566444d1d4c291dccae414e188dab726 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_39__g30 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_39__g30 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_40__g31( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_45a7fe08d52f929db27e5920e6e2f85b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_45a7fe08d52f929db27e5920e6e2f85b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_45a7fe08d52f929db27e5920e6e2f85b, codeobj_45a7fe08d52f929db27e5920e6e2f85b, module_matplotlib$_cm, sizeof(void *) );
    frame_45a7fe08d52f929db27e5920e6e2f85b = cache_frame_45a7fe08d52f929db27e5920e6e2f85b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_45a7fe08d52f929db27e5920e6e2f85b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_45a7fe08d52f929db27e5920e6e2f85b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_int_pos_2;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_float_0_84;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_45a7fe08d52f929db27e5920e6e2f85b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_45a7fe08d52f929db27e5920e6e2f85b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_45a7fe08d52f929db27e5920e6e2f85b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_45a7fe08d52f929db27e5920e6e2f85b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_45a7fe08d52f929db27e5920e6e2f85b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_45a7fe08d52f929db27e5920e6e2f85b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_45a7fe08d52f929db27e5920e6e2f85b,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_45a7fe08d52f929db27e5920e6e2f85b == cache_frame_45a7fe08d52f929db27e5920e6e2f85b )
    {
        Py_DECREF( frame_45a7fe08d52f929db27e5920e6e2f85b );
    }
    cache_frame_45a7fe08d52f929db27e5920e6e2f85b = NULL;

    assertFrameObject( frame_45a7fe08d52f929db27e5920e6e2f85b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_40__g31 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_40__g31 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_41__g32( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *var_ret = NULL;
    PyObject *var_m = NULL;
    struct Nuitka_FrameObject *frame_93383a9ac06d4ec237b25dd6b51aa254;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_93383a9ac06d4ec237b25dd6b51aa254 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_93383a9ac06d4ec237b25dd6b51aa254, codeobj_93383a9ac06d4ec237b25dd6b51aa254, module_matplotlib$_cm, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_93383a9ac06d4ec237b25dd6b51aa254 = cache_frame_93383a9ac06d4ec237b25dd6b51aa254;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_93383a9ac06d4ec237b25dd6b51aa254 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_93383a9ac06d4ec237b25dd6b51aa254 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_len_arg_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_zeros );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_len_arg_1 = par_x;
        tmp_args_element_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 147;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_93383a9ac06d4ec237b25dd6b51aa254->m_frame.f_lineno = 147;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_ret == NULL );
        var_ret = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_x );
        tmp_compexpr_left_1 = par_x;
        tmp_compexpr_right_1 = const_float_0_25;
        tmp_assign_source_2 = RICH_COMPARE_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_m == NULL );
        var_m = tmp_assign_source_2;
    }
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_ass_subscript_1;
        tmp_left_name_1 = const_int_pos_4;
        CHECK_OBJECT( par_x );
        tmp_subscribed_name_1 = par_x;
        CHECK_OBJECT( var_m );
        tmp_subscript_name_1 = var_m;
        tmp_right_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_ass_subvalue_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_ass_subvalue_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_ret );
        tmp_ass_subscribed_1 = var_ret;
        CHECK_OBJECT( var_m );
        tmp_ass_subscript_1 = var_m;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( par_x );
        tmp_compexpr_left_2 = par_x;
        tmp_compexpr_right_2 = const_float_0_25;
        tmp_left_name_2 = RICH_COMPARE_GTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_x );
        tmp_compexpr_left_3 = par_x;
        tmp_compexpr_right_3 = const_float_0_92;
        tmp_right_name_2 = RICH_COMPARE_LT_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 150;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_m;
            assert( old != NULL );
            var_m = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_ass_subvalue_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_right_name_4;
        PyObject *tmp_ass_subscribed_2;
        PyObject *tmp_ass_subscript_2;
        tmp_left_name_4 = const_int_neg_2;
        CHECK_OBJECT( par_x );
        tmp_subscribed_name_2 = par_x;
        CHECK_OBJECT( var_m );
        tmp_subscript_name_2 = var_m;
        tmp_right_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_3 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_4, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_4 = const_float_1_84;
        tmp_ass_subvalue_2 = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_3, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_3 );
        if ( tmp_ass_subvalue_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_ret );
        tmp_ass_subscribed_2 = var_ret;
        CHECK_OBJECT( var_m );
        tmp_ass_subscript_2 = var_m;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
        Py_DECREF( tmp_ass_subvalue_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        CHECK_OBJECT( par_x );
        tmp_compexpr_left_4 = par_x;
        tmp_compexpr_right_4 = const_float_0_92;
        tmp_assign_source_4 = RICH_COMPARE_GTE_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_m;
            assert( old != NULL );
            var_m = tmp_assign_source_4;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_ass_subvalue_3;
        PyObject *tmp_left_name_5;
        PyObject *tmp_left_name_6;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_right_name_5;
        PyObject *tmp_right_name_6;
        PyObject *tmp_ass_subscribed_3;
        PyObject *tmp_ass_subscript_3;
        CHECK_OBJECT( par_x );
        tmp_subscribed_name_3 = par_x;
        CHECK_OBJECT( var_m );
        tmp_subscript_name_3 = var_m;
        tmp_left_name_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_float_0_08;
        tmp_left_name_5 = BINARY_OPERATION_TRUEDIV_OBJECT_FLOAT( tmp_left_name_6, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_6 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_6 = const_float_11_5;
        tmp_ass_subvalue_3 = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_5, tmp_right_name_6 );
        Py_DECREF( tmp_left_name_5 );
        if ( tmp_ass_subvalue_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_ret );
        tmp_ass_subscribed_3 = var_ret;
        CHECK_OBJECT( var_m );
        tmp_ass_subscript_3 = var_m;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
        Py_DECREF( tmp_ass_subvalue_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_93383a9ac06d4ec237b25dd6b51aa254 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_93383a9ac06d4ec237b25dd6b51aa254 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_93383a9ac06d4ec237b25dd6b51aa254, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_93383a9ac06d4ec237b25dd6b51aa254->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_93383a9ac06d4ec237b25dd6b51aa254, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_93383a9ac06d4ec237b25dd6b51aa254,
        type_description_1,
        par_x,
        var_ret,
        var_m
    );


    // Release cached frame.
    if ( frame_93383a9ac06d4ec237b25dd6b51aa254 == cache_frame_93383a9ac06d4ec237b25dd6b51aa254 )
    {
        Py_DECREF( frame_93383a9ac06d4ec237b25dd6b51aa254 );
    }
    cache_frame_93383a9ac06d4ec237b25dd6b51aa254 = NULL;

    assertFrameObject( frame_93383a9ac06d4ec237b25dd6b51aa254 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_ret );
    tmp_return_value = var_ret;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_41__g32 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)var_ret );
    Py_DECREF( var_ret );
    var_ret = NULL;

    CHECK_OBJECT( (PyObject *)var_m );
    Py_DECREF( var_m );
    var_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_41__g32 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_42__g33( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6d5891a73a626ffe1449c0a32bde90be;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6d5891a73a626ffe1449c0a32bde90be = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6d5891a73a626ffe1449c0a32bde90be, codeobj_6d5891a73a626ffe1449c0a32bde90be, module_matplotlib$_cm, sizeof(void *) );
    frame_6d5891a73a626ffe1449c0a32bde90be = cache_frame_6d5891a73a626ffe1449c0a32bde90be;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6d5891a73a626ffe1449c0a32bde90be );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6d5891a73a626ffe1449c0a32bde90be ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_abs );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = const_int_pos_2;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 155;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_float_0_5;
        tmp_args_element_name_1 = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 155;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_6d5891a73a626ffe1449c0a32bde90be->m_frame.f_lineno = 155;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6d5891a73a626ffe1449c0a32bde90be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6d5891a73a626ffe1449c0a32bde90be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6d5891a73a626ffe1449c0a32bde90be );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6d5891a73a626ffe1449c0a32bde90be, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6d5891a73a626ffe1449c0a32bde90be->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6d5891a73a626ffe1449c0a32bde90be, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6d5891a73a626ffe1449c0a32bde90be,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_6d5891a73a626ffe1449c0a32bde90be == cache_frame_6d5891a73a626ffe1449c0a32bde90be )
    {
        Py_DECREF( frame_6d5891a73a626ffe1449c0a32bde90be );
    }
    cache_frame_6d5891a73a626ffe1449c0a32bde90be = NULL;

    assertFrameObject( frame_6d5891a73a626ffe1449c0a32bde90be );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_42__g33 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_42__g33 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_43__g34( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ca4ef550c0f985f1de07b531fab6582c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ca4ef550c0f985f1de07b531fab6582c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ca4ef550c0f985f1de07b531fab6582c, codeobj_ca4ef550c0f985f1de07b531fab6582c, module_matplotlib$_cm, sizeof(void *) );
    frame_ca4ef550c0f985f1de07b531fab6582c = cache_frame_ca4ef550c0f985f1de07b531fab6582c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ca4ef550c0f985f1de07b531fab6582c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ca4ef550c0f985f1de07b531fab6582c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_int_pos_2;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_return_value = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ca4ef550c0f985f1de07b531fab6582c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ca4ef550c0f985f1de07b531fab6582c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ca4ef550c0f985f1de07b531fab6582c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ca4ef550c0f985f1de07b531fab6582c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ca4ef550c0f985f1de07b531fab6582c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ca4ef550c0f985f1de07b531fab6582c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ca4ef550c0f985f1de07b531fab6582c,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_ca4ef550c0f985f1de07b531fab6582c == cache_frame_ca4ef550c0f985f1de07b531fab6582c )
    {
        Py_DECREF( frame_ca4ef550c0f985f1de07b531fab6582c );
    }
    cache_frame_ca4ef550c0f985f1de07b531fab6582c = NULL;

    assertFrameObject( frame_ca4ef550c0f985f1de07b531fab6582c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_43__g34 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_43__g34 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_44__g35( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_147ee6952ad69b667578f6330066de5c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_147ee6952ad69b667578f6330066de5c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_147ee6952ad69b667578f6330066de5c, codeobj_147ee6952ad69b667578f6330066de5c, module_matplotlib$_cm, sizeof(void *) );
    frame_147ee6952ad69b667578f6330066de5c = cache_frame_147ee6952ad69b667578f6330066de5c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_147ee6952ad69b667578f6330066de5c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_147ee6952ad69b667578f6330066de5c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_int_pos_2;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_float_0_5;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_147ee6952ad69b667578f6330066de5c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_147ee6952ad69b667578f6330066de5c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_147ee6952ad69b667578f6330066de5c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_147ee6952ad69b667578f6330066de5c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_147ee6952ad69b667578f6330066de5c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_147ee6952ad69b667578f6330066de5c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_147ee6952ad69b667578f6330066de5c,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_147ee6952ad69b667578f6330066de5c == cache_frame_147ee6952ad69b667578f6330066de5c )
    {
        Py_DECREF( frame_147ee6952ad69b667578f6330066de5c );
    }
    cache_frame_147ee6952ad69b667578f6330066de5c = NULL;

    assertFrameObject( frame_147ee6952ad69b667578f6330066de5c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_44__g35 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_44__g35 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_45__g36( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0f545769d1a2556f56803e324fae7085;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0f545769d1a2556f56803e324fae7085 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0f545769d1a2556f56803e324fae7085, codeobj_0f545769d1a2556f56803e324fae7085, module_matplotlib$_cm, sizeof(void *) );
    frame_0f545769d1a2556f56803e324fae7085 = cache_frame_0f545769d1a2556f56803e324fae7085;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0f545769d1a2556f56803e324fae7085 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0f545769d1a2556f56803e324fae7085 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_int_pos_2;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f545769d1a2556f56803e324fae7085 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f545769d1a2556f56803e324fae7085 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f545769d1a2556f56803e324fae7085 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0f545769d1a2556f56803e324fae7085, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0f545769d1a2556f56803e324fae7085->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0f545769d1a2556f56803e324fae7085, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0f545769d1a2556f56803e324fae7085,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_0f545769d1a2556f56803e324fae7085 == cache_frame_0f545769d1a2556f56803e324fae7085 )
    {
        Py_DECREF( frame_0f545769d1a2556f56803e324fae7085 );
    }
    cache_frame_0f545769d1a2556f56803e324fae7085 = NULL;

    assertFrameObject( frame_0f545769d1a2556f56803e324fae7085 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_45__g36 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_45__g36 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_46__gist_heat_red( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ca6a378878e823427eba3f7dfa3f4a5f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ca6a378878e823427eba3f7dfa3f4a5f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ca6a378878e823427eba3f7dfa3f4a5f, codeobj_ca6a378878e823427eba3f7dfa3f4a5f, module_matplotlib$_cm, sizeof(void *) );
    frame_ca6a378878e823427eba3f7dfa3f4a5f = cache_frame_ca6a378878e823427eba3f7dfa3f4a5f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ca6a378878e823427eba3f7dfa3f4a5f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ca6a378878e823427eba3f7dfa3f4a5f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_float_1_5;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_return_value = BINARY_OPERATION_MUL_FLOAT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1007;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ca6a378878e823427eba3f7dfa3f4a5f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ca6a378878e823427eba3f7dfa3f4a5f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ca6a378878e823427eba3f7dfa3f4a5f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ca6a378878e823427eba3f7dfa3f4a5f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ca6a378878e823427eba3f7dfa3f4a5f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ca6a378878e823427eba3f7dfa3f4a5f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ca6a378878e823427eba3f7dfa3f4a5f,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_ca6a378878e823427eba3f7dfa3f4a5f == cache_frame_ca6a378878e823427eba3f7dfa3f4a5f )
    {
        Py_DECREF( frame_ca6a378878e823427eba3f7dfa3f4a5f );
    }
    cache_frame_ca6a378878e823427eba3f7dfa3f4a5f = NULL;

    assertFrameObject( frame_ca6a378878e823427eba3f7dfa3f4a5f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_46__gist_heat_red );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_46__gist_heat_red );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_47__gist_heat_green( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ff54ad51aa14d088b02f2d24e41ac145;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ff54ad51aa14d088b02f2d24e41ac145 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ff54ad51aa14d088b02f2d24e41ac145, codeobj_ff54ad51aa14d088b02f2d24e41ac145, module_matplotlib$_cm, sizeof(void *) );
    frame_ff54ad51aa14d088b02f2d24e41ac145 = cache_frame_ff54ad51aa14d088b02f2d24e41ac145;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ff54ad51aa14d088b02f2d24e41ac145 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ff54ad51aa14d088b02f2d24e41ac145 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_int_pos_2;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1008;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1008;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff54ad51aa14d088b02f2d24e41ac145 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff54ad51aa14d088b02f2d24e41ac145 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff54ad51aa14d088b02f2d24e41ac145 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ff54ad51aa14d088b02f2d24e41ac145, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ff54ad51aa14d088b02f2d24e41ac145->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ff54ad51aa14d088b02f2d24e41ac145, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ff54ad51aa14d088b02f2d24e41ac145,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_ff54ad51aa14d088b02f2d24e41ac145 == cache_frame_ff54ad51aa14d088b02f2d24e41ac145 )
    {
        Py_DECREF( frame_ff54ad51aa14d088b02f2d24e41ac145 );
    }
    cache_frame_ff54ad51aa14d088b02f2d24e41ac145 = NULL;

    assertFrameObject( frame_ff54ad51aa14d088b02f2d24e41ac145 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_47__gist_heat_green );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_47__gist_heat_green );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_48__gist_heat_blue( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_298269a309931b4e28d196e53b52b9d7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_298269a309931b4e28d196e53b52b9d7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_298269a309931b4e28d196e53b52b9d7, codeobj_298269a309931b4e28d196e53b52b9d7, module_matplotlib$_cm, sizeof(void *) );
    frame_298269a309931b4e28d196e53b52b9d7 = cache_frame_298269a309931b4e28d196e53b52b9d7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_298269a309931b4e28d196e53b52b9d7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_298269a309931b4e28d196e53b52b9d7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_int_pos_4;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_left_name_1 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1009;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_3;
        tmp_return_value = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1009;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_298269a309931b4e28d196e53b52b9d7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_298269a309931b4e28d196e53b52b9d7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_298269a309931b4e28d196e53b52b9d7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_298269a309931b4e28d196e53b52b9d7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_298269a309931b4e28d196e53b52b9d7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_298269a309931b4e28d196e53b52b9d7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_298269a309931b4e28d196e53b52b9d7,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_298269a309931b4e28d196e53b52b9d7 == cache_frame_298269a309931b4e28d196e53b52b9d7 )
    {
        Py_DECREF( frame_298269a309931b4e28d196e53b52b9d7 );
    }
    cache_frame_298269a309931b4e28d196e53b52b9d7 = NULL;

    assertFrameObject( frame_298269a309931b4e28d196e53b52b9d7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_48__gist_heat_blue );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_48__gist_heat_blue );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$_cm$$$function_49__gist_yarg( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7ab5ebee71076c5d9b0e3985e77a7554;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7ab5ebee71076c5d9b0e3985e77a7554 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7ab5ebee71076c5d9b0e3985e77a7554, codeobj_7ab5ebee71076c5d9b0e3985e77a7554, module_matplotlib$_cm, sizeof(void *) );
    frame_7ab5ebee71076c5d9b0e3985e77a7554 = cache_frame_7ab5ebee71076c5d9b0e3985e77a7554;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7ab5ebee71076c5d9b0e3985e77a7554 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7ab5ebee71076c5d9b0e3985e77a7554 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_int_pos_1;
        CHECK_OBJECT( par_x );
        tmp_right_name_1 = par_x;
        tmp_return_value = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1088;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7ab5ebee71076c5d9b0e3985e77a7554 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7ab5ebee71076c5d9b0e3985e77a7554 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7ab5ebee71076c5d9b0e3985e77a7554 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7ab5ebee71076c5d9b0e3985e77a7554, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7ab5ebee71076c5d9b0e3985e77a7554->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7ab5ebee71076c5d9b0e3985e77a7554, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7ab5ebee71076c5d9b0e3985e77a7554,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_7ab5ebee71076c5d9b0e3985e77a7554 == cache_frame_7ab5ebee71076c5d9b0e3985e77a7554 )
    {
        Py_DECREF( frame_7ab5ebee71076c5d9b0e3985e77a7554 );
    }
    cache_frame_7ab5ebee71076c5d9b0e3985e77a7554 = NULL;

    assertFrameObject( frame_7ab5ebee71076c5d9b0e3985e77a7554 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_49__gist_yarg );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$_cm$$$function_49__gist_yarg );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_10__g1(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_10__g1,
        const_str_plain__g1,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9b3cd153a3f8975c8cbb5bdac89db94e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_11__g2(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_11__g2,
        const_str_plain__g2,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_da1e2d0577c3f33d39ce13d722082e7c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_12__g3(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_12__g3,
        const_str_plain__g3,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9a00559ab1fce59fdfb7a48ac0fbd836,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_13__g4(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_13__g4,
        const_str_plain__g4,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5cf23711c93867864876412ea15953a6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_14__g5(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_14__g5,
        const_str_plain__g5,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e6223b72b1e3a56299cfc38b154bc21e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_15__g6(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_15__g6,
        const_str_plain__g6,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_857da5f6463870875e79355e4ffcc4e0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_16__g7(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_16__g7,
        const_str_plain__g7,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f91be58d49ac067f82c34fb0c522641c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_17__g8(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_17__g8,
        const_str_plain__g8,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_572b6279086edfe79cc5d4b59b9d3a78,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_18__g9(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_18__g9,
        const_str_plain__g9,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0665c21023d00b9af46ab168ecffda2d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_19__g10(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_19__g10,
        const_str_plain__g10,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6f001a3c707cfa288f8adae11f30a1d9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_1__flag_red(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_1__flag_red,
        const_str_plain__flag_red,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9da89fbbcfc0335d1bbafb14a8117620,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_20__g11(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_20__g11,
        const_str_plain__g11,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a43ef645600be41fb6834b58933b4683,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_21__g12(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_21__g12,
        const_str_plain__g12,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3d07f3dd6ca66b2aecfeae9495e79601,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_22__g13(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_22__g13,
        const_str_plain__g13,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8b0e4f93545329d1d94e42993abf7216,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_23__g14(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_23__g14,
        const_str_plain__g14,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c09d6f101ce2957484c6ba3b60f2f5cf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_24__g15(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_24__g15,
        const_str_plain__g15,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8cff908993bcdf98dabe0c3736dcf388,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_25__g16(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_25__g16,
        const_str_plain__g16,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3318b94fe7114ea97fc057bb2890d2d6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_26__g17(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_26__g17,
        const_str_plain__g17,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ed3244b65d5d34826d10d89ba234077,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_27__g18(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_27__g18,
        const_str_plain__g18,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_acf507c7209ba5bcc78d816026bbdd7b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_28__g19(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_28__g19,
        const_str_plain__g19,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_642906f415e89b8021acbbc2acfc4bb5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_29__g20(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_29__g20,
        const_str_plain__g20,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b185d50514175a783314284b7e1c8664,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_2__flag_green(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_2__flag_green,
        const_str_plain__flag_green,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bc3e536fbfa165ee58d9a0d82198ce1d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_30__g21(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_30__g21,
        const_str_plain__g21,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b04b846f655acf9210cbb390b3a144ec,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_31__g22(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_31__g22,
        const_str_plain__g22,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bb72c4ebde9e34e2e57d29944502cdb1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_32__g23(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_32__g23,
        const_str_plain__g23,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_74adb45065271f93643eb2787503f6ff,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_33__g24(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_33__g24,
        const_str_plain__g24,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7a5c1be07c6c8bf106a51f3f3f4989f5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_34__g25(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_34__g25,
        const_str_plain__g25,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ec2392149c4f5985c8e0e8e598cd76cf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_35__g26(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_35__g26,
        const_str_plain__g26,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d65caf7c0a8973481ee4e27a9b0f6129,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_36__g27(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_36__g27,
        const_str_plain__g27,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_510a5e3fafa7208dd155230ea37e1486,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_37__g28(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_37__g28,
        const_str_plain__g28,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ed6b474b87a1b9e43d1ffcd2f2821ef9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_38__g29(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_38__g29,
        const_str_plain__g29,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b86579b6ac33f39ef296b2058171f200,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_39__g30(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_39__g30,
        const_str_plain__g30,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_566444d1d4c291dccae414e188dab726,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_3__flag_blue(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_3__flag_blue,
        const_str_plain__flag_blue,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_96dc18310950969d5f48576ce081a466,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_40__g31(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_40__g31,
        const_str_plain__g31,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_45a7fe08d52f929db27e5920e6e2f85b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_41__g32(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_41__g32,
        const_str_plain__g32,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_93383a9ac06d4ec237b25dd6b51aa254,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_42__g33(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_42__g33,
        const_str_plain__g33,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6d5891a73a626ffe1449c0a32bde90be,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_43__g34(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_43__g34,
        const_str_plain__g34,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ca4ef550c0f985f1de07b531fab6582c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_44__g35(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_44__g35,
        const_str_plain__g35,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_147ee6952ad69b667578f6330066de5c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_45__g36(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_45__g36,
        const_str_plain__g36,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0f545769d1a2556f56803e324fae7085,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_46__gist_heat_red(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_46__gist_heat_red,
        const_str_plain__gist_heat_red,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ca6a378878e823427eba3f7dfa3f4a5f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_47__gist_heat_green(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_47__gist_heat_green,
        const_str_plain__gist_heat_green,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ff54ad51aa14d088b02f2d24e41ac145,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_48__gist_heat_blue(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_48__gist_heat_blue,
        const_str_plain__gist_heat_blue,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_298269a309931b4e28d196e53b52b9d7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_49__gist_yarg(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_49__gist_yarg,
        const_str_plain__gist_yarg,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7ab5ebee71076c5d9b0e3985e77a7554,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_4__prism_red(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_4__prism_red,
        const_str_plain__prism_red,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f1d46b9cdedca619e4f3c486cb441557,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_5__prism_green(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_5__prism_green,
        const_str_plain__prism_green,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a5d59bf3a308aedf73adad4d6f80043d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_6__prism_blue(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_6__prism_blue,
        const_str_plain__prism_blue,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_71c9beb9d5e02f5ecc46acc3901775d6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_7__ch_helper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_7__ch_helper,
        const_str_plain__ch_helper,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_80e296267b0748a2b363ed54c4de79e8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        const_str_digest_0870d49f1cf88348cb56a7f4adc73408,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_8_cubehelix( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_8_cubehelix,
        const_str_plain_cubehelix,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_32d77dfbf1973b6159d4f408ea9d29f7,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        const_str_digest_68bcfeed91a2fbe6bb71f773db17210e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$_cm$$$function_9__g0(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$_cm$$$function_9__g0,
        const_str_plain__g0,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9cbe26b9a96f59538bb1963f17e7d388,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$_cm,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$_cm =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib._cm",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$_cm)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$_cm)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$_cm );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib._cm: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib._cm: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib._cm: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$_cm" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$_cm = Py_InitModule4(
        "matplotlib._cm",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$_cm = PyModule_Create( &mdef_matplotlib$_cm );
#endif

    moduledict_matplotlib$_cm = MODULE_DICT( module_matplotlib$_cm );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$_cm,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$_cm,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$_cm,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$_cm,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$_cm );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_70f25990f64eb086931fae485976330d, module_matplotlib$_cm );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var_i = NULL;
    PyObject *tmp_dictcontraction_1__$0 = NULL;
    PyObject *tmp_dictcontraction_1__contraction = NULL;
    PyObject *tmp_dictcontraction_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_dc635218d6bfe5f1da89e2d1d276c5ff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_eced98829c6d9c04ad2c5fcef3e36c2c_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_0ec62d994d1e365c7efad4dabaaba134;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_dc635218d6bfe5f1da89e2d1d276c5ff = MAKE_MODULE_FRAME( codeobj_dc635218d6bfe5f1da89e2d1d276c5ff, module_matplotlib$_cm );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_dc635218d6bfe5f1da89e2d1d276c5ff );
    assert( Py_REFCNT( frame_dc635218d6bfe5f1da89e2d1d276c5ff ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_functools;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$_cm;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_partial_tuple;
        tmp_level_name_1 = const_int_0;
        frame_dc635218d6bfe5f1da89e2d1d276c5ff->m_frame.f_lineno = 9;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_partial );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_partial, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_numpy;
        tmp_globals_name_2 = (PyObject *)moduledict_matplotlib$_cm;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_dc635218d6bfe5f1da89e2d1d276c5ff->m_frame.f_lineno = 11;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_np, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = PyDict_Copy( const_dict_139fa967a21793ecdbc3e590120916a2 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__binary_data, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = PyDict_Copy( const_dict_61e4a4a8cd2b4181694d4cfb1751640d );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__autumn_data, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = PyDict_Copy( const_dict_e67366b36b736b2b1824e4124103e620 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__bone_data, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = PyDict_Copy( const_dict_3adc72b697f1d7d162e0d950131b4b5a );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__cool_data, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = PyDict_Copy( const_dict_8f097861f42f3010bd8f6de6022ffc3c );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__copper_data, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_matplotlib$_cm$$$function_1__flag_red(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_red, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_matplotlib$_cm$$$function_2__flag_green(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_green, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_matplotlib$_cm$$$function_3__flag_blue(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_blue, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_mvar_value_5;
        tmp_dict_key_1 = const_str_plain_red;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_red );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__flag_red );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_dict_value_1 = tmp_mvar_value_3;
        tmp_assign_source_14 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_green;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_green );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__flag_green );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_dict_value_2 = tmp_mvar_value_4;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_blue;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_blue );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__flag_blue );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_dict_value_3 = tmp_mvar_value_5;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_data, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_matplotlib$_cm$$$function_4__prism_red(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_red, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_matplotlib$_cm$$$function_5__prism_green(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_green, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_matplotlib$_cm$$$function_6__prism_blue(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_blue, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_8;
        tmp_dict_key_4 = const_str_plain_red;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_red );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__prism_red );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_dict_value_4 = tmp_mvar_value_6;
        tmp_assign_source_18 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_18, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_green;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_green );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__prism_green );
        }

        CHECK_OBJECT( tmp_mvar_value_7 );
        tmp_dict_value_5 = tmp_mvar_value_7;
        tmp_res = PyDict_SetItem( tmp_assign_source_18, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_blue;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_blue );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__prism_blue );
        }

        CHECK_OBJECT( tmp_mvar_value_8 );
        tmp_dict_value_6 = tmp_mvar_value_8;
        tmp_res = PyDict_SetItem( tmp_assign_source_18, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_data, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_matplotlib$_cm$$$function_7__ch_helper(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__ch_helper, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_float_1_0_float_0_5_float_minus_1_5_float_1_0_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_20 = MAKE_FUNCTION_matplotlib$_cm$$$function_8_cubehelix( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_cubehelix, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_cubehelix );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cubehelix );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_called_name_1 = tmp_mvar_value_9;
        frame_dc635218d6bfe5f1da89e2d1d276c5ff->m_frame.f_lineno = 108;
        tmp_assign_source_21 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__cubehelix_data, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = const_tuple_2bea485d544db289d067c788ebf78b62_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__bwr_data, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = const_tuple_a22c772c730dca9240495aa06353c5e0_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__brg_data, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_matplotlib$_cm$$$function_9__g0(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g0, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_matplotlib$_cm$$$function_10__g1(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g1, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_matplotlib$_cm$$$function_11__g2(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g2, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_matplotlib$_cm$$$function_12__g3(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g3, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_matplotlib$_cm$$$function_13__g4(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g4, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = MAKE_FUNCTION_matplotlib$_cm$$$function_14__g5(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g5, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = MAKE_FUNCTION_matplotlib$_cm$$$function_15__g6(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g6, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = MAKE_FUNCTION_matplotlib$_cm$$$function_16__g7(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g7, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = MAKE_FUNCTION_matplotlib$_cm$$$function_17__g8(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g8, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = MAKE_FUNCTION_matplotlib$_cm$$$function_18__g9(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g9, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = MAKE_FUNCTION_matplotlib$_cm$$$function_19__g10(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g10, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        tmp_assign_source_35 = MAKE_FUNCTION_matplotlib$_cm$$$function_20__g11(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g11, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = MAKE_FUNCTION_matplotlib$_cm$$$function_21__g12(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g12, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        tmp_assign_source_37 = MAKE_FUNCTION_matplotlib$_cm$$$function_22__g13(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g13, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        tmp_assign_source_38 = MAKE_FUNCTION_matplotlib$_cm$$$function_23__g14(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g14, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        tmp_assign_source_39 = MAKE_FUNCTION_matplotlib$_cm$$$function_24__g15(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g15, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        tmp_assign_source_40 = MAKE_FUNCTION_matplotlib$_cm$$$function_25__g16(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g16, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = MAKE_FUNCTION_matplotlib$_cm$$$function_26__g17(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g17, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = MAKE_FUNCTION_matplotlib$_cm$$$function_27__g18(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g18, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        tmp_assign_source_43 = MAKE_FUNCTION_matplotlib$_cm$$$function_28__g19(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g19, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        tmp_assign_source_44 = MAKE_FUNCTION_matplotlib$_cm$$$function_29__g20(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g20, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        tmp_assign_source_45 = MAKE_FUNCTION_matplotlib$_cm$$$function_30__g21(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g21, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        tmp_assign_source_46 = MAKE_FUNCTION_matplotlib$_cm$$$function_31__g22(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g22, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        tmp_assign_source_47 = MAKE_FUNCTION_matplotlib$_cm$$$function_32__g23(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g23, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        tmp_assign_source_48 = MAKE_FUNCTION_matplotlib$_cm$$$function_33__g24(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g24, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        tmp_assign_source_49 = MAKE_FUNCTION_matplotlib$_cm$$$function_34__g25(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g25, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        tmp_assign_source_50 = MAKE_FUNCTION_matplotlib$_cm$$$function_35__g26(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g26, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        tmp_assign_source_51 = MAKE_FUNCTION_matplotlib$_cm$$$function_36__g27(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g27, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        tmp_assign_source_52 = MAKE_FUNCTION_matplotlib$_cm$$$function_37__g28(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g28, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        tmp_assign_source_53 = MAKE_FUNCTION_matplotlib$_cm$$$function_38__g29(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g29, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        tmp_assign_source_54 = MAKE_FUNCTION_matplotlib$_cm$$$function_39__g30(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g30, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        tmp_assign_source_55 = MAKE_FUNCTION_matplotlib$_cm$$$function_40__g31(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g31, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = MAKE_FUNCTION_matplotlib$_cm$$$function_41__g32(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g32, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        tmp_assign_source_57 = MAKE_FUNCTION_matplotlib$_cm$$$function_42__g33(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g33, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        tmp_assign_source_58 = MAKE_FUNCTION_matplotlib$_cm$$$function_43__g34(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g34, tmp_assign_source_58 );
    }
    {
        PyObject *tmp_assign_source_59;
        tmp_assign_source_59 = MAKE_FUNCTION_matplotlib$_cm$$$function_44__g35(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g35, tmp_assign_source_59 );
    }
    {
        PyObject *tmp_assign_source_60;
        tmp_assign_source_60 = MAKE_FUNCTION_matplotlib$_cm$$$function_45__g36(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__g36, tmp_assign_source_60 );
    }
    {
        PyObject *tmp_assign_source_61;
        {
            PyObject *tmp_assign_source_62;
            PyObject *tmp_iter_arg_1;
            tmp_iter_arg_1 = const_xrange_0_37;
            tmp_assign_source_62 = MAKE_ITERATOR( tmp_iter_arg_1 );
            assert( !(tmp_assign_source_62 == NULL) );
            assert( tmp_dictcontraction_1__$0 == NULL );
            tmp_dictcontraction_1__$0 = tmp_assign_source_62;
        }
        {
            PyObject *tmp_assign_source_63;
            tmp_assign_source_63 = PyDict_New();
            assert( tmp_dictcontraction_1__contraction == NULL );
            tmp_dictcontraction_1__contraction = tmp_assign_source_63;
        }
        // Tried code:
        MAKE_OR_REUSE_FRAME( cache_frame_eced98829c6d9c04ad2c5fcef3e36c2c_2, codeobj_eced98829c6d9c04ad2c5fcef3e36c2c, module_matplotlib$_cm, sizeof(void *) );
        frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 = cache_frame_eced98829c6d9c04ad2c5fcef3e36c2c_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_64;
            CHECK_OBJECT( tmp_dictcontraction_1__$0 );
            tmp_next_source_1 = tmp_dictcontraction_1__$0;
            tmp_assign_source_64 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_64 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 160;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_dictcontraction_1__iter_value_0;
                tmp_dictcontraction_1__iter_value_0 = tmp_assign_source_64;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_65;
            CHECK_OBJECT( tmp_dictcontraction_1__iter_value_0 );
            tmp_assign_source_65 = tmp_dictcontraction_1__iter_value_0;
            {
                PyObject *old = outline_0_var_i;
                outline_0_var_i = tmp_assign_source_65;
                Py_INCREF( outline_0_var_i );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            tmp_subscribed_name_1 = (PyObject *)moduledict_matplotlib$_cm;
            tmp_called_instance_1 = const_str_digest_f73618ed65447e45c026300593ea43e2;
            CHECK_OBJECT( outline_0_var_i );
            tmp_args_element_name_1 = outline_0_var_i;
            frame_eced98829c6d9c04ad2c5fcef3e36c2c_2->m_frame.f_lineno = 160;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_subscript_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_format, call_args );
            }

            if ( tmp_subscript_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_2 = "o";
                goto try_except_handler_2;
            }
            tmp_dictset_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_2 = "o";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( tmp_dictcontraction_1__contraction );
            tmp_dictset_dict = tmp_dictcontraction_1__contraction;
            CHECK_OBJECT( outline_0_var_i );
            tmp_dictset_key = outline_0_var_i;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_2 = "o";
                goto try_except_handler_2;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_2 = "o";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_dictcontraction_1__contraction );
        tmp_assign_source_61 = tmp_dictcontraction_1__contraction;
        Py_INCREF( tmp_assign_source_61 );
        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$_cm );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__$0 );
        Py_DECREF( tmp_dictcontraction_1__$0 );
        tmp_dictcontraction_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__contraction );
        Py_DECREF( tmp_dictcontraction_1__contraction );
        tmp_dictcontraction_1__contraction = NULL;

        Py_XDECREF( tmp_dictcontraction_1__iter_value_0 );
        tmp_dictcontraction_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__$0 );
        Py_DECREF( tmp_dictcontraction_1__$0 );
        tmp_dictcontraction_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__contraction );
        Py_DECREF( tmp_dictcontraction_1__contraction );
        tmp_dictcontraction_1__contraction = NULL;

        Py_XDECREF( tmp_dictcontraction_1__iter_value_0 );
        tmp_dictcontraction_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_eced98829c6d9c04ad2c5fcef3e36c2c_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_eced98829c6d9c04ad2c5fcef3e36c2c_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_eced98829c6d9c04ad2c5fcef3e36c2c_2,
            type_description_2,
            outline_0_var_i
        );


        // Release cached frame.
        if ( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 == cache_frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 )
        {
            Py_DECREF( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 );
        }
        cache_frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 = NULL;

        assertFrameObject( frame_eced98829c6d9c04ad2c5fcef3e36c2c_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_1;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$_cm );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_1:;
        Py_XDECREF( outline_0_var_i );
        outline_0_var_i = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_1:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_i );
        outline_0_var_i = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$_cm );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 160;
        goto frame_exception_exit_1;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc, tmp_assign_source_61 );
    }
    {
        PyObject *tmp_assign_source_66;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_subscript_name_4;
        tmp_dict_key_7 = const_str_plain_red;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        CHECK_OBJECT( tmp_mvar_value_10 );
        tmp_subscribed_name_2 = tmp_mvar_value_10;
        tmp_subscript_name_2 = const_int_pos_7;
        tmp_dict_value_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 7 );
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_66 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_66, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_green;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_assign_source_66 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_3 = tmp_mvar_value_11;
        tmp_subscript_name_3 = const_int_pos_5;
        tmp_dict_value_8 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 5 );
        if ( tmp_dict_value_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_66 );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_66, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_blue;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_assign_source_66 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_4 = tmp_mvar_value_12;
        tmp_subscript_name_4 = const_int_pos_15;
        tmp_dict_value_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 15 );
        if ( tmp_dict_value_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_66 );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_66, tmp_dict_key_9, tmp_dict_value_9 );
        Py_DECREF( tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gnuplot_data, tmp_assign_source_66 );
    }
    {
        PyObject *tmp_assign_source_67;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_subscript_name_7;
        tmp_dict_key_10 = const_str_plain_red;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_5 = tmp_mvar_value_13;
        tmp_subscript_name_5 = const_int_pos_30;
        tmp_dict_value_10 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 30 );
        if ( tmp_dict_value_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_67 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_67, tmp_dict_key_10, tmp_dict_value_10 );
        Py_DECREF( tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_plain_green;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_assign_source_67 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_6 = tmp_mvar_value_14;
        tmp_subscript_name_6 = const_int_pos_31;
        tmp_dict_value_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 31 );
        if ( tmp_dict_value_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_67 );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_67, tmp_dict_key_11, tmp_dict_value_11 );
        Py_DECREF( tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_plain_blue;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_assign_source_67 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_7 = tmp_mvar_value_15;
        tmp_subscript_name_7 = const_int_pos_32;
        tmp_dict_value_12 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 32 );
        if ( tmp_dict_value_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_67 );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_67, tmp_dict_key_12, tmp_dict_value_12 );
        Py_DECREF( tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gnuplot2_data, tmp_assign_source_67 );
    }
    {
        PyObject *tmp_assign_source_68;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_subscribed_name_8;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_subscript_name_8;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_subscribed_name_9;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_subscript_name_9;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_subscript_name_10;
        tmp_dict_key_13 = const_str_plain_red;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_8 = tmp_mvar_value_16;
        tmp_subscript_name_8 = const_int_pos_23;
        tmp_dict_value_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_8, tmp_subscript_name_8, 23 );
        if ( tmp_dict_value_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_68 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_68, tmp_dict_key_13, tmp_dict_value_13 );
        Py_DECREF( tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_plain_green;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_assign_source_68 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_9 = tmp_mvar_value_17;
        tmp_subscript_name_9 = const_int_pos_28;
        tmp_dict_value_14 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_9, tmp_subscript_name_9, 28 );
        if ( tmp_dict_value_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_68 );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_68, tmp_dict_key_14, tmp_dict_value_14 );
        Py_DECREF( tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_plain_blue;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_assign_source_68 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_10 = tmp_mvar_value_18;
        tmp_subscript_name_10 = const_int_pos_3;
        tmp_dict_value_15 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_10, tmp_subscript_name_10, 3 );
        if ( tmp_dict_value_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_68 );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_68, tmp_dict_key_15, tmp_dict_value_15 );
        Py_DECREF( tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__ocean_data, tmp_assign_source_68 );
    }
    {
        PyObject *tmp_assign_source_69;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_subscribed_name_11;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_subscript_name_11;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_subscribed_name_12;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_subscript_name_12;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_subscribed_name_13;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_subscript_name_13;
        tmp_dict_key_16 = const_str_plain_red;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_11 = tmp_mvar_value_19;
        tmp_subscript_name_11 = const_int_pos_34;
        tmp_dict_value_16 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_11, tmp_subscript_name_11, 34 );
        if ( tmp_dict_value_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_69 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_69, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_str_plain_green;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_20 == NULL )
        {
            Py_DECREF( tmp_assign_source_69 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_12 = tmp_mvar_value_20;
        tmp_subscript_name_12 = const_int_pos_35;
        tmp_dict_value_17 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_12, tmp_subscript_name_12, 35 );
        if ( tmp_dict_value_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_69 );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_69, tmp_dict_key_17, tmp_dict_value_17 );
        Py_DECREF( tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_18 = const_str_plain_blue;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_assign_source_69 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_13 = tmp_mvar_value_21;
        tmp_subscript_name_13 = const_int_pos_36;
        tmp_dict_value_18 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_13, tmp_subscript_name_13, 36 );
        if ( tmp_dict_value_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_69 );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_69, tmp_dict_key_18, tmp_dict_value_18 );
        Py_DECREF( tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__afmhot_data, tmp_assign_source_69 );
    }
    {
        PyObject *tmp_assign_source_70;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_subscribed_name_14;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_subscript_name_14;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_subscribed_name_15;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_subscript_name_15;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_subscribed_name_16;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_subscript_name_16;
        tmp_dict_key_19 = const_str_plain_red;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_14 = tmp_mvar_value_22;
        tmp_subscript_name_14 = const_int_pos_33;
        tmp_dict_value_19 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_14, tmp_subscript_name_14, 33 );
        if ( tmp_dict_value_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_70 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_70, tmp_dict_key_19, tmp_dict_value_19 );
        Py_DECREF( tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_20 = const_str_plain_green;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_assign_source_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_15 = tmp_mvar_value_23;
        tmp_subscript_name_15 = const_int_pos_13;
        tmp_dict_value_20 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_15, tmp_subscript_name_15, 13 );
        if ( tmp_dict_value_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_70 );

            exception_lineno = 188;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_70, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_plain_blue;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_assign_source_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_16 = tmp_mvar_value_24;
        tmp_subscript_name_16 = const_int_pos_10;
        tmp_dict_value_21 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_16, tmp_subscript_name_16, 10 );
        if ( tmp_dict_value_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_70 );

            exception_lineno = 189;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_70, tmp_dict_key_21, tmp_dict_value_21 );
        Py_DECREF( tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__rainbow_data, tmp_assign_source_70 );
    }
    {
        PyObject *tmp_assign_source_71;
        tmp_assign_source_71 = const_tuple_77bd0eb31ef97c2e7618000ce303f673_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__seismic_data, tmp_assign_source_71 );
    }
    {
        PyObject *tmp_assign_source_72;
        tmp_assign_source_72 = const_tuple_6e6f4e9a37231f81f1e456047fd8c742_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__terrain_data, tmp_assign_source_72 );
    }
    {
        PyObject *tmp_assign_source_73;
        tmp_assign_source_73 = PyDict_Copy( const_dict_79ecbf763b054a3ad895cae3fb89c58e );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gray_data, tmp_assign_source_73 );
    }
    {
        PyObject *tmp_assign_source_74;
        tmp_assign_source_74 = PyDict_Copy( const_dict_d876a613e78a88b4cea47cbcba7077b2 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__hot_data, tmp_assign_source_74 );
    }
    {
        PyObject *tmp_assign_source_75;
        tmp_assign_source_75 = PyDict_Copy( const_dict_a065b631920800bd13e7ea9419838858 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__hsv_data, tmp_assign_source_75 );
    }
    {
        PyObject *tmp_assign_source_76;
        tmp_assign_source_76 = PyDict_Copy( const_dict_fd4a49377d1d658f7d30b821b6352628 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__jet_data, tmp_assign_source_76 );
    }
    {
        PyObject *tmp_assign_source_77;
        tmp_assign_source_77 = PyDict_Copy( const_dict_1b20bf7e6394a197db2b925f2ae9f1f2 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__pink_data, tmp_assign_source_77 );
    }
    {
        PyObject *tmp_assign_source_78;
        tmp_assign_source_78 = PyDict_Copy( const_dict_d6323100fc0b68b2862861068aa9dfb4 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__spring_data, tmp_assign_source_78 );
    }
    {
        PyObject *tmp_assign_source_79;
        tmp_assign_source_79 = PyDict_Copy( const_dict_a6becc14d04a47becd62ae70b4c4482c );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__summer_data, tmp_assign_source_79 );
    }
    {
        PyObject *tmp_assign_source_80;
        tmp_assign_source_80 = PyDict_Copy( const_dict_61b96e51e18659e8ba92a9cb886c68eb );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__winter_data, tmp_assign_source_80 );
    }
    {
        PyObject *tmp_assign_source_81;
        tmp_assign_source_81 = DEEP_COPY( const_dict_e5f8cd4b8b884f01b9dbdb6c0cf95972 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__nipy_spectral_data, tmp_assign_source_81 );
    }
    {
        PyObject *tmp_assign_source_82;
        tmp_assign_source_82 = const_tuple_7eb55817b097308a4aa82a88a7a374a0_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Blues_data, tmp_assign_source_82 );
    }
    {
        PyObject *tmp_assign_source_83;
        tmp_assign_source_83 = const_tuple_a1c2e0d0fdb72592d26c53db2b423104_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__BrBG_data, tmp_assign_source_83 );
    }
    {
        PyObject *tmp_assign_source_84;
        tmp_assign_source_84 = const_tuple_13493111d1a4f8c8a058ebd7f95c09b1_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__BuGn_data, tmp_assign_source_84 );
    }
    {
        PyObject *tmp_assign_source_85;
        tmp_assign_source_85 = const_tuple_3f501f1fed43a18bc40d5702c0cd69db_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__BuPu_data, tmp_assign_source_85 );
    }
    {
        PyObject *tmp_assign_source_86;
        tmp_assign_source_86 = const_tuple_6eb90b29c730cddab6ab1d936156a49c_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__GnBu_data, tmp_assign_source_86 );
    }
    {
        PyObject *tmp_assign_source_87;
        tmp_assign_source_87 = const_tuple_4d8a3db908fea5ac787eae57a231d107_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Greens_data, tmp_assign_source_87 );
    }
    {
        PyObject *tmp_assign_source_88;
        tmp_assign_source_88 = const_tuple_dbd02ca2a43f79960ecaa20263657c10_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Greys_data, tmp_assign_source_88 );
    }
    {
        PyObject *tmp_assign_source_89;
        tmp_assign_source_89 = const_tuple_9e6a97cefbfc93e4dbd6fe4790754edf_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Oranges_data, tmp_assign_source_89 );
    }
    {
        PyObject *tmp_assign_source_90;
        tmp_assign_source_90 = const_tuple_cc9102d236a92569966c4ebd4c8fbcf5_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__OrRd_data, tmp_assign_source_90 );
    }
    {
        PyObject *tmp_assign_source_91;
        tmp_assign_source_91 = const_tuple_e22ad1a6fd2b90375e84cff21f64303a_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PiYG_data, tmp_assign_source_91 );
    }
    {
        PyObject *tmp_assign_source_92;
        tmp_assign_source_92 = const_tuple_2a4c4f5b12a7944706ab0b855d978f15_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PRGn_data, tmp_assign_source_92 );
    }
    {
        PyObject *tmp_assign_source_93;
        tmp_assign_source_93 = const_tuple_4f66db73ea9fdbfdceb9e620df1e4919_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuBu_data, tmp_assign_source_93 );
    }
    {
        PyObject *tmp_assign_source_94;
        tmp_assign_source_94 = const_tuple_122f654ebd563953484696204c943f8b_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuBuGn_data, tmp_assign_source_94 );
    }
    {
        PyObject *tmp_assign_source_95;
        tmp_assign_source_95 = const_tuple_fb0dd03a0e957d10a507ea76b9c3ad27_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuOr_data, tmp_assign_source_95 );
    }
    {
        PyObject *tmp_assign_source_96;
        tmp_assign_source_96 = const_tuple_16581877f4601a2ddb7154cb6281285a_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuRd_data, tmp_assign_source_96 );
    }
    {
        PyObject *tmp_assign_source_97;
        tmp_assign_source_97 = const_tuple_b19a69e04a28b726d8b7d59807ef8eef_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Purples_data, tmp_assign_source_97 );
    }
    {
        PyObject *tmp_assign_source_98;
        tmp_assign_source_98 = const_tuple_d0062c44ff73c1c48f3f7d27f6672421_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdBu_data, tmp_assign_source_98 );
    }
    {
        PyObject *tmp_assign_source_99;
        tmp_assign_source_99 = const_tuple_9b04358fc83d8c29f6d13e55a485e086_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdGy_data, tmp_assign_source_99 );
    }
    {
        PyObject *tmp_assign_source_100;
        tmp_assign_source_100 = const_tuple_1cf08670e58213d33381f121283ddef7_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdPu_data, tmp_assign_source_100 );
    }
    {
        PyObject *tmp_assign_source_101;
        tmp_assign_source_101 = const_tuple_20bd1f5c0a0f9a93d11e666cd7289af4_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdYlBu_data, tmp_assign_source_101 );
    }
    {
        PyObject *tmp_assign_source_102;
        tmp_assign_source_102 = const_tuple_440d7807917857d4ae4d4cd0c76093e7_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdYlGn_data, tmp_assign_source_102 );
    }
    {
        PyObject *tmp_assign_source_103;
        tmp_assign_source_103 = const_tuple_194951aabc8c4d0f914fa1c18e921dac_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Reds_data, tmp_assign_source_103 );
    }
    {
        PyObject *tmp_assign_source_104;
        tmp_assign_source_104 = const_tuple_db9a981db24a26ea90b278665de2841d_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Spectral_data, tmp_assign_source_104 );
    }
    {
        PyObject *tmp_assign_source_105;
        tmp_assign_source_105 = const_tuple_db58afb74f702702860a0cdf1356a649_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlGn_data, tmp_assign_source_105 );
    }
    {
        PyObject *tmp_assign_source_106;
        tmp_assign_source_106 = const_tuple_259735cef93a8d5cd32533f90f8ce8d3_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlGnBu_data, tmp_assign_source_106 );
    }
    {
        PyObject *tmp_assign_source_107;
        tmp_assign_source_107 = const_tuple_ac83b7f87b65db8bc61c88a0c684aae6_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlOrBr_data, tmp_assign_source_107 );
    }
    {
        PyObject *tmp_assign_source_108;
        tmp_assign_source_108 = const_tuple_8a21be97bcec74887e2b70da615f70d9_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlOrRd_data, tmp_assign_source_108 );
    }
    {
        PyObject *tmp_assign_source_109;
        tmp_assign_source_109 = const_tuple_dd7b62811603d0af86732a1f25835a50_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Accent_data, tmp_assign_source_109 );
    }
    {
        PyObject *tmp_assign_source_110;
        tmp_assign_source_110 = const_tuple_6430e1188db9556163de382028b9d704_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Dark2_data, tmp_assign_source_110 );
    }
    {
        PyObject *tmp_assign_source_111;
        tmp_assign_source_111 = const_tuple_b84fc382b1c73eb24052e00184983189_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Paired_data, tmp_assign_source_111 );
    }
    {
        PyObject *tmp_assign_source_112;
        tmp_assign_source_112 = const_tuple_eb5d9c64a7345357330502cba765a133_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Pastel1_data, tmp_assign_source_112 );
    }
    {
        PyObject *tmp_assign_source_113;
        tmp_assign_source_113 = const_tuple_d6bde7eec8df9cf0ee10c9abbaa88fc9_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Pastel2_data, tmp_assign_source_113 );
    }
    {
        PyObject *tmp_assign_source_114;
        tmp_assign_source_114 = const_tuple_de81471e775d0db13ccc93277369c0bb_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Set1_data, tmp_assign_source_114 );
    }
    {
        PyObject *tmp_assign_source_115;
        tmp_assign_source_115 = const_tuple_9fa91c64747bb6ff4492053856cc06e7_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Set2_data, tmp_assign_source_115 );
    }
    {
        PyObject *tmp_assign_source_116;
        tmp_assign_source_116 = const_tuple_2fe5f075a061fd18cc310f1b06eb16ae_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Set3_data, tmp_assign_source_116 );
    }
    {
        PyObject *tmp_assign_source_117;
        tmp_assign_source_117 = PyDict_Copy( const_dict_7ba6c40e971dab085fcf28da729f7167 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_earth_data, tmp_assign_source_117 );
    }
    {
        PyObject *tmp_assign_source_118;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_subscribed_name_17;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_subscript_name_17;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_subscribed_name_18;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_subscript_name_18;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_subscribed_name_19;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_subscript_name_19;
        tmp_dict_key_22 = const_str_plain_red;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1002;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_17 = tmp_mvar_value_25;
        tmp_subscript_name_17 = const_int_pos_3;
        tmp_dict_value_22 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_17, tmp_subscript_name_17, 3 );
        if ( tmp_dict_value_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1002;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_118 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_118, tmp_dict_key_22, tmp_dict_value_22 );
        Py_DECREF( tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_23 = const_str_plain_green;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_assign_source_118 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1003;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_18 = tmp_mvar_value_26;
        tmp_subscript_name_18 = const_int_pos_3;
        tmp_dict_value_23 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_18, tmp_subscript_name_18, 3 );
        if ( tmp_dict_value_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_118 );

            exception_lineno = 1003;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_118, tmp_dict_key_23, tmp_dict_value_23 );
        Py_DECREF( tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_24 = const_str_plain_blue;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_gfunc );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gfunc );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_assign_source_118 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gfunc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1004;

            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_19 = tmp_mvar_value_27;
        tmp_subscript_name_19 = const_int_pos_3;
        tmp_dict_value_24 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_19, tmp_subscript_name_19, 3 );
        if ( tmp_dict_value_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_118 );

            exception_lineno = 1004;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_118, tmp_dict_key_24, tmp_dict_value_24 );
        Py_DECREF( tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_gray_data, tmp_assign_source_118 );
    }
    {
        PyObject *tmp_assign_source_119;
        tmp_assign_source_119 = MAKE_FUNCTION_matplotlib$_cm$$$function_46__gist_heat_red(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_red, tmp_assign_source_119 );
    }
    {
        PyObject *tmp_assign_source_120;
        tmp_assign_source_120 = MAKE_FUNCTION_matplotlib$_cm$$$function_47__gist_heat_green(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_green, tmp_assign_source_120 );
    }
    {
        PyObject *tmp_assign_source_121;
        tmp_assign_source_121 = MAKE_FUNCTION_matplotlib$_cm$$$function_48__gist_heat_blue(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_blue, tmp_assign_source_121 );
    }
    {
        PyObject *tmp_assign_source_122;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_mvar_value_30;
        tmp_dict_key_25 = const_str_plain_red;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_red );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_heat_red );
        }

        CHECK_OBJECT( tmp_mvar_value_28 );
        tmp_dict_value_25 = tmp_mvar_value_28;
        tmp_assign_source_122 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_122, tmp_dict_key_25, tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_26 = const_str_plain_green;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_green );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_heat_green );
        }

        CHECK_OBJECT( tmp_mvar_value_29 );
        tmp_dict_value_26 = tmp_mvar_value_29;
        tmp_res = PyDict_SetItem( tmp_assign_source_122, tmp_dict_key_26, tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_str_plain_blue;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_blue );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_heat_blue );
        }

        CHECK_OBJECT( tmp_mvar_value_30 );
        tmp_dict_value_27 = tmp_mvar_value_30;
        tmp_res = PyDict_SetItem( tmp_assign_source_122, tmp_dict_key_27, tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_data, tmp_assign_source_122 );
    }
    {
        PyObject *tmp_assign_source_123;
        tmp_assign_source_123 = PyDict_Copy( const_dict_cc81f270cbb7633b5af45d15d4c591ec );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_ncar_data, tmp_assign_source_123 );
    }
    {
        PyObject *tmp_assign_source_124;
        tmp_assign_source_124 = const_tuple_d09dc699a96e56cc6c61b2a9d13ae443_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_rainbow_data, tmp_assign_source_124 );
    }
    {
        PyObject *tmp_assign_source_125;
        tmp_assign_source_125 = PyDict_Copy( const_dict_26510cda7d1dc75818c57eac9563d1d1 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_stern_data, tmp_assign_source_125 );
    }
    {
        PyObject *tmp_assign_source_126;
        tmp_assign_source_126 = MAKE_FUNCTION_matplotlib$_cm$$$function_49__gist_yarg(  );



        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_yarg, tmp_assign_source_126 );
    }
    {
        PyObject *tmp_assign_source_127;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_mvar_value_33;
        tmp_dict_key_28 = const_str_plain_red;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_yarg );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_yarg );
        }

        CHECK_OBJECT( tmp_mvar_value_31 );
        tmp_dict_value_28 = tmp_mvar_value_31;
        tmp_assign_source_127 = _PyDict_NewPresized( 3 );
        tmp_res = PyDict_SetItem( tmp_assign_source_127, tmp_dict_key_28, tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_29 = const_str_plain_green;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_yarg );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_yarg );
        }

        CHECK_OBJECT( tmp_mvar_value_32 );
        tmp_dict_value_29 = tmp_mvar_value_32;
        tmp_res = PyDict_SetItem( tmp_assign_source_127, tmp_dict_key_29, tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_30 = const_str_plain_blue;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_yarg );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_yarg );
        }

        CHECK_OBJECT( tmp_mvar_value_33 );
        tmp_dict_value_30 = tmp_mvar_value_33;
        tmp_res = PyDict_SetItem( tmp_assign_source_127, tmp_dict_key_30, tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_yarg_data, tmp_assign_source_127 );
    }
    {
        PyObject *tmp_assign_source_128;
        tmp_assign_source_128 = DEEP_COPY( const_dict_ac09b61043b30f22f2e956405ad79658 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__coolwarm_data, tmp_assign_source_128 );
    }
    {
        PyObject *tmp_assign_source_129;
        tmp_assign_source_129 = PyDict_Copy( const_dict_46fdda9c2313b830d68498039918be48 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__CMRmap_data, tmp_assign_source_129 );
    }
    {
        PyObject *tmp_assign_source_130;
        tmp_assign_source_130 = DEEP_COPY( const_dict_13dcc09e8b14458a914dc22a80eeee13 );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__wistia_data, tmp_assign_source_130 );
    }
    {
        PyObject *tmp_assign_source_131;
        tmp_assign_source_131 = const_tuple_58dec871a15dc46004a9c29f566fd8e9_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab10_data, tmp_assign_source_131 );
    }
    {
        PyObject *tmp_assign_source_132;
        tmp_assign_source_132 = const_tuple_391173ce944bd5a033152d725addac81_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab20_data, tmp_assign_source_132 );
    }
    {
        PyObject *tmp_assign_source_133;
        tmp_assign_source_133 = const_tuple_53dc862f8960bed320d0251ce04aeaf0_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab20b_data, tmp_assign_source_133 );
    }
    {
        PyObject *tmp_assign_source_134;
        tmp_assign_source_134 = const_tuple_ef7272d3c529510edf3b0815822dbaa3_tuple;
        UPDATE_STRING_DICT0( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab20c_data, tmp_assign_source_134 );
    }
    {
        PyObject *tmp_assign_source_135;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_dict_key_36;
        PyObject *tmp_dict_value_36;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_dict_key_37;
        PyObject *tmp_dict_value_37;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_dict_key_38;
        PyObject *tmp_dict_value_38;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_dict_key_39;
        PyObject *tmp_dict_value_39;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_dict_key_40;
        PyObject *tmp_dict_value_40;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_dict_key_41;
        PyObject *tmp_dict_value_41;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_dict_key_42;
        PyObject *tmp_dict_value_42;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_dict_key_43;
        PyObject *tmp_dict_value_43;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_dict_key_44;
        PyObject *tmp_dict_value_44;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_dict_key_45;
        PyObject *tmp_dict_value_45;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_dict_key_46;
        PyObject *tmp_dict_value_46;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_dict_key_47;
        PyObject *tmp_dict_value_47;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_dict_key_48;
        PyObject *tmp_dict_value_48;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_dict_key_49;
        PyObject *tmp_dict_value_49;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_dict_key_50;
        PyObject *tmp_dict_value_50;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_dict_key_51;
        PyObject *tmp_dict_value_51;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_dict_key_52;
        PyObject *tmp_dict_value_52;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_dict_key_53;
        PyObject *tmp_dict_value_53;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_dict_key_54;
        PyObject *tmp_dict_value_54;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_dict_key_55;
        PyObject *tmp_dict_value_55;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_dict_key_56;
        PyObject *tmp_dict_value_56;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_dict_key_57;
        PyObject *tmp_dict_value_57;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_dict_key_58;
        PyObject *tmp_dict_value_58;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_dict_key_59;
        PyObject *tmp_dict_value_59;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_dict_key_60;
        PyObject *tmp_dict_value_60;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_dict_key_61;
        PyObject *tmp_dict_value_61;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_dict_key_62;
        PyObject *tmp_dict_value_62;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_dict_key_63;
        PyObject *tmp_dict_value_63;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_dict_key_64;
        PyObject *tmp_dict_value_64;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_dict_key_65;
        PyObject *tmp_dict_value_65;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_dict_key_66;
        PyObject *tmp_dict_value_66;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_dict_key_67;
        PyObject *tmp_dict_value_67;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_dict_key_68;
        PyObject *tmp_dict_value_68;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_dict_key_69;
        PyObject *tmp_dict_value_69;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_dict_key_70;
        PyObject *tmp_dict_value_70;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_dict_key_71;
        PyObject *tmp_dict_value_71;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_dict_key_72;
        PyObject *tmp_dict_value_72;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_dict_key_73;
        PyObject *tmp_dict_value_73;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_dict_key_74;
        PyObject *tmp_dict_value_74;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_dict_key_75;
        PyObject *tmp_dict_value_75;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_dict_key_76;
        PyObject *tmp_dict_value_76;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_dict_key_77;
        PyObject *tmp_dict_value_77;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_dict_key_78;
        PyObject *tmp_dict_value_78;
        PyObject *tmp_mvar_value_81;
        PyObject *tmp_dict_key_79;
        PyObject *tmp_dict_value_79;
        PyObject *tmp_mvar_value_82;
        PyObject *tmp_dict_key_80;
        PyObject *tmp_dict_value_80;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_dict_key_81;
        PyObject *tmp_dict_value_81;
        PyObject *tmp_mvar_value_84;
        PyObject *tmp_dict_key_82;
        PyObject *tmp_dict_value_82;
        PyObject *tmp_mvar_value_85;
        PyObject *tmp_dict_key_83;
        PyObject *tmp_dict_value_83;
        PyObject *tmp_mvar_value_86;
        PyObject *tmp_dict_key_84;
        PyObject *tmp_dict_value_84;
        PyObject *tmp_mvar_value_87;
        PyObject *tmp_dict_key_85;
        PyObject *tmp_dict_value_85;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_dict_key_86;
        PyObject *tmp_dict_value_86;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_dict_key_87;
        PyObject *tmp_dict_value_87;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_dict_key_88;
        PyObject *tmp_dict_value_88;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_dict_key_89;
        PyObject *tmp_dict_value_89;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_dict_key_90;
        PyObject *tmp_dict_value_90;
        PyObject *tmp_mvar_value_93;
        PyObject *tmp_dict_key_91;
        PyObject *tmp_dict_value_91;
        PyObject *tmp_mvar_value_94;
        PyObject *tmp_dict_key_92;
        PyObject *tmp_dict_value_92;
        PyObject *tmp_mvar_value_95;
        PyObject *tmp_dict_key_93;
        PyObject *tmp_dict_value_93;
        PyObject *tmp_mvar_value_96;
        PyObject *tmp_dict_key_94;
        PyObject *tmp_dict_value_94;
        PyObject *tmp_dict_key_95;
        PyObject *tmp_dict_value_95;
        PyObject *tmp_mvar_value_97;
        PyObject *tmp_dict_key_96;
        PyObject *tmp_dict_value_96;
        PyObject *tmp_dict_key_97;
        PyObject *tmp_dict_value_97;
        PyObject *tmp_mvar_value_98;
        PyObject *tmp_dict_key_98;
        PyObject *tmp_dict_value_98;
        PyObject *tmp_dict_key_99;
        PyObject *tmp_dict_value_99;
        PyObject *tmp_mvar_value_99;
        PyObject *tmp_dict_key_100;
        PyObject *tmp_dict_value_100;
        PyObject *tmp_dict_key_101;
        PyObject *tmp_dict_value_101;
        PyObject *tmp_mvar_value_100;
        PyObject *tmp_dict_key_102;
        PyObject *tmp_dict_value_102;
        PyObject *tmp_dict_key_103;
        PyObject *tmp_dict_value_103;
        PyObject *tmp_mvar_value_101;
        PyObject *tmp_dict_key_104;
        PyObject *tmp_dict_value_104;
        PyObject *tmp_dict_key_105;
        PyObject *tmp_dict_value_105;
        PyObject *tmp_mvar_value_102;
        PyObject *tmp_dict_key_106;
        PyObject *tmp_dict_value_106;
        PyObject *tmp_dict_key_107;
        PyObject *tmp_dict_value_107;
        PyObject *tmp_mvar_value_103;
        PyObject *tmp_dict_key_108;
        PyObject *tmp_dict_value_108;
        PyObject *tmp_dict_key_109;
        PyObject *tmp_dict_value_109;
        PyObject *tmp_mvar_value_104;
        PyObject *tmp_dict_key_110;
        PyObject *tmp_dict_value_110;
        PyObject *tmp_dict_key_111;
        PyObject *tmp_dict_value_111;
        PyObject *tmp_mvar_value_105;
        PyObject *tmp_dict_key_112;
        PyObject *tmp_dict_value_112;
        PyObject *tmp_dict_key_113;
        PyObject *tmp_dict_value_113;
        PyObject *tmp_mvar_value_106;
        PyObject *tmp_dict_key_114;
        PyObject *tmp_dict_value_114;
        PyObject *tmp_dict_key_115;
        PyObject *tmp_dict_value_115;
        PyObject *tmp_mvar_value_107;
        PyObject *tmp_dict_key_116;
        PyObject *tmp_dict_value_116;
        PyObject *tmp_dict_key_117;
        PyObject *tmp_dict_value_117;
        PyObject *tmp_mvar_value_108;
        tmp_dict_key_31 = const_str_plain_Blues;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Blues_data );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Blues_data );
        }

        if ( tmp_mvar_value_34 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Blues_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1350;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_31 = tmp_mvar_value_34;
        tmp_assign_source_135 = _PyDict_NewPresized( 75 );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_31, tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_32 = const_str_plain_BrBG;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__BrBG_data );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__BrBG_data );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_BrBG_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1351;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_32 = tmp_mvar_value_35;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_32, tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_33 = const_str_plain_BuGn;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__BuGn_data );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__BuGn_data );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_BuGn_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1352;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_33 = tmp_mvar_value_36;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_33, tmp_dict_value_33 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_34 = const_str_plain_BuPu;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__BuPu_data );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__BuPu_data );
        }

        if ( tmp_mvar_value_37 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_BuPu_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1353;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_34 = tmp_mvar_value_37;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_34, tmp_dict_value_34 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_35 = const_str_plain_CMRmap;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__CMRmap_data );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__CMRmap_data );
        }

        CHECK_OBJECT( tmp_mvar_value_38 );
        tmp_dict_value_35 = tmp_mvar_value_38;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_35, tmp_dict_value_35 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_36 = const_str_plain_GnBu;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__GnBu_data );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GnBu_data );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GnBu_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1355;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_36 = tmp_mvar_value_39;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_36, tmp_dict_value_36 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_37 = const_str_plain_Greens;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Greens_data );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Greens_data );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Greens_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1356;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_37 = tmp_mvar_value_40;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_37, tmp_dict_value_37 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_38 = const_str_plain_Greys;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Greys_data );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Greys_data );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Greys_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1357;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_38 = tmp_mvar_value_41;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_38, tmp_dict_value_38 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_39 = const_str_plain_OrRd;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__OrRd_data );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__OrRd_data );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_OrRd_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1358;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_39 = tmp_mvar_value_42;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_39, tmp_dict_value_39 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_40 = const_str_plain_Oranges;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Oranges_data );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Oranges_data );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Oranges_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1359;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_40 = tmp_mvar_value_43;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_40, tmp_dict_value_40 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_41 = const_str_plain_PRGn;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PRGn_data );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__PRGn_data );
        }

        if ( tmp_mvar_value_44 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_PRGn_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1360;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_41 = tmp_mvar_value_44;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_41, tmp_dict_value_41 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_42 = const_str_plain_PiYG;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PiYG_data );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__PiYG_data );
        }

        if ( tmp_mvar_value_45 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_PiYG_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1361;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_42 = tmp_mvar_value_45;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_42, tmp_dict_value_42 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_43 = const_str_plain_PuBu;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuBu_data );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__PuBu_data );
        }

        if ( tmp_mvar_value_46 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_PuBu_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1362;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_43 = tmp_mvar_value_46;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_43, tmp_dict_value_43 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_44 = const_str_plain_PuBuGn;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuBuGn_data );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__PuBuGn_data );
        }

        if ( tmp_mvar_value_47 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_PuBuGn_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1363;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_44 = tmp_mvar_value_47;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_44, tmp_dict_value_44 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_45 = const_str_plain_PuOr;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuOr_data );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__PuOr_data );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_PuOr_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1364;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_45 = tmp_mvar_value_48;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_45, tmp_dict_value_45 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_46 = const_str_plain_PuRd;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__PuRd_data );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__PuRd_data );
        }

        if ( tmp_mvar_value_49 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_PuRd_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1365;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_46 = tmp_mvar_value_49;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_46, tmp_dict_value_46 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_47 = const_str_plain_Purples;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Purples_data );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Purples_data );
        }

        if ( tmp_mvar_value_50 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Purples_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1366;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_47 = tmp_mvar_value_50;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_47, tmp_dict_value_47 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_48 = const_str_plain_RdBu;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdBu_data );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__RdBu_data );
        }

        if ( tmp_mvar_value_51 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_RdBu_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1367;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_48 = tmp_mvar_value_51;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_48, tmp_dict_value_48 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_49 = const_str_plain_RdGy;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdGy_data );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__RdGy_data );
        }

        if ( tmp_mvar_value_52 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_RdGy_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1368;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_49 = tmp_mvar_value_52;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_49, tmp_dict_value_49 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_50 = const_str_plain_RdPu;
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdPu_data );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__RdPu_data );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_RdPu_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1369;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_50 = tmp_mvar_value_53;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_50, tmp_dict_value_50 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_51 = const_str_plain_RdYlBu;
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdYlBu_data );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__RdYlBu_data );
        }

        if ( tmp_mvar_value_54 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_RdYlBu_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1370;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_51 = tmp_mvar_value_54;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_51, tmp_dict_value_51 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_52 = const_str_plain_RdYlGn;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__RdYlGn_data );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__RdYlGn_data );
        }

        if ( tmp_mvar_value_55 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_RdYlGn_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1371;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_52 = tmp_mvar_value_55;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_52, tmp_dict_value_52 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_53 = const_str_plain_Reds;
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Reds_data );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Reds_data );
        }

        if ( tmp_mvar_value_56 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Reds_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1372;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_53 = tmp_mvar_value_56;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_53, tmp_dict_value_53 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_54 = const_str_plain_Spectral;
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Spectral_data );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Spectral_data );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Spectral_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1373;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_54 = tmp_mvar_value_57;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_54, tmp_dict_value_54 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_55 = const_str_plain_Wistia;
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__wistia_data );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__wistia_data );
        }

        CHECK_OBJECT( tmp_mvar_value_58 );
        tmp_dict_value_55 = tmp_mvar_value_58;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_55, tmp_dict_value_55 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_56 = const_str_plain_YlGn;
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlGn_data );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__YlGn_data );
        }

        if ( tmp_mvar_value_59 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_YlGn_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1375;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_56 = tmp_mvar_value_59;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_56, tmp_dict_value_56 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_57 = const_str_plain_YlGnBu;
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlGnBu_data );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__YlGnBu_data );
        }

        if ( tmp_mvar_value_60 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_YlGnBu_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1376;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_57 = tmp_mvar_value_60;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_57, tmp_dict_value_57 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_58 = const_str_plain_YlOrBr;
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlOrBr_data );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__YlOrBr_data );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_YlOrBr_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1377;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_58 = tmp_mvar_value_61;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_58, tmp_dict_value_58 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_59 = const_str_plain_YlOrRd;
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__YlOrRd_data );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__YlOrRd_data );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_YlOrRd_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1378;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_59 = tmp_mvar_value_62;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_59, tmp_dict_value_59 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_60 = const_str_plain_afmhot;
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__afmhot_data );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__afmhot_data );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_afmhot_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1379;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_60 = tmp_mvar_value_63;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_60, tmp_dict_value_60 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_61 = const_str_plain_autumn;
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__autumn_data );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__autumn_data );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_autumn_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1380;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_61 = tmp_mvar_value_64;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_61, tmp_dict_value_61 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_62 = const_str_plain_binary;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__binary_data );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__binary_data );
        }

        if ( tmp_mvar_value_65 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_binary_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1381;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_62 = tmp_mvar_value_65;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_62, tmp_dict_value_62 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_63 = const_str_plain_bone;
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__bone_data );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__bone_data );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_bone_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1382;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_63 = tmp_mvar_value_66;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_63, tmp_dict_value_63 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_64 = const_str_plain_brg;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__brg_data );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__brg_data );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_brg_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1383;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_64 = tmp_mvar_value_67;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_64, tmp_dict_value_64 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_65 = const_str_plain_bwr;
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__bwr_data );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__bwr_data );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_bwr_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1384;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_65 = tmp_mvar_value_68;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_65, tmp_dict_value_65 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_66 = const_str_plain_cool;
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__cool_data );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__cool_data );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_cool_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1385;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_66 = tmp_mvar_value_69;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_66, tmp_dict_value_66 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_67 = const_str_plain_coolwarm;
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__coolwarm_data );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__coolwarm_data );
        }

        CHECK_OBJECT( tmp_mvar_value_70 );
        tmp_dict_value_67 = tmp_mvar_value_70;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_67, tmp_dict_value_67 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_68 = const_str_plain_copper;
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__copper_data );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__copper_data );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_copper_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1387;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_68 = tmp_mvar_value_71;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_68, tmp_dict_value_68 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_69 = const_str_plain_cubehelix;
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__cubehelix_data );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__cubehelix_data );
        }

        if ( tmp_mvar_value_72 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_cubehelix_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1388;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_69 = tmp_mvar_value_72;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_69, tmp_dict_value_69 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_70 = const_str_plain_flag;
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__flag_data );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__flag_data );
        }

        if ( tmp_mvar_value_73 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_flag_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1389;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_70 = tmp_mvar_value_73;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_70, tmp_dict_value_70 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_71 = const_str_plain_gist_earth;
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_earth_data );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_earth_data );
        }

        if ( tmp_mvar_value_74 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_gist_earth_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1390;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_71 = tmp_mvar_value_74;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_71, tmp_dict_value_71 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_72 = const_str_plain_gist_gray;
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_gray_data );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_gray_data );
        }

        CHECK_OBJECT( tmp_mvar_value_75 );
        tmp_dict_value_72 = tmp_mvar_value_75;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_72, tmp_dict_value_72 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_73 = const_str_plain_gist_heat;
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_heat_data );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_heat_data );
        }

        CHECK_OBJECT( tmp_mvar_value_76 );
        tmp_dict_value_73 = tmp_mvar_value_76;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_73, tmp_dict_value_73 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_74 = const_str_plain_gist_ncar;
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_ncar_data );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_ncar_data );
        }

        CHECK_OBJECT( tmp_mvar_value_77 );
        tmp_dict_value_74 = tmp_mvar_value_77;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_74, tmp_dict_value_74 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_75 = const_str_plain_gist_rainbow;
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_rainbow_data );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_rainbow_data );
        }

        CHECK_OBJECT( tmp_mvar_value_78 );
        tmp_dict_value_75 = tmp_mvar_value_78;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_75, tmp_dict_value_75 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_76 = const_str_plain_gist_stern;
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_stern_data );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_stern_data );
        }

        CHECK_OBJECT( tmp_mvar_value_79 );
        tmp_dict_value_76 = tmp_mvar_value_79;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_76, tmp_dict_value_76 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_77 = const_str_plain_gist_yarg;
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gist_yarg_data );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gist_yarg_data );
        }

        CHECK_OBJECT( tmp_mvar_value_80 );
        tmp_dict_value_77 = tmp_mvar_value_80;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_77, tmp_dict_value_77 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_78 = const_str_plain_gnuplot;
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gnuplot_data );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gnuplot_data );
        }

        if ( tmp_mvar_value_81 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_gnuplot_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1397;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_78 = tmp_mvar_value_81;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_78, tmp_dict_value_78 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_79 = const_str_plain_gnuplot2;
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gnuplot2_data );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gnuplot2_data );
        }

        if ( tmp_mvar_value_82 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_gnuplot2_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1398;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_79 = tmp_mvar_value_82;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_79, tmp_dict_value_79 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_80 = const_str_plain_gray;
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__gray_data );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__gray_data );
        }

        if ( tmp_mvar_value_83 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_gray_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1399;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_80 = tmp_mvar_value_83;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_80, tmp_dict_value_80 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_81 = const_str_plain_hot;
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__hot_data );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__hot_data );
        }

        if ( tmp_mvar_value_84 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_hot_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1400;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_81 = tmp_mvar_value_84;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_81, tmp_dict_value_81 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_82 = const_str_plain_hsv;
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__hsv_data );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__hsv_data );
        }

        if ( tmp_mvar_value_85 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_hsv_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1401;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_82 = tmp_mvar_value_85;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_82, tmp_dict_value_82 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_83 = const_str_plain_jet;
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__jet_data );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__jet_data );
        }

        if ( tmp_mvar_value_86 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_jet_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1402;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_83 = tmp_mvar_value_86;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_83, tmp_dict_value_83 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_84 = const_str_plain_nipy_spectral;
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__nipy_spectral_data );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__nipy_spectral_data );
        }

        if ( tmp_mvar_value_87 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_nipy_spectral_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1403;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_84 = tmp_mvar_value_87;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_84, tmp_dict_value_84 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_85 = const_str_plain_ocean;
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__ocean_data );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ocean_data );
        }

        if ( tmp_mvar_value_88 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ocean_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1404;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_85 = tmp_mvar_value_88;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_85, tmp_dict_value_85 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_86 = const_str_plain_pink;
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__pink_data );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__pink_data );
        }

        if ( tmp_mvar_value_89 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_pink_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1405;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_86 = tmp_mvar_value_89;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_86, tmp_dict_value_86 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_87 = const_str_plain_prism;
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__prism_data );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__prism_data );
        }

        if ( tmp_mvar_value_90 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_prism_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1406;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_87 = tmp_mvar_value_90;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_87, tmp_dict_value_87 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_88 = const_str_plain_rainbow;
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__rainbow_data );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__rainbow_data );
        }

        if ( tmp_mvar_value_91 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_rainbow_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1407;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_88 = tmp_mvar_value_91;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_88, tmp_dict_value_88 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_89 = const_str_plain_seismic;
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__seismic_data );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seismic_data );
        }

        if ( tmp_mvar_value_92 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seismic_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1408;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_89 = tmp_mvar_value_92;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_89, tmp_dict_value_89 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_90 = const_str_plain_spring;
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__spring_data );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__spring_data );
        }

        if ( tmp_mvar_value_93 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_spring_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1409;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_90 = tmp_mvar_value_93;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_90, tmp_dict_value_90 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_91 = const_str_plain_summer;
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__summer_data );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__summer_data );
        }

        if ( tmp_mvar_value_94 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_summer_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1410;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_91 = tmp_mvar_value_94;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_91, tmp_dict_value_91 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_92 = const_str_plain_terrain;
        tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__terrain_data );

        if (unlikely( tmp_mvar_value_95 == NULL ))
        {
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__terrain_data );
        }

        if ( tmp_mvar_value_95 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_terrain_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1411;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_92 = tmp_mvar_value_95;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_92, tmp_dict_value_92 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_93 = const_str_plain_winter;
        tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__winter_data );

        if (unlikely( tmp_mvar_value_96 == NULL ))
        {
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__winter_data );
        }

        if ( tmp_mvar_value_96 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_winter_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1412;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_93 = tmp_mvar_value_96;
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_93, tmp_dict_value_93 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_94 = const_str_plain_Accent;
        tmp_dict_key_95 = const_str_plain_listed;
        tmp_mvar_value_97 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Accent_data );

        if (unlikely( tmp_mvar_value_97 == NULL ))
        {
            tmp_mvar_value_97 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Accent_data );
        }

        if ( tmp_mvar_value_97 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Accent_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1414;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_95 = tmp_mvar_value_97;
        tmp_dict_value_94 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_94, tmp_dict_key_95, tmp_dict_value_95 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_94, tmp_dict_value_94 );
        Py_DECREF( tmp_dict_value_94 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_96 = const_str_plain_Dark2;
        tmp_dict_key_97 = const_str_plain_listed;
        tmp_mvar_value_98 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Dark2_data );

        if (unlikely( tmp_mvar_value_98 == NULL ))
        {
            tmp_mvar_value_98 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Dark2_data );
        }

        if ( tmp_mvar_value_98 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Dark2_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1415;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_97 = tmp_mvar_value_98;
        tmp_dict_value_96 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_96, tmp_dict_key_97, tmp_dict_value_97 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_96, tmp_dict_value_96 );
        Py_DECREF( tmp_dict_value_96 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_98 = const_str_plain_Paired;
        tmp_dict_key_99 = const_str_plain_listed;
        tmp_mvar_value_99 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Paired_data );

        if (unlikely( tmp_mvar_value_99 == NULL ))
        {
            tmp_mvar_value_99 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Paired_data );
        }

        if ( tmp_mvar_value_99 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Paired_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1416;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_99 = tmp_mvar_value_99;
        tmp_dict_value_98 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_98, tmp_dict_key_99, tmp_dict_value_99 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_98, tmp_dict_value_98 );
        Py_DECREF( tmp_dict_value_98 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_100 = const_str_plain_Pastel1;
        tmp_dict_key_101 = const_str_plain_listed;
        tmp_mvar_value_100 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Pastel1_data );

        if (unlikely( tmp_mvar_value_100 == NULL ))
        {
            tmp_mvar_value_100 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Pastel1_data );
        }

        if ( tmp_mvar_value_100 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Pastel1_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1417;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_101 = tmp_mvar_value_100;
        tmp_dict_value_100 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_100, tmp_dict_key_101, tmp_dict_value_101 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_100, tmp_dict_value_100 );
        Py_DECREF( tmp_dict_value_100 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_102 = const_str_plain_Pastel2;
        tmp_dict_key_103 = const_str_plain_listed;
        tmp_mvar_value_101 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Pastel2_data );

        if (unlikely( tmp_mvar_value_101 == NULL ))
        {
            tmp_mvar_value_101 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Pastel2_data );
        }

        if ( tmp_mvar_value_101 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Pastel2_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1418;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_103 = tmp_mvar_value_101;
        tmp_dict_value_102 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_102, tmp_dict_key_103, tmp_dict_value_103 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_102, tmp_dict_value_102 );
        Py_DECREF( tmp_dict_value_102 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_104 = const_str_plain_Set1;
        tmp_dict_key_105 = const_str_plain_listed;
        tmp_mvar_value_102 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Set1_data );

        if (unlikely( tmp_mvar_value_102 == NULL ))
        {
            tmp_mvar_value_102 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Set1_data );
        }

        if ( tmp_mvar_value_102 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Set1_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1419;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_105 = tmp_mvar_value_102;
        tmp_dict_value_104 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_104, tmp_dict_key_105, tmp_dict_value_105 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_104, tmp_dict_value_104 );
        Py_DECREF( tmp_dict_value_104 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_106 = const_str_plain_Set2;
        tmp_dict_key_107 = const_str_plain_listed;
        tmp_mvar_value_103 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Set2_data );

        if (unlikely( tmp_mvar_value_103 == NULL ))
        {
            tmp_mvar_value_103 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Set2_data );
        }

        if ( tmp_mvar_value_103 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Set2_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1420;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_107 = tmp_mvar_value_103;
        tmp_dict_value_106 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_106, tmp_dict_key_107, tmp_dict_value_107 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_106, tmp_dict_value_106 );
        Py_DECREF( tmp_dict_value_106 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_108 = const_str_plain_Set3;
        tmp_dict_key_109 = const_str_plain_listed;
        tmp_mvar_value_104 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__Set3_data );

        if (unlikely( tmp_mvar_value_104 == NULL ))
        {
            tmp_mvar_value_104 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__Set3_data );
        }

        if ( tmp_mvar_value_104 == NULL )
        {
            Py_DECREF( tmp_assign_source_135 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_Set3_data" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 1421;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_109 = tmp_mvar_value_104;
        tmp_dict_value_108 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_108, tmp_dict_key_109, tmp_dict_value_109 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_108, tmp_dict_value_108 );
        Py_DECREF( tmp_dict_value_108 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_110 = const_str_plain_tab10;
        tmp_dict_key_111 = const_str_plain_listed;
        tmp_mvar_value_105 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab10_data );

        if (unlikely( tmp_mvar_value_105 == NULL ))
        {
            tmp_mvar_value_105 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__tab10_data );
        }

        CHECK_OBJECT( tmp_mvar_value_105 );
        tmp_dict_value_111 = tmp_mvar_value_105;
        tmp_dict_value_110 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_110, tmp_dict_key_111, tmp_dict_value_111 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_110, tmp_dict_value_110 );
        Py_DECREF( tmp_dict_value_110 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_112 = const_str_plain_tab20;
        tmp_dict_key_113 = const_str_plain_listed;
        tmp_mvar_value_106 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab20_data );

        if (unlikely( tmp_mvar_value_106 == NULL ))
        {
            tmp_mvar_value_106 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__tab20_data );
        }

        CHECK_OBJECT( tmp_mvar_value_106 );
        tmp_dict_value_113 = tmp_mvar_value_106;
        tmp_dict_value_112 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_112, tmp_dict_key_113, tmp_dict_value_113 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_112, tmp_dict_value_112 );
        Py_DECREF( tmp_dict_value_112 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_114 = const_str_plain_tab20b;
        tmp_dict_key_115 = const_str_plain_listed;
        tmp_mvar_value_107 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab20b_data );

        if (unlikely( tmp_mvar_value_107 == NULL ))
        {
            tmp_mvar_value_107 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__tab20b_data );
        }

        CHECK_OBJECT( tmp_mvar_value_107 );
        tmp_dict_value_115 = tmp_mvar_value_107;
        tmp_dict_value_114 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_114, tmp_dict_key_115, tmp_dict_value_115 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_114, tmp_dict_value_114 );
        Py_DECREF( tmp_dict_value_114 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_116 = const_str_plain_tab20c;
        tmp_dict_key_117 = const_str_plain_listed;
        tmp_mvar_value_108 = GET_STRING_DICT_VALUE( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain__tab20c_data );

        if (unlikely( tmp_mvar_value_108 == NULL ))
        {
            tmp_mvar_value_108 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__tab20c_data );
        }

        CHECK_OBJECT( tmp_mvar_value_108 );
        tmp_dict_value_117 = tmp_mvar_value_108;
        tmp_dict_value_116 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_116, tmp_dict_key_117, tmp_dict_value_117 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_135, tmp_dict_key_116, tmp_dict_value_116 );
        Py_DECREF( tmp_dict_value_116 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_matplotlib$_cm, (Nuitka_StringObject *)const_str_plain_datad, tmp_assign_source_135 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc635218d6bfe5f1da89e2d1d276c5ff );
#endif
    popFrameStack();

    assertFrameObject( frame_dc635218d6bfe5f1da89e2d1d276c5ff );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc635218d6bfe5f1da89e2d1d276c5ff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dc635218d6bfe5f1da89e2d1d276c5ff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dc635218d6bfe5f1da89e2d1d276c5ff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dc635218d6bfe5f1da89e2d1d276c5ff, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_matplotlib$_cm );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
