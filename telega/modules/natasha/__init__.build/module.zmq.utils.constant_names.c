/* Generated code for Python module 'zmq.utils.constant_names'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_zmq$utils$constant_names" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_zmq$utils$constant_names;
PyDictObject *moduledict_zmq$utils$constant_names;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_bytes_sockopt_names;
static PyObject *const_str_plain_new_in;
extern PyObject *const_str_plain_itertools;
static PyObject *const_str_plain_CURVE_SERVERKEY;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain_THREAD_SCHED_POLICY;
static PyObject *const_str_plain_GSSAPI_SERVICE_PRINCIPAL;
static PyObject *const_str_plain_SRCFD;
extern PyObject *const_str_plain_FD;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_SOCKS_PROXY;
static PyObject *const_dict_2a5f51e5c7751b46e45cff87b85ac6dd;
static PyObject *const_str_plain_CURVE_PUBLICKEY;
static PyObject *const_str_plain_RECOVERY_IVL_MSEC;
static PyObject *const_str_plain_CONNECT_RID;
static PyObject *const_str_plain_EVENT;
static PyObject *const_str_plain_ZAP_DOMAIN;
static PyObject *const_str_plain_MAX_SOCKETS;
static PyObject *const_tuple_int_pos_3_int_pos_2_int_pos_2_tuple;
extern PyObject *const_str_plain_RATE;
extern PyObject *const_tuple_str_plain_E_tuple;
static PyObject *const_str_plain_XPUB_WELCOME_MSG;
static PyObject *const_str_plain_VMCI_BUFFER_MIN_SIZE;
static PyObject *const_list_str_plain_FD_list;
static PyObject *const_str_plain_THREAD_NAME_PREFIX;
static PyObject *const_str_plain_THREAD_AFFINITY_CPU_ADD;
static PyObject *const_str_plain_MAXMSGSIZE;
static PyObject *const_dict_417adb9ca677d2b9372ce267faa88805;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_SNDBUF;
static PyObject *const_str_plain_TCP_ACCEPT_FILTER;
static PyObject *const_str_plain_VMCI_BUFFER_MAX_SIZE;
static PyObject *const_str_digest_a62ac28eb174428a502692dd12109d04;
static PyObject *const_list_595fec3f25e3937ebb153fce699edca7_list;
static PyObject *const_dict_295e83e36d21d2865b5d167c5055ba21;
extern PyObject *const_str_plain_int64_sockopt_names;
static PyObject *const_str_plain_MCAST_LOOP;
static PyObject *const_list_4b77c782b15c0ee0e7de3f00550f63d7_list;
static PyObject *const_list_str_plain_MORE_str_plain_SRCFD_str_plain_SHARED_list;
extern PyObject *const_str_plain_UNSUBSCRIBE;
static PyObject *const_str_plain_DOWNSTREAM;
static PyObject *const_str_plain_no_prefix;
static PyObject *const_str_plain_CONNECT_ROUTING_ID;
static PyObject *const_list_03046f42639e5f33779fffca82a9f62c_list;
extern PyObject *const_tuple_empty;
static PyObject *const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list;
static PyObject *const_str_plain_VMCI_BUFFER_SIZE;
extern PyObject *const_str_plain_name;
static PyObject *const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list;
extern PyObject *const_str_plain_HWM;
extern PyObject *const_str_plain_msg_opt_names;
extern PyObject *const_str_plain_switched_sockopt_names;
static PyObject *const_str_plain_SHARED;
extern PyObject *const_str_plain_LAST_ENDPOINT;
static PyObject *const_str_digest_b768ec0e9a42563696ab2d9edc0a3111;
static PyObject *const_str_plain_CURVE_SECRETKEY;
static PyObject *const_str_plain_IDENTITY;
extern PyObject *const_str_digest_d4b76186abce19777779ec7f28c5ac91;
extern PyObject *const_tuple_str_plain_chain_tuple;
static PyObject *const_str_plain_RCVBUF;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_int_sockopt_names;
static PyObject *const_str_plain_THREAD_AFFINITY_CPU_REMOVE;
static PyObject *const_str_plain_MORE;
static PyObject *const_list_449dda691be93df6c2c896c2f6ca2ca0_list;
extern PyObject *const_str_plain_SUBSCRIBE;
static PyObject *const_str_plain_PLAIN_USERNAME;
static PyObject *const_str_plain_IO_THREADS;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_dd79ed315912baf3fc3b7866166d495f;
static PyObject *const_str_plain_SOCKET_LIMIT;
static PyObject *const_str_plain_THREAD_PRIORITY;
static PyObject *const_str_plain_PLAIN_PASSWORD;
static PyObject *const_str_plain_AFFINITY;
extern PyObject *const_str_plain_fd_sockopt_names;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_draft_in;
static PyObject *const_str_digest_36cda4dfd208c336c466e40f2d596663;
static PyObject *const_str_plain_removed_in;
static PyObject *const_str_plain_RECOVERY_IVL;
extern PyObject *const_str_plain_base_names;
extern PyObject *const_str_plain_RCVMORE;
static PyObject *const_str_plain_all_names;
extern PyObject *const_str_plain_chain;
static PyObject *const_str_plain_MSG_T_SIZE;
extern PyObject *const_int_pos_3;
static PyObject *const_tuple_str_plain_EVENT_tuple;
static PyObject *const_str_plain_BLOCKY;
static PyObject *const_list_a71a9688c352a23265b556e41f338e95_list;
static PyObject *const_str_plain_UPSTREAM;
extern PyObject *const_tuple_str_plain_name_tuple;
static PyObject *const_str_plain_ROUTING_ID;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_E;
extern PyObject *const_str_plain_startswith;
static PyObject *const_str_plain_GSSAPI_PRINCIPAL;
static PyObject *const_str_plain_SWAP;
extern PyObject *const_str_plain_ctx_opt_names;
static PyObject *const_str_plain_BINDTODEVICE;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_new_in = UNSTREAM_STRING_ASCII( &constant_bin[ 652085 ], 6, 1 );
    const_str_plain_CURVE_SERVERKEY = UNSTREAM_STRING_ASCII( &constant_bin[ 5846307 ], 15, 1 );
    const_str_plain_THREAD_SCHED_POLICY = UNSTREAM_STRING_ASCII( &constant_bin[ 5846322 ], 19, 1 );
    const_str_plain_GSSAPI_SERVICE_PRINCIPAL = UNSTREAM_STRING_ASCII( &constant_bin[ 5846341 ], 24, 1 );
    const_str_plain_SRCFD = UNSTREAM_STRING_ASCII( &constant_bin[ 5846365 ], 5, 1 );
    const_str_plain_SOCKS_PROXY = UNSTREAM_STRING_ASCII( &constant_bin[ 5846370 ], 11, 1 );
    const_dict_2a5f51e5c7751b46e45cff87b85ac6dd = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5846381 ], 2524 );
    const_str_plain_CURVE_PUBLICKEY = UNSTREAM_STRING_ASCII( &constant_bin[ 5847145 ], 15, 1 );
    const_str_plain_RECOVERY_IVL_MSEC = UNSTREAM_STRING_ASCII( &constant_bin[ 5848905 ], 17, 1 );
    const_str_plain_CONNECT_RID = UNSTREAM_STRING_ASCII( &constant_bin[ 5847495 ], 11, 1 );
    const_str_plain_EVENT = UNSTREAM_STRING_ASCII( &constant_bin[ 555826 ], 5, 1 );
    const_str_plain_ZAP_DOMAIN = UNSTREAM_STRING_ASCII( &constant_bin[ 5847248 ], 10, 1 );
    const_str_plain_MAX_SOCKETS = UNSTREAM_STRING_ASCII( &constant_bin[ 5846569 ], 11, 1 );
    const_tuple_int_pos_3_int_pos_2_int_pos_2_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_int_pos_3_int_pos_2_int_pos_2_tuple, 0, const_int_pos_3 ); Py_INCREF( const_int_pos_3 );
    PyTuple_SET_ITEM( const_tuple_int_pos_3_int_pos_2_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_3_int_pos_2_int_pos_2_tuple, 2, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_str_plain_XPUB_WELCOME_MSG = UNSTREAM_STRING_ASCII( &constant_bin[ 5848922 ], 16, 1 );
    const_str_plain_VMCI_BUFFER_MIN_SIZE = UNSTREAM_STRING_ASCII( &constant_bin[ 5848938 ], 20, 1 );
    const_list_str_plain_FD_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_FD_list, 0, const_str_plain_FD ); Py_INCREF( const_str_plain_FD );
    const_str_plain_THREAD_NAME_PREFIX = UNSTREAM_STRING_ASCII( &constant_bin[ 5847821 ], 18, 1 );
    const_str_plain_THREAD_AFFINITY_CPU_ADD = UNSTREAM_STRING_ASCII( &constant_bin[ 5847768 ], 23, 1 );
    const_str_plain_MAXMSGSIZE = UNSTREAM_STRING_ASCII( &constant_bin[ 5848958 ], 10, 1 );
    const_dict_417adb9ca677d2b9372ce267faa88805 = _PyDict_NewPresized( 1 );
    const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list = PyList_New( 6 );
    const_str_plain_UPSTREAM = UNSTREAM_STRING_ASCII( &constant_bin[ 5848968 ], 8, 1 );
    PyList_SET_ITEM( const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list, 0, const_str_plain_UPSTREAM ); Py_INCREF( const_str_plain_UPSTREAM );
    const_str_plain_DOWNSTREAM = UNSTREAM_STRING_ASCII( &constant_bin[ 5848976 ], 10, 1 );
    PyList_SET_ITEM( const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list, 1, const_str_plain_DOWNSTREAM ); Py_INCREF( const_str_plain_DOWNSTREAM );
    PyList_SET_ITEM( const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list, 2, const_str_plain_HWM ); Py_INCREF( const_str_plain_HWM );
    const_str_plain_SWAP = UNSTREAM_STRING_ASCII( &constant_bin[ 4930821 ], 4, 1 );
    PyList_SET_ITEM( const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list, 3, const_str_plain_SWAP ); Py_INCREF( const_str_plain_SWAP );
    const_str_plain_MCAST_LOOP = UNSTREAM_STRING_ASCII( &constant_bin[ 5848986 ], 10, 1 );
    PyList_SET_ITEM( const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list, 4, const_str_plain_MCAST_LOOP ); Py_INCREF( const_str_plain_MCAST_LOOP );
    PyList_SET_ITEM( const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list, 5, const_str_plain_RECOVERY_IVL_MSEC ); Py_INCREF( const_str_plain_RECOVERY_IVL_MSEC );
    PyDict_SetItem( const_dict_417adb9ca677d2b9372ce267faa88805, const_tuple_int_pos_3_int_pos_2_int_pos_2_tuple, const_list_1ae7ff9cf809a1e8aa7fe99b8c393e4e_list );
    assert( PyDict_Size( const_dict_417adb9ca677d2b9372ce267faa88805 ) == 1 );
    const_str_plain_SNDBUF = UNSTREAM_STRING_ASCII( &constant_bin[ 5848996 ], 6, 1 );
    const_str_plain_TCP_ACCEPT_FILTER = UNSTREAM_STRING_ASCII( &constant_bin[ 5849002 ], 17, 1 );
    const_str_plain_VMCI_BUFFER_MAX_SIZE = UNSTREAM_STRING_ASCII( &constant_bin[ 5849019 ], 20, 1 );
    const_str_digest_a62ac28eb174428a502692dd12109d04 = UNSTREAM_STRING_ASCII( &constant_bin[ 5849039 ], 18, 0 );
    const_list_595fec3f25e3937ebb153fce699edca7_list = PyList_New( 19 );
    const_str_plain_IDENTITY = UNSTREAM_STRING_ASCII( &constant_bin[ 1225306 ], 8, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 0, const_str_plain_IDENTITY ); Py_INCREF( const_str_plain_IDENTITY );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 1, const_str_plain_SUBSCRIBE ); Py_INCREF( const_str_plain_SUBSCRIBE );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 2, const_str_plain_UNSUBSCRIBE ); Py_INCREF( const_str_plain_UNSUBSCRIBE );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 3, const_str_plain_LAST_ENDPOINT ); Py_INCREF( const_str_plain_LAST_ENDPOINT );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 4, const_str_plain_TCP_ACCEPT_FILTER ); Py_INCREF( const_str_plain_TCP_ACCEPT_FILTER );
    const_str_plain_PLAIN_USERNAME = UNSTREAM_STRING_ASCII( &constant_bin[ 5847099 ], 14, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 5, const_str_plain_PLAIN_USERNAME ); Py_INCREF( const_str_plain_PLAIN_USERNAME );
    const_str_plain_PLAIN_PASSWORD = UNSTREAM_STRING_ASCII( &constant_bin[ 5847115 ], 14, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 6, const_str_plain_PLAIN_PASSWORD ); Py_INCREF( const_str_plain_PLAIN_PASSWORD );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 7, const_str_plain_CURVE_PUBLICKEY ); Py_INCREF( const_str_plain_CURVE_PUBLICKEY );
    const_str_plain_CURVE_SECRETKEY = UNSTREAM_STRING_ASCII( &constant_bin[ 5847162 ], 15, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 8, const_str_plain_CURVE_SECRETKEY ); Py_INCREF( const_str_plain_CURVE_SECRETKEY );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 9, const_str_plain_CURVE_SERVERKEY ); Py_INCREF( const_str_plain_CURVE_SERVERKEY );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 10, const_str_plain_ZAP_DOMAIN ); Py_INCREF( const_str_plain_ZAP_DOMAIN );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 11, const_str_plain_CONNECT_RID ); Py_INCREF( const_str_plain_CONNECT_RID );
    const_str_plain_GSSAPI_PRINCIPAL = UNSTREAM_STRING_ASCII( &constant_bin[ 5847523 ], 16, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 12, const_str_plain_GSSAPI_PRINCIPAL ); Py_INCREF( const_str_plain_GSSAPI_PRINCIPAL );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 13, const_str_plain_GSSAPI_SERVICE_PRINCIPAL ); Py_INCREF( const_str_plain_GSSAPI_SERVICE_PRINCIPAL );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 14, const_str_plain_SOCKS_PROXY ); Py_INCREF( const_str_plain_SOCKS_PROXY );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 15, const_str_plain_XPUB_WELCOME_MSG ); Py_INCREF( const_str_plain_XPUB_WELCOME_MSG );
    const_str_plain_ROUTING_ID = UNSTREAM_STRING_ASCII( &constant_bin[ 5847702 ], 10, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 16, const_str_plain_ROUTING_ID ); Py_INCREF( const_str_plain_ROUTING_ID );
    const_str_plain_CONNECT_ROUTING_ID = UNSTREAM_STRING_ASCII( &constant_bin[ 5847714 ], 18, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 17, const_str_plain_CONNECT_ROUTING_ID ); Py_INCREF( const_str_plain_CONNECT_ROUTING_ID );
    const_str_plain_BINDTODEVICE = UNSTREAM_STRING_ASCII( &constant_bin[ 5847903 ], 12, 1 );
    PyList_SET_ITEM( const_list_595fec3f25e3937ebb153fce699edca7_list, 18, const_str_plain_BINDTODEVICE ); Py_INCREF( const_str_plain_BINDTODEVICE );
    const_dict_295e83e36d21d2865b5d167c5055ba21 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5849057 ], 539 );
    const_list_4b77c782b15c0ee0e7de3f00550f63d7_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5849596 ], 861 );
    const_list_str_plain_MORE_str_plain_SRCFD_str_plain_SHARED_list = PyList_New( 3 );
    const_str_plain_MORE = UNSTREAM_STRING_ASCII( &constant_bin[ 67975 ], 4, 1 );
    PyList_SET_ITEM( const_list_str_plain_MORE_str_plain_SRCFD_str_plain_SHARED_list, 0, const_str_plain_MORE ); Py_INCREF( const_str_plain_MORE );
    PyList_SET_ITEM( const_list_str_plain_MORE_str_plain_SRCFD_str_plain_SHARED_list, 1, const_str_plain_SRCFD ); Py_INCREF( const_str_plain_SRCFD );
    const_str_plain_SHARED = UNSTREAM_STRING_ASCII( &constant_bin[ 5847633 ], 6, 1 );
    PyList_SET_ITEM( const_list_str_plain_MORE_str_plain_SRCFD_str_plain_SHARED_list, 2, const_str_plain_SHARED ); Py_INCREF( const_str_plain_SHARED );
    const_str_plain_no_prefix = UNSTREAM_STRING_ASCII( &constant_bin[ 5850457 ], 9, 1 );
    const_list_03046f42639e5f33779fffca82a9f62c_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5850466 ], 1995 );
    const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list = PyList_New( 9 );
    const_str_plain_AFFINITY = UNSTREAM_STRING_ASCII( &constant_bin[ 5847775 ], 8, 1 );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 0, const_str_plain_AFFINITY ); Py_INCREF( const_str_plain_AFFINITY );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 1, const_str_plain_MAXMSGSIZE ); Py_INCREF( const_str_plain_MAXMSGSIZE );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 2, const_str_plain_HWM ); Py_INCREF( const_str_plain_HWM );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 3, const_str_plain_SWAP ); Py_INCREF( const_str_plain_SWAP );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 4, const_str_plain_MCAST_LOOP ); Py_INCREF( const_str_plain_MCAST_LOOP );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 5, const_str_plain_RECOVERY_IVL_MSEC ); Py_INCREF( const_str_plain_RECOVERY_IVL_MSEC );
    const_str_plain_VMCI_BUFFER_SIZE = UNSTREAM_STRING_ASCII( &constant_bin[ 5849330 ], 16, 1 );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 6, const_str_plain_VMCI_BUFFER_SIZE ); Py_INCREF( const_str_plain_VMCI_BUFFER_SIZE );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 7, const_str_plain_VMCI_BUFFER_MIN_SIZE ); Py_INCREF( const_str_plain_VMCI_BUFFER_MIN_SIZE );
    PyList_SET_ITEM( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list, 8, const_str_plain_VMCI_BUFFER_MAX_SIZE ); Py_INCREF( const_str_plain_VMCI_BUFFER_MAX_SIZE );
    const_str_digest_b768ec0e9a42563696ab2d9edc0a3111 = UNSTREAM_STRING_ASCII( &constant_bin[ 5852461 ], 27, 0 );
    const_str_plain_RCVBUF = UNSTREAM_STRING_ASCII( &constant_bin[ 5852488 ], 6, 1 );
    const_str_plain_THREAD_AFFINITY_CPU_REMOVE = UNSTREAM_STRING_ASCII( &constant_bin[ 5847793 ], 26, 1 );
    const_list_449dda691be93df6c2c896c2f6ca2ca0_list = PyList_New( 5 );
    PyList_SET_ITEM( const_list_449dda691be93df6c2c896c2f6ca2ca0_list, 0, const_str_plain_RATE ); Py_INCREF( const_str_plain_RATE );
    const_str_plain_RECOVERY_IVL = UNSTREAM_STRING_ASCII( &constant_bin[ 5848905 ], 12, 1 );
    PyList_SET_ITEM( const_list_449dda691be93df6c2c896c2f6ca2ca0_list, 1, const_str_plain_RECOVERY_IVL ); Py_INCREF( const_str_plain_RECOVERY_IVL );
    PyList_SET_ITEM( const_list_449dda691be93df6c2c896c2f6ca2ca0_list, 2, const_str_plain_SNDBUF ); Py_INCREF( const_str_plain_SNDBUF );
    PyList_SET_ITEM( const_list_449dda691be93df6c2c896c2f6ca2ca0_list, 3, const_str_plain_RCVBUF ); Py_INCREF( const_str_plain_RCVBUF );
    PyList_SET_ITEM( const_list_449dda691be93df6c2c896c2f6ca2ca0_list, 4, const_str_plain_RCVMORE ); Py_INCREF( const_str_plain_RCVMORE );
    const_str_plain_IO_THREADS = UNSTREAM_STRING_ASCII( &constant_bin[ 5846557 ], 10, 1 );
    const_str_digest_dd79ed315912baf3fc3b7866166d495f = UNSTREAM_STRING_ASCII( &constant_bin[ 5852494 ], 43, 0 );
    const_str_plain_SOCKET_LIMIT = UNSTREAM_STRING_ASCII( &constant_bin[ 5847325 ], 12, 1 );
    const_str_plain_THREAD_PRIORITY = UNSTREAM_STRING_ASCII( &constant_bin[ 5847339 ], 15, 1 );
    const_str_plain_draft_in = UNSTREAM_STRING_ASCII( &constant_bin[ 5852537 ], 8, 1 );
    const_str_digest_36cda4dfd208c336c466e40f2d596663 = UNSTREAM_STRING_ASCII( &constant_bin[ 5852545 ], 33, 0 );
    const_str_plain_removed_in = UNSTREAM_STRING_ASCII( &constant_bin[ 5852578 ], 10, 1 );
    const_str_plain_all_names = UNSTREAM_STRING_ASCII( &constant_bin[ 5852588 ], 9, 1 );
    const_str_plain_MSG_T_SIZE = UNSTREAM_STRING_ASCII( &constant_bin[ 5847756 ], 10, 1 );
    const_tuple_str_plain_EVENT_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_EVENT_tuple, 0, const_str_plain_EVENT ); Py_INCREF( const_str_plain_EVENT );
    const_str_plain_BLOCKY = UNSTREAM_STRING_ASCII( &constant_bin[ 5849135 ], 6, 1 );
    const_list_a71a9688c352a23265b556e41f338e95_list = PyList_New( 10 );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 0, const_str_plain_IO_THREADS ); Py_INCREF( const_str_plain_IO_THREADS );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 1, const_str_plain_MAX_SOCKETS ); Py_INCREF( const_str_plain_MAX_SOCKETS );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 2, const_str_plain_SOCKET_LIMIT ); Py_INCREF( const_str_plain_SOCKET_LIMIT );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 3, const_str_plain_THREAD_PRIORITY ); Py_INCREF( const_str_plain_THREAD_PRIORITY );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 4, const_str_plain_THREAD_SCHED_POLICY ); Py_INCREF( const_str_plain_THREAD_SCHED_POLICY );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 5, const_str_plain_BLOCKY ); Py_INCREF( const_str_plain_BLOCKY );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 6, const_str_plain_MSG_T_SIZE ); Py_INCREF( const_str_plain_MSG_T_SIZE );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 7, const_str_plain_THREAD_AFFINITY_CPU_ADD ); Py_INCREF( const_str_plain_THREAD_AFFINITY_CPU_ADD );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 8, const_str_plain_THREAD_AFFINITY_CPU_REMOVE ); Py_INCREF( const_str_plain_THREAD_AFFINITY_CPU_REMOVE );
    PyList_SET_ITEM( const_list_a71a9688c352a23265b556e41f338e95_list, 9, const_str_plain_THREAD_NAME_PREFIX ); Py_INCREF( const_str_plain_THREAD_NAME_PREFIX );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_zmq$utils$constant_names( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_6e0041e142b32c90d305b2c79db15e45;
static PyCodeObject *codeobj_3222ded109efad773b9320beccd6ff9a;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_b768ec0e9a42563696ab2d9edc0a3111 );
    codeobj_6e0041e142b32c90d305b2c79db15e45 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_36cda4dfd208c336c466e40f2d596663, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_3222ded109efad773b9320beccd6ff9a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_no_prefix, 547, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_zmq$utils$constant_names$$$function_1_no_prefix(  );


// The module function definitions.
static PyObject *impl_zmq$utils$constant_names$$$function_1_no_prefix( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3222ded109efad773b9320beccd6ff9a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_3222ded109efad773b9320beccd6ff9a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3222ded109efad773b9320beccd6ff9a, codeobj_3222ded109efad773b9320beccd6ff9a, module_zmq$utils$constant_names, sizeof(void *) );
    frame_3222ded109efad773b9320beccd6ff9a = cache_frame_3222ded109efad773b9320beccd6ff9a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3222ded109efad773b9320beccd6ff9a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3222ded109efad773b9320beccd6ff9a ) == 2 ); // Frame stack

    // Framed code:
    {
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_name );
        tmp_called_instance_1 = par_name;
        frame_3222ded109efad773b9320beccd6ff9a->m_frame.f_lineno = 549;
        tmp_and_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_E_tuple, 0 ) );

        if ( tmp_and_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 549;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_and_left_value_1 );

            exception_lineno = 549;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        Py_DECREF( tmp_and_left_value_1 );
        CHECK_OBJECT( par_name );
        tmp_called_instance_2 = par_name;
        frame_3222ded109efad773b9320beccd6ff9a->m_frame.f_lineno = 549;
        tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_EVENT_tuple, 0 ) );

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 549;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 549;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_and_right_value_1 );
        tmp_return_value = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_return_value = tmp_and_left_value_1;
        and_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3222ded109efad773b9320beccd6ff9a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3222ded109efad773b9320beccd6ff9a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3222ded109efad773b9320beccd6ff9a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3222ded109efad773b9320beccd6ff9a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3222ded109efad773b9320beccd6ff9a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3222ded109efad773b9320beccd6ff9a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3222ded109efad773b9320beccd6ff9a,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_3222ded109efad773b9320beccd6ff9a == cache_frame_3222ded109efad773b9320beccd6ff9a )
    {
        Py_DECREF( frame_3222ded109efad773b9320beccd6ff9a );
    }
    cache_frame_3222ded109efad773b9320beccd6ff9a = NULL;

    assertFrameObject( frame_3222ded109efad773b9320beccd6ff9a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( zmq$utils$constant_names$$$function_1_no_prefix );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$utils$constant_names$$$function_1_no_prefix );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_zmq$utils$constant_names$$$function_1_no_prefix(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$utils$constant_names$$$function_1_no_prefix,
        const_str_plain_no_prefix,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3222ded109efad773b9320beccd6ff9a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$utils$constant_names,
        const_str_digest_dd79ed315912baf3fc3b7866166d495f,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_zmq$utils$constant_names =
{
    PyModuleDef_HEAD_INIT,
    "zmq.utils.constant_names",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(zmq$utils$constant_names)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(zmq$utils$constant_names)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_zmq$utils$constant_names );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("zmq.utils.constant_names: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.utils.constant_names: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.utils.constant_names: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initzmq$utils$constant_names" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_zmq$utils$constant_names = Py_InitModule4(
        "zmq.utils.constant_names",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_zmq$utils$constant_names = PyModule_Create( &mdef_zmq$utils$constant_names );
#endif

    moduledict_zmq$utils$constant_names = MODULE_DICT( module_zmq$utils$constant_names );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_zmq$utils$constant_names,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_zmq$utils$constant_names,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$utils$constant_names,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$utils$constant_names,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_zmq$utils$constant_names );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_d4b76186abce19777779ec7f28c5ac91, module_zmq$utils$constant_names );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_6e0041e142b32c90d305b2c79db15e45;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_a62ac28eb174428a502692dd12109d04;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_6e0041e142b32c90d305b2c79db15e45 = MAKE_MODULE_FRAME( codeobj_6e0041e142b32c90d305b2c79db15e45, module_zmq$utils$constant_names );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_6e0041e142b32c90d305b2c79db15e45 );
    assert( Py_REFCNT( frame_6e0041e142b32c90d305b2c79db15e45 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = DEEP_COPY( const_dict_2a5f51e5c7751b46e45cff87b85ac6dd );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_new_in, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = DEEP_COPY( const_dict_295e83e36d21d2865b5d167c5055ba21 );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_draft_in, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = DEEP_COPY( const_dict_417adb9ca677d2b9372ce267faa88805 );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_removed_in, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = LIST_COPY( const_list_03046f42639e5f33779fffca82a9f62c_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_base_names, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = LIST_COPY( const_list_0169c4815ac7b665fc6a3eb97ddb5c7a_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_int64_sockopt_names, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = LIST_COPY( const_list_595fec3f25e3937ebb153fce699edca7_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_bytes_sockopt_names, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = LIST_COPY( const_list_str_plain_FD_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_fd_sockopt_names, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = LIST_COPY( const_list_4b77c782b15c0ee0e7de3f00550f63d7_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_int_sockopt_names, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = LIST_COPY( const_list_449dda691be93df6c2c896c2f6ca2ca0_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_switched_sockopt_names, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = LIST_COPY( const_list_a71a9688c352a23265b556e41f338e95_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_ctx_opt_names, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = LIST_COPY( const_list_str_plain_MORE_str_plain_SRCFD_str_plain_SHARED_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_msg_opt_names, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_itertools;
        tmp_globals_name_1 = (PyObject *)moduledict_zmq$utils$constant_names;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_chain_tuple;
        tmp_level_name_1 = const_int_0;
        frame_6e0041e142b32c90d305b2c79db15e45->m_frame.f_lineno = 532;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_chain );
        Py_DECREF( tmp_import_name_from_1 );
        assert( !(tmp_assign_source_15 == NULL) );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_chain, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_chain );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_chain );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_base_names );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_base_names );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "base_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 535;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_ctx_opt_names );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ctx_opt_names );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ctx_opt_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 536;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_5;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_bytes_sockopt_names );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_bytes_sockopt_names );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "bytes_sockopt_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 537;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_fd_sockopt_names );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fd_sockopt_names );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fd_sockopt_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 538;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = tmp_mvar_value_7;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_int_sockopt_names );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_int_sockopt_names );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "int_sockopt_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 539;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = tmp_mvar_value_8;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_int64_sockopt_names );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_int64_sockopt_names );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "int64_sockopt_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 540;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = tmp_mvar_value_9;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_switched_sockopt_names );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_switched_sockopt_names );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "switched_sockopt_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 541;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_7 = tmp_mvar_value_10;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_msg_opt_names );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_msg_opt_names );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "msg_opt_names" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 542;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_8 = tmp_mvar_value_11;
        frame_6e0041e142b32c90d305b2c79db15e45->m_frame.f_lineno = 534;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_list_arg_1 = CALL_FUNCTION_WITH_ARGS8( tmp_called_name_1, call_args );
        }

        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 534;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 534;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_all_names, tmp_assign_source_16 );
    }
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_zmq$utils$constant_names, const_str_plain_chain );
    tmp_result = tmp_res != -1;
    if ( tmp_result == false ) CLEAR_ERROR_OCCURRED();

    if ( tmp_result == false )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "chain" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 545;

        goto frame_exception_exit_1;
    }


    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e0041e142b32c90d305b2c79db15e45 );
#endif
    popFrameStack();

    assertFrameObject( frame_6e0041e142b32c90d305b2c79db15e45 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e0041e142b32c90d305b2c79db15e45 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6e0041e142b32c90d305b2c79db15e45, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6e0041e142b32c90d305b2c79db15e45->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6e0041e142b32c90d305b2c79db15e45, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_zmq$utils$constant_names$$$function_1_no_prefix(  );



        UPDATE_STRING_DICT1( moduledict_zmq$utils$constant_names, (Nuitka_StringObject *)const_str_plain_no_prefix, tmp_assign_source_17 );
    }

    return MOD_RETURN_VALUE( module_zmq$utils$constant_names );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
