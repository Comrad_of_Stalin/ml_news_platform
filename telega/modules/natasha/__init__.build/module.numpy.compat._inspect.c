/* Generated code for Python module 'numpy.compat._inspect'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_numpy$compat$_inspect" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_numpy$compat$_inspect;
PyDictObject *moduledict_numpy$compat$_inspect;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_seq;
static PyObject *const_str_digest_646e9f70cdc4cf0e166d36348a351236;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_FunctionType;
extern PyObject *const_str_plain_ismethod;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_object_tuple;
extern PyObject *const_str_plain_arg;
extern PyObject *const_str_plain_args;
extern PyObject *const_tuple_str_plain_value_tuple;
static PyObject *const_tuple_str_plain_arg_str_plain_convert_str_plain_join_tuple;
extern PyObject *const_str_plain_division;
extern PyObject *const_str_plain___func__;
extern PyObject *const_str_chr_42;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_chr_61;
static PyObject *const_str_digest_a5e0ce60474936ff4fe637b73deeb379;
extern PyObject *const_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e;
extern PyObject *const_str_plain_func;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_names;
extern PyObject *const_str_plain_frame;
static PyObject *const_tuple_dd3d4d153967fe55781383c1bab538af_tuple;
extern PyObject *const_str_plain_absolute_import;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_getargs;
static PyObject *const_str_digest_129f1cb01608b00bbebfdfa52e622686;
static PyObject *const_str_digest_0500385763188f3380fbe7c0e156318f;
static PyObject *const_str_digest_84dcb9bb4d3bb2857ada9b3cbe0dd2e2;
extern PyObject *const_str_chr_41;
extern PyObject *const_str_plain_str;
static PyObject *const_tuple_str_plain__o_str_plain_convert_str_plain_join_tuple;
static PyObject *const_str_digest_a83aceb7bb1610b07bb4aa0d5929759c;
static PyObject *const_tuple_int_pos_1_int_pos_2_int_pos_4_int_pos_8_tuple;
extern PyObject *const_str_plain_value;
static PyObject *const_str_digest_08edfc13a2e2e7251c3e657f02188f75;
static PyObject *const_tuple_d81c714c4770cf9b4d31a953e3a0428d_tuple;
extern PyObject *const_str_plain_formatargspec;
static PyObject *const_str_plain_firstdefault;
extern PyObject *const_str_plain_f_locals;
extern PyObject *const_int_pos_8;
extern PyObject *const_str_plain_convert;
static PyObject *const_str_plain_CO_OPTIMIZED;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_strseq;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_nargs;
extern PyObject *const_str_plain_locals;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_e29b2dd797324911c09e946ecfd020d8;
static PyObject *const_str_plain_CO_NEWLOCALS;
static PyObject *const_str_plain_formatvarkw;
extern PyObject *const_str_plain_varargs;
extern PyObject *const_slice_none_int_pos_1_none;
extern PyObject *const_str_plain___all__;
static PyObject *const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_plain_specs;
static PyObject *const_str_digest_c5951d7d116bc43321514308414ba3c7;
extern PyObject *const_int_0;
static PyObject *const_tuple_str_plain_object_str_plain_convert_str_plain_join_tuple;
static PyObject *const_tuple_1ee26fc031b0d600be25e753551a53ce_tuple;
extern PyObject *const_str_plain_co_argcount;
static PyObject *const_list_str_empty_str_dot_list;
extern PyObject *const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
extern PyObject *const_str_plain_co;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_CO_VARKEYWORDS;
static PyObject *const_str_plain_getargvalues;
extern PyObject *const_str_plain_defaults;
static PyObject *const_str_plain_formatargvalues;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain___code__;
static PyObject *const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple;
extern PyObject *const_str_chr_40;
extern PyObject *const_int_pos_4;
static PyObject *const_str_digest_afb9b5755cfaa44302e8c9eb75122936;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_list_type_list_type_tuple_list;
extern PyObject *const_str_plain_getargspec;
static PyObject *const_str_plain_joinseq;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_print_function;
static PyObject *const_str_digest_79ac642d7b8dc6ee710acf11ddcacbc4;
extern PyObject *const_str_plain___defaults__;
extern PyObject *const_str_plain_isfunction;
extern PyObject *const_tuple_str_plain_seq_tuple;
static PyObject *const_str_plain_formatarg;
static PyObject *const_str_digest_23aae3e33a9cf4084fea333e12b542d2;
static PyObject *const_str_digest_cf2c145d088ddeb3529db52075c4768d;
static PyObject *const_str_digest_5daabe3920a0716da0b22cf88170d111;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_varkw;
extern PyObject *const_str_plain_tuple;
extern PyObject *const_str_digest_e997aebb3079207099b441c0b6d4d9fd;
static PyObject *const_str_digest_7440cab895a5199c1ff18e9db8abea12;
extern PyObject *const_str_plain_types;
extern PyObject *const_str_plain_f_code;
static PyObject *const_str_plain_formatvalue;
extern PyObject *const_tuple_str_plain_name_tuple;
extern PyObject *const_str_plain_co_varnames;
extern PyObject *const_str_plain_MethodType;
extern PyObject *const_str_digest_fac02364b0dd00de8633426a14f325ac;
static PyObject *const_list_str_plain_getargspec_str_plain_formatargspec_list;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_iscode;
static PyObject *const_str_digest_cbb05eef038de4e41ffeff9e47641835;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain__o;
extern PyObject *const_str_plain_CodeType;
extern PyObject *const_str_plain_co_flags;
extern PyObject *const_str_empty;
static PyObject *const_tuple_35b3912cea0c1609f6cc6043ff8c8fef_tuple;
extern PyObject *const_str_plain_spec;
static PyObject *const_str_plain_formatvarargs;
static PyObject *const_str_plain_CO_VARARGS;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_646e9f70cdc4cf0e166d36348a351236 = UNSTREAM_STRING_ASCII( &constant_bin[ 2849901 ], 42, 0 );
    const_tuple_str_plain_arg_str_plain_convert_str_plain_join_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_arg_str_plain_convert_str_plain_join_tuple, 0, const_str_plain_arg ); Py_INCREF( const_str_plain_arg );
    PyTuple_SET_ITEM( const_tuple_str_plain_arg_str_plain_convert_str_plain_join_tuple, 1, const_str_plain_convert ); Py_INCREF( const_str_plain_convert );
    PyTuple_SET_ITEM( const_tuple_str_plain_arg_str_plain_convert_str_plain_join_tuple, 2, const_str_plain_join ); Py_INCREF( const_str_plain_join );
    const_str_digest_a5e0ce60474936ff4fe637b73deeb379 = UNSTREAM_STRING_ASCII( &constant_bin[ 2849943 ], 30, 0 );
    const_tuple_dd3d4d153967fe55781383c1bab538af_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 1, const_str_plain_varargs ); Py_INCREF( const_str_plain_varargs );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 2, const_str_plain_varkw ); Py_INCREF( const_str_plain_varkw );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 3, const_str_plain_locals ); Py_INCREF( const_str_plain_locals );
    const_str_plain_formatarg = UNSTREAM_STRING_ASCII( &constant_bin[ 2849973 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 4, const_str_plain_formatarg ); Py_INCREF( const_str_plain_formatarg );
    const_str_plain_formatvarargs = UNSTREAM_STRING_ASCII( &constant_bin[ 2849982 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 5, const_str_plain_formatvarargs ); Py_INCREF( const_str_plain_formatvarargs );
    const_str_plain_formatvarkw = UNSTREAM_STRING_ASCII( &constant_bin[ 2849995 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 6, const_str_plain_formatvarkw ); Py_INCREF( const_str_plain_formatvarkw );
    const_str_plain_formatvalue = UNSTREAM_STRING_ASCII( &constant_bin[ 2850006 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 7, const_str_plain_formatvalue ); Py_INCREF( const_str_plain_formatvalue );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 8, const_str_plain_join ); Py_INCREF( const_str_plain_join );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 9, const_str_plain_convert ); Py_INCREF( const_str_plain_convert );
    PyTuple_SET_ITEM( const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 10, const_str_plain_specs ); Py_INCREF( const_str_plain_specs );
    const_str_digest_129f1cb01608b00bbebfdfa52e622686 = UNSTREAM_STRING_ASCII( &constant_bin[ 2850017 ], 284, 0 );
    const_str_digest_0500385763188f3380fbe7c0e156318f = UNSTREAM_STRING_ASCII( &constant_bin[ 2850301 ], 353, 0 );
    const_str_digest_84dcb9bb4d3bb2857ada9b3cbe0dd2e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2850654 ], 365, 0 );
    const_tuple_str_plain__o_str_plain_convert_str_plain_join_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain__o_str_plain_convert_str_plain_join_tuple, 0, const_str_plain__o ); Py_INCREF( const_str_plain__o );
    PyTuple_SET_ITEM( const_tuple_str_plain__o_str_plain_convert_str_plain_join_tuple, 1, const_str_plain_convert ); Py_INCREF( const_str_plain_convert );
    PyTuple_SET_ITEM( const_tuple_str_plain__o_str_plain_convert_str_plain_join_tuple, 2, const_str_plain_join ); Py_INCREF( const_str_plain_join );
    const_str_digest_a83aceb7bb1610b07bb4aa0d5929759c = UNSTREAM_STRING_ASCII( &constant_bin[ 2851019 ], 519, 0 );
    const_tuple_int_pos_1_int_pos_2_int_pos_4_int_pos_8_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_int_pos_1_int_pos_2_int_pos_4_int_pos_8_tuple, 0, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_1_int_pos_2_int_pos_4_int_pos_8_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_1_int_pos_2_int_pos_4_int_pos_8_tuple, 2, const_int_pos_4 ); Py_INCREF( const_int_pos_4 );
    PyTuple_SET_ITEM( const_tuple_int_pos_1_int_pos_2_int_pos_4_int_pos_8_tuple, 3, const_int_pos_8 ); Py_INCREF( const_int_pos_8 );
    const_str_digest_08edfc13a2e2e7251c3e657f02188f75 = UNSTREAM_STRING_ASCII( &constant_bin[ 2851538 ], 24, 0 );
    const_tuple_d81c714c4770cf9b4d31a953e3a0428d_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_d81c714c4770cf9b4d31a953e3a0428d_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_d81c714c4770cf9b4d31a953e3a0428d_tuple, 1, const_str_plain_locals ); Py_INCREF( const_str_plain_locals );
    PyTuple_SET_ITEM( const_tuple_d81c714c4770cf9b4d31a953e3a0428d_tuple, 2, const_str_plain_formatarg ); Py_INCREF( const_str_plain_formatarg );
    PyTuple_SET_ITEM( const_tuple_d81c714c4770cf9b4d31a953e3a0428d_tuple, 3, const_str_plain_formatvalue ); Py_INCREF( const_str_plain_formatvalue );
    const_str_plain_firstdefault = UNSTREAM_STRING_ASCII( &constant_bin[ 2851562 ], 12, 1 );
    const_str_plain_CO_OPTIMIZED = UNSTREAM_STRING_ASCII( &constant_bin[ 2851574 ], 12, 1 );
    const_str_plain_strseq = UNSTREAM_STRING_ASCII( &constant_bin[ 2851586 ], 6, 1 );
    const_str_digest_e29b2dd797324911c09e946ecfd020d8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2851592 ], 899, 0 );
    const_str_plain_CO_NEWLOCALS = UNSTREAM_STRING_ASCII( &constant_bin[ 2852491 ], 12, 1 );
    const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 0, const_str_plain_co ); Py_INCREF( const_str_plain_co );
    PyTuple_SET_ITEM( const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 1, const_str_plain_nargs ); Py_INCREF( const_str_plain_nargs );
    PyTuple_SET_ITEM( const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 2, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 3, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 4, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 5, const_str_plain_varargs ); Py_INCREF( const_str_plain_varargs );
    PyTuple_SET_ITEM( const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 6, const_str_plain_varkw ); Py_INCREF( const_str_plain_varkw );
    const_str_digest_c5951d7d116bc43321514308414ba3c7 = UNSTREAM_STRING_ASCII( &constant_bin[ 2852503 ], 28, 0 );
    const_tuple_str_plain_object_str_plain_convert_str_plain_join_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_object_str_plain_convert_str_plain_join_tuple, 0, const_str_plain_object ); Py_INCREF( const_str_plain_object );
    PyTuple_SET_ITEM( const_tuple_str_plain_object_str_plain_convert_str_plain_join_tuple, 1, const_str_plain_convert ); Py_INCREF( const_str_plain_convert );
    PyTuple_SET_ITEM( const_tuple_str_plain_object_str_plain_convert_str_plain_join_tuple, 2, const_str_plain_join ); Py_INCREF( const_str_plain_join );
    const_tuple_1ee26fc031b0d600be25e753551a53ce_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_1ee26fc031b0d600be25e753551a53ce_tuple, 0, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_1ee26fc031b0d600be25e753551a53ce_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_1ee26fc031b0d600be25e753551a53ce_tuple, 2, const_str_plain_varargs ); Py_INCREF( const_str_plain_varargs );
    PyTuple_SET_ITEM( const_tuple_1ee26fc031b0d600be25e753551a53ce_tuple, 3, const_str_plain_varkw ); Py_INCREF( const_str_plain_varkw );
    const_list_str_empty_str_dot_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_empty_str_dot_list, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyList_SET_ITEM( const_list_str_empty_str_dot_list, 1, const_str_dot ); Py_INCREF( const_str_dot );
    const_str_plain_CO_VARKEYWORDS = UNSTREAM_STRING_ASCII( &constant_bin[ 2852531 ], 14, 1 );
    const_str_plain_getargvalues = UNSTREAM_STRING_ASCII( &constant_bin[ 2852545 ], 12, 1 );
    const_str_plain_formatargvalues = UNSTREAM_STRING_ASCII( &constant_bin[ 2852557 ], 15, 1 );
    const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple = PyTuple_New( 13 );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 1, const_str_plain_varargs ); Py_INCREF( const_str_plain_varargs );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 2, const_str_plain_varkw ); Py_INCREF( const_str_plain_varkw );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 3, const_str_plain_defaults ); Py_INCREF( const_str_plain_defaults );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 4, const_str_plain_formatarg ); Py_INCREF( const_str_plain_formatarg );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 5, const_str_plain_formatvarargs ); Py_INCREF( const_str_plain_formatvarargs );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 6, const_str_plain_formatvarkw ); Py_INCREF( const_str_plain_formatvarkw );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 7, const_str_plain_formatvalue ); Py_INCREF( const_str_plain_formatvalue );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 8, const_str_plain_join ); Py_INCREF( const_str_plain_join );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 9, const_str_plain_specs ); Py_INCREF( const_str_plain_specs );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 10, const_str_plain_firstdefault ); Py_INCREF( const_str_plain_firstdefault );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 11, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 12, const_str_plain_spec ); Py_INCREF( const_str_plain_spec );
    const_str_digest_afb9b5755cfaa44302e8c9eb75122936 = UNSTREAM_STRING_ASCII( &constant_bin[ 2852572 ], 32, 0 );
    const_str_plain_joinseq = UNSTREAM_STRING_ASCII( &constant_bin[ 2852604 ], 7, 1 );
    const_str_digest_79ac642d7b8dc6ee710acf11ddcacbc4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2852611 ], 432, 0 );
    const_str_digest_23aae3e33a9cf4084fea333e12b542d2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2853043 ], 61, 0 );
    const_str_digest_cf2c145d088ddeb3529db52075c4768d = UNSTREAM_STRING_ASCII( &constant_bin[ 2853104 ], 221, 0 );
    const_str_digest_5daabe3920a0716da0b22cf88170d111 = UNSTREAM_STRING_ASCII( &constant_bin[ 2853325 ], 365, 0 );
    const_str_digest_7440cab895a5199c1ff18e9db8abea12 = UNSTREAM_STRING_ASCII( &constant_bin[ 2853690 ], 24, 0 );
    const_list_str_plain_getargspec_str_plain_formatargspec_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_getargspec_str_plain_formatargspec_list, 0, const_str_plain_getargspec ); Py_INCREF( const_str_plain_getargspec );
    PyList_SET_ITEM( const_list_str_plain_getargspec_str_plain_formatargspec_list, 1, const_str_plain_formatargspec ); Py_INCREF( const_str_plain_formatargspec );
    const_str_digest_cbb05eef038de4e41ffeff9e47641835 = UNSTREAM_STRING_ASCII( &constant_bin[ 2853714 ], 364, 0 );
    const_tuple_35b3912cea0c1609f6cc6043ff8c8fef_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_35b3912cea0c1609f6cc6043ff8c8fef_tuple, 0, const_str_plain_frame ); Py_INCREF( const_str_plain_frame );
    PyTuple_SET_ITEM( const_tuple_35b3912cea0c1609f6cc6043ff8c8fef_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_35b3912cea0c1609f6cc6043ff8c8fef_tuple, 2, const_str_plain_varargs ); Py_INCREF( const_str_plain_varargs );
    PyTuple_SET_ITEM( const_tuple_35b3912cea0c1609f6cc6043ff8c8fef_tuple, 3, const_str_plain_varkw ); Py_INCREF( const_str_plain_varkw );
    const_str_plain_CO_VARARGS = UNSTREAM_STRING_ASCII( &constant_bin[ 2854078 ], 10, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_numpy$compat$_inspect( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_87d75cc746da5b79698dcdc623c1f0b6;
static PyCodeObject *codeobj_27527367320174f1cf9ca141eaf070c0;
static PyCodeObject *codeobj_5ac82fe02d3d45f0f549fd51a6aec7e5;
static PyCodeObject *codeobj_ce22ed3a5527fbc2b09c0b5bf66dff9b;
static PyCodeObject *codeobj_02f134968e1e5dff47acd0a9a7ffa4ed;
static PyCodeObject *codeobj_0743f43bed3f0746ed9b04b8caf721aa;
static PyCodeObject *codeobj_69ca82376070df8456da156019afc5e4;
static PyCodeObject *codeobj_234b0701f205dc5eb7004de53709d763;
static PyCodeObject *codeobj_46b3696902cc52fb087f2deae1eea536;
static PyCodeObject *codeobj_00f517dc4c47c176b87469a8688bc1cd;
static PyCodeObject *codeobj_abbc4ee1cbee4e33e8d1adf9be474f65;
static PyCodeObject *codeobj_27feeae07ac304d1089f1d3194c3da7b;
static PyCodeObject *codeobj_d6c4ba68f5667a6a61797850d399633e;
static PyCodeObject *codeobj_a9cb69eb82671a7eacbf8232da27e165;
static PyCodeObject *codeobj_337b6f4b3dc610925a8178d102b8884b;
static PyCodeObject *codeobj_45ab897d9c275aecf4e43ef83f73763c;
static PyCodeObject *codeobj_d25fd477f8210159e6e68d5305403270;
static PyCodeObject *codeobj_08d638aebf9395a1c2cdee14e9c8ec6a;
static PyCodeObject *codeobj_21d26dce8ec88880e5f6b494314bf6fb;
static PyCodeObject *codeobj_2ce0b7b2b7353e40357e1c2cd7ec0cc0;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_08edfc13a2e2e7251c3e657f02188f75 );
    codeobj_87d75cc746da5b79698dcdc623c1f0b6 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 144, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_27527367320174f1cf9ca141eaf070c0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 145, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5ac82fe02d3d45f0f549fd51a6aec7e5 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 172, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ce22ed3a5527fbc2b09c0b5bf66dff9b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 173, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_02f134968e1e5dff47acd0a9a7ffa4ed = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 146, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0743f43bed3f0746ed9b04b8caf721aa = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 174, const_tuple_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_69ca82376070df8456da156019afc5e4 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 138, const_tuple_str_plain__o_str_plain_convert_str_plain_join_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_234b0701f205dc5eb7004de53709d763 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 187, const_tuple_str_plain_arg_str_plain_convert_str_plain_join_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_46b3696902cc52fb087f2deae1eea536 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_a5e0ce60474936ff4fe637b73deeb379, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_00f517dc4c47c176b87469a8688bc1cd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_convert, 184, const_tuple_d81c714c4770cf9b4d31a953e3a0428d_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_abbc4ee1cbee4e33e8d1adf9be474f65 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_formatargspec, 142, const_tuple_c58dea6e6f1c7cad938b9b136f1c6074_tuple, 9, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_27feeae07ac304d1089f1d3194c3da7b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_formatargvalues, 170, const_tuple_dd3d4d153967fe55781383c1bab538af_tuple, 9, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d6c4ba68f5667a6a61797850d399633e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_getargs, 67, const_tuple_b55c6f07694f2d5315bc77662f9ae4b4_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a9cb69eb82671a7eacbf8232da27e165 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_getargspec, 98, const_tuple_1ee26fc031b0d600be25e753551a53ce_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_337b6f4b3dc610925a8178d102b8884b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_getargvalues, 115, const_tuple_35b3912cea0c1609f6cc6043ff8c8fef_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_45ab897d9c275aecf4e43ef83f73763c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_iscode, 43, const_tuple_str_plain_object_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d25fd477f8210159e6e68d5305403270 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_isfunction, 28, const_tuple_str_plain_object_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_08d638aebf9395a1c2cdee14e9c8ec6a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ismethod, 15, const_tuple_str_plain_object_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_21d26dce8ec88880e5f6b494314bf6fb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_joinseq, 127, const_tuple_str_plain_seq_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ce0b7b2b7353e40357e1c2cd7ec0cc0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_strseq, 133, const_tuple_str_plain_object_str_plain_convert_str_plain_join_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_10_lambda(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_11_lambda(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_12_lambda(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_13_formatargvalues( PyObject *defaults );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_13_formatargvalues$$$function_1_convert( PyObject *defaults );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_14_lambda(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_15_lambda(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_16_lambda(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_1_ismethod(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_2_isfunction(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_3_iscode(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_4_getargs(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_5_getargspec(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_6_getargvalues(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_7_joinseq(  );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_8_strseq( PyObject *defaults );


static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_9_formatargspec( PyObject *defaults );


// The module function definitions.
static PyObject *impl_numpy$compat$_inspect$$$function_1_ismethod( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_object = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_08d638aebf9395a1c2cdee14e9c8ec6a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_08d638aebf9395a1c2cdee14e9c8ec6a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_08d638aebf9395a1c2cdee14e9c8ec6a, codeobj_08d638aebf9395a1c2cdee14e9c8ec6a, module_numpy$compat$_inspect, sizeof(void *) );
    frame_08d638aebf9395a1c2cdee14e9c8ec6a = cache_frame_08d638aebf9395a1c2cdee14e9c8ec6a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_08d638aebf9395a1c2cdee14e9c8ec6a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_08d638aebf9395a1c2cdee14e9c8ec6a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_object );
        tmp_isinstance_inst_1 = par_object;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_types );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_types );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "types" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 26;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_MethodType );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08d638aebf9395a1c2cdee14e9c8ec6a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_08d638aebf9395a1c2cdee14e9c8ec6a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08d638aebf9395a1c2cdee14e9c8ec6a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_08d638aebf9395a1c2cdee14e9c8ec6a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_08d638aebf9395a1c2cdee14e9c8ec6a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_08d638aebf9395a1c2cdee14e9c8ec6a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_08d638aebf9395a1c2cdee14e9c8ec6a,
        type_description_1,
        par_object
    );


    // Release cached frame.
    if ( frame_08d638aebf9395a1c2cdee14e9c8ec6a == cache_frame_08d638aebf9395a1c2cdee14e9c8ec6a )
    {
        Py_DECREF( frame_08d638aebf9395a1c2cdee14e9c8ec6a );
    }
    cache_frame_08d638aebf9395a1c2cdee14e9c8ec6a = NULL;

    assertFrameObject( frame_08d638aebf9395a1c2cdee14e9c8ec6a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_1_ismethod );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_1_ismethod );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_2_isfunction( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_object = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d25fd477f8210159e6e68d5305403270;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_d25fd477f8210159e6e68d5305403270 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d25fd477f8210159e6e68d5305403270, codeobj_d25fd477f8210159e6e68d5305403270, module_numpy$compat$_inspect, sizeof(void *) );
    frame_d25fd477f8210159e6e68d5305403270 = cache_frame_d25fd477f8210159e6e68d5305403270;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d25fd477f8210159e6e68d5305403270 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d25fd477f8210159e6e68d5305403270 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_object );
        tmp_isinstance_inst_1 = par_object;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_types );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_types );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "types" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_FunctionType );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d25fd477f8210159e6e68d5305403270 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d25fd477f8210159e6e68d5305403270 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d25fd477f8210159e6e68d5305403270 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d25fd477f8210159e6e68d5305403270, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d25fd477f8210159e6e68d5305403270->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d25fd477f8210159e6e68d5305403270, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d25fd477f8210159e6e68d5305403270,
        type_description_1,
        par_object
    );


    // Release cached frame.
    if ( frame_d25fd477f8210159e6e68d5305403270 == cache_frame_d25fd477f8210159e6e68d5305403270 )
    {
        Py_DECREF( frame_d25fd477f8210159e6e68d5305403270 );
    }
    cache_frame_d25fd477f8210159e6e68d5305403270 = NULL;

    assertFrameObject( frame_d25fd477f8210159e6e68d5305403270 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_2_isfunction );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_2_isfunction );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_3_iscode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_object = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_45ab897d9c275aecf4e43ef83f73763c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_45ab897d9c275aecf4e43ef83f73763c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_45ab897d9c275aecf4e43ef83f73763c, codeobj_45ab897d9c275aecf4e43ef83f73763c, module_numpy$compat$_inspect, sizeof(void *) );
    frame_45ab897d9c275aecf4e43ef83f73763c = cache_frame_45ab897d9c275aecf4e43ef83f73763c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_45ab897d9c275aecf4e43ef83f73763c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_45ab897d9c275aecf4e43ef83f73763c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_object );
        tmp_isinstance_inst_1 = par_object;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_types );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_types );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "types" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_CodeType );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_45ab897d9c275aecf4e43ef83f73763c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_45ab897d9c275aecf4e43ef83f73763c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_45ab897d9c275aecf4e43ef83f73763c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_45ab897d9c275aecf4e43ef83f73763c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_45ab897d9c275aecf4e43ef83f73763c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_45ab897d9c275aecf4e43ef83f73763c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_45ab897d9c275aecf4e43ef83f73763c,
        type_description_1,
        par_object
    );


    // Release cached frame.
    if ( frame_45ab897d9c275aecf4e43ef83f73763c == cache_frame_45ab897d9c275aecf4e43ef83f73763c )
    {
        Py_DECREF( frame_45ab897d9c275aecf4e43ef83f73763c );
    }
    cache_frame_45ab897d9c275aecf4e43ef83f73763c = NULL;

    assertFrameObject( frame_45ab897d9c275aecf4e43ef83f73763c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_3_iscode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_3_iscode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_4_getargs( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_co = python_pars[ 0 ];
    PyObject *var_nargs = NULL;
    PyObject *var_names = NULL;
    PyObject *var_args = NULL;
    PyObject *var_i = NULL;
    PyObject *var_varargs = NULL;
    PyObject *var_varkw = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_d6c4ba68f5667a6a61797850d399633e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_d6c4ba68f5667a6a61797850d399633e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d6c4ba68f5667a6a61797850d399633e, codeobj_d6c4ba68f5667a6a61797850d399633e, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d6c4ba68f5667a6a61797850d399633e = cache_frame_d6c4ba68f5667a6a61797850d399633e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d6c4ba68f5667a6a61797850d399633e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d6c4ba68f5667a6a61797850d399633e ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_iscode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iscode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iscode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_co );
        tmp_args_element_name_1 = par_co;
        frame_d6c4ba68f5667a6a61797850d399633e->m_frame.f_lineno = 76;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_7440cab895a5199c1ff18e9db8abea12;
            frame_d6c4ba68f5667a6a61797850d399633e->m_frame.f_lineno = 77;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 77;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_co );
        tmp_source_name_1 = par_co;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_co_argcount );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nargs == NULL );
        var_nargs = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_co );
        tmp_source_name_2 = par_co;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_co_varnames );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_names == NULL );
        var_names = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_step_name_1;
        CHECK_OBJECT( var_names );
        tmp_subscribed_name_1 = var_names;
        tmp_start_name_1 = Py_None;
        CHECK_OBJECT( var_nargs );
        tmp_stop_name_1 = var_nargs;
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_list_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_args == NULL );
        var_args = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_xrange_low_1;
        CHECK_OBJECT( var_nargs );
        tmp_xrange_low_1 = var_nargs;
        tmp_iter_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 86;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_6 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_6;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_subscript_name_3;
        CHECK_OBJECT( var_args );
        tmp_subscribed_name_3 = var_args;
        CHECK_OBJECT( var_i );
        tmp_subscript_name_2 = var_i;
        tmp_subscribed_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_2 );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_subscript_name_3 = const_slice_none_int_pos_1_none;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_3 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_1 = LIST_COPY( const_list_str_empty_str_dot_list );
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_make_exception_arg_2;
            tmp_make_exception_arg_2 = const_str_digest_646e9f70cdc4cf0e166d36348a351236;
            frame_d6c4ba68f5667a6a61797850d399633e->m_frame.f_lineno = 88;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_2 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            assert( !(tmp_raise_type_2 == NULL) );
            exception_type = tmp_raise_type_2;
            exception_lineno = 88;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 86;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = Py_None;
        assert( var_varargs == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_varargs = tmp_assign_source_7;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_op_bitand_res_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_co );
        tmp_source_name_3 = par_co;
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_co_flags );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_CO_VARARGS );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CO_VARARGS );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CO_VARARGS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 90;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_1 = tmp_mvar_value_2;
        tmp_op_bitand_res_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_op_bitand_res_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_op_bitand_res_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_op_bitand_res_1 );

            exception_lineno = 90;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_op_bitand_res_1 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_source_name_4;
            PyObject *tmp_subscript_name_4;
            CHECK_OBJECT( par_co );
            tmp_source_name_4 = par_co;
            tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_co_varnames );
            if ( tmp_subscribed_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_nargs );
            tmp_subscript_name_4 = var_nargs;
            tmp_assign_source_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            Py_DECREF( tmp_subscribed_name_4 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_varargs;
                assert( old != NULL );
                var_varargs = tmp_assign_source_8;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( var_nargs );
            tmp_left_name_2 = var_nargs;
            tmp_right_name_2 = const_int_pos_1;
            tmp_assign_source_9 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_nargs;
                assert( old != NULL );
                var_nargs = tmp_assign_source_9;
                Py_DECREF( old );
            }

        }
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = Py_None;
        assert( var_varkw == NULL );
        Py_INCREF( tmp_assign_source_10 );
        var_varkw = tmp_assign_source_10;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_left_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_right_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_op_bitand_res_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_co );
        tmp_source_name_5 = par_co;
        tmp_left_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_co_flags );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_CO_VARKEYWORDS );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CO_VARKEYWORDS );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_left_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CO_VARKEYWORDS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_3 = tmp_mvar_value_3;
        tmp_op_bitand_res_2 = BINARY_OPERATION( PyNumber_And, tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_3 );
        if ( tmp_op_bitand_res_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_op_bitand_res_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_op_bitand_res_2 );

            exception_lineno = 94;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_op_bitand_res_2 );
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_source_name_6;
            PyObject *tmp_subscript_name_5;
            CHECK_OBJECT( par_co );
            tmp_source_name_6 = par_co;
            tmp_subscribed_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_co_varnames );
            if ( tmp_subscribed_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 95;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_nargs );
            tmp_subscript_name_5 = var_nargs;
            tmp_assign_source_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            Py_DECREF( tmp_subscribed_name_5 );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 95;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_varkw;
                assert( old != NULL );
                var_varkw = tmp_assign_source_11;
                Py_DECREF( old );
            }

        }
        branch_no_4:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d6c4ba68f5667a6a61797850d399633e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d6c4ba68f5667a6a61797850d399633e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d6c4ba68f5667a6a61797850d399633e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d6c4ba68f5667a6a61797850d399633e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d6c4ba68f5667a6a61797850d399633e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d6c4ba68f5667a6a61797850d399633e,
        type_description_1,
        par_co,
        var_nargs,
        var_names,
        var_args,
        var_i,
        var_varargs,
        var_varkw
    );


    // Release cached frame.
    if ( frame_d6c4ba68f5667a6a61797850d399633e == cache_frame_d6c4ba68f5667a6a61797850d399633e )
    {
        Py_DECREF( frame_d6c4ba68f5667a6a61797850d399633e );
    }
    cache_frame_d6c4ba68f5667a6a61797850d399633e = NULL;

    assertFrameObject( frame_d6c4ba68f5667a6a61797850d399633e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( var_args );
        tmp_tuple_element_1 = var_args;
        tmp_return_value = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_varargs );
        tmp_tuple_element_1 = var_varargs;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_varkw );
        tmp_tuple_element_1 = var_varkw;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_1 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_4_getargs );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_co );
    Py_DECREF( par_co );
    par_co = NULL;

    CHECK_OBJECT( (PyObject *)var_nargs );
    Py_DECREF( var_nargs );
    var_nargs = NULL;

    CHECK_OBJECT( (PyObject *)var_names );
    Py_DECREF( var_names );
    var_names = NULL;

    CHECK_OBJECT( (PyObject *)var_args );
    Py_DECREF( var_args );
    var_args = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    CHECK_OBJECT( (PyObject *)var_varargs );
    Py_DECREF( var_varargs );
    var_varargs = NULL;

    CHECK_OBJECT( (PyObject *)var_varkw );
    Py_DECREF( var_varkw );
    var_varkw = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_co );
    Py_DECREF( par_co );
    par_co = NULL;

    Py_XDECREF( var_nargs );
    var_nargs = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_varargs );
    var_varargs = NULL;

    Py_XDECREF( var_varkw );
    var_varkw = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_4_getargs );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_5_getargspec( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_func = python_pars[ 0 ];
    PyObject *var_args = NULL;
    PyObject *var_varargs = NULL;
    PyObject *var_varkw = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_a9cb69eb82671a7eacbf8232da27e165;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a9cb69eb82671a7eacbf8232da27e165 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a9cb69eb82671a7eacbf8232da27e165, codeobj_a9cb69eb82671a7eacbf8232da27e165, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a9cb69eb82671a7eacbf8232da27e165 = cache_frame_a9cb69eb82671a7eacbf8232da27e165;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a9cb69eb82671a7eacbf8232da27e165 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a9cb69eb82671a7eacbf8232da27e165 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_ismethod );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ismethod );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ismethod" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_func );
        tmp_args_element_name_1 = par_func;
        frame_a9cb69eb82671a7eacbf8232da27e165->m_frame.f_lineno = 108;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 108;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( par_func );
            tmp_source_name_1 = par_func;
            tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___func__ );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_func;
                assert( old != NULL );
                par_func = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_isfunction );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_isfunction );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "isfunction" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_func );
        tmp_args_element_name_2 = par_func;
        frame_a9cb69eb82671a7eacbf8232da27e165->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_c5951d7d116bc43321514308414ba3c7;
            frame_a9cb69eb82671a7eacbf8232da27e165->m_frame.f_lineno = 111;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 111;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_getargs );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_getargs );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "getargs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( par_func );
        tmp_source_name_2 = par_func;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___code__ );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_a9cb69eb82671a7eacbf8232da27e165->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 112;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 112;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 112;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_3 == NULL );
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooo";
                    exception_lineno = 112;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooo";
            exception_lineno = 112;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
        assert( var_args == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_args = tmp_assign_source_6;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
        assert( var_varargs == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_varargs = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_3;
        assert( var_varkw == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_varkw = tmp_assign_source_8;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_args );
        tmp_tuple_element_1 = var_args;
        tmp_return_value = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_varargs );
        tmp_tuple_element_1 = var_varargs;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_varkw );
        tmp_tuple_element_1 = var_varkw;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_func );
        tmp_source_name_3 = par_func;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___defaults__ );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 113;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_return_value, 3, tmp_tuple_element_1 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9cb69eb82671a7eacbf8232da27e165 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9cb69eb82671a7eacbf8232da27e165 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9cb69eb82671a7eacbf8232da27e165 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a9cb69eb82671a7eacbf8232da27e165, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a9cb69eb82671a7eacbf8232da27e165->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a9cb69eb82671a7eacbf8232da27e165, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a9cb69eb82671a7eacbf8232da27e165,
        type_description_1,
        par_func,
        var_args,
        var_varargs,
        var_varkw
    );


    // Release cached frame.
    if ( frame_a9cb69eb82671a7eacbf8232da27e165 == cache_frame_a9cb69eb82671a7eacbf8232da27e165 )
    {
        Py_DECREF( frame_a9cb69eb82671a7eacbf8232da27e165 );
    }
    cache_frame_a9cb69eb82671a7eacbf8232da27e165 = NULL;

    assertFrameObject( frame_a9cb69eb82671a7eacbf8232da27e165 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_5_getargspec );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)var_args );
    Py_DECREF( var_args );
    var_args = NULL;

    CHECK_OBJECT( (PyObject *)var_varargs );
    Py_DECREF( var_varargs );
    var_varargs = NULL;

    CHECK_OBJECT( (PyObject *)var_varkw );
    Py_DECREF( var_varkw );
    var_varkw = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    Py_XDECREF( var_varargs );
    var_varargs = NULL;

    Py_XDECREF( var_varkw );
    var_varkw = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_5_getargspec );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_6_getargvalues( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_frame = python_pars[ 0 ];
    PyObject *var_args = NULL;
    PyObject *var_varargs = NULL;
    PyObject *var_varkw = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_337b6f4b3dc610925a8178d102b8884b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_337b6f4b3dc610925a8178d102b8884b = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_337b6f4b3dc610925a8178d102b8884b, codeobj_337b6f4b3dc610925a8178d102b8884b, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_337b6f4b3dc610925a8178d102b8884b = cache_frame_337b6f4b3dc610925a8178d102b8884b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_337b6f4b3dc610925a8178d102b8884b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_337b6f4b3dc610925a8178d102b8884b ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_getargs );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_getargs );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "getargs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_frame );
        tmp_source_name_1 = par_frame;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_f_code );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_337b6f4b3dc610925a8178d102b8884b->m_frame.f_lineno = 124;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 124;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 124;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 124;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_3 == NULL );
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooo";
                    exception_lineno = 124;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooo";
            exception_lineno = 124;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;
        assert( var_args == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_args = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;
        assert( var_varargs == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_varargs = tmp_assign_source_6;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_3;
        assert( var_varkw == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_varkw = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_args );
        tmp_tuple_element_1 = var_args;
        tmp_return_value = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_varargs );
        tmp_tuple_element_1 = var_varargs;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_varkw );
        tmp_tuple_element_1 = var_varkw;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_frame );
        tmp_source_name_2 = par_frame;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_f_locals );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 125;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_return_value, 3, tmp_tuple_element_1 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_337b6f4b3dc610925a8178d102b8884b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_337b6f4b3dc610925a8178d102b8884b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_337b6f4b3dc610925a8178d102b8884b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_337b6f4b3dc610925a8178d102b8884b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_337b6f4b3dc610925a8178d102b8884b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_337b6f4b3dc610925a8178d102b8884b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_337b6f4b3dc610925a8178d102b8884b,
        type_description_1,
        par_frame,
        var_args,
        var_varargs,
        var_varkw
    );


    // Release cached frame.
    if ( frame_337b6f4b3dc610925a8178d102b8884b == cache_frame_337b6f4b3dc610925a8178d102b8884b )
    {
        Py_DECREF( frame_337b6f4b3dc610925a8178d102b8884b );
    }
    cache_frame_337b6f4b3dc610925a8178d102b8884b = NULL;

    assertFrameObject( frame_337b6f4b3dc610925a8178d102b8884b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_6_getargvalues );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_frame );
    Py_DECREF( par_frame );
    par_frame = NULL;

    CHECK_OBJECT( (PyObject *)var_args );
    Py_DECREF( var_args );
    var_args = NULL;

    CHECK_OBJECT( (PyObject *)var_varargs );
    Py_DECREF( var_varargs );
    var_varargs = NULL;

    CHECK_OBJECT( (PyObject *)var_varkw );
    Py_DECREF( var_varkw );
    var_varkw = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_frame );
    Py_DECREF( par_frame );
    par_frame = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    Py_XDECREF( var_varargs );
    var_varargs = NULL;

    Py_XDECREF( var_varkw );
    var_varkw = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_6_getargvalues );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_7_joinseq( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_seq = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_21d26dce8ec88880e5f6b494314bf6fb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_21d26dce8ec88880e5f6b494314bf6fb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_21d26dce8ec88880e5f6b494314bf6fb, codeobj_21d26dce8ec88880e5f6b494314bf6fb, module_numpy$compat$_inspect, sizeof(void *) );
    frame_21d26dce8ec88880e5f6b494314bf6fb = cache_frame_21d26dce8ec88880e5f6b494314bf6fb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_21d26dce8ec88880e5f6b494314bf6fb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_21d26dce8ec88880e5f6b494314bf6fb ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_seq );
        tmp_len_arg_1 = par_seq;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_left_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_right_name_2;
            tmp_left_name_2 = const_str_chr_40;
            CHECK_OBJECT( par_seq );
            tmp_subscribed_name_1 = par_seq;
            tmp_subscript_name_1 = const_int_0;
            tmp_right_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_left_name_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_2, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = const_str_digest_fac02364b0dd00de8633426a14f325ac;
            tmp_return_value = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_left_name_3;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_3;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_right_name_4;
            tmp_left_name_4 = const_str_chr_40;
            tmp_called_instance_1 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
            CHECK_OBJECT( par_seq );
            tmp_args_element_name_1 = par_seq;
            frame_21d26dce8ec88880e5f6b494314bf6fb->m_frame.f_lineno = 131;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_right_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
            }

            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_left_name_3 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_4, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_left_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_right_name_4 = const_str_chr_41;
            tmp_return_value = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_3, tmp_right_name_4 );
            Py_DECREF( tmp_left_name_3 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_21d26dce8ec88880e5f6b494314bf6fb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_21d26dce8ec88880e5f6b494314bf6fb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_21d26dce8ec88880e5f6b494314bf6fb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_21d26dce8ec88880e5f6b494314bf6fb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_21d26dce8ec88880e5f6b494314bf6fb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_21d26dce8ec88880e5f6b494314bf6fb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_21d26dce8ec88880e5f6b494314bf6fb,
        type_description_1,
        par_seq
    );


    // Release cached frame.
    if ( frame_21d26dce8ec88880e5f6b494314bf6fb == cache_frame_21d26dce8ec88880e5f6b494314bf6fb )
    {
        Py_DECREF( frame_21d26dce8ec88880e5f6b494314bf6fb );
    }
    cache_frame_21d26dce8ec88880e5f6b494314bf6fb = NULL;

    assertFrameObject( frame_21d26dce8ec88880e5f6b494314bf6fb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_7_joinseq );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_seq );
    Py_DECREF( par_seq );
    par_seq = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_seq );
    Py_DECREF( par_seq );
    par_seq = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_7_joinseq );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_8_strseq( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_object = python_pars[ 0 ];
    PyObject *par_convert = python_pars[ 1 ];
    PyObject *par_join = python_pars[ 2 ];
    PyObject *outline_0_var__o = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_69ca82376070df8456da156019afc5e4_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_69ca82376070df8456da156019afc5e4_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0, codeobj_2ce0b7b2b7353e40357e1c2cd7ec0cc0, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 = cache_frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_type_arg_1;
        CHECK_OBJECT( par_object );
        tmp_type_arg_1 = par_object;
        tmp_compexpr_left_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        assert( !(tmp_compexpr_left_1 == NULL) );
        tmp_compexpr_right_1 = LIST_COPY( const_list_type_list_type_tuple_list );
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_join );
            tmp_called_name_1 = par_join;
            // Tried code:
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_iter_arg_1;
                CHECK_OBJECT( par_object );
                tmp_iter_arg_1 = par_object;
                tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }
                assert( tmp_listcomp_1__$0 == NULL );
                tmp_listcomp_1__$0 = tmp_assign_source_1;
            }
            {
                PyObject *tmp_assign_source_2;
                tmp_assign_source_2 = PyList_New( 0 );
                assert( tmp_listcomp_1__contraction == NULL );
                tmp_listcomp_1__contraction = tmp_assign_source_2;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_69ca82376070df8456da156019afc5e4_2, codeobj_69ca82376070df8456da156019afc5e4, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *) );
            frame_69ca82376070df8456da156019afc5e4_2 = cache_frame_69ca82376070df8456da156019afc5e4_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_69ca82376070df8456da156019afc5e4_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_69ca82376070df8456da156019afc5e4_2 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_1:;
            {
                PyObject *tmp_next_source_1;
                PyObject *tmp_assign_source_3;
                CHECK_OBJECT( tmp_listcomp_1__$0 );
                tmp_next_source_1 = tmp_listcomp_1__$0;
                tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_1;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "ooo";
                        exception_lineno = 138;
                        goto try_except_handler_3;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_1__iter_value_0;
                    tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_4;
                CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
                {
                    PyObject *old = outline_0_var__o;
                    outline_0_var__o = tmp_assign_source_4;
                    Py_INCREF( outline_0_var__o );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                PyObject *tmp_called_name_2;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_args_element_name_4;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_strseq );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_strseq );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "strseq" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 138;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }

                tmp_called_name_2 = tmp_mvar_value_1;
                CHECK_OBJECT( outline_0_var__o );
                tmp_args_element_name_2 = outline_0_var__o;
                CHECK_OBJECT( par_convert );
                tmp_args_element_name_3 = par_convert;
                CHECK_OBJECT( par_join );
                tmp_args_element_name_4 = par_join;
                frame_69ca82376070df8456da156019afc5e4_2->m_frame.f_lineno = 138;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                    tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
                }

                if ( tmp_append_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                Py_DECREF( tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            goto loop_start_1;
            loop_end_1:;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_args_element_name_1 = tmp_listcomp_1__contraction;
            Py_INCREF( tmp_args_element_name_1 );
            goto try_return_handler_3;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_8_strseq );
            return NULL;
            // Return handler code:
            try_return_handler_3:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            goto frame_return_exit_2;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto frame_exception_exit_2;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_69ca82376070df8456da156019afc5e4_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_return_exit_2:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_69ca82376070df8456da156019afc5e4_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_2;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_69ca82376070df8456da156019afc5e4_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_69ca82376070df8456da156019afc5e4_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_69ca82376070df8456da156019afc5e4_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_69ca82376070df8456da156019afc5e4_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_69ca82376070df8456da156019afc5e4_2,
                type_description_2,
                outline_0_var__o,
                par_convert,
                par_join
            );


            // Release cached frame.
            if ( frame_69ca82376070df8456da156019afc5e4_2 == cache_frame_69ca82376070df8456da156019afc5e4_2 )
            {
                Py_DECREF( frame_69ca82376070df8456da156019afc5e4_2 );
            }
            cache_frame_69ca82376070df8456da156019afc5e4_2 = NULL;

            assertFrameObject( frame_69ca82376070df8456da156019afc5e4_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;
            type_description_1 = "ooo";
            goto try_except_handler_2;
            skip_nested_handling_1:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_8_strseq );
            return NULL;
            // Return handler code:
            try_return_handler_2:;
            Py_XDECREF( outline_0_var__o );
            outline_0_var__o = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_0_var__o );
            outline_0_var__o = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_8_strseq );
            return NULL;
            outline_exception_1:;
            exception_lineno = 138;
            goto frame_exception_exit_1;
            outline_result_1:;
            frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0->m_frame.f_lineno = 138;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( par_convert );
            tmp_called_name_3 = par_convert;
            CHECK_OBJECT( par_object );
            tmp_args_element_name_5 = par_object;
            frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0->m_frame.f_lineno = 140;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0,
        type_description_1,
        par_object,
        par_convert,
        par_join
    );


    // Release cached frame.
    if ( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 == cache_frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 )
    {
        Py_DECREF( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 );
    }
    cache_frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 = NULL;

    assertFrameObject( frame_2ce0b7b2b7353e40357e1c2cd7ec0cc0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_8_strseq );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    CHECK_OBJECT( (PyObject *)par_convert );
    Py_DECREF( par_convert );
    par_convert = NULL;

    CHECK_OBJECT( (PyObject *)par_join );
    Py_DECREF( par_join );
    par_join = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_object );
    Py_DECREF( par_object );
    par_object = NULL;

    CHECK_OBJECT( (PyObject *)par_convert );
    Py_DECREF( par_convert );
    par_convert = NULL;

    CHECK_OBJECT( (PyObject *)par_join );
    Py_DECREF( par_join );
    par_join = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_8_strseq );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_10_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_87d75cc746da5b79698dcdc623c1f0b6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_87d75cc746da5b79698dcdc623c1f0b6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_87d75cc746da5b79698dcdc623c1f0b6, codeobj_87d75cc746da5b79698dcdc623c1f0b6, module_numpy$compat$_inspect, sizeof(void *) );
    frame_87d75cc746da5b79698dcdc623c1f0b6 = cache_frame_87d75cc746da5b79698dcdc623c1f0b6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_87d75cc746da5b79698dcdc623c1f0b6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_87d75cc746da5b79698dcdc623c1f0b6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_chr_42;
        CHECK_OBJECT( par_name );
        tmp_right_name_1 = par_name;
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_87d75cc746da5b79698dcdc623c1f0b6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_87d75cc746da5b79698dcdc623c1f0b6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_87d75cc746da5b79698dcdc623c1f0b6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_87d75cc746da5b79698dcdc623c1f0b6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_87d75cc746da5b79698dcdc623c1f0b6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_87d75cc746da5b79698dcdc623c1f0b6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_87d75cc746da5b79698dcdc623c1f0b6,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_87d75cc746da5b79698dcdc623c1f0b6 == cache_frame_87d75cc746da5b79698dcdc623c1f0b6 )
    {
        Py_DECREF( frame_87d75cc746da5b79698dcdc623c1f0b6 );
    }
    cache_frame_87d75cc746da5b79698dcdc623c1f0b6 = NULL;

    assertFrameObject( frame_87d75cc746da5b79698dcdc623c1f0b6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_10_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_10_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_11_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_27527367320174f1cf9ca141eaf070c0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_27527367320174f1cf9ca141eaf070c0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_27527367320174f1cf9ca141eaf070c0, codeobj_27527367320174f1cf9ca141eaf070c0, module_numpy$compat$_inspect, sizeof(void *) );
    frame_27527367320174f1cf9ca141eaf070c0 = cache_frame_27527367320174f1cf9ca141eaf070c0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_27527367320174f1cf9ca141eaf070c0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_27527367320174f1cf9ca141eaf070c0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e;
        CHECK_OBJECT( par_name );
        tmp_right_name_1 = par_name;
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27527367320174f1cf9ca141eaf070c0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_27527367320174f1cf9ca141eaf070c0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27527367320174f1cf9ca141eaf070c0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_27527367320174f1cf9ca141eaf070c0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_27527367320174f1cf9ca141eaf070c0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_27527367320174f1cf9ca141eaf070c0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_27527367320174f1cf9ca141eaf070c0,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_27527367320174f1cf9ca141eaf070c0 == cache_frame_27527367320174f1cf9ca141eaf070c0 )
    {
        Py_DECREF( frame_27527367320174f1cf9ca141eaf070c0 );
    }
    cache_frame_27527367320174f1cf9ca141eaf070c0 = NULL;

    assertFrameObject( frame_27527367320174f1cf9ca141eaf070c0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_11_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_11_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_12_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_02f134968e1e5dff47acd0a9a7ffa4ed;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_02f134968e1e5dff47acd0a9a7ffa4ed = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_02f134968e1e5dff47acd0a9a7ffa4ed, codeobj_02f134968e1e5dff47acd0a9a7ffa4ed, module_numpy$compat$_inspect, sizeof(void *) );
    frame_02f134968e1e5dff47acd0a9a7ffa4ed = cache_frame_02f134968e1e5dff47acd0a9a7ffa4ed;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_02f134968e1e5dff47acd0a9a7ffa4ed );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_02f134968e1e5dff47acd0a9a7ffa4ed ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_operand_name_1;
        tmp_left_name_1 = const_str_chr_61;
        CHECK_OBJECT( par_value );
        tmp_operand_name_1 = par_value;
        tmp_right_name_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02f134968e1e5dff47acd0a9a7ffa4ed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_02f134968e1e5dff47acd0a9a7ffa4ed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02f134968e1e5dff47acd0a9a7ffa4ed );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_02f134968e1e5dff47acd0a9a7ffa4ed, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_02f134968e1e5dff47acd0a9a7ffa4ed->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_02f134968e1e5dff47acd0a9a7ffa4ed, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_02f134968e1e5dff47acd0a9a7ffa4ed,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_02f134968e1e5dff47acd0a9a7ffa4ed == cache_frame_02f134968e1e5dff47acd0a9a7ffa4ed )
    {
        Py_DECREF( frame_02f134968e1e5dff47acd0a9a7ffa4ed );
    }
    cache_frame_02f134968e1e5dff47acd0a9a7ffa4ed = NULL;

    assertFrameObject( frame_02f134968e1e5dff47acd0a9a7ffa4ed );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_12_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_12_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_9_formatargspec( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_varargs = python_pars[ 1 ];
    PyObject *par_varkw = python_pars[ 2 ];
    PyObject *par_defaults = python_pars[ 3 ];
    PyObject *par_formatarg = python_pars[ 4 ];
    PyObject *par_formatvarargs = python_pars[ 5 ];
    PyObject *par_formatvarkw = python_pars[ 6 ];
    PyObject *par_formatvalue = python_pars[ 7 ];
    PyObject *par_join = python_pars[ 8 ];
    PyObject *var_specs = NULL;
    PyObject *var_firstdefault = NULL;
    PyObject *var_i = NULL;
    PyObject *var_spec = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_abbc4ee1cbee4e33e8d1adf9be474f65;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_abbc4ee1cbee4e33e8d1adf9be474f65 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_specs == NULL );
        var_specs = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_abbc4ee1cbee4e33e8d1adf9be474f65, codeobj_abbc4ee1cbee4e33e8d1adf9be474f65, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_abbc4ee1cbee4e33e8d1adf9be474f65 = cache_frame_abbc4ee1cbee4e33e8d1adf9be474f65;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_abbc4ee1cbee4e33e8d1adf9be474f65 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_abbc4ee1cbee4e33e8d1adf9be474f65 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_defaults );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_defaults );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_len_arg_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_len_arg_2;
            CHECK_OBJECT( par_args );
            tmp_len_arg_1 = par_args;
            tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 158;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_defaults );
            tmp_len_arg_2 = par_defaults;
            tmp_right_name_1 = BUILTIN_LEN( tmp_len_arg_2 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 158;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = BINARY_OPERATION_SUB_LONG_LONG( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            Py_DECREF( tmp_right_name_1 );
            assert( !(tmp_assign_source_2 == NULL) );
            assert( var_firstdefault == NULL );
            var_firstdefault = tmp_assign_source_2;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_len_arg_3;
        CHECK_OBJECT( par_args );
        tmp_len_arg_3 = par_args;
        tmp_xrange_low_1 = BUILTIN_LEN( tmp_len_arg_3 );
        if ( tmp_xrange_low_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        Py_DECREF( tmp_xrange_low_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooo";
                exception_lineno = 159;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_5;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_strseq );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_strseq );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "strseq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_args );
        tmp_subscribed_name_1 = par_args;
        CHECK_OBJECT( var_i );
        tmp_subscript_name_1 = var_i;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_formatarg );
        tmp_args_element_name_2 = par_formatarg;
        CHECK_OBJECT( par_join );
        tmp_args_element_name_3 = par_join;
        frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_spec;
            var_spec = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_defaults );
        tmp_truth_name_2 = CHECK_IF_TRUE( par_defaults );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_and_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_i );
        tmp_compexpr_left_1 = var_i;
        if ( var_firstdefault == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "firstdefault" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_compexpr_right_1 = var_firstdefault;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            CHECK_OBJECT( var_spec );
            tmp_left_name_2 = var_spec;
            CHECK_OBJECT( par_formatvalue );
            tmp_called_name_2 = par_formatvalue;
            CHECK_OBJECT( par_defaults );
            tmp_subscribed_name_2 = par_defaults;
            CHECK_OBJECT( var_i );
            tmp_left_name_3 = var_i;
            if ( var_firstdefault == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "firstdefault" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 162;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_right_name_3 = var_firstdefault;
            tmp_subscript_name_2 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
            if ( tmp_subscript_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_args_element_name_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_2;
            }
            frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 162;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_right_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_7 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_spec;
                assert( old != NULL );
                var_spec = tmp_assign_source_7;
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( var_specs );
        tmp_called_instance_1 = var_specs;
        CHECK_OBJECT( var_spec );
        tmp_args_element_name_5 = var_spec;
        frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 159;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_varargs );
        tmp_compexpr_left_2 = par_varargs;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_1;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_7;
            CHECK_OBJECT( var_specs );
            tmp_source_name_1 = var_specs;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_formatvarargs );
            tmp_called_name_4 = par_formatvarargs;
            CHECK_OBJECT( par_varargs );
            tmp_args_element_name_7 = par_varargs;
            frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 165;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 165;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 165;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( par_varkw );
        tmp_compexpr_left_3 = par_varkw;
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( var_specs );
            tmp_source_name_2 = var_specs;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_formatvarkw );
            tmp_called_name_6 = par_formatvarkw;
            CHECK_OBJECT( par_varkw );
            tmp_args_element_name_9 = par_varkw;
            frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 167;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_args_element_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 167;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 167;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_4:;
    }
    {
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_4;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_right_name_5;
        tmp_left_name_5 = const_str_chr_40;
        tmp_called_instance_2 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
        CHECK_OBJECT( var_specs );
        tmp_args_element_name_10 = var_specs;
        frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame.f_lineno = 168;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_right_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_join, call_args );
        }

        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_4 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_5, tmp_right_name_4 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_str_chr_41;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_4, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_abbc4ee1cbee4e33e8d1adf9be474f65 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_abbc4ee1cbee4e33e8d1adf9be474f65 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_abbc4ee1cbee4e33e8d1adf9be474f65 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_abbc4ee1cbee4e33e8d1adf9be474f65, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_abbc4ee1cbee4e33e8d1adf9be474f65->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_abbc4ee1cbee4e33e8d1adf9be474f65, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_abbc4ee1cbee4e33e8d1adf9be474f65,
        type_description_1,
        par_args,
        par_varargs,
        par_varkw,
        par_defaults,
        par_formatarg,
        par_formatvarargs,
        par_formatvarkw,
        par_formatvalue,
        par_join,
        var_specs,
        var_firstdefault,
        var_i,
        var_spec
    );


    // Release cached frame.
    if ( frame_abbc4ee1cbee4e33e8d1adf9be474f65 == cache_frame_abbc4ee1cbee4e33e8d1adf9be474f65 )
    {
        Py_DECREF( frame_abbc4ee1cbee4e33e8d1adf9be474f65 );
    }
    cache_frame_abbc4ee1cbee4e33e8d1adf9be474f65 = NULL;

    assertFrameObject( frame_abbc4ee1cbee4e33e8d1adf9be474f65 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_9_formatargspec );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_varargs );
    Py_DECREF( par_varargs );
    par_varargs = NULL;

    CHECK_OBJECT( (PyObject *)par_varkw );
    Py_DECREF( par_varkw );
    par_varkw = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    CHECK_OBJECT( (PyObject *)par_formatarg );
    Py_DECREF( par_formatarg );
    par_formatarg = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarargs );
    Py_DECREF( par_formatvarargs );
    par_formatvarargs = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarkw );
    Py_DECREF( par_formatvarkw );
    par_formatvarkw = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvalue );
    Py_DECREF( par_formatvalue );
    par_formatvalue = NULL;

    CHECK_OBJECT( (PyObject *)par_join );
    Py_DECREF( par_join );
    par_join = NULL;

    CHECK_OBJECT( (PyObject *)var_specs );
    Py_DECREF( var_specs );
    var_specs = NULL;

    Py_XDECREF( var_firstdefault );
    var_firstdefault = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_spec );
    var_spec = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_varargs );
    Py_DECREF( par_varargs );
    par_varargs = NULL;

    CHECK_OBJECT( (PyObject *)par_varkw );
    Py_DECREF( par_varkw );
    par_varkw = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    CHECK_OBJECT( (PyObject *)par_formatarg );
    Py_DECREF( par_formatarg );
    par_formatarg = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarargs );
    Py_DECREF( par_formatvarargs );
    par_formatvarargs = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarkw );
    Py_DECREF( par_formatvarkw );
    par_formatvarkw = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvalue );
    Py_DECREF( par_formatvalue );
    par_formatvalue = NULL;

    CHECK_OBJECT( (PyObject *)par_join );
    Py_DECREF( par_join );
    par_join = NULL;

    CHECK_OBJECT( (PyObject *)var_specs );
    Py_DECREF( var_specs );
    var_specs = NULL;

    Py_XDECREF( var_firstdefault );
    var_firstdefault = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_spec );
    var_spec = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_9_formatargspec );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_14_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5ac82fe02d3d45f0f549fd51a6aec7e5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5ac82fe02d3d45f0f549fd51a6aec7e5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5ac82fe02d3d45f0f549fd51a6aec7e5, codeobj_5ac82fe02d3d45f0f549fd51a6aec7e5, module_numpy$compat$_inspect, sizeof(void *) );
    frame_5ac82fe02d3d45f0f549fd51a6aec7e5 = cache_frame_5ac82fe02d3d45f0f549fd51a6aec7e5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_chr_42;
        CHECK_OBJECT( par_name );
        tmp_right_name_1 = par_name;
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5ac82fe02d3d45f0f549fd51a6aec7e5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5ac82fe02d3d45f0f549fd51a6aec7e5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5ac82fe02d3d45f0f549fd51a6aec7e5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5ac82fe02d3d45f0f549fd51a6aec7e5,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 == cache_frame_5ac82fe02d3d45f0f549fd51a6aec7e5 )
    {
        Py_DECREF( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 );
    }
    cache_frame_5ac82fe02d3d45f0f549fd51a6aec7e5 = NULL;

    assertFrameObject( frame_5ac82fe02d3d45f0f549fd51a6aec7e5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_14_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_14_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_15_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ce22ed3a5527fbc2b09c0b5bf66dff9b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ce22ed3a5527fbc2b09c0b5bf66dff9b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ce22ed3a5527fbc2b09c0b5bf66dff9b, codeobj_ce22ed3a5527fbc2b09c0b5bf66dff9b, module_numpy$compat$_inspect, sizeof(void *) );
    frame_ce22ed3a5527fbc2b09c0b5bf66dff9b = cache_frame_ce22ed3a5527fbc2b09c0b5bf66dff9b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e;
        CHECK_OBJECT( par_name );
        tmp_right_name_1 = par_name;
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ce22ed3a5527fbc2b09c0b5bf66dff9b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ce22ed3a5527fbc2b09c0b5bf66dff9b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ce22ed3a5527fbc2b09c0b5bf66dff9b,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b == cache_frame_ce22ed3a5527fbc2b09c0b5bf66dff9b )
    {
        Py_DECREF( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b );
    }
    cache_frame_ce22ed3a5527fbc2b09c0b5bf66dff9b = NULL;

    assertFrameObject( frame_ce22ed3a5527fbc2b09c0b5bf66dff9b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_15_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_15_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_16_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0743f43bed3f0746ed9b04b8caf721aa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0743f43bed3f0746ed9b04b8caf721aa = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0743f43bed3f0746ed9b04b8caf721aa, codeobj_0743f43bed3f0746ed9b04b8caf721aa, module_numpy$compat$_inspect, sizeof(void *) );
    frame_0743f43bed3f0746ed9b04b8caf721aa = cache_frame_0743f43bed3f0746ed9b04b8caf721aa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0743f43bed3f0746ed9b04b8caf721aa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0743f43bed3f0746ed9b04b8caf721aa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_operand_name_1;
        tmp_left_name_1 = const_str_chr_61;
        CHECK_OBJECT( par_value );
        tmp_operand_name_1 = par_value;
        tmp_right_name_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0743f43bed3f0746ed9b04b8caf721aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0743f43bed3f0746ed9b04b8caf721aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0743f43bed3f0746ed9b04b8caf721aa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0743f43bed3f0746ed9b04b8caf721aa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0743f43bed3f0746ed9b04b8caf721aa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0743f43bed3f0746ed9b04b8caf721aa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0743f43bed3f0746ed9b04b8caf721aa,
        type_description_1,
        par_value
    );


    // Release cached frame.
    if ( frame_0743f43bed3f0746ed9b04b8caf721aa == cache_frame_0743f43bed3f0746ed9b04b8caf721aa )
    {
        Py_DECREF( frame_0743f43bed3f0746ed9b04b8caf721aa );
    }
    cache_frame_0743f43bed3f0746ed9b04b8caf721aa = NULL;

    assertFrameObject( frame_0743f43bed3f0746ed9b04b8caf721aa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_16_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_16_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_13_formatargvalues( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_varargs = python_pars[ 1 ];
    PyObject *par_varkw = python_pars[ 2 ];
    PyObject *par_locals = python_pars[ 3 ];
    PyObject *par_formatarg = python_pars[ 4 ];
    PyObject *par_formatvarargs = python_pars[ 5 ];
    PyObject *par_formatvarkw = python_pars[ 6 ];
    PyObject *par_formatvalue = python_pars[ 7 ];
    PyObject *par_join = python_pars[ 8 ];
    PyObject *var_convert = NULL;
    PyObject *var_specs = NULL;
    PyObject *outline_0_var_arg = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_27feeae07ac304d1089f1d3194c3da7b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_234b0701f205dc5eb7004de53709d763_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_234b0701f205dc5eb7004de53709d763_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_27feeae07ac304d1089f1d3194c3da7b = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_defaults_1;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( par_locals );
        tmp_tuple_element_1 = par_locals;
        tmp_defaults_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_formatarg );
        tmp_tuple_element_1 = par_formatarg;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_formatvalue );
        tmp_tuple_element_1 = par_formatvalue;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 2, tmp_tuple_element_1 );
        tmp_assign_source_1 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_13_formatargvalues$$$function_1_convert( tmp_defaults_1 );



        assert( var_convert == NULL );
        var_convert = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_27feeae07ac304d1089f1d3194c3da7b, codeobj_27feeae07ac304d1089f1d3194c3da7b, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_27feeae07ac304d1089f1d3194c3da7b = cache_frame_27feeae07ac304d1089f1d3194c3da7b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_27feeae07ac304d1089f1d3194c3da7b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_27feeae07ac304d1089f1d3194c3da7b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        // Tried code:
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_args );
            tmp_iter_arg_1 = par_args;
            tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            tmp_assign_source_4 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_234b0701f205dc5eb7004de53709d763_2, codeobj_234b0701f205dc5eb7004de53709d763, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *) );
        frame_234b0701f205dc5eb7004de53709d763_2 = cache_frame_234b0701f205dc5eb7004de53709d763_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_234b0701f205dc5eb7004de53709d763_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_234b0701f205dc5eb7004de53709d763_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "ooo";
                    exception_lineno = 187;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_5;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_6 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_arg;
                outline_0_var_arg = tmp_assign_source_6;
                Py_INCREF( outline_0_var_arg );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_strseq );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_strseq );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "strseq" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 187;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( outline_0_var_arg );
            tmp_args_element_name_1 = outline_0_var_arg;
            CHECK_OBJECT( var_convert );
            tmp_args_element_name_2 = var_convert;
            CHECK_OBJECT( par_join );
            tmp_args_element_name_3 = par_join;
            frame_234b0701f205dc5eb7004de53709d763_2->m_frame.f_lineno = 187;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
            }

            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_2 = "ooo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_2 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_2 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_13_formatargvalues );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_234b0701f205dc5eb7004de53709d763_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_234b0701f205dc5eb7004de53709d763_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_234b0701f205dc5eb7004de53709d763_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_234b0701f205dc5eb7004de53709d763_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_234b0701f205dc5eb7004de53709d763_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_234b0701f205dc5eb7004de53709d763_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_234b0701f205dc5eb7004de53709d763_2,
            type_description_2,
            outline_0_var_arg,
            var_convert,
            par_join
        );


        // Release cached frame.
        if ( frame_234b0701f205dc5eb7004de53709d763_2 == cache_frame_234b0701f205dc5eb7004de53709d763_2 )
        {
            Py_DECREF( frame_234b0701f205dc5eb7004de53709d763_2 );
        }
        cache_frame_234b0701f205dc5eb7004de53709d763_2 = NULL;

        assertFrameObject( frame_234b0701f205dc5eb7004de53709d763_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_13_formatargvalues );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_arg );
        outline_0_var_arg = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_arg );
        outline_0_var_arg = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_13_formatargvalues );
        return NULL;
        outline_exception_1:;
        exception_lineno = 187;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_specs == NULL );
        var_specs = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_varargs );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_varargs );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_left_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_right_name_1;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_specs );
            tmp_source_name_1 = var_specs;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_formatvarargs );
            tmp_called_name_3 = par_formatvarargs;
            CHECK_OBJECT( par_varargs );
            tmp_args_element_name_5 = par_varargs;
            frame_27feeae07ac304d1089f1d3194c3da7b->m_frame.f_lineno = 190;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_left_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 190;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_formatvalue );
            tmp_called_name_4 = par_formatvalue;
            CHECK_OBJECT( par_locals );
            tmp_subscribed_name_1 = par_locals;
            CHECK_OBJECT( par_varargs );
            tmp_subscript_name_1 = par_varargs;
            tmp_args_element_name_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 190;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_27feeae07ac304d1089f1d3194c3da7b->m_frame.f_lineno = 190;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 190;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_4 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 190;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_27feeae07ac304d1089f1d3194c3da7b->m_frame.f_lineno = 190;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_varkw );
        tmp_truth_name_2 = CHECK_IF_TRUE( par_varkw );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_left_name_2;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( var_specs );
            tmp_source_name_2 = var_specs;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_formatvarkw );
            tmp_called_name_6 = par_formatvarkw;
            CHECK_OBJECT( par_varkw );
            tmp_args_element_name_8 = par_varkw;
            frame_27feeae07ac304d1089f1d3194c3da7b->m_frame.f_lineno = 192;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_left_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 192;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_formatvalue );
            tmp_called_name_7 = par_formatvalue;
            CHECK_OBJECT( par_locals );
            tmp_subscribed_name_2 = par_locals;
            CHECK_OBJECT( par_varkw );
            tmp_subscript_name_2 = par_varkw;
            tmp_args_element_name_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_args_element_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 192;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_27feeae07ac304d1089f1d3194c3da7b->m_frame.f_lineno = 192;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_right_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 192;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_7 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_args_element_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 192;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_27feeae07ac304d1089f1d3194c3da7b->m_frame.f_lineno = 192;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 192;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_right_name_4;
        tmp_left_name_4 = const_str_chr_40;
        tmp_called_instance_1 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
        CHECK_OBJECT( var_specs );
        tmp_args_element_name_10 = var_specs;
        frame_27feeae07ac304d1089f1d3194c3da7b->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_right_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
        }

        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_3 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_4, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_4 = const_str_chr_41;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_3, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27feeae07ac304d1089f1d3194c3da7b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_27feeae07ac304d1089f1d3194c3da7b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27feeae07ac304d1089f1d3194c3da7b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_27feeae07ac304d1089f1d3194c3da7b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_27feeae07ac304d1089f1d3194c3da7b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_27feeae07ac304d1089f1d3194c3da7b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_27feeae07ac304d1089f1d3194c3da7b,
        type_description_1,
        par_args,
        par_varargs,
        par_varkw,
        par_locals,
        par_formatarg,
        par_formatvarargs,
        par_formatvarkw,
        par_formatvalue,
        par_join,
        var_convert,
        var_specs
    );


    // Release cached frame.
    if ( frame_27feeae07ac304d1089f1d3194c3da7b == cache_frame_27feeae07ac304d1089f1d3194c3da7b )
    {
        Py_DECREF( frame_27feeae07ac304d1089f1d3194c3da7b );
    }
    cache_frame_27feeae07ac304d1089f1d3194c3da7b = NULL;

    assertFrameObject( frame_27feeae07ac304d1089f1d3194c3da7b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_13_formatargvalues );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_varargs );
    Py_DECREF( par_varargs );
    par_varargs = NULL;

    CHECK_OBJECT( (PyObject *)par_varkw );
    Py_DECREF( par_varkw );
    par_varkw = NULL;

    CHECK_OBJECT( (PyObject *)par_locals );
    Py_DECREF( par_locals );
    par_locals = NULL;

    CHECK_OBJECT( (PyObject *)par_formatarg );
    Py_DECREF( par_formatarg );
    par_formatarg = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarargs );
    Py_DECREF( par_formatvarargs );
    par_formatvarargs = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarkw );
    Py_DECREF( par_formatvarkw );
    par_formatvarkw = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvalue );
    Py_DECREF( par_formatvalue );
    par_formatvalue = NULL;

    CHECK_OBJECT( (PyObject *)par_join );
    Py_DECREF( par_join );
    par_join = NULL;

    CHECK_OBJECT( (PyObject *)var_convert );
    Py_DECREF( var_convert );
    var_convert = NULL;

    CHECK_OBJECT( (PyObject *)var_specs );
    Py_DECREF( var_specs );
    var_specs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_varargs );
    Py_DECREF( par_varargs );
    par_varargs = NULL;

    CHECK_OBJECT( (PyObject *)par_varkw );
    Py_DECREF( par_varkw );
    par_varkw = NULL;

    CHECK_OBJECT( (PyObject *)par_locals );
    Py_DECREF( par_locals );
    par_locals = NULL;

    CHECK_OBJECT( (PyObject *)par_formatarg );
    Py_DECREF( par_formatarg );
    par_formatarg = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarargs );
    Py_DECREF( par_formatvarargs );
    par_formatvarargs = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvarkw );
    Py_DECREF( par_formatvarkw );
    par_formatvarkw = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvalue );
    Py_DECREF( par_formatvalue );
    par_formatvalue = NULL;

    CHECK_OBJECT( (PyObject *)par_join );
    Py_DECREF( par_join );
    par_join = NULL;

    CHECK_OBJECT( (PyObject *)var_convert );
    Py_DECREF( var_convert );
    var_convert = NULL;

    Py_XDECREF( var_specs );
    var_specs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_13_formatargvalues );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_numpy$compat$_inspect$$$function_13_formatargvalues$$$function_1_convert( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *par_locals = python_pars[ 1 ];
    PyObject *par_formatarg = python_pars[ 2 ];
    PyObject *par_formatvalue = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_00f517dc4c47c176b87469a8688bc1cd;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_00f517dc4c47c176b87469a8688bc1cd = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_00f517dc4c47c176b87469a8688bc1cd, codeobj_00f517dc4c47c176b87469a8688bc1cd, module_numpy$compat$_inspect, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_00f517dc4c47c176b87469a8688bc1cd = cache_frame_00f517dc4c47c176b87469a8688bc1cd;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_00f517dc4c47c176b87469a8688bc1cd );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_00f517dc4c47c176b87469a8688bc1cd ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_formatarg );
        tmp_called_name_1 = par_formatarg;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_1 = par_name;
        frame_00f517dc4c47c176b87469a8688bc1cd->m_frame.f_lineno = 186;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_left_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_formatvalue );
        tmp_called_name_2 = par_formatvalue;
        CHECK_OBJECT( par_locals );
        tmp_subscribed_name_1 = par_locals;
        CHECK_OBJECT( par_name );
        tmp_subscript_name_1 = par_name;
        tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 186;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_00f517dc4c47c176b87469a8688bc1cd->m_frame.f_lineno = 186;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 186;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_00f517dc4c47c176b87469a8688bc1cd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_00f517dc4c47c176b87469a8688bc1cd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_00f517dc4c47c176b87469a8688bc1cd );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_00f517dc4c47c176b87469a8688bc1cd, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_00f517dc4c47c176b87469a8688bc1cd->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_00f517dc4c47c176b87469a8688bc1cd, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_00f517dc4c47c176b87469a8688bc1cd,
        type_description_1,
        par_name,
        par_locals,
        par_formatarg,
        par_formatvalue
    );


    // Release cached frame.
    if ( frame_00f517dc4c47c176b87469a8688bc1cd == cache_frame_00f517dc4c47c176b87469a8688bc1cd )
    {
        Py_DECREF( frame_00f517dc4c47c176b87469a8688bc1cd );
    }
    cache_frame_00f517dc4c47c176b87469a8688bc1cd = NULL;

    assertFrameObject( frame_00f517dc4c47c176b87469a8688bc1cd );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_13_formatargvalues$$$function_1_convert );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_locals );
    Py_DECREF( par_locals );
    par_locals = NULL;

    CHECK_OBJECT( (PyObject *)par_formatarg );
    Py_DECREF( par_formatarg );
    par_formatarg = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvalue );
    Py_DECREF( par_formatvalue );
    par_formatvalue = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_locals );
    Py_DECREF( par_locals );
    par_locals = NULL;

    CHECK_OBJECT( (PyObject *)par_formatarg );
    Py_DECREF( par_formatarg );
    par_formatarg = NULL;

    CHECK_OBJECT( (PyObject *)par_formatvalue );
    Py_DECREF( par_formatvalue );
    par_formatvalue = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( numpy$compat$_inspect$$$function_13_formatargvalues$$$function_1_convert );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_10_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_10_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_87d75cc746da5b79698dcdc623c1f0b6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_11_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_11_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_27527367320174f1cf9ca141eaf070c0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_12_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_12_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_02f134968e1e5dff47acd0a9a7ffa4ed,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_13_formatargvalues( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_13_formatargvalues,
        const_str_plain_formatargvalues,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_27feeae07ac304d1089f1d3194c3da7b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_cbb05eef038de4e41ffeff9e47641835,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_13_formatargvalues$$$function_1_convert( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_13_formatargvalues$$$function_1_convert,
        const_str_plain_convert,
#if PYTHON_VERSION >= 300
        const_str_digest_afb9b5755cfaa44302e8c9eb75122936,
#endif
        codeobj_00f517dc4c47c176b87469a8688bc1cd,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_14_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_14_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5ac82fe02d3d45f0f549fd51a6aec7e5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_15_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_15_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ce22ed3a5527fbc2b09c0b5bf66dff9b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_16_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_16_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0743f43bed3f0746ed9b04b8caf721aa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_1_ismethod(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_1_ismethod,
        const_str_plain_ismethod,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_08d638aebf9395a1c2cdee14e9c8ec6a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_79ac642d7b8dc6ee710acf11ddcacbc4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_2_isfunction(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_2_isfunction,
        const_str_plain_isfunction,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d25fd477f8210159e6e68d5305403270,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_a83aceb7bb1610b07bb4aa0d5929759c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_3_iscode(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_3_iscode,
        const_str_plain_iscode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_45ab897d9c275aecf4e43ef83f73763c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_e29b2dd797324911c09e946ecfd020d8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_4_getargs(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_4_getargs,
        const_str_plain_getargs,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d6c4ba68f5667a6a61797850d399633e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_129f1cb01608b00bbebfdfa52e622686,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_5_getargspec(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_5_getargspec,
        const_str_plain_getargspec,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a9cb69eb82671a7eacbf8232da27e165,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_5daabe3920a0716da0b22cf88170d111,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_6_getargvalues(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_6_getargvalues,
        const_str_plain_getargvalues,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_337b6f4b3dc610925a8178d102b8884b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_0500385763188f3380fbe7c0e156318f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_7_joinseq(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_7_joinseq,
        const_str_plain_joinseq,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_21d26dce8ec88880e5f6b494314bf6fb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_8_strseq( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_8_strseq,
        const_str_plain_strseq,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ce0b7b2b7353e40357e1c2cd7ec0cc0,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_23aae3e33a9cf4084fea333e12b542d2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_numpy$compat$_inspect$$$function_9_formatargspec( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_numpy$compat$_inspect$$$function_9_formatargspec,
        const_str_plain_formatargspec,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_abbc4ee1cbee4e33e8d1adf9be474f65,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_numpy$compat$_inspect,
        const_str_digest_84dcb9bb4d3bb2857ada9b3cbe0dd2e2,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_numpy$compat$_inspect =
{
    PyModuleDef_HEAD_INIT,
    "numpy.compat._inspect",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(numpy$compat$_inspect)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(numpy$compat$_inspect)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_numpy$compat$_inspect );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("numpy.compat._inspect: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("numpy.compat._inspect: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("numpy.compat._inspect: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnumpy$compat$_inspect" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_numpy$compat$_inspect = Py_InitModule4(
        "numpy.compat._inspect",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_numpy$compat$_inspect = PyModule_Create( &mdef_numpy$compat$_inspect );
#endif

    moduledict_numpy$compat$_inspect = MODULE_DICT( module_numpy$compat$_inspect );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_numpy$compat$_inspect,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_numpy$compat$_inspect,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_numpy$compat$_inspect,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_numpy$compat$_inspect,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_numpy$compat$_inspect );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_e997aebb3079207099b441c0b6d4d9fd, module_numpy$compat$_inspect );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__element_4 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_46b3696902cc52fb087f2deae1eea536;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_cf2c145d088ddeb3529db52075c4768d;
        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_46b3696902cc52fb087f2deae1eea536 = MAKE_MODULE_FRAME( codeobj_46b3696902cc52fb087f2deae1eea536, module_numpy$compat$_inspect );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_46b3696902cc52fb087f2deae1eea536 );
    assert( Py_REFCNT( frame_46b3696902cc52fb087f2deae1eea536 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        frame_46b3696902cc52fb087f2deae1eea536->m_frame.f_lineno = 8;
        tmp_assign_source_4 = PyImport_ImportModule("__future__");
        assert( !(tmp_assign_source_4 == NULL) );
        assert( tmp_import_from_1__module == NULL );
        Py_INCREF( tmp_assign_source_4 );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_division );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_division, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_absolute_import );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_absolute_import, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_print_function );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_types;
        tmp_globals_name_1 = (PyObject *)moduledict_numpy$compat$_inspect;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_46b3696902cc52fb087f2deae1eea536->m_frame.f_lineno = 10;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_types, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = LIST_COPY( const_list_str_plain_getargspec_str_plain_formatargspec_list );
        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_1_ismethod(  );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_ismethod, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_2_isfunction(  );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_isfunction, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_3_iscode(  );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_iscode, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_iter_arg_1;
        tmp_iter_arg_1 = const_tuple_int_pos_1_int_pos_2_int_pos_4_int_pos_8_tuple;
        tmp_assign_source_13 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        assert( !(tmp_assign_source_13 == NULL) );
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_13;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_1, 0, 4 );
        if ( tmp_assign_source_14 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 65;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_14;
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_15 = UNPACK_NEXT( tmp_unpack_2, 1, 4 );
        if ( tmp_assign_source_15 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 65;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_16 = UNPACK_NEXT( tmp_unpack_3, 2, 4 );
        if ( tmp_assign_source_16 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 65;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_3 == NULL );
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_16;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_4, 3, 4 );
        if ( tmp_assign_source_17 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 65;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_4 == NULL );
        tmp_tuple_unpack_1__element_4 = tmp_assign_source_17;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_46b3696902cc52fb087f2deae1eea536 );
#endif
    popFrameStack();

    assertFrameObject( frame_46b3696902cc52fb087f2deae1eea536 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_46b3696902cc52fb087f2deae1eea536 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_46b3696902cc52fb087f2deae1eea536, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_46b3696902cc52fb087f2deae1eea536->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_46b3696902cc52fb087f2deae1eea536, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_18;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_18 = tmp_tuple_unpack_1__element_1;
        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_CO_OPTIMIZED, tmp_assign_source_18 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_19;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_19 = tmp_tuple_unpack_1__element_2;
        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_CO_NEWLOCALS, tmp_assign_source_19 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_20;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_20 = tmp_tuple_unpack_1__element_3;
        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_CO_VARARGS, tmp_assign_source_20 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        PyObject *tmp_assign_source_21;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_4 );
        tmp_assign_source_21 = tmp_tuple_unpack_1__element_4;
        UPDATE_STRING_DICT0( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_CO_VARKEYWORDS, tmp_assign_source_21 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_4 );
    tmp_tuple_unpack_1__element_4 = NULL;

    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_4_getargs(  );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_getargs, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_5_getargspec(  );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_getargspec, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_6_getargvalues(  );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_getargvalues, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_7_joinseq(  );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_joinseq, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_defaults_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_joinseq );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_joinseq );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_defaults_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
        tmp_assign_source_26 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_8_strseq( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_strseq, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_defaults_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_mvar_value_4;
        tmp_tuple_element_2 = Py_None;
        tmp_defaults_2 = PyTuple_New( 8 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_2 );
        tmp_tuple_element_2 = Py_None;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_defaults_2, 1, tmp_tuple_element_2 );
        tmp_tuple_element_2 = Py_None;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_defaults_2, 2, tmp_tuple_element_2 );
        tmp_tuple_element_2 = (PyObject *)&PyUnicode_Type;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_defaults_2, 3, tmp_tuple_element_2 );
        tmp_tuple_element_2 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_10_lambda(  );



        PyTuple_SET_ITEM( tmp_defaults_2, 4, tmp_tuple_element_2 );
        tmp_tuple_element_2 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_11_lambda(  );



        PyTuple_SET_ITEM( tmp_defaults_2, 5, tmp_tuple_element_2 );
        tmp_tuple_element_2 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_12_lambda(  );



        PyTuple_SET_ITEM( tmp_defaults_2, 6, tmp_tuple_element_2 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_joinseq );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_joinseq );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_tuple_element_2 = tmp_mvar_value_4;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_defaults_2, 7, tmp_tuple_element_2 );
        tmp_assign_source_27 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_9_formatargspec( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_formatargspec, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_defaults_3;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_mvar_value_5;
        tmp_tuple_element_3 = (PyObject *)&PyUnicode_Type;
        tmp_defaults_3 = PyTuple_New( 5 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_defaults_3, 0, tmp_tuple_element_3 );
        tmp_tuple_element_3 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_14_lambda(  );



        PyTuple_SET_ITEM( tmp_defaults_3, 1, tmp_tuple_element_3 );
        tmp_tuple_element_3 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_15_lambda(  );



        PyTuple_SET_ITEM( tmp_defaults_3, 2, tmp_tuple_element_3 );
        tmp_tuple_element_3 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_16_lambda(  );



        PyTuple_SET_ITEM( tmp_defaults_3, 3, tmp_tuple_element_3 );
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_joinseq );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_joinseq );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_tuple_element_3 = tmp_mvar_value_5;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_defaults_3, 4, tmp_tuple_element_3 );
        tmp_assign_source_28 = MAKE_FUNCTION_numpy$compat$_inspect$$$function_13_formatargvalues( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_numpy$compat$_inspect, (Nuitka_StringObject *)const_str_plain_formatargvalues, tmp_assign_source_28 );
    }

    return MOD_RETURN_VALUE( module_numpy$compat$_inspect );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
