/* Generated code for Python module 'pygments.token'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_pygments$token" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pygments$token;
PyDictObject *moduledict_pygments$token;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_nn;
extern PyObject *const_str_plain_Whitespace;
extern PyObject *const_str_plain_nb;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_tuple_str_plain_self_str_plain_args_tuple;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_kc;
extern PyObject *const_str_plain_Special;
extern PyObject *const_str_plain_cp;
extern PyObject *const_str_plain_gs;
extern PyObject *const_str_plain_mb;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_plain_gu;
static PyObject *const_str_plain_Affix;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_Char;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_vi;
static PyObject *const_str_plain_gp;
static PyObject *const_str_plain_Inserted;
static PyObject *const_str_plain_mh;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_Property;
static PyObject *const_str_plain_Pseudo;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_cs;
extern PyObject *const_tuple_str_plain_self_str_plain_memo_tuple;
static PyObject *const_str_plain_Delimiter;
static PyObject *const_str_plain_kp;
static PyObject *const_str_plain_Multiline;
extern PyObject *const_str_plain_err;
extern PyObject *const_str_plain_Error;
extern PyObject *const_str_plain_Namespace;
static PyObject *const_str_plain_Subheading;
extern PyObject *const_tuple_str_plain_self_str_plain_val_tuple;
static PyObject *const_tuple_str_plain_ttype_str_plain_other_tuple;
static PyObject *const_str_plain_Declaration;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_plain_Word;
extern PyObject *const_str_plain_Preproc;
static PyObject *const_str_plain_Interpol;
static PyObject *const_str_digest_b542f26fd411245a226a2d091915225d;
extern PyObject *const_str_plain_Label;
extern PyObject *const_str_plain__TokenType;
extern PyObject *const_str_plain_ttype;
extern PyObject *const_str_plain_join;
static PyObject *const_str_plain_vm;
static PyObject *const_str_plain_kd;
extern PyObject *const_tuple_type_tuple_tuple;
static PyObject *const_str_plain_Double;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain_reverse;
extern PyObject *const_str_plain_sx;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_plain_Deleted;
extern PyObject *const_str_plain_Emph;
extern PyObject *const_str_plain___deepcopy__;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain_add;
static PyObject *const_str_plain_Builtin;
static PyObject *const_str_plain_si;
static PyObject *const_str_plain_is_token_subtype;
extern PyObject *const_str_plain_gt;
extern PyObject *const_str_plain_Output;
static PyObject *const_str_plain_mf;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_Reserved;
extern PyObject *const_str_plain_sr;
static PyObject *const_str_plain_Hashbang;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_node;
extern PyObject *const_str_plain_w;
extern PyObject *const_str_plain_item;
extern PyObject *const_str_plain_p;
extern PyObject *const_str_plain_g;
extern PyObject *const_str_plain_fm;
extern PyObject *const_str_plain_s1;
extern PyObject *const_str_plain_gi;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_str_plain_vg;
extern PyObject *const_str_plain_String;
extern PyObject *const_str_plain_sh;
extern PyObject *const_str_plain_Float;
extern PyObject *const_str_plain_parent;
static PyObject *const_str_digest_2ca8de4fe91dcc09eb2c7de7c3b87f15;
extern PyObject *const_str_plain_Generic;
extern PyObject *const_str_plain_Escape;
static PyObject *const_str_plain_esc;
extern PyObject *const_str_plain_kn;
static PyObject *const_str_plain_Heredoc;
static PyObject *const_str_plain_Traceback;
static PyObject *const_str_digest_7dd6e8a7f32ff05029ba00cd4965c102;
extern PyObject *const_str_plain_Token;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_str_plain_nl;
extern PyObject *const_str_plain_l;
extern PyObject *const_str_plain_Type;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_Decorator;
extern PyObject *const_str_plain_STANDARD_TYPES;
extern PyObject *const_str_plain___getattr__;
static PyObject *const_str_plain_il;
static PyObject *const_str_digest_89a304972c125e8fab3bba113cc5fe07;
extern PyObject *const_str_plain_nc;
extern PyObject *const_str_plain_ge;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_no;
extern PyObject *const_str_plain_go;
extern PyObject *const_str_plain_new;
static PyObject *const_str_plain_cpf;
extern PyObject *const_str_plain_py;
extern PyObject *const_str_plain_cm;
static PyObject *const_str_plain_PreprocFile;
extern PyObject *const_str_plain_Text;
static PyObject *const_str_plain_Prompt;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_gr;
extern PyObject *const_str_plain_se;
extern PyObject *const_str_plain_Literal;
extern PyObject *const_str_plain_sc;
static PyObject *const_str_digest_468e96c58f89dc8a6d49b703d6714a78;
extern PyObject *const_str_plain_bp;
static PyObject *const_str_plain_kr;
extern PyObject *const_str_plain_nt;
static PyObject *const_str_digest_35a299a7b7727b217bcbfd45be071b40;
static PyObject *const_str_digest_7c06797014767a9eb821b24b41410bde;
extern PyObject *const_str_plain___contains__;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_plain_Punctuation;
extern PyObject *const_str_plain_k;
extern PyObject *const_str_plain_Operator;
static PyObject *const_tuple_str_plain_self_str_plain_buf_str_plain_node_tuple;
extern PyObject *const_str_plain_ss;
static PyObject *const_tuple_str_plain_self_str_plain_val_str_plain_new_tuple;
static PyObject *const_str_plain_Backtick;
extern PyObject *const_str_plain_c1;
static PyObject *const_str_plain_ow;
static PyObject *const_str_plain_Heading;
extern PyObject *const_str_plain_buf;
extern PyObject *const_str_plain_Function;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_val;
static PyObject *const_str_digest_10b00f31e85ba3c55c79a114a57f809f;
extern PyObject *const_str_plain_string_to_tokentype;
extern PyObject *const_str_plain_Doc;
extern PyObject *const_str_plain_Strong;
extern PyObject *const_str_plain_mo;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_nd;
extern PyObject *const_str_plain_o;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_x;
static PyObject *const_str_plain_mi;
static PyObject *const_tuple_str_plain_s_str_plain_node_str_plain_item_tuple;
extern PyObject *const_str_plain_c;
extern PyObject *const_str_plain_Tag;
extern PyObject *const_str_plain_Regex;
extern PyObject *const_str_plain_Attribute;
extern PyObject *const_str_plain_Instance;
static PyObject *const_str_plain_sd;
extern PyObject *const_str_plain_s2;
static PyObject *const_str_plain_Bin;
extern PyObject *const_str_plain_Comment;
static PyObject *const_str_plain_kt;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_Date;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_ff7174a0034638263c9a5af9b3adaa4b;
static PyObject *const_str_plain_ni;
static PyObject *const_str_plain_Magic;
extern PyObject *const_str_plain_sa;
extern PyObject *const_str_plain_Integer;
extern PyObject *const_str_plain_gd;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_dl;
static PyObject *const_str_digest_1f5fb9cdd385efb9f50eee859ad83f35;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_memo;
extern PyObject *const_str_plain_Variable;
static PyObject *const_str_plain_na;
extern PyObject *const_str_plain_tuple;
extern PyObject *const_str_plain_Constant;
static PyObject *const_str_plain_Single;
extern PyObject *const_str_plain___copy__;
extern PyObject *const_str_plain_other;
extern PyObject *const_str_plain_nv;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_sb;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_Number;
extern PyObject *const_str_plain_ld;
extern PyObject *const_str_plain_nx;
extern PyObject *const_str_plain_Class;
extern PyObject *const_str_plain_Name;
extern PyObject *const_str_plain___repr__;
static PyObject *const_str_plain_Entity;
extern PyObject *const_str_plain_Other;
extern PyObject *const_str_plain_Keyword;
static PyObject *const_str_plain_subtypes;
extern PyObject *const_str_plain_Exception;
extern PyObject *const_str_digest_c574207cc2e883086040c6beb0095313;
static PyObject *const_str_plain_Symbol;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_ch;
extern PyObject *const_str_plain_isupper;
static PyObject *const_str_plain_gh;
static PyObject *const_str_plain_Global;
extern PyObject *const_str_plain_ne;
static PyObject *const_str_plain_nf;
static PyObject *const_str_digest_683072b52d73f853d80e4612c8014837;
extern PyObject *const_str_plain_Long;
extern PyObject *const_str_empty;
static PyObject *const_str_plain_Hex;
static PyObject *const_str_digest_a1019ec6f1a44f37d84f4bbb58730cbb;
extern PyObject *const_str_plain___getattribute__;
static PyObject *const_str_plain_Oct;
static PyObject *const_str_plain_vc;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_gu = UNSTREAM_STRING_ASCII( &constant_bin[ 7776 ], 2, 1 );
    const_str_plain_Affix = UNSTREAM_STRING_ASCII( &constant_bin[ 5039716 ], 5, 1 );
    const_str_plain_gp = UNSTREAM_STRING_ASCII( &constant_bin[ 113845 ], 2, 1 );
    const_str_plain_Inserted = UNSTREAM_STRING_ASCII( &constant_bin[ 3070504 ], 8, 1 );
    const_str_plain_mh = UNSTREAM_STRING_ASCII( &constant_bin[ 784655 ], 2, 1 );
    const_str_plain_Pseudo = UNSTREAM_STRING_ASCII( &constant_bin[ 3959574 ], 6, 1 );
    const_str_plain_Delimiter = UNSTREAM_STRING_ASCII( &constant_bin[ 5039721 ], 9, 1 );
    const_str_plain_kp = UNSTREAM_STRING_ASCII( &constant_bin[ 60715 ], 2, 1 );
    const_str_plain_Multiline = UNSTREAM_STRING_ASCII( &constant_bin[ 4653347 ], 9, 1 );
    const_str_plain_Subheading = UNSTREAM_STRING_ASCII( &constant_bin[ 5039730 ], 10, 1 );
    const_tuple_str_plain_ttype_str_plain_other_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ttype_str_plain_other_tuple, 0, const_str_plain_ttype ); Py_INCREF( const_str_plain_ttype );
    PyTuple_SET_ITEM( const_tuple_str_plain_ttype_str_plain_other_tuple, 1, const_str_plain_other ); Py_INCREF( const_str_plain_other );
    const_str_plain_Declaration = UNSTREAM_STRING_ASCII( &constant_bin[ 5039740 ], 11, 1 );
    const_str_plain_Interpol = UNSTREAM_STRING_ASCII( &constant_bin[ 157249 ], 8, 1 );
    const_str_digest_b542f26fd411245a226a2d091915225d = UNSTREAM_STRING_ASCII( &constant_bin[ 5039751 ], 130, 0 );
    const_str_plain_vm = UNSTREAM_STRING_ASCII( &constant_bin[ 216272 ], 2, 1 );
    const_str_plain_kd = UNSTREAM_STRING_ASCII( &constant_bin[ 574257 ], 2, 1 );
    const_str_plain_Double = UNSTREAM_STRING_ASCII( &constant_bin[ 108070 ], 6, 1 );
    const_str_plain_Deleted = UNSTREAM_STRING_ASCII( &constant_bin[ 5039881 ], 7, 1 );
    const_str_plain_Builtin = UNSTREAM_STRING_ASCII( &constant_bin[ 68047 ], 7, 1 );
    const_str_plain_si = UNSTREAM_STRING_ASCII( &constant_bin[ 1110 ], 2, 1 );
    const_str_plain_is_token_subtype = UNSTREAM_STRING_ASCII( &constant_bin[ 5039888 ], 16, 1 );
    const_str_plain_mf = UNSTREAM_STRING_ASCII( &constant_bin[ 717 ], 2, 1 );
    const_str_plain_Hashbang = UNSTREAM_STRING_ASCII( &constant_bin[ 5039904 ], 8, 1 );
    const_str_plain_vg = UNSTREAM_STRING_ASCII( &constant_bin[ 897188 ], 2, 1 );
    const_str_digest_2ca8de4fe91dcc09eb2c7de7c3b87f15 = UNSTREAM_STRING_ASCII( &constant_bin[ 4549278 ], 19, 0 );
    const_str_plain_esc = UNSTREAM_STRING_ASCII( &constant_bin[ 9957 ], 3, 1 );
    const_str_plain_Heredoc = UNSTREAM_STRING_ASCII( &constant_bin[ 5039912 ], 7, 1 );
    const_str_plain_Traceback = UNSTREAM_STRING_ASCII( &constant_bin[ 975752 ], 9, 1 );
    const_str_digest_7dd6e8a7f32ff05029ba00cd4965c102 = UNSTREAM_STRING_ASCII( &constant_bin[ 5039919 ], 19, 0 );
    const_str_plain_il = UNSTREAM_STRING_ASCII( &constant_bin[ 801 ], 2, 1 );
    const_str_digest_89a304972c125e8fab3bba113cc5fe07 = UNSTREAM_STRING_ASCII( &constant_bin[ 5039938 ], 23, 0 );
    const_str_plain_cpf = UNSTREAM_STRING_ASCII( &constant_bin[ 5039961 ], 3, 1 );
    const_str_plain_PreprocFile = UNSTREAM_STRING_ASCII( &constant_bin[ 5039964 ], 11, 1 );
    const_str_plain_Prompt = UNSTREAM_STRING_ASCII( &constant_bin[ 4601629 ], 6, 1 );
    const_str_digest_468e96c58f89dc8a6d49b703d6714a78 = UNSTREAM_STRING_ASCII( &constant_bin[ 5039975 ], 16, 0 );
    const_str_plain_kr = UNSTREAM_STRING_ASCII( &constant_bin[ 57205 ], 2, 1 );
    const_str_digest_35a299a7b7727b217bcbfd45be071b40 = UNSTREAM_STRING_ASCII( &constant_bin[ 5039991 ], 381, 0 );
    const_str_digest_7c06797014767a9eb821b24b41410bde = UNSTREAM_STRING_ASCII( &constant_bin[ 5040372 ], 22, 0 );
    const_str_plain_Punctuation = UNSTREAM_STRING_ASCII( &constant_bin[ 5040394 ], 11, 1 );
    const_tuple_str_plain_self_str_plain_buf_str_plain_node_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_buf_str_plain_node_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_buf_str_plain_node_tuple, 1, const_str_plain_buf ); Py_INCREF( const_str_plain_buf );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_buf_str_plain_node_tuple, 2, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    const_tuple_str_plain_self_str_plain_val_str_plain_new_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_val_str_plain_new_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_val_str_plain_new_tuple, 1, const_str_plain_val ); Py_INCREF( const_str_plain_val );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_val_str_plain_new_tuple, 2, const_str_plain_new ); Py_INCREF( const_str_plain_new );
    const_str_plain_Backtick = UNSTREAM_STRING_ASCII( &constant_bin[ 5040405 ], 8, 1 );
    const_str_plain_ow = UNSTREAM_STRING_ASCII( &constant_bin[ 804 ], 2, 1 );
    const_str_plain_Heading = UNSTREAM_STRING_ASCII( &constant_bin[ 5040413 ], 7, 1 );
    const_str_digest_10b00f31e85ba3c55c79a114a57f809f = UNSTREAM_STRING_ASCII( &constant_bin[ 5040420 ], 203, 0 );
    const_str_plain_mi = UNSTREAM_STRING_ASCII( &constant_bin[ 2633 ], 2, 1 );
    const_tuple_str_plain_s_str_plain_node_str_plain_item_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_s_str_plain_node_str_plain_item_tuple, 0, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_str_plain_s_str_plain_node_str_plain_item_tuple, 1, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    PyTuple_SET_ITEM( const_tuple_str_plain_s_str_plain_node_str_plain_item_tuple, 2, const_str_plain_item ); Py_INCREF( const_str_plain_item );
    const_str_plain_sd = UNSTREAM_STRING_ASCII( &constant_bin[ 600 ], 2, 1 );
    const_str_plain_Bin = UNSTREAM_STRING_ASCII( &constant_bin[ 199686 ], 3, 1 );
    const_str_plain_kt = UNSTREAM_STRING_ASCII( &constant_bin[ 3876 ], 2, 1 );
    const_str_digest_ff7174a0034638263c9a5af9b3adaa4b = UNSTREAM_STRING_ASCII( &constant_bin[ 5040623 ], 23, 0 );
    const_str_plain_ni = UNSTREAM_STRING_ASCII( &constant_bin[ 7 ], 2, 1 );
    const_str_plain_Magic = UNSTREAM_STRING_ASCII( &constant_bin[ 905515 ], 5, 1 );
    const_str_digest_1f5fb9cdd385efb9f50eee859ad83f35 = UNSTREAM_STRING_ASCII( &constant_bin[ 5040646 ], 19, 0 );
    const_str_plain_na = UNSTREAM_STRING_ASCII( &constant_bin[ 4417 ], 2, 1 );
    const_str_plain_Single = UNSTREAM_STRING_ASCII( &constant_bin[ 267131 ], 6, 1 );
    const_str_plain_Entity = UNSTREAM_STRING_ASCII( &constant_bin[ 662727 ], 6, 1 );
    const_str_plain_subtypes = UNSTREAM_STRING_ASCII( &constant_bin[ 3079191 ], 8, 1 );
    const_str_plain_Symbol = UNSTREAM_STRING_ASCII( &constant_bin[ 18222 ], 6, 1 );
    const_str_plain_gh = UNSTREAM_STRING_ASCII( &constant_bin[ 1064 ], 2, 1 );
    const_str_plain_Global = UNSTREAM_STRING_ASCII( &constant_bin[ 199102 ], 6, 1 );
    const_str_plain_nf = UNSTREAM_STRING_ASCII( &constant_bin[ 1235 ], 2, 1 );
    const_str_digest_683072b52d73f853d80e4612c8014837 = UNSTREAM_STRING_ASCII( &constant_bin[ 5040665 ], 23, 0 );
    const_str_plain_Hex = UNSTREAM_STRING_ASCII( &constant_bin[ 4549560 ], 3, 1 );
    const_str_digest_a1019ec6f1a44f37d84f4bbb58730cbb = UNSTREAM_STRING_ASCII( &constant_bin[ 5040688 ], 17, 0 );
    const_str_plain_Oct = UNSTREAM_STRING_ASCII( &constant_bin[ 12438 ], 3, 1 );
    const_str_plain_vc = UNSTREAM_STRING_ASCII( &constant_bin[ 207722 ], 2, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pygments$token( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_6d865baa791616739033b5b9c8601f86;
static PyCodeObject *codeobj_d2f80fdf8ff53b520942da8efaf174fa;
static PyCodeObject *codeobj_e1994a1cfca09fb0427ef33915476c8c;
static PyCodeObject *codeobj_46f3580c6f2717446708a36fddfca764;
static PyCodeObject *codeobj_08997db1d1461861668cdd8cccd5bd18;
static PyCodeObject *codeobj_03a19b4476edaf0e2c9b9dbd33c1d1b1;
static PyCodeObject *codeobj_01ca8b68e61735d1fe57df58dc418320;
static PyCodeObject *codeobj_bf256e3a0e73e943aabb4ce7462b1951;
static PyCodeObject *codeobj_671bff747132a6a57058c2f3d7959c80;
static PyCodeObject *codeobj_826696bec25ba72bc9f4ba5a988804e7;
static PyCodeObject *codeobj_05a3f0c99eca441b1846b45466d00bb6;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_a1019ec6f1a44f37d84f4bbb58730cbb );
    codeobj_6d865baa791616739033b5b9c8601f86 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_89a304972c125e8fab3bba113cc5fe07, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_d2f80fdf8ff53b520942da8efaf174fa = MAKE_CODEOBJ( module_filename_obj, const_str_plain__TokenType, 13, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_e1994a1cfca09fb0427ef33915476c8c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___contains__, 29, const_tuple_str_plain_self_str_plain_val_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_46f3580c6f2717446708a36fddfca764 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___copy__, 47, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_08997db1d1461861668cdd8cccd5bd18 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___deepcopy__, 51, const_tuple_str_plain_self_str_plain_memo_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_03a19b4476edaf0e2c9b9dbd33c1d1b1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___getattr__, 35, const_tuple_str_plain_self_str_plain_val_str_plain_new_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_01ca8b68e61735d1fe57df58dc418320 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 25, const_tuple_str_plain_self_str_plain_args_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
    codeobj_bf256e3a0e73e943aabb4ce7462b1951 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 44, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_671bff747132a6a57058c2f3d7959c80 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_token_subtype, 86, const_tuple_str_plain_ttype_str_plain_other_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_826696bec25ba72bc9f4ba5a988804e7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_split, 16, const_tuple_str_plain_self_str_plain_buf_str_plain_node_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_05a3f0c99eca441b1846b45466d00bb6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_string_to_tokentype, 95, const_tuple_str_plain_s_str_plain_node_str_plain_item_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_1_split(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_2___init__(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_3___contains__(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_4___getattr__(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_5___repr__(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_6___copy__(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_7___deepcopy__(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_8_is_token_subtype(  );


static PyObject *MAKE_FUNCTION_pygments$token$$$function_9_string_to_tokentype(  );


// The module function definitions.
static PyObject *impl_pygments$token$$$function_1_split( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_buf = NULL;
    PyObject *var_node = NULL;
    struct Nuitka_FrameObject *frame_826696bec25ba72bc9f4ba5a988804e7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_826696bec25ba72bc9f4ba5a988804e7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_buf == NULL );
        var_buf = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( par_self );
        tmp_assign_source_2 = par_self;
        assert( var_node == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_node = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_826696bec25ba72bc9f4ba5a988804e7, codeobj_826696bec25ba72bc9f4ba5a988804e7, module_pygments$token, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_826696bec25ba72bc9f4ba5a988804e7 = cache_frame_826696bec25ba72bc9f4ba5a988804e7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_826696bec25ba72bc9f4ba5a988804e7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_826696bec25ba72bc9f4ba5a988804e7 ) == 2 ); // Frame stack

    // Framed code:
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_node );
        tmp_compexpr_left_1 = var_node;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_buf );
        tmp_called_instance_1 = var_buf;
        CHECK_OBJECT( var_node );
        tmp_args_element_name_1 = var_node;
        frame_826696bec25ba72bc9f4ba5a988804e7->m_frame.f_lineno = 20;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_node );
        tmp_source_name_1 = var_node;
        tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parent );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_node;
            assert( old != NULL );
            var_node = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( var_buf );
        tmp_called_instance_2 = var_buf;
        frame_826696bec25ba72bc9f4ba5a988804e7->m_frame.f_lineno = 22;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_reverse );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_826696bec25ba72bc9f4ba5a988804e7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_826696bec25ba72bc9f4ba5a988804e7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_826696bec25ba72bc9f4ba5a988804e7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_826696bec25ba72bc9f4ba5a988804e7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_826696bec25ba72bc9f4ba5a988804e7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_826696bec25ba72bc9f4ba5a988804e7,
        type_description_1,
        par_self,
        var_buf,
        var_node
    );


    // Release cached frame.
    if ( frame_826696bec25ba72bc9f4ba5a988804e7 == cache_frame_826696bec25ba72bc9f4ba5a988804e7 )
    {
        Py_DECREF( frame_826696bec25ba72bc9f4ba5a988804e7 );
    }
    cache_frame_826696bec25ba72bc9f4ba5a988804e7 = NULL;

    assertFrameObject( frame_826696bec25ba72bc9f4ba5a988804e7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_buf );
    tmp_return_value = var_buf;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_1_split );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_buf );
    Py_DECREF( var_buf );
    var_buf = NULL;

    CHECK_OBJECT( (PyObject *)var_node );
    Py_DECREF( var_node );
    var_node = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_buf );
    Py_DECREF( var_buf );
    var_buf = NULL;

    CHECK_OBJECT( (PyObject *)var_node );
    Py_DECREF( var_node );
    var_node = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_1_split );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_2___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_01ca8b68e61735d1fe57df58dc418320;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_01ca8b68e61735d1fe57df58dc418320 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_01ca8b68e61735d1fe57df58dc418320, codeobj_01ca8b68e61735d1fe57df58dc418320, module_pygments$token, sizeof(void *)+sizeof(void *) );
    frame_01ca8b68e61735d1fe57df58dc418320 = cache_frame_01ca8b68e61735d1fe57df58dc418320;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_01ca8b68e61735d1fe57df58dc418320 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_01ca8b68e61735d1fe57df58dc418320 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = PySet_New( NULL );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_subtypes, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_01ca8b68e61735d1fe57df58dc418320 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_01ca8b68e61735d1fe57df58dc418320 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_01ca8b68e61735d1fe57df58dc418320, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_01ca8b68e61735d1fe57df58dc418320->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_01ca8b68e61735d1fe57df58dc418320, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_01ca8b68e61735d1fe57df58dc418320,
        type_description_1,
        par_self,
        par_args
    );


    // Release cached frame.
    if ( frame_01ca8b68e61735d1fe57df58dc418320 == cache_frame_01ca8b68e61735d1fe57df58dc418320 )
    {
        Py_DECREF( frame_01ca8b68e61735d1fe57df58dc418320 );
    }
    cache_frame_01ca8b68e61735d1fe57df58dc418320 = NULL;

    assertFrameObject( frame_01ca8b68e61735d1fe57df58dc418320 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_2___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_2___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_3___contains__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_val = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_e1994a1cfca09fb0427ef33915476c8c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e1994a1cfca09fb0427ef33915476c8c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e1994a1cfca09fb0427ef33915476c8c, codeobj_e1994a1cfca09fb0427ef33915476c8c, module_pygments$token, sizeof(void *)+sizeof(void *) );
    frame_e1994a1cfca09fb0427ef33915476c8c = cache_frame_e1994a1cfca09fb0427ef33915476c8c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e1994a1cfca09fb0427ef33915476c8c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e1994a1cfca09fb0427ef33915476c8c ) == 2 ); // Frame stack

    // Framed code:
    {
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_step_name_1;
        CHECK_OBJECT( par_self );
        tmp_compexpr_left_1 = par_self;
        CHECK_OBJECT( par_val );
        tmp_compexpr_right_1 = par_val;
        tmp_or_left_value_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? Py_True : Py_False;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_val );
        tmp_type_arg_1 = par_val;
        tmp_compexpr_left_2 = BUILTIN_TYPE1( tmp_type_arg_1 );
        assert( !(tmp_compexpr_left_2 == NULL) );
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_1 );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_2 );

            exception_lineno = 31;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? Py_True : Py_False;
        Py_DECREF( tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_val );
        tmp_subscribed_name_1 = par_val;
        tmp_start_name_1 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_len_arg_1 = par_self;
        tmp_stop_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_stop_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        Py_DECREF( tmp_stop_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_compexpr_left_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_compexpr_right_3 = par_self;
        tmp_and_right_value_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_and_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        Py_INCREF( tmp_and_left_value_1 );
        tmp_or_right_value_1 = tmp_and_left_value_1;
        and_end_1:;
        tmp_return_value = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_return_value = tmp_or_left_value_1;
        or_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1994a1cfca09fb0427ef33915476c8c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1994a1cfca09fb0427ef33915476c8c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1994a1cfca09fb0427ef33915476c8c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e1994a1cfca09fb0427ef33915476c8c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e1994a1cfca09fb0427ef33915476c8c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e1994a1cfca09fb0427ef33915476c8c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e1994a1cfca09fb0427ef33915476c8c,
        type_description_1,
        par_self,
        par_val
    );


    // Release cached frame.
    if ( frame_e1994a1cfca09fb0427ef33915476c8c == cache_frame_e1994a1cfca09fb0427ef33915476c8c )
    {
        Py_DECREF( frame_e1994a1cfca09fb0427ef33915476c8c );
    }
    cache_frame_e1994a1cfca09fb0427ef33915476c8c = NULL;

    assertFrameObject( frame_e1994a1cfca09fb0427ef33915476c8c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_3___contains__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_3___contains__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_4___getattr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_val = python_pars[ 1 ];
    PyObject *var_new = NULL;
    struct Nuitka_FrameObject *frame_03a19b4476edaf0e2c9b9dbd33c1d1b1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_03a19b4476edaf0e2c9b9dbd33c1d1b1, codeobj_03a19b4476edaf0e2c9b9dbd33c1d1b1, module_pygments$token, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 = cache_frame_03a19b4476edaf0e2c9b9dbd33c1d1b1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_val );
        tmp_operand_name_1 = par_val;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_val );
        tmp_subscribed_name_1 = par_val;
        tmp_subscript_name_1 = const_int_0;
        tmp_called_instance_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_03a19b4476edaf0e2c9b9dbd33c1d1b1->m_frame.f_lineno = 36;
        tmp_operand_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_isupper );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_called_instance_2 = (PyObject *)&PyTuple_Type;
            CHECK_OBJECT( par_self );
            tmp_args_element_name_1 = par_self;
            CHECK_OBJECT( par_val );
            tmp_args_element_name_2 = par_val;
            frame_03a19b4476edaf0e2c9b9dbd33c1d1b1->m_frame.f_lineno = 37;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain___getattribute__, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain__TokenType );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__TokenType );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_TokenType" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_left_name_1 = par_self;
        CHECK_OBJECT( par_val );
        tmp_tuple_element_1 = par_val;
        tmp_right_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        tmp_args_element_name_3 = BINARY_OPERATION_ADD_OBJECT_TUPLE( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_03a19b4476edaf0e2c9b9dbd33c1d1b1->m_frame.f_lineno = 38;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_new == NULL );
        var_new = tmp_assign_source_1;
    }
    {
        PyObject *tmp_setattr_target_1;
        PyObject *tmp_setattr_attr_1;
        PyObject *tmp_setattr_value_1;
        PyObject *tmp_capi_result_1;
        CHECK_OBJECT( par_self );
        tmp_setattr_target_1 = par_self;
        CHECK_OBJECT( par_val );
        tmp_setattr_attr_1 = par_val;
        CHECK_OBJECT( var_new );
        tmp_setattr_value_1 = var_new;
        tmp_capi_result_1 = BUILTIN_SETATTR( tmp_setattr_target_1, tmp_setattr_attr_1, tmp_setattr_value_1 );
        if ( tmp_capi_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_subtypes );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_new );
        tmp_args_element_name_4 = var_new;
        frame_03a19b4476edaf0e2c9b9dbd33c1d1b1->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_add, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_self );
        tmp_assattr_name_1 = par_self;
        CHECK_OBJECT( var_new );
        tmp_assattr_target_1 = var_new;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_parent, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_03a19b4476edaf0e2c9b9dbd33c1d1b1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_03a19b4476edaf0e2c9b9dbd33c1d1b1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_03a19b4476edaf0e2c9b9dbd33c1d1b1,
        type_description_1,
        par_self,
        par_val,
        var_new
    );


    // Release cached frame.
    if ( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 == cache_frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 )
    {
        Py_DECREF( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 );
    }
    cache_frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 = NULL;

    assertFrameObject( frame_03a19b4476edaf0e2c9b9dbd33c1d1b1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_new );
    tmp_return_value = var_new;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_4___getattr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    Py_XDECREF( var_new );
    var_new = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    Py_XDECREF( var_new );
    var_new = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_4___getattr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_5___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_bf256e3a0e73e943aabb4ce7462b1951;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bf256e3a0e73e943aabb4ce7462b1951 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bf256e3a0e73e943aabb4ce7462b1951, codeobj_bf256e3a0e73e943aabb4ce7462b1951, module_pygments$token, sizeof(void *) );
    frame_bf256e3a0e73e943aabb4ce7462b1951 = cache_frame_bf256e3a0e73e943aabb4ce7462b1951;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bf256e3a0e73e943aabb4ce7462b1951 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bf256e3a0e73e943aabb4ce7462b1951 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        tmp_left_name_2 = const_str_plain_Token;
        CHECK_OBJECT( par_self );
        tmp_and_left_value_1 = par_self;
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_and_right_value_1 = const_str_dot;
        tmp_or_left_value_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_or_left_value_1 = tmp_and_left_value_1;
        and_end_1:;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_or_right_value_1 = const_str_empty;
        tmp_right_name_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_right_name_1 = tmp_or_left_value_1;
        or_end_1:;
        tmp_left_name_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = const_str_dot;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_bf256e3a0e73e943aabb4ce7462b1951->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
        }

        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf256e3a0e73e943aabb4ce7462b1951 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf256e3a0e73e943aabb4ce7462b1951 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf256e3a0e73e943aabb4ce7462b1951 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bf256e3a0e73e943aabb4ce7462b1951, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bf256e3a0e73e943aabb4ce7462b1951->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bf256e3a0e73e943aabb4ce7462b1951, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bf256e3a0e73e943aabb4ce7462b1951,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_bf256e3a0e73e943aabb4ce7462b1951 == cache_frame_bf256e3a0e73e943aabb4ce7462b1951 )
    {
        Py_DECREF( frame_bf256e3a0e73e943aabb4ce7462b1951 );
    }
    cache_frame_bf256e3a0e73e943aabb4ce7462b1951 = NULL;

    assertFrameObject( frame_bf256e3a0e73e943aabb4ce7462b1951 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_5___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_5___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_6___copy__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_self );
    tmp_return_value = par_self;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_6___copy__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_6___copy__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_7___deepcopy__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_memo = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_self );
    tmp_return_value = par_self;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_7___deepcopy__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_memo );
    Py_DECREF( par_memo );
    par_memo = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_7___deepcopy__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_8_is_token_subtype( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_ttype = python_pars[ 0 ];
    PyObject *par_other = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_671bff747132a6a57058c2f3d7959c80;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_671bff747132a6a57058c2f3d7959c80 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_671bff747132a6a57058c2f3d7959c80, codeobj_671bff747132a6a57058c2f3d7959c80, module_pygments$token, sizeof(void *)+sizeof(void *) );
    frame_671bff747132a6a57058c2f3d7959c80 = cache_frame_671bff747132a6a57058c2f3d7959c80;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_671bff747132a6a57058c2f3d7959c80 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_671bff747132a6a57058c2f3d7959c80 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_ttype );
        tmp_compexpr_left_1 = par_ttype;
        CHECK_OBJECT( par_other );
        tmp_compexpr_right_1 = par_other;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_671bff747132a6a57058c2f3d7959c80 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_671bff747132a6a57058c2f3d7959c80 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_671bff747132a6a57058c2f3d7959c80 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_671bff747132a6a57058c2f3d7959c80, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_671bff747132a6a57058c2f3d7959c80->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_671bff747132a6a57058c2f3d7959c80, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_671bff747132a6a57058c2f3d7959c80,
        type_description_1,
        par_ttype,
        par_other
    );


    // Release cached frame.
    if ( frame_671bff747132a6a57058c2f3d7959c80 == cache_frame_671bff747132a6a57058c2f3d7959c80 )
    {
        Py_DECREF( frame_671bff747132a6a57058c2f3d7959c80 );
    }
    cache_frame_671bff747132a6a57058c2f3d7959c80 = NULL;

    assertFrameObject( frame_671bff747132a6a57058c2f3d7959c80 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_8_is_token_subtype );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_ttype );
    Py_DECREF( par_ttype );
    par_ttype = NULL;

    CHECK_OBJECT( (PyObject *)par_other );
    Py_DECREF( par_other );
    par_other = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_ttype );
    Py_DECREF( par_ttype );
    par_ttype = NULL;

    CHECK_OBJECT( (PyObject *)par_other );
    Py_DECREF( par_other );
    par_other = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_8_is_token_subtype );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$token$$$function_9_string_to_tokentype( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    PyObject *var_node = NULL;
    PyObject *var_item = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_05a3f0c99eca441b1846b45466d00bb6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_05a3f0c99eca441b1846b45466d00bb6 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_05a3f0c99eca441b1846b45466d00bb6, codeobj_05a3f0c99eca441b1846b45466d00bb6, module_pygments$token, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_05a3f0c99eca441b1846b45466d00bb6 = cache_frame_05a3f0c99eca441b1846b45466d00bb6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_05a3f0c99eca441b1846b45466d00bb6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_05a3f0c99eca441b1846b45466d00bb6 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_s );
        tmp_isinstance_inst_1 = par_s;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain__TokenType );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__TokenType );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_TokenType" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_s );
        tmp_return_value = par_s;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_s );
        tmp_operand_name_1 = par_s;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 114;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_return_value = tmp_mvar_value_2;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_1 = tmp_mvar_value_3;
        assert( var_node == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_node = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_s );
        tmp_called_instance_1 = par_s;
        frame_05a3f0c99eca441b1846b45466d00bb6->m_frame.f_lineno = 116;
        tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 116;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_item;
            var_item = tmp_assign_source_4;
            Py_INCREF( var_item );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        CHECK_OBJECT( var_node );
        tmp_getattr_target_1 = var_node;
        CHECK_OBJECT( var_item );
        tmp_getattr_attr_1 = var_item;
        tmp_assign_source_5 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_node;
            assert( old != NULL );
            var_node = tmp_assign_source_5;
            Py_DECREF( old );
        }

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 116;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_05a3f0c99eca441b1846b45466d00bb6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_05a3f0c99eca441b1846b45466d00bb6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_05a3f0c99eca441b1846b45466d00bb6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_05a3f0c99eca441b1846b45466d00bb6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_05a3f0c99eca441b1846b45466d00bb6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_05a3f0c99eca441b1846b45466d00bb6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_05a3f0c99eca441b1846b45466d00bb6,
        type_description_1,
        par_s,
        var_node,
        var_item
    );


    // Release cached frame.
    if ( frame_05a3f0c99eca441b1846b45466d00bb6 == cache_frame_05a3f0c99eca441b1846b45466d00bb6 )
    {
        Py_DECREF( frame_05a3f0c99eca441b1846b45466d00bb6 );
    }
    cache_frame_05a3f0c99eca441b1846b45466d00bb6 = NULL;

    assertFrameObject( frame_05a3f0c99eca441b1846b45466d00bb6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_node );
    tmp_return_value = var_node;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_9_string_to_tokentype );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_item );
    var_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_item );
    var_item = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$token$$$function_9_string_to_tokentype );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_1_split(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_1_split,
        const_str_plain_split,
#if PYTHON_VERSION >= 300
        const_str_digest_468e96c58f89dc8a6d49b703d6714a78,
#endif
        codeobj_826696bec25ba72bc9f4ba5a988804e7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_2___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_2___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_1f5fb9cdd385efb9f50eee859ad83f35,
#endif
        codeobj_01ca8b68e61735d1fe57df58dc418320,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_3___contains__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_3___contains__,
        const_str_plain___contains__,
#if PYTHON_VERSION >= 300
        const_str_digest_683072b52d73f853d80e4612c8014837,
#endif
        codeobj_e1994a1cfca09fb0427ef33915476c8c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_4___getattr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_4___getattr__,
        const_str_plain___getattr__,
#if PYTHON_VERSION >= 300
        const_str_digest_7c06797014767a9eb821b24b41410bde,
#endif
        codeobj_03a19b4476edaf0e2c9b9dbd33c1d1b1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_5___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_5___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_2ca8de4fe91dcc09eb2c7de7c3b87f15,
#endif
        codeobj_bf256e3a0e73e943aabb4ce7462b1951,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_6___copy__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_6___copy__,
        const_str_plain___copy__,
#if PYTHON_VERSION >= 300
        const_str_digest_7dd6e8a7f32ff05029ba00cd4965c102,
#endif
        codeobj_46f3580c6f2717446708a36fddfca764,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_7___deepcopy__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_7___deepcopy__,
        const_str_plain___deepcopy__,
#if PYTHON_VERSION >= 300
        const_str_digest_ff7174a0034638263c9a5af9b3adaa4b,
#endif
        codeobj_08997db1d1461861668cdd8cccd5bd18,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_8_is_token_subtype(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_8_is_token_subtype,
        const_str_plain_is_token_subtype,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_671bff747132a6a57058c2f3d7959c80,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        const_str_digest_b542f26fd411245a226a2d091915225d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$token$$$function_9_string_to_tokentype(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$token$$$function_9_string_to_tokentype,
        const_str_plain_string_to_tokentype,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_05a3f0c99eca441b1846b45466d00bb6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$token,
        const_str_digest_35a299a7b7727b217bcbfd45be071b40,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pygments$token =
{
    PyModuleDef_HEAD_INIT,
    "pygments.token",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(pygments$token)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(pygments$token)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pygments$token );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pygments.token: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygments.token: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygments.token: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpygments$token" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pygments$token = Py_InitModule4(
        "pygments.token",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pygments$token = PyModule_Create( &mdef_pygments$token );
#endif

    moduledict_pygments$token = MODULE_DICT( module_pygments$token );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_pygments$token,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pygments$token,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pygments$token,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pygments$token,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pygments$token );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_c574207cc2e883086040c6beb0095313, module_pygments$token );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    struct Nuitka_FrameObject *frame_6d865baa791616739033b5b9c8601f86;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_pygments$token_13 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_d2f80fdf8ff53b520942da8efaf174fa_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_d2f80fdf8ff53b520942da8efaf174fa_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_10b00f31e85ba3c55c79a114a57f809f;
        UPDATE_STRING_DICT0( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_6d865baa791616739033b5b9c8601f86 = MAKE_MODULE_FRAME( codeobj_6d865baa791616739033b5b9c8601f86, module_pygments$token );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_6d865baa791616739033b5b9c8601f86 );
    assert( Py_REFCNT( frame_6d865baa791616739033b5b9c8601f86 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_tuple_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_4 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_6 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 13;

                goto try_except_handler_1;
            }
            tmp_tuple_element_1 = const_str_plain__TokenType;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_6d865baa791616739033b5b9c8601f86->m_frame.f_lineno = 13;
            tmp_assign_source_7 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 13;

                goto try_except_handler_1;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_7;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 13;

                goto try_except_handler_1;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 13;

                    goto try_except_handler_1;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 13;

                    goto try_except_handler_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 13;

                    goto try_except_handler_1;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 13;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_1;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_8;
            tmp_assign_source_8 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_8;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_9;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_pygments$token_13 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c574207cc2e883086040c6beb0095313;
        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_plain__TokenType;
        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_d2f80fdf8ff53b520942da8efaf174fa_2, codeobj_d2f80fdf8ff53b520942da8efaf174fa, module_pygments$token, sizeof(void *) );
        frame_d2f80fdf8ff53b520942da8efaf174fa_2 = cache_frame_d2f80fdf8ff53b520942da8efaf174fa_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_d2f80fdf8ff53b520942da8efaf174fa_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_d2f80fdf8ff53b520942da8efaf174fa_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain_parent, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_pygments$token$$$function_1_split(  );



        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain_split, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_pygments$token$$$function_2___init__(  );



        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_pygments$token$$$function_3___contains__(  );



        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___contains__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_pygments$token$$$function_4___getattr__(  );



        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___getattr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_pygments$token$$$function_5___repr__(  );



        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_pygments$token$$$function_6___copy__(  );



        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___copy__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_pygments$token$$$function_7___deepcopy__(  );



        tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___deepcopy__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d2f80fdf8ff53b520942da8efaf174fa_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d2f80fdf8ff53b520942da8efaf174fa_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_d2f80fdf8ff53b520942da8efaf174fa_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_d2f80fdf8ff53b520942da8efaf174fa_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_d2f80fdf8ff53b520942da8efaf174fa_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_d2f80fdf8ff53b520942da8efaf174fa_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_d2f80fdf8ff53b520942da8efaf174fa_2 == cache_frame_d2f80fdf8ff53b520942da8efaf174fa_2 )
        {
            Py_DECREF( frame_d2f80fdf8ff53b520942da8efaf174fa_2 );
        }
        cache_frame_d2f80fdf8ff53b520942da8efaf174fa_2 = NULL;

        assertFrameObject( frame_d2f80fdf8ff53b520942da8efaf174fa_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_3;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_tuple_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 13;

                goto try_except_handler_3;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_tuple_tuple;
            tmp_res = PyObject_SetItem( locals_pygments$token_13, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 13;

                goto try_except_handler_3;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain__TokenType;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_pygments$token_13;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_6d865baa791616739033b5b9c8601f86->m_frame.f_lineno = 13;
            tmp_assign_source_10 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 13;

                goto try_except_handler_3;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_10;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_9 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_9 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pygments$token );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        Py_DECREF( locals_pygments$token_13 );
        locals_pygments$token_13 = NULL;
        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_pygments$token_13 );
        locals_pygments$token_13 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pygments$token );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pygments$token );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 13;
        goto try_except_handler_1;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain__TokenType, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain__TokenType );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__TokenType );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_TokenType" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;

            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        frame_6d865baa791616739033b5b9c8601f86->m_frame.f_lineno = 56;
        tmp_assign_source_11 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_source_name_5 = tmp_mvar_value_4;
        tmp_assign_source_12 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_Text );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Text, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Text );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Text );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_source_name_6 = tmp_mvar_value_5;
        tmp_assign_source_13 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_Whitespace );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Whitespace, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_6;
        tmp_assign_source_14 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_Escape );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Escape, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_7;
        tmp_assign_source_15 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_Error );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Error, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_8;
        tmp_assign_source_16 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_Other );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Other, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 67;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_9;
        tmp_assign_source_17 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_Keyword );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 68;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_10;
        tmp_assign_source_18 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_Name );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_11;
        tmp_assign_source_19 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_Literal );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Literal, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Literal );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Literal );
        }

        CHECK_OBJECT( tmp_mvar_value_12 );
        tmp_source_name_13 = tmp_mvar_value_12;
        tmp_assign_source_20 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_String );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Literal );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Literal );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Literal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_13;
        tmp_assign_source_21 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_Number );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_14;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;

            goto frame_exception_exit_1;
        }

        tmp_source_name_15 = tmp_mvar_value_14;
        tmp_assign_source_22 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_Punctuation );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Punctuation, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_source_name_16;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;

            goto frame_exception_exit_1;
        }

        tmp_source_name_16 = tmp_mvar_value_15;
        tmp_assign_source_23 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_Operator );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Operator, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_source_name_17;
        PyObject *tmp_mvar_value_16;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;

            goto frame_exception_exit_1;
        }

        tmp_source_name_17 = tmp_mvar_value_16;
        tmp_assign_source_24 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_Comment );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_source_name_18;
        PyObject *tmp_mvar_value_17;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_source_name_18 = tmp_mvar_value_17;
        tmp_assign_source_25 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_Generic );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_mvar_value_19;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }

        tmp_assattr_name_3 = tmp_mvar_value_18;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }

        tmp_assattr_target_3 = tmp_mvar_value_19;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_Token, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_mvar_value_21;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }

        tmp_assattr_name_4 = tmp_mvar_value_20;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }

        tmp_assattr_target_4 = tmp_mvar_value_21;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_String, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_assattr_target_5;
        PyObject *tmp_mvar_value_23;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_assattr_name_5 = tmp_mvar_value_22;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_assattr_target_5 = tmp_mvar_value_23;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_Number, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_pygments$token$$$function_8_is_token_subtype(  );



        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_is_token_subtype, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_pygments$token$$$function_9_string_to_tokentype(  );



        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_string_to_tokentype, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_source_name_19;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_source_name_20;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_source_name_21;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_source_name_22;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_source_name_23;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_source_name_24;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_source_name_25;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_source_name_26;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_source_name_27;
        PyObject *tmp_source_name_28;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_source_name_29;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_source_name_30;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_source_name_31;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_source_name_32;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_source_name_33;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_source_name_34;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_source_name_35;
        PyObject *tmp_source_name_36;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_source_name_37;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_source_name_38;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_source_name_39;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_source_name_40;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_source_name_41;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_source_name_42;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_source_name_43;
        PyObject *tmp_source_name_44;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_source_name_45;
        PyObject *tmp_source_name_46;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_source_name_47;
        PyObject *tmp_source_name_48;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_source_name_49;
        PyObject *tmp_source_name_50;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_dict_key_36;
        PyObject *tmp_dict_value_36;
        PyObject *tmp_source_name_51;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_dict_key_37;
        PyObject *tmp_dict_value_37;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_dict_key_38;
        PyObject *tmp_dict_value_38;
        PyObject *tmp_source_name_52;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_dict_key_39;
        PyObject *tmp_dict_value_39;
        PyObject *tmp_source_name_53;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_dict_key_40;
        PyObject *tmp_dict_value_40;
        PyObject *tmp_source_name_54;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_dict_key_41;
        PyObject *tmp_dict_value_41;
        PyObject *tmp_source_name_55;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_dict_key_42;
        PyObject *tmp_dict_value_42;
        PyObject *tmp_source_name_56;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_dict_key_43;
        PyObject *tmp_dict_value_43;
        PyObject *tmp_source_name_57;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_dict_key_44;
        PyObject *tmp_dict_value_44;
        PyObject *tmp_source_name_58;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_dict_key_45;
        PyObject *tmp_dict_value_45;
        PyObject *tmp_source_name_59;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_dict_key_46;
        PyObject *tmp_dict_value_46;
        PyObject *tmp_source_name_60;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_dict_key_47;
        PyObject *tmp_dict_value_47;
        PyObject *tmp_source_name_61;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_dict_key_48;
        PyObject *tmp_dict_value_48;
        PyObject *tmp_source_name_62;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_dict_key_49;
        PyObject *tmp_dict_value_49;
        PyObject *tmp_source_name_63;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_dict_key_50;
        PyObject *tmp_dict_value_50;
        PyObject *tmp_source_name_64;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_dict_key_51;
        PyObject *tmp_dict_value_51;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_dict_key_52;
        PyObject *tmp_dict_value_52;
        PyObject *tmp_source_name_65;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_dict_key_53;
        PyObject *tmp_dict_value_53;
        PyObject *tmp_source_name_66;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_dict_key_54;
        PyObject *tmp_dict_value_54;
        PyObject *tmp_source_name_67;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_dict_key_55;
        PyObject *tmp_dict_value_55;
        PyObject *tmp_source_name_68;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_dict_key_56;
        PyObject *tmp_dict_value_56;
        PyObject *tmp_source_name_69;
        PyObject *tmp_source_name_70;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_dict_key_57;
        PyObject *tmp_dict_value_57;
        PyObject *tmp_source_name_71;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_dict_key_58;
        PyObject *tmp_dict_value_58;
        PyObject *tmp_mvar_value_81;
        PyObject *tmp_dict_key_59;
        PyObject *tmp_dict_value_59;
        PyObject *tmp_source_name_72;
        PyObject *tmp_mvar_value_82;
        PyObject *tmp_dict_key_60;
        PyObject *tmp_dict_value_60;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_dict_key_61;
        PyObject *tmp_dict_value_61;
        PyObject *tmp_mvar_value_84;
        PyObject *tmp_dict_key_62;
        PyObject *tmp_dict_value_62;
        PyObject *tmp_source_name_73;
        PyObject *tmp_mvar_value_85;
        PyObject *tmp_dict_key_63;
        PyObject *tmp_dict_value_63;
        PyObject *tmp_source_name_74;
        PyObject *tmp_mvar_value_86;
        PyObject *tmp_dict_key_64;
        PyObject *tmp_dict_value_64;
        PyObject *tmp_source_name_75;
        PyObject *tmp_mvar_value_87;
        PyObject *tmp_dict_key_65;
        PyObject *tmp_dict_value_65;
        PyObject *tmp_source_name_76;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_dict_key_66;
        PyObject *tmp_dict_value_66;
        PyObject *tmp_source_name_77;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_dict_key_67;
        PyObject *tmp_dict_value_67;
        PyObject *tmp_source_name_78;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_dict_key_68;
        PyObject *tmp_dict_value_68;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_dict_key_69;
        PyObject *tmp_dict_value_69;
        PyObject *tmp_source_name_79;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_dict_key_70;
        PyObject *tmp_dict_value_70;
        PyObject *tmp_source_name_80;
        PyObject *tmp_mvar_value_93;
        PyObject *tmp_dict_key_71;
        PyObject *tmp_dict_value_71;
        PyObject *tmp_source_name_81;
        PyObject *tmp_mvar_value_94;
        PyObject *tmp_dict_key_72;
        PyObject *tmp_dict_value_72;
        PyObject *tmp_source_name_82;
        PyObject *tmp_mvar_value_95;
        PyObject *tmp_dict_key_73;
        PyObject *tmp_dict_value_73;
        PyObject *tmp_source_name_83;
        PyObject *tmp_mvar_value_96;
        PyObject *tmp_dict_key_74;
        PyObject *tmp_dict_value_74;
        PyObject *tmp_source_name_84;
        PyObject *tmp_mvar_value_97;
        PyObject *tmp_dict_key_75;
        PyObject *tmp_dict_value_75;
        PyObject *tmp_source_name_85;
        PyObject *tmp_mvar_value_98;
        PyObject *tmp_dict_key_76;
        PyObject *tmp_dict_value_76;
        PyObject *tmp_source_name_86;
        PyObject *tmp_mvar_value_99;
        PyObject *tmp_dict_key_77;
        PyObject *tmp_dict_value_77;
        PyObject *tmp_source_name_87;
        PyObject *tmp_mvar_value_100;
        PyObject *tmp_dict_key_78;
        PyObject *tmp_dict_value_78;
        PyObject *tmp_source_name_88;
        PyObject *tmp_mvar_value_101;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Token );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Token );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Token" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_1 = tmp_mvar_value_24;
        tmp_dict_value_1 = const_str_empty;
        tmp_assign_source_28 = _PyDict_NewPresized( 78 );
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_1, tmp_dict_value_1 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Text );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Text );
        }

        if ( tmp_mvar_value_25 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_2 = tmp_mvar_value_25;
        tmp_dict_value_2 = const_str_empty;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_2, tmp_dict_value_2 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Whitespace );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Whitespace );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Whitespace" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_3 = tmp_mvar_value_26;
        tmp_dict_value_3 = const_str_plain_w;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_3, tmp_dict_value_3 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Escape );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Escape );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Escape" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_4 = tmp_mvar_value_27;
        tmp_dict_value_4 = const_str_plain_esc;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_4, tmp_dict_value_4 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Error );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Error );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Error" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_5 = tmp_mvar_value_28;
        tmp_dict_value_5 = const_str_plain_err;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_5, tmp_dict_value_5 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Other );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Other );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Other" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_6 = tmp_mvar_value_29;
        tmp_dict_value_6 = const_str_plain_x;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_6, tmp_dict_value_6 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keyword );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keyword" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_7 = tmp_mvar_value_30;
        tmp_dict_value_7 = const_str_plain_k;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_7, tmp_dict_value_7 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keyword );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keyword" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;

            goto frame_exception_exit_1;
        }

        tmp_source_name_19 = tmp_mvar_value_31;
        tmp_dict_key_8 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_Constant );
        if ( tmp_dict_key_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 134;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_8 = const_str_plain_kc;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_key_8 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keyword );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keyword" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;

            goto frame_exception_exit_1;
        }

        tmp_source_name_20 = tmp_mvar_value_32;
        tmp_dict_key_9 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_Declaration );
        if ( tmp_dict_key_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 135;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_9 = const_str_plain_kd;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_9, tmp_dict_value_9 );
        Py_DECREF( tmp_dict_key_9 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keyword );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keyword" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }

        tmp_source_name_21 = tmp_mvar_value_33;
        tmp_dict_key_10 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_Namespace );
        if ( tmp_dict_key_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 136;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_10 = const_str_plain_kn;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_10, tmp_dict_value_10 );
        Py_DECREF( tmp_dict_key_10 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keyword );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keyword" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 137;

            goto frame_exception_exit_1;
        }

        tmp_source_name_22 = tmp_mvar_value_34;
        tmp_dict_key_11 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_Pseudo );
        if ( tmp_dict_key_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 137;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_11 = const_str_plain_kp;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_11, tmp_dict_value_11 );
        Py_DECREF( tmp_dict_key_11 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keyword );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keyword" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;

            goto frame_exception_exit_1;
        }

        tmp_source_name_23 = tmp_mvar_value_35;
        tmp_dict_key_12 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_Reserved );
        if ( tmp_dict_key_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 138;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_12 = const_str_plain_kr;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_12, tmp_dict_value_12 );
        Py_DECREF( tmp_dict_key_12 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Keyword );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keyword );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keyword" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }

        tmp_source_name_24 = tmp_mvar_value_36;
        tmp_dict_key_13 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_Type );
        if ( tmp_dict_key_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_13 = const_str_plain_kt;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_13, tmp_dict_value_13 );
        Py_DECREF( tmp_dict_key_13 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_37 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_14 = tmp_mvar_value_37;
        tmp_dict_value_14 = const_str_plain_n;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_14, tmp_dict_value_14 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_38 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_source_name_25 = tmp_mvar_value_38;
        tmp_dict_key_15 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_Attribute );
        if ( tmp_dict_key_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_15 = const_str_plain_na;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_15, tmp_dict_value_15 );
        Py_DECREF( tmp_dict_key_15 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;

            goto frame_exception_exit_1;
        }

        tmp_source_name_26 = tmp_mvar_value_39;
        tmp_dict_key_16 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_Builtin );
        if ( tmp_dict_key_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 143;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_16 = const_str_plain_nb;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_key_16 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }

        tmp_source_name_28 = tmp_mvar_value_40;
        tmp_source_name_27 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_Builtin );
        if ( tmp_source_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_17 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_Pseudo );
        Py_DECREF( tmp_source_name_27 );
        if ( tmp_dict_key_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_17 = const_str_plain_bp;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_17, tmp_dict_value_17 );
        Py_DECREF( tmp_dict_key_17 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;

            goto frame_exception_exit_1;
        }

        tmp_source_name_29 = tmp_mvar_value_41;
        tmp_dict_key_18 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_Class );
        if ( tmp_dict_key_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 145;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_18 = const_str_plain_nc;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_18, tmp_dict_value_18 );
        Py_DECREF( tmp_dict_key_18 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_source_name_30 = tmp_mvar_value_42;
        tmp_dict_key_19 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_Constant );
        if ( tmp_dict_key_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_19 = const_str_plain_no;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_19, tmp_dict_value_19 );
        Py_DECREF( tmp_dict_key_19 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }

        tmp_source_name_31 = tmp_mvar_value_43;
        tmp_dict_key_20 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_Decorator );
        if ( tmp_dict_key_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_20 = const_str_plain_nd;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_key_20 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_44 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;

            goto frame_exception_exit_1;
        }

        tmp_source_name_32 = tmp_mvar_value_44;
        tmp_dict_key_21 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_Entity );
        if ( tmp_dict_key_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 148;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_21 = const_str_plain_ni;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_21, tmp_dict_value_21 );
        Py_DECREF( tmp_dict_key_21 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_45 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }

        tmp_source_name_33 = tmp_mvar_value_45;
        tmp_dict_key_22 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_Exception );
        if ( tmp_dict_key_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_22 = const_str_plain_ne;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_22, tmp_dict_value_22 );
        Py_DECREF( tmp_dict_key_22 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_46 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;

            goto frame_exception_exit_1;
        }

        tmp_source_name_34 = tmp_mvar_value_46;
        tmp_dict_key_23 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_Function );
        if ( tmp_dict_key_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 150;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_23 = const_str_plain_nf;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_23, tmp_dict_value_23 );
        Py_DECREF( tmp_dict_key_23 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_47 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 151;

            goto frame_exception_exit_1;
        }

        tmp_source_name_36 = tmp_mvar_value_47;
        tmp_source_name_35 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_Function );
        if ( tmp_source_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 151;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_24 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_Magic );
        Py_DECREF( tmp_source_name_35 );
        if ( tmp_dict_key_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 151;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_24 = const_str_plain_fm;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_24, tmp_dict_value_24 );
        Py_DECREF( tmp_dict_key_24 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 152;

            goto frame_exception_exit_1;
        }

        tmp_source_name_37 = tmp_mvar_value_48;
        tmp_dict_key_25 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_Property );
        if ( tmp_dict_key_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 152;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_25 = const_str_plain_py;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_25, tmp_dict_value_25 );
        Py_DECREF( tmp_dict_key_25 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_49 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;

            goto frame_exception_exit_1;
        }

        tmp_source_name_38 = tmp_mvar_value_49;
        tmp_dict_key_26 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_Label );
        if ( tmp_dict_key_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 153;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_26 = const_str_plain_nl;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_26, tmp_dict_value_26 );
        Py_DECREF( tmp_dict_key_26 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_50 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;

            goto frame_exception_exit_1;
        }

        tmp_source_name_39 = tmp_mvar_value_50;
        tmp_dict_key_27 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_Namespace );
        if ( tmp_dict_key_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 154;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_27 = const_str_plain_nn;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_27, tmp_dict_value_27 );
        Py_DECREF( tmp_dict_key_27 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_51 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;

            goto frame_exception_exit_1;
        }

        tmp_source_name_40 = tmp_mvar_value_51;
        tmp_dict_key_28 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain_Other );
        if ( tmp_dict_key_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 155;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_28 = const_str_plain_nx;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_28, tmp_dict_value_28 );
        Py_DECREF( tmp_dict_key_28 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_52 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;

            goto frame_exception_exit_1;
        }

        tmp_source_name_41 = tmp_mvar_value_52;
        tmp_dict_key_29 = LOOKUP_ATTRIBUTE( tmp_source_name_41, const_str_plain_Tag );
        if ( tmp_dict_key_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 156;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_29 = const_str_plain_nt;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_29, tmp_dict_value_29 );
        Py_DECREF( tmp_dict_key_29 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 157;

            goto frame_exception_exit_1;
        }

        tmp_source_name_42 = tmp_mvar_value_53;
        tmp_dict_key_30 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain_Variable );
        if ( tmp_dict_key_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 157;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_30 = const_str_plain_nv;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_30, tmp_dict_value_30 );
        Py_DECREF( tmp_dict_key_30 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_54 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 158;

            goto frame_exception_exit_1;
        }

        tmp_source_name_44 = tmp_mvar_value_54;
        tmp_source_name_43 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain_Variable );
        if ( tmp_source_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 158;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_31 = LOOKUP_ATTRIBUTE( tmp_source_name_43, const_str_plain_Class );
        Py_DECREF( tmp_source_name_43 );
        if ( tmp_dict_key_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 158;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_31 = const_str_plain_vc;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_31, tmp_dict_value_31 );
        Py_DECREF( tmp_dict_key_31 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_55 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }

        tmp_source_name_46 = tmp_mvar_value_55;
        tmp_source_name_45 = LOOKUP_ATTRIBUTE( tmp_source_name_46, const_str_plain_Variable );
        if ( tmp_source_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_32 = LOOKUP_ATTRIBUTE( tmp_source_name_45, const_str_plain_Global );
        Py_DECREF( tmp_source_name_45 );
        if ( tmp_dict_key_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_32 = const_str_plain_vg;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_32, tmp_dict_value_32 );
        Py_DECREF( tmp_dict_key_32 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_56 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_source_name_48 = tmp_mvar_value_56;
        tmp_source_name_47 = LOOKUP_ATTRIBUTE( tmp_source_name_48, const_str_plain_Variable );
        if ( tmp_source_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_33 = LOOKUP_ATTRIBUTE( tmp_source_name_47, const_str_plain_Instance );
        Py_DECREF( tmp_source_name_47 );
        if ( tmp_dict_key_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_33 = const_str_plain_vi;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_33, tmp_dict_value_33 );
        Py_DECREF( tmp_dict_key_33 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Name );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Name );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;

            goto frame_exception_exit_1;
        }

        tmp_source_name_50 = tmp_mvar_value_57;
        tmp_source_name_49 = LOOKUP_ATTRIBUTE( tmp_source_name_50, const_str_plain_Variable );
        if ( tmp_source_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 161;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_34 = LOOKUP_ATTRIBUTE( tmp_source_name_49, const_str_plain_Magic );
        Py_DECREF( tmp_source_name_49 );
        if ( tmp_dict_key_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 161;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_34 = const_str_plain_vm;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_34, tmp_dict_value_34 );
        Py_DECREF( tmp_dict_key_34 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Literal );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Literal );
        }

        if ( tmp_mvar_value_58 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Literal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_35 = tmp_mvar_value_58;
        tmp_dict_value_35 = const_str_plain_l;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_35, tmp_dict_value_35 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Literal );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Literal );
        }

        if ( tmp_mvar_value_59 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Literal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_source_name_51 = tmp_mvar_value_59;
        tmp_dict_key_36 = LOOKUP_ATTRIBUTE( tmp_source_name_51, const_str_plain_Date );
        if ( tmp_dict_key_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_36 = const_str_plain_ld;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_36, tmp_dict_value_36 );
        Py_DECREF( tmp_dict_key_36 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_60 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_37 = tmp_mvar_value_60;
        tmp_dict_value_37 = const_str_plain_s;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_37, tmp_dict_value_37 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_source_name_52 = tmp_mvar_value_61;
        tmp_dict_key_38 = LOOKUP_ATTRIBUTE( tmp_source_name_52, const_str_plain_Affix );
        if ( tmp_dict_key_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_38 = const_str_plain_sa;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_38, tmp_dict_value_38 );
        Py_DECREF( tmp_dict_key_38 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;

            goto frame_exception_exit_1;
        }

        tmp_source_name_53 = tmp_mvar_value_62;
        tmp_dict_key_39 = LOOKUP_ATTRIBUTE( tmp_source_name_53, const_str_plain_Backtick );
        if ( tmp_dict_key_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 168;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_39 = const_str_plain_sb;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_39, tmp_dict_value_39 );
        Py_DECREF( tmp_dict_key_39 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }

        tmp_source_name_54 = tmp_mvar_value_63;
        tmp_dict_key_40 = LOOKUP_ATTRIBUTE( tmp_source_name_54, const_str_plain_Char );
        if ( tmp_dict_key_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_40 = const_str_plain_sc;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_40, tmp_dict_value_40 );
        Py_DECREF( tmp_dict_key_40 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }

        tmp_source_name_55 = tmp_mvar_value_64;
        tmp_dict_key_41 = LOOKUP_ATTRIBUTE( tmp_source_name_55, const_str_plain_Delimiter );
        if ( tmp_dict_key_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_41 = const_str_plain_dl;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_41, tmp_dict_value_41 );
        Py_DECREF( tmp_dict_key_41 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_65 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_source_name_56 = tmp_mvar_value_65;
        tmp_dict_key_42 = LOOKUP_ATTRIBUTE( tmp_source_name_56, const_str_plain_Doc );
        if ( tmp_dict_key_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_42 = const_str_plain_sd;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_42, tmp_dict_value_42 );
        Py_DECREF( tmp_dict_key_42 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_source_name_57 = tmp_mvar_value_66;
        tmp_dict_key_43 = LOOKUP_ATTRIBUTE( tmp_source_name_57, const_str_plain_Double );
        if ( tmp_dict_key_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_43 = const_str_plain_s2;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_43, tmp_dict_value_43 );
        Py_DECREF( tmp_dict_key_43 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_source_name_58 = tmp_mvar_value_67;
        tmp_dict_key_44 = LOOKUP_ATTRIBUTE( tmp_source_name_58, const_str_plain_Escape );
        if ( tmp_dict_key_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_44 = const_str_plain_se;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_44, tmp_dict_value_44 );
        Py_DECREF( tmp_dict_key_44 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_source_name_59 = tmp_mvar_value_68;
        tmp_dict_key_45 = LOOKUP_ATTRIBUTE( tmp_source_name_59, const_str_plain_Heredoc );
        if ( tmp_dict_key_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_45 = const_str_plain_sh;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_45, tmp_dict_value_45 );
        Py_DECREF( tmp_dict_key_45 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_source_name_60 = tmp_mvar_value_69;
        tmp_dict_key_46 = LOOKUP_ATTRIBUTE( tmp_source_name_60, const_str_plain_Interpol );
        if ( tmp_dict_key_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_46 = const_str_plain_si;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_46, tmp_dict_value_46 );
        Py_DECREF( tmp_dict_key_46 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }

        tmp_source_name_61 = tmp_mvar_value_70;
        tmp_dict_key_47 = LOOKUP_ATTRIBUTE( tmp_source_name_61, const_str_plain_Other );
        if ( tmp_dict_key_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_47 = const_str_plain_sx;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_47, tmp_dict_value_47 );
        Py_DECREF( tmp_dict_key_47 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }

        tmp_source_name_62 = tmp_mvar_value_71;
        tmp_dict_key_48 = LOOKUP_ATTRIBUTE( tmp_source_name_62, const_str_plain_Regex );
        if ( tmp_dict_key_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_48 = const_str_plain_sr;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_48, tmp_dict_value_48 );
        Py_DECREF( tmp_dict_key_48 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_72 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 178;

            goto frame_exception_exit_1;
        }

        tmp_source_name_63 = tmp_mvar_value_72;
        tmp_dict_key_49 = LOOKUP_ATTRIBUTE( tmp_source_name_63, const_str_plain_Single );
        if ( tmp_dict_key_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 178;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_49 = const_str_plain_s1;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_49, tmp_dict_value_49 );
        Py_DECREF( tmp_dict_key_49 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_String );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_String );
        }

        if ( tmp_mvar_value_73 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "String" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;

            goto frame_exception_exit_1;
        }

        tmp_source_name_64 = tmp_mvar_value_73;
        tmp_dict_key_50 = LOOKUP_ATTRIBUTE( tmp_source_name_64, const_str_plain_Symbol );
        if ( tmp_dict_key_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 179;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_50 = const_str_plain_ss;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_50, tmp_dict_value_50 );
        Py_DECREF( tmp_dict_key_50 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_74 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_51 = tmp_mvar_value_74;
        tmp_dict_value_51 = const_str_plain_m;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_51, tmp_dict_value_51 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_75 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }

        tmp_source_name_65 = tmp_mvar_value_75;
        tmp_dict_key_52 = LOOKUP_ATTRIBUTE( tmp_source_name_65, const_str_plain_Bin );
        if ( tmp_dict_key_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_52 = const_str_plain_mb;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_52, tmp_dict_value_52 );
        Py_DECREF( tmp_dict_key_52 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_76 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_source_name_66 = tmp_mvar_value_76;
        tmp_dict_key_53 = LOOKUP_ATTRIBUTE( tmp_source_name_66, const_str_plain_Float );
        if ( tmp_dict_key_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_53 = const_str_plain_mf;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_53, tmp_dict_value_53 );
        Py_DECREF( tmp_dict_key_53 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_77 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;

            goto frame_exception_exit_1;
        }

        tmp_source_name_67 = tmp_mvar_value_77;
        tmp_dict_key_54 = LOOKUP_ATTRIBUTE( tmp_source_name_67, const_str_plain_Hex );
        if ( tmp_dict_key_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 184;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_54 = const_str_plain_mh;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_54, tmp_dict_value_54 );
        Py_DECREF( tmp_dict_key_54 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_78 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;

            goto frame_exception_exit_1;
        }

        tmp_source_name_68 = tmp_mvar_value_78;
        tmp_dict_key_55 = LOOKUP_ATTRIBUTE( tmp_source_name_68, const_str_plain_Integer );
        if ( tmp_dict_key_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 185;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_55 = const_str_plain_mi;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_55, tmp_dict_value_55 );
        Py_DECREF( tmp_dict_key_55 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_79 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 186;

            goto frame_exception_exit_1;
        }

        tmp_source_name_70 = tmp_mvar_value_79;
        tmp_source_name_69 = LOOKUP_ATTRIBUTE( tmp_source_name_70, const_str_plain_Integer );
        if ( tmp_source_name_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 186;

            goto frame_exception_exit_1;
        }
        tmp_dict_key_56 = LOOKUP_ATTRIBUTE( tmp_source_name_69, const_str_plain_Long );
        Py_DECREF( tmp_source_name_69 );
        if ( tmp_dict_key_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 186;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_56 = const_str_plain_il;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_56, tmp_dict_value_56 );
        Py_DECREF( tmp_dict_key_56 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Number );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Number );
        }

        if ( tmp_mvar_value_80 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Number" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;

            goto frame_exception_exit_1;
        }

        tmp_source_name_71 = tmp_mvar_value_80;
        tmp_dict_key_57 = LOOKUP_ATTRIBUTE( tmp_source_name_71, const_str_plain_Oct );
        if ( tmp_dict_key_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 187;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_57 = const_str_plain_mo;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_57, tmp_dict_value_57 );
        Py_DECREF( tmp_dict_key_57 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Operator );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Operator );
        }

        if ( tmp_mvar_value_81 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_58 = tmp_mvar_value_81;
        tmp_dict_value_58 = const_str_plain_o;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_58, tmp_dict_value_58 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Operator );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Operator );
        }

        if ( tmp_mvar_value_82 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Operator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 190;

            goto frame_exception_exit_1;
        }

        tmp_source_name_72 = tmp_mvar_value_82;
        tmp_dict_key_59 = LOOKUP_ATTRIBUTE( tmp_source_name_72, const_str_plain_Word );
        if ( tmp_dict_key_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 190;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_59 = const_str_plain_ow;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_59, tmp_dict_value_59 );
        Py_DECREF( tmp_dict_key_59 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Punctuation );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Punctuation );
        }

        if ( tmp_mvar_value_83 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Punctuation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_60 = tmp_mvar_value_83;
        tmp_dict_value_60 = const_str_plain_p;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_60, tmp_dict_value_60 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Comment );
        }

        if ( tmp_mvar_value_84 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Comment" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_61 = tmp_mvar_value_84;
        tmp_dict_value_61 = const_str_plain_c;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_61, tmp_dict_value_61 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Comment );
        }

        if ( tmp_mvar_value_85 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Comment" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 195;

            goto frame_exception_exit_1;
        }

        tmp_source_name_73 = tmp_mvar_value_85;
        tmp_dict_key_62 = LOOKUP_ATTRIBUTE( tmp_source_name_73, const_str_plain_Hashbang );
        if ( tmp_dict_key_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 195;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_62 = const_str_plain_ch;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_62, tmp_dict_value_62 );
        Py_DECREF( tmp_dict_key_62 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Comment );
        }

        if ( tmp_mvar_value_86 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Comment" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 196;

            goto frame_exception_exit_1;
        }

        tmp_source_name_74 = tmp_mvar_value_86;
        tmp_dict_key_63 = LOOKUP_ATTRIBUTE( tmp_source_name_74, const_str_plain_Multiline );
        if ( tmp_dict_key_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 196;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_63 = const_str_plain_cm;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_63, tmp_dict_value_63 );
        Py_DECREF( tmp_dict_key_63 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Comment );
        }

        if ( tmp_mvar_value_87 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Comment" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;

            goto frame_exception_exit_1;
        }

        tmp_source_name_75 = tmp_mvar_value_87;
        tmp_dict_key_64 = LOOKUP_ATTRIBUTE( tmp_source_name_75, const_str_plain_Preproc );
        if ( tmp_dict_key_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 197;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_64 = const_str_plain_cp;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_64, tmp_dict_value_64 );
        Py_DECREF( tmp_dict_key_64 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Comment );
        }

        if ( tmp_mvar_value_88 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Comment" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 198;

            goto frame_exception_exit_1;
        }

        tmp_source_name_76 = tmp_mvar_value_88;
        tmp_dict_key_65 = LOOKUP_ATTRIBUTE( tmp_source_name_76, const_str_plain_PreprocFile );
        if ( tmp_dict_key_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 198;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_65 = const_str_plain_cpf;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_65, tmp_dict_value_65 );
        Py_DECREF( tmp_dict_key_65 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Comment );
        }

        if ( tmp_mvar_value_89 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Comment" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }

        tmp_source_name_77 = tmp_mvar_value_89;
        tmp_dict_key_66 = LOOKUP_ATTRIBUTE( tmp_source_name_77, const_str_plain_Single );
        if ( tmp_dict_key_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_66 = const_str_plain_c1;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_66, tmp_dict_value_66 );
        Py_DECREF( tmp_dict_key_66 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Comment );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Comment );
        }

        if ( tmp_mvar_value_90 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Comment" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 200;

            goto frame_exception_exit_1;
        }

        tmp_source_name_78 = tmp_mvar_value_90;
        tmp_dict_key_67 = LOOKUP_ATTRIBUTE( tmp_source_name_78, const_str_plain_Special );
        if ( tmp_dict_key_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 200;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_67 = const_str_plain_cs;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_67, tmp_dict_value_67 );
        Py_DECREF( tmp_dict_key_67 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_91 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 202;

            goto frame_exception_exit_1;
        }

        tmp_dict_key_68 = tmp_mvar_value_91;
        tmp_dict_value_68 = const_str_plain_g;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_68, tmp_dict_value_68 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_92 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 203;

            goto frame_exception_exit_1;
        }

        tmp_source_name_79 = tmp_mvar_value_92;
        tmp_dict_key_69 = LOOKUP_ATTRIBUTE( tmp_source_name_79, const_str_plain_Deleted );
        if ( tmp_dict_key_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 203;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_69 = const_str_plain_gd;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_69, tmp_dict_value_69 );
        Py_DECREF( tmp_dict_key_69 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_93 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 204;

            goto frame_exception_exit_1;
        }

        tmp_source_name_80 = tmp_mvar_value_93;
        tmp_dict_key_70 = LOOKUP_ATTRIBUTE( tmp_source_name_80, const_str_plain_Emph );
        if ( tmp_dict_key_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 204;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_70 = const_str_plain_ge;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_70, tmp_dict_value_70 );
        Py_DECREF( tmp_dict_key_70 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_94 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 205;

            goto frame_exception_exit_1;
        }

        tmp_source_name_81 = tmp_mvar_value_94;
        tmp_dict_key_71 = LOOKUP_ATTRIBUTE( tmp_source_name_81, const_str_plain_Error );
        if ( tmp_dict_key_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 205;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_71 = const_str_plain_gr;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_71, tmp_dict_value_71 );
        Py_DECREF( tmp_dict_key_71 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_95 == NULL ))
        {
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_95 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }

        tmp_source_name_82 = tmp_mvar_value_95;
        tmp_dict_key_72 = LOOKUP_ATTRIBUTE( tmp_source_name_82, const_str_plain_Heading );
        if ( tmp_dict_key_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_72 = const_str_plain_gh;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_72, tmp_dict_value_72 );
        Py_DECREF( tmp_dict_key_72 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_96 == NULL ))
        {
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_96 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;

            goto frame_exception_exit_1;
        }

        tmp_source_name_83 = tmp_mvar_value_96;
        tmp_dict_key_73 = LOOKUP_ATTRIBUTE( tmp_source_name_83, const_str_plain_Inserted );
        if ( tmp_dict_key_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 207;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_73 = const_str_plain_gi;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_73, tmp_dict_value_73 );
        Py_DECREF( tmp_dict_key_73 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_97 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_97 == NULL ))
        {
            tmp_mvar_value_97 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_97 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 208;

            goto frame_exception_exit_1;
        }

        tmp_source_name_84 = tmp_mvar_value_97;
        tmp_dict_key_74 = LOOKUP_ATTRIBUTE( tmp_source_name_84, const_str_plain_Output );
        if ( tmp_dict_key_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 208;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_74 = const_str_plain_go;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_74, tmp_dict_value_74 );
        Py_DECREF( tmp_dict_key_74 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_98 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_98 == NULL ))
        {
            tmp_mvar_value_98 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_98 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 209;

            goto frame_exception_exit_1;
        }

        tmp_source_name_85 = tmp_mvar_value_98;
        tmp_dict_key_75 = LOOKUP_ATTRIBUTE( tmp_source_name_85, const_str_plain_Prompt );
        if ( tmp_dict_key_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 209;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_75 = const_str_plain_gp;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_75, tmp_dict_value_75 );
        Py_DECREF( tmp_dict_key_75 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_99 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_99 == NULL ))
        {
            tmp_mvar_value_99 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_99 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 210;

            goto frame_exception_exit_1;
        }

        tmp_source_name_86 = tmp_mvar_value_99;
        tmp_dict_key_76 = LOOKUP_ATTRIBUTE( tmp_source_name_86, const_str_plain_Strong );
        if ( tmp_dict_key_76 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 210;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_76 = const_str_plain_gs;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_76, tmp_dict_value_76 );
        Py_DECREF( tmp_dict_key_76 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_100 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_100 == NULL ))
        {
            tmp_mvar_value_100 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_100 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 211;

            goto frame_exception_exit_1;
        }

        tmp_source_name_87 = tmp_mvar_value_100;
        tmp_dict_key_77 = LOOKUP_ATTRIBUTE( tmp_source_name_87, const_str_plain_Subheading );
        if ( tmp_dict_key_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 211;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_77 = const_str_plain_gu;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_77, tmp_dict_value_77 );
        Py_DECREF( tmp_dict_key_77 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_101 = GET_STRING_DICT_VALUE( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_Generic );

        if (unlikely( tmp_mvar_value_101 == NULL ))
        {
            tmp_mvar_value_101 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Generic );
        }

        if ( tmp_mvar_value_101 == NULL )
        {
            Py_DECREF( tmp_assign_source_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Generic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 212;

            goto frame_exception_exit_1;
        }

        tmp_source_name_88 = tmp_mvar_value_101;
        tmp_dict_key_78 = LOOKUP_ATTRIBUTE( tmp_source_name_88, const_str_plain_Traceback );
        if ( tmp_dict_key_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 212;

            goto frame_exception_exit_1;
        }
        tmp_dict_value_78 = const_str_plain_gt;
        tmp_res = PyDict_SetItem( tmp_assign_source_28, tmp_dict_key_78, tmp_dict_value_78 );
        Py_DECREF( tmp_dict_key_78 );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_28 );

            exception_lineno = 124;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$token, (Nuitka_StringObject *)const_str_plain_STANDARD_TYPES, tmp_assign_source_28 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6d865baa791616739033b5b9c8601f86 );
#endif
    popFrameStack();

    assertFrameObject( frame_6d865baa791616739033b5b9c8601f86 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6d865baa791616739033b5b9c8601f86 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6d865baa791616739033b5b9c8601f86, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6d865baa791616739033b5b9c8601f86->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6d865baa791616739033b5b9c8601f86, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_pygments$token );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
