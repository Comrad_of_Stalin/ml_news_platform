/* Generated code for Python module 'pymorphy2.opencorpora_dict.storage'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_pymorphy2$opencorpora_dict$storage" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pymorphy2$opencorpora_dict$storage;
PyDictObject *moduledict_pymorphy2$opencorpora_dict$storage;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_itertools;
extern PyObject *const_str_plain_prediction_suffixes_dawgs;
extern PyObject *const_str_plain_tag;
static PyObject *const_str_digest_62ced834d3e8324ba6678e44f1cbde32;
extern PyObject *const_str_plain_links;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_array;
extern PyObject *const_str_plain_paradigm_len;
extern PyObject *const_str_plain_getLogger;
static PyObject *const_str_plain__load_meta;
static PyObject *const_str_plain_prediction_suffixes_dawg;
static PyObject *const_tuple_str_digest_461ec8c61dee4c2c2dcfcc984f28c487_tuple;
extern PyObject *const_str_plain_zip;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_minor;
extern PyObject *const_str_plain_save;
extern PyObject *const_str_plain_prefix_id;
extern PyObject *const_str_plain___exit__;
static PyObject *const_str_digest_12fb4694fb9fcdaa3217c81de2f36a5c;
extern PyObject *const_str_plain_struct;
extern PyObject *const_str_plain_unpack;
static PyObject *const_str_plain_pymorphy2_version;
static PyObject *const_tuple_050dd7804c0479823b6978945f649ea6_tuple;
static PyObject *const_str_plain__assert_format_is_compatible;
static PyObject *const_str_digest_363e1f675eb3d81a8095581e7d6aa1e2;
static PyObject *const_tuple_str_plain_p_str_plain_path_tuple;
static PyObject *const_str_plain__load_tag_class;
extern PyObject *const_str_plain_items;
extern PyObject *const_tuple_str_plain_PARADIGM_PREFIXES_str_plain_PREDICTION_PREFIXES_tuple;
extern PyObject *const_str_plain_fn;
static PyObject *const_tuple_str_plain_path_str_plain_out_path_tuple;
extern PyObject *const_str_plain_grammemes;
static PyObject *const_str_digest_6f227101a6d47a22c047f701472a5cb0;
extern PyObject *const_str_plain_paradigm_prefixes;
extern PyObject *const_str_plain_PARADIGM_PREFIXES;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_95f6424560611d246f267f2ad8ca8874;
extern PyObject *const_str_plain_fromfile;
extern PyObject *const_str_plain_H;
static PyObject *const_tuple_str_digest_735cb515f948a018f1e620ea33122570_tuple;
extern PyObject *const_str_plain___enter__;
static PyObject *const_dict_15ab90b31dfaacebcd2e280ebae53401;
static PyObject *const_str_plain_format_version;
static PyObject *const_tuple_befdf3706f7025480263df5d104f1caf_tuple;
static PyObject *const_str_digest_4190a579127570ed281dc39447207faf;
extern PyObject *const_str_plain_join;
static PyObject *const_tuple_str_digest_1bd09a763c8b38cd78af0e6a5d992c87_tuple;
extern PyObject *const_str_plain_revision;
static PyObject *const_str_plain_source_lexemes_count;
extern PyObject *const_str_plain_read;
static PyObject *const_tuple_37004a6a826594e9e23ba550e6b62223_tuple;
static PyObject *const_str_digest_77456c53d050fd605724e2705d57a3fe;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_para;
extern PyObject *const_str_plain_out_path;
static PyObject *const_tuple_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_absolute_import;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain_meta;
extern PyObject *const_str_digest_d45b7aaa3187ef32d2beec74353f4cca;
static PyObject *const_str_plain_gramtab_length;
extern PyObject *const_str_plain_wb;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain_lexemes;
static PyObject *const_str_digest_f540b7c6a5c24848def98e8776abd3df;
extern PyObject *const_str_plain_parsed_dict;
static PyObject *const_str_plain_source_links_count;
static PyObject *const_str_plain__load_gramtab;
static PyObject *const_tuple_str_digest_f540b7c6a5c24848def98e8776abd3df_tuple;
extern PyObject *const_str_plain_iterkeys;
static PyObject *const_str_digest_affe8b2e5cc38c825cf1b08041a52a4f;
extern PyObject *const_str_digest_44a90c7502259e04130dba80b2d605d3;
extern PyObject *const_str_digest_4e9682f5d714f894d265491bc430d2ad;
static PyObject *const_str_plain_paradigm_prefixes_length;
static PyObject *const_str_plain_source_revision;
extern PyObject *const_str_plain__from_internal_tag;
static PyObject *const_str_digest_7ba451d3f8a14c32d5340f541ccdc012;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_registry;
static PyObject *const_str_plain_words_dawg_len;
extern PyObject *const_str_plain_p;
static PyObject *const_str_plain_words_dawg;
static PyObject *const_str_plain_prediction_prefixes_dawg_length;
static PyObject *const_str_plain_prediction_suffixes_dawg_lenghts;
static PyObject *const_str_plain_gramtab_formats;
extern PyObject *const_str_plain_collections;
extern PyObject *const_str_plain_json_read;
extern PyObject *const_str_plain_suffixes;
static PyObject *const_str_plain_LoadedDictionary;
extern PyObject *const_str_plain_load;
static PyObject *const_tuple_str_digest_ffe84336574aab4705a07cd32f4567e9_tuple;
extern PyObject *const_str_plain_keys;
extern PyObject *const_str_plain_rb;
extern PyObject *const_str_plain_indent;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_enumerate;
static PyObject *const_tuple_str_digest_77456c53d050fd605724e2705d57a3fe_tuple;
extern PyObject *const_str_plain_gramtab;
extern PyObject *const_tuple_str_plain_dawg_tuple;
static PyObject *const_str_digest_461ec8c61dee4c2c2dcfcc984f28c487;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_a24a2f5d59e8cf71107ca1ce19436812;
static PyObject *const_tuple_str_digest_7ba451d3f8a14c32d5340f541ccdc012_tuple;
extern PyObject *const_str_plain__init_grammemes;
extern PyObject *const_str_plain_datetime;
static PyObject *const_tuple_ba2246cfb8ba10b831875d9e12011b9d_tuple;
extern PyObject *const_str_plain_major;
static PyObject *const_str_plain_paradigms_length;
extern PyObject *const_str_plain_izip;
static PyObject *const_str_plain_gramtab_filename;
static PyObject *const_str_plain_paradigms_count;
static PyObject *const_str_digest_7d5d1392b0d186e0cb7195994bc0e765;
extern PyObject *const_str_plain_PREDICTION_PREFIXES;
static PyObject *const_tuple_str_digest_95f6424560611d246f267f2ad8ca8874_tuple;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_compiled_dict;
static PyObject *const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple;
static PyObject *const_str_digest_1bd09a763c8b38cd78af0e6a5d992c87;
extern PyObject *const_str_plain_pack;
static PyObject *const_str_plain_prediction_suffixes_dawg_lengths;
static PyObject *const_str_digest_2d0f3f34101c8d1ec2cf506fd8962036;
static PyObject *const_str_digest_ee2e795a82006414dbeef053d3c7c6e2;
static PyObject *const_str_plain_str_gramtab;
extern PyObject *const_str_plain_k;
extern PyObject *const_str_plain_f;
static PyObject *const_str_plain_parse_float;
static PyObject *const_str_digest_362a54b311f40a17b3197e3b9f3e2ec1;
extern PyObject *const_str_plain___version__;
static PyObject *const_list_str_plain_source_str_digest_f50366a6d32ef2492b3f85df1d0f8544_list;
extern PyObject *const_str_digest_969c69e9fbc42cc6a8915b0b5d93fbb0;
static PyObject *const_tuple_str_plain_tag_str_plain_Tag_tuple;
extern PyObject *const_int_0;
static PyObject *const_tuple_700c29999bb988695267ade4566335c3_tuple;
static PyObject *const_tuple_str_plain_filename_str_plain_meta_tuple;
extern PyObject *const_str_plain_exists;
extern PyObject *const_str_plain_msg;
static PyObject *const_str_digest_3b990c06e7442baf7e5180e2fa57f730;
static PyObject *const_str_digest_5dffe1b8ac250873b7da4b62752cf207;
extern PyObject *const_str_plain_tag_str;
extern PyObject *const_tuple_int_pos_2_tuple;
static PyObject *const_str_digest_4724d4b522be3f8f06676393f6c93087;
extern PyObject *const_str_plain_PredictionSuffixesDAWG;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98;
static PyObject *const_str_digest_c53becbf4353e4da85be8e397d486c0b;
static PyObject *const_str_plain_compiled_at;
extern PyObject *const_str_plain_OrderedDict;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_Tag;
extern PyObject *const_str_plain_utcnow;
extern PyObject *const_str_plain_prediction_options;
extern PyObject *const_str_plain_namedtuple;
extern PyObject *const_str_plain_isoformat;
extern PyObject *const_int_pos_4;
extern PyObject *const_str_plain_prediction_prefixes;
extern PyObject *const_str_angle_lambda;
static PyObject *const_str_digest_3b6fbce621c67f99314fd35ccdf56314;
static PyObject *const_str_digest_e07b892de4fffdf11e3fd8d0a63239b4;
static PyObject *const_str_digest_735cb515f948a018f1e620ea33122570;
static PyObject *const_str_digest_ffe84336574aab4705a07cd32f4567e9;
static PyObject *const_str_plain__dawg_len;
static PyObject *const_tuple_str_plain_json_write_str_plain_json_read_tuple;
extern PyObject *const_str_plain_load_dict;
static PyObject *const_str_digest_74531d15f942b19fea46429a1b996771;
static PyObject *const_dict_5d3329810349a48ba364b65708c4137e;
extern PyObject *const_str_plain___cached__;
static PyObject *const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple;
static PyObject *const_tuple_str_digest_4e9682f5d714f894d265491bc430d2ad_tuple;
extern PyObject *const_str_plain_save_compiled_dict;
extern PyObject *const_str_plain_paradigms;
extern PyObject *const_str_plain_tofile;
extern PyObject *const_str_digest_11de6548c656143b505417b369c3f813;
extern PyObject *const_str_plain_WordsDawg;
static PyObject *const_str_digest_8ee69f7274ac2df4c3e435ad05a88f27;
extern PyObject *const_str_plain_source;
extern PyObject *const_str_plain_words;
extern PyObject *const_str_plain_debug;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_filename;
extern PyObject *const_str_plain__f;
static PyObject *const_str_digest_75d6424facd0bcab0f0d95042aa846f1;
extern PyObject *const_str_plain_json_write;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_new_gramtab;
static PyObject *const_str_digest_49e5da426d804b69a49162ed40a8b58f;
static PyObject *const_tuple_str_digest_5dffe1b8ac250873b7da4b62752cf207_tuple;
static PyObject *const_tuple_str_plain_tagset_tuple;
static PyObject *const_str_plain_words_dawg_length;
extern PyObject *const_str_plain_logger;
static PyObject *const_str_plain_suffixes_length;
extern PyObject *const_tuple_str_plain_H_tuple;
static PyObject *const_str_plain_grammemes_filename;
static PyObject *const_str_plain_CURRENT_FORMAT_VERSION;
static PyObject *const_str_digest_ea3b39b1eb560d67c3a621e2ee6b48a7;
static PyObject *const_tuple_str_digest_74531d15f942b19fea46429a1b996771_tuple;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_k_tuple;
static PyObject *const_str_plain_tagset;
static PyObject *const_str_plain_gramtab_format;
static PyObject *const_tuple_str_digest_3b990c06e7442baf7e5180e2fa57f730_tuple;
static PyObject *const_str_plain__load_paradigms;
extern PyObject *const_str_plain_write;
static PyObject *const_str_plain_gramtab_name;
static PyObject *const_str_digest_7749eac3a559542096769a3088266128;
extern PyObject *const_str_plain_get;
static PyObject *const_list_a3c633733c653128003d731da37f55a5_list;
extern PyObject *const_str_plain_DAWG;
static PyObject *const_str_plain_source_version;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_version;
static PyObject *const_str_digest_f50366a6d32ef2492b3f85df1d0f8544;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_dawg;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_plain_pymorphy2;
static PyObject *const_str_digest_cd8399df6542e7b2ff2d6ac1e3b6c520;
extern PyObject *const_str_plain_logging;
extern PyObject *const_str_plain_format;
static PyObject *const_str_plain_curr_major;
static PyObject *const_str_plain_curr_minor;
static PyObject *const_tuple_str_plain_tag_str_str_plain_Tag_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_62ced834d3e8324ba6678e44f1cbde32 = UNSTREAM_STRING_ASCII( &constant_bin[ 5054683 ], 88, 0 );
    const_str_plain__load_meta = UNSTREAM_STRING_ASCII( &constant_bin[ 5054771 ], 10, 1 );
    const_str_plain_prediction_suffixes_dawg = UNSTREAM_STRING_ASCII( &constant_bin[ 5051110 ], 24, 1 );
    const_tuple_str_digest_461ec8c61dee4c2c2dcfcc984f28c487_tuple = PyTuple_New( 1 );
    const_str_digest_461ec8c61dee4c2c2dcfcc984f28c487 = UNSTREAM_STRING_ASCII( &constant_bin[ 5054781 ], 15, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_461ec8c61dee4c2c2dcfcc984f28c487_tuple, 0, const_str_digest_461ec8c61dee4c2c2dcfcc984f28c487 ); Py_INCREF( const_str_digest_461ec8c61dee4c2c2dcfcc984f28c487 );
    const_str_digest_12fb4694fb9fcdaa3217c81de2f36a5c = UNSTREAM_STRING_ASCII( &constant_bin[ 5054796 ], 56, 0 );
    const_str_plain_pymorphy2_version = UNSTREAM_STRING_ASCII( &constant_bin[ 5054852 ], 17, 1 );
    const_tuple_050dd7804c0479823b6978945f649ea6_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_050dd7804c0479823b6978945f649ea6_tuple, 0, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    const_str_plain_gramtab_format = UNSTREAM_STRING_ASCII( &constant_bin[ 5054869 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_050dd7804c0479823b6978945f649ea6_tuple, 1, const_str_plain_gramtab_format ); Py_INCREF( const_str_plain_gramtab_format );
    PyTuple_SET_ITEM( const_tuple_050dd7804c0479823b6978945f649ea6_tuple, 2, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    const_str_plain_gramtab_formats = UNSTREAM_STRING_ASCII( &constant_bin[ 5054883 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_050dd7804c0479823b6978945f649ea6_tuple, 3, const_str_plain_gramtab_formats ); Py_INCREF( const_str_plain_gramtab_formats );
    const_str_plain_gramtab_filename = UNSTREAM_STRING_ASCII( &constant_bin[ 5054898 ], 16, 1 );
    PyTuple_SET_ITEM( const_tuple_050dd7804c0479823b6978945f649ea6_tuple, 4, const_str_plain_gramtab_filename ); Py_INCREF( const_str_plain_gramtab_filename );
    const_str_plain__assert_format_is_compatible = UNSTREAM_STRING_ASCII( &constant_bin[ 5054914 ], 28, 1 );
    const_str_digest_363e1f675eb3d81a8095581e7d6aa1e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5054942 ], 98, 0 );
    const_tuple_str_plain_p_str_plain_path_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_p_str_plain_path_tuple, 0, const_str_plain_p ); Py_INCREF( const_str_plain_p );
    PyTuple_SET_ITEM( const_tuple_str_plain_p_str_plain_path_tuple, 1, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    const_str_plain__load_tag_class = UNSTREAM_STRING_ASCII( &constant_bin[ 5055040 ], 15, 1 );
    const_tuple_str_plain_path_str_plain_out_path_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_out_path_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_out_path_tuple, 1, const_str_plain_out_path ); Py_INCREF( const_str_plain_out_path );
    const_str_digest_6f227101a6d47a22c047f701472a5cb0 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055055 ], 31, 0 );
    const_str_digest_95f6424560611d246f267f2ad8ca8874 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055086 ], 13, 0 );
    const_tuple_str_digest_735cb515f948a018f1e620ea33122570_tuple = PyTuple_New( 1 );
    const_str_digest_735cb515f948a018f1e620ea33122570 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055099 ], 10, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_735cb515f948a018f1e620ea33122570_tuple, 0, const_str_digest_735cb515f948a018f1e620ea33122570 ); Py_INCREF( const_str_digest_735cb515f948a018f1e620ea33122570 );
    const_dict_15ab90b31dfaacebcd2e280ebae53401 = _PyDict_NewPresized( 1 );
    const_str_plain_parse_float = UNSTREAM_STRING_ASCII( &constant_bin[ 5055109 ], 11, 1 );
    PyDict_SetItem( const_dict_15ab90b31dfaacebcd2e280ebae53401, const_str_plain_parse_float, (PyObject *)&PyUnicode_Type );
    assert( PyDict_Size( const_dict_15ab90b31dfaacebcd2e280ebae53401 ) == 1 );
    const_str_plain_format_version = UNSTREAM_STRING_ASCII( &constant_bin[ 2734337 ], 14, 1 );
    const_tuple_befdf3706f7025480263df5d104f1caf_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_befdf3706f7025480263df5d104f1caf_tuple, 0, const_str_plain_gramtab_format ); Py_INCREF( const_str_plain_gramtab_format );
    const_str_plain_grammemes_filename = UNSTREAM_STRING_ASCII( &constant_bin[ 5055120 ], 18, 1 );
    PyTuple_SET_ITEM( const_tuple_befdf3706f7025480263df5d104f1caf_tuple, 1, const_str_plain_grammemes_filename ); Py_INCREF( const_str_plain_grammemes_filename );
    PyTuple_SET_ITEM( const_tuple_befdf3706f7025480263df5d104f1caf_tuple, 2, const_str_plain_Tag ); Py_INCREF( const_str_plain_Tag );
    PyTuple_SET_ITEM( const_tuple_befdf3706f7025480263df5d104f1caf_tuple, 3, const_str_plain_grammemes ); Py_INCREF( const_str_plain_grammemes );
    const_str_digest_4190a579127570ed281dc39447207faf = UNSTREAM_STRING_ASCII( &constant_bin[ 5055138 ], 21, 0 );
    const_tuple_str_digest_1bd09a763c8b38cd78af0e6a5d992c87_tuple = PyTuple_New( 1 );
    const_str_digest_1bd09a763c8b38cd78af0e6a5d992c87 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055159 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_1bd09a763c8b38cd78af0e6a5d992c87_tuple, 0, const_str_digest_1bd09a763c8b38cd78af0e6a5d992c87 ); Py_INCREF( const_str_digest_1bd09a763c8b38cd78af0e6a5d992c87 );
    const_str_plain_source_lexemes_count = UNSTREAM_STRING_ASCII( &constant_bin[ 5055183 ], 20, 1 );
    const_tuple_37004a6a826594e9e23ba550e6b62223_tuple = PyTuple_New( 16 );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 0, const_str_plain_compiled_dict ); Py_INCREF( const_str_plain_compiled_dict );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 1, const_str_plain_out_path ); Py_INCREF( const_str_plain_out_path );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 2, const_str_plain__f ); Py_INCREF( const_str_plain__f );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 3, const_str_plain_gramtab_formats ); Py_INCREF( const_str_plain_gramtab_formats );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 4, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 5, const_str_plain_Tag ); Py_INCREF( const_str_plain_Tag );
    const_str_plain_new_gramtab = UNSTREAM_STRING_ASCII( &constant_bin[ 5055203 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 6, const_str_plain_new_gramtab ); Py_INCREF( const_str_plain_new_gramtab );
    const_str_plain_gramtab_name = UNSTREAM_STRING_ASCII( &constant_bin[ 5055214 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 7, const_str_plain_gramtab_name ); Py_INCREF( const_str_plain_gramtab_name );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 8, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 9, const_str_plain_para ); Py_INCREF( const_str_plain_para );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 10, const_str_plain_prefix_id ); Py_INCREF( const_str_plain_prefix_id );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 11, const_str_plain_prediction_suffixes_dawg ); Py_INCREF( const_str_plain_prediction_suffixes_dawg );
    const_str_plain__dawg_len = UNSTREAM_STRING_ASCII( &constant_bin[ 5054824 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 12, const_str_plain__dawg_len ); Py_INCREF( const_str_plain__dawg_len );
    const_str_plain_words_dawg_len = UNSTREAM_STRING_ASCII( &constant_bin[ 5055226 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 13, const_str_plain_words_dawg_len ); Py_INCREF( const_str_plain_words_dawg_len );
    const_str_plain_prediction_suffixes_dawg_lenghts = UNSTREAM_STRING_ASCII( &constant_bin[ 5055240 ], 32, 1 );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 14, const_str_plain_prediction_suffixes_dawg_lenghts ); Py_INCREF( const_str_plain_prediction_suffixes_dawg_lenghts );
    PyTuple_SET_ITEM( const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 15, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    const_str_digest_77456c53d050fd605724e2705d57a3fe = UNSTREAM_STRING_ASCII( &constant_bin[ 5055272 ], 14, 0 );
    const_tuple_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98_tuple = PyTuple_New( 1 );
    const_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055286 ], 31, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98_tuple, 0, const_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98 ); Py_INCREF( const_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98 );
    const_str_plain_gramtab_length = UNSTREAM_STRING_ASCII( &constant_bin[ 5055317 ], 14, 1 );
    const_str_digest_f540b7c6a5c24848def98e8776abd3df = UNSTREAM_STRING_ASCII( &constant_bin[ 5055331 ], 20, 0 );
    const_str_plain_source_links_count = UNSTREAM_STRING_ASCII( &constant_bin[ 5055351 ], 18, 1 );
    const_str_plain__load_gramtab = UNSTREAM_STRING_ASCII( &constant_bin[ 5055369 ], 13, 1 );
    const_tuple_str_digest_f540b7c6a5c24848def98e8776abd3df_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_f540b7c6a5c24848def98e8776abd3df_tuple, 0, const_str_digest_f540b7c6a5c24848def98e8776abd3df ); Py_INCREF( const_str_digest_f540b7c6a5c24848def98e8776abd3df );
    const_str_digest_affe8b2e5cc38c825cf1b08041a52a4f = UNSTREAM_STRING_ASCII( &constant_bin[ 5055382 ], 16, 0 );
    const_str_plain_paradigm_prefixes_length = UNSTREAM_STRING_ASCII( &constant_bin[ 5055398 ], 24, 1 );
    const_str_plain_source_revision = UNSTREAM_STRING_ASCII( &constant_bin[ 5055422 ], 15, 1 );
    const_str_digest_7ba451d3f8a14c32d5340f541ccdc012 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055437 ], 9, 0 );
    const_str_plain_words_dawg = UNSTREAM_STRING_ASCII( &constant_bin[ 5051098 ], 10, 1 );
    const_str_plain_prediction_prefixes_dawg_length = UNSTREAM_STRING_ASCII( &constant_bin[ 5055446 ], 31, 1 );
    const_str_plain_LoadedDictionary = UNSTREAM_STRING_ASCII( &constant_bin[ 5055477 ], 16, 1 );
    const_tuple_str_digest_ffe84336574aab4705a07cd32f4567e9_tuple = PyTuple_New( 1 );
    const_str_digest_ffe84336574aab4705a07cd32f4567e9 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055493 ], 16, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_ffe84336574aab4705a07cd32f4567e9_tuple, 0, const_str_digest_ffe84336574aab4705a07cd32f4567e9 ); Py_INCREF( const_str_digest_ffe84336574aab4705a07cd32f4567e9 );
    const_tuple_str_digest_77456c53d050fd605724e2705d57a3fe_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_77456c53d050fd605724e2705d57a3fe_tuple, 0, const_str_digest_77456c53d050fd605724e2705d57a3fe ); Py_INCREF( const_str_digest_77456c53d050fd605724e2705d57a3fe );
    const_str_digest_a24a2f5d59e8cf71107ca1ce19436812 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055509 ], 117, 0 );
    const_tuple_str_digest_7ba451d3f8a14c32d5340f541ccdc012_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_7ba451d3f8a14c32d5340f541ccdc012_tuple, 0, const_str_digest_7ba451d3f8a14c32d5340f541ccdc012 ); Py_INCREF( const_str_digest_7ba451d3f8a14c32d5340f541ccdc012 );
    const_tuple_ba2246cfb8ba10b831875d9e12011b9d_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_ba2246cfb8ba10b831875d9e12011b9d_tuple, 0, const_str_plain_format_version ); Py_INCREF( const_str_plain_format_version );
    PyTuple_SET_ITEM( const_tuple_ba2246cfb8ba10b831875d9e12011b9d_tuple, 1, const_str_digest_d45b7aaa3187ef32d2beec74353f4cca ); Py_INCREF( const_str_digest_d45b7aaa3187ef32d2beec74353f4cca );
    const_str_plain_paradigms_length = UNSTREAM_STRING_ASCII( &constant_bin[ 5055626 ], 16, 1 );
    const_str_plain_paradigms_count = UNSTREAM_STRING_ASCII( &constant_bin[ 5055642 ], 15, 1 );
    const_str_digest_7d5d1392b0d186e0cb7195994bc0e765 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055657 ], 27, 0 );
    const_tuple_str_digest_95f6424560611d246f267f2ad8ca8874_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_95f6424560611d246f267f2ad8ca8874_tuple, 0, const_str_digest_95f6424560611d246f267f2ad8ca8874 ); Py_INCREF( const_str_digest_95f6424560611d246f267f2ad8ca8874 );
    const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 0, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 1, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 2, const_str_plain_format_version ); Py_INCREF( const_str_plain_format_version );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 3, const_str_plain_major ); Py_INCREF( const_str_plain_major );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 4, const_str_plain_minor ); Py_INCREF( const_str_plain_minor );
    const_str_plain_curr_major = UNSTREAM_STRING_ASCII( &constant_bin[ 5055684 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 5, const_str_plain_curr_major ); Py_INCREF( const_str_plain_curr_major );
    const_str_plain_curr_minor = UNSTREAM_STRING_ASCII( &constant_bin[ 5055694 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 6, const_str_plain_curr_minor ); Py_INCREF( const_str_plain_curr_minor );
    PyTuple_SET_ITEM( const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 7, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    const_str_plain_prediction_suffixes_dawg_lengths = UNSTREAM_STRING_ASCII( &constant_bin[ 5055704 ], 32, 1 );
    const_str_digest_2d0f3f34101c8d1ec2cf506fd8962036 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055736 ], 42, 0 );
    const_str_digest_ee2e795a82006414dbeef053d3c7c6e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055778 ], 66, 0 );
    const_str_plain_str_gramtab = UNSTREAM_STRING_ASCII( &constant_bin[ 5055844 ], 11, 1 );
    const_str_digest_362a54b311f40a17b3197e3b9f3e2ec1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055855 ], 102, 0 );
    const_list_str_plain_source_str_digest_f50366a6d32ef2492b3f85df1d0f8544_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_source_str_digest_f50366a6d32ef2492b3f85df1d0f8544_list, 0, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    const_str_digest_f50366a6d32ef2492b3f85df1d0f8544 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055957 ], 15, 0 );
    PyList_SET_ITEM( const_list_str_plain_source_str_digest_f50366a6d32ef2492b3f85df1d0f8544_list, 1, const_str_digest_f50366a6d32ef2492b3f85df1d0f8544 ); Py_INCREF( const_str_digest_f50366a6d32ef2492b3f85df1d0f8544 );
    const_tuple_str_plain_tag_str_plain_Tag_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_tag_str_plain_Tag_tuple, 0, const_str_plain_tag ); Py_INCREF( const_str_plain_tag );
    PyTuple_SET_ITEM( const_tuple_str_plain_tag_str_plain_Tag_tuple, 1, const_str_plain_Tag ); Py_INCREF( const_str_plain_Tag );
    const_tuple_700c29999bb988695267ade4566335c3_tuple = PyTuple_New( 15 );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 1, const_str_plain_gramtab_format ); Py_INCREF( const_str_plain_gramtab_format );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 2, const_str_plain__f ); Py_INCREF( const_str_plain__f );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 3, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 4, const_str_plain_Tag ); Py_INCREF( const_str_plain_Tag );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 5, const_str_plain_str_gramtab ); Py_INCREF( const_str_plain_str_gramtab );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 6, const_str_plain_gramtab ); Py_INCREF( const_str_plain_gramtab );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 7, const_str_plain_suffixes ); Py_INCREF( const_str_plain_suffixes );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 8, const_str_plain_paradigm_prefixes ); Py_INCREF( const_str_plain_paradigm_prefixes );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 9, const_str_plain_paradigms ); Py_INCREF( const_str_plain_paradigms );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 10, const_str_plain_words ); Py_INCREF( const_str_plain_words );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 11, const_str_plain_prediction_prefixes ); Py_INCREF( const_str_plain_prediction_prefixes );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 12, const_str_plain_prediction_suffixes_dawgs ); Py_INCREF( const_str_plain_prediction_suffixes_dawgs );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 13, const_str_plain_prefix_id ); Py_INCREF( const_str_plain_prefix_id );
    PyTuple_SET_ITEM( const_tuple_700c29999bb988695267ade4566335c3_tuple, 14, const_str_plain_fn ); Py_INCREF( const_str_plain_fn );
    const_tuple_str_plain_filename_str_plain_meta_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_filename_str_plain_meta_tuple, 0, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    PyTuple_SET_ITEM( const_tuple_str_plain_filename_str_plain_meta_tuple, 1, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    const_str_digest_3b990c06e7442baf7e5180e2fa57f730 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055862 ], 34, 0 );
    const_str_digest_5dffe1b8ac250873b7da4b62752cf207 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055972 ], 9, 0 );
    const_str_digest_4724d4b522be3f8f06676393f6c93087 = UNSTREAM_STRING_ASCII( &constant_bin[ 5055981 ], 59, 0 );
    const_str_digest_c53becbf4353e4da85be8e397d486c0b = UNSTREAM_STRING_ASCII( &constant_bin[ 5054796 ], 37, 0 );
    const_str_plain_compiled_at = UNSTREAM_STRING_ASCII( &constant_bin[ 5056040 ], 11, 1 );
    const_str_digest_3b6fbce621c67f99314fd35ccdf56314 = UNSTREAM_STRING_ASCII( &constant_bin[ 5056051 ], 15, 0 );
    const_str_digest_e07b892de4fffdf11e3fd8d0a63239b4 = UNSTREAM_STRING_ASCII( &constant_bin[ 5056066 ], 37, 0 );
    const_tuple_str_plain_json_write_str_plain_json_read_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_json_write_str_plain_json_read_tuple, 0, const_str_plain_json_write ); Py_INCREF( const_str_plain_json_write );
    PyTuple_SET_ITEM( const_tuple_str_plain_json_write_str_plain_json_read_tuple, 1, const_str_plain_json_read ); Py_INCREF( const_str_plain_json_read );
    const_str_digest_74531d15f942b19fea46429a1b996771 = UNSTREAM_STRING_ASCII( &constant_bin[ 5056103 ], 22, 0 );
    const_dict_5d3329810349a48ba364b65708c4137e = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_5d3329810349a48ba364b65708c4137e, const_str_plain_indent, const_int_pos_4 );
    assert( PyDict_Size( const_dict_5d3329810349a48ba364b65708c4137e ) == 1 );
    const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 0, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    PyTuple_SET_ITEM( const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 1, const_str_plain_paradigms ); Py_INCREF( const_str_plain_paradigms );
    PyTuple_SET_ITEM( const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 2, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 3, const_str_plain_paradigms_count ); Py_INCREF( const_str_plain_paradigms_count );
    PyTuple_SET_ITEM( const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 4, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 5, const_str_plain_paradigm_len ); Py_INCREF( const_str_plain_paradigm_len );
    PyTuple_SET_ITEM( const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 6, const_str_plain_para ); Py_INCREF( const_str_plain_para );
    const_tuple_str_digest_4e9682f5d714f894d265491bc430d2ad_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4e9682f5d714f894d265491bc430d2ad_tuple, 0, const_str_digest_4e9682f5d714f894d265491bc430d2ad ); Py_INCREF( const_str_digest_4e9682f5d714f894d265491bc430d2ad );
    const_str_digest_8ee69f7274ac2df4c3e435ad05a88f27 = UNSTREAM_STRING_ASCII( &constant_bin[ 5056125 ], 43, 0 );
    const_str_digest_75d6424facd0bcab0f0d95042aa846f1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5056168 ], 62, 0 );
    const_str_digest_49e5da426d804b69a49162ed40a8b58f = UNSTREAM_STRING_ASCII( &constant_bin[ 5056230 ], 27, 0 );
    const_tuple_str_digest_5dffe1b8ac250873b7da4b62752cf207_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_5dffe1b8ac250873b7da4b62752cf207_tuple, 0, const_str_digest_5dffe1b8ac250873b7da4b62752cf207 ); Py_INCREF( const_str_digest_5dffe1b8ac250873b7da4b62752cf207 );
    const_tuple_str_plain_tagset_tuple = PyTuple_New( 1 );
    const_str_plain_tagset = UNSTREAM_STRING_ASCII( &constant_bin[ 5046632 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_tagset_tuple, 0, const_str_plain_tagset ); Py_INCREF( const_str_plain_tagset );
    const_str_plain_words_dawg_length = UNSTREAM_STRING_ASCII( &constant_bin[ 5056257 ], 17, 1 );
    const_str_plain_suffixes_length = UNSTREAM_STRING_ASCII( &constant_bin[ 5056274 ], 15, 1 );
    const_str_plain_CURRENT_FORMAT_VERSION = UNSTREAM_STRING_ASCII( &constant_bin[ 5056289 ], 22, 1 );
    const_str_digest_ea3b39b1eb560d67c3a621e2ee6b48a7 = UNSTREAM_STRING_ASCII( &constant_bin[ 5056311 ], 36, 0 );
    const_tuple_str_digest_74531d15f942b19fea46429a1b996771_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_74531d15f942b19fea46429a1b996771_tuple, 0, const_str_digest_74531d15f942b19fea46429a1b996771 ); Py_INCREF( const_str_digest_74531d15f942b19fea46429a1b996771 );
    const_tuple_str_digest_3b990c06e7442baf7e5180e2fa57f730_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_3b990c06e7442baf7e5180e2fa57f730_tuple, 0, const_str_digest_3b990c06e7442baf7e5180e2fa57f730 ); Py_INCREF( const_str_digest_3b990c06e7442baf7e5180e2fa57f730 );
    const_str_plain__load_paradigms = UNSTREAM_STRING_ASCII( &constant_bin[ 5056347 ], 15, 1 );
    const_str_digest_7749eac3a559542096769a3088266128 = UNSTREAM_STRING_ASCII( &constant_bin[ 1062918 ], 3, 0 );
    const_list_a3c633733c653128003d731da37f55a5_list = PyList_New( 9 );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 0, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 1, const_str_plain_gramtab ); Py_INCREF( const_str_plain_gramtab );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 2, const_str_plain_suffixes ); Py_INCREF( const_str_plain_suffixes );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 3, const_str_plain_paradigms ); Py_INCREF( const_str_plain_paradigms );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 4, const_str_plain_words ); Py_INCREF( const_str_plain_words );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 5, const_str_plain_prediction_prefixes ); Py_INCREF( const_str_plain_prediction_prefixes );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 6, const_str_plain_prediction_suffixes_dawgs ); Py_INCREF( const_str_plain_prediction_suffixes_dawgs );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 7, const_str_plain_Tag ); Py_INCREF( const_str_plain_Tag );
    PyList_SET_ITEM( const_list_a3c633733c653128003d731da37f55a5_list, 8, const_str_plain_paradigm_prefixes ); Py_INCREF( const_str_plain_paradigm_prefixes );
    const_str_plain_source_version = UNSTREAM_STRING_ASCII( &constant_bin[ 5056362 ], 14, 1 );
    const_str_digest_cd8399df6542e7b2ff2d6ac1e3b6c520 = UNSTREAM_STRING_ASCII( &constant_bin[ 5056376 ], 26, 0 );
    const_tuple_str_plain_tag_str_str_plain_Tag_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_tag_str_str_plain_Tag_tuple, 0, const_str_plain_tag_str ); Py_INCREF( const_str_plain_tag_str );
    PyTuple_SET_ITEM( const_tuple_str_plain_tag_str_str_plain_Tag_tuple, 1, const_str_plain_Tag ); Py_INCREF( const_str_plain_Tag );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pymorphy2$opencorpora_dict$storage( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_4007b7e640a295d14887314b70082670;
static PyCodeObject *codeobj_2fbae624d3388521275076f86e01e1a6;
static PyCodeObject *codeobj_b6ea40c79e2667303f75ee5298d1597e;
static PyCodeObject *codeobj_fbf83c2d8181f7d6f57d3d9781a0e68f;
static PyCodeObject *codeobj_f257632e5874e7152b8fb440b1064ff7;
static PyCodeObject *codeobj_29a14529e89ac976cd103e3b22bb9431;
static PyCodeObject *codeobj_e2c7b701efdfb0059bde11253f0f4107;
static PyCodeObject *codeobj_7a5eb3cc50852796d75056d49deaff04;
static PyCodeObject *codeobj_cb2379d30777ca5f4d3803a0f343de28;
static PyCodeObject *codeobj_b680e6cef3cf581d6a93684a16e0a392;
static PyCodeObject *codeobj_5acff70762873e0da2a4fe71148e3260;
static PyCodeObject *codeobj_76bd878f799e10911e0aea01e3d8408d;
static PyCodeObject *codeobj_0ec95cd40c3f582dbea240b706615488;
static PyCodeObject *codeobj_d582e54ee741d26b1dc33273d1b13c4f;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_e07b892de4fffdf11e3fd8d0a63239b4 );
    codeobj_4007b7e640a295d14887314b70082670 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 110, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_k_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2fbae624d3388521275076f86e01e1a6 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 43, const_tuple_str_plain_p_str_plain_path_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b6ea40c79e2667303f75ee5298d1597e = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 77, const_tuple_str_plain_path_str_plain_out_path_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_fbf83c2d8181f7d6f57d3d9781a0e68f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 84, const_tuple_str_plain_tag_str_plain_Tag_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f257632e5874e7152b8fb440b1064ff7 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 51, const_tuple_str_plain_tag_str_str_plain_Tag_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_29a14529e89ac976cd103e3b22bb9431 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8ee69f7274ac2df4c3e435ad05a88f27, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e2c7b701efdfb0059bde11253f0f4107 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__assert_format_is_compatible, 194, const_tuple_34df23eaddb3aeb3f24c6bfcb12f41db_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7a5eb3cc50852796d75056d49deaff04 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__dawg_len, 109, const_tuple_str_plain_dawg_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_cb2379d30777ca5f4d3803a0f343de28 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__load_gramtab, 168, const_tuple_050dd7804c0479823b6978945f649ea6_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b680e6cef3cf581d6a93684a16e0a392 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__load_meta, 146, const_tuple_str_plain_filename_str_plain_meta_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5acff70762873e0da2a4fe71148e3260 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__load_paradigms, 178, const_tuple_346ff4942317aaa8c350f3c029efffd8_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_76bd878f799e10911e0aea01e3d8408d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__load_tag_class, 154, const_tuple_befdf3706f7025480263df5d104f1caf_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0ec95cd40c3f582dbea240b706615488 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load_dict, 37, const_tuple_700c29999bb988695267ade4566335c3_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d582e54ee741d26b1dc33273d1b13c4f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_save_compiled_dict, 71, const_tuple_37004a6a826594e9e23ba550e6b62223_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_maker( void );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_3__load_meta(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_4__load_tag_class(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_5__load_gramtab(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms(  );


static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_7__assert_format_is_compatible(  );


// The module function definitions.
static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_path = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *par_gramtab_format = python_pars[ 1 ];
    PyObject *var__f = NULL;
    PyObject *var_meta = NULL;
    PyObject *var_Tag = NULL;
    PyObject *var_str_gramtab = NULL;
    PyObject *var_gramtab = NULL;
    PyObject *var_suffixes = NULL;
    PyObject *var_paradigm_prefixes = NULL;
    PyObject *var_paradigms = NULL;
    PyObject *var_words = NULL;
    PyObject *var_prediction_prefixes = NULL;
    PyObject *var_prediction_suffixes_dawgs = NULL;
    PyObject *var_prefix_id = NULL;
    PyObject *var_fn = NULL;
    PyObject *outline_0_var_tag_str = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_0ec95cd40c3f582dbea240b706615488;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_f257632e5874e7152b8fb440b1064ff7_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_f257632e5874e7152b8fb440b1064ff7_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_0ec95cd40c3f582dbea240b706615488 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict$$$function_1_lambda(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_path;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var__f == NULL );
        var__f = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0ec95cd40c3f582dbea240b706615488, codeobj_0ec95cd40c3f582dbea240b706615488, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0ec95cd40c3f582dbea240b706615488 = cache_frame_0ec95cd40c3f582dbea240b706615488;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0ec95cd40c3f582dbea240b706615488 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0ec95cd40c3f582dbea240b706615488 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_meta );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__load_meta );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_load_meta" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var__f );
        tmp_called_name_2 = var__f;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 45;
        tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_digest_7ba451d3f8a14c32d5340f541ccdc012_tuple, 0 ) );

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_meta == NULL );
        var_meta = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__assert_format_is_compatible );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__assert_format_is_compatible );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_assert_format_is_compatible" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_2;
        CHECK_OBJECT( var_meta );
        tmp_args_element_name_2 = var_meta;
        CHECK_OBJECT( PyCell_GET( par_path ) );
        tmp_args_element_name_3 = PyCell_GET( par_path );
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_5;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_tag_class );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__load_tag_class );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_load_tag_class" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_3;
        CHECK_OBJECT( par_gramtab_format );
        tmp_args_element_name_4 = par_gramtab_format;
        CHECK_OBJECT( var__f );
        tmp_called_name_5 = var__f;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 48;
        tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_str_digest_77456c53d050fd605724e2705d57a3fe_tuple, 0 ) );

        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_Tag == NULL );
        var_Tag = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_gramtab );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__load_gramtab );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_load_gramtab" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_4;
        CHECK_OBJECT( var_meta );
        tmp_args_element_name_6 = var_meta;
        CHECK_OBJECT( par_gramtab_format );
        tmp_args_element_name_7 = par_gramtab_format;
        CHECK_OBJECT( PyCell_GET( par_path ) );
        tmp_args_element_name_8 = PyCell_GET( par_path );
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 50;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, call_args );
        }

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_str_gramtab == NULL );
        var_str_gramtab = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( var_str_gramtab );
            tmp_iter_arg_1 = var_str_gramtab;
            tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_1 = "coooooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_6;
        }
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_f257632e5874e7152b8fb440b1064ff7_2, codeobj_f257632e5874e7152b8fb440b1064ff7, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *) );
        frame_f257632e5874e7152b8fb440b1064ff7_2 = cache_frame_f257632e5874e7152b8fb440b1064ff7_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_f257632e5874e7152b8fb440b1064ff7_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_f257632e5874e7152b8fb440b1064ff7_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 51;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_9 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_tag_str;
                outline_0_var_tag_str = tmp_assign_source_9;
                Py_INCREF( outline_0_var_tag_str );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( var_Tag );
            tmp_called_name_7 = var_Tag;
            CHECK_OBJECT( outline_0_var_tag_str );
            tmp_args_element_name_9 = outline_0_var_tag_str;
            frame_f257632e5874e7152b8fb440b1064ff7_2->m_frame.f_lineno = 51;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_5 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_5 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_1_load_dict );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_f257632e5874e7152b8fb440b1064ff7_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_f257632e5874e7152b8fb440b1064ff7_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_f257632e5874e7152b8fb440b1064ff7_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_f257632e5874e7152b8fb440b1064ff7_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_f257632e5874e7152b8fb440b1064ff7_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_f257632e5874e7152b8fb440b1064ff7_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_f257632e5874e7152b8fb440b1064ff7_2,
            type_description_2,
            outline_0_var_tag_str,
            var_Tag
        );


        // Release cached frame.
        if ( frame_f257632e5874e7152b8fb440b1064ff7_2 == cache_frame_f257632e5874e7152b8fb440b1064ff7_2 )
        {
            Py_DECREF( frame_f257632e5874e7152b8fb440b1064ff7_2 );
        }
        cache_frame_f257632e5874e7152b8fb440b1064ff7_2 = NULL;

        assertFrameObject( frame_f257632e5874e7152b8fb440b1064ff7_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "coooooooooooooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_1_load_dict );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_tag_str );
        outline_0_var_tag_str = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_tag_str );
        outline_0_var_tag_str = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_1_load_dict );
        return NULL;
        outline_exception_1:;
        exception_lineno = 51;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_gramtab == NULL );
        var_gramtab = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_9;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_read );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_read );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_read" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_5;
        CHECK_OBJECT( var__f );
        tmp_called_name_9 = var__f;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 53;
        tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_digest_95f6424560611d246f267f2ad8ca8874_tuple, 0 ) );

        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_suffixes == NULL );
        var_suffixes = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_11;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_read );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_read );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_read" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_6;
        CHECK_OBJECT( var__f );
        tmp_called_name_11 = var__f;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 54;
        tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_digest_74531d15f942b19fea46429a1b996771_tuple, 0 ) );

        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 54;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_paradigm_prefixes == NULL );
        var_paradigm_prefixes = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_13;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_paradigms );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__load_paradigms );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_load_paradigms" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_7;
        CHECK_OBJECT( var__f );
        tmp_called_name_13 = var__f;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 55;
        tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_digest_461ec8c61dee4c2c2dcfcc984f28c487_tuple, 0 ) );

        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_paradigms == NULL );
        var_paradigms = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_name_14;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_called_name_15;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_dawg );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dawg );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dawg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 56;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_8;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 56;
        tmp_source_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_WordsDawg );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_load );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var__f );
        tmp_called_name_15 = var__f;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 56;
        tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_digest_735cb515f948a018f1e620ea33122570_tuple, 0 ) );

        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 56;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 56;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_words == NULL );
        var_words = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_called_name_16;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_called_name_17;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_dawg );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dawg );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dawg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_9;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 58;
        tmp_source_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_DAWG );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_load );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var__f );
        tmp_called_name_17 = var__f;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 58;
        tmp_args_element_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_digest_1bd09a763c8b38cd78af0e6a5d992c87_tuple, 0 ) );

        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_16 );

            exception_lineno = 58;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 58;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_assign_source_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_prediction_prefixes == NULL );
        var_prediction_prefixes = tmp_assign_source_14;
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = PyList_New( 0 );
        assert( var_prediction_suffixes_dawgs == NULL );
        var_prediction_suffixes_dawgs = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( var_paradigm_prefixes );
        tmp_len_arg_1 = var_paradigm_prefixes;
        tmp_xrange_low_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_xrange_low_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_2 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        Py_DECREF( tmp_xrange_low_1 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_16;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_17;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_2 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_17 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_17 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "coooooooooooooo";
                exception_lineno = 61;
                goto try_except_handler_4;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_17;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_18;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_18 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_prefix_id;
            var_prefix_id = tmp_assign_source_18;
            Py_INCREF( var_prefix_id );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_called_name_18;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( var__f );
        tmp_called_name_18 = var__f;
        tmp_left_name_1 = const_str_digest_7d5d1392b0d186e0cb7195994bc0e765;
        CHECK_OBJECT( var_prefix_id );
        tmp_right_name_1 = var_prefix_id;
        tmp_args_element_name_15 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 62;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        {
            PyObject *old = var_fn;
            var_fn = tmp_assign_source_19;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_16;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }

        tmp_source_name_3 = tmp_mvar_value_10;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_path );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_fn );
        tmp_args_element_name_16 = var_fn;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_exists, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 63;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_19;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_18;
        CHECK_OBJECT( var_prediction_suffixes_dawgs );
        tmp_source_name_4 = var_prediction_suffixes_dawgs;
        tmp_called_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
        if ( tmp_called_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_dawg );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dawg );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dawg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }

        tmp_called_instance_5 = tmp_mvar_value_11;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 64;
        tmp_called_instance_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_PredictionSuffixesDAWG );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_19 );

            exception_lineno = 64;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_fn );
        tmp_args_element_name_18 = var_fn;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_18 };
            tmp_args_element_name_17 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_load, call_args );
        }

        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_19 );

            exception_lineno = 64;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_17 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "coooooooooooooo";
            goto try_except_handler_4;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 61;
        type_description_1 = "coooooooooooooo";
        goto try_except_handler_4;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_args_element_name_27;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_LoadedDictionary );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LoadedDictionary );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LoadedDictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_12;
        CHECK_OBJECT( var_meta );
        tmp_args_element_name_19 = var_meta;
        CHECK_OBJECT( var_gramtab );
        tmp_args_element_name_20 = var_gramtab;
        CHECK_OBJECT( var_suffixes );
        tmp_args_element_name_21 = var_suffixes;
        CHECK_OBJECT( var_paradigms );
        tmp_args_element_name_22 = var_paradigms;
        CHECK_OBJECT( var_words );
        tmp_args_element_name_23 = var_words;
        CHECK_OBJECT( var_prediction_prefixes );
        tmp_args_element_name_24 = var_prediction_prefixes;
        CHECK_OBJECT( var_prediction_suffixes_dawgs );
        tmp_args_element_name_25 = var_prediction_suffixes_dawgs;
        CHECK_OBJECT( var_Tag );
        tmp_args_element_name_26 = var_Tag;
        CHECK_OBJECT( var_paradigm_prefixes );
        tmp_args_element_name_27 = var_paradigm_prefixes;
        frame_0ec95cd40c3f582dbea240b706615488->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_args_element_name_19, tmp_args_element_name_20, tmp_args_element_name_21, tmp_args_element_name_22, tmp_args_element_name_23, tmp_args_element_name_24, tmp_args_element_name_25, tmp_args_element_name_26, tmp_args_element_name_27 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS9( tmp_called_name_20, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "coooooooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ec95cd40c3f582dbea240b706615488 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ec95cd40c3f582dbea240b706615488 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ec95cd40c3f582dbea240b706615488 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0ec95cd40c3f582dbea240b706615488, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0ec95cd40c3f582dbea240b706615488->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0ec95cd40c3f582dbea240b706615488, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0ec95cd40c3f582dbea240b706615488,
        type_description_1,
        par_path,
        par_gramtab_format,
        var__f,
        var_meta,
        var_Tag,
        var_str_gramtab,
        var_gramtab,
        var_suffixes,
        var_paradigm_prefixes,
        var_paradigms,
        var_words,
        var_prediction_prefixes,
        var_prediction_suffixes_dawgs,
        var_prefix_id,
        var_fn
    );


    // Release cached frame.
    if ( frame_0ec95cd40c3f582dbea240b706615488 == cache_frame_0ec95cd40c3f582dbea240b706615488 )
    {
        Py_DECREF( frame_0ec95cd40c3f582dbea240b706615488 );
    }
    cache_frame_0ec95cd40c3f582dbea240b706615488 = NULL;

    assertFrameObject( frame_0ec95cd40c3f582dbea240b706615488 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_1_load_dict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_gramtab_format );
    Py_DECREF( par_gramtab_format );
    par_gramtab_format = NULL;

    CHECK_OBJECT( (PyObject *)var__f );
    Py_DECREF( var__f );
    var__f = NULL;

    CHECK_OBJECT( (PyObject *)var_meta );
    Py_DECREF( var_meta );
    var_meta = NULL;

    CHECK_OBJECT( (PyObject *)var_Tag );
    Py_DECREF( var_Tag );
    var_Tag = NULL;

    CHECK_OBJECT( (PyObject *)var_str_gramtab );
    Py_DECREF( var_str_gramtab );
    var_str_gramtab = NULL;

    CHECK_OBJECT( (PyObject *)var_gramtab );
    Py_DECREF( var_gramtab );
    var_gramtab = NULL;

    CHECK_OBJECT( (PyObject *)var_suffixes );
    Py_DECREF( var_suffixes );
    var_suffixes = NULL;

    CHECK_OBJECT( (PyObject *)var_paradigm_prefixes );
    Py_DECREF( var_paradigm_prefixes );
    var_paradigm_prefixes = NULL;

    CHECK_OBJECT( (PyObject *)var_paradigms );
    Py_DECREF( var_paradigms );
    var_paradigms = NULL;

    CHECK_OBJECT( (PyObject *)var_words );
    Py_DECREF( var_words );
    var_words = NULL;

    CHECK_OBJECT( (PyObject *)var_prediction_prefixes );
    Py_DECREF( var_prediction_prefixes );
    var_prediction_prefixes = NULL;

    CHECK_OBJECT( (PyObject *)var_prediction_suffixes_dawgs );
    Py_DECREF( var_prediction_suffixes_dawgs );
    var_prediction_suffixes_dawgs = NULL;

    Py_XDECREF( var_prefix_id );
    var_prefix_id = NULL;

    Py_XDECREF( var_fn );
    var_fn = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_gramtab_format );
    Py_DECREF( par_gramtab_format );
    par_gramtab_format = NULL;

    CHECK_OBJECT( (PyObject *)var__f );
    Py_DECREF( var__f );
    var__f = NULL;

    Py_XDECREF( var_meta );
    var_meta = NULL;

    Py_XDECREF( var_Tag );
    var_Tag = NULL;

    Py_XDECREF( var_str_gramtab );
    var_str_gramtab = NULL;

    Py_XDECREF( var_gramtab );
    var_gramtab = NULL;

    Py_XDECREF( var_suffixes );
    var_suffixes = NULL;

    Py_XDECREF( var_paradigm_prefixes );
    var_paradigm_prefixes = NULL;

    Py_XDECREF( var_paradigms );
    var_paradigms = NULL;

    Py_XDECREF( var_words );
    var_words = NULL;

    Py_XDECREF( var_prediction_prefixes );
    var_prediction_prefixes = NULL;

    Py_XDECREF( var_prediction_suffixes_dawgs );
    var_prediction_suffixes_dawgs = NULL;

    Py_XDECREF( var_prefix_id );
    var_prefix_id = NULL;

    Py_XDECREF( var_fn );
    var_fn = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_1_load_dict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_p = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2fbae624d3388521275076f86e01e1a6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2fbae624d3388521275076f86e01e1a6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2fbae624d3388521275076f86e01e1a6, codeobj_2fbae624d3388521275076f86e01e1a6, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *) );
    frame_2fbae624d3388521275076f86e01e1a6 = cache_frame_2fbae624d3388521275076f86e01e1a6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2fbae624d3388521275076f86e01e1a6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2fbae624d3388521275076f86e01e1a6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "path" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_p );
        tmp_args_element_name_2 = par_p;
        frame_2fbae624d3388521275076f86e01e1a6->m_frame.f_lineno = 43;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2fbae624d3388521275076f86e01e1a6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2fbae624d3388521275076f86e01e1a6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2fbae624d3388521275076f86e01e1a6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2fbae624d3388521275076f86e01e1a6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2fbae624d3388521275076f86e01e1a6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2fbae624d3388521275076f86e01e1a6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2fbae624d3388521275076f86e01e1a6,
        type_description_1,
        par_p,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_2fbae624d3388521275076f86e01e1a6 == cache_frame_2fbae624d3388521275076f86e01e1a6 )
    {
        Py_DECREF( frame_2fbae624d3388521275076f86e01e1a6 );
    }
    cache_frame_2fbae624d3388521275076f86e01e1a6 = NULL;

    assertFrameObject( frame_2fbae624d3388521275076f86e01e1a6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_1_load_dict$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_p );
    Py_DECREF( par_p );
    par_p = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_p );
    Py_DECREF( par_p );
    par_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_1_load_dict$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_compiled_dict = python_pars[ 0 ];
    struct Nuitka_CellObject *par_out_path = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *var__f = NULL;
    PyObject *var_gramtab_formats = NULL;
    PyObject *var_format = NULL;
    PyObject *var_Tag = NULL;
    PyObject *var_new_gramtab = NULL;
    PyObject *var_gramtab_name = NULL;
    PyObject *var_f = NULL;
    PyObject *var_para = NULL;
    PyObject *var_prefix_id = NULL;
    PyObject *var_prediction_suffixes_dawg = NULL;
    PyObject *var__dawg_len = NULL;
    PyObject *var_words_dawg_len = NULL;
    PyObject *var_prediction_suffixes_dawg_lenghts = NULL;
    PyObject *var_meta = NULL;
    PyObject *outline_0_var_tag = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_for_loop_4__for_iterator = NULL;
    PyObject *tmp_for_loop_4__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_d582e54ee741d26b1dc33273d1b13c4f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    struct Nuitka_FrameObject *frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    static struct Nuitka_FrameObject *cache_frame_d582e54ee741d26b1dc33273d1b13c4f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d582e54ee741d26b1dc33273d1b13c4f, codeobj_d582e54ee741d26b1dc33273d1b13c4f, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d582e54ee741d26b1dc33273d1b13c4f = cache_frame_d582e54ee741d26b1dc33273d1b13c4f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d582e54ee741d26b1dc33273d1b13c4f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d582e54ee741d26b1dc33273d1b13c4f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_logger );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 76;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_info, &PyTuple_GET_ITEM( const_tuple_str_digest_5dffe1b8ac250873b7da4b62752cf207_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_1_lambda(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_out_path;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var__f == NULL );
        var__f = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_write );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_write );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_write" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( var__f );
        tmp_called_name_2 = var__f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 79;
        tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_digest_77456c53d050fd605724e2705d57a3fe_tuple, 0 ) );

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_2 = par_compiled_dict;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parsed_dict );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 79;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_grammemes );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 79;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 79;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( var_gramtab_formats == NULL );
        var_gramtab_formats = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_tagset );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tagset );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tagset" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_registry );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 82;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_items );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ocoooooooooooooo";
                exception_lineno = 82;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_5 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ocoooooooooooooo";
            exception_lineno = 82;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ocoooooooooooooo";
            exception_lineno = 82;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ocoooooooooooooo";
                    exception_lineno = 82;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ocoooooooooooooo";
            exception_lineno = 82;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_format;
            var_format = tmp_assign_source_8;
            Py_INCREF( var_format );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_Tag;
            var_Tag = tmp_assign_source_9;
            Py_INCREF( var_Tag );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( var_Tag );
        tmp_source_name_4 = var_Tag;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__init_grammemes );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_6 = par_compiled_dict;
        tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_parsed_dict );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 83;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_grammemes );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 83;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_assign_source_10;
        // Tried code:
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( par_compiled_dict );
            tmp_source_name_7 = par_compiled_dict;
            tmp_iter_arg_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_gramtab );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;
                type_description_1 = "ocoooooooooooooo";
                goto try_except_handler_5;
            }
            tmp_assign_source_11 = MAKE_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;
                type_description_1 = "ocoooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_listcomp_1__$0;
                tmp_listcomp_1__$0 = tmp_assign_source_11;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = PyList_New( 0 );
            {
                PyObject *old = tmp_listcomp_1__contraction;
                tmp_listcomp_1__contraction = tmp_assign_source_12;
                Py_XDECREF( old );
            }

        }
        MAKE_OR_REUSE_FRAME( cache_frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2, codeobj_fbf83c2d8181f7d6f57d3d9781a0e68f, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *) );
        frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 = cache_frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_13;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_2 = tmp_listcomp_1__$0;
            tmp_assign_source_13 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 84;
                    goto try_except_handler_6;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_13;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_14;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_14 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_tag;
                outline_0_var_tag = tmp_assign_source_14;
                Py_INCREF( outline_0_var_tag );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( var_Tag );
            tmp_called_instance_3 = var_Tag;
            CHECK_OBJECT( outline_0_var_tag );
            tmp_args_element_name_4 = outline_0_var_tag;
            frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2->m_frame.f_lineno = 84;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_append_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain__from_internal_tag, call_args );
            }

            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;
                type_description_2 = "oo";
                goto try_except_handler_6;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;
                type_description_2 = "oo";
                goto try_except_handler_6;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_2 = "oo";
            goto try_except_handler_6;
        }
        goto loop_start_2;
        loop_end_2:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_10 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_10 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict );
        return NULL;
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_5;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2,
            type_description_2,
            outline_0_var_tag,
            var_Tag
        );


        // Release cached frame.
        if ( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 == cache_frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 )
        {
            Py_DECREF( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 );
        }
        cache_frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 = NULL;

        assertFrameObject( frame_fbf83c2d8181f7d6f57d3d9781a0e68f_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ocoooooooooooooo";
        goto try_except_handler_5;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        Py_XDECREF( outline_0_var_tag );
        outline_0_var_tag = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_tag );
        outline_0_var_tag = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict );
        return NULL;
        outline_exception_1:;
        exception_lineno = 84;
        goto try_except_handler_2;
        outline_result_1:;
        {
            PyObject *old = var_new_gramtab;
            var_new_gramtab = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_3b6fbce621c67f99314fd35ccdf56314;
        CHECK_OBJECT( var_format );
        tmp_right_name_1 = var_format;
        tmp_assign_source_15 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_gramtab_name;
            var_gramtab_name = tmp_assign_source_15;
            Py_XDECREF( old );
        }

    }
    CHECK_OBJECT( var_gramtab_name );
    tmp_dictset_value = var_gramtab_name;
    CHECK_OBJECT( var_gramtab_formats );
    tmp_dictset_dict = var_gramtab_formats;
    CHECK_OBJECT( var_format );
    tmp_dictset_key = var_format;
    tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 87;
        type_description_1 = "ocoooooooooooooo";
        goto try_except_handler_2;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_write );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_write );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_write" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        CHECK_OBJECT( var__f );
        tmp_called_name_5 = var__f;
        CHECK_OBJECT( var_gramtab_name );
        tmp_args_element_name_6 = var_gramtab_name;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_new_gramtab );
        tmp_args_element_name_7 = var_new_gramtab;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_7 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 82;
        type_description_1 = "ocoooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_open_filename_1;
        PyObject *tmp_called_name_6;
        PyObject *tmp_open_mode_1;
        CHECK_OBJECT( var__f );
        tmp_called_name_6 = var__f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 91;
        tmp_open_filename_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_digest_461ec8c61dee4c2c2dcfcc984f28c487_tuple, 0 ) );

        if ( tmp_open_filename_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_7;
        }
        tmp_open_mode_1 = const_str_plain_wb;
        tmp_assign_source_16 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
        Py_DECREF( tmp_open_filename_1 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_7;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_16;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_name_7;
        PyObject *tmp_source_name_8;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_8 = tmp_with_1__source;
        tmp_called_name_7 = LOOKUP_SPECIAL( tmp_source_name_8, const_str_plain___enter__ );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_7;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 91;
        tmp_assign_source_17 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
        Py_DECREF( tmp_called_name_7 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_7;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_17;
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_9 = tmp_with_1__source;
        tmp_assign_source_18 = LOOKUP_SPECIAL( tmp_source_name_9, const_str_plain___exit__ );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_7;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_18;
    }
    {
        nuitka_bool tmp_assign_source_19;
        tmp_assign_source_19 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_19;
    }
    {
        PyObject *tmp_assign_source_20;
        CHECK_OBJECT( tmp_with_1__enter );
        tmp_assign_source_20 = tmp_with_1__enter;
        assert( var_f == NULL );
        Py_INCREF( tmp_assign_source_20 );
        var_f = tmp_assign_source_20;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_name_8;
        PyObject *tmp_source_name_10;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_12;
        CHECK_OBJECT( var_f );
        tmp_source_name_10 = var_f;
        tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_write );
        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_struct );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_struct );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "struct" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }

        tmp_source_name_11 = tmp_mvar_value_5;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_pack );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 92;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_args_element_name_9 = const_str_digest_969c69e9fbc42cc6a8915b0b5d93fbb0;
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_12 = par_compiled_dict;
        tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_paradigms );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_9 );

            exception_lineno = 92;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_args_element_name_10 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_9 );

            exception_lineno = 92;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_args_element_name_8 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 92;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_iter_arg_4;
        PyObject *tmp_source_name_13;
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_13 = par_compiled_dict;
        tmp_iter_arg_4 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_paradigms );
        if ( tmp_iter_arg_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_assign_source_21 = MAKE_ITERATOR( tmp_iter_arg_4 );
        Py_DECREF( tmp_iter_arg_4 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_9;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_21;
    }
    // Tried code:
    loop_start_3:;
    {
        PyObject *tmp_next_source_3;
        PyObject *tmp_assign_source_22;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_3 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_22 = ITERATOR_NEXT( tmp_next_source_3 );
        if ( tmp_assign_source_22 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_3;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ocoooooooooooooo";
                exception_lineno = 93;
                goto try_except_handler_10;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_22;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_23;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_23 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_para;
            var_para = tmp_assign_source_23;
            Py_INCREF( var_para );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_10;
        PyObject *tmp_source_name_14;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_11;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_len_arg_2;
        CHECK_OBJECT( var_f );
        tmp_source_name_14 = var_f;
        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_write );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_10;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_struct );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_struct );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "struct" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_10;
        }

        tmp_source_name_15 = tmp_mvar_value_6;
        tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_pack );
        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 94;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_10;
        }
        tmp_args_element_name_12 = const_str_digest_969c69e9fbc42cc6a8915b0b5d93fbb0;
        CHECK_OBJECT( var_para );
        tmp_len_arg_2 = var_para;
        tmp_args_element_name_13 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_11 );

            exception_lineno = 94;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_10;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 94;
        {
            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13 };
            tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 94;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_10;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 94;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_10;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_14;
        CHECK_OBJECT( var_para );
        tmp_called_instance_4 = var_para;
        CHECK_OBJECT( var_f );
        tmp_args_element_name_14 = var_f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 95;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_tofile, call_args );
        }

        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_10;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 93;
        type_description_1 = "ocoooooooooooooo";
        goto try_except_handler_10;
    }
    goto loop_start_3;
    loop_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_9;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    goto try_end_5;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_d582e54ee741d26b1dc33273d1b13c4f, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != 0 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_d582e54ee741d26b1dc33273d1b13c4f, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PyException_SetTraceback( exception_keeper_value_7, (PyObject *)exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_11;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_24;
            tmp_assign_source_24 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_24;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_args_element_name_16;
            PyObject *tmp_args_element_name_17;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_12 = tmp_with_1__exit;
            tmp_args_element_name_15 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_16 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_17 = EXC_TRACEBACK(PyThreadState_GET());
            frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 93;
            {
                PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16, tmp_args_element_name_17 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_12, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ocoooooooooooooo";
                goto try_except_handler_11;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ocoooooooooooooo";
                goto try_except_handler_11;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 93;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame) frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_11;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 91;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame) frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ocoooooooooooooo";
        goto try_except_handler_11;
        branch_end_1:;
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_8;
    // End of try:
    try_end_6:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_5;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict );
    return NULL;
    // End of try:
    try_end_5:;
    goto try_end_7;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_13;
            PyObject *tmp_call_result_8;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_13 = tmp_with_1__exit;
            frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 93;
            tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_9 );
                Py_XDECREF( exception_keeper_value_9 );
                Py_XDECREF( exception_keeper_tb_9 );

                exception_lineno = 93;
                type_description_1 = "ocoooooooooooooo";
                goto try_except_handler_7;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_no_3:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto try_except_handler_7;
    // End of try:
    try_end_7:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_14;
            PyObject *tmp_call_result_9;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_14 = tmp_with_1__exit;
            frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 93;
            tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ocoooooooooooooo";
                goto try_except_handler_7;
            }
            Py_DECREF( tmp_call_result_9 );
        }
        branch_no_4:;
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    {
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_call_result_10;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_called_name_16;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_source_name_16;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_write );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_write );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_write" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_7;
        CHECK_OBJECT( var__f );
        tmp_called_name_16 = var__f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 97;
        tmp_args_element_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_str_digest_95f6424560611d246f267f2ad8ca8874_tuple, 0 ) );

        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_16 = par_compiled_dict;
        tmp_args_element_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_suffixes );
        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_18 );

            exception_lineno = 97;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19 };
            tmp_call_result_10 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_args_element_name_18 );
        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_call_result_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_10 );
    }
    {
        PyObject *tmp_called_name_17;
        PyObject *tmp_source_name_17;
        PyObject *tmp_source_name_18;
        PyObject *tmp_call_result_11;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_called_name_18;
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_18 = par_compiled_dict;
        tmp_source_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_words_dawg );
        if ( tmp_source_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_save );
        Py_DECREF( tmp_source_name_17 );
        if ( tmp_called_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var__f );
        tmp_called_name_18 = var__f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 98;
        tmp_args_element_name_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_digest_735cb515f948a018f1e620ea33122570_tuple, 0 ) );

        if ( tmp_args_element_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_17 );

            exception_lineno = 98;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_20 };
            tmp_call_result_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_called_name_17 );
        Py_DECREF( tmp_args_element_name_20 );
        if ( tmp_call_result_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_11 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_iter_arg_5;
        PyObject *tmp_called_name_19;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_source_name_19;
        tmp_called_name_19 = (PyObject *)&PyEnum_Type;
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_19 = par_compiled_dict;
        tmp_args_element_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_prediction_suffixes_dawgs );
        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_21 };
            tmp_iter_arg_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_iter_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_25 = MAKE_ITERATOR( tmp_iter_arg_5 );
        Py_DECREF( tmp_iter_arg_5 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_3__for_iterator == NULL );
        tmp_for_loop_3__for_iterator = tmp_assign_source_25;
    }
    // Tried code:
    loop_start_4:;
    {
        PyObject *tmp_next_source_4;
        PyObject *tmp_assign_source_26;
        CHECK_OBJECT( tmp_for_loop_3__for_iterator );
        tmp_next_source_4 = tmp_for_loop_3__for_iterator;
        tmp_assign_source_26 = ITERATOR_NEXT( tmp_next_source_4 );
        if ( tmp_assign_source_26 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_4;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ocoooooooooooooo";
                exception_lineno = 100;
                goto try_except_handler_12;
            }
        }

        {
            PyObject *old = tmp_for_loop_3__iter_value;
            tmp_for_loop_3__iter_value = tmp_assign_source_26;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_iter_arg_6;
        CHECK_OBJECT( tmp_for_loop_3__iter_value );
        tmp_iter_arg_6 = tmp_for_loop_3__iter_value;
        tmp_assign_source_27 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_13;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__source_iter;
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_27;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_28 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_28 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ocoooooooooooooo";
            exception_lineno = 100;
            goto try_except_handler_14;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_1;
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_28;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_29 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_29 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ocoooooooooooooo";
            exception_lineno = 100;
            goto try_except_handler_14;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_2;
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_29;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ocoooooooooooooo";
                    exception_lineno = 100;
                    goto try_except_handler_14;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ocoooooooooooooo";
            exception_lineno = 100;
            goto try_except_handler_14;
        }
    }
    goto try_end_9;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto try_except_handler_13;
    // End of try:
    try_end_9:;
    goto try_end_10;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto try_except_handler_12;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_30;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_30 = tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = var_prefix_id;
            var_prefix_id = tmp_assign_source_30;
            Py_INCREF( var_prefix_id );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_31;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_31 = tmp_tuple_unpack_2__element_2;
        {
            PyObject *old = var_prediction_suffixes_dawg;
            var_prediction_suffixes_dawg = tmp_assign_source_31;
            Py_INCREF( var_prediction_suffixes_dawg );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        PyObject *tmp_called_name_20;
        PyObject *tmp_source_name_20;
        PyObject *tmp_call_result_12;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_name_21;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( var_prediction_suffixes_dawg );
        tmp_source_name_20 = var_prediction_suffixes_dawg;
        tmp_called_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_save );
        if ( tmp_called_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_12;
        }
        CHECK_OBJECT( var__f );
        tmp_called_name_21 = var__f;
        tmp_left_name_2 = const_str_digest_7d5d1392b0d186e0cb7195994bc0e765;
        CHECK_OBJECT( var_prefix_id );
        tmp_right_name_2 = var_prefix_id;
        tmp_args_element_name_23 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_args_element_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_20 );

            exception_lineno = 101;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_12;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_23 };
            tmp_args_element_name_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
        }

        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_20 );

            exception_lineno = 101;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_12;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_22 };
            tmp_call_result_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_called_name_20 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_call_result_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_12;
        }
        Py_DECREF( tmp_call_result_12 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 100;
        type_description_1 = "ocoooooooooooooo";
        goto try_except_handler_12;
    }
    goto loop_start_4;
    loop_end_4:;
    goto try_end_11;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto frame_exception_exit_1;
    // End of try:
    try_end_11:;
    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    {
        PyObject *tmp_called_name_22;
        PyObject *tmp_source_name_21;
        PyObject *tmp_called_name_23;
        PyObject *tmp_source_name_22;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_result_13;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_called_name_24;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_dawg );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dawg );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dawg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_22 = tmp_mvar_value_8;
        tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_DAWG );
        if ( tmp_called_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_PREDICTION_PREFIXES );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PREDICTION_PREFIXES );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_called_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PREDICTION_PREFIXES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_24 = tmp_mvar_value_9;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_source_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_called_name_23 );
        if ( tmp_source_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_save );
        Py_DECREF( tmp_source_name_21 );
        if ( tmp_called_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var__f );
        tmp_called_name_24 = var__f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 104;
        tmp_args_element_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, &PyTuple_GET_ITEM( const_tuple_str_digest_1bd09a763c8b38cd78af0e6a5d992c87_tuple, 0 ) );

        if ( tmp_args_element_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_22 );

            exception_lineno = 104;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_25 };
            tmp_call_result_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_called_name_22 );
        Py_DECREF( tmp_args_element_name_25 );
        if ( tmp_call_result_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_13 );
    }
    {
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_call_result_14;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_called_name_26;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_write );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_write );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_write" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_10;
        CHECK_OBJECT( var__f );
        tmp_called_name_26 = var__f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 105;
        tmp_args_element_name_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, &PyTuple_GET_ITEM( const_tuple_str_digest_74531d15f942b19fea46429a1b996771_tuple, 0 ) );

        if ( tmp_args_element_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_PARADIGM_PREFIXES );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PARADIGM_PREFIXES );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PARADIGM_PREFIXES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_27 = tmp_mvar_value_11;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_26, tmp_args_element_name_27 };
            tmp_call_result_14 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_args_element_name_26 );
        if ( tmp_call_result_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_14 );
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_call_result_15;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_logger );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_5 = tmp_mvar_value_12;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 107;
        tmp_call_result_15 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_debug, &PyTuple_GET_ITEM( const_tuple_str_digest_f540b7c6a5c24848def98e8776abd3df_tuple, 0 ) );

        if ( tmp_call_result_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_15 );
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len(  );



        assert( var__dawg_len == NULL );
        var__dawg_len = tmp_assign_source_32;
    }
    {
        PyObject *tmp_called_instance_6;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_call_result_16;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_logger );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_6 = tmp_mvar_value_13;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 112;
        tmp_call_result_16 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_debug, &PyTuple_GET_ITEM( const_tuple_str_digest_ffe84336574aab4705a07cd32f4567e9_tuple, 0 ) );

        if ( tmp_call_result_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_16 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_called_name_27;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_source_name_23;
        CHECK_OBJECT( var__dawg_len );
        tmp_called_name_27 = var__dawg_len;
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_23 = par_compiled_dict;
        tmp_args_element_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_words_dawg );
        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 113;
        {
            PyObject *call_args[] = { tmp_args_element_name_28 };
            tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
        }

        Py_DECREF( tmp_args_element_name_28 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_words_dawg_len == NULL );
        var_words_dawg_len = tmp_assign_source_33;
    }
    {
        PyObject *tmp_called_instance_7;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_call_result_17;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_logger );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logger );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logger" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_7 = tmp_mvar_value_14;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 114;
        tmp_call_result_17 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_debug, &PyTuple_GET_ITEM( const_tuple_str_digest_296f2041bfcb81cf5eb380f4c3a3fd98_tuple, 0 ) );

        if ( tmp_call_result_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_17 );
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = PyList_New( 0 );
        assert( var_prediction_suffixes_dawg_lenghts == NULL );
        var_prediction_suffixes_dawg_lenghts = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_iter_arg_7;
        PyObject *tmp_source_name_24;
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_24 = par_compiled_dict;
        tmp_iter_arg_7 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_prediction_suffixes_dawgs );
        if ( tmp_iter_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_35 = MAKE_ITERATOR( tmp_iter_arg_7 );
        Py_DECREF( tmp_iter_arg_7 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_4__for_iterator == NULL );
        tmp_for_loop_4__for_iterator = tmp_assign_source_35;
    }
    // Tried code:
    loop_start_5:;
    {
        PyObject *tmp_next_source_5;
        PyObject *tmp_assign_source_36;
        CHECK_OBJECT( tmp_for_loop_4__for_iterator );
        tmp_next_source_5 = tmp_for_loop_4__for_iterator;
        tmp_assign_source_36 = ITERATOR_NEXT( tmp_next_source_5 );
        if ( tmp_assign_source_36 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_5;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ocoooooooooooooo";
                exception_lineno = 117;
                goto try_except_handler_15;
            }
        }

        {
            PyObject *old = tmp_for_loop_4__iter_value;
            tmp_for_loop_4__iter_value = tmp_assign_source_36;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_37;
        CHECK_OBJECT( tmp_for_loop_4__iter_value );
        tmp_assign_source_37 = tmp_for_loop_4__iter_value;
        {
            PyObject *old = var_prediction_suffixes_dawg;
            var_prediction_suffixes_dawg = tmp_assign_source_37;
            Py_INCREF( var_prediction_suffixes_dawg );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_28;
        PyObject *tmp_source_name_25;
        PyObject *tmp_call_result_18;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_called_name_29;
        PyObject *tmp_args_element_name_30;
        CHECK_OBJECT( var_prediction_suffixes_dawg_lenghts );
        tmp_source_name_25 = var_prediction_suffixes_dawg_lenghts;
        tmp_called_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_append );
        if ( tmp_called_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_15;
        }
        CHECK_OBJECT( var__dawg_len );
        tmp_called_name_29 = var__dawg_len;
        CHECK_OBJECT( var_prediction_suffixes_dawg );
        tmp_args_element_name_30 = var_prediction_suffixes_dawg;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_30 };
            tmp_args_element_name_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_29, call_args );
        }

        if ( tmp_args_element_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 118;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_15;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_29 };
            tmp_call_result_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_element_name_29 );
        if ( tmp_call_result_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "ocoooooooooooooo";
            goto try_except_handler_15;
        }
        Py_DECREF( tmp_call_result_18 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 117;
        type_description_1 = "ocoooooooooooooo";
        goto try_except_handler_15;
    }
    goto loop_start_5;
    loop_end_5:;
    goto try_end_12;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_4__iter_value );
    tmp_for_loop_4__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
    Py_DECREF( tmp_for_loop_4__for_iterator );
    tmp_for_loop_4__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto frame_exception_exit_1;
    // End of try:
    try_end_12:;
    Py_XDECREF( tmp_for_loop_4__iter_value );
    tmp_for_loop_4__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
    Py_DECREF( tmp_for_loop_4__for_iterator );
    tmp_for_loop_4__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_list_element_1;
        PyObject *tmp_list_element_2;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_list_element_3;
        PyObject *tmp_source_name_26;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_list_element_4;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_called_instance_9;
        PyObject *tmp_source_name_27;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_list_element_5;
        PyObject *tmp_source_name_28;
        PyObject *tmp_source_name_29;
        PyObject *tmp_list_element_6;
        PyObject *tmp_source_name_30;
        PyObject *tmp_source_name_31;
        PyObject *tmp_list_element_7;
        PyObject *tmp_len_arg_3;
        PyObject *tmp_source_name_32;
        PyObject *tmp_source_name_33;
        PyObject *tmp_list_element_8;
        PyObject *tmp_len_arg_4;
        PyObject *tmp_source_name_34;
        PyObject *tmp_source_name_35;
        PyObject *tmp_list_element_9;
        PyObject *tmp_len_arg_5;
        PyObject *tmp_source_name_36;
        PyObject *tmp_list_element_10;
        PyObject *tmp_list_element_11;
        PyObject *tmp_len_arg_6;
        PyObject *tmp_source_name_37;
        PyObject *tmp_list_element_12;
        PyObject *tmp_len_arg_7;
        PyObject *tmp_source_name_38;
        PyObject *tmp_list_element_13;
        PyObject *tmp_list_element_14;
        PyObject *tmp_source_name_39;
        PyObject *tmp_list_element_15;
        PyObject *tmp_list_element_16;
        PyObject *tmp_len_arg_8;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_list_element_17;
        PyObject *tmp_len_arg_9;
        PyObject *tmp_mvar_value_19;
        tmp_list_element_2 = const_str_plain_format_version;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_2 );
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_CURRENT_FORMAT_VERSION );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CURRENT_FORMAT_VERSION );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CURRENT_FORMAT_VERSION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_list_element_2 = tmp_mvar_value_15;
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_2 );
        tmp_assign_source_38 = PyList_New( 17 );
        PyList_SET_ITEM( tmp_assign_source_38, 0, tmp_list_element_1 );
        tmp_list_element_3 = const_str_plain_pymorphy2_version;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_3 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_3 );
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_pymorphy2 );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pymorphy2 );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pymorphy2" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_26 = tmp_mvar_value_16;
        tmp_list_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain___version__ );
        if ( tmp_list_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 122;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_3 );
        PyList_SET_ITEM( tmp_assign_source_38, 1, tmp_list_element_1 );
        tmp_list_element_4 = const_str_plain_compiled_at;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_4 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_4 );
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_datetime );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_27 = tmp_mvar_value_17;
        tmp_called_instance_9 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_datetime );
        if ( tmp_called_instance_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 123;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 123;
        tmp_called_instance_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_utcnow );
        Py_DECREF( tmp_called_instance_9 );
        if ( tmp_called_instance_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 123;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 123;
        tmp_list_element_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_isoformat );
        Py_DECREF( tmp_called_instance_8 );
        if ( tmp_list_element_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 123;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_4 );
        PyList_SET_ITEM( tmp_assign_source_38, 2, tmp_list_element_1 );
        tmp_list_element_1 = LIST_COPY( const_list_str_plain_source_str_digest_f50366a6d32ef2492b3f85df1d0f8544_list );
        PyList_SET_ITEM( tmp_assign_source_38, 3, tmp_list_element_1 );
        tmp_list_element_5 = const_str_plain_source_version;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_5 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_5 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_29 = par_compiled_dict;
        tmp_source_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_parsed_dict );
        if ( tmp_source_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 126;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_version );
        Py_DECREF( tmp_source_name_28 );
        if ( tmp_list_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 126;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_5 );
        PyList_SET_ITEM( tmp_assign_source_38, 4, tmp_list_element_1 );
        tmp_list_element_6 = const_str_plain_source_revision;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_6 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_6 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_31 = par_compiled_dict;
        tmp_source_name_30 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_parsed_dict );
        if ( tmp_source_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 127;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_revision );
        Py_DECREF( tmp_source_name_30 );
        if ( tmp_list_element_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 127;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_6 );
        PyList_SET_ITEM( tmp_assign_source_38, 5, tmp_list_element_1 );
        tmp_list_element_7 = const_str_plain_source_lexemes_count;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_7 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_7 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_33 = par_compiled_dict;
        tmp_source_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_parsed_dict );
        if ( tmp_source_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 128;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_len_arg_3 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_lexemes );
        Py_DECREF( tmp_source_name_32 );
        if ( tmp_len_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 128;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_7 = BUILTIN_LEN( tmp_len_arg_3 );
        Py_DECREF( tmp_len_arg_3 );
        if ( tmp_list_element_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 128;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_7 );
        PyList_SET_ITEM( tmp_assign_source_38, 6, tmp_list_element_1 );
        tmp_list_element_8 = const_str_plain_source_links_count;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_8 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_8 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_35 = par_compiled_dict;
        tmp_source_name_34 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_parsed_dict );
        if ( tmp_source_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 129;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_len_arg_4 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_links );
        Py_DECREF( tmp_source_name_34 );
        if ( tmp_len_arg_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 129;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_8 = BUILTIN_LEN( tmp_len_arg_4 );
        Py_DECREF( tmp_len_arg_4 );
        if ( tmp_list_element_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 129;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_8 );
        PyList_SET_ITEM( tmp_assign_source_38, 7, tmp_list_element_1 );
        tmp_list_element_9 = const_str_plain_gramtab_length;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_9 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_9 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_36 = par_compiled_dict;
        tmp_len_arg_5 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_gramtab );
        if ( tmp_len_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 131;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_9 = BUILTIN_LEN( tmp_len_arg_5 );
        Py_DECREF( tmp_len_arg_5 );
        if ( tmp_list_element_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 131;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_9 );
        PyList_SET_ITEM( tmp_assign_source_38, 8, tmp_list_element_1 );
        tmp_list_element_10 = const_str_plain_gramtab_formats;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_10 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_10 );
        CHECK_OBJECT( var_gramtab_formats );
        tmp_list_element_10 = var_gramtab_formats;
        Py_INCREF( tmp_list_element_10 );
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_10 );
        PyList_SET_ITEM( tmp_assign_source_38, 9, tmp_list_element_1 );
        tmp_list_element_11 = const_str_plain_paradigms_length;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_11 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_11 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_37 = par_compiled_dict;
        tmp_len_arg_6 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_paradigms );
        if ( tmp_len_arg_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 133;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_11 = BUILTIN_LEN( tmp_len_arg_6 );
        Py_DECREF( tmp_len_arg_6 );
        if ( tmp_list_element_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 133;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_11 );
        PyList_SET_ITEM( tmp_assign_source_38, 10, tmp_list_element_1 );
        tmp_list_element_12 = const_str_plain_suffixes_length;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_12 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_12 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_38 = par_compiled_dict;
        tmp_len_arg_7 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_suffixes );
        if ( tmp_len_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 134;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_12 = BUILTIN_LEN( tmp_len_arg_7 );
        Py_DECREF( tmp_len_arg_7 );
        if ( tmp_list_element_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 134;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_12 );
        PyList_SET_ITEM( tmp_assign_source_38, 11, tmp_list_element_1 );
        tmp_list_element_13 = const_str_plain_words_dawg_length;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_13 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_13 );
        CHECK_OBJECT( var_words_dawg_len );
        tmp_list_element_13 = var_words_dawg_len;
        Py_INCREF( tmp_list_element_13 );
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_13 );
        PyList_SET_ITEM( tmp_assign_source_38, 12, tmp_list_element_1 );
        tmp_list_element_14 = const_str_plain_prediction_options;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_14 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_14 );
        CHECK_OBJECT( par_compiled_dict );
        tmp_source_name_39 = par_compiled_dict;
        tmp_list_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_prediction_options );
        if ( tmp_list_element_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 137;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_14 );
        PyList_SET_ITEM( tmp_assign_source_38, 13, tmp_list_element_1 );
        tmp_list_element_15 = const_str_plain_prediction_suffixes_dawg_lengths;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_15 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_15 );
        CHECK_OBJECT( var_prediction_suffixes_dawg_lenghts );
        tmp_list_element_15 = var_prediction_suffixes_dawg_lenghts;
        Py_INCREF( tmp_list_element_15 );
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_15 );
        PyList_SET_ITEM( tmp_assign_source_38, 14, tmp_list_element_1 );
        tmp_list_element_16 = const_str_plain_prediction_prefixes_dawg_length;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_16 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_16 );
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_PREDICTION_PREFIXES );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PREDICTION_PREFIXES );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PREDICTION_PREFIXES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_len_arg_8 = tmp_mvar_value_18;
        tmp_list_element_16 = BUILTIN_LEN( tmp_len_arg_8 );
        if ( tmp_list_element_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 139;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_16 );
        PyList_SET_ITEM( tmp_assign_source_38, 15, tmp_list_element_1 );
        tmp_list_element_17 = const_str_plain_paradigm_prefixes_length;
        tmp_list_element_1 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_17 );
        PyList_SET_ITEM( tmp_list_element_1, 0, tmp_list_element_17 );
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_PARADIGM_PREFIXES );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PARADIGM_PREFIXES );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PARADIGM_PREFIXES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_len_arg_9 = tmp_mvar_value_19;
        tmp_list_element_17 = BUILTIN_LEN( tmp_len_arg_9 );
        if ( tmp_list_element_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_38 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 140;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_list_element_1, 1, tmp_list_element_17 );
        PyList_SET_ITEM( tmp_assign_source_38, 16, tmp_list_element_1 );
        assert( var_meta == NULL );
        var_meta = tmp_assign_source_38;
    }
    {
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_call_result_19;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_31;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_write );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_write );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_write" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_20;
        CHECK_OBJECT( var__f );
        tmp_called_name_31 = var__f;
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 143;
        tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, &PyTuple_GET_ITEM( const_tuple_str_digest_7ba451d3f8a14c32d5340f541ccdc012_tuple, 0 ) );

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_meta );
        tmp_tuple_element_1 = var_meta;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_5d3329810349a48ba364b65708c4137e );
        frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame.f_lineno = 143;
        tmp_call_result_19 = CALL_FUNCTION( tmp_called_name_30, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ocoooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_19 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d582e54ee741d26b1dc33273d1b13c4f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d582e54ee741d26b1dc33273d1b13c4f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d582e54ee741d26b1dc33273d1b13c4f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d582e54ee741d26b1dc33273d1b13c4f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d582e54ee741d26b1dc33273d1b13c4f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d582e54ee741d26b1dc33273d1b13c4f,
        type_description_1,
        par_compiled_dict,
        par_out_path,
        var__f,
        var_gramtab_formats,
        var_format,
        var_Tag,
        var_new_gramtab,
        var_gramtab_name,
        var_f,
        var_para,
        var_prefix_id,
        var_prediction_suffixes_dawg,
        var__dawg_len,
        var_words_dawg_len,
        var_prediction_suffixes_dawg_lenghts,
        var_meta
    );


    // Release cached frame.
    if ( frame_d582e54ee741d26b1dc33273d1b13c4f == cache_frame_d582e54ee741d26b1dc33273d1b13c4f )
    {
        Py_DECREF( frame_d582e54ee741d26b1dc33273d1b13c4f );
    }
    cache_frame_d582e54ee741d26b1dc33273d1b13c4f = NULL;

    assertFrameObject( frame_d582e54ee741d26b1dc33273d1b13c4f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_compiled_dict );
    Py_DECREF( par_compiled_dict );
    par_compiled_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_out_path );
    Py_DECREF( par_out_path );
    par_out_path = NULL;

    CHECK_OBJECT( (PyObject *)var__f );
    Py_DECREF( var__f );
    var__f = NULL;

    CHECK_OBJECT( (PyObject *)var_gramtab_formats );
    Py_DECREF( var_gramtab_formats );
    var_gramtab_formats = NULL;

    Py_XDECREF( var_format );
    var_format = NULL;

    Py_XDECREF( var_Tag );
    var_Tag = NULL;

    Py_XDECREF( var_new_gramtab );
    var_new_gramtab = NULL;

    Py_XDECREF( var_gramtab_name );
    var_gramtab_name = NULL;

    CHECK_OBJECT( (PyObject *)var_f );
    Py_DECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_para );
    var_para = NULL;

    Py_XDECREF( var_prefix_id );
    var_prefix_id = NULL;

    Py_XDECREF( var_prediction_suffixes_dawg );
    var_prediction_suffixes_dawg = NULL;

    CHECK_OBJECT( (PyObject *)var__dawg_len );
    Py_DECREF( var__dawg_len );
    var__dawg_len = NULL;

    CHECK_OBJECT( (PyObject *)var_words_dawg_len );
    Py_DECREF( var_words_dawg_len );
    var_words_dawg_len = NULL;

    CHECK_OBJECT( (PyObject *)var_prediction_suffixes_dawg_lenghts );
    Py_DECREF( var_prediction_suffixes_dawg_lenghts );
    var_prediction_suffixes_dawg_lenghts = NULL;

    CHECK_OBJECT( (PyObject *)var_meta );
    Py_DECREF( var_meta );
    var_meta = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_compiled_dict );
    Py_DECREF( par_compiled_dict );
    par_compiled_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_out_path );
    Py_DECREF( par_out_path );
    par_out_path = NULL;

    Py_XDECREF( var__f );
    var__f = NULL;

    Py_XDECREF( var_gramtab_formats );
    var_gramtab_formats = NULL;

    Py_XDECREF( var_format );
    var_format = NULL;

    Py_XDECREF( var_Tag );
    var_Tag = NULL;

    Py_XDECREF( var_new_gramtab );
    var_new_gramtab = NULL;

    Py_XDECREF( var_gramtab_name );
    var_gramtab_name = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_para );
    var_para = NULL;

    Py_XDECREF( var_prefix_id );
    var_prefix_id = NULL;

    Py_XDECREF( var_prediction_suffixes_dawg );
    var_prediction_suffixes_dawg = NULL;

    Py_XDECREF( var__dawg_len );
    var__dawg_len = NULL;

    Py_XDECREF( var_words_dawg_len );
    var_words_dawg_len = NULL;

    Py_XDECREF( var_prediction_suffixes_dawg_lenghts );
    var_prediction_suffixes_dawg_lenghts = NULL;

    Py_XDECREF( var_meta );
    var_meta = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b6ea40c79e2667303f75ee5298d1597e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b6ea40c79e2667303f75ee5298d1597e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b6ea40c79e2667303f75ee5298d1597e, codeobj_b6ea40c79e2667303f75ee5298d1597e, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *) );
    frame_b6ea40c79e2667303f75ee5298d1597e = cache_frame_b6ea40c79e2667303f75ee5298d1597e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b6ea40c79e2667303f75ee5298d1597e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b6ea40c79e2667303f75ee5298d1597e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "out_path" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        frame_b6ea40c79e2667303f75ee5298d1597e->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b6ea40c79e2667303f75ee5298d1597e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b6ea40c79e2667303f75ee5298d1597e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b6ea40c79e2667303f75ee5298d1597e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b6ea40c79e2667303f75ee5298d1597e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b6ea40c79e2667303f75ee5298d1597e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b6ea40c79e2667303f75ee5298d1597e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b6ea40c79e2667303f75ee5298d1597e,
        type_description_1,
        par_path,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_b6ea40c79e2667303f75ee5298d1597e == cache_frame_b6ea40c79e2667303f75ee5298d1597e )
    {
        Py_DECREF( frame_b6ea40c79e2667303f75ee5298d1597e );
    }
    cache_frame_b6ea40c79e2667303f75ee5298d1597e = NULL;

    assertFrameObject( frame_b6ea40c79e2667303f75ee5298d1597e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_dawg = python_pars[ 0 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_7a5eb3cc50852796d75056d49deaff04;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7a5eb3cc50852796d75056d49deaff04 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7a5eb3cc50852796d75056d49deaff04, codeobj_7a5eb3cc50852796d75056d49deaff04, module_pymorphy2$opencorpora_dict$storage, sizeof(void *) );
    frame_7a5eb3cc50852796d75056d49deaff04 = cache_frame_7a5eb3cc50852796d75056d49deaff04;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7a5eb3cc50852796d75056d49deaff04 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7a5eb3cc50852796d75056d49deaff04 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_sum_sequence_1;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_dawg );
            tmp_called_instance_1 = par_dawg;
            frame_7a5eb3cc50852796d75056d49deaff04->m_frame.f_lineno = 110;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_iterkeys );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 110;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 110;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_sum_sequence_1 = pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_sum_sequence_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len );
        return NULL;
        outline_result_1:;
        tmp_return_value = BUILTIN_SUM1( tmp_sum_sequence_1 );
        Py_DECREF( tmp_sum_sequence_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a5eb3cc50852796d75056d49deaff04 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a5eb3cc50852796d75056d49deaff04 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7a5eb3cc50852796d75056d49deaff04 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7a5eb3cc50852796d75056d49deaff04, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7a5eb3cc50852796d75056d49deaff04->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7a5eb3cc50852796d75056d49deaff04, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7a5eb3cc50852796d75056d49deaff04,
        type_description_1,
        par_dawg
    );


    // Release cached frame.
    if ( frame_7a5eb3cc50852796d75056d49deaff04 == cache_frame_7a5eb3cc50852796d75056d49deaff04 )
    {
        Py_DECREF( frame_7a5eb3cc50852796d75056d49deaff04 );
    }
    cache_frame_7a5eb3cc50852796d75056d49deaff04 = NULL;

    assertFrameObject( frame_7a5eb3cc50852796d75056d49deaff04 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_dawg );
    Py_DECREF( par_dawg );
    par_dawg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_dawg );
    Py_DECREF( par_dawg );
    par_dawg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_locals {
    PyObject *var_k;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_locals *generator_heap = (struct pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_k = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_4007b7e640a295d14887314b70082670, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 110;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_k;
            generator_heap->var_k = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_k );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_expression_name_1 = const_int_pos_1;
        Py_INCREF( tmp_expression_name_1 );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 110;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 110;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_k
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_context,
        module_pymorphy2$opencorpora_dict$storage,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_12fb4694fb9fcdaa3217c81de2f36a5c,
#endif
        codeobj_4007b7e640a295d14887314b70082670,
        1,
        sizeof(struct pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_3__load_meta( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_filename = python_pars[ 0 ];
    PyObject *var_meta = NULL;
    struct Nuitka_FrameObject *frame_b680e6cef3cf581d6a93684a16e0a392;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_b680e6cef3cf581d6a93684a16e0a392 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b680e6cef3cf581d6a93684a16e0a392, codeobj_b680e6cef3cf581d6a93684a16e0a392, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *) );
    frame_b680e6cef3cf581d6a93684a16e0a392 = cache_frame_b680e6cef3cf581d6a93684a16e0a392;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b680e6cef3cf581d6a93684a16e0a392 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b680e6cef3cf581d6a93684a16e0a392 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_read );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_read );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_read" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_filename );
        tmp_tuple_element_1 = par_filename;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_15ab90b31dfaacebcd2e280ebae53401 );
        frame_b680e6cef3cf581d6a93684a16e0a392->m_frame.f_lineno = 148;
        tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_meta == NULL );
        var_meta = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_attribute_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_collections );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_collections );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "collections" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_attribute_name_1 = const_str_plain_OrderedDict;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_1, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_collections );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_collections );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "collections" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 150;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_3;
            CHECK_OBJECT( var_meta );
            tmp_args_element_name_1 = var_meta;
            frame_b680e6cef3cf581d6a93684a16e0a392->m_frame.f_lineno = 150;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_OrderedDict, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 150;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dict_seq_1;
        CHECK_OBJECT( var_meta );
        tmp_dict_seq_1 = var_meta;
        tmp_return_value = TO_DICT( tmp_dict_seq_1, NULL );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b680e6cef3cf581d6a93684a16e0a392 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b680e6cef3cf581d6a93684a16e0a392 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b680e6cef3cf581d6a93684a16e0a392 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b680e6cef3cf581d6a93684a16e0a392, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b680e6cef3cf581d6a93684a16e0a392->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b680e6cef3cf581d6a93684a16e0a392, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b680e6cef3cf581d6a93684a16e0a392,
        type_description_1,
        par_filename,
        var_meta
    );


    // Release cached frame.
    if ( frame_b680e6cef3cf581d6a93684a16e0a392 == cache_frame_b680e6cef3cf581d6a93684a16e0a392 )
    {
        Py_DECREF( frame_b680e6cef3cf581d6a93684a16e0a392 );
    }
    cache_frame_b680e6cef3cf581d6a93684a16e0a392 = NULL;

    assertFrameObject( frame_b680e6cef3cf581d6a93684a16e0a392 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_3__load_meta );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)var_meta );
    Py_DECREF( var_meta );
    var_meta = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    Py_XDECREF( var_meta );
    var_meta = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_3__load_meta );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_4__load_tag_class( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_gramtab_format = python_pars[ 0 ];
    PyObject *par_grammemes_filename = python_pars[ 1 ];
    PyObject *var_Tag = NULL;
    PyObject *var_grammemes = NULL;
    struct Nuitka_FrameObject *frame_76bd878f799e10911e0aea01e3d8408d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_76bd878f799e10911e0aea01e3d8408d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_76bd878f799e10911e0aea01e3d8408d, codeobj_76bd878f799e10911e0aea01e3d8408d, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_76bd878f799e10911e0aea01e3d8408d = cache_frame_76bd878f799e10911e0aea01e3d8408d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_76bd878f799e10911e0aea01e3d8408d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_76bd878f799e10911e0aea01e3d8408d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_gramtab_format );
        tmp_compexpr_left_1 = par_gramtab_format;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_tagset );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tagset );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tagset" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_registry );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_left_name_1 = const_str_digest_2d0f3f34101c8d1ec2cf506fd8962036;
            CHECK_OBJECT( par_gramtab_format );
            tmp_right_name_1 = par_gramtab_format;
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_76bd878f799e10911e0aea01e3d8408d->m_frame.f_lineno = 157;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 157;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_tagset );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tagset );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tagset" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_registry );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_gramtab_format );
        tmp_subscript_name_1 = par_gramtab_format;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_Tag == NULL );
        var_Tag = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_read );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_read );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_read" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        CHECK_OBJECT( par_grammemes_filename );
        tmp_args_element_name_1 = par_grammemes_filename;
        frame_76bd878f799e10911e0aea01e3d8408d->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_grammemes == NULL );
        var_grammemes = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_Tag );
        tmp_called_instance_1 = var_Tag;
        CHECK_OBJECT( var_grammemes );
        tmp_args_element_name_2 = var_grammemes;
        frame_76bd878f799e10911e0aea01e3d8408d->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain__init_grammemes, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_76bd878f799e10911e0aea01e3d8408d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_76bd878f799e10911e0aea01e3d8408d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_76bd878f799e10911e0aea01e3d8408d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_76bd878f799e10911e0aea01e3d8408d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_76bd878f799e10911e0aea01e3d8408d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_76bd878f799e10911e0aea01e3d8408d,
        type_description_1,
        par_gramtab_format,
        par_grammemes_filename,
        var_Tag,
        var_grammemes
    );


    // Release cached frame.
    if ( frame_76bd878f799e10911e0aea01e3d8408d == cache_frame_76bd878f799e10911e0aea01e3d8408d )
    {
        Py_DECREF( frame_76bd878f799e10911e0aea01e3d8408d );
    }
    cache_frame_76bd878f799e10911e0aea01e3d8408d = NULL;

    assertFrameObject( frame_76bd878f799e10911e0aea01e3d8408d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_Tag );
    tmp_return_value = var_Tag;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_4__load_tag_class );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_gramtab_format );
    Py_DECREF( par_gramtab_format );
    par_gramtab_format = NULL;

    CHECK_OBJECT( (PyObject *)par_grammemes_filename );
    Py_DECREF( par_grammemes_filename );
    par_grammemes_filename = NULL;

    CHECK_OBJECT( (PyObject *)var_Tag );
    Py_DECREF( var_Tag );
    var_Tag = NULL;

    CHECK_OBJECT( (PyObject *)var_grammemes );
    Py_DECREF( var_grammemes );
    var_grammemes = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_gramtab_format );
    Py_DECREF( par_gramtab_format );
    par_gramtab_format = NULL;

    CHECK_OBJECT( (PyObject *)par_grammemes_filename );
    Py_DECREF( par_grammemes_filename );
    par_grammemes_filename = NULL;

    Py_XDECREF( var_Tag );
    var_Tag = NULL;

    Py_XDECREF( var_grammemes );
    var_grammemes = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_4__load_tag_class );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_5__load_gramtab( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_meta = python_pars[ 0 ];
    PyObject *par_gramtab_format = python_pars[ 1 ];
    PyObject *par_path = python_pars[ 2 ];
    PyObject *var_gramtab_formats = NULL;
    PyObject *var_gramtab_filename = NULL;
    struct Nuitka_FrameObject *frame_cb2379d30777ca5f4d3803a0f343de28;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_cb2379d30777ca5f4d3803a0f343de28 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cb2379d30777ca5f4d3803a0f343de28, codeobj_cb2379d30777ca5f4d3803a0f343de28, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cb2379d30777ca5f4d3803a0f343de28 = cache_frame_cb2379d30777ca5f4d3803a0f343de28;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cb2379d30777ca5f4d3803a0f343de28 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cb2379d30777ca5f4d3803a0f343de28 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        CHECK_OBJECT( par_meta );
        tmp_called_instance_1 = par_meta;
        tmp_call_arg_element_1 = const_str_plain_gramtab_formats;
        tmp_call_arg_element_2 = PyDict_New();
        frame_cb2379d30777ca5f4d3803a0f343de28->m_frame.f_lineno = 170;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_gramtab_formats == NULL );
        var_gramtab_formats = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_gramtab_format );
        tmp_compexpr_left_1 = par_gramtab_format;
        CHECK_OBJECT( var_gramtab_formats );
        tmp_compexpr_right_1 = var_gramtab_formats;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_called_instance_2;
            tmp_left_name_1 = const_str_digest_75d6424facd0bcab0f0d95042aa846f1;
            CHECK_OBJECT( par_gramtab_format );
            tmp_tuple_element_1 = par_gramtab_format;
            tmp_right_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_gramtab_formats );
            tmp_called_instance_2 = var_gramtab_formats;
            frame_cb2379d30777ca5f4d3803a0f343de28->m_frame.f_lineno = 172;
            tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_keys );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_right_name_1 );

                exception_lineno = 172;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            frame_cb2379d30777ca5f4d3803a0f343de28->m_frame.f_lineno = 172;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 172;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        CHECK_OBJECT( var_gramtab_formats );
        tmp_subscribed_name_1 = var_gramtab_formats;
        CHECK_OBJECT( par_gramtab_format );
        tmp_subscript_name_1 = par_gramtab_format;
        tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_cb2379d30777ca5f4d3803a0f343de28->m_frame.f_lineno = 174;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_gramtab_filename == NULL );
        var_gramtab_filename = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_read );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json_read );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json_read" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_gramtab_filename );
        tmp_args_element_name_3 = var_gramtab_filename;
        frame_cb2379d30777ca5f4d3803a0f343de28->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb2379d30777ca5f4d3803a0f343de28 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb2379d30777ca5f4d3803a0f343de28 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb2379d30777ca5f4d3803a0f343de28 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cb2379d30777ca5f4d3803a0f343de28, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cb2379d30777ca5f4d3803a0f343de28->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cb2379d30777ca5f4d3803a0f343de28, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cb2379d30777ca5f4d3803a0f343de28,
        type_description_1,
        par_meta,
        par_gramtab_format,
        par_path,
        var_gramtab_formats,
        var_gramtab_filename
    );


    // Release cached frame.
    if ( frame_cb2379d30777ca5f4d3803a0f343de28 == cache_frame_cb2379d30777ca5f4d3803a0f343de28 )
    {
        Py_DECREF( frame_cb2379d30777ca5f4d3803a0f343de28 );
    }
    cache_frame_cb2379d30777ca5f4d3803a0f343de28 = NULL;

    assertFrameObject( frame_cb2379d30777ca5f4d3803a0f343de28 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_5__load_gramtab );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_gramtab_format );
    Py_DECREF( par_gramtab_format );
    par_gramtab_format = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)var_gramtab_formats );
    Py_DECREF( var_gramtab_formats );
    var_gramtab_formats = NULL;

    CHECK_OBJECT( (PyObject *)var_gramtab_filename );
    Py_DECREF( var_gramtab_filename );
    var_gramtab_filename = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_gramtab_format );
    Py_DECREF( par_gramtab_format );
    par_gramtab_format = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( var_gramtab_formats );
    var_gramtab_formats = NULL;

    Py_XDECREF( var_gramtab_filename );
    var_gramtab_filename = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_5__load_gramtab );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_filename = python_pars[ 0 ];
    PyObject *var_paradigms = NULL;
    PyObject *var_f = NULL;
    PyObject *var_paradigms_count = NULL;
    PyObject *var_x = NULL;
    PyObject *var_paradigm_len = NULL;
    PyObject *var_para = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_5acff70762873e0da2a4fe71148e3260;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_5acff70762873e0da2a4fe71148e3260 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_paradigms == NULL );
        var_paradigms = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5acff70762873e0da2a4fe71148e3260, codeobj_5acff70762873e0da2a4fe71148e3260, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5acff70762873e0da2a4fe71148e3260 = cache_frame_5acff70762873e0da2a4fe71148e3260;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5acff70762873e0da2a4fe71148e3260 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5acff70762873e0da2a4fe71148e3260 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_open_filename_1;
        PyObject *tmp_open_mode_1;
        CHECK_OBJECT( par_filename );
        tmp_open_filename_1 = par_filename;
        tmp_open_mode_1 = const_str_plain_rb;
        tmp_assign_source_2 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_1 = tmp_with_1__source;
        tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_1, const_str_plain___enter__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 181;
        tmp_assign_source_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_2 = tmp_with_1__source;
        tmp_assign_source_4 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___exit__ );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_assign_source_5;
        tmp_assign_source_5 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_with_1__enter );
        tmp_assign_source_6 = tmp_with_1__enter;
        assert( var_f == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_f = tmp_assign_source_6;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_struct );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_struct );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "struct" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }

        tmp_source_name_3 = tmp_mvar_value_1;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_unpack );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        tmp_args_element_name_1 = const_str_digest_969c69e9fbc42cc6a8915b0b5d93fbb0;
        CHECK_OBJECT( var_f );
        tmp_called_instance_1 = var_f;
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 182;
        tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_read, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 182;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 182;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_subscribed_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_assign_source_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        assert( var_paradigms_count == NULL );
        var_paradigms_count = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_xrange_low_1;
        CHECK_OBJECT( var_paradigms_count );
        tmp_xrange_low_1 = var_paradigms_count;
        tmp_iter_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_8;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_9 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 184;
                goto try_except_handler_5;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_10 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_x;
            var_x = tmp_assign_source_10;
            Py_INCREF( var_x );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_subscript_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_struct );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_struct );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "struct" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }

        tmp_source_name_4 = tmp_mvar_value_2;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_unpack );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        tmp_args_element_name_3 = const_str_digest_969c69e9fbc42cc6a8915b0b5d93fbb0;
        CHECK_OBJECT( var_f );
        tmp_called_instance_2 = var_f;
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 185;
        tmp_args_element_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_read, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 185;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 185;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_subscribed_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_assign_source_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = var_paradigm_len;
            var_paradigm_len = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_array );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_array );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "array" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }

        tmp_called_instance_3 = tmp_mvar_value_3;
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 187;
        tmp_assign_source_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_array, &PyTuple_GET_ITEM( const_tuple_str_plain_H_tuple, 0 ) );

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = var_para;
            var_para = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        CHECK_OBJECT( var_para );
        tmp_called_instance_4 = var_para;
        CHECK_OBJECT( var_f );
        tmp_args_element_name_5 = var_f;
        CHECK_OBJECT( var_paradigm_len );
        tmp_args_element_name_6 = var_paradigm_len;
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 188;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_fromfile, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_7;
        CHECK_OBJECT( var_paradigms );
        tmp_called_instance_5 = var_paradigms;
        CHECK_OBJECT( var_para );
        tmp_args_element_name_7 = var_para;
        frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 190;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 184;
        type_description_1 = "ooooooo";
        goto try_except_handler_5;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_4;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_5acff70762873e0da2a4fe71148e3260, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_5acff70762873e0da2a4fe71148e3260, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooooo";
            goto try_except_handler_6;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_13;
            tmp_assign_source_13 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_13;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_args_element_name_10;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            tmp_args_element_name_8 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_9 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_10 = EXC_TRACEBACK(PyThreadState_GET());
            frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 184;
            {
                PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 184;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_5acff70762873e0da2a4fe71148e3260->m_frame) frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooo";
            goto try_except_handler_6;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 181;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_5acff70762873e0da2a4fe71148e3260->m_frame) frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooo";
        goto try_except_handler_6;
        branch_end_1:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms );
    return NULL;
    // End of try:
    try_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 184;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_4 );
                Py_XDECREF( exception_keeper_value_4 );
                Py_XDECREF( exception_keeper_tb_4 );

                exception_lineno = 184;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_3:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_call_result_4;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_6 = tmp_with_1__exit;
            frame_5acff70762873e0da2a4fe71148e3260->m_frame.f_lineno = 184;
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_4:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5acff70762873e0da2a4fe71148e3260 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5acff70762873e0da2a4fe71148e3260 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5acff70762873e0da2a4fe71148e3260, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5acff70762873e0da2a4fe71148e3260->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5acff70762873e0da2a4fe71148e3260, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5acff70762873e0da2a4fe71148e3260,
        type_description_1,
        par_filename,
        var_paradigms,
        var_f,
        var_paradigms_count,
        var_x,
        var_paradigm_len,
        var_para
    );


    // Release cached frame.
    if ( frame_5acff70762873e0da2a4fe71148e3260 == cache_frame_5acff70762873e0da2a4fe71148e3260 )
    {
        Py_DECREF( frame_5acff70762873e0da2a4fe71148e3260 );
    }
    cache_frame_5acff70762873e0da2a4fe71148e3260 = NULL;

    assertFrameObject( frame_5acff70762873e0da2a4fe71148e3260 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    CHECK_OBJECT( var_paradigms );
    tmp_return_value = var_paradigms;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)var_paradigms );
    Py_DECREF( var_paradigms );
    var_paradigms = NULL;

    CHECK_OBJECT( (PyObject *)var_f );
    Py_DECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_paradigms_count );
    var_paradigms_count = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_paradigm_len );
    var_paradigm_len = NULL;

    Py_XDECREF( var_para );
    var_para = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)var_paradigms );
    Py_DECREF( var_paradigms );
    var_paradigms = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_paradigms_count );
    var_paradigms_count = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_paradigm_len );
    var_paradigm_len = NULL;

    Py_XDECREF( var_para );
    var_para = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pymorphy2$opencorpora_dict$storage$$$function_7__assert_format_is_compatible( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_meta = python_pars[ 0 ];
    PyObject *par_path = python_pars[ 1 ];
    PyObject *var_format_version = NULL;
    PyObject *var_major = NULL;
    PyObject *var_minor = NULL;
    PyObject *var_curr_major = NULL;
    PyObject *var_curr_minor = NULL;
    PyObject *var_msg = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_e2c7b701efdfb0059bde11253f0f4107;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_e2c7b701efdfb0059bde11253f0f4107 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e2c7b701efdfb0059bde11253f0f4107, codeobj_e2c7b701efdfb0059bde11253f0f4107, module_pymorphy2$opencorpora_dict$storage, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e2c7b701efdfb0059bde11253f0f4107 = cache_frame_e2c7b701efdfb0059bde11253f0f4107;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e2c7b701efdfb0059bde11253f0f4107 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e2c7b701efdfb0059bde11253f0f4107 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_unicode_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_meta );
        tmp_called_instance_1 = par_meta;
        frame_e2c7b701efdfb0059bde11253f0f4107->m_frame.f_lineno = 196;
        tmp_unicode_arg_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_ba2246cfb8ba10b831875d9e12011b9d_tuple, 0 ) );

        if ( tmp_unicode_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = PyObject_Unicode( tmp_unicode_arg_1 );
        Py_DECREF( tmp_unicode_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_format_version == NULL );
        var_format_version = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = const_str_dot;
        CHECK_OBJECT( var_format_version );
        tmp_compexpr_right_1 = var_format_version;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_left_name_1 = const_str_digest_cd8399df6542e7b2ff2d6ac1e3b6c520;
            CHECK_OBJECT( var_format_version );
            tmp_right_name_1 = var_format_version;
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            frame_e2c7b701efdfb0059bde11253f0f4107->m_frame.f_lineno = 199;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 199;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_format_version );
        tmp_called_instance_2 = var_format_version;
        frame_e2c7b701efdfb0059bde11253f0f4107->m_frame.f_lineno = 201;
        tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 201;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 201;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooo";
                    exception_lineno = 201;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooo";
            exception_lineno = 201;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;
        assert( var_major == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_major = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;
        assert( var_minor == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_minor = tmp_assign_source_6;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_CURRENT_FORMAT_VERSION );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CURRENT_FORMAT_VERSION );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CURRENT_FORMAT_VERSION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 202;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }

        tmp_called_instance_3 = tmp_mvar_value_1;
        frame_e2c7b701efdfb0059bde11253f0f4107->m_frame.f_lineno = 202;
        tmp_iter_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_assign_source_7 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_2__source_iter == NULL );
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_7;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 202;
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_2__element_1 == NULL );
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 202;
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_2__element_2 == NULL );
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_9;
    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooo";
                    exception_lineno = 202;
                    goto try_except_handler_5;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooo";
            exception_lineno = 202;
            goto try_except_handler_5;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_4;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_10 = tmp_tuple_unpack_2__element_1;
        assert( var_curr_major == NULL );
        Py_INCREF( tmp_assign_source_10 );
        var_curr_major = tmp_assign_source_10;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_11 = tmp_tuple_unpack_2__element_2;
        assert( var_curr_minor == NULL );
        Py_INCREF( tmp_assign_source_11 );
        var_curr_minor = tmp_assign_source_11;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_major );
        tmp_compexpr_left_2 = var_major;
        CHECK_OBJECT( var_curr_major );
        tmp_compexpr_right_2 = var_curr_major;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_tuple_element_1;
            tmp_left_name_2 = const_str_digest_363e1f675eb3d81a8095581e7d6aa1e2;
            CHECK_OBJECT( par_path );
            tmp_tuple_element_1 = par_path;
            tmp_right_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_format_version );
            tmp_tuple_element_1 = var_format_version;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( var_curr_major );
            tmp_tuple_element_1 = var_curr_major;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_right_name_2, 2, tmp_tuple_element_1 );
            tmp_assign_source_12 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_msg == NULL );
            var_msg = tmp_assign_source_12;
        }
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_make_exception_arg_2;
            CHECK_OBJECT( var_msg );
            tmp_make_exception_arg_2 = var_msg;
            frame_e2c7b701efdfb0059bde11253f0f4107->m_frame.f_lineno = 208;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_2 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_2 == NULL) );
            exception_type = tmp_raise_type_2;
            exception_lineno = 208;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e2c7b701efdfb0059bde11253f0f4107 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e2c7b701efdfb0059bde11253f0f4107 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e2c7b701efdfb0059bde11253f0f4107, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e2c7b701efdfb0059bde11253f0f4107->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e2c7b701efdfb0059bde11253f0f4107, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e2c7b701efdfb0059bde11253f0f4107,
        type_description_1,
        par_meta,
        par_path,
        var_format_version,
        var_major,
        var_minor,
        var_curr_major,
        var_curr_minor,
        var_msg
    );


    // Release cached frame.
    if ( frame_e2c7b701efdfb0059bde11253f0f4107 == cache_frame_e2c7b701efdfb0059bde11253f0f4107 )
    {
        Py_DECREF( frame_e2c7b701efdfb0059bde11253f0f4107 );
    }
    cache_frame_e2c7b701efdfb0059bde11253f0f4107 = NULL;

    assertFrameObject( frame_e2c7b701efdfb0059bde11253f0f4107 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_7__assert_format_is_compatible );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)var_format_version );
    Py_DECREF( var_format_version );
    var_format_version = NULL;

    CHECK_OBJECT( (PyObject *)var_major );
    Py_DECREF( var_major );
    var_major = NULL;

    CHECK_OBJECT( (PyObject *)var_minor );
    Py_DECREF( var_minor );
    var_minor = NULL;

    CHECK_OBJECT( (PyObject *)var_curr_major );
    Py_DECREF( var_curr_major );
    var_curr_major = NULL;

    CHECK_OBJECT( (PyObject *)var_curr_minor );
    Py_DECREF( var_curr_minor );
    var_curr_minor = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( var_format_version );
    var_format_version = NULL;

    Py_XDECREF( var_major );
    var_major = NULL;

    Py_XDECREF( var_minor );
    var_minor = NULL;

    Py_XDECREF( var_curr_major );
    var_curr_major = NULL;

    Py_XDECREF( var_curr_minor );
    var_curr_minor = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage$$$function_7__assert_format_is_compatible );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict,
        const_str_plain_load_dict,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0ec95cd40c3f582dbea240b706615488,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        const_str_digest_62ced834d3e8324ba6678e44f1cbde32,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_49e5da426d804b69a49162ed40a8b58f,
#endif
        codeobj_2fbae624d3388521275076f86e01e1a6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict,
        const_str_plain_save_compiled_dict,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d582e54ee741d26b1dc33273d1b13c4f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        const_str_digest_a24a2f5d59e8cf71107ca1ce19436812,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_ea3b39b1eb560d67c3a621e2ee6b48a7,
#endif
        codeobj_b6ea40c79e2667303f75ee5298d1597e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict$$$function_2__dawg_len,
        const_str_plain__dawg_len,
#if PYTHON_VERSION >= 300
        const_str_digest_c53becbf4353e4da85be8e397d486c0b,
#endif
        codeobj_7a5eb3cc50852796d75056d49deaff04,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_3__load_meta(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_3__load_meta,
        const_str_plain__load_meta,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b680e6cef3cf581d6a93684a16e0a392,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        const_str_digest_affe8b2e5cc38c825cf1b08041a52a4f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_4__load_tag_class(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_4__load_tag_class,
        const_str_plain__load_tag_class,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_76bd878f799e10911e0aea01e3d8408d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        const_str_digest_ee2e795a82006414dbeef053d3c7c6e2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_5__load_gramtab(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_5__load_gramtab,
        const_str_plain__load_gramtab,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cb2379d30777ca5f4d3803a0f343de28,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        const_str_digest_6f227101a6d47a22c047f701472a5cb0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms,
        const_str_plain__load_paradigms,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5acff70762873e0da2a4fe71148e3260,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        const_str_digest_4190a579127570ed281dc39447207faf,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_7__assert_format_is_compatible(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pymorphy2$opencorpora_dict$storage$$$function_7__assert_format_is_compatible,
        const_str_plain__assert_format_is_compatible,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e2c7b701efdfb0059bde11253f0f4107,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pymorphy2$opencorpora_dict$storage,
        const_str_digest_4724d4b522be3f8f06676393f6c93087,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pymorphy2$opencorpora_dict$storage =
{
    PyModuleDef_HEAD_INIT,
    "pymorphy2.opencorpora_dict.storage",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict$storage)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(pymorphy2$opencorpora_dict$storage)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pymorphy2$opencorpora_dict$storage );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pymorphy2.opencorpora_dict.storage: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pymorphy2.opencorpora_dict.storage: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pymorphy2.opencorpora_dict.storage: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpymorphy2$opencorpora_dict$storage" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pymorphy2$opencorpora_dict$storage = Py_InitModule4(
        "pymorphy2.opencorpora_dict.storage",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pymorphy2$opencorpora_dict$storage = PyModule_Create( &mdef_pymorphy2$opencorpora_dict$storage );
#endif

    moduledict_pymorphy2$opencorpora_dict$storage = MODULE_DICT( module_pymorphy2$opencorpora_dict$storage );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_pymorphy2$opencorpora_dict$storage,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pymorphy2$opencorpora_dict$storage,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pymorphy2$opencorpora_dict$storage,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pymorphy2$opencorpora_dict$storage,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pymorphy2$opencorpora_dict$storage );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_3b990c06e7442baf7e5180e2fa57f730, module_pymorphy2$opencorpora_dict$storage );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_29a14529e89ac976cd103e3b22bb9431;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_362a54b311f40a17b3197e3b9f3e2ec1;
        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_29a14529e89ac976cd103e3b22bb9431 = MAKE_MODULE_FRAME( codeobj_29a14529e89ac976cd103e3b22bb9431, module_pymorphy2$opencorpora_dict$storage );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_29a14529e89ac976cd103e3b22bb9431 );
    assert( Py_REFCNT( frame_29a14529e89ac976cd103e3b22bb9431 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 6;
        tmp_assign_source_4 = PyImport_ImportModule("__future__");
        assert( !(tmp_assign_source_4 == NULL) );
        assert( tmp_import_from_1__module == NULL );
        Py_INCREF( tmp_assign_source_4 );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_absolute_import );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_absolute_import, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_unicode_literals );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_datetime;
        tmp_globals_name_1 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 7;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_datetime, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_os;
        tmp_globals_name_2 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 8;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_logging;
        tmp_globals_name_3 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 9;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_logging, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_collections;
        tmp_globals_name_4 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 10;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_collections, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_itertools;
        tmp_globals_name_5 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 11;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        assert( !(tmp_assign_source_11 == NULL) );
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_itertools, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_array;
        tmp_globals_name_6 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 12;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_array, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_struct;
        tmp_globals_name_7 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = Py_None;
        tmp_level_name_7 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 13;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_struct, tmp_assign_source_13 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_itertools );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_itertools );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_assign_source_14 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_izip );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_izip, tmp_assign_source_14 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_29a14529e89ac976cd103e3b22bb9431, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_29a14529e89ac976cd103e3b22bb9431, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_15;
            tmp_assign_source_15 = (PyObject *)&PyZip_Type;
            UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_izip, tmp_assign_source_15 );
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 15;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_29a14529e89ac976cd103e3b22bb9431->m_frame) frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pymorphy2$opencorpora_dict$storage );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_2:;
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_pymorphy2;
        tmp_globals_name_8 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = Py_None;
        tmp_level_name_8 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 20;
        tmp_assign_source_16 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_pymorphy2, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_pymorphy2;
        tmp_globals_name_9 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_tagset_tuple;
        tmp_level_name_9 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 21;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_tagset );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_tagset, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_plain_pymorphy2;
        tmp_globals_name_10 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_dawg_tuple;
        tmp_level_name_10 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 22;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_dawg );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_dawg, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_11de6548c656143b505417b369c3f813;
        tmp_globals_name_11 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_PARADIGM_PREFIXES_str_plain_PREDICTION_PREFIXES_tuple;
        tmp_level_name_11 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 23;
        tmp_assign_source_19 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_19;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_PARADIGM_PREFIXES );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_PARADIGM_PREFIXES, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_PREDICTION_PREFIXES );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_PREDICTION_PREFIXES, tmp_assign_source_21 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_44a90c7502259e04130dba80b2d605d3;
        tmp_globals_name_12 = (PyObject *)moduledict_pymorphy2$opencorpora_dict$storage;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_json_write_str_plain_json_read_tuple;
        tmp_level_name_12 = const_int_0;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 24;
        tmp_assign_source_22 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_22;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_7 = tmp_import_from_3__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_json_write );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_write, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_8 = tmp_import_from_3__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_json_read );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_json_read, tmp_assign_source_24 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_logging );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logging );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logging" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 26;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_4;
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 26;
        tmp_assign_source_25 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_getLogger, &PyTuple_GET_ITEM( const_tuple_str_digest_3b990c06e7442baf7e5180e2fa57f730_tuple, 0 ) );

        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_logger, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = const_str_digest_7749eac3a559542096769a3088266128;
        UPDATE_STRING_DICT0( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_CURRENT_FORMAT_VERSION, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_collections );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_collections );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "collections" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_5;
        tmp_call_arg_element_1 = const_str_plain_LoadedDictionary;
        tmp_call_arg_element_2 = LIST_COPY( const_list_a3c633733c653128003d731da37f55a5_list );
        frame_29a14529e89ac976cd103e3b22bb9431->m_frame.f_lineno = 30;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_27 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_namedtuple, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_LoadedDictionary, tmp_assign_source_27 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_29a14529e89ac976cd103e3b22bb9431 );
#endif
    popFrameStack();

    assertFrameObject( frame_29a14529e89ac976cd103e3b22bb9431 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_29a14529e89ac976cd103e3b22bb9431 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_29a14529e89ac976cd103e3b22bb9431, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_29a14529e89ac976cd103e3b22bb9431->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_29a14529e89ac976cd103e3b22bb9431, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_str_digest_4e9682f5d714f894d265491bc430d2ad_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_28 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_1_load_dict( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_load_dict, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_2_save_compiled_dict(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain_save_compiled_dict, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_3__load_meta(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_meta, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_4__load_tag_class(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_tag_class, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_5__load_gramtab(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_gramtab, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_6__load_paradigms(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__load_paradigms, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = MAKE_FUNCTION_pymorphy2$opencorpora_dict$storage$$$function_7__assert_format_is_compatible(  );



        UPDATE_STRING_DICT1( moduledict_pymorphy2$opencorpora_dict$storage, (Nuitka_StringObject *)const_str_plain__assert_format_is_compatible, tmp_assign_source_34 );
    }

    return MOD_RETURN_VALUE( module_pymorphy2$opencorpora_dict$storage );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
