/* Generated code for Python module 'jedi.api'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jedi$api" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jedi$api;
PyDictObject *moduledict_jedi$api;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_str_plain_Completion_tuple;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain_reset_time;
extern PyObject *const_str_plain__name;
extern PyObject *const_str_plain_max;
extern PyObject *const_str_plain_get_call_signature_details;
static PyObject *const_str_digest_9d8c6d366414ef1e85c16a13d371a449;
extern PyObject *const_str_digest_1d33749dff5e5d6b977290c5d2c43e47;
extern PyObject *const_str_plain_m;
static PyObject *const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_293817b0dc883ddab36bbbd19cf19ad8;
static PyObject *const_str_digest_2e023310748780e89483763f51dacac2;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_tuple_str_plain_cache_tuple;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_is_py3;
extern PyObject *const_tuple_tuple_empty_tuple;
static PyObject *const_str_digest_9e55e4857d0ad75b0912975beaf9d07f;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple;
static PyObject *const_str_digest_4f919be81b31d70bd9602dab4da576ce;
static PyObject *const_str_plain__def;
static PyObject *const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_check;
extern PyObject *const_str_plain_goto_definitions;
extern PyObject *const_tuple_str_plain_ModuleContext_tuple;
static PyObject *const_tuple_str_plain_usages_tuple;
static PyObject *const_tuple_1975d1900a6d3fb1ef42b420838be7e9_tuple;
static PyObject *const_tuple_str_plain_interpreter_tuple;
extern PyObject *const_str_plain_infer_import;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_endswith;
extern PyObject *const_str_plain__sys_path;
static PyObject *const_str_digest_8903485cc65a7cd77bb2d48304f586a9;
static PyObject *const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple;
static PyObject *const_str_digest_2587a82b2319532c94bbeee41e21af98;
extern PyObject *const_str_plain_cache_call_signatures;
extern PyObject *const_str_plain_goto;
extern PyObject *const_str_plain_map;
extern PyObject *const_str_plain_False;
static PyObject *const_tuple_str_plain_dotted_path_in_sys_path_tuple;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_str_plain_SubModuleName;
static PyObject *const_str_plain_references;
extern PyObject *const_str_digest_cc282ec370e83077ad88e5954bcf7b3d;
extern PyObject *const_int_0;
static PyObject *const_str_digest_214a4ae8c169a3cf220a05c4ef7cce2b;
static PyObject *const_tuple_str_plain_name_str_plain_script_str_plain_create_name_tuple;
extern PyObject *const_str_plain_code;
static PyObject *const_tuple_95bac2cbe5c516b25272f7e2e055b01a_tuple;
extern PyObject *const_tuple_str_plain_helpers_tuple;
extern PyObject *const_str_plain_bracket_leaf;
extern PyObject *const_tuple_str_plain_settings_tuple;
extern PyObject *const_str_plain_tree_name;
static PyObject *const_str_digest_9abc1f4b568a3c6e95f620df48d1ed17;
extern PyObject *const_str_digest_a19478749b11c4aeb121b2ebd2eeb5ca;
static PyObject *const_str_digest_eebc58417963cbd112d5b768dbcb7e22;
extern PyObject *const_slice_none_int_neg_1_int_pos_2;
extern PyObject *const_str_plain_type;
static PyObject *const_str_digest_08516a7712d209048033a0a9c06e8f29;
extern PyObject *const_tuple_str_plain_n_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_sys_path;
extern PyObject *const_str_plain_definitions;
extern PyObject *const_str_plain_enable_warning;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_is_definition;
extern PyObject *const_str_plain_evaluate_call_of_leaf;
extern PyObject *const_str_plain_jedi;
extern PyObject *const_tuple_str_plain_TreeNameDefinition_str_plain_ParamName_tuple;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_tuple_str_plain_parsed_tuple;
static PyObject *const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple;
extern PyObject *const_str_plain___repr__;
extern PyObject *const_str_plain_module;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_tuple_str_plain_n_str_plain_self_tuple;
extern PyObject *const_tuple_str_plain_imports_tuple;
static PyObject *const_str_digest_31d7a0834fe968eabab69cf8f5a48e05;
extern PyObject *const_str_plain_is_import;
static PyObject *const_str_plain_analysis_modules;
extern PyObject *const_str_plain_preload_module;
extern PyObject *const_str_plain_get_definition;
extern PyObject *const_str_plain_infer_enabled;
extern PyObject *const_str_digest_c32a307eb4cbe1d85147469cdfd3f906;
extern PyObject *const_str_plain_set_debug_function;
static PyObject *const_tuple_str_plain_modules_str_plain_m_str_plain_s_tuple;
extern PyObject *const_str_plain_helpers;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_get_module_names;
extern PyObject *const_tuple_false_false_tuple;
extern PyObject *const_str_plain_parso;
extern PyObject *const_str_plain_abspath;
extern PyObject *const_str_plain_getcwd;
extern PyObject *const_tuple_str_plain_classes_tuple;
static PyObject *const_str_digest_159c0e51aa7bed34f7d69aa031acaa8b;
extern PyObject *const_str_plain_try_iter_content;
extern PyObject *const_str_plain_filter;
extern PyObject *const_int_pos_3000;
extern PyObject *const_str_plain_read;
extern PyObject *const_str_plain_names;
extern PyObject *const_str_plain_cache;
extern PyObject *const_str_digest_f2ae848d840696d0977d1f391a666889;
extern PyObject *const_str_plain_children;
extern PyObject *const_str_plain_cache_directory;
extern PyObject *const_tuple_str_plain_d_tuple;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_import_name;
static PyObject *const_str_digest_bf86e1f77a0f2f17c8ad1f8fd620c7ff;
static PyObject *const_str_plain_include_builtins;
static PyObject *const_str_digest_2314c5bd8ebf4446ae1c4cc88a5ccb24;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_get_defined_names;
extern PyObject *const_str_plain_classdef;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_CallSignature;
extern PyObject *const_str_plain_script;
static PyObject *const_tuple_da267b696a61d5af498916eff5ceca83_tuple;
extern PyObject *const_str_plain_context;
extern PyObject *const_tuple_str_newline_tuple;
static PyObject *const_tuple_912f763cd2a9f37568505a0facdca874_tuple;
static PyObject *const_str_digest_037c84dba6b02cdb4b71ec3700af569f;
static PyObject *const_str_plain_is_module;
static PyObject *const_tuple_56d1b9080848996cc36c1880fd5088bb_tuple;
extern PyObject *const_str_digest_deb952b7ac13ad711de11121ed587a30;
static PyObject *const_str_plain__code;
static PyObject *const_str_digest_5e2ddd8a63b2a0dc4f109507f9269534;
static PyObject *const_tuple_str_digest_4f919be81b31d70bd9602dab4da576ce_tuple;
extern PyObject *const_str_plain_stacklevel;
static PyObject *const_str_digest_0dc5267be73cc2f56c6e9cdca7c79da2;
extern PyObject *const_str_plain_is_analysis;
static PyObject *const_str_digest_ccb4af1f53eae28017d11975ed978e92;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_diff_cache;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_plain_expr_stmt;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_path_tuple;
extern PyObject *const_str_plain_fast_parser;
static PyObject *const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_debug_function;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_8cec39e80ada9292d72ce8db3885c33b;
extern PyObject *const_str_plain_parsed;
extern PyObject *const_str_plain_settings;
extern PyObject *const_str_plain___main__;
extern PyObject *const_str_plain_goto_assignments;
extern PyObject *const_str_plain_d;
static PyObject *const_str_digest_fb001c9aa45e5bdb49173ed95a8840ad;
extern PyObject *const_str_plain_call_signatures;
extern PyObject *const_str_digest_785ebe15053a4fa731c98e296de8edd7;
extern PyObject *const_str_plain_namespaces;
extern PyObject *const_str_plain_load_grammar;
extern PyObject *const_str_plain_sys;
static PyObject *const_str_digest_1a23c18cf958634aadd7c1de5dbd50bb;
static PyObject *const_str_plain__orig_path;
extern PyObject *const_str_digest_8354bab93b2e72580e53a32ea4f9c404;
extern PyObject *const_str_plain_cache_path;
static PyObject *const_str_plain_found_builtin;
extern PyObject *const_str_plain_import_names;
static PyObject *const_tuple_str_plain_get_executable_nodes_tuple;
extern PyObject *const_str_plain_code_lines;
extern PyObject *const_str_plain_key;
static PyObject *const_tuple_77fb638699b4e46872421a956b1cf316_tuple;
extern PyObject *const_str_plain___init__;
extern PyObject *const_tuple_str_plain_Evaluator_tuple;
extern PyObject *const_str_plain_types;
extern PyObject *const_str_plain__module_node;
extern PyObject *const_str_plain_import_from;
extern PyObject *const_str_plain_get_paths;
extern PyObject *const_str_plain_evaluate_goto_definition;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_create_name;
static PyObject *const_str_digest_af46f61fe85f7c6a0a59451be505d157;
extern PyObject *const_str_plain_force_unicode;
static PyObject *const_tuple_str_plain_try_iter_content_tuple;
static PyObject *const_str_digest_64a8f90da046b08e884ef75a97a7425a;
static PyObject *const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple;
static PyObject *const_tuple_str_plain_d_str_plain_self_str_plain_call_signature_details_tuple;
extern PyObject *const_str_plain_analysis;
static PyObject *const_str_digest_e984d578221bf5ab887bc928606fe92f;
extern PyObject *const_str_plain_tree;
static PyObject *const_tuple_f7069389666698bf65b5a573f10588c5_tuple;
extern PyObject *const_tuple_str_plain_d_str_plain_self_tuple;
static PyObject *const_str_digest_60ac3a6e6496dc688804c5013df38ce7;
static PyObject *const_tuple_str_digest_ffaea7622ccee3658d13c942bf68d6cf_tuple;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain__code_lines;
extern PyObject *const_str_plain_Script;
extern PyObject *const_str_plain_classes;
static PyObject *const_tuple_int_pos_3000_tuple;
extern PyObject *const_str_digest_3e1604d04ce104740ab4a0cd2cf306db;
extern PyObject *const_str_plain_file_input;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_digest_7ca129d2d421fe965ad48cbb290b579b_tuple;
static PyObject *const_str_plain_NUITKA_PACKAGE_jedi_api;
extern PyObject *const_str_plain_new_name;
extern PyObject *const_str_plain_module_context;
extern PyObject *const_str_plain_definition;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain_warnings;
extern PyObject *const_str_plain_leaf;
extern PyObject *const_str_plain_create_context;
static PyObject *const_str_digest_910348dcbc1269ee447323aeeea7e275;
extern PyObject *const_tuple_str_plain_InterpreterEnvironment_tuple;
extern PyObject *const_str_plain_Interpreter;
static PyObject *const_tuple_str_plain_get_default_project_tuple;
extern PyObject *const_str_plain_node;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_digest_a1d7a196367634ea1b859fd2603f25f6;
static PyObject *const_str_plain__usages;
extern PyObject *const_str_plain_setrecursionlimit;
extern PyObject *const_str_plain_InterpreterEnvironment;
static PyObject *const_str_digest_ffaea7622ccee3658d13c942bf68d6cf;
static PyObject *const_str_plain_def_ref_filter;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_NUITKA_PACKAGE_jedi;
extern PyObject *const_str_plain__evaluator;
extern PyObject *const_str_plain_environment;
extern PyObject *const_str_plain_parent;
extern PyObject *const_str_plain_dotted_path_in_sys_path;
extern PyObject *const_str_plain_script_path;
static PyObject *const_str_plain_follow_builtin_imports;
extern PyObject *const_str_plain___path__;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_473f264bb147aa628c27f8075dfa47de_tuple;
extern PyObject *const_str_plain_Definition;
extern PyObject *const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
static PyObject *const_str_digest_d7ae3998101be51c72e9e902e5692ad0;
extern PyObject *const_int_pos_10;
static PyObject *const_str_digest_3fc9aa4c9fda68d670234c9c9746a3ae;
static PyObject *const_str_digest_9003b6e82ea7814524d19ee7cb013361;
static PyObject *const_tuple_str_plain_init_tuple;
extern PyObject *const_str_plain_funcdef;
extern PyObject *const_str_plain_keyword_name_str;
static PyObject *const_str_digest_6b3ae216d1716a65bce6e25881060f5a;
extern PyObject *const_str_digest_93e555a5b1643558b28cf6b2256158dc;
static PyObject *const_tuple_str_plain_get_module_names_str_plain_evaluate_call_of_leaf_tuple;
extern PyObject *const_str_plain_completion;
static PyObject *const_str_plain__analysis;
extern PyObject *const_str_plain__get_module;
static PyObject *const_str_digest_6d214cc6e077147dca5b4cd34a3570b8;
extern PyObject *const_str_plain_get_sys_path;
static PyObject *const_str_plain_line_len;
extern PyObject *const_str_plain_is_nested;
extern PyObject *const_str_plain_parse_and_get_code;
extern PyObject *const_tuple_f0f15781590b4fd8bce07dd7072b2735_tuple;
extern PyObject *const_str_plain_testlist;
static PyObject *const_str_digest_d7e04ec0da60cb7506fb299cd50368ad;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_plain_c;
extern PyObject *const_str_plain_enable_notice;
extern PyObject *const_tuple_str_plain_tree_name_to_contexts_tuple;
extern PyObject *const_str_plain_dirname;
extern PyObject *const_dict_fc2d0675235f4f6c6fd56070d077c5e5;
extern PyObject *const_str_plain_modules;
extern PyObject *const_str_plain___class__;
static PyObject *const_tuple_str_plain_environment_none_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_source;
extern PyObject *const_str_plain_api;
extern PyObject *const_str_plain_debug;
static PyObject *const_str_plain_interpreter;
extern PyObject *const_str_plain_get_default_project;
extern PyObject *const_str_plain_print_to_stdout;
extern PyObject *const_str_plain_Evaluator;
extern PyObject *const_str_plain_split_lines;
extern PyObject *const_str_digest_5bfaf90dbd407b4fc29090c8f6415242;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_completions;
extern PyObject *const_str_plain_add_module_to_cache;
extern PyObject *const_str_plain_get_name_of_position;
static PyObject *const_str_digest_b2ffc8dde85a8b5f5fc0aef3c7df3023;
extern PyObject *const_str_plain_ParamName;
extern PyObject *const_str_plain_speed;
extern PyObject *const_str_plain_warn;
static PyObject *const_str_plain_iter_import_completions;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_tuple_8c36b0382c3c994d34f61ea74136adab_tuple;
extern PyObject *const_str_plain_MixedModuleContext;
static PyObject *const_str_digest_850ff8eb9217fa94bb33e66eb611bf8d;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_plain_notices;
extern PyObject *const_str_plain_Completion;
extern PyObject *const_str_plain_in_builtin_module;
extern PyObject *const_str_plain___exit__;
static PyObject *const_str_plain_filter_follow_imports;
extern PyObject *const_str_plain_a;
static PyObject *const_tuple_9a6486ae5f50ae17b330461832e21956_tuple;
extern PyObject *const_dict_f154c9a58c9419d7e391901d7b7fe49e;
extern PyObject *const_tuple_str_plain_s_tuple;
extern PyObject *const_str_plain_param;
extern PyObject *const_str_plain_enable_speed;
extern PyObject *const_str_plain___enter__;
static PyObject *const_str_plain_line_string;
extern PyObject *const_str_plain_cls;
static PyObject *const_str_plain_is_def;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_sorted_definitions;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain_usages;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_plain_new_names;
static PyObject *const_str_plain__grammar;
extern PyObject *const_str_plain__path;
static PyObject *const_tuple_str_plain_unpack_tuple_to_dict_tuple;
extern PyObject *const_str_plain_reset_recursion_limitations;
extern PyObject *const_str_plain_defs;
extern PyObject *const_str_plain_Import;
static PyObject *const_tuple_21bcbde56788c55e40631a02c3742ccc_tuple;
extern PyObject *const_tuple_str_plain_name_str_plain_self_tuple;
extern PyObject *const_str_plain_DeprecationWarning;
static PyObject *const_str_plain_func_cb;
extern PyObject *const_str_plain_py__call__;
extern PyObject *const_str_plain_init;
extern PyObject *const_str_plain_line;
extern PyObject *const_str_plain_rb;
extern PyObject *const_tuple_str_plain_debug_tuple;
static PyObject *const_str_digest_97a5a8278f1145052d7b47b1e40714f2;
extern PyObject *const_str_plain_get_executable_nodes;
static PyObject *const_str_plain_call_signature_details;
extern PyObject *const_str_plain_call_index;
extern PyObject *const_tuple_str_plain_x_tuple;
extern PyObject *const_str_plain__pos;
static PyObject *const_tuple_str_digest_6d214cc6e077147dca5b4cd34a3570b8_tuple;
extern PyObject *const_str_digest_cd4a43d41990fe11ed21029f9707bb7b;
extern PyObject *const_str_plain_all_scopes;
extern PyObject *const_str_plain_eval_node;
extern PyObject *const_str_plain_start_pos;
extern PyObject *const_str_plain_ModuleContext;
extern PyObject *const_tuple_str_plain_import_name_str_plain_import_from_tuple;
extern PyObject *const_str_plain_project;
extern PyObject *const_str_plain_encoding;
extern PyObject *const_tuple_str_plain_a_str_plain_self_tuple;
static PyObject *const_str_digest_48757229a8765f68a570cd77bc9a6e90;
extern PyObject *const_str_plain_kwds;
static PyObject *const_str_plain_additional_module_paths;
static PyObject *const_str_plain_ana;
static PyObject *const_str_digest_4b0f73d6163cc9e4158bce4085f7eb24;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_column;
static PyObject *const_str_digest_fa9567be3e6ce4294ffeb38c0d795338;
extern PyObject *const_str_plain_imports;
extern PyObject *const_str_plain_TreeNameDefinition;
extern PyObject *const_str_digest_4ea3c739c0994e1c59a1638cbf9bd81c;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_tuple_str_plain_funcdef_str_plain_classdef_tuple;
extern PyObject *const_tuple_str_plain_tree_tuple;
extern PyObject *const_str_plain_unpack_tuple_to_dict;
static PyObject *const_str_digest_752ca1a8394d43d581c2778099e279bd;
extern PyObject *const_tuple_type_object_tuple;
static PyObject *const_str_plain_follow_imports;
static PyObject *const_str_digest_0fbd70c135f5eed3cd3799b1364c79b9;
extern PyObject *const_tuple_str_plain_force_unicode_str_plain_is_py3_tuple;
extern PyObject *const_str_plain_get_leaf_for_position;
extern PyObject *const_str_digest_7ca129d2d421fe965ad48cbb290b579b;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain_tree_name_to_contexts;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_keepends;
extern PyObject *const_tuple_str_plain_name_tuple;
extern PyObject *const_str_digest_3d8c90d9a9bb20202fbddcdf6f6b7d19;
static PyObject *const_str_digest_5956fa9b5c0a37254ac03448d15cc622;
static PyObject *const_str_plain__project;
static PyObject *const_tuple_a578a96148d87df5432533b1dcad1882_tuple;
extern PyObject *const_str_plain_clear_time_caches;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_digest_506c2565b910c9d58a55a1d887dc4a2e;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_plain_Completion_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Completion_tuple, 0, const_str_plain_Completion ); Py_INCREF( const_str_plain_Completion );
    const_str_digest_9d8c6d366414ef1e85c16a13d371a449 = UNSTREAM_STRING_ASCII( &constant_bin[ 930779 ], 51, 0 );
    const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple, 1, const_str_plain_leaf ); Py_INCREF( const_str_plain_leaf );
    PyTuple_SET_ITEM( const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple, 2, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple, 3, const_str_plain_definitions ); Py_INCREF( const_str_plain_definitions );
    PyTuple_SET_ITEM( const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple, 4, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple, 5, const_str_plain_defs ); Py_INCREF( const_str_plain_defs );
    const_str_digest_293817b0dc883ddab36bbbd19cf19ad8 = UNSTREAM_STRING_ASCII( &constant_bin[ 930830 ], 23, 0 );
    const_str_digest_2e023310748780e89483763f51dacac2 = UNSTREAM_STRING_ASCII( &constant_bin[ 930853 ], 41, 0 );
    const_str_digest_9e55e4857d0ad75b0912975beaf9d07f = UNSTREAM_STRING_ASCII( &constant_bin[ 930779 ], 18, 0 );
    const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_follow_imports = UNSTREAM_STRING_ASCII( &constant_bin[ 930894 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 1, const_str_plain_follow_imports ); Py_INCREF( const_str_plain_follow_imports );
    const_str_plain_follow_builtin_imports = UNSTREAM_STRING_ASCII( &constant_bin[ 930908 ], 22, 1 );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 2, const_str_plain_follow_builtin_imports ); Py_INCREF( const_str_plain_follow_builtin_imports );
    const_str_plain_filter_follow_imports = UNSTREAM_STRING_ASCII( &constant_bin[ 930930 ], 21, 1 );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 3, const_str_plain_filter_follow_imports ); Py_INCREF( const_str_plain_filter_follow_imports );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 4, const_str_plain_tree_name ); Py_INCREF( const_str_plain_tree_name );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 5, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 6, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 7, const_str_plain_check ); Py_INCREF( const_str_plain_check );
    PyTuple_SET_ITEM( const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 8, const_str_plain_defs ); Py_INCREF( const_str_plain_defs );
    const_str_digest_4f919be81b31d70bd9602dab4da576ce = UNSTREAM_STRING_ASCII( &constant_bin[ 930951 ], 15, 0 );
    const_str_plain__def = UNSTREAM_STRING_ASCII( &constant_bin[ 99325 ], 4, 1 );
    const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 1, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 2, const_str_plain_line ); Py_INCREF( const_str_plain_line );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 3, const_str_plain_column ); Py_INCREF( const_str_plain_column );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 4, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 5, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 6, const_str_plain_sys_path ); Py_INCREF( const_str_plain_sys_path );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 7, const_str_plain_environment ); Py_INCREF( const_str_plain_environment );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 8, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 9, const_str_plain_project ); Py_INCREF( const_str_plain_project );
    const_str_plain_line_string = UNSTREAM_STRING_ASCII( &constant_bin[ 930966 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 10, const_str_plain_line_string ); Py_INCREF( const_str_plain_line_string );
    const_str_plain_line_len = UNSTREAM_STRING_ASCII( &constant_bin[ 930977 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 11, const_str_plain_line_len ); Py_INCREF( const_str_plain_line_len );
    const_tuple_str_plain_usages_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_usages_tuple, 0, const_str_plain_usages ); Py_INCREF( const_str_plain_usages );
    const_tuple_1975d1900a6d3fb1ef42b420838be7e9_tuple = PyTuple_New( 2 );
    const_str_plain_NUITKA_PACKAGE_jedi_api = UNSTREAM_STRING_ASCII( &constant_bin[ 930985 ], 23, 1 );
    PyTuple_SET_ITEM( const_tuple_1975d1900a6d3fb1ef42b420838be7e9_tuple, 0, const_str_plain_NUITKA_PACKAGE_jedi_api ); Py_INCREF( const_str_plain_NUITKA_PACKAGE_jedi_api );
    PyTuple_SET_ITEM( const_tuple_1975d1900a6d3fb1ef42b420838be7e9_tuple, 1, const_str_digest_5bfaf90dbd407b4fc29090c8f6415242 ); Py_INCREF( const_str_digest_5bfaf90dbd407b4fc29090c8f6415242 );
    const_tuple_str_plain_interpreter_tuple = PyTuple_New( 1 );
    const_str_plain_interpreter = UNSTREAM_STRING_ASCII( &constant_bin[ 190754 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_interpreter_tuple, 0, const_str_plain_interpreter ); Py_INCREF( const_str_plain_interpreter );
    const_str_digest_8903485cc65a7cd77bb2d48304f586a9 = UNSTREAM_STRING_ASCII( &constant_bin[ 931008 ], 17, 0 );
    const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple, 2, const_str_digest_c075052d723d6707083e869a0e3659bb ); Py_INCREF( const_str_digest_c075052d723d6707083e869a0e3659bb );
    PyTuple_SET_ITEM( const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple, 3, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple, 4, Py_True ); Py_INCREF( Py_True );
    PyTuple_SET_ITEM( const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple, 5, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple, 6, Py_None ); Py_INCREF( Py_None );
    const_str_digest_2587a82b2319532c94bbeee41e21af98 = UNSTREAM_STRING_ASCII( &constant_bin[ 931025 ], 572, 0 );
    const_tuple_str_plain_dotted_path_in_sys_path_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dotted_path_in_sys_path_tuple, 0, const_str_plain_dotted_path_in_sys_path ); Py_INCREF( const_str_plain_dotted_path_in_sys_path );
    const_str_plain_references = UNSTREAM_STRING_ASCII( &constant_bin[ 551236 ], 10, 1 );
    const_str_digest_214a4ae8c169a3cf220a05c4ef7cce2b = UNSTREAM_STRING_ASCII( &constant_bin[ 931597 ], 15, 0 );
    const_tuple_str_plain_name_str_plain_script_str_plain_create_name_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_script_str_plain_create_name_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_script_str_plain_create_name_tuple, 1, const_str_plain_script ); Py_INCREF( const_str_plain_script );
    const_str_plain_create_name = UNSTREAM_STRING_ASCII( &constant_bin[ 931612 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_script_str_plain_create_name_tuple, 2, const_str_plain_create_name ); Py_INCREF( const_str_plain_create_name );
    const_tuple_95bac2cbe5c516b25272f7e2e055b01a_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_95bac2cbe5c516b25272f7e2e055b01a_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_call_signature_details = UNSTREAM_STRING_ASCII( &constant_bin[ 931623 ], 22, 1 );
    PyTuple_SET_ITEM( const_tuple_95bac2cbe5c516b25272f7e2e055b01a_tuple, 1, const_str_plain_call_signature_details ); Py_INCREF( const_str_plain_call_signature_details );
    PyTuple_SET_ITEM( const_tuple_95bac2cbe5c516b25272f7e2e055b01a_tuple, 2, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_95bac2cbe5c516b25272f7e2e055b01a_tuple, 3, const_str_plain_definitions ); Py_INCREF( const_str_plain_definitions );
    const_str_digest_9abc1f4b568a3c6e95f620df48d1ed17 = UNSTREAM_STRING_ASCII( &constant_bin[ 931645 ], 30, 0 );
    const_str_digest_eebc58417963cbd112d5b768dbcb7e22 = UNSTREAM_STRING_ASCII( &constant_bin[ 931675 ], 272, 0 );
    const_str_digest_08516a7712d209048033a0a9c06e8f29 = UNSTREAM_STRING_ASCII( &constant_bin[ 931947 ], 23, 0 );
    const_tuple_str_plain_parsed_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_parsed_tuple, 0, const_str_plain_parsed ); Py_INCREF( const_str_plain_parsed );
    const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 1, const_str_plain_module ); Py_INCREF( const_str_plain_module );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 2, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 3, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 4, const_str_plain_import_names ); Py_INCREF( const_str_plain_import_names );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 5, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 6, const_str_plain_types ); Py_INCREF( const_str_plain_types );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 7, const_str_plain_testlist ); Py_INCREF( const_str_plain_testlist );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 8, const_str_plain_defs ); Py_INCREF( const_str_plain_defs );
    const_str_plain_ana = UNSTREAM_STRING_ASCII( &constant_bin[ 9004 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 9, const_str_plain_ana ); Py_INCREF( const_str_plain_ana );
    const_str_digest_31d7a0834fe968eabab69cf8f5a48e05 = UNSTREAM_STRING_ASCII( &constant_bin[ 931970 ], 647, 0 );
    const_str_plain_analysis_modules = UNSTREAM_STRING_ASCII( &constant_bin[ 932617 ], 16, 1 );
    const_tuple_str_plain_modules_str_plain_m_str_plain_s_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_modules_str_plain_m_str_plain_s_tuple, 0, const_str_plain_modules ); Py_INCREF( const_str_plain_modules );
    PyTuple_SET_ITEM( const_tuple_str_plain_modules_str_plain_m_str_plain_s_tuple, 1, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_str_plain_modules_str_plain_m_str_plain_s_tuple, 2, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    const_str_digest_159c0e51aa7bed34f7d69aa031acaa8b = UNSTREAM_STRING_ASCII( &constant_bin[ 932633 ], 20, 0 );
    const_str_digest_bf86e1f77a0f2f17c8ad1f8fd620c7ff = UNSTREAM_STRING_ASCII( &constant_bin[ 932653 ], 29, 0 );
    const_str_plain_include_builtins = UNSTREAM_STRING_ASCII( &constant_bin[ 931410 ], 16, 1 );
    const_str_digest_2314c5bd8ebf4446ae1c4cc88a5ccb24 = UNSTREAM_STRING_ASCII( &constant_bin[ 932682 ], 35, 0 );
    const_tuple_da267b696a61d5af498916eff5ceca83_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_da267b696a61d5af498916eff5ceca83_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_da267b696a61d5af498916eff5ceca83_tuple, 1, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    const_str_plain_is_module = UNSTREAM_STRING_ASCII( &constant_bin[ 932623 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_da267b696a61d5af498916eff5ceca83_tuple, 2, const_str_plain_is_module ); Py_INCREF( const_str_plain_is_module );
    PyTuple_SET_ITEM( const_tuple_da267b696a61d5af498916eff5ceca83_tuple, 3, const_str_plain_module_context ); Py_INCREF( const_str_plain_module_context );
    const_tuple_912f763cd2a9f37568505a0facdca874_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_912f763cd2a9f37568505a0facdca874_tuple, 0, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    PyTuple_SET_ITEM( const_tuple_912f763cd2a9f37568505a0facdca874_tuple, 1, const_str_plain_tree_name ); Py_INCREF( const_str_plain_tree_name );
    PyTuple_SET_ITEM( const_tuple_912f763cd2a9f37568505a0facdca874_tuple, 2, const_str_plain_definition ); Py_INCREF( const_str_plain_definition );
    PyTuple_SET_ITEM( const_tuple_912f763cd2a9f37568505a0facdca874_tuple, 3, const_str_plain_completions ); Py_INCREF( const_str_plain_completions );
    const_str_digest_037c84dba6b02cdb4b71ec3700af569f = UNSTREAM_STRING_ASCII( &constant_bin[ 932717 ], 360, 0 );
    const_tuple_56d1b9080848996cc36c1880fd5088bb_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_56d1b9080848996cc36c1880fd5088bb_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_56d1b9080848996cc36c1880fd5088bb_tuple, 1, const_str_plain_completion ); Py_INCREF( const_str_plain_completion );
    PyTuple_SET_ITEM( const_tuple_56d1b9080848996cc36c1880fd5088bb_tuple, 2, const_str_plain_completions ); Py_INCREF( const_str_plain_completions );
    const_str_plain_iter_import_completions = UNSTREAM_STRING_ASCII( &constant_bin[ 930807 ], 23, 1 );
    PyTuple_SET_ITEM( const_tuple_56d1b9080848996cc36c1880fd5088bb_tuple, 3, const_str_plain_iter_import_completions ); Py_INCREF( const_str_plain_iter_import_completions );
    const_str_plain__code = UNSTREAM_STRING_ASCII( &constant_bin[ 60193 ], 5, 1 );
    const_str_digest_5e2ddd8a63b2a0dc4f109507f9269534 = UNSTREAM_STRING_ASCII( &constant_bin[ 933077 ], 585, 0 );
    const_tuple_str_digest_4f919be81b31d70bd9602dab4da576ce_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4f919be81b31d70bd9602dab4da576ce_tuple, 0, const_str_digest_4f919be81b31d70bd9602dab4da576ce ); Py_INCREF( const_str_digest_4f919be81b31d70bd9602dab4da576ce );
    const_str_digest_0dc5267be73cc2f56c6e9cdca7c79da2 = UNSTREAM_STRING_ASCII( &constant_bin[ 932682 ], 16, 0 );
    const_str_digest_ccb4af1f53eae28017d11975ed978e92 = UNSTREAM_STRING_ASCII( &constant_bin[ 933662 ], 72, 0 );
    const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple, 1, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple, 2, const_str_plain_namespaces ); Py_INCREF( const_str_plain_namespaces );
    PyTuple_SET_ITEM( const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple, 3, const_str_plain_kwds ); Py_INCREF( const_str_plain_kwds );
    PyTuple_SET_ITEM( const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple, 4, const_str_plain_environment ); Py_INCREF( const_str_plain_environment );
    PyTuple_SET_ITEM( const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple, 5, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_8cec39e80ada9292d72ce8db3885c33b = UNSTREAM_STRING_ASCII( &constant_bin[ 933734 ], 15, 0 );
    const_str_digest_fb001c9aa45e5bdb49173ed95a8840ad = UNSTREAM_STRING_ASCII( &constant_bin[ 933749 ], 234, 0 );
    const_str_digest_1a23c18cf958634aadd7c1de5dbd50bb = UNSTREAM_STRING_ASCII( &constant_bin[ 933983 ], 515, 0 );
    const_str_plain__orig_path = UNSTREAM_STRING_ASCII( &constant_bin[ 934498 ], 10, 1 );
    const_str_plain_found_builtin = UNSTREAM_STRING_ASCII( &constant_bin[ 934508 ], 13, 1 );
    const_tuple_str_plain_get_executable_nodes_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_executable_nodes_tuple, 0, const_str_plain_get_executable_nodes ); Py_INCREF( const_str_plain_get_executable_nodes );
    const_tuple_77fb638699b4e46872421a956b1cf316_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_77fb638699b4e46872421a956b1cf316_tuple, 0, const_str_plain_include_builtins ); Py_INCREF( const_str_plain_include_builtins );
    PyTuple_SET_ITEM( const_tuple_77fb638699b4e46872421a956b1cf316_tuple, 1, const_str_plain_tree_name ); Py_INCREF( const_str_plain_tree_name );
    PyTuple_SET_ITEM( const_tuple_77fb638699b4e46872421a956b1cf316_tuple, 2, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_77fb638699b4e46872421a956b1cf316_tuple, 3, const_str_plain_definitions ); Py_INCREF( const_str_plain_definitions );
    PyTuple_SET_ITEM( const_tuple_77fb638699b4e46872421a956b1cf316_tuple, 4, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_digest_af46f61fe85f7c6a0a59451be505d157 = UNSTREAM_STRING_ASCII( &constant_bin[ 934521 ], 34, 0 );
    const_tuple_str_plain_try_iter_content_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_try_iter_content_tuple, 0, const_str_plain_try_iter_content ); Py_INCREF( const_str_plain_try_iter_content );
    const_str_digest_64a8f90da046b08e884ef75a97a7425a = UNSTREAM_STRING_ASCII( &constant_bin[ 934555 ], 38, 0 );
    const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple, 2, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple, 3, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple, 4, const_str_digest_c075052d723d6707083e869a0e3659bb ); Py_INCREF( const_str_digest_c075052d723d6707083e869a0e3659bb );
    PyTuple_SET_ITEM( const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple, 5, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple, 6, Py_None ); Py_INCREF( Py_None );
    const_tuple_str_plain_d_str_plain_self_str_plain_call_signature_details_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_d_str_plain_self_str_plain_call_signature_details_tuple, 0, const_str_plain_d ); Py_INCREF( const_str_plain_d );
    PyTuple_SET_ITEM( const_tuple_str_plain_d_str_plain_self_str_plain_call_signature_details_tuple, 1, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_d_str_plain_self_str_plain_call_signature_details_tuple, 2, const_str_plain_call_signature_details ); Py_INCREF( const_str_plain_call_signature_details );
    const_str_digest_e984d578221bf5ab887bc928606fe92f = UNSTREAM_STRING_ASCII( &constant_bin[ 934593 ], 18, 0 );
    const_tuple_f7069389666698bf65b5a573f10588c5_tuple = PyTuple_New( 4 );
    const_str_plain_func_cb = UNSTREAM_STRING_ASCII( &constant_bin[ 933913 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_f7069389666698bf65b5a573f10588c5_tuple, 0, const_str_plain_func_cb ); Py_INCREF( const_str_plain_func_cb );
    PyTuple_SET_ITEM( const_tuple_f7069389666698bf65b5a573f10588c5_tuple, 1, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    const_str_plain_notices = UNSTREAM_STRING_ASCII( &constant_bin[ 934611 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_f7069389666698bf65b5a573f10588c5_tuple, 2, const_str_plain_notices ); Py_INCREF( const_str_plain_notices );
    PyTuple_SET_ITEM( const_tuple_f7069389666698bf65b5a573f10588c5_tuple, 3, const_str_plain_speed ); Py_INCREF( const_str_plain_speed );
    const_str_digest_60ac3a6e6496dc688804c5013df38ce7 = UNSTREAM_STRING_ASCII( &constant_bin[ 934618 ], 23, 0 );
    const_tuple_str_digest_ffaea7622ccee3658d13c942bf68d6cf_tuple = PyTuple_New( 1 );
    const_str_digest_ffaea7622ccee3658d13c942bf68d6cf = UNSTREAM_STRING_ASCII( &constant_bin[ 934641 ], 17, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_ffaea7622ccee3658d13c942bf68d6cf_tuple, 0, const_str_digest_ffaea7622ccee3658d13c942bf68d6cf ); Py_INCREF( const_str_digest_ffaea7622ccee3658d13c942bf68d6cf );
    const_tuple_int_pos_3000_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_3000_tuple, 0, const_int_pos_3000 ); Py_INCREF( const_int_pos_3000 );
    const_str_digest_910348dcbc1269ee447323aeeea7e275 = UNSTREAM_STRING_ASCII( &constant_bin[ 934658 ], 54, 0 );
    const_tuple_str_plain_get_default_project_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_default_project_tuple, 0, const_str_plain_get_default_project ); Py_INCREF( const_str_plain_get_default_project );
    const_str_digest_a1d7a196367634ea1b859fd2603f25f6 = UNSTREAM_STRING_ASCII( &constant_bin[ 934712 ], 72, 0 );
    const_str_plain__usages = UNSTREAM_STRING_ASCII( &constant_bin[ 931668 ], 7, 1 );
    const_str_plain_def_ref_filter = UNSTREAM_STRING_ASCII( &constant_bin[ 932668 ], 14, 1 );
    const_tuple_473f264bb147aa628c27f8075dfa47de_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 0, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 1, const_str_plain_check ); Py_INCREF( const_str_plain_check );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 3, const_str_plain_new_names ); Py_INCREF( const_str_plain_new_names );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 4, const_str_plain_found_builtin ); Py_INCREF( const_str_plain_found_builtin );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 5, const_str_plain_new_name ); Py_INCREF( const_str_plain_new_name );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 6, const_str_plain_filter_follow_imports ); Py_INCREF( const_str_plain_filter_follow_imports );
    PyTuple_SET_ITEM( const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 7, const_str_plain_follow_builtin_imports ); Py_INCREF( const_str_plain_follow_builtin_imports );
    const_str_digest_d7ae3998101be51c72e9e902e5692ad0 = UNSTREAM_STRING_ASCII( &constant_bin[ 934784 ], 411, 0 );
    const_str_digest_3fc9aa4c9fda68d670234c9c9746a3ae = UNSTREAM_STRING_ASCII( &constant_bin[ 935195 ], 45, 0 );
    const_str_digest_9003b6e82ea7814524d19ee7cb013361 = UNSTREAM_STRING_ASCII( &constant_bin[ 935240 ], 398, 0 );
    const_tuple_str_plain_init_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_init_tuple, 0, const_str_plain_init ); Py_INCREF( const_str_plain_init );
    const_str_digest_6b3ae216d1716a65bce6e25881060f5a = UNSTREAM_STRING_ASCII( &constant_bin[ 935638 ], 26, 0 );
    const_tuple_str_plain_get_module_names_str_plain_evaluate_call_of_leaf_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_module_names_str_plain_evaluate_call_of_leaf_tuple, 0, const_str_plain_get_module_names ); Py_INCREF( const_str_plain_get_module_names );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_module_names_str_plain_evaluate_call_of_leaf_tuple, 1, const_str_plain_evaluate_call_of_leaf ); Py_INCREF( const_str_plain_evaluate_call_of_leaf );
    const_str_plain__analysis = UNSTREAM_STRING_ASCII( &constant_bin[ 932689 ], 9, 1 );
    const_str_digest_6d214cc6e077147dca5b4cd34a3570b8 = UNSTREAM_STRING_ASCII( &constant_bin[ 935664 ], 18, 0 );
    const_str_digest_d7e04ec0da60cb7506fb299cd50368ad = UNSTREAM_STRING_ASCII( &constant_bin[ 935682 ], 223, 0 );
    const_tuple_str_plain_environment_none_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_environment_none_tuple, 0, const_str_plain_environment ); Py_INCREF( const_str_plain_environment );
    PyTuple_SET_ITEM( const_tuple_str_plain_environment_none_tuple, 1, Py_None ); Py_INCREF( Py_None );
    const_str_digest_b2ffc8dde85a8b5f5fc0aef3c7df3023 = UNSTREAM_STRING_ASCII( &constant_bin[ 935905 ], 11, 0 );
    const_tuple_8c36b0382c3c994d34f61ea74136adab_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_8c36b0382c3c994d34f61ea74136adab_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_additional_module_paths = UNSTREAM_STRING_ASCII( &constant_bin[ 931299 ], 23, 1 );
    PyTuple_SET_ITEM( const_tuple_8c36b0382c3c994d34f61ea74136adab_tuple, 1, const_str_plain_additional_module_paths ); Py_INCREF( const_str_plain_additional_module_paths );
    PyTuple_SET_ITEM( const_tuple_8c36b0382c3c994d34f61ea74136adab_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_8c36b0382c3c994d34f61ea74136adab_tuple, 3, const_str_plain__usages ); Py_INCREF( const_str_plain__usages );
    const_str_digest_850ff8eb9217fa94bb33e66eb611bf8d = UNSTREAM_STRING_ASCII( &constant_bin[ 935916 ], 18, 0 );
    const_tuple_9a6486ae5f50ae17b330461832e21956_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 0, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 1, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 2, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 3, const_str_plain_all_scopes ); Py_INCREF( const_str_plain_all_scopes );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 4, const_str_plain_definitions ); Py_INCREF( const_str_plain_definitions );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 5, const_str_plain_references ); Py_INCREF( const_str_plain_references );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 6, const_str_plain_environment ); Py_INCREF( const_str_plain_environment );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 7, const_str_plain_def_ref_filter ); Py_INCREF( const_str_plain_def_ref_filter );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 8, const_str_plain_create_name ); Py_INCREF( const_str_plain_create_name );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 9, const_str_plain_script ); Py_INCREF( const_str_plain_script );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 10, const_str_plain_module_context ); Py_INCREF( const_str_plain_module_context );
    PyTuple_SET_ITEM( const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 11, const_str_plain_defs ); Py_INCREF( const_str_plain_defs );
    const_str_plain_is_def = UNSTREAM_STRING_ASCII( &constant_bin[ 935934 ], 6, 1 );
    const_str_plain__grammar = UNSTREAM_STRING_ASCII( &constant_bin[ 667317 ], 8, 1 );
    const_tuple_str_plain_unpack_tuple_to_dict_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_unpack_tuple_to_dict_tuple, 0, const_str_plain_unpack_tuple_to_dict ); Py_INCREF( const_str_plain_unpack_tuple_to_dict );
    const_tuple_21bcbde56788c55e40631a02c3742ccc_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_21bcbde56788c55e40631a02c3742ccc_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_21bcbde56788c55e40631a02c3742ccc_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_21bcbde56788c55e40631a02c3742ccc_tuple, 2, const_str_plain_import_names ); Py_INCREF( const_str_plain_import_names );
    PyTuple_SET_ITEM( const_tuple_21bcbde56788c55e40631a02c3742ccc_tuple, 3, const_str_plain_module ); Py_INCREF( const_str_plain_module );
    const_str_digest_97a5a8278f1145052d7b47b1e40714f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 935940 ], 20, 0 );
    const_tuple_str_digest_6d214cc6e077147dca5b4cd34a3570b8_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_6d214cc6e077147dca5b4cd34a3570b8_tuple, 0, const_str_digest_6d214cc6e077147dca5b4cd34a3570b8 ); Py_INCREF( const_str_digest_6d214cc6e077147dca5b4cd34a3570b8 );
    const_str_digest_48757229a8765f68a570cd77bc9a6e90 = UNSTREAM_STRING_ASCII( &constant_bin[ 935960 ], 63, 0 );
    const_str_digest_4b0f73d6163cc9e4158bce4085f7eb24 = UNSTREAM_STRING_ASCII( &constant_bin[ 936023 ], 22, 0 );
    const_str_digest_fa9567be3e6ce4294ffeb38c0d795338 = UNSTREAM_STRING_ASCII( &constant_bin[ 936045 ], 23, 0 );
    const_str_digest_752ca1a8394d43d581c2778099e279bd = UNSTREAM_STRING_ASCII( &constant_bin[ 936068 ], 489, 0 );
    const_str_digest_0fbd70c135f5eed3cd3799b1364c79b9 = UNSTREAM_STRING_ASCII( &constant_bin[ 931645 ], 13, 0 );
    const_str_digest_5956fa9b5c0a37254ac03448d15cc622 = UNSTREAM_STRING_ASCII( &constant_bin[ 936557 ], 1427, 0 );
    const_str_plain__project = UNSTREAM_STRING_ASCII( &constant_bin[ 937984 ], 8, 1 );
    const_tuple_a578a96148d87df5432533b1dcad1882_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_a578a96148d87df5432533b1dcad1882_tuple, 0, const_str_plain__def ); Py_INCREF( const_str_plain__def );
    PyTuple_SET_ITEM( const_tuple_a578a96148d87df5432533b1dcad1882_tuple, 1, const_str_plain_is_def ); Py_INCREF( const_str_plain_is_def );
    PyTuple_SET_ITEM( const_tuple_a578a96148d87df5432533b1dcad1882_tuple, 2, const_str_plain_definitions ); Py_INCREF( const_str_plain_definitions );
    PyTuple_SET_ITEM( const_tuple_a578a96148d87df5432533b1dcad1882_tuple, 3, const_str_plain_references ); Py_INCREF( const_str_plain_references );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jedi$api( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_e0f64dc30825516bca3f8166b32fd68e;
static PyCodeObject *codeobj_774df3a9437f71117153addab97147f7;
static PyCodeObject *codeobj_ee834041e03ee3bb1f8af48d78aaa9f4;
static PyCodeObject *codeobj_d6c8d0ca4835e4b2ef5a38087eda7541;
static PyCodeObject *codeobj_9dcb0af9f9e7c3518a4d55e0bcdf336d;
static PyCodeObject *codeobj_dd6dfa35d65ab0fc6b224a567ddd2465;
static PyCodeObject *codeobj_f3e8cc459c3bd51a956febada8d1e733;
static PyCodeObject *codeobj_9d973ba712c6e586db7c45d50e883d8a;
static PyCodeObject *codeobj_00283d797dc6e53bc846371cfaafc3f0;
static PyCodeObject *codeobj_cc79bc898b5151f400caf23d67590344;
static PyCodeObject *codeobj_4c473900aeb01acfaf0655fa87b8600e;
static PyCodeObject *codeobj_18864da8d67c001f2ef4d02e01a19cd4;
static PyCodeObject *codeobj_adf1f479c98f4ee3189eef628359f34b;
static PyCodeObject *codeobj_d9743976b47d07f2790838f244e39c16;
static PyCodeObject *codeobj_766e9b0d1c67d44adbc3e2a6ed011954;
static PyCodeObject *codeobj_c1930f3c2365667fc0f7ea3a1d9cc3dc;
static PyCodeObject *codeobj_2a4a505dbc600e5d254792d9fe4b650a;
static PyCodeObject *codeobj_c2ffe85c025b82dd0a130c545e9ba132;
static PyCodeObject *codeobj_9a10fd60e3a46a712ec5e1e8c1f9d504;
static PyCodeObject *codeobj_b707086c23679791b0fdd79bb19830fe;
static PyCodeObject *codeobj_e36d4bf9a33efd7a74bd830b6bc3c93a;
static PyCodeObject *codeobj_62e2049811a9fa0bb8d9072fc36548f6;
static PyCodeObject *codeobj_a708c6c0c7e14854e4d89d212cbec165;
static PyCodeObject *codeobj_3636078bdb31c7973aad9575da42b0a5;
static PyCodeObject *codeobj_4f569dc0a04bc624621bfb55ceaced21;
static PyCodeObject *codeobj_8c8f61040eabcc4a506848e31297fefe;
static PyCodeObject *codeobj_e860bc0ee9177e567626a09673042f35;
static PyCodeObject *codeobj_3d5f6a807b8650ae92ff2e4d07a08aeb;
static PyCodeObject *codeobj_4ae0934951a98b59abe31a69fd48febb;
static PyCodeObject *codeobj_078186f669372586eb34c685197d831f;
static PyCodeObject *codeobj_d377d00d18d32214d6677962c7a243cd;
static PyCodeObject *codeobj_91acf3337269e0ab62a5d399cafa6f47;
static PyCodeObject *codeobj_d0250c08e540e8597497c3240e385814;
static PyCodeObject *codeobj_3cee4d78acba2a6869e442f856042225;
static PyCodeObject *codeobj_b8f17eb17f01749af14c8a24fe0b44e7;
static PyCodeObject *codeobj_441859b3ba7e70dea7e3ab377b6cdb4a;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_97a5a8278f1145052d7b47b1e40714f2 );
    codeobj_e0f64dc30825516bca3f8166b32fd68e = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 365, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_path_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_774df3a9437f71117153addab97147f7 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 382, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ee834041e03ee3bb1f8af48d78aaa9f4 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 482, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d6c8d0ca4835e4b2ef5a38087eda7541 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 381, const_tuple_str_plain_a_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9dcb0af9f9e7c3518a4d55e0bcdf336d = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 308, const_tuple_str_plain_d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dd6dfa35d65ab0fc6b224a567ddd2465 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 274, const_tuple_str_plain_d_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f3e8cc459c3bd51a956febada8d1e733 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 346, const_tuple_str_plain_d_str_plain_self_str_plain_call_signature_details_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9d973ba712c6e586db7c45d50e883d8a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 418, const_tuple_str_plain_n_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_00283d797dc6e53bc846371cfaafc3f0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 306, const_tuple_str_plain_n_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cc79bc898b5151f400caf23d67590344 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 477, const_tuple_str_plain_name_str_plain_script_str_plain_create_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4c473900aeb01acfaf0655fa87b8600e = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 223, const_tuple_str_plain_name_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_18864da8d67c001f2ef4d02e01a19cd4 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 222, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_adf1f479c98f4ee3189eef628359f34b = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8903485cc65a7cd77bb2d48304f586a9, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_d9743976b47d07f2790838f244e39c16 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Interpreter, 387, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_766e9b0d1c67d44adbc3e2a6ed011954 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Script, 46, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_c1930f3c2365667fc0f7ea3a1d9cc3dc = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 83, const_tuple_90e27b4a148a04b2859e2cffffa8a7f2_tuple, 8, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2a4a505dbc600e5d254792d9fe4b650a = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 403, const_tuple_a3d122723fbb00fbf2b127c8d10ce7b2_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_c2ffe85c025b82dd0a130c545e9ba132 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 159, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9a10fd60e3a46a712ec5e1e8c1f9d504 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__analysis, 352, const_tuple_610d16fff5226fe7e3a04fb4d8fa6f3c_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b707086c23679791b0fdd79bb19830fe = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_module, 432, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e36d4bf9a33efd7a74bd830b6bc3c93a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_module, 145, const_tuple_21bcbde56788c55e40631a02c3742ccc_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_62e2049811a9fa0bb8d9072fc36548f6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__usages, 298, const_tuple_77fb638699b4e46872421a956b1cf316_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_a708c6c0c7e14854e4d89d212cbec165 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_call_signatures, 312, const_tuple_95bac2cbe5c516b25272f7e2e055b01a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3636078bdb31c7973aad9575da42b0a5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_check, 266, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4f569dc0a04bc624621bfb55ceaced21 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_check, 269, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8c8f61040eabcc4a506848e31297fefe = MAKE_CODEOBJ( module_filename_obj, const_str_plain_completions, 166, const_tuple_56d1b9080848996cc36c1880fd5088bb_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e860bc0ee9177e567626a09673042f35 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_create_name, 462, const_tuple_da267b696a61d5af498916eff5ceca83_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_3d5f6a807b8650ae92ff2e4d07a08aeb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_def_ref_filter, 458, const_tuple_a578a96148d87df5432533b1dcad1882_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_4ae0934951a98b59abe31a69fd48febb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_filter_follow_imports, 241, const_tuple_473f264bb147aa628c27f8075dfa47de_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_078186f669372586eb34c685197d831f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_goto_assignments, 229, const_tuple_84de2b7df0ad63bf81334ccf64d24a77_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d377d00d18d32214d6677962c7a243cd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_goto_definitions, 201, const_tuple_64dfd64f943691e8e73b4dbd43574f2c_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_91acf3337269e0ab62a5d399cafa6f47 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_iter_import_completions, 181, const_tuple_912f763cd2a9f37568505a0facdca874_tuple, 0, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_d0250c08e540e8597497c3240e385814 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_names, 442, const_tuple_9a6486ae5f50ae17b330461832e21956_tuple, 7, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3cee4d78acba2a6869e442f856042225 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_preload_module, 485, const_tuple_str_plain_modules_str_plain_m_str_plain_s_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
    codeobj_b8f17eb17f01749af14c8a24fe0b44e7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_debug_function, 497, const_tuple_f7069389666698bf65b5a573f10588c5_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_441859b3ba7e70dea7e3ab377b6cdb4a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_usages, 277, const_tuple_8c36b0382c3c994d34f61ea74136adab_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
}

// The module function declarations.
static PyObject *jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_maker( void );


static PyObject *jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_maker( void );


static PyObject *jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_2_complex_call_helper_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_11_complex_call_helper_pos_keywords_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_10___init__(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_11__get_module(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_1_def_ref_filter(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_2_create_name(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_3_lambda(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_13_preload_module(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_14_set_debug_function( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_1___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_2__get_module(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_3___repr__(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_4_completions(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_4_completions$$$function_1_iter_import_completions(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_5_goto_definitions(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_2_check(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_3_check(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_7_usages( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_7_usages$$$function_1__usages( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_8_call_signatures(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_9__analysis(  );


static PyObject *MAKE_FUNCTION_jedi$api$$$function_9__analysis$$$function_1_lambda(  );


// The module function definitions.
static PyObject *impl_jedi$api$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_source = python_pars[ 1 ];
    PyObject *par_line = python_pars[ 2 ];
    PyObject *par_column = python_pars[ 3 ];
    PyObject *par_path = python_pars[ 4 ];
    PyObject *par_encoding = python_pars[ 5 ];
    PyObject *par_sys_path = python_pars[ 6 ];
    PyObject *par_environment = python_pars[ 7 ];
    PyObject *var_f = NULL;
    PyObject *var_project = NULL;
    PyObject *var_line_string = NULL;
    PyObject *var_line_len = NULL;
    PyObject *tmp_comparison_chain_1__comparison_result = NULL;
    PyObject *tmp_comparison_chain_1__operand_2 = NULL;
    PyObject *tmp_comparison_chain_2__comparison_result = NULL;
    PyObject *tmp_comparison_chain_2__operand_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_c1930f3c2365667fc0f7ea3a1d9cc3dc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    static struct Nuitka_FrameObject *cache_frame_c1930f3c2365667fc0f7ea3a1d9cc3dc = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c1930f3c2365667fc0f7ea3a1d9cc3dc, codeobj_c1930f3c2365667fc0f7ea3a1d9cc3dc, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c1930f3c2365667fc0f7ea3a1d9cc3dc = cache_frame_c1930f3c2365667fc0f7ea3a1d9cc3dc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_path );
        tmp_assattr_name_1 = par_path;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__orig_path, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_path );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_path );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 87;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 87;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_abspath, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_assattr_name_2 = Py_None;
        Py_INCREF( tmp_assattr_name_2 );
        condexpr_end_1:;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_path, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_source );
        tmp_compexpr_left_1 = par_source;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_open_filename_1;
            PyObject *tmp_open_mode_1;
            CHECK_OBJECT( par_path );
            tmp_open_filename_1 = par_path;
            tmp_open_mode_1 = const_str_plain_rb;
            tmp_assign_source_1 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__source == NULL );
            tmp_with_1__source = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_2 = tmp_with_1__source;
            tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___enter__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_2;
            }
            frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 91;
            tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__enter == NULL );
            tmp_with_1__enter = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_3 = tmp_with_1__source;
            tmp_assign_source_3 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___exit__ );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__exit == NULL );
            tmp_with_1__exit = tmp_assign_source_3;
        }
        {
            nuitka_bool tmp_assign_source_4;
            tmp_assign_source_4 = NUITKA_BOOL_TRUE;
            tmp_with_1__indicator = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_with_1__enter );
            tmp_assign_source_5 = tmp_with_1__enter;
            assert( var_f == NULL );
            Py_INCREF( tmp_assign_source_5 );
            var_f = tmp_assign_source_5;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_f );
            tmp_called_instance_2 = var_f;
            frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 92;
            tmp_assign_source_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_read );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = par_source;
                assert( old != NULL );
                par_source = tmp_assign_source_6;
                Py_DECREF( old );
            }

        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_c1930f3c2365667fc0f7ea3a1d9cc3dc, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_5;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                nuitka_bool tmp_assign_source_7;
                tmp_assign_source_7 = NUITKA_BOOL_FALSE;
                tmp_with_1__indicator = tmp_assign_source_7;
            }
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_args_element_name_4;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_2 = tmp_with_1__exit;
                tmp_args_element_name_2 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_3 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_4 = EXC_TRACEBACK(PyThreadState_GET());
                frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 92;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                    tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
                }

                if ( tmp_operand_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 92;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_5;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                Py_DECREF( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 92;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_5;
                }
                tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 92;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame) frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_5;
                branch_no_3:;
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 91;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame) frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_5;
            branch_end_2:;
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_2:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_1;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_1___init__ );
        return NULL;
        // End of try:
        try_end_1:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_5;
            nuitka_bool tmp_compexpr_left_3;
            nuitka_bool tmp_compexpr_right_3;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_3 = tmp_with_1__indicator;
            tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
            tmp_condition_result_5 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_name_3;
                PyObject *tmp_call_result_1;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_3 = tmp_with_1__exit;
                frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 92;
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_3 );
                    Py_XDECREF( exception_keeper_value_3 );
                    Py_XDECREF( exception_keeper_tb_3 );

                    exception_lineno = 92;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_4:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_2;
        // End of try:
        try_end_3:;
        {
            nuitka_bool tmp_condition_result_6;
            nuitka_bool tmp_compexpr_left_4;
            nuitka_bool tmp_compexpr_right_4;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_4 = tmp_with_1__indicator;
            tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
            tmp_condition_result_6 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_4 = tmp_with_1__exit;
                frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 92;
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 92;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_5:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        Py_XDECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        Py_XDECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_assattr_target_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_parso );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parso );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parso" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 95;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_2;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 95;
        tmp_assattr_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_load_grammar );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__grammar, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_7;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( par_sys_path );
        tmp_compexpr_left_5 = par_sys_path;
        tmp_compexpr_right_5 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_5 != tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_is_py3 );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_py3 );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_py3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_2 = tmp_mvar_value_3;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_7 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_7 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_list_arg_1;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_6;
            tmp_called_name_5 = (PyObject *)&PyMap_Type;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_force_unicode );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_force_unicode );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "force_unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 98;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_5 = tmp_mvar_value_4;
            CHECK_OBJECT( par_sys_path );
            tmp_args_element_name_6 = par_sys_path;
            frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 98;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_list_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
            }

            if ( tmp_list_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_8 = PySequence_List( tmp_list_arg_1 );
            Py_DECREF( tmp_list_arg_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_sys_path;
                assert( old != NULL );
                par_sys_path = tmp_assign_source_8;
                Py_DECREF( old );
            }

        }
        branch_no_6:;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_7;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_called_name_7;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_source_name_6;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_get_default_project );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_default_project );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_default_project" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_5;
        CHECK_OBJECT( par_path );
        tmp_truth_name_2 = CHECK_IF_TRUE( par_path );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_6;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_path );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_dirname );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_path );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_7;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 102;
        tmp_args_element_name_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_getcwd );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        condexpr_end_2:;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_project == NULL );
        var_project = tmp_assign_source_9;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        CHECK_OBJECT( par_sys_path );
        tmp_compexpr_left_6 = par_sys_path;
        tmp_compexpr_right_6 = Py_None;
        tmp_condition_result_9 = ( tmp_compexpr_left_6 != tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assattr_name_4;
            PyObject *tmp_assattr_target_4;
            CHECK_OBJECT( par_sys_path );
            tmp_assattr_name_4 = par_sys_path;
            CHECK_OBJECT( var_project );
            tmp_assattr_target_4 = var_project;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__sys_path, tmp_assattr_name_4 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_7:;
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_7;
        PyObject *tmp_assattr_target_5;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Evaluator );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Evaluator );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Evaluator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_8;
        CHECK_OBJECT( var_project );
        tmp_tuple_element_1 = var_project;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_environment;
        CHECK_OBJECT( par_environment );
        tmp_dict_value_1 = par_environment;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_script_path;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_path );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 108;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 107;
        tmp_assattr_name_5 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__evaluator, tmp_assattr_name_5 );
        Py_DECREF( tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        CHECK_OBJECT( var_project );
        tmp_assattr_name_6 = var_project;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__project, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_result_3;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_5 = tmp_mvar_value_9;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 111;
        tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_speed, &PyTuple_GET_ITEM( const_tuple_str_plain_init_tuple, 0 ) );

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_8;
        PyObject *tmp_source_name_9;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_source_name_10;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_11;
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__evaluator );
        if ( tmp_source_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_parse_and_get_code );
        Py_DECREF( tmp_source_name_8 );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        tmp_dict_key_3 = const_str_plain_code;
        if ( par_source == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "source" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }

        tmp_dict_value_3 = par_source;
        tmp_kw_name_2 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_path;
        CHECK_OBJECT( par_self );
        tmp_source_name_10 = par_self;
        tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_path );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 114;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_encoding;
        CHECK_OBJECT( par_encoding );
        tmp_dict_value_5 = par_encoding;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_cache;
        tmp_dict_value_6 = Py_False;
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_diff_cache;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_settings );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_settings );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_kw_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "settings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }

        tmp_source_name_11 = tmp_mvar_value_10;
        tmp_dict_value_7 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_fast_parser );
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 117;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_cache_path;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_settings );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_settings );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_kw_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "settings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }

        tmp_source_name_12 = tmp_mvar_value_11;
        tmp_dict_value_8 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_cache_directory );
        if ( tmp_dict_value_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_kw_name_2 );

            exception_lineno = 118;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 112;
        tmp_iter_arg_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_9, tmp_kw_name_2 );
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        tmp_assign_source_10 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooo";
            exception_lineno = 112;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_12 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooo";
            exception_lineno = 112;
            goto try_except_handler_7;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_12;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooo";
                    exception_lineno = 112;
                    goto try_except_handler_7;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooo";
            exception_lineno = 112;
            goto try_except_handler_7;
        }
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_6;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_assattr_target_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assattr_name_7 = tmp_tuple_unpack_1__element_1;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain__module_node, tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_6;
        }
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_13 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = par_source;
            par_source = tmp_assign_source_13;
            Py_INCREF( par_source );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_called_instance_6;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_call_result_4;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_6 = tmp_mvar_value_12;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 120;
        tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_speed, &PyTuple_GET_ITEM( const_tuple_str_plain_parsed_tuple, 0 ) );

        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_called_name_10;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_assattr_target_8;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_parso );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parso );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parso" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_13;
        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_split_lines );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_source );
        tmp_tuple_element_2 = par_source;
        tmp_args_name_2 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
        tmp_kw_name_3 = PyDict_Copy( const_dict_fc2d0675235f4f6c6fd56070d077c5e5 );
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 121;
        tmp_assattr_name_8 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_2, tmp_kw_name_3 );
        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_assattr_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain__code_lines, tmp_assattr_name_8 );
        Py_DECREF( tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_9;
        PyObject *tmp_assattr_target_9;
        CHECK_OBJECT( par_source );
        tmp_assattr_name_9 = par_source;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_9 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain__code, tmp_assattr_name_9 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_14;
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        PyObject *tmp_called_name_11;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_14;
        PyObject *tmp_args_element_name_10;
        CHECK_OBJECT( par_line );
        tmp_compexpr_left_7 = par_line;
        tmp_compexpr_right_7 = Py_None;
        tmp_condition_result_10 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        tmp_called_name_11 = LOOKUP_BUILTIN( const_str_plain_max );
        assert( tmp_called_name_11 != NULL );
        CHECK_OBJECT( par_self );
        tmp_source_name_14 = par_self;
        tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain__code_lines );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_9 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_10 = const_int_pos_1;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_assign_source_14 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( par_line );
        tmp_assign_source_14 = par_line;
        Py_INCREF( tmp_assign_source_14 );
        condexpr_end_3:;
        {
            PyObject *old = par_line;
            assert( old != NULL );
            par_line = tmp_assign_source_14;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_operand_name_3;
        {
            PyObject *tmp_assign_source_15;
            CHECK_OBJECT( par_line );
            tmp_assign_source_15 = par_line;
            assert( tmp_comparison_chain_1__operand_2 == NULL );
            Py_INCREF( tmp_assign_source_15 );
            tmp_comparison_chain_1__operand_2 = tmp_assign_source_15;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_compexpr_left_8;
            PyObject *tmp_compexpr_right_8;
            tmp_compexpr_left_8 = const_int_0;
            CHECK_OBJECT( tmp_comparison_chain_1__operand_2 );
            tmp_compexpr_right_8 = tmp_comparison_chain_1__operand_2;
            tmp_assign_source_16 = RICH_COMPARE_LT_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 124;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_8;
            }
            assert( tmp_comparison_chain_1__comparison_result == NULL );
            tmp_comparison_chain_1__comparison_result = tmp_assign_source_16;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_4;
            CHECK_OBJECT( tmp_comparison_chain_1__comparison_result );
            tmp_operand_name_4 = tmp_comparison_chain_1__comparison_result;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 124;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_8;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            CHECK_OBJECT( tmp_comparison_chain_1__comparison_result );
            tmp_operand_name_3 = tmp_comparison_chain_1__comparison_result;
            Py_INCREF( tmp_operand_name_3 );
            goto try_return_handler_8;
            branch_no_9:;
        }
        {
            PyObject *tmp_compexpr_left_9;
            PyObject *tmp_compexpr_right_9;
            PyObject *tmp_len_arg_2;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( tmp_comparison_chain_1__operand_2 );
            tmp_compexpr_left_9 = tmp_comparison_chain_1__operand_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_15 = par_self;
            tmp_len_arg_2 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain__code_lines );
            if ( tmp_len_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 124;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_8;
            }
            tmp_compexpr_right_9 = BUILTIN_LEN( tmp_len_arg_2 );
            Py_DECREF( tmp_len_arg_2 );
            if ( tmp_compexpr_right_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 124;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_8;
            }
            tmp_operand_name_3 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
            Py_DECREF( tmp_compexpr_right_9 );
            if ( tmp_operand_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 124;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_8;
            }
            goto try_return_handler_8;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_1___init__ );
        return NULL;
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__operand_2 );
        Py_DECREF( tmp_comparison_chain_1__operand_2 );
        tmp_comparison_chain_1__operand_2 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__comparison_result );
        Py_DECREF( tmp_comparison_chain_1__comparison_result );
        tmp_comparison_chain_1__comparison_result = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__operand_2 );
        Py_DECREF( tmp_comparison_chain_1__operand_2 );
        tmp_comparison_chain_1__operand_2 = NULL;

        Py_XDECREF( tmp_comparison_chain_1__comparison_result );
        tmp_comparison_chain_1__comparison_result = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto frame_exception_exit_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_1___init__ );
        return NULL;
        outline_result_1:;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        Py_DECREF( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_2e023310748780e89483763f51dacac2;
            frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 125;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 125;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_8:;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_16;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_16 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain__code_lines );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_line );
        tmp_left_name_1 = par_line;
        tmp_right_name_1 = const_int_pos_1;
        tmp_subscript_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_subscript_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscribed_name_1 );

            exception_lineno = 127;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_line_string == NULL );
        var_line_string = tmp_assign_source_17;
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_len_arg_3;
        CHECK_OBJECT( var_line_string );
        tmp_len_arg_3 = var_line_string;
        tmp_assign_source_18 = BUILTIN_LEN( tmp_len_arg_3 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_line_len == NULL );
        var_line_len = tmp_assign_source_18;
    }
    {
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_call_result_5;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_line_string );
        tmp_called_instance_7 = var_line_string;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 129;
        tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_digest_7ca129d2d421fe965ad48cbb290b579b_tuple, 0 ) );

        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_5 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_5 );

            exception_lineno = 129;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_13 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_5 );
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( var_line_len );
            tmp_left_name_2 = var_line_len;
            tmp_right_name_2 = const_int_pos_1;
            tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceSubtract, &tmp_left_name_2, tmp_right_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_19 = tmp_left_name_2;
            var_line_len = tmp_assign_source_19;

        }
        branch_no_10:;
    }
    {
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_call_result_6;
        int tmp_truth_name_4;
        CHECK_OBJECT( var_line_string );
        tmp_called_instance_8 = var_line_string;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 131;
        tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_call_result_6 );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_6 );

            exception_lineno = 131;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_14 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_6 );
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            CHECK_OBJECT( var_line_len );
            tmp_left_name_3 = var_line_len;
            tmp_right_name_3 = const_int_pos_1;
            tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceSubtract, &tmp_left_name_3, tmp_right_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 132;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_20 = tmp_left_name_3;
            var_line_len = tmp_assign_source_20;

        }
        branch_no_11:;
    }
    {
        PyObject *tmp_assign_source_21;
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_compexpr_left_10;
        PyObject *tmp_compexpr_right_10;
        CHECK_OBJECT( par_column );
        tmp_compexpr_left_10 = par_column;
        tmp_compexpr_right_10 = Py_None;
        tmp_condition_result_15 = ( tmp_compexpr_left_10 == tmp_compexpr_right_10 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        if ( var_line_len == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "line_len" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_21 = var_line_len;
        goto condexpr_end_4;
        condexpr_false_4:;
        CHECK_OBJECT( par_column );
        tmp_assign_source_21 = par_column;
        condexpr_end_4:;
        {
            PyObject *old = par_column;
            assert( old != NULL );
            par_column = tmp_assign_source_21;
            Py_INCREF( par_column );
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_operand_name_5;
        {
            PyObject *tmp_assign_source_22;
            CHECK_OBJECT( par_column );
            tmp_assign_source_22 = par_column;
            assert( tmp_comparison_chain_2__operand_2 == NULL );
            Py_INCREF( tmp_assign_source_22 );
            tmp_comparison_chain_2__operand_2 = tmp_assign_source_22;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            tmp_compexpr_left_11 = const_int_0;
            CHECK_OBJECT( tmp_comparison_chain_2__operand_2 );
            tmp_compexpr_right_11 = tmp_comparison_chain_2__operand_2;
            tmp_assign_source_23 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_9;
            }
            assert( tmp_comparison_chain_2__comparison_result == NULL );
            tmp_comparison_chain_2__comparison_result = tmp_assign_source_23;
        }
        {
            nuitka_bool tmp_condition_result_17;
            PyObject *tmp_operand_name_6;
            CHECK_OBJECT( tmp_comparison_chain_2__comparison_result );
            tmp_operand_name_6 = tmp_comparison_chain_2__comparison_result;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_9;
            }
            tmp_condition_result_17 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            CHECK_OBJECT( tmp_comparison_chain_2__comparison_result );
            tmp_operand_name_5 = tmp_comparison_chain_2__comparison_result;
            Py_INCREF( tmp_operand_name_5 );
            goto try_return_handler_9;
            branch_no_13:;
        }
        {
            PyObject *tmp_compexpr_left_12;
            PyObject *tmp_compexpr_right_12;
            CHECK_OBJECT( tmp_comparison_chain_2__operand_2 );
            tmp_compexpr_left_12 = tmp_comparison_chain_2__operand_2;
            if ( var_line_len == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "line_len" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 135;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_9;
            }

            tmp_compexpr_right_12 = var_line_len;
            tmp_operand_name_5 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
            if ( tmp_operand_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_9;
            }
            goto try_return_handler_9;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_1___init__ );
        return NULL;
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_2__operand_2 );
        Py_DECREF( tmp_comparison_chain_2__operand_2 );
        tmp_comparison_chain_2__operand_2 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_2__comparison_result );
        Py_DECREF( tmp_comparison_chain_2__comparison_result );
        tmp_comparison_chain_2__comparison_result = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_2__operand_2 );
        Py_DECREF( tmp_comparison_chain_2__operand_2 );
        tmp_comparison_chain_2__operand_2 = NULL;

        Py_XDECREF( tmp_comparison_chain_2__comparison_result );
        tmp_comparison_chain_2__comparison_result = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto frame_exception_exit_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_1___init__ );
        return NULL;
        outline_result_2:;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
        Py_DECREF( tmp_operand_name_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_16 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_make_exception_arg_2;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            PyObject *tmp_tuple_element_3;
            tmp_left_name_4 = const_str_digest_a1d7a196367634ea1b859fd2603f25f6;
            CHECK_OBJECT( par_column );
            tmp_tuple_element_3 = par_column;
            tmp_right_name_4 = PyTuple_New( 4 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_3 );
            if ( var_line_len == NULL )
            {
                Py_DECREF( tmp_right_name_4 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "line_len" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 138;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_3 = var_line_len;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_3 );
            CHECK_OBJECT( par_line );
            tmp_tuple_element_3 = par_line;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_right_name_4, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( var_line_string );
            tmp_tuple_element_3 = var_line_string;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_right_name_4, 3, tmp_tuple_element_3 );
            tmp_make_exception_arg_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_make_exception_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 136;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_2 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_2 );
            assert( !(tmp_raise_type_2 == NULL) );
            exception_type = tmp_raise_type_2;
            exception_lineno = 136;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_12:;
    }
    {
        PyObject *tmp_assattr_name_10;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_assattr_target_10;
        CHECK_OBJECT( par_line );
        tmp_tuple_element_4 = par_line;
        tmp_assattr_name_10 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_assattr_name_10, 0, tmp_tuple_element_4 );
        CHECK_OBJECT( par_column );
        tmp_tuple_element_4 = par_column;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_assattr_name_10, 1, tmp_tuple_element_4 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_10 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain__pos, tmp_assattr_name_10 );
        Py_DECREF( tmp_assattr_name_10 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_11;
        PyObject *tmp_assattr_target_11;
        CHECK_OBJECT( par_path );
        tmp_assattr_name_11 = par_path;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_11 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_11, const_str_plain__path, tmp_assattr_name_11 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_9;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_call_result_7;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_cache );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cache );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cache" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_9 = tmp_mvar_value_14;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 142;
        tmp_call_result_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_clear_time_caches );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_instance_10;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_call_result_8;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_10 = tmp_mvar_value_15;
        frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame.f_lineno = 143;
        tmp_call_result_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_10, const_str_plain_reset_time );
        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c1930f3c2365667fc0f7ea3a1d9cc3dc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c1930f3c2365667fc0f7ea3a1d9cc3dc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c1930f3c2365667fc0f7ea3a1d9cc3dc,
        type_description_1,
        par_self,
        par_source,
        par_line,
        par_column,
        par_path,
        par_encoding,
        par_sys_path,
        par_environment,
        var_f,
        var_project,
        var_line_string,
        var_line_len
    );


    // Release cached frame.
    if ( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc == cache_frame_c1930f3c2365667fc0f7ea3a1d9cc3dc )
    {
        Py_DECREF( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc );
    }
    cache_frame_c1930f3c2365667fc0f7ea3a1d9cc3dc = NULL;

    assertFrameObject( frame_c1930f3c2365667fc0f7ea3a1d9cc3dc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_line );
    Py_DECREF( par_line );
    par_line = NULL;

    CHECK_OBJECT( (PyObject *)par_column );
    Py_DECREF( par_column );
    par_column = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_sys_path );
    Py_DECREF( par_sys_path );
    par_sys_path = NULL;

    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    CHECK_OBJECT( (PyObject *)var_project );
    Py_DECREF( var_project );
    var_project = NULL;

    CHECK_OBJECT( (PyObject *)var_line_string );
    Py_DECREF( var_line_string );
    var_line_string = NULL;

    Py_XDECREF( var_line_len );
    var_line_len = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_line );
    Py_DECREF( par_line );
    par_line = NULL;

    CHECK_OBJECT( (PyObject *)par_column );
    Py_DECREF( par_column );
    par_column = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    Py_XDECREF( par_sys_path );
    par_sys_path = NULL;

    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_project );
    var_project = NULL;

    Py_XDECREF( var_line_string );
    var_line_string = NULL;

    Py_XDECREF( var_line_len );
    var_line_len = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_2__get_module( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_name = NULL;
    PyObject *var_import_names = NULL;
    PyObject *var_module = NULL;
    struct Nuitka_FrameObject *frame_e36d4bf9a33efd7a74bd830b6bc3c93a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_e36d4bf9a33efd7a74bd830b6bc3c93a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_plain___main__;
        assert( var_name == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_name = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e36d4bf9a33efd7a74bd830b6bc3c93a, codeobj_e36d4bf9a33efd7a74bd830b6bc3c93a, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e36d4bf9a33efd7a74bd830b6bc3c93a = cache_frame_e36d4bf9a33efd7a74bd830b6bc3c93a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e36d4bf9a33efd7a74bd830b6bc3c93a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e36d4bf9a33efd7a74bd830b6bc3c93a ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_3;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_dotted_path_in_sys_path );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dotted_path_in_sys_path );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dotted_path_in_sys_path" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 148;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__evaluator );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_e36d4bf9a33efd7a74bd830b6bc3c93a->m_frame.f_lineno = 148;
            tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_sys_path );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_path );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 148;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_e36d4bf9a33efd7a74bd830b6bc3c93a->m_frame.f_lineno = 148;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_import_names == NULL );
            var_import_names = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_import_names );
            tmp_compexpr_left_2 = var_import_names;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_args_element_name_3;
                tmp_called_instance_2 = const_str_dot;
                CHECK_OBJECT( var_import_names );
                tmp_args_element_name_3 = var_import_names;
                frame_e36d4bf9a33efd7a74bd830b6bc3c93a->m_frame.f_lineno = 150;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_join, call_args );
                }

                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 150;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_name;
                    assert( old != NULL );
                    var_name = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_7;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_ModuleContext );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ModuleContext );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ModuleContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 152;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__evaluator );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__module_node );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 153;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_path );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 153;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_code_lines;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__code_lines );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 154;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_e36d4bf9a33efd7a74bd830b6bc3c93a->m_frame.f_lineno = 152;
        tmp_assign_source_4 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_module == NULL );
        var_module = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_9;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_imports );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_imports );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "imports" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_add_module_to_cache );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__evaluator );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        if ( var_name == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = var_name;
        CHECK_OBJECT( var_module );
        tmp_args_element_name_6 = var_module;
        frame_e36d4bf9a33efd7a74bd830b6bc3c93a->m_frame.f_lineno = 156;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e36d4bf9a33efd7a74bd830b6bc3c93a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e36d4bf9a33efd7a74bd830b6bc3c93a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e36d4bf9a33efd7a74bd830b6bc3c93a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e36d4bf9a33efd7a74bd830b6bc3c93a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e36d4bf9a33efd7a74bd830b6bc3c93a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e36d4bf9a33efd7a74bd830b6bc3c93a,
        type_description_1,
        par_self,
        var_name,
        var_import_names,
        var_module
    );


    // Release cached frame.
    if ( frame_e36d4bf9a33efd7a74bd830b6bc3c93a == cache_frame_e36d4bf9a33efd7a74bd830b6bc3c93a )
    {
        Py_DECREF( frame_e36d4bf9a33efd7a74bd830b6bc3c93a );
    }
    cache_frame_e36d4bf9a33efd7a74bd830b6bc3c93a = NULL;

    assertFrameObject( frame_e36d4bf9a33efd7a74bd830b6bc3c93a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_module );
    tmp_return_value = var_module;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_2__get_module );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_import_names );
    var_import_names = NULL;

    CHECK_OBJECT( (PyObject *)var_module );
    Py_DECREF( var_module );
    var_module = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_import_names );
    var_import_names = NULL;

    Py_XDECREF( var_module );
    var_module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_2__get_module );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_3___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c2ffe85c025b82dd0a130c545e9ba132;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c2ffe85c025b82dd0a130c545e9ba132 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c2ffe85c025b82dd0a130c545e9ba132, codeobj_c2ffe85c025b82dd0a130c545e9ba132, module_jedi$api, sizeof(void *) );
    frame_c2ffe85c025b82dd0a130c545e9ba132 = cache_frame_c2ffe85c025b82dd0a130c545e9ba132;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c2ffe85c025b82dd0a130c545e9ba132 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c2ffe85c025b82dd0a130c545e9ba132 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        tmp_left_name_1 = const_str_digest_b2ffc8dde85a8b5f5fc0aef3c7df3023;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__orig_path );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 162;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 162;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__evaluator );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 163;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_environment );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 163;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c2ffe85c025b82dd0a130c545e9ba132 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c2ffe85c025b82dd0a130c545e9ba132 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c2ffe85c025b82dd0a130c545e9ba132 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c2ffe85c025b82dd0a130c545e9ba132, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c2ffe85c025b82dd0a130c545e9ba132->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c2ffe85c025b82dd0a130c545e9ba132, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c2ffe85c025b82dd0a130c545e9ba132,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_c2ffe85c025b82dd0a130c545e9ba132 == cache_frame_c2ffe85c025b82dd0a130c545e9ba132 )
    {
        Py_DECREF( frame_c2ffe85c025b82dd0a130c545e9ba132 );
    }
    cache_frame_c2ffe85c025b82dd0a130c545e9ba132 = NULL;

    assertFrameObject( frame_c2ffe85c025b82dd0a130c545e9ba132 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_3___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_3___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_4_completions( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_completion = NULL;
    struct Nuitka_CellObject *var_completions = PyCell_EMPTY();
    PyObject *var_iter_import_completions = NULL;
    struct Nuitka_FrameObject *frame_8c8f61040eabcc4a506848e31297fefe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_8c8f61040eabcc4a506848e31297fefe = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8c8f61040eabcc4a506848e31297fefe, codeobj_8c8f61040eabcc4a506848e31297fefe, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8c8f61040eabcc4a506848e31297fefe = cache_frame_8c8f61040eabcc4a506848e31297fefe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8c8f61040eabcc4a506848e31297fefe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8c8f61040eabcc4a506848e31297fefe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_8c8f61040eabcc4a506848e31297fefe->m_frame.f_lineno = 174;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_speed, &PyTuple_GET_ITEM( const_tuple_str_digest_ffaea7622ccee3658d13c942bf68d6cf_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Completion );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Completion );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Completion" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__evaluator );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        frame_8c8f61040eabcc4a506848e31297fefe->m_frame.f_lineno = 176;
        tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain__get_module );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 176;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__code_lines );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 176;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__pos );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 177;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_call_signatures );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 177;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        frame_8c8f61040eabcc4a506848e31297fefe->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        assert( var_completion == NULL );
        var_completion = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( var_completion );
        tmp_called_instance_3 = var_completion;
        frame_8c8f61040eabcc4a506848e31297fefe->m_frame.f_lineno = 179;
        tmp_assign_source_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_completions );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_completions ) == NULL );
        PyCell_SET( var_completions, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = MAKE_FUNCTION_jedi$api$$$function_4_completions$$$function_1_iter_import_completions(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_3)->m_closure[0] = var_completions;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_3)->m_closure[0] );


        assert( var_iter_import_completions == NULL );
        var_iter_import_completions = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_name_2;
        CHECK_OBJECT( var_iter_import_completions );
        tmp_called_name_2 = var_iter_import_completions;
        frame_8c8f61040eabcc4a506848e31297fefe->m_frame.f_lineno = 191;
        tmp_list_arg_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        tmp_len_arg_1 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_10;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            PyObject *tmp_source_name_5;
            tmp_assattr_name_1 = Py_False;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__evaluator );
            if ( tmp_assattr_target_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "ooco";
                goto frame_exception_exit_1;
            }
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_infer_enabled, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_target_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "ooco";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 198;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_3;
        frame_8c8f61040eabcc4a506848e31297fefe->m_frame.f_lineno = 198;
        tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_speed, &PyTuple_GET_ITEM( const_tuple_str_digest_4f919be81b31d70bd9602dab4da576ce_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ooco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c8f61040eabcc4a506848e31297fefe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c8f61040eabcc4a506848e31297fefe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8c8f61040eabcc4a506848e31297fefe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8c8f61040eabcc4a506848e31297fefe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8c8f61040eabcc4a506848e31297fefe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8c8f61040eabcc4a506848e31297fefe,
        type_description_1,
        par_self,
        var_completion,
        var_completions,
        var_iter_import_completions
    );


    // Release cached frame.
    if ( frame_8c8f61040eabcc4a506848e31297fefe == cache_frame_8c8f61040eabcc4a506848e31297fefe )
    {
        Py_DECREF( frame_8c8f61040eabcc4a506848e31297fefe );
    }
    cache_frame_8c8f61040eabcc4a506848e31297fefe = NULL;

    assertFrameObject( frame_8c8f61040eabcc4a506848e31297fefe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( PyCell_GET( var_completions ) );
    tmp_return_value = PyCell_GET( var_completions );
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_4_completions );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_completion );
    Py_DECREF( var_completion );
    var_completion = NULL;

    CHECK_OBJECT( (PyObject *)var_completions );
    Py_DECREF( var_completions );
    var_completions = NULL;

    CHECK_OBJECT( (PyObject *)var_iter_import_completions );
    Py_DECREF( var_iter_import_completions );
    var_iter_import_completions = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_completion );
    var_completion = NULL;

    CHECK_OBJECT( (PyObject *)var_completions );
    Py_DECREF( var_completions );
    var_completions = NULL;

    Py_XDECREF( var_iter_import_completions );
    var_iter_import_completions = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_4_completions );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_4_completions$$$function_1_iter_import_completions( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = self->m_closure[0];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );


    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_4_completions$$$function_1_iter_import_completions );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_locals {
    PyObject *var_c;
    PyObject *var_tree_name;
    PyObject *var_definition;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_locals *generator_heap = (struct jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_c = NULL;
    generator_heap->var_tree_name = NULL;
    generator_heap->var_definition = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_91acf3337269e0ab62a5d399cafa6f47, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "completions" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 182;
            generator_heap->type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }

        tmp_iter_arg_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 182;
            generator_heap->type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "oooc";
                generator_heap->exception_lineno = 182;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = generator_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = generator_heap->var_c;
            generator_heap->var_c = tmp_assign_source_3;
            Py_INCREF( generator_heap->var_c );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( generator_heap->var_c );
        tmp_source_name_2 = generator_heap->var_c;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__name );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 183;
            generator_heap->type_description_1 = "oooc";
            goto try_except_handler_2;
        }
        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_tree_name );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 183;
            generator_heap->type_description_1 = "oooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = generator_heap->var_tree_name;
            generator_heap->var_tree_name = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( generator_heap->var_tree_name );
        tmp_compexpr_left_1 = generator_heap->var_tree_name;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( generator_heap->var_tree_name );
        tmp_called_instance_1 = generator_heap->var_tree_name;
        generator->m_frame->m_frame.f_lineno = 186;
        tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_definition );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 186;
            generator_heap->type_description_1 = "oooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = generator_heap->var_definition;
            generator_heap->var_definition = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( generator_heap->var_definition );
        tmp_compexpr_left_2 = generator_heap->var_definition;
        tmp_compexpr_right_2 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( generator_heap->var_definition );
        tmp_source_name_3 = generator_heap->var_definition;
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_type );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "oooc";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_3 = const_tuple_str_plain_import_name_str_plain_import_from_tuple;
        generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "oooc";
            goto try_except_handler_2;
        }
        tmp_and_right_value_1 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_expression_name_1;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            CHECK_OBJECT( generator_heap->var_c );
            tmp_expression_name_1 = generator_heap->var_c;
            Py_INCREF( tmp_expression_name_1 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(nuitka_bool), &tmp_and_right_value_1, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(nuitka_bool), &tmp_and_right_value_1, sizeof(nuitka_bool), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 189;
                generator_heap->type_description_1 = "oooc";
                goto try_except_handler_2;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 182;
        generator_heap->type_description_1 = "oooc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator_heap->var_c,
            generator_heap->var_tree_name,
            generator_heap->var_definition,
            generator->m_closure[0]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    Py_XDECREF( generator_heap->var_tree_name );
    generator_heap->var_tree_name = NULL;

    Py_XDECREF( generator_heap->var_definition );
    generator_heap->var_definition = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    Py_XDECREF( generator_heap->var_tree_name );
    generator_heap->var_tree_name = NULL;

    Py_XDECREF( generator_heap->var_definition );
    generator_heap->var_definition = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_maker( void )
{
    return Nuitka_Generator_New(
        jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_context,
        module_jedi$api,
        const_str_plain_iter_import_completions,
#if PYTHON_VERSION >= 350
        const_str_digest_9d8c6d366414ef1e85c16a13d371a449,
#endif
        codeobj_91acf3337269e0ab62a5d399cafa6f47,
        1,
        sizeof(struct jedi$api$$$function_4_completions$$$function_1_iter_import_completions$$$genobj_1_iter_import_completions_locals)
    );
}


static PyObject *impl_jedi$api$$$function_5_goto_definitions( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_leaf = NULL;
    PyObject *var_context = NULL;
    PyObject *var_definitions = NULL;
    PyObject *var_names = NULL;
    PyObject *var_defs = NULL;
    PyObject *outline_0_var_s = NULL;
    PyObject *outline_1_var_name = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_d377d00d18d32214d6677962c7a243cd;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_18864da8d67c001f2ef4d02e01a19cd4_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_18864da8d67c001f2ef4d02e01a19cd4_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    struct Nuitka_FrameObject *frame_4c473900aeb01acfaf0655fa87b8600e_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_4c473900aeb01acfaf0655fa87b8600e_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_d377d00d18d32214d6677962c7a243cd = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d377d00d18d32214d6677962c7a243cd, codeobj_d377d00d18d32214d6677962c7a243cd, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d377d00d18d32214d6677962c7a243cd = cache_frame_d377d00d18d32214d6677962c7a243cd;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d377d00d18d32214d6677962c7a243cd );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d377d00d18d32214d6677962c7a243cd ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__module_node );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_name_of_position );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__pos );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 213;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_d377d00d18d32214d6677962c7a243cd->m_frame.f_lineno = 213;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_leaf == NULL );
        var_leaf = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_leaf );
        tmp_compexpr_left_1 = var_leaf;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__module_node );
            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_get_leaf_for_position );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__pos );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 215;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            frame_d377d00d18d32214d6677962c7a243cd->m_frame.f_lineno = 215;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_leaf;
                assert( old != NULL );
                var_leaf = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_leaf );
            tmp_compexpr_left_2 = var_leaf;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_return_value = PyList_New( 0 );
            goto frame_return_exit_1;
            branch_no_2:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_source_name_8;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_source_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain__evaluator );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_create_context );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_d377d00d18d32214d6677962c7a243cd->m_frame.f_lineno = 219;
        tmp_args_element_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__get_module );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 219;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_leaf );
        tmp_args_element_name_4 = var_leaf;
        frame_d377d00d18d32214d6677962c7a243cd->m_frame.f_lineno = 219;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_context == NULL );
        var_context = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_10;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_helpers );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 220;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_1;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_evaluate_goto_definition );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 220;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_10 = par_self;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain__evaluator );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 220;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_context );
        tmp_args_element_name_6 = var_context;
        CHECK_OBJECT( var_leaf );
        tmp_args_element_name_7 = var_leaf;
        frame_d377d00d18d32214d6677962c7a243cd->m_frame.f_lineno = 220;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 220;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_definitions == NULL );
        var_definitions = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( var_definitions );
            tmp_iter_arg_1 = var_definitions;
            tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_6;
        }
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_18864da8d67c001f2ef4d02e01a19cd4_2, codeobj_18864da8d67c001f2ef4d02e01a19cd4, module_jedi$api, sizeof(void *) );
        frame_18864da8d67c001f2ef4d02e01a19cd4_2 = cache_frame_18864da8d67c001f2ef4d02e01a19cd4_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_18864da8d67c001f2ef4d02e01a19cd4_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_18864da8d67c001f2ef4d02e01a19cd4_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 222;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_9 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_s;
                outline_0_var_s = tmp_assign_source_9;
                Py_INCREF( outline_0_var_s );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_s );
            tmp_source_name_11 = outline_0_var_s;
            tmp_append_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_name );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_5 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_5 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_18864da8d67c001f2ef4d02e01a19cd4_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_18864da8d67c001f2ef4d02e01a19cd4_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_18864da8d67c001f2ef4d02e01a19cd4_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_18864da8d67c001f2ef4d02e01a19cd4_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_18864da8d67c001f2ef4d02e01a19cd4_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_18864da8d67c001f2ef4d02e01a19cd4_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_18864da8d67c001f2ef4d02e01a19cd4_2,
            type_description_2,
            outline_0_var_s
        );


        // Release cached frame.
        if ( frame_18864da8d67c001f2ef4d02e01a19cd4_2 == cache_frame_18864da8d67c001f2ef4d02e01a19cd4_2 )
        {
            Py_DECREF( frame_18864da8d67c001f2ef4d02e01a19cd4_2 );
        }
        cache_frame_18864da8d67c001f2ef4d02e01a19cd4_2 = NULL;

        assertFrameObject( frame_18864da8d67c001f2ef4d02e01a19cd4_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_s );
        outline_0_var_s = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_s );
        outline_0_var_s = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
        return NULL;
        outline_exception_1:;
        exception_lineno = 222;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_names == NULL );
        var_names = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_10;
        // Tried code:
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_iter_arg_2;
            CHECK_OBJECT( var_names );
            tmp_iter_arg_2 = var_names;
            tmp_assign_source_11 = MAKE_ITERATOR( tmp_iter_arg_2 );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_1 = "oooooo";
                goto try_except_handler_4;
            }
            assert( tmp_listcomp_2__$0 == NULL );
            tmp_listcomp_2__$0 = tmp_assign_source_11;
        }
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = PyList_New( 0 );
            assert( tmp_listcomp_2__contraction == NULL );
            tmp_listcomp_2__contraction = tmp_assign_source_12;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_4c473900aeb01acfaf0655fa87b8600e_3, codeobj_4c473900aeb01acfaf0655fa87b8600e, module_jedi$api, sizeof(void *)+sizeof(void *) );
        frame_4c473900aeb01acfaf0655fa87b8600e_3 = cache_frame_4c473900aeb01acfaf0655fa87b8600e_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_4c473900aeb01acfaf0655fa87b8600e_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_4c473900aeb01acfaf0655fa87b8600e_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_13;
            CHECK_OBJECT( tmp_listcomp_2__$0 );
            tmp_next_source_2 = tmp_listcomp_2__$0;
            tmp_assign_source_13 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 223;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = tmp_listcomp_2__iter_value_0;
                tmp_listcomp_2__iter_value_0 = tmp_assign_source_13;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_14;
            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
            tmp_assign_source_14 = tmp_listcomp_2__iter_value_0;
            {
                PyObject *old = outline_1_var_name;
                outline_1_var_name = tmp_assign_source_14;
                Py_INCREF( outline_1_var_name );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_2;
            PyObject *tmp_append_value_2;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_source_name_13;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_listcomp_2__contraction );
            tmp_append_list_2 = tmp_listcomp_2__contraction;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_classes );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_classes );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 223;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }

            tmp_source_name_12 = tmp_mvar_value_2;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_Definition );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_13 = par_self;
            tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain__evaluator );
            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 223;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( outline_1_var_name );
            tmp_args_element_name_9 = outline_1_var_name;
            frame_4c473900aeb01acfaf0655fa87b8600e_3->m_frame.f_lineno = 223;
            {
                PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_append_value_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_append_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            assert( PyList_Check( tmp_append_list_2 ) );
            tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
            Py_DECREF( tmp_append_value_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_2 = "oo";
            goto try_except_handler_5;
        }
        goto loop_start_2;
        loop_end_2:;
        CHECK_OBJECT( tmp_listcomp_2__contraction );
        tmp_assign_source_10 = tmp_listcomp_2__contraction;
        Py_INCREF( tmp_assign_source_10 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        goto frame_return_exit_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_4c473900aeb01acfaf0655fa87b8600e_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_3:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_4c473900aeb01acfaf0655fa87b8600e_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_4;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_4c473900aeb01acfaf0655fa87b8600e_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_4c473900aeb01acfaf0655fa87b8600e_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_4c473900aeb01acfaf0655fa87b8600e_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_4c473900aeb01acfaf0655fa87b8600e_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_4c473900aeb01acfaf0655fa87b8600e_3,
            type_description_2,
            outline_1_var_name,
            par_self
        );


        // Release cached frame.
        if ( frame_4c473900aeb01acfaf0655fa87b8600e_3 == cache_frame_4c473900aeb01acfaf0655fa87b8600e_3 )
        {
            Py_DECREF( frame_4c473900aeb01acfaf0655fa87b8600e_3 );
        }
        cache_frame_4c473900aeb01acfaf0655fa87b8600e_3 = NULL;

        assertFrameObject( frame_4c473900aeb01acfaf0655fa87b8600e_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "oooooo";
        goto try_except_handler_4;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_XDECREF( outline_1_var_name );
        outline_1_var_name = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var_name );
        outline_1_var_name = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
        return NULL;
        outline_exception_2:;
        exception_lineno = 223;
        goto frame_exception_exit_1;
        outline_result_2:;
        assert( var_defs == NULL );
        var_defs = tmp_assign_source_10;
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_set_arg_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_helpers );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 227;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_3;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_sorted_definitions );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_defs );
        tmp_set_arg_1 = var_defs;
        tmp_args_element_name_10 = PySet_New( tmp_set_arg_1 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 227;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_d377d00d18d32214d6677962c7a243cd->m_frame.f_lineno = 227;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d377d00d18d32214d6677962c7a243cd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d377d00d18d32214d6677962c7a243cd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d377d00d18d32214d6677962c7a243cd );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d377d00d18d32214d6677962c7a243cd, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d377d00d18d32214d6677962c7a243cd->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d377d00d18d32214d6677962c7a243cd, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d377d00d18d32214d6677962c7a243cd,
        type_description_1,
        par_self,
        var_leaf,
        var_context,
        var_definitions,
        var_names,
        var_defs
    );


    // Release cached frame.
    if ( frame_d377d00d18d32214d6677962c7a243cd == cache_frame_d377d00d18d32214d6677962c7a243cd )
    {
        Py_DECREF( frame_d377d00d18d32214d6677962c7a243cd );
    }
    cache_frame_d377d00d18d32214d6677962c7a243cd = NULL;

    assertFrameObject( frame_d377d00d18d32214d6677962c7a243cd );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_leaf );
    var_leaf = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_defs );
    var_defs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_leaf );
    var_leaf = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_defs );
    var_defs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_5_goto_definitions );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_6_goto_assignments( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_follow_imports = python_pars[ 1 ];
    struct Nuitka_CellObject *par_follow_builtin_imports = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *var_filter_follow_imports = PyCell_EMPTY();
    PyObject *var_tree_name = NULL;
    PyObject *var_context = NULL;
    PyObject *var_names = NULL;
    PyObject *var_check = NULL;
    PyObject *var_defs = NULL;
    PyObject *outline_0_var_d = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_078186f669372586eb34c685197d831f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_dd6dfa35d65ab0fc6b224a567ddd2465_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_078186f669372586eb34c685197d831f = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = var_filter_follow_imports;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] = par_follow_builtin_imports;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] );


        assert( PyCell_GET( var_filter_follow_imports ) == NULL );
        PyCell_SET( var_filter_follow_imports, tmp_assign_source_1 );

    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_078186f669372586eb34c685197d831f, codeobj_078186f669372586eb34c685197d831f, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_078186f669372586eb34c685197d831f = cache_frame_078186f669372586eb34c685197d831f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_078186f669372586eb34c685197d831f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_078186f669372586eb34c685197d831f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__module_node );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_name_of_position );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__pos );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 259;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        frame_078186f669372586eb34c685197d831f->m_frame.f_lineno = 259;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        assert( var_tree_name == NULL );
        var_tree_name = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_tree_name );
        tmp_compexpr_left_1 = var_tree_name;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = PyList_New( 0 );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__evaluator );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_create_context );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_078186f669372586eb34c685197d831f->m_frame.f_lineno = 262;
        tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__get_module );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 262;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tree_name );
        tmp_args_element_name_3 = var_tree_name;
        frame_078186f669372586eb34c685197d831f->m_frame.f_lineno = 262;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        assert( var_context == NULL );
        var_context = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__evaluator );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 263;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_context );
        tmp_args_element_name_4 = var_context;
        CHECK_OBJECT( var_tree_name );
        tmp_args_element_name_5 = var_tree_name;
        frame_078186f669372586eb34c685197d831f->m_frame.f_lineno = 263;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_list_arg_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_goto, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 263;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 263;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        assert( var_names == NULL );
        var_names = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_follow_imports );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_follow_imports );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_2_check(  );



            assert( var_check == NULL );
            var_check = tmp_assign_source_5;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_3_check(  );



            assert( var_check == NULL );
            var_check = tmp_assign_source_6;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        CHECK_OBJECT( PyCell_GET( var_filter_follow_imports ) );
        tmp_called_name_3 = PyCell_GET( var_filter_follow_imports );
        CHECK_OBJECT( var_names );
        tmp_args_element_name_6 = var_names;
        CHECK_OBJECT( var_check );
        tmp_args_element_name_7 = var_check;
        frame_078186f669372586eb34c685197d831f->m_frame.f_lineno = 272;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_names;
            assert( old != NULL );
            var_names = tmp_assign_source_7;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        // Tried code:
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_set_arg_1;
            CHECK_OBJECT( var_names );
            tmp_set_arg_1 = var_names;
            tmp_iter_arg_1 = PySet_New( tmp_set_arg_1 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 274;
                type_description_1 = "ooccooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 274;
                type_description_1 = "ooccooooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_9;
        }
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_10;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_dd6dfa35d65ab0fc6b224a567ddd2465_2, codeobj_dd6dfa35d65ab0fc6b224a567ddd2465, module_jedi$api, sizeof(void *)+sizeof(void *) );
        frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 = cache_frame_dd6dfa35d65ab0fc6b224a567ddd2465_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_11;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 274;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_11;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_12;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_12 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_d;
                outline_0_var_d = tmp_assign_source_12;
                Py_INCREF( outline_0_var_d );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_source_name_8;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_classes );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_classes );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 274;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }

            tmp_source_name_7 = tmp_mvar_value_1;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_Definition );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 274;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_8 = par_self;
            tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain__evaluator );
            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 274;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( outline_0_var_d );
            tmp_args_element_name_9 = outline_0_var_d;
            frame_dd6dfa35d65ab0fc6b224a567ddd2465_2->m_frame.f_lineno = 274;
            {
                PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 274;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 274;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_8 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_8 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_dd6dfa35d65ab0fc6b224a567ddd2465_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_dd6dfa35d65ab0fc6b224a567ddd2465_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_dd6dfa35d65ab0fc6b224a567ddd2465_2,
            type_description_2,
            outline_0_var_d,
            par_self
        );


        // Release cached frame.
        if ( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 == cache_frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 )
        {
            Py_DECREF( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 );
        }
        cache_frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 = NULL;

        assertFrameObject( frame_dd6dfa35d65ab0fc6b224a567ddd2465_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooccooooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_d );
        outline_0_var_d = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_d );
        outline_0_var_d = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments );
        return NULL;
        outline_exception_1:;
        exception_lineno = 274;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_defs == NULL );
        var_defs = tmp_assign_source_8;
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_helpers );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 275;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_2;
        CHECK_OBJECT( var_defs );
        tmp_args_element_name_10 = var_defs;
        frame_078186f669372586eb34c685197d831f->m_frame.f_lineno = 275;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_sorted_definitions, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "ooccooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_078186f669372586eb34c685197d831f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_078186f669372586eb34c685197d831f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_078186f669372586eb34c685197d831f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_078186f669372586eb34c685197d831f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_078186f669372586eb34c685197d831f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_078186f669372586eb34c685197d831f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_078186f669372586eb34c685197d831f,
        type_description_1,
        par_self,
        par_follow_imports,
        par_follow_builtin_imports,
        var_filter_follow_imports,
        var_tree_name,
        var_context,
        var_names,
        var_check,
        var_defs
    );


    // Release cached frame.
    if ( frame_078186f669372586eb34c685197d831f == cache_frame_078186f669372586eb34c685197d831f )
    {
        Py_DECREF( frame_078186f669372586eb34c685197d831f );
    }
    cache_frame_078186f669372586eb34c685197d831f = NULL;

    assertFrameObject( frame_078186f669372586eb34c685197d831f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_imports );
    Py_DECREF( par_follow_imports );
    par_follow_imports = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_builtin_imports );
    Py_DECREF( par_follow_builtin_imports );
    par_follow_builtin_imports = NULL;

    CHECK_OBJECT( (PyObject *)var_filter_follow_imports );
    Py_DECREF( var_filter_follow_imports );
    var_filter_follow_imports = NULL;

    CHECK_OBJECT( (PyObject *)var_tree_name );
    Py_DECREF( var_tree_name );
    var_tree_name = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_check );
    var_check = NULL;

    Py_XDECREF( var_defs );
    var_defs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_imports );
    Py_DECREF( par_follow_imports );
    par_follow_imports = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_builtin_imports );
    Py_DECREF( par_follow_builtin_imports );
    par_follow_builtin_imports = NULL;

    CHECK_OBJECT( (PyObject *)var_filter_follow_imports );
    Py_DECREF( var_filter_follow_imports );
    var_filter_follow_imports = NULL;

    Py_XDECREF( var_tree_name );
    var_tree_name = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_check );
    var_check = NULL;

    Py_XDECREF( var_defs );
    var_defs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_names = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_check = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_check;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = self->m_closure[0];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] = self->m_closure[1];
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] = par_names;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_names );
    Py_DECREF( par_names );
    par_names = NULL;

    CHECK_OBJECT( (PyObject *)par_check );
    Py_DECREF( par_check );
    par_check = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_names );
    Py_DECREF( par_names );
    par_names = NULL;

    CHECK_OBJECT( (PyObject *)par_check );
    Py_DECREF( par_check );
    par_check = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_locals {
    PyObject *var_name;
    PyObject *var_new_names;
    PyObject *var_found_builtin;
    PyObject *var_new_name;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_for_loop_2__for_iterator;
    PyObject *tmp_for_loop_2__iter_value;
    PyObject *tmp_for_loop_3__for_iterator;
    PyObject *tmp_for_loop_3__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_locals *generator_heap = (struct jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_name = NULL;
    generator_heap->var_new_names = NULL;
    generator_heap->var_found_builtin = NULL;
    generator_heap->var_new_name = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_for_loop_2__for_iterator = NULL;
    generator_heap->tmp_for_loop_2__iter_value = NULL;
    generator_heap->tmp_for_loop_3__for_iterator = NULL;
    generator_heap->tmp_for_loop_3__iter_value = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_4ae0934951a98b59abe31a69fd48febb, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        if ( PyCell_GET( generator->m_closure[3] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "names" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 242;
            generator_heap->type_description_1 = "ccoooocc";
            goto frame_exception_exit_1;
        }

        tmp_iter_arg_1 = PyCell_GET( generator->m_closure[3] );
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 242;
            generator_heap->type_description_1 = "ccoooocc";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "ccoooocc";
                generator_heap->exception_lineno = 242;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = generator_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = generator_heap->var_name;
            generator_heap->var_name = tmp_assign_source_3;
            Py_INCREF( generator_heap->var_name );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "check" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 243;
            generator_heap->type_description_1 = "ccoooocc";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = PyCell_GET( generator->m_closure[0] );
        CHECK_OBJECT( generator_heap->var_name );
        tmp_args_element_name_1 = generator_heap->var_name;
        generator->m_frame->m_frame.f_lineno = 243;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 243;
            generator_heap->type_description_1 = "ccoooocc";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_call_result_1 );

            generator_heap->exception_lineno = 243;
            generator_heap->type_description_1 = "ccoooocc";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_list_arg_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_3;
            if ( PyCell_GET( generator->m_closure[1] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "filter_follow_imports" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 244;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }

            tmp_called_name_2 = PyCell_GET( generator->m_closure[1] );
            CHECK_OBJECT( generator_heap->var_name );
            tmp_called_instance_1 = generator_heap->var_name;
            generator->m_frame->m_frame.f_lineno = 244;
            tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_goto );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 244;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            if ( PyCell_GET( generator->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_args_element_name_2 );
                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "check" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 244;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }

            tmp_args_element_name_3 = PyCell_GET( generator->m_closure[0] );
            generator->m_frame->m_frame.f_lineno = 244;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_list_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_list_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 244;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            tmp_assign_source_4 = PySequence_List( tmp_list_arg_1 );
            Py_DECREF( tmp_list_arg_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 244;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            {
                PyObject *old = generator_heap->var_new_names;
                generator_heap->var_new_names = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = Py_False;
            {
                PyObject *old = generator_heap->var_found_builtin;
                generator_heap->var_found_builtin = tmp_assign_source_5;
                Py_INCREF( generator_heap->var_found_builtin );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_2;
            if ( PyCell_GET( generator->m_closure[2] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "follow_builtin_imports" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 246;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }

            tmp_truth_name_2 = CHECK_IF_TRUE( PyCell_GET( generator->m_closure[2] ) );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 246;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_iter_arg_2;
                CHECK_OBJECT( generator_heap->var_new_names );
                tmp_iter_arg_2 = generator_heap->var_new_names;
                tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_2 );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 247;
                    generator_heap->type_description_1 = "ccoooocc";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = generator_heap->tmp_for_loop_2__for_iterator;
                    generator_heap->tmp_for_loop_2__for_iterator = tmp_assign_source_6;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_7;
                CHECK_OBJECT( generator_heap->tmp_for_loop_2__for_iterator );
                tmp_next_source_2 = generator_heap->tmp_for_loop_2__for_iterator;
                tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_7 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_1 = "ccoooocc";
                        generator_heap->exception_lineno = 247;
                        goto try_except_handler_3;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_for_loop_2__iter_value;
                    generator_heap->tmp_for_loop_2__iter_value = tmp_assign_source_7;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_8;
                CHECK_OBJECT( generator_heap->tmp_for_loop_2__iter_value );
                tmp_assign_source_8 = generator_heap->tmp_for_loop_2__iter_value;
                {
                    PyObject *old = generator_heap->var_new_name;
                    generator_heap->var_new_name = tmp_assign_source_8;
                    Py_INCREF( generator_heap->var_new_name );
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_1;
                PyObject *tmp_compexpr_right_1;
                PyObject *tmp_source_name_1;
                CHECK_OBJECT( generator_heap->var_new_name );
                tmp_source_name_1 = generator_heap->var_new_name;
                tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_start_pos );
                if ( tmp_compexpr_left_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 248;
                    generator_heap->type_description_1 = "ccoooocc";
                    goto try_except_handler_3;
                }
                tmp_compexpr_right_1 = Py_None;
                tmp_condition_result_3 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_compexpr_left_1 );
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_9;
                    tmp_assign_source_9 = Py_True;
                    {
                        PyObject *old = generator_heap->var_found_builtin;
                        generator_heap->var_found_builtin = tmp_assign_source_9;
                        Py_INCREF( generator_heap->var_found_builtin );
                        Py_XDECREF( old );
                    }

                }
                branch_no_3:;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 247;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_3;
            }
            goto loop_start_2;
            loop_end_2:;
            goto try_end_1;
            // Exception handler code:
            try_except_handler_3:;
            generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
            generator_heap->tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
            generator_heap->tmp_for_loop_2__for_iterator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_1;
            generator_heap->exception_value = generator_heap->exception_keeper_value_1;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

            goto try_except_handler_2;
            // End of try:
            try_end_1:;
            Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
            generator_heap->tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
            generator_heap->tmp_for_loop_2__for_iterator = NULL;

            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_4;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            int tmp_truth_name_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_1;
            if ( generator_heap->var_found_builtin == NULL )
            {

                generator_heap->exception_type = PyExc_UnboundLocalError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "found_builtin" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 251;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }

            tmp_truth_name_3 = CHECK_IF_TRUE( generator_heap->var_found_builtin );
            if ( tmp_truth_name_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 251;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            tmp_and_left_value_1 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( generator_heap->var_name );
            tmp_isinstance_inst_1 = generator_heap->var_name;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_imports );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_imports );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "imports" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 251;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }

            tmp_source_name_2 = tmp_mvar_value_1;
            tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_SubModuleName );
            if ( tmp_isinstance_cls_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 251;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            generator_heap->tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            Py_DECREF( tmp_isinstance_cls_1 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 251;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            tmp_operand_name_1 = ( generator_heap->tmp_res != 0 ) ? Py_True : Py_False;
            generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 251;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            tmp_and_right_value_1 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_4 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_4 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_expression_name_1;
                NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
                CHECK_OBJECT( generator_heap->var_name );
                tmp_expression_name_1 = generator_heap->var_name;
                Py_INCREF( tmp_expression_name_1 );
                Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(nuitka_bool), &tmp_and_right_value_1, sizeof(nuitka_bool), &tmp_truth_name_3, sizeof(int), &tmp_operand_name_1, sizeof(PyObject *), &tmp_isinstance_inst_1, sizeof(PyObject *), &tmp_isinstance_cls_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
                generator->m_yield_return_index = 1;
                return tmp_expression_name_1;
                yield_return_1:
                Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(nuitka_bool), &tmp_and_right_value_1, sizeof(nuitka_bool), &tmp_truth_name_3, sizeof(int), &tmp_operand_name_1, sizeof(PyObject *), &tmp_isinstance_inst_1, sizeof(PyObject *), &tmp_isinstance_cls_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 252;
                    generator_heap->type_description_1 = "ccoooocc";
                    goto try_except_handler_2;
                }
                tmp_yield_result_1 = yield_return_value;
            }
            goto branch_end_4;
            branch_no_4:;
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_iter_arg_3;
                CHECK_OBJECT( generator_heap->var_new_names );
                tmp_iter_arg_3 = generator_heap->var_new_names;
                tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
                if ( tmp_assign_source_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 254;
                    generator_heap->type_description_1 = "ccoooocc";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = generator_heap->tmp_for_loop_3__for_iterator;
                    generator_heap->tmp_for_loop_3__for_iterator = tmp_assign_source_10;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_3:;
            {
                PyObject *tmp_next_source_3;
                PyObject *tmp_assign_source_11;
                CHECK_OBJECT( generator_heap->tmp_for_loop_3__for_iterator );
                tmp_next_source_3 = generator_heap->tmp_for_loop_3__for_iterator;
                tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_3 );
                if ( tmp_assign_source_11 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_3;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_1 = "ccoooocc";
                        generator_heap->exception_lineno = 254;
                        goto try_except_handler_4;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_for_loop_3__iter_value;
                    generator_heap->tmp_for_loop_3__iter_value = tmp_assign_source_11;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_12;
                CHECK_OBJECT( generator_heap->tmp_for_loop_3__iter_value );
                tmp_assign_source_12 = generator_heap->tmp_for_loop_3__iter_value;
                {
                    PyObject *old = generator_heap->var_new_name;
                    generator_heap->var_new_name = tmp_assign_source_12;
                    Py_INCREF( generator_heap->var_new_name );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_expression_name_2;
                NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
                CHECK_OBJECT( generator_heap->var_new_name );
                tmp_expression_name_2 = generator_heap->var_new_name;
                Py_INCREF( tmp_expression_name_2 );
                Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(nuitka_bool), &tmp_and_right_value_1, sizeof(nuitka_bool), &tmp_truth_name_3, sizeof(int), &tmp_operand_name_1, sizeof(PyObject *), &tmp_isinstance_inst_1, sizeof(PyObject *), &tmp_isinstance_cls_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
                generator->m_yield_return_index = 2;
                return tmp_expression_name_2;
                yield_return_2:
                Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(nuitka_bool), &tmp_and_right_value_1, sizeof(nuitka_bool), &tmp_truth_name_3, sizeof(int), &tmp_operand_name_1, sizeof(PyObject *), &tmp_isinstance_inst_1, sizeof(PyObject *), &tmp_isinstance_cls_1, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 255;
                    generator_heap->type_description_1 = "ccoooocc";
                    goto try_except_handler_4;
                }
                tmp_yield_result_2 = yield_return_value;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 254;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_4;
            }
            goto loop_start_3;
            loop_end_3:;
            goto try_end_2;
            // Exception handler code:
            try_except_handler_4:;
            generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_for_loop_3__iter_value );
            generator_heap->tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_3__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_3__for_iterator );
            generator_heap->tmp_for_loop_3__for_iterator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_2;
            generator_heap->exception_value = generator_heap->exception_keeper_value_2;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

            goto try_except_handler_2;
            // End of try:
            try_end_2:;
            Py_XDECREF( generator_heap->tmp_for_loop_3__iter_value );
            generator_heap->tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_3__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_3__for_iterator );
            generator_heap->tmp_for_loop_3__for_iterator = NULL;

            branch_end_4:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_expression_name_3;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_3;
            CHECK_OBJECT( generator_heap->var_name );
            tmp_expression_name_3 = generator_heap->var_name;
            Py_INCREF( tmp_expression_name_3 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), NULL );
            generator->m_yield_return_index = 3;
            return tmp_expression_name_3;
            yield_return_3:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 257;
                generator_heap->type_description_1 = "ccoooocc";
                goto try_except_handler_2;
            }
            tmp_yield_result_3 = yield_return_value;
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 242;
        generator_heap->type_description_1 = "ccoooocc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[3],
            generator->m_closure[0],
            generator_heap->var_name,
            generator_heap->var_new_names,
            generator_heap->var_found_builtin,
            generator_heap->var_new_name,
            generator->m_closure[1],
            generator->m_closure[2]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;

    Py_XDECREF( generator_heap->var_new_names );
    generator_heap->var_new_names = NULL;

    Py_XDECREF( generator_heap->var_found_builtin );
    generator_heap->var_found_builtin = NULL;

    Py_XDECREF( generator_heap->var_new_name );
    generator_heap->var_new_name = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;

    Py_XDECREF( generator_heap->var_new_names );
    generator_heap->var_new_names = NULL;

    Py_XDECREF( generator_heap->var_found_builtin );
    generator_heap->var_found_builtin = NULL;

    Py_XDECREF( generator_heap->var_new_name );
    generator_heap->var_new_name = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_maker( void )
{
    return Nuitka_Generator_New(
        jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_context,
        module_jedi$api,
        const_str_plain_filter_follow_imports,
#if PYTHON_VERSION >= 350
        const_str_digest_910348dcbc1269ee447323aeeea7e275,
#endif
        codeobj_4ae0934951a98b59abe31a69fd48febb,
        4,
        sizeof(struct jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports$$$genobj_1_filter_follow_imports_locals)
    );
}


static PyObject *impl_jedi$api$$$function_6_goto_assignments$$$function_2_check( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3636078bdb31c7973aad9575da42b0a5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3636078bdb31c7973aad9575da42b0a5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3636078bdb31c7973aad9575da42b0a5, codeobj_3636078bdb31c7973aad9575da42b0a5, module_jedi$api, sizeof(void *) );
    frame_3636078bdb31c7973aad9575da42b0a5 = cache_frame_3636078bdb31c7973aad9575da42b0a5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3636078bdb31c7973aad9575da42b0a5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3636078bdb31c7973aad9575da42b0a5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_name );
        tmp_called_instance_1 = par_name;
        frame_3636078bdb31c7973aad9575da42b0a5->m_frame.f_lineno = 267;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_is_import );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 267;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3636078bdb31c7973aad9575da42b0a5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3636078bdb31c7973aad9575da42b0a5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3636078bdb31c7973aad9575da42b0a5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3636078bdb31c7973aad9575da42b0a5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3636078bdb31c7973aad9575da42b0a5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3636078bdb31c7973aad9575da42b0a5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3636078bdb31c7973aad9575da42b0a5,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_3636078bdb31c7973aad9575da42b0a5 == cache_frame_3636078bdb31c7973aad9575da42b0a5 )
    {
        Py_DECREF( frame_3636078bdb31c7973aad9575da42b0a5 );
    }
    cache_frame_3636078bdb31c7973aad9575da42b0a5 = NULL;

    assertFrameObject( frame_3636078bdb31c7973aad9575da42b0a5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments$$$function_2_check );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments$$$function_2_check );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_6_goto_assignments$$$function_3_check( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_4f569dc0a04bc624621bfb55ceaced21;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_4f569dc0a04bc624621bfb55ceaced21 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4f569dc0a04bc624621bfb55ceaced21, codeobj_4f569dc0a04bc624621bfb55ceaced21, module_jedi$api, sizeof(void *) );
    frame_4f569dc0a04bc624621bfb55ceaced21 = cache_frame_4f569dc0a04bc624621bfb55ceaced21;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4f569dc0a04bc624621bfb55ceaced21 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4f569dc0a04bc624621bfb55ceaced21 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_name );
        tmp_isinstance_inst_1 = par_name;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_imports );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_imports );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "imports" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 270;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_SubModuleName );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4f569dc0a04bc624621bfb55ceaced21 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4f569dc0a04bc624621bfb55ceaced21 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4f569dc0a04bc624621bfb55ceaced21 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4f569dc0a04bc624621bfb55ceaced21, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4f569dc0a04bc624621bfb55ceaced21->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4f569dc0a04bc624621bfb55ceaced21, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4f569dc0a04bc624621bfb55ceaced21,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_4f569dc0a04bc624621bfb55ceaced21 == cache_frame_4f569dc0a04bc624621bfb55ceaced21 )
    {
        Py_DECREF( frame_4f569dc0a04bc624621bfb55ceaced21 );
    }
    cache_frame_4f569dc0a04bc624621bfb55ceaced21 = NULL;

    assertFrameObject( frame_4f569dc0a04bc624621bfb55ceaced21 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments$$$function_3_check );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_6_goto_assignments$$$function_3_check );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_7_usages( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *par_additional_module_paths = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var__usages = NULL;
    struct Nuitka_FrameObject *frame_441859b3ba7e70dea7e3ab377b6cdb4a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_441859b3ba7e70dea7e3ab377b6cdb4a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_441859b3ba7e70dea7e3ab377b6cdb4a, codeobj_441859b3ba7e70dea7e3ab377b6cdb4a, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_441859b3ba7e70dea7e3ab377b6cdb4a = cache_frame_441859b3ba7e70dea7e3ab377b6cdb4a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_441859b3ba7e70dea7e3ab377b6cdb4a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_441859b3ba7e70dea7e3ab377b6cdb4a ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_additional_module_paths );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_additional_module_paths );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 291;
            type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_kw_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_warnings );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 292;
                type_description_1 = "cooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 292;
                type_description_1 = "cooo";
                goto frame_exception_exit_1;
            }
            tmp_tuple_element_1 = const_str_digest_ccb4af1f53eae28017d11975ed978e92;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DeprecationWarning" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 294;
                type_description_1 = "cooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_1 = tmp_mvar_value_2;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            tmp_kw_name_1 = PyDict_Copy( const_dict_f154c9a58c9419d7e391901d7b7fe49e );
            frame_441859b3ba7e70dea7e3ab377b6cdb4a->m_frame.f_lineno = 292;
            tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 292;
                type_description_1 = "cooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_true_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_1 = MAKE_FUNCTION_jedi$api$$$function_7_usages$$$function_1__usages( tmp_defaults_1 );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_self;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var__usages == NULL );
        var__usages = tmp_assign_source_1;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        CHECK_OBJECT( var__usages );
        tmp_dircall_arg1_1 = var__usages;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg2_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_return_value = impl___internal__$$$function_2_complex_call_helper_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_441859b3ba7e70dea7e3ab377b6cdb4a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_441859b3ba7e70dea7e3ab377b6cdb4a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_441859b3ba7e70dea7e3ab377b6cdb4a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_441859b3ba7e70dea7e3ab377b6cdb4a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_441859b3ba7e70dea7e3ab377b6cdb4a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_441859b3ba7e70dea7e3ab377b6cdb4a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_441859b3ba7e70dea7e3ab377b6cdb4a,
        type_description_1,
        par_self,
        par_additional_module_paths,
        par_kwargs,
        var__usages
    );


    // Release cached frame.
    if ( frame_441859b3ba7e70dea7e3ab377b6cdb4a == cache_frame_441859b3ba7e70dea7e3ab377b6cdb4a )
    {
        Py_DECREF( frame_441859b3ba7e70dea7e3ab377b6cdb4a );
    }
    cache_frame_441859b3ba7e70dea7e3ab377b6cdb4a = NULL;

    assertFrameObject( frame_441859b3ba7e70dea7e3ab377b6cdb4a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_additional_module_paths );
    Py_DECREF( par_additional_module_paths );
    par_additional_module_paths = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var__usages );
    Py_DECREF( var__usages );
    var__usages = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_additional_module_paths );
    Py_DECREF( par_additional_module_paths );
    par_additional_module_paths = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var__usages );
    var__usages = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_7_usages$$$function_1__usages( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_include_builtins = python_pars[ 0 ];
    PyObject *var_tree_name = NULL;
    PyObject *var_names = NULL;
    PyObject *var_definitions = NULL;
    PyObject *outline_0_var_n = NULL;
    PyObject *outline_1_var_d = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_62e2049811a9fa0bb8d9072fc36548f6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_00283d797dc6e53bc846371cfaafc3f0_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_00283d797dc6e53bc846371cfaafc3f0_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    struct Nuitka_FrameObject *frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_62e2049811a9fa0bb8d9072fc36548f6 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_62e2049811a9fa0bb8d9072fc36548f6, codeobj_62e2049811a9fa0bb8d9072fc36548f6, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_62e2049811a9fa0bb8d9072fc36548f6 = cache_frame_62e2049811a9fa0bb8d9072fc36548f6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_62e2049811a9fa0bb8d9072fc36548f6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_62e2049811a9fa0bb8d9072fc36548f6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 299;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( self->m_closure[0] );
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__module_node );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_name_of_position );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 299;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( self->m_closure[0] );
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__pos );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 299;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        frame_62e2049811a9fa0bb8d9072fc36548f6->m_frame.f_lineno = 299;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        assert( var_tree_name == NULL );
        var_tree_name = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_tree_name );
        tmp_compexpr_left_1 = var_tree_name;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = PyList_New( 0 );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_usages );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_usages );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "usages" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 304;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_1;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_usages );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 304;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
        frame_62e2049811a9fa0bb8d9072fc36548f6->m_frame.f_lineno = 304;
        tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__get_module );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 304;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tree_name );
        tmp_args_element_name_3 = var_tree_name;
        frame_62e2049811a9fa0bb8d9072fc36548f6->m_frame.f_lineno = 304;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        assert( var_names == NULL );
        var_names = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( var_names );
            tmp_iter_arg_1 = var_names;
            tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 306;
                type_description_1 = "ooooc";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_00283d797dc6e53bc846371cfaafc3f0_2, codeobj_00283d797dc6e53bc846371cfaafc3f0, module_jedi$api, sizeof(void *)+sizeof(void *) );
        frame_00283d797dc6e53bc846371cfaafc3f0_2 = cache_frame_00283d797dc6e53bc846371cfaafc3f0_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_00283d797dc6e53bc846371cfaafc3f0_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_00283d797dc6e53bc846371cfaafc3f0_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oc";
                    exception_lineno = 306;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_7 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_n;
                outline_0_var_n = tmp_assign_source_7;
                Py_INCREF( outline_0_var_n );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_classes );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_classes );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 306;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }

            tmp_source_name_5 = tmp_mvar_value_2;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_Definition );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 306;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 306;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }

            tmp_source_name_6 = PyCell_GET( self->m_closure[0] );
            tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__evaluator );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 306;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( outline_0_var_n );
            tmp_args_element_name_5 = outline_0_var_n;
            frame_00283d797dc6e53bc846371cfaafc3f0_2->m_frame.f_lineno = 306;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 306;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 306;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 306;
            type_description_2 = "oc";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_3 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_3 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_00283d797dc6e53bc846371cfaafc3f0_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_00283d797dc6e53bc846371cfaafc3f0_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_00283d797dc6e53bc846371cfaafc3f0_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_00283d797dc6e53bc846371cfaafc3f0_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_00283d797dc6e53bc846371cfaafc3f0_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_00283d797dc6e53bc846371cfaafc3f0_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_00283d797dc6e53bc846371cfaafc3f0_2,
            type_description_2,
            outline_0_var_n,
            self->m_closure[0]
        );


        // Release cached frame.
        if ( frame_00283d797dc6e53bc846371cfaafc3f0_2 == cache_frame_00283d797dc6e53bc846371cfaafc3f0_2 )
        {
            Py_DECREF( frame_00283d797dc6e53bc846371cfaafc3f0_2 );
        }
        cache_frame_00283d797dc6e53bc846371cfaafc3f0_2 = NULL;

        assertFrameObject( frame_00283d797dc6e53bc846371cfaafc3f0_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooooc";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_n );
        outline_0_var_n = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_n );
        outline_0_var_n = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
        return NULL;
        outline_exception_1:;
        exception_lineno = 306;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_definitions == NULL );
        var_definitions = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_include_builtins );
        tmp_operand_name_1 = par_include_builtins;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 307;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_8;
            // Tried code:
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_iter_arg_2;
                CHECK_OBJECT( var_definitions );
                tmp_iter_arg_2 = var_definitions;
                tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_2 );
                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 308;
                    type_description_1 = "ooooc";
                    goto try_except_handler_4;
                }
                assert( tmp_listcomp_2__$0 == NULL );
                tmp_listcomp_2__$0 = tmp_assign_source_9;
            }
            {
                PyObject *tmp_assign_source_10;
                tmp_assign_source_10 = PyList_New( 0 );
                assert( tmp_listcomp_2__contraction == NULL );
                tmp_listcomp_2__contraction = tmp_assign_source_10;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3, codeobj_9dcb0af9f9e7c3518a4d55e0bcdf336d, module_jedi$api, sizeof(void *) );
            frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 = cache_frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_11;
                CHECK_OBJECT( tmp_listcomp_2__$0 );
                tmp_next_source_2 = tmp_listcomp_2__$0;
                tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_11 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "o";
                        exception_lineno = 308;
                        goto try_except_handler_5;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_2__iter_value_0;
                    tmp_listcomp_2__iter_value_0 = tmp_assign_source_11;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_12;
                CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
                tmp_assign_source_12 = tmp_listcomp_2__iter_value_0;
                {
                    PyObject *old = outline_1_var_d;
                    outline_1_var_d = tmp_assign_source_12;
                    Py_INCREF( outline_1_var_d );
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_called_instance_2;
                CHECK_OBJECT( outline_1_var_d );
                tmp_called_instance_2 = outline_1_var_d;
                frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3->m_frame.f_lineno = 308;
                tmp_operand_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_in_builtin_module );
                if ( tmp_operand_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 308;
                    type_description_2 = "o";
                    goto try_except_handler_5;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                Py_DECREF( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 308;
                    type_description_2 = "o";
                    goto try_except_handler_5;
                }
                tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_append_list_2;
                    PyObject *tmp_append_value_2;
                    CHECK_OBJECT( tmp_listcomp_2__contraction );
                    tmp_append_list_2 = tmp_listcomp_2__contraction;
                    CHECK_OBJECT( outline_1_var_d );
                    tmp_append_value_2 = outline_1_var_d;
                    assert( PyList_Check( tmp_append_list_2 ) );
                    tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 308;
                        type_description_2 = "o";
                        goto try_except_handler_5;
                    }
                }
                branch_no_3:;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 308;
                type_description_2 = "o";
                goto try_except_handler_5;
            }
            goto loop_start_2;
            loop_end_2:;
            CHECK_OBJECT( tmp_listcomp_2__contraction );
            tmp_assign_source_8 = tmp_listcomp_2__contraction;
            Py_INCREF( tmp_assign_source_8 );
            goto try_return_handler_5;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
            return NULL;
            // Return handler code:
            try_return_handler_5:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
            Py_DECREF( tmp_listcomp_2__$0 );
            tmp_listcomp_2__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
            Py_DECREF( tmp_listcomp_2__contraction );
            tmp_listcomp_2__contraction = NULL;

            Py_XDECREF( tmp_listcomp_2__iter_value_0 );
            tmp_listcomp_2__iter_value_0 = NULL;

            goto frame_return_exit_3;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
            Py_DECREF( tmp_listcomp_2__$0 );
            tmp_listcomp_2__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
            Py_DECREF( tmp_listcomp_2__contraction );
            tmp_listcomp_2__contraction = NULL;

            Py_XDECREF( tmp_listcomp_2__iter_value_0 );
            tmp_listcomp_2__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_3;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_2;

            frame_return_exit_3:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_4;

            frame_exception_exit_3:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3,
                type_description_2,
                outline_1_var_d
            );


            // Release cached frame.
            if ( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 == cache_frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 )
            {
                Py_DECREF( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 );
            }
            cache_frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 = NULL;

            assertFrameObject( frame_9dcb0af9f9e7c3518a4d55e0bcdf336d_3 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_2;

            frame_no_exception_2:;
            goto skip_nested_handling_2;
            nested_frame_exit_2:;
            type_description_1 = "ooooc";
            goto try_except_handler_4;
            skip_nested_handling_2:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
            return NULL;
            // Return handler code:
            try_return_handler_4:;
            Py_XDECREF( outline_1_var_d );
            outline_1_var_d = NULL;

            goto outline_result_2;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_1_var_d );
            outline_1_var_d = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto outline_exception_2;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
            return NULL;
            outline_exception_2:;
            exception_lineno = 308;
            goto frame_exception_exit_1;
            outline_result_2:;
            {
                PyObject *old = var_definitions;
                assert( old != NULL );
                var_definitions = tmp_assign_source_8;
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_helpers );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 309;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_3;
        CHECK_OBJECT( var_definitions );
        tmp_args_element_name_6 = var_definitions;
        frame_62e2049811a9fa0bb8d9072fc36548f6->m_frame.f_lineno = 309;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_sorted_definitions, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 309;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_62e2049811a9fa0bb8d9072fc36548f6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_62e2049811a9fa0bb8d9072fc36548f6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_62e2049811a9fa0bb8d9072fc36548f6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_62e2049811a9fa0bb8d9072fc36548f6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_62e2049811a9fa0bb8d9072fc36548f6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_62e2049811a9fa0bb8d9072fc36548f6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_62e2049811a9fa0bb8d9072fc36548f6,
        type_description_1,
        par_include_builtins,
        var_tree_name,
        var_names,
        var_definitions,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_62e2049811a9fa0bb8d9072fc36548f6 == cache_frame_62e2049811a9fa0bb8d9072fc36548f6 )
    {
        Py_DECREF( frame_62e2049811a9fa0bb8d9072fc36548f6 );
    }
    cache_frame_62e2049811a9fa0bb8d9072fc36548f6 = NULL;

    assertFrameObject( frame_62e2049811a9fa0bb8d9072fc36548f6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_include_builtins );
    Py_DECREF( par_include_builtins );
    par_include_builtins = NULL;

    CHECK_OBJECT( (PyObject *)var_tree_name );
    Py_DECREF( var_tree_name );
    var_tree_name = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_include_builtins );
    Py_DECREF( par_include_builtins );
    par_include_builtins = NULL;

    Py_XDECREF( var_tree_name );
    var_tree_name = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_7_usages$$$function_1__usages );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_8_call_signatures( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_call_signature_details = NULL;
    PyObject *var_context = NULL;
    PyObject *var_definitions = NULL;
    PyObject *outline_0_var_d = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_a708c6c0c7e14854e4d89d212cbec165;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_f3e8cc459c3bd51a956febada8d1e733_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_f3e8cc459c3bd51a956febada8d1e733_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_a708c6c0c7e14854e4d89d212cbec165 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a708c6c0c7e14854e4d89d212cbec165, codeobj_a708c6c0c7e14854e4d89d212cbec165, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a708c6c0c7e14854e4d89d212cbec165 = cache_frame_a708c6c0c7e14854e4d89d212cbec165;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a708c6c0c7e14854e4d89d212cbec165 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a708c6c0c7e14854e4d89d212cbec165 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_helpers );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 329;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_call_signature_details );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__module_node );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 329;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__pos );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 329;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_a708c6c0c7e14854e4d89d212cbec165->m_frame.f_lineno = 329;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_call_signature_details == NULL );
        var_call_signature_details = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_call_signature_details );
        tmp_compexpr_left_1 = var_call_signature_details;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = PyList_New( 0 );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__evaluator );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_create_context );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_a708c6c0c7e14854e4d89d212cbec165->m_frame.f_lineno = 334;
        tmp_args_element_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__get_module );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 334;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_call_signature_details );
        tmp_source_name_6 = var_call_signature_details;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_bracket_leaf );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 335;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_a708c6c0c7e14854e4d89d212cbec165->m_frame.f_lineno = 333;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_context == NULL );
        var_context = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_8;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_source_name_9;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_source_name_10;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_source_name_11;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_helpers );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 337;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_2;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_cache_call_signatures );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain__evaluator );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 338;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_context );
        tmp_args_element_name_6 = var_context;
        CHECK_OBJECT( var_call_signature_details );
        tmp_source_name_9 = var_call_signature_details;
        tmp_args_element_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_bracket_leaf );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_5 );

            exception_lineno = 340;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_10 = par_self;
        tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain__code_lines );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_5 );
            Py_DECREF( tmp_args_element_name_7 );

            exception_lineno = 341;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_11 = par_self;
        tmp_args_element_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain__pos );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_5 );
            Py_DECREF( tmp_args_element_name_7 );
            Py_DECREF( tmp_args_element_name_8 );

            exception_lineno = 342;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_a708c6c0c7e14854e4d89d212cbec165->m_frame.f_lineno = 337;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        Py_DECREF( tmp_args_element_name_7 );
        Py_DECREF( tmp_args_element_name_8 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_definitions == NULL );
        var_definitions = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 344;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_3;
        frame_a708c6c0c7e14854e4d89d212cbec165->m_frame.f_lineno = 344;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_speed, &PyTuple_GET_ITEM( const_tuple_str_digest_6d214cc6e077147dca5b4cd34a3570b8_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 344;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_definitions );
        tmp_iter_arg_1 = var_definitions;
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_5;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_f3e8cc459c3bd51a956febada8d1e733_2, codeobj_f3e8cc459c3bd51a956febada8d1e733, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f3e8cc459c3bd51a956febada8d1e733_2 = cache_frame_f3e8cc459c3bd51a956febada8d1e733_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f3e8cc459c3bd51a956febada8d1e733_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f3e8cc459c3bd51a956febada8d1e733_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "ooo";
                exception_lineno = 346;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_7 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_d;
            outline_0_var_d = tmp_assign_source_7;
            Py_INCREF( outline_0_var_d );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_source_name_12;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( outline_0_var_d );
        tmp_source_name_12 = outline_0_var_d;
        tmp_attribute_name_1 = const_str_plain_py__call__;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_12, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 350;
            type_description_2 = "ooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_13;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_source_name_14;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_source_name_15;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_source_name_16;
            PyObject *tmp_source_name_17;
            PyObject *tmp_args_element_name_13;
            PyObject *tmp_source_name_18;
            PyObject *tmp_args_element_name_14;
            PyObject *tmp_source_name_19;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_classes );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_classes );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 346;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }

            tmp_source_name_13 = tmp_mvar_value_4;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_CallSignature );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 346;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_14 = par_self;
            tmp_args_element_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain__evaluator );
            if ( tmp_args_element_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 346;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( outline_0_var_d );
            tmp_source_name_15 = outline_0_var_d;
            tmp_args_element_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_name );
            if ( tmp_args_element_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_10 );

                exception_lineno = 346;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_call_signature_details );
            tmp_source_name_17 = var_call_signature_details;
            tmp_source_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_bracket_leaf );
            if ( tmp_source_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_10 );
                Py_DECREF( tmp_args_element_name_11 );

                exception_lineno = 347;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            tmp_args_element_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_start_pos );
            Py_DECREF( tmp_source_name_16 );
            if ( tmp_args_element_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_10 );
                Py_DECREF( tmp_args_element_name_11 );

                exception_lineno = 347;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_call_signature_details );
            tmp_source_name_18 = var_call_signature_details;
            tmp_args_element_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_call_index );
            if ( tmp_args_element_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_10 );
                Py_DECREF( tmp_args_element_name_11 );
                Py_DECREF( tmp_args_element_name_12 );

                exception_lineno = 348;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_call_signature_details );
            tmp_source_name_19 = var_call_signature_details;
            tmp_args_element_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_keyword_name_str );
            if ( tmp_args_element_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_10 );
                Py_DECREF( tmp_args_element_name_11 );
                Py_DECREF( tmp_args_element_name_12 );
                Py_DECREF( tmp_args_element_name_13 );

                exception_lineno = 349;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            frame_f3e8cc459c3bd51a956febada8d1e733_2->m_frame.f_lineno = 346;
            {
                PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_10 );
            Py_DECREF( tmp_args_element_name_11 );
            Py_DECREF( tmp_args_element_name_12 );
            Py_DECREF( tmp_args_element_name_13 );
            Py_DECREF( tmp_args_element_name_14 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 346;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 346;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 346;
        type_description_2 = "ooo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_8_call_signatures );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f3e8cc459c3bd51a956febada8d1e733_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f3e8cc459c3bd51a956febada8d1e733_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f3e8cc459c3bd51a956febada8d1e733_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f3e8cc459c3bd51a956febada8d1e733_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f3e8cc459c3bd51a956febada8d1e733_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f3e8cc459c3bd51a956febada8d1e733_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f3e8cc459c3bd51a956febada8d1e733_2,
        type_description_2,
        outline_0_var_d,
        par_self,
        var_call_signature_details
    );


    // Release cached frame.
    if ( frame_f3e8cc459c3bd51a956febada8d1e733_2 == cache_frame_f3e8cc459c3bd51a956febada8d1e733_2 )
    {
        Py_DECREF( frame_f3e8cc459c3bd51a956febada8d1e733_2 );
    }
    cache_frame_f3e8cc459c3bd51a956febada8d1e733_2 = NULL;

    assertFrameObject( frame_f3e8cc459c3bd51a956febada8d1e733_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "oooo";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_8_call_signatures );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_d );
    outline_0_var_d = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_d );
    outline_0_var_d = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_8_call_signatures );
    return NULL;
    outline_exception_1:;
    exception_lineno = 346;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a708c6c0c7e14854e4d89d212cbec165 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a708c6c0c7e14854e4d89d212cbec165 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a708c6c0c7e14854e4d89d212cbec165 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a708c6c0c7e14854e4d89d212cbec165, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a708c6c0c7e14854e4d89d212cbec165->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a708c6c0c7e14854e4d89d212cbec165, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a708c6c0c7e14854e4d89d212cbec165,
        type_description_1,
        par_self,
        var_call_signature_details,
        var_context,
        var_definitions
    );


    // Release cached frame.
    if ( frame_a708c6c0c7e14854e4d89d212cbec165 == cache_frame_a708c6c0c7e14854e4d89d212cbec165 )
    {
        Py_DECREF( frame_a708c6c0c7e14854e4d89d212cbec165 );
    }
    cache_frame_a708c6c0c7e14854e4d89d212cbec165 = NULL;

    assertFrameObject( frame_a708c6c0c7e14854e4d89d212cbec165 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_8_call_signatures );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_call_signature_details );
    Py_DECREF( var_call_signature_details );
    var_call_signature_details = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_call_signature_details );
    var_call_signature_details = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_8_call_signatures );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_9__analysis( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_module = NULL;
    PyObject *var_node = NULL;
    PyObject *var_context = NULL;
    PyObject *var_import_names = NULL;
    PyObject *var_n = NULL;
    PyObject *var_types = NULL;
    PyObject *var_testlist = NULL;
    PyObject *var_defs = NULL;
    PyObject *var_ana = NULL;
    PyObject *outline_0_var_a = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_9a10fd60e3a46a712ec5e1e8c1f9d504;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    struct Nuitka_FrameObject *frame_d6c8d0ca4835e4b2ef5a38087eda7541_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_9a10fd60e3a46a712ec5e1e8c1f9d504 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9a10fd60e3a46a712ec5e1e8c1f9d504, codeobj_9a10fd60e3a46a712ec5e1e8c1f9d504, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9a10fd60e3a46a712ec5e1e8c1f9d504 = cache_frame_9a10fd60e3a46a712ec5e1e8c1f9d504;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_source_name_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__evaluator );
        if ( tmp_assattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 353;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_is_analysis, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 353;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_list_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__module_node );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 354;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_2 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_assattr_name_2, 0, tmp_list_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_assattr_target_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__evaluator );
        if ( tmp_assattr_target_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_2 );

            exception_lineno = 354;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_analysis_modules, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_target_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 354;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 355;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__get_module );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 355;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_module == NULL );
        var_module = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_get_executable_nodes );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_executable_nodes );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_executable_nodes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__module_node );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }
        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 357;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooo";
                exception_lineno = 357;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_node;
            var_node = tmp_assign_source_4;
            Py_INCREF( var_node );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_module );
        tmp_called_instance_2 = var_module;
        CHECK_OBJECT( var_node );
        tmp_args_element_name_2 = var_node;
        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 358;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_create_context, call_args );
        }

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "oooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = var_context;
            var_context = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_node );
        tmp_source_name_5 = var_node;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "oooooooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_right_1 = const_tuple_str_plain_funcdef_str_plain_classdef_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "oooooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_source_name_7;
            PyObject *tmp_subscript_name_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_tree_name_to_contexts );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tree_name_to_contexts );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tree_name_to_contexts" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 361;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__evaluator );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_context );
            tmp_args_element_name_4 = var_context;
            CHECK_OBJECT( var_node );
            tmp_source_name_7 = var_node;
            tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_children );
            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_3 );

                exception_lineno = 361;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }
            tmp_subscript_name_1 = const_int_pos_1;
            tmp_args_element_name_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
            Py_DECREF( tmp_subscribed_name_1 );
            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_3 );

                exception_lineno = 361;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }
            frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 361;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_source_name_8;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( var_node );
            tmp_isinstance_inst_1 = var_node;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_tree );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tree );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tree" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 362;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }

            tmp_source_name_8 = tmp_mvar_value_3;
            tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_Import );
            if ( tmp_isinstance_cls_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 362;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            Py_DECREF( tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 362;
                type_description_1 = "oooooooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_set_arg_1;
                PyObject *tmp_called_instance_3;
                CHECK_OBJECT( var_node );
                tmp_called_instance_3 = var_node;
                frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 363;
                tmp_set_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_defined_names );
                if ( tmp_set_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 363;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_3;
                }
                tmp_assign_source_6 = PySet_New( tmp_set_arg_1 );
                Py_DECREF( tmp_set_arg_1 );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 363;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = var_import_names;
                    var_import_names = tmp_assign_source_6;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_call_result_2;
                int tmp_truth_name_1;
                CHECK_OBJECT( var_node );
                tmp_called_instance_4 = var_node;
                frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 364;
                tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_is_nested );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 364;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_3;
                }
                tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_2 );
                if ( tmp_truth_name_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_call_result_2 );

                    exception_lineno = 364;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_3;
                }
                tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_2 );
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_7;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_set_arg_2;
                    CHECK_OBJECT( var_import_names );
                    tmp_left_name_1 = var_import_names;
                    {
                        PyObject *tmp_assign_source_8;
                        PyObject *tmp_iter_arg_2;
                        PyObject *tmp_called_instance_5;
                        CHECK_OBJECT( var_node );
                        tmp_called_instance_5 = var_node;
                        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 365;
                        tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_get_paths );
                        if ( tmp_iter_arg_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 365;
                            type_description_1 = "oooooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_2 );
                        Py_DECREF( tmp_iter_arg_2 );
                        if ( tmp_assign_source_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 365;
                            type_description_1 = "oooooooooo";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = tmp_genexpr_1__$0;
                            tmp_genexpr_1__$0 = tmp_assign_source_8;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    tmp_set_arg_2 = jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_maker();

                    ((struct Nuitka_GeneratorObject *)tmp_set_arg_2)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


                    goto try_return_handler_4;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
                    return NULL;
                    // Return handler code:
                    try_return_handler_4:;
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                    Py_DECREF( tmp_genexpr_1__$0 );
                    tmp_genexpr_1__$0 = NULL;

                    goto outline_result_1;
                    // End of try:
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                    Py_DECREF( tmp_genexpr_1__$0 );
                    tmp_genexpr_1__$0 = NULL;

                    // Return statement must have exited already.
                    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
                    return NULL;
                    outline_result_1:;
                    tmp_right_name_1 = PySet_New( tmp_set_arg_2 );
                    Py_DECREF( tmp_set_arg_2 );
                    if ( tmp_right_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 365;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceOr, &tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 365;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_assign_source_7 = tmp_left_name_1;
                    var_import_names = tmp_assign_source_7;

                }
                branch_no_3:;
            }
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_iter_arg_3;
                CHECK_OBJECT( var_import_names );
                tmp_iter_arg_3 = var_import_names;
                tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_3 );
                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 366;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = tmp_for_loop_2__for_iterator;
                    tmp_for_loop_2__for_iterator = tmp_assign_source_9;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_10;
                CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                tmp_next_source_2 = tmp_for_loop_2__for_iterator;
                tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_10 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooooooooo";
                        exception_lineno = 366;
                        goto try_except_handler_5;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_2__iter_value;
                    tmp_for_loop_2__iter_value = tmp_assign_source_10;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_11;
                CHECK_OBJECT( tmp_for_loop_2__iter_value );
                tmp_assign_source_11 = tmp_for_loop_2__iter_value;
                {
                    PyObject *old = var_n;
                    var_n = tmp_assign_source_11;
                    Py_INCREF( var_n );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_instance_6;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_6;
                PyObject *tmp_args_element_name_7;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_imports );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_imports );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "imports" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 367;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_5;
                }

                tmp_called_instance_6 = tmp_mvar_value_4;
                CHECK_OBJECT( var_context );
                tmp_args_element_name_6 = var_context;
                CHECK_OBJECT( var_n );
                tmp_args_element_name_7 = var_n;
                frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 367;
                {
                    PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                    tmp_call_result_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain_infer_import, call_args );
                }

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 367;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 366;
                type_description_1 = "oooooooooo";
                goto try_except_handler_5;
            }
            goto loop_start_2;
            loop_end_2:;
            goto try_end_1;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto try_except_handler_3;
            // End of try:
            try_end_1:;
            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                PyObject *tmp_source_name_9;
                CHECK_OBJECT( var_node );
                tmp_source_name_9 = var_node;
                tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_type );
                if ( tmp_compexpr_left_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 368;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_3;
                }
                tmp_compexpr_right_2 = const_str_plain_expr_stmt;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                Py_DECREF( tmp_compexpr_left_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 368;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_3;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_12;
                    PyObject *tmp_called_instance_7;
                    PyObject *tmp_args_element_name_8;
                    CHECK_OBJECT( var_context );
                    tmp_called_instance_7 = var_context;
                    CHECK_OBJECT( var_node );
                    tmp_args_element_name_8 = var_node;
                    frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 369;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_8 };
                        tmp_assign_source_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_eval_node, call_args );
                    }

                    if ( tmp_assign_source_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 369;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    {
                        PyObject *old = var_types;
                        var_types = tmp_assign_source_12;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_iter_arg_4;
                    PyObject *tmp_subscribed_name_2;
                    PyObject *tmp_source_name_10;
                    PyObject *tmp_subscript_name_2;
                    CHECK_OBJECT( var_node );
                    tmp_source_name_10 = var_node;
                    tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_children );
                    if ( tmp_subscribed_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 370;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_subscript_name_2 = const_slice_none_int_neg_1_int_pos_2;
                    tmp_iter_arg_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                    Py_DECREF( tmp_subscribed_name_2 );
                    if ( tmp_iter_arg_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 370;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_4 );
                    Py_DECREF( tmp_iter_arg_4 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 370;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    {
                        PyObject *old = tmp_for_loop_3__for_iterator;
                        tmp_for_loop_3__for_iterator = tmp_assign_source_13;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                loop_start_3:;
                {
                    PyObject *tmp_next_source_3;
                    PyObject *tmp_assign_source_14;
                    CHECK_OBJECT( tmp_for_loop_3__for_iterator );
                    tmp_next_source_3 = tmp_for_loop_3__for_iterator;
                    tmp_assign_source_14 = ITERATOR_NEXT( tmp_next_source_3 );
                    if ( tmp_assign_source_14 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_3;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "oooooooooo";
                            exception_lineno = 370;
                            goto try_except_handler_6;
                        }
                    }

                    {
                        PyObject *old = tmp_for_loop_3__iter_value;
                        tmp_for_loop_3__iter_value = tmp_assign_source_14;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_15;
                    CHECK_OBJECT( tmp_for_loop_3__iter_value );
                    tmp_assign_source_15 = tmp_for_loop_3__iter_value;
                    {
                        PyObject *old = var_testlist;
                        var_testlist = tmp_assign_source_15;
                        Py_INCREF( var_testlist );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_5;
                    PyObject *tmp_call_result_4;
                    PyObject *tmp_args_element_name_9;
                    PyObject *tmp_args_element_name_10;
                    PyObject *tmp_args_element_name_11;
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_unpack_tuple_to_dict );

                    if (unlikely( tmp_mvar_value_5 == NULL ))
                    {
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack_tuple_to_dict );
                    }

                    if ( tmp_mvar_value_5 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unpack_tuple_to_dict" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 372;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_6;
                    }

                    tmp_called_name_3 = tmp_mvar_value_5;
                    CHECK_OBJECT( var_context );
                    tmp_args_element_name_9 = var_context;
                    CHECK_OBJECT( var_types );
                    tmp_args_element_name_10 = var_types;
                    CHECK_OBJECT( var_testlist );
                    tmp_args_element_name_11 = var_testlist;
                    frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 372;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11 };
                        tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
                    }

                    if ( tmp_call_result_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 372;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_6;
                    }
                    Py_DECREF( tmp_call_result_4 );
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 370;
                    type_description_1 = "oooooooooo";
                    goto try_except_handler_6;
                }
                goto loop_start_3;
                loop_end_3:;
                goto try_end_2;
                // Exception handler code:
                try_except_handler_6:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_for_loop_3__iter_value );
                tmp_for_loop_3__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
                Py_DECREF( tmp_for_loop_3__for_iterator );
                tmp_for_loop_3__for_iterator = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto try_except_handler_3;
                // End of try:
                try_end_2:;
                Py_XDECREF( tmp_for_loop_3__iter_value );
                tmp_for_loop_3__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
                Py_DECREF( tmp_for_loop_3__for_iterator );
                tmp_for_loop_3__for_iterator = NULL;

                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_3;
                    PyObject *tmp_compexpr_right_3;
                    PyObject *tmp_source_name_11;
                    CHECK_OBJECT( var_node );
                    tmp_source_name_11 = var_node;
                    tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_type );
                    if ( tmp_compexpr_left_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 374;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_compexpr_right_3 = const_str_plain_name;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                    Py_DECREF( tmp_compexpr_left_3 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 374;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_16;
                        PyObject *tmp_called_instance_8;
                        PyObject *tmp_source_name_12;
                        PyObject *tmp_args_element_name_12;
                        PyObject *tmp_args_element_name_13;
                        CHECK_OBJECT( par_self );
                        tmp_source_name_12 = par_self;
                        tmp_called_instance_8 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain__evaluator );
                        if ( tmp_called_instance_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 375;
                            type_description_1 = "oooooooooo";
                            goto try_except_handler_3;
                        }
                        CHECK_OBJECT( var_context );
                        tmp_args_element_name_12 = var_context;
                        CHECK_OBJECT( var_node );
                        tmp_args_element_name_13 = var_node;
                        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 375;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13 };
                            tmp_assign_source_16 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_8, const_str_plain_goto_definitions, call_args );
                        }

                        Py_DECREF( tmp_called_instance_8 );
                        if ( tmp_assign_source_16 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 375;
                            type_description_1 = "oooooooooo";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = var_defs;
                            var_defs = tmp_assign_source_16;
                            Py_XDECREF( old );
                        }

                    }
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        PyObject *tmp_assign_source_17;
                        PyObject *tmp_called_name_4;
                        PyObject *tmp_mvar_value_6;
                        PyObject *tmp_args_element_name_14;
                        PyObject *tmp_args_element_name_15;
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_evaluate_call_of_leaf );

                        if (unlikely( tmp_mvar_value_6 == NULL ))
                        {
                            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_evaluate_call_of_leaf );
                        }

                        if ( tmp_mvar_value_6 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "evaluate_call_of_leaf" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 377;
                            type_description_1 = "oooooooooo";
                            goto try_except_handler_3;
                        }

                        tmp_called_name_4 = tmp_mvar_value_6;
                        CHECK_OBJECT( var_context );
                        tmp_args_element_name_14 = var_context;
                        CHECK_OBJECT( var_node );
                        tmp_args_element_name_15 = var_node;
                        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 377;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_14, tmp_args_element_name_15 };
                            tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
                        }

                        if ( tmp_assign_source_17 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 377;
                            type_description_1 = "oooooooooo";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = var_defs;
                            var_defs = tmp_assign_source_17;
                            Py_XDECREF( old );
                        }

                    }
                    branch_end_5:;
                }
                {
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_mvar_value_7;
                    PyObject *tmp_call_result_5;
                    PyObject *tmp_args_element_name_16;
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_try_iter_content );

                    if (unlikely( tmp_mvar_value_7 == NULL ))
                    {
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_try_iter_content );
                    }

                    if ( tmp_mvar_value_7 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "try_iter_content" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 378;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }

                    tmp_called_name_5 = tmp_mvar_value_7;
                    CHECK_OBJECT( var_defs );
                    tmp_args_element_name_16 = var_defs;
                    frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 378;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_16 };
                        tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                    }

                    if ( tmp_call_result_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 378;
                        type_description_1 = "oooooooooo";
                        goto try_except_handler_3;
                    }
                    Py_DECREF( tmp_call_result_5 );
                }
                branch_end_4:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_instance_9;
        PyObject *tmp_source_name_13;
        PyObject *tmp_call_result_6;
        CHECK_OBJECT( par_self );
        tmp_source_name_13 = par_self;
        tmp_called_instance_9 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain__evaluator );
        if ( tmp_called_instance_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 379;
            type_description_1 = "oooooooooo";
            goto try_except_handler_3;
        }
        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 379;
        tmp_call_result_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_reset_recursion_limitations );
        Py_DECREF( tmp_called_instance_9 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 379;
            type_description_1 = "oooooooooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 357;
        type_description_1 = "oooooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_18;
        // Tried code:
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_iter_arg_5;
            PyObject *tmp_source_name_14;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( par_self );
            tmp_source_name_15 = par_self;
            tmp_source_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain__evaluator );
            if ( tmp_source_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "oooooooooo";
                goto try_except_handler_7;
            }
            tmp_iter_arg_5 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_analysis );
            Py_DECREF( tmp_source_name_14 );
            if ( tmp_iter_arg_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "oooooooooo";
                goto try_except_handler_7;
            }
            tmp_assign_source_19 = MAKE_ITERATOR( tmp_iter_arg_5 );
            Py_DECREF( tmp_iter_arg_5 );
            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_1 = "oooooooooo";
                goto try_except_handler_7;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_19;
        }
        {
            PyObject *tmp_assign_source_20;
            tmp_assign_source_20 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_20;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_d6c8d0ca4835e4b2ef5a38087eda7541_2, codeobj_d6c8d0ca4835e4b2ef5a38087eda7541, module_jedi$api, sizeof(void *)+sizeof(void *) );
        frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 = cache_frame_d6c8d0ca4835e4b2ef5a38087eda7541_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_4:;
        {
            PyObject *tmp_next_source_4;
            PyObject *tmp_assign_source_21;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_4 = tmp_listcomp_1__$0;
            tmp_assign_source_21 = ITERATOR_NEXT( tmp_next_source_4 );
            if ( tmp_assign_source_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_4;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 381;
                    goto try_except_handler_8;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_21;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_22;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_22 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_a;
                outline_0_var_a = tmp_assign_source_22;
                Py_INCREF( outline_0_var_a );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_source_name_16;
            PyObject *tmp_source_name_17;
            CHECK_OBJECT( par_self );
            tmp_source_name_16 = par_self;
            tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_path );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_2 = "oo";
                goto try_except_handler_8;
            }
            CHECK_OBJECT( outline_0_var_a );
            tmp_source_name_17 = outline_0_var_a;
            tmp_compexpr_right_4 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_path );
            if ( tmp_compexpr_right_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_4 );

                exception_lineno = 381;
                type_description_2 = "oo";
                goto try_except_handler_8;
            }
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            Py_DECREF( tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_2 = "oo";
                goto try_except_handler_8;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_a );
                tmp_append_value_1 = outline_0_var_a;
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 381;
                    type_description_2 = "oo";
                    goto try_except_handler_8;
                }
            }
            branch_no_6:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 381;
            type_description_2 = "oo";
            goto try_except_handler_8;
        }
        goto loop_start_4;
        loop_end_4:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_18 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_18 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
        return NULL;
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_7;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_d6c8d0ca4835e4b2ef5a38087eda7541_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_d6c8d0ca4835e4b2ef5a38087eda7541_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_d6c8d0ca4835e4b2ef5a38087eda7541_2,
            type_description_2,
            outline_0_var_a,
            par_self
        );


        // Release cached frame.
        if ( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 == cache_frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 )
        {
            Py_DECREF( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 );
        }
        cache_frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 = NULL;

        assertFrameObject( frame_d6c8d0ca4835e4b2ef5a38087eda7541_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oooooooooo";
        goto try_except_handler_7;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
        return NULL;
        // Return handler code:
        try_return_handler_7:;
        Py_XDECREF( outline_0_var_a );
        outline_0_var_a = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_a );
        outline_0_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
        return NULL;
        outline_exception_1:;
        exception_lineno = 381;
        goto try_except_handler_2;
        outline_result_2:;
        assert( var_ana == NULL );
        var_ana = tmp_assign_source_18;
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_set_arg_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_called_name_6 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_6 != NULL );
        CHECK_OBJECT( var_ana );
        tmp_set_arg_3 = var_ana;
        tmp_tuple_element_1 = PySet_New( tmp_set_arg_3 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 382;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_key;
        tmp_dict_value_1 = MAKE_FUNCTION_jedi$api$$$function_9__analysis$$$function_1_lambda(  );



        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = 382;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 382;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }
        goto try_return_handler_2;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_source_name_18;
        tmp_assattr_name_3 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_source_name_18 = par_self;
        tmp_assattr_target_3 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain__evaluator );
        if ( tmp_assattr_target_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_is_analysis, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_target_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
    }
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_6 == NULL )
    {
        exception_keeper_tb_6 = MAKE_TRACEBACK( frame_9a10fd60e3a46a712ec5e1e8c1f9d504, exception_keeper_lineno_6 );
    }
    else if ( exception_keeper_lineno_6 != 0 )
    {
        exception_keeper_tb_6 = ADD_TRACEBACK( exception_keeper_tb_6, frame_9a10fd60e3a46a712ec5e1e8c1f9d504, exception_keeper_lineno_6 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    PyException_SetTraceback( exception_keeper_value_6, (PyObject *)exception_keeper_tb_6 );
    PUBLISH_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    // Tried code:
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_source_name_19;
        tmp_assattr_name_4 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_source_name_19 = par_self;
        tmp_assattr_target_4 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain__evaluator );
        if ( tmp_assattr_target_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;
            type_description_1 = "oooooooooo";
            goto try_except_handler_9;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_is_analysis, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_target_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;
            type_description_1 = "oooooooooo";
            goto try_except_handler_9;
        }
    }
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 356;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame) frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "oooooooooo";
    goto try_except_handler_9;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
    return NULL;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9a10fd60e3a46a712ec5e1e8c1f9d504, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9a10fd60e3a46a712ec5e1e8c1f9d504->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9a10fd60e3a46a712ec5e1e8c1f9d504, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9a10fd60e3a46a712ec5e1e8c1f9d504,
        type_description_1,
        par_self,
        var_module,
        var_node,
        var_context,
        var_import_names,
        var_n,
        var_types,
        var_testlist,
        var_defs,
        var_ana
    );


    // Release cached frame.
    if ( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 == cache_frame_9a10fd60e3a46a712ec5e1e8c1f9d504 )
    {
        Py_DECREF( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 );
    }
    cache_frame_9a10fd60e3a46a712ec5e1e8c1f9d504 = NULL;

    assertFrameObject( frame_9a10fd60e3a46a712ec5e1e8c1f9d504 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_module );
    Py_DECREF( var_module );
    var_module = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_import_names );
    var_import_names = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_types );
    var_types = NULL;

    Py_XDECREF( var_testlist );
    var_testlist = NULL;

    Py_XDECREF( var_defs );
    var_defs = NULL;

    CHECK_OBJECT( (PyObject *)var_ana );
    Py_DECREF( var_ana );
    var_ana = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_module );
    var_module = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_context );
    var_context = NULL;

    Py_XDECREF( var_import_names );
    var_import_names = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_types );
    var_types = NULL;

    Py_XDECREF( var_testlist );
    var_testlist = NULL;

    Py_XDECREF( var_defs );
    var_defs = NULL;

    Py_XDECREF( var_ana );
    var_ana = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_locals {
    PyObject *var_path;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_locals *generator_heap = (struct jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_path = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_e0f64dc30825516bca3f8166b32fd68e, module_jedi$api, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 365;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_path;
            generator_heap->var_path = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_path );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_path );
        tmp_subscribed_name_1 = generator_heap->var_path;
        tmp_subscript_name_1 = const_int_neg_1;
        tmp_expression_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, -1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 365;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 365;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 365;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_path
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_path );
    generator_heap->var_path = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_path );
    generator_heap->var_path = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_context,
        module_jedi$api,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_2314c5bd8ebf4446ae1c4cc88a5ccb24,
#endif
        codeobj_e0f64dc30825516bca3f8166b32fd68e,
        1,
        sizeof(struct jedi$api$$$function_9__analysis$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_jedi$api$$$function_9__analysis$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_774df3a9437f71117153addab97147f7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_774df3a9437f71117153addab97147f7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_774df3a9437f71117153addab97147f7, codeobj_774df3a9437f71117153addab97147f7, module_jedi$api, sizeof(void *) );
    frame_774df3a9437f71117153addab97147f7 = cache_frame_774df3a9437f71117153addab97147f7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_774df3a9437f71117153addab97147f7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_774df3a9437f71117153addab97147f7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_x );
        tmp_source_name_1 = par_x;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_line );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 382;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_774df3a9437f71117153addab97147f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_774df3a9437f71117153addab97147f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_774df3a9437f71117153addab97147f7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_774df3a9437f71117153addab97147f7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_774df3a9437f71117153addab97147f7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_774df3a9437f71117153addab97147f7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_774df3a9437f71117153addab97147f7,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_774df3a9437f71117153addab97147f7 == cache_frame_774df3a9437f71117153addab97147f7 )
    {
        Py_DECREF( frame_774df3a9437f71117153addab97147f7 );
    }
    cache_frame_774df3a9437f71117153addab97147f7 = NULL;

    assertFrameObject( frame_774df3a9437f71117153addab97147f7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_9__analysis$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_10___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_source = python_pars[ 1 ];
    PyObject *par_namespaces = python_pars[ 2 ];
    PyObject *par_kwds = python_pars[ 3 ];
    PyObject *var_environment = NULL;
    PyObject *outline_0_var_n = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_2a4a505dbc600e5d254792d9fe4b650a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_9d973ba712c6e586db7c45d50e883d8a_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_9d973ba712c6e586db7c45d50e883d8a_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_2a4a505dbc600e5d254792d9fe4b650a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2a4a505dbc600e5d254792d9fe4b650a, codeobj_2a4a505dbc600e5d254792d9fe4b650a, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2a4a505dbc600e5d254792d9fe4b650a = cache_frame_2a4a505dbc600e5d254792d9fe4b650a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2a4a505dbc600e5d254792d9fe4b650a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2a4a505dbc600e5d254792d9fe4b650a ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_namespaces );
            tmp_iter_arg_1 = par_namespaces;
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 418;
                type_description_1 = "oooooN";
                goto try_except_handler_3;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_9d973ba712c6e586db7c45d50e883d8a_2, codeobj_9d973ba712c6e586db7c45d50e883d8a, module_jedi$api, sizeof(void *) );
        frame_9d973ba712c6e586db7c45d50e883d8a_2 = cache_frame_9d973ba712c6e586db7c45d50e883d8a_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_9d973ba712c6e586db7c45d50e883d8a_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_9d973ba712c6e586db7c45d50e883d8a_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 418;
                    goto try_except_handler_4;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_n;
                outline_0_var_n = tmp_assign_source_5;
                Py_INCREF( outline_0_var_n );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_dict_seq_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_n );
            tmp_dict_seq_1 = outline_0_var_n;
            tmp_append_value_1 = TO_DICT( tmp_dict_seq_1, NULL );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 418;
                type_description_2 = "o";
                goto try_except_handler_4;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 418;
                type_description_2 = "o";
                goto try_except_handler_4;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 418;
            type_description_2 = "o";
            goto try_except_handler_4;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_1 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_10___init__ );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_9d973ba712c6e586db7c45d50e883d8a_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_9d973ba712c6e586db7c45d50e883d8a_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_3;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_9d973ba712c6e586db7c45d50e883d8a_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_9d973ba712c6e586db7c45d50e883d8a_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_9d973ba712c6e586db7c45d50e883d8a_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_9d973ba712c6e586db7c45d50e883d8a_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_9d973ba712c6e586db7c45d50e883d8a_2,
            type_description_2,
            outline_0_var_n
        );


        // Release cached frame.
        if ( frame_9d973ba712c6e586db7c45d50e883d8a_2 == cache_frame_9d973ba712c6e586db7c45d50e883d8a_2 )
        {
            Py_DECREF( frame_9d973ba712c6e586db7c45d50e883d8a_2 );
        }
        cache_frame_9d973ba712c6e586db7c45d50e883d8a_2 = NULL;

        assertFrameObject( frame_9d973ba712c6e586db7c45d50e883d8a_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oooooN";
        goto try_except_handler_3;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_10___init__ );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        Py_XDECREF( outline_0_var_n );
        outline_0_var_n = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_n );
        outline_0_var_n = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_10___init__ );
        return NULL;
        outline_exception_1:;
        exception_lineno = 418;
        goto try_except_handler_2;
        outline_result_1:;
        {
            PyObject *old = par_namespaces;
            assert( old != NULL );
            par_namespaces = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_2a4a505dbc600e5d254792d9fe4b650a, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_2a4a505dbc600e5d254792d9fe4b650a, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_Exception;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 419;
            type_description_1 = "oooooN";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_3fc9aa4c9fda68d670234c9c9746a3ae;
            frame_2a4a505dbc600e5d254792d9fe4b650a->m_frame.f_lineno = 420;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 420;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooN";
            goto try_except_handler_5;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 417;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_2a4a505dbc600e5d254792d9fe4b650a->m_frame) frame_2a4a505dbc600e5d254792d9fe4b650a->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooN";
        goto try_except_handler_5;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_10___init__ );
    return NULL;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_kwds );
        tmp_called_instance_1 = par_kwds;
        frame_2a4a505dbc600e5d254792d9fe4b650a->m_frame.f_lineno = 422;
        tmp_assign_source_6 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_environment_none_tuple, 0 ) );

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 422;
            type_description_1 = "oooooN";
            goto frame_exception_exit_1;
        }
        assert( var_environment == NULL );
        var_environment = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_environment );
        tmp_compexpr_left_2 = var_environment;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_InterpreterEnvironment );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InterpreterEnvironment );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InterpreterEnvironment" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 424;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            frame_2a4a505dbc600e5d254792d9fe4b650a->m_frame.f_lineno = 424;
            tmp_assign_source_7 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 424;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_environment;
                assert( old != NULL );
                var_environment = tmp_assign_source_7;
                Py_DECREF( old );
            }

        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_mvar_value_2;
            CHECK_OBJECT( var_environment );
            tmp_isinstance_inst_1 = var_environment;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_InterpreterEnvironment );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InterpreterEnvironment );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InterpreterEnvironment" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 426;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_1 = tmp_mvar_value_2;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 426;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 426;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                tmp_make_exception_arg_2 = const_str_digest_48757229a8765f68a570cd77bc9a6e90;
                frame_2a4a505dbc600e5d254792d9fe4b650a->m_frame.f_lineno = 427;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                }

                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 427;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }
            branch_no_3:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dircall_arg4_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Interpreter );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Interpreter );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Interpreter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 429;
            type_description_1 = "oooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_3;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 429;
            type_description_1 = "oooooN";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 429;
            type_description_1 = "oooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_source );
        tmp_tuple_element_1 = par_source;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_environment;
        CHECK_OBJECT( var_environment );
        tmp_dict_value_1 = var_environment;
        tmp_dircall_arg3_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        CHECK_OBJECT( par_kwds );
        tmp_dircall_arg4_1 = par_kwds;
        Py_INCREF( tmp_dircall_arg4_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1, tmp_dircall_arg4_1};
            tmp_call_result_1 = impl___internal__$$$function_11_complex_call_helper_pos_keywords_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 429;
            type_description_1 = "oooooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_namespaces );
        tmp_assattr_name_1 = par_namespaces;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_namespaces, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 430;
            type_description_1 = "oooooN";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a4a505dbc600e5d254792d9fe4b650a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a4a505dbc600e5d254792d9fe4b650a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2a4a505dbc600e5d254792d9fe4b650a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2a4a505dbc600e5d254792d9fe4b650a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2a4a505dbc600e5d254792d9fe4b650a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2a4a505dbc600e5d254792d9fe4b650a,
        type_description_1,
        par_self,
        par_source,
        par_namespaces,
        par_kwds,
        var_environment,
        NULL
    );


    // Release cached frame.
    if ( frame_2a4a505dbc600e5d254792d9fe4b650a == cache_frame_2a4a505dbc600e5d254792d9fe4b650a )
    {
        Py_DECREF( frame_2a4a505dbc600e5d254792d9fe4b650a );
    }
    cache_frame_2a4a505dbc600e5d254792d9fe4b650a = NULL;

    assertFrameObject( frame_2a4a505dbc600e5d254792d9fe4b650a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_10___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_namespaces );
    Py_DECREF( par_namespaces );
    par_namespaces = NULL;

    CHECK_OBJECT( (PyObject *)par_kwds );
    Py_DECREF( par_kwds );
    par_kwds = NULL;

    CHECK_OBJECT( (PyObject *)var_environment );
    Py_DECREF( var_environment );
    var_environment = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_namespaces );
    Py_DECREF( par_namespaces );
    par_namespaces = NULL;

    CHECK_OBJECT( (PyObject *)par_kwds );
    Py_DECREF( par_kwds );
    par_kwds = NULL;

    Py_XDECREF( var_environment );
    var_environment = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_10___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_11__get_module( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b707086c23679791b0fdd79bb19830fe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_b707086c23679791b0fdd79bb19830fe = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b707086c23679791b0fdd79bb19830fe, codeobj_b707086c23679791b0fdd79bb19830fe, module_jedi$api, sizeof(void *) );
    frame_b707086c23679791b0fdd79bb19830fe = cache_frame_b707086c23679791b0fdd79bb19830fe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b707086c23679791b0fdd79bb19830fe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b707086c23679791b0fdd79bb19830fe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_6;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_interpreter );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_interpreter );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "interpreter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 433;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_MixedModuleContext );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 433;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__evaluator );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 434;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__module_node );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 435;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_namespaces );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 436;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_path;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_path );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 437;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_code_lines;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__code_lines );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 438;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_b707086c23679791b0fdd79bb19830fe->m_frame.f_lineno = 433;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 433;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b707086c23679791b0fdd79bb19830fe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b707086c23679791b0fdd79bb19830fe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b707086c23679791b0fdd79bb19830fe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b707086c23679791b0fdd79bb19830fe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b707086c23679791b0fdd79bb19830fe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b707086c23679791b0fdd79bb19830fe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b707086c23679791b0fdd79bb19830fe,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_b707086c23679791b0fdd79bb19830fe == cache_frame_b707086c23679791b0fdd79bb19830fe )
    {
        Py_DECREF( frame_b707086c23679791b0fdd79bb19830fe );
    }
    cache_frame_b707086c23679791b0fdd79bb19830fe = NULL;

    assertFrameObject( frame_b707086c23679791b0fdd79bb19830fe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_11__get_module );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_11__get_module );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_12_names( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    PyObject *par_path = python_pars[ 1 ];
    PyObject *par_encoding = python_pars[ 2 ];
    PyObject *par_all_scopes = python_pars[ 3 ];
    struct Nuitka_CellObject *par_definitions = PyCell_NEW1( python_pars[ 4 ] );
    struct Nuitka_CellObject *par_references = PyCell_NEW1( python_pars[ 5 ] );
    PyObject *par_environment = python_pars[ 6 ];
    PyObject *var_def_ref_filter = NULL;
    PyObject *var_create_name = NULL;
    PyObject *var_script = NULL;
    struct Nuitka_CellObject *var_module_context = PyCell_EMPTY();
    PyObject *var_defs = NULL;
    PyObject *outline_0_var_name = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_d0250c08e540e8597497c3240e385814;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_cc79bc898b5151f400caf23d67590344_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_cc79bc898b5151f400caf23d67590344_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d0250c08e540e8597497c3240e385814 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_1_def_ref_filter(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_definitions;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] = par_references;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] );


        assert( var_def_ref_filter == NULL );
        var_def_ref_filter = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_2_create_name(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = var_module_context;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );


        assert( var_create_name == NULL );
        var_create_name = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d0250c08e540e8597497c3240e385814, codeobj_d0250c08e540e8597497c3240e385814, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d0250c08e540e8597497c3240e385814 = cache_frame_d0250c08e540e8597497c3240e385814;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d0250c08e540e8597497c3240e385814 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d0250c08e540e8597497c3240e385814 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Script );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Script );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Script" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 474;
            type_description_1 = "ooooccooooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_source );
        tmp_tuple_element_1 = par_source;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_line;
        tmp_dict_value_1 = const_int_pos_1;
        tmp_kw_name_1 = _PyDict_NewPresized( 5 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_column;
        tmp_dict_value_2 = const_int_0;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_path;
        CHECK_OBJECT( par_path );
        tmp_dict_value_3 = par_path;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_encoding;
        CHECK_OBJECT( par_encoding );
        tmp_dict_value_4 = par_encoding;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_environment;
        CHECK_OBJECT( par_environment );
        tmp_dict_value_5 = par_environment;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        frame_d0250c08e540e8597497c3240e385814->m_frame.f_lineno = 474;
        tmp_assign_source_3 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 474;
            type_description_1 = "ooooccooooco";
            goto frame_exception_exit_1;
        }
        assert( var_script == NULL );
        var_script = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_script );
        tmp_called_instance_1 = var_script;
        frame_d0250c08e540e8597497c3240e385814->m_frame.f_lineno = 475;
        tmp_assign_source_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__get_module );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 475;
            type_description_1 = "ooooccooooco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_module_context ) == NULL );
        PyCell_SET( var_module_context, tmp_assign_source_4 );

    }
    {
        PyObject *tmp_assign_source_5;
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_get_module_names );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_module_names );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_module_names" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 480;
                type_description_1 = "ooooccooooco";
                goto try_except_handler_2;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( var_script );
            tmp_source_name_1 = var_script;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__module_node );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 480;
                type_description_1 = "ooooccooooco";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( par_all_scopes );
            tmp_args_element_name_2 = par_all_scopes;
            frame_d0250c08e540e8597497c3240e385814->m_frame.f_lineno = 480;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 480;
                type_description_1 = "ooooccooooco";
                goto try_except_handler_2;
            }
            tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 477;
                type_description_1 = "ooooccooooco";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_6;
        }
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_cc79bc898b5151f400caf23d67590344_2, codeobj_cc79bc898b5151f400caf23d67590344, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *) );
        frame_cc79bc898b5151f400caf23d67590344_2 = cache_frame_cc79bc898b5151f400caf23d67590344_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_cc79bc898b5151f400caf23d67590344_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_cc79bc898b5151f400caf23d67590344_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "ooo";
                    exception_lineno = 477;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_9 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_name;
                outline_0_var_name = tmp_assign_source_9;
                Py_INCREF( outline_0_var_name );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_classes );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_classes );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 477;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }

            tmp_source_name_2 = tmp_mvar_value_3;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_Definition );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 477;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_script );
            tmp_source_name_3 = var_script;
            tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__evaluator );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 478;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_create_name );
            tmp_called_name_4 = var_create_name;
            CHECK_OBJECT( outline_0_var_name );
            tmp_args_element_name_5 = outline_0_var_name;
            frame_cc79bc898b5151f400caf23d67590344_2->m_frame.f_lineno = 479;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_args_element_name_3 );

                exception_lineno = 479;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            frame_cc79bc898b5151f400caf23d67590344_2->m_frame.f_lineno = 477;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 477;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 477;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 477;
            type_description_2 = "ooo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_5 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_5 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_cc79bc898b5151f400caf23d67590344_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_cc79bc898b5151f400caf23d67590344_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_cc79bc898b5151f400caf23d67590344_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_cc79bc898b5151f400caf23d67590344_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_cc79bc898b5151f400caf23d67590344_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_cc79bc898b5151f400caf23d67590344_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_cc79bc898b5151f400caf23d67590344_2,
            type_description_2,
            outline_0_var_name,
            var_script,
            var_create_name
        );


        // Release cached frame.
        if ( frame_cc79bc898b5151f400caf23d67590344_2 == cache_frame_cc79bc898b5151f400caf23d67590344_2 )
        {
            Py_DECREF( frame_cc79bc898b5151f400caf23d67590344_2 );
        }
        cache_frame_cc79bc898b5151f400caf23d67590344_2 = NULL;

        assertFrameObject( frame_cc79bc898b5151f400caf23d67590344_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooooccooooco";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_name );
        outline_0_var_name = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_name );
        outline_0_var_name = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names );
        return NULL;
        outline_exception_1:;
        exception_lineno = 477;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_defs == NULL );
        var_defs = tmp_assign_source_5;
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_called_name_6;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        tmp_called_name_5 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_5 != NULL );
        tmp_called_name_6 = (PyObject *)&PyFilter_Type;
        CHECK_OBJECT( var_def_ref_filter );
        tmp_args_element_name_6 = var_def_ref_filter;
        CHECK_OBJECT( var_defs );
        tmp_args_element_name_7 = var_defs;
        frame_d0250c08e540e8597497c3240e385814->m_frame.f_lineno = 482;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_tuple_element_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 482;
            type_description_1 = "ooooccooooco";
            goto frame_exception_exit_1;
        }
        tmp_args_name_2 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
        tmp_dict_key_6 = const_str_plain_key;
        tmp_dict_value_6 = MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_3_lambda(  );



        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        frame_d0250c08e540e8597497c3240e385814->m_frame.f_lineno = 482;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 482;
            type_description_1 = "ooooccooooco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0250c08e540e8597497c3240e385814 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0250c08e540e8597497c3240e385814 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d0250c08e540e8597497c3240e385814 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d0250c08e540e8597497c3240e385814, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d0250c08e540e8597497c3240e385814->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d0250c08e540e8597497c3240e385814, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d0250c08e540e8597497c3240e385814,
        type_description_1,
        par_source,
        par_path,
        par_encoding,
        par_all_scopes,
        par_definitions,
        par_references,
        par_environment,
        var_def_ref_filter,
        var_create_name,
        var_script,
        var_module_context,
        var_defs
    );


    // Release cached frame.
    if ( frame_d0250c08e540e8597497c3240e385814 == cache_frame_d0250c08e540e8597497c3240e385814 )
    {
        Py_DECREF( frame_d0250c08e540e8597497c3240e385814 );
    }
    cache_frame_d0250c08e540e8597497c3240e385814 = NULL;

    assertFrameObject( frame_d0250c08e540e8597497c3240e385814 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_all_scopes );
    Py_DECREF( par_all_scopes );
    par_all_scopes = NULL;

    CHECK_OBJECT( (PyObject *)par_definitions );
    Py_DECREF( par_definitions );
    par_definitions = NULL;

    CHECK_OBJECT( (PyObject *)par_references );
    Py_DECREF( par_references );
    par_references = NULL;

    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)var_def_ref_filter );
    Py_DECREF( var_def_ref_filter );
    var_def_ref_filter = NULL;

    CHECK_OBJECT( (PyObject *)var_create_name );
    Py_DECREF( var_create_name );
    var_create_name = NULL;

    CHECK_OBJECT( (PyObject *)var_script );
    Py_DECREF( var_script );
    var_script = NULL;

    CHECK_OBJECT( (PyObject *)var_module_context );
    Py_DECREF( var_module_context );
    var_module_context = NULL;

    CHECK_OBJECT( (PyObject *)var_defs );
    Py_DECREF( var_defs );
    var_defs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_all_scopes );
    Py_DECREF( par_all_scopes );
    par_all_scopes = NULL;

    CHECK_OBJECT( (PyObject *)par_definitions );
    Py_DECREF( par_definitions );
    par_definitions = NULL;

    CHECK_OBJECT( (PyObject *)par_references );
    Py_DECREF( par_references );
    par_references = NULL;

    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)var_def_ref_filter );
    Py_DECREF( var_def_ref_filter );
    var_def_ref_filter = NULL;

    CHECK_OBJECT( (PyObject *)var_create_name );
    Py_DECREF( var_create_name );
    var_create_name = NULL;

    Py_XDECREF( var_script );
    var_script = NULL;

    CHECK_OBJECT( (PyObject *)var_module_context );
    Py_DECREF( var_module_context );
    var_module_context = NULL;

    Py_XDECREF( var_defs );
    var_defs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_12_names$$$function_1_def_ref_filter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par__def = python_pars[ 0 ];
    PyObject *var_is_def = NULL;
    struct Nuitka_FrameObject *frame_3d5f6a807b8650ae92ff2e4d07a08aeb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_3d5f6a807b8650ae92ff2e4d07a08aeb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3d5f6a807b8650ae92ff2e4d07a08aeb, codeobj_3d5f6a807b8650ae92ff2e4d07a08aeb, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3d5f6a807b8650ae92ff2e4d07a08aeb = cache_frame_3d5f6a807b8650ae92ff2e4d07a08aeb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3d5f6a807b8650ae92ff2e4d07a08aeb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3d5f6a807b8650ae92ff2e4d07a08aeb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par__def );
        tmp_source_name_2 = par__def;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__name );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 459;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_tree_name );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 459;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        frame_3d5f6a807b8650ae92ff2e4d07a08aeb->m_frame.f_lineno = 459;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_is_definition );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 459;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        assert( var_is_def == NULL );
        var_is_def = tmp_assign_source_1;
    }
    {
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        int tmp_and_left_truth_2;
        PyObject *tmp_and_left_value_2;
        PyObject *tmp_and_right_value_2;
        PyObject *tmp_operand_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "definitions" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 460;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_and_left_value_1 = PyCell_GET( self->m_closure[0] );
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 460;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_is_def );
        tmp_and_right_value_1 = var_is_def;
        tmp_or_left_value_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_or_left_value_1 = tmp_and_left_value_1;
        and_end_1:;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 460;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "references" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 460;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_and_left_value_2 = PyCell_GET( self->m_closure[1] );
        tmp_and_left_truth_2 = CHECK_IF_TRUE( tmp_and_left_value_2 );
        if ( tmp_and_left_truth_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 460;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        CHECK_OBJECT( var_is_def );
        tmp_operand_name_1 = var_is_def;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 460;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_2 = ( tmp_res == 0 ) ? Py_True : Py_False;
        tmp_or_right_value_1 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_or_right_value_1 = tmp_and_left_value_2;
        and_end_2:;
        tmp_return_value = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_return_value = tmp_or_left_value_1;
        or_end_1:;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d5f6a807b8650ae92ff2e4d07a08aeb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d5f6a807b8650ae92ff2e4d07a08aeb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d5f6a807b8650ae92ff2e4d07a08aeb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3d5f6a807b8650ae92ff2e4d07a08aeb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3d5f6a807b8650ae92ff2e4d07a08aeb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3d5f6a807b8650ae92ff2e4d07a08aeb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3d5f6a807b8650ae92ff2e4d07a08aeb,
        type_description_1,
        par__def,
        var_is_def,
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_3d5f6a807b8650ae92ff2e4d07a08aeb == cache_frame_3d5f6a807b8650ae92ff2e4d07a08aeb )
    {
        Py_DECREF( frame_3d5f6a807b8650ae92ff2e4d07a08aeb );
    }
    cache_frame_3d5f6a807b8650ae92ff2e4d07a08aeb = NULL;

    assertFrameObject( frame_3d5f6a807b8650ae92ff2e4d07a08aeb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names$$$function_1_def_ref_filter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par__def );
    Py_DECREF( par__def );
    par__def = NULL;

    CHECK_OBJECT( (PyObject *)var_is_def );
    Py_DECREF( var_is_def );
    var_is_def = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par__def );
    Py_DECREF( par__def );
    par__def = NULL;

    Py_XDECREF( var_is_def );
    var_is_def = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names$$$function_1_def_ref_filter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_12_names$$$function_2_create_name( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *var_cls = NULL;
    PyObject *var_is_module = NULL;
    struct Nuitka_FrameObject *frame_e860bc0ee9177e567626a09673042f35;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_e860bc0ee9177e567626a09673042f35 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e860bc0ee9177e567626a09673042f35, codeobj_e860bc0ee9177e567626a09673042f35, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e860bc0ee9177e567626a09673042f35 = cache_frame_e860bc0ee9177e567626a09673042f35;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e860bc0ee9177e567626a09673042f35 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e860bc0ee9177e567626a09673042f35 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_name );
        tmp_source_name_2 = par_name;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parent );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 463;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_type );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 463;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_param;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 463;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_ParamName );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ParamName );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ParamName" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 464;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }

            tmp_assign_source_1 = tmp_mvar_value_1;
            assert( var_cls == NULL );
            Py_INCREF( tmp_assign_source_1 );
            var_cls = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TreeNameDefinition" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 466;
                type_description_1 = "oooc";
                goto frame_exception_exit_1;
            }

            tmp_assign_source_2 = tmp_mvar_value_2;
            assert( var_cls == NULL );
            Py_INCREF( tmp_assign_source_2 );
            var_cls = tmp_assign_source_2;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_name );
        tmp_source_name_4 = par_name;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_parent );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 467;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_type );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 467;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_str_plain_file_input;
        tmp_assign_source_3 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 467;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        assert( var_is_module == NULL );
        var_is_module = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( var_cls );
        tmp_called_name_1 = var_cls;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "module_context" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 469;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = PyCell_GET( self->m_closure[0] );
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_create_context );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 469;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_is_module );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_is_module );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 469;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_2 = par_name;
        Py_INCREF( tmp_args_element_name_2 );
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( par_name );
        tmp_source_name_6 = par_name;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_parent );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 469;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        condexpr_end_1:;
        frame_e860bc0ee9177e567626a09673042f35->m_frame.f_lineno = 469;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 469;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_args_element_name_3 = par_name;
        frame_e860bc0ee9177e567626a09673042f35->m_frame.f_lineno = 468;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 468;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e860bc0ee9177e567626a09673042f35 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e860bc0ee9177e567626a09673042f35 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e860bc0ee9177e567626a09673042f35 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e860bc0ee9177e567626a09673042f35, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e860bc0ee9177e567626a09673042f35->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e860bc0ee9177e567626a09673042f35, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e860bc0ee9177e567626a09673042f35,
        type_description_1,
        par_name,
        var_cls,
        var_is_module,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_e860bc0ee9177e567626a09673042f35 == cache_frame_e860bc0ee9177e567626a09673042f35 )
    {
        Py_DECREF( frame_e860bc0ee9177e567626a09673042f35 );
    }
    cache_frame_e860bc0ee9177e567626a09673042f35 = NULL;

    assertFrameObject( frame_e860bc0ee9177e567626a09673042f35 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names$$$function_2_create_name );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)var_cls );
    Py_DECREF( var_cls );
    var_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_is_module );
    Py_DECREF( var_is_module );
    var_is_module = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_cls );
    var_cls = NULL;

    Py_XDECREF( var_is_module );
    var_is_module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names$$$function_2_create_name );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_12_names$$$function_3_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ee834041e03ee3bb1f8af48d78aaa9f4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ee834041e03ee3bb1f8af48d78aaa9f4 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ee834041e03ee3bb1f8af48d78aaa9f4, codeobj_ee834041e03ee3bb1f8af48d78aaa9f4, module_jedi$api, sizeof(void *) );
    frame_ee834041e03ee3bb1f8af48d78aaa9f4 = cache_frame_ee834041e03ee3bb1f8af48d78aaa9f4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ee834041e03ee3bb1f8af48d78aaa9f4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ee834041e03ee3bb1f8af48d78aaa9f4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_x );
        tmp_source_name_1 = par_x;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_line );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 482;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_x );
        tmp_source_name_2 = par_x;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_column );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 482;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ee834041e03ee3bb1f8af48d78aaa9f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ee834041e03ee3bb1f8af48d78aaa9f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ee834041e03ee3bb1f8af48d78aaa9f4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ee834041e03ee3bb1f8af48d78aaa9f4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ee834041e03ee3bb1f8af48d78aaa9f4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ee834041e03ee3bb1f8af48d78aaa9f4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ee834041e03ee3bb1f8af48d78aaa9f4,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_ee834041e03ee3bb1f8af48d78aaa9f4 == cache_frame_ee834041e03ee3bb1f8af48d78aaa9f4 )
    {
        Py_DECREF( frame_ee834041e03ee3bb1f8af48d78aaa9f4 );
    }
    cache_frame_ee834041e03ee3bb1f8af48d78aaa9f4 = NULL;

    assertFrameObject( frame_ee834041e03ee3bb1f8af48d78aaa9f4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names$$$function_3_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_12_names$$$function_3_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_13_preload_module( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_modules = python_pars[ 0 ];
    PyObject *var_m = NULL;
    PyObject *var_s = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_3cee4d78acba2a6869e442f856042225;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_3cee4d78acba2a6869e442f856042225 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3cee4d78acba2a6869e442f856042225, codeobj_3cee4d78acba2a6869e442f856042225, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3cee4d78acba2a6869e442f856042225 = cache_frame_3cee4d78acba2a6869e442f856042225;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3cee4d78acba2a6869e442f856042225 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3cee4d78acba2a6869e442f856042225 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_modules );
        tmp_iter_arg_1 = par_modules;
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 492;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 492;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_m;
            var_m = tmp_assign_source_3;
            Py_INCREF( var_m );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_850ff8eb9217fa94bb33e66eb611bf8d;
        CHECK_OBJECT( var_m );
        tmp_right_name_1 = var_m;
        tmp_assign_source_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 493;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_s;
            var_s = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Script );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Script );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Script" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 494;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_s );
        tmp_args_element_name_1 = var_s;
        tmp_args_element_name_2 = const_int_pos_1;
        CHECK_OBJECT( var_s );
        tmp_len_arg_1 = var_s;
        tmp_args_element_name_3 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 494;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_args_element_name_4 = Py_None;
        frame_3cee4d78acba2a6869e442f856042225->m_frame.f_lineno = 494;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 494;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        frame_3cee4d78acba2a6869e442f856042225->m_frame.f_lineno = 494;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_completions );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 494;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 492;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3cee4d78acba2a6869e442f856042225 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3cee4d78acba2a6869e442f856042225 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3cee4d78acba2a6869e442f856042225, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3cee4d78acba2a6869e442f856042225->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3cee4d78acba2a6869e442f856042225, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3cee4d78acba2a6869e442f856042225,
        type_description_1,
        par_modules,
        var_m,
        var_s
    );


    // Release cached frame.
    if ( frame_3cee4d78acba2a6869e442f856042225 == cache_frame_3cee4d78acba2a6869e442f856042225 )
    {
        Py_DECREF( frame_3cee4d78acba2a6869e442f856042225 );
    }
    cache_frame_3cee4d78acba2a6869e442f856042225 = NULL;

    assertFrameObject( frame_3cee4d78acba2a6869e442f856042225 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_13_preload_module );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_modules );
    Py_DECREF( par_modules );
    par_modules = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_modules );
    Py_DECREF( par_modules );
    par_modules = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_13_preload_module );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$api$$$function_14_set_debug_function( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_func_cb = python_pars[ 0 ];
    PyObject *par_warnings = python_pars[ 1 ];
    PyObject *par_notices = python_pars[ 2 ];
    PyObject *par_speed = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_b8f17eb17f01749af14c8a24fe0b44e7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_b8f17eb17f01749af14c8a24fe0b44e7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b8f17eb17f01749af14c8a24fe0b44e7, codeobj_b8f17eb17f01749af14c8a24fe0b44e7, module_jedi$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b8f17eb17f01749af14c8a24fe0b44e7 = cache_frame_b8f17eb17f01749af14c8a24fe0b44e7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b8f17eb17f01749af14c8a24fe0b44e7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b8f17eb17f01749af14c8a24fe0b44e7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_func_cb );
        tmp_assattr_name_1 = par_func_cb;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 506;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_debug_function, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 506;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_warnings );
        tmp_assattr_name_2 = par_warnings;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 507;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_enable_warning, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 507;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( par_notices );
        tmp_assattr_name_3 = par_notices;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 508;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_3 = tmp_mvar_value_3;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_enable_notice, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 508;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_mvar_value_4;
        CHECK_OBJECT( par_speed );
        tmp_assattr_name_4 = par_speed;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 509;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_4 = tmp_mvar_value_4;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_enable_speed, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 509;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b8f17eb17f01749af14c8a24fe0b44e7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b8f17eb17f01749af14c8a24fe0b44e7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b8f17eb17f01749af14c8a24fe0b44e7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b8f17eb17f01749af14c8a24fe0b44e7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b8f17eb17f01749af14c8a24fe0b44e7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b8f17eb17f01749af14c8a24fe0b44e7,
        type_description_1,
        par_func_cb,
        par_warnings,
        par_notices,
        par_speed
    );


    // Release cached frame.
    if ( frame_b8f17eb17f01749af14c8a24fe0b44e7 == cache_frame_b8f17eb17f01749af14c8a24fe0b44e7 )
    {
        Py_DECREF( frame_b8f17eb17f01749af14c8a24fe0b44e7 );
    }
    cache_frame_b8f17eb17f01749af14c8a24fe0b44e7 = NULL;

    assertFrameObject( frame_b8f17eb17f01749af14c8a24fe0b44e7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_14_set_debug_function );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_func_cb );
    Py_DECREF( par_func_cb );
    par_func_cb = NULL;

    CHECK_OBJECT( (PyObject *)par_warnings );
    Py_DECREF( par_warnings );
    par_warnings = NULL;

    CHECK_OBJECT( (PyObject *)par_notices );
    Py_DECREF( par_notices );
    par_notices = NULL;

    CHECK_OBJECT( (PyObject *)par_speed );
    Py_DECREF( par_speed );
    par_speed = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_func_cb );
    Py_DECREF( par_func_cb );
    par_func_cb = NULL;

    CHECK_OBJECT( (PyObject *)par_warnings );
    Py_DECREF( par_warnings );
    par_warnings = NULL;

    CHECK_OBJECT( (PyObject *)par_notices );
    Py_DECREF( par_notices );
    par_notices = NULL;

    CHECK_OBJECT( (PyObject *)par_speed );
    Py_DECREF( par_speed );
    par_speed = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$api$$$function_14_set_debug_function );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_10___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_10___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_159c0e51aa7bed34f7d69aa031acaa8b,
#endif
        codeobj_2a4a505dbc600e5d254792d9fe4b650a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_752ca1a8394d43d581c2778099e279bd,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_11__get_module(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_11__get_module,
        const_str_plain__get_module,
#if PYTHON_VERSION >= 300
        const_str_digest_08516a7712d209048033a0a9c06e8f29,
#endif
        codeobj_b707086c23679791b0fdd79bb19830fe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_12_names,
        const_str_plain_names,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d0250c08e540e8597497c3240e385814,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_31d7a0834fe968eabab69cf8f5a48e05,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_1_def_ref_filter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_12_names$$$function_1_def_ref_filter,
        const_str_plain_def_ref_filter,
#if PYTHON_VERSION >= 300
        const_str_digest_bf86e1f77a0f2f17c8ad1f8fd620c7ff,
#endif
        codeobj_3d5f6a807b8650ae92ff2e4d07a08aeb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_2_create_name(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_12_names$$$function_2_create_name,
        const_str_plain_create_name,
#if PYTHON_VERSION >= 300
        const_str_digest_6b3ae216d1716a65bce6e25881060f5a,
#endif
        codeobj_e860bc0ee9177e567626a09673042f35,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_12_names$$$function_3_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_12_names$$$function_3_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_fa9567be3e6ce4294ffeb38c0d795338,
#endif
        codeobj_ee834041e03ee3bb1f8af48d78aaa9f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_13_preload_module(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_13_preload_module,
        const_str_plain_preload_module,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3cee4d78acba2a6869e442f856042225,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_d7e04ec0da60cb7506fb299cd50368ad,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_14_set_debug_function( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_14_set_debug_function,
        const_str_plain_set_debug_function,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b8f17eb17f01749af14c8a24fe0b44e7,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_fb001c9aa45e5bdb49173ed95a8840ad,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_1___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_214a4ae8c169a3cf220a05c4ef7cce2b,
#endif
        codeobj_c1930f3c2365667fc0f7ea3a1d9cc3dc,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_2__get_module(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_2__get_module,
        const_str_plain__get_module,
#if PYTHON_VERSION >= 300
        const_str_digest_e984d578221bf5ab887bc928606fe92f,
#endif
        codeobj_e36d4bf9a33efd7a74bd830b6bc3c93a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_3___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_3___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_8cec39e80ada9292d72ce8db3885c33b,
#endif
        codeobj_c2ffe85c025b82dd0a130c545e9ba132,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_4_completions(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_4_completions,
        const_str_plain_completions,
#if PYTHON_VERSION >= 300
        const_str_digest_9e55e4857d0ad75b0912975beaf9d07f,
#endif
        codeobj_8c8f61040eabcc4a506848e31297fefe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_eebc58417963cbd112d5b768dbcb7e22,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_4_completions$$$function_1_iter_import_completions(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_4_completions$$$function_1_iter_import_completions,
        const_str_plain_iter_import_completions,
#if PYTHON_VERSION >= 300
        const_str_digest_9d8c6d366414ef1e85c16a13d371a449,
#endif
        codeobj_91acf3337269e0ab62a5d399cafa6f47,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_5_goto_definitions(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_5_goto_definitions,
        const_str_plain_goto_definitions,
#if PYTHON_VERSION >= 300
        const_str_digest_60ac3a6e6496dc688804c5013df38ce7,
#endif
        codeobj_d377d00d18d32214d6677962c7a243cd,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_5e2ddd8a63b2a0dc4f109507f9269534,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_6_goto_assignments,
        const_str_plain_goto_assignments,
#if PYTHON_VERSION >= 300
        const_str_digest_293817b0dc883ddab36bbbd19cf19ad8,
#endif
        codeobj_078186f669372586eb34c685197d831f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_1a23c18cf958634aadd7c1de5dbd50bb,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_6_goto_assignments$$$function_1_filter_follow_imports,
        const_str_plain_filter_follow_imports,
#if PYTHON_VERSION >= 300
        const_str_digest_910348dcbc1269ee447323aeeea7e275,
#endif
        codeobj_4ae0934951a98b59abe31a69fd48febb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_2_check(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_6_goto_assignments$$$function_2_check,
        const_str_plain_check,
#if PYTHON_VERSION >= 300
        const_str_digest_64a8f90da046b08e884ef75a97a7425a,
#endif
        codeobj_3636078bdb31c7973aad9575da42b0a5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments$$$function_3_check(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_6_goto_assignments$$$function_3_check,
        const_str_plain_check,
#if PYTHON_VERSION >= 300
        const_str_digest_64a8f90da046b08e884ef75a97a7425a,
#endif
        codeobj_4f569dc0a04bc624621bfb55ceaced21,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_7_usages( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_7_usages,
        const_str_plain_usages,
#if PYTHON_VERSION >= 300
        const_str_digest_0fbd70c135f5eed3cd3799b1364c79b9,
#endif
        codeobj_441859b3ba7e70dea7e3ab377b6cdb4a,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_2587a82b2319532c94bbeee41e21af98,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_7_usages$$$function_1__usages( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_7_usages$$$function_1__usages,
        const_str_plain__usages,
#if PYTHON_VERSION >= 300
        const_str_digest_9abc1f4b568a3c6e95f620df48d1ed17,
#endif
        codeobj_62e2049811a9fa0bb8d9072fc36548f6,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_8_call_signatures(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_8_call_signatures,
        const_str_plain_call_signatures,
#if PYTHON_VERSION >= 300
        const_str_digest_4b0f73d6163cc9e4158bce4085f7eb24,
#endif
        codeobj_a708c6c0c7e14854e4d89d212cbec165,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        const_str_digest_037c84dba6b02cdb4b71ec3700af569f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_9__analysis(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_9__analysis,
        const_str_plain__analysis,
#if PYTHON_VERSION >= 300
        const_str_digest_0dc5267be73cc2f56c6e9cdca7c79da2,
#endif
        codeobj_9a10fd60e3a46a712ec5e1e8c1f9d504,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$api$$$function_9__analysis$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$api$$$function_9__analysis$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_af46f61fe85f7c6a0a59451be505d157,
#endif
        codeobj_774df3a9437f71117153addab97147f7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$api,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jedi$api =
{
    PyModuleDef_HEAD_INIT,
    "jedi.api",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jedi$api)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jedi$api)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jedi$api );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jedi.api: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.api: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.api: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjedi$api" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jedi$api = Py_InitModule4(
        "jedi.api",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jedi$api = PyModule_Create( &mdef_jedi$api );
#endif

    moduledict_jedi$api = MODULE_DICT( module_jedi$api );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jedi$api,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 1
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jedi$api,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jedi$api );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_8354bab93b2e72580e53a32ea4f9c404, module_jedi$api );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 1
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_adf1f479c98f4ee3189eef628359f34b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_jedi$api_46 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_766e9b0d1c67d44adbc3e2a6ed011954_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_766e9b0d1c67d44adbc3e2a6ed011954_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *locals_jedi$api_387 = NULL;
    struct Nuitka_FrameObject *frame_d9743976b47d07f2790838f244e39c16_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_d9743976b47d07f2790838f244e39c16_3 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_9003b6e82ea7814524d19ee7cb013361;
        UPDATE_STRING_DICT0( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_adf1f479c98f4ee3189eef628359f34b = MAKE_MODULE_FRAME( codeobj_adf1f479c98f4ee3189eef628359f34b, module_jedi$api );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_adf1f479c98f4ee3189eef628359f34b );
    assert( Py_REFCNT( frame_adf1f479c98f4ee3189eef628359f34b ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_2;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_source_name_1 = PyObject_GetAttr( module, const_str_plain_path );
            }
            else
            {
                tmp_source_name_1 = NULL;
            }
        }

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_dirname );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = module_filename_obj;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PyList_New( 3 );
        PyList_SET_ITEM( tmp_assign_source_3, 0, tmp_list_element_1 );
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_source_name_2 = PyObject_GetAttr( module, const_str_plain_path );
            }
            else
            {
                tmp_source_name_2 = NULL;
            }
        }

        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_join );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_called_instance_1 = PyObject_GetAttr( module, const_str_plain_environ );
            }
            else
            {
                tmp_called_instance_1 = NULL;
            }
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_f0f15781590b4fd8bce07dd7072b2735_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = const_str_plain_api;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_3, 1, tmp_list_element_1 );
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_called_instance_2 = PyObject_GetAttr( module, const_str_plain_environ );
            }
            else
            {
                tmp_called_instance_2 = NULL;
            }
        }

        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 1;
        tmp_list_element_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_1975d1900a6d3fb1ef42b420838be7e9_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_3, 2, tmp_list_element_1 );
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___path__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_os;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 12;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_sys;
        tmp_globals_name_2 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 13;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        assert( !(tmp_assign_source_6 == NULL) );
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_warnings;
        tmp_globals_name_3 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 14;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_warnings, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_parso;
        tmp_globals_name_4 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 16;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_parso, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_3d8c90d9a9bb20202fbddcdf6f6b7d19;
        tmp_globals_name_5 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_tree_tuple;
        tmp_level_name_5 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 17;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_tree );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_tree, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_4ea3c739c0994e1c59a1638cbf9bd81c;
        tmp_globals_name_6 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_force_unicode_str_plain_is_py3_tuple;
        tmp_level_name_6 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 19;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_force_unicode );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_force_unicode, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_is_py3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_is_py3, tmp_assign_source_12 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_deb952b7ac13ad711de11121ed587a30;
        tmp_globals_name_7 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_get_executable_nodes_tuple;
        tmp_level_name_7 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 20;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_get_executable_nodes );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_get_executable_nodes, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_jedi;
        tmp_globals_name_8 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_debug_tuple;
        tmp_level_name_8 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 21;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_debug );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_jedi;
        tmp_globals_name_9 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_settings_tuple;
        tmp_level_name_9 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 22;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_settings );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_settings, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_plain_jedi;
        tmp_globals_name_10 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_cache_tuple;
        tmp_level_name_10 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 23;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_cache );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_cache, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_8354bab93b2e72580e53a32ea4f9c404;
        tmp_globals_name_11 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_classes_tuple;
        tmp_level_name_11 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 24;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_classes );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_classes, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_8354bab93b2e72580e53a32ea4f9c404;
        tmp_globals_name_12 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_interpreter_tuple;
        tmp_level_name_12 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 25;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_interpreter );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_interpreter, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_digest_8354bab93b2e72580e53a32ea4f9c404;
        tmp_globals_name_13 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = const_tuple_str_plain_helpers_tuple;
        tmp_level_name_13 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 26;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_helpers );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_helpers, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_11;
        PyObject *tmp_name_name_14;
        PyObject *tmp_globals_name_14;
        PyObject *tmp_locals_name_14;
        PyObject *tmp_fromlist_name_14;
        PyObject *tmp_level_name_14;
        tmp_name_name_14 = const_str_digest_1d33749dff5e5d6b977290c5d2c43e47;
        tmp_globals_name_14 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_14 = Py_None;
        tmp_fromlist_name_14 = const_tuple_str_plain_Completion_tuple;
        tmp_level_name_14 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 27;
        tmp_import_name_from_11 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
        if ( tmp_import_name_from_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_Completion );
        Py_DECREF( tmp_import_name_from_11 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Completion, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_12;
        PyObject *tmp_name_name_15;
        PyObject *tmp_globals_name_15;
        PyObject *tmp_locals_name_15;
        PyObject *tmp_fromlist_name_15;
        PyObject *tmp_level_name_15;
        tmp_name_name_15 = const_str_digest_a19478749b11c4aeb121b2ebd2eeb5ca;
        tmp_globals_name_15 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_15 = Py_None;
        tmp_fromlist_name_15 = const_tuple_str_plain_InterpreterEnvironment_tuple;
        tmp_level_name_15 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 28;
        tmp_import_name_from_12 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
        if ( tmp_import_name_from_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_InterpreterEnvironment );
        Py_DECREF( tmp_import_name_from_12 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_InterpreterEnvironment, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_13;
        PyObject *tmp_name_name_16;
        PyObject *tmp_globals_name_16;
        PyObject *tmp_locals_name_16;
        PyObject *tmp_fromlist_name_16;
        PyObject *tmp_level_name_16;
        tmp_name_name_16 = const_str_digest_cc282ec370e83077ad88e5954bcf7b3d;
        tmp_globals_name_16 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_16 = Py_None;
        tmp_fromlist_name_16 = const_tuple_str_plain_get_default_project_tuple;
        tmp_level_name_16 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 29;
        tmp_import_name_from_13 = IMPORT_MODULE5( tmp_name_name_16, tmp_globals_name_16, tmp_locals_name_16, tmp_fromlist_name_16, tmp_level_name_16 );
        if ( tmp_import_name_from_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_get_default_project );
        Py_DECREF( tmp_import_name_from_13 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_get_default_project, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_14;
        PyObject *tmp_name_name_17;
        PyObject *tmp_globals_name_17;
        PyObject *tmp_locals_name_17;
        PyObject *tmp_fromlist_name_17;
        PyObject *tmp_level_name_17;
        tmp_name_name_17 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_17 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_17 = Py_None;
        tmp_fromlist_name_17 = const_tuple_str_plain_Evaluator_tuple;
        tmp_level_name_17 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 30;
        tmp_import_name_from_14 = IMPORT_MODULE5( tmp_name_name_17, tmp_globals_name_17, tmp_locals_name_17, tmp_fromlist_name_17, tmp_level_name_17 );
        if ( tmp_import_name_from_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_Evaluator );
        Py_DECREF( tmp_import_name_from_14 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Evaluator, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_15;
        PyObject *tmp_name_name_18;
        PyObject *tmp_globals_name_18;
        PyObject *tmp_locals_name_18;
        PyObject *tmp_fromlist_name_18;
        PyObject *tmp_level_name_18;
        tmp_name_name_18 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_18 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_18 = Py_None;
        tmp_fromlist_name_18 = const_tuple_str_plain_imports_tuple;
        tmp_level_name_18 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 31;
        tmp_import_name_from_15 = IMPORT_MODULE5( tmp_name_name_18, tmp_globals_name_18, tmp_locals_name_18, tmp_fromlist_name_18, tmp_level_name_18 );
        if ( tmp_import_name_from_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_imports );
        Py_DECREF( tmp_import_name_from_15 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_imports, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_16;
        PyObject *tmp_name_name_19;
        PyObject *tmp_globals_name_19;
        PyObject *tmp_locals_name_19;
        PyObject *tmp_fromlist_name_19;
        PyObject *tmp_level_name_19;
        tmp_name_name_19 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_19 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_19 = Py_None;
        tmp_fromlist_name_19 = const_tuple_str_plain_usages_tuple;
        tmp_level_name_19 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 32;
        tmp_import_name_from_16 = IMPORT_MODULE5( tmp_name_name_19, tmp_globals_name_19, tmp_locals_name_19, tmp_fromlist_name_19, tmp_level_name_19 );
        if ( tmp_import_name_from_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_usages );
        Py_DECREF( tmp_import_name_from_16 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_usages, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_17;
        PyObject *tmp_name_name_20;
        PyObject *tmp_globals_name_20;
        PyObject *tmp_locals_name_20;
        PyObject *tmp_fromlist_name_20;
        PyObject *tmp_level_name_20;
        tmp_name_name_20 = const_str_digest_93e555a5b1643558b28cf6b2256158dc;
        tmp_globals_name_20 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_20 = Py_None;
        tmp_fromlist_name_20 = const_tuple_str_plain_try_iter_content_tuple;
        tmp_level_name_20 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 33;
        tmp_import_name_from_17 = IMPORT_MODULE5( tmp_name_name_20, tmp_globals_name_20, tmp_locals_name_20, tmp_fromlist_name_20, tmp_level_name_20 );
        if ( tmp_import_name_from_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_try_iter_content );
        Py_DECREF( tmp_import_name_from_17 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_try_iter_content, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_name_name_21;
        PyObject *tmp_globals_name_21;
        PyObject *tmp_locals_name_21;
        PyObject *tmp_fromlist_name_21;
        PyObject *tmp_level_name_21;
        tmp_name_name_21 = const_str_digest_506c2565b910c9d58a55a1d887dc4a2e;
        tmp_globals_name_21 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_21 = Py_None;
        tmp_fromlist_name_21 = const_tuple_str_plain_get_module_names_str_plain_evaluate_call_of_leaf_tuple;
        tmp_level_name_21 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 34;
        tmp_assign_source_27 = IMPORT_MODULE5( tmp_name_name_21, tmp_globals_name_21, tmp_locals_name_21, tmp_fromlist_name_21, tmp_level_name_21 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_27;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_18 = tmp_import_from_2__module;
        tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_get_module_names );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_get_module_names, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_import_name_from_19;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_19 = tmp_import_from_2__module;
        tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_evaluate_call_of_leaf );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_evaluate_call_of_leaf, tmp_assign_source_29 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_import_name_from_20;
        PyObject *tmp_name_name_22;
        PyObject *tmp_globals_name_22;
        PyObject *tmp_locals_name_22;
        PyObject *tmp_fromlist_name_22;
        PyObject *tmp_level_name_22;
        tmp_name_name_22 = const_str_digest_f2ae848d840696d0977d1f391a666889;
        tmp_globals_name_22 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_22 = Py_None;
        tmp_fromlist_name_22 = const_tuple_str_plain_dotted_path_in_sys_path_tuple;
        tmp_level_name_22 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 35;
        tmp_import_name_from_20 = IMPORT_MODULE5( tmp_name_name_22, tmp_globals_name_22, tmp_locals_name_22, tmp_fromlist_name_22, tmp_level_name_22 );
        if ( tmp_import_name_from_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_dotted_path_in_sys_path );
        Py_DECREF( tmp_import_name_from_20 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_dotted_path_in_sys_path, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_name_name_23;
        PyObject *tmp_globals_name_23;
        PyObject *tmp_locals_name_23;
        PyObject *tmp_fromlist_name_23;
        PyObject *tmp_level_name_23;
        tmp_name_name_23 = const_str_digest_c32a307eb4cbe1d85147469cdfd3f906;
        tmp_globals_name_23 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_23 = Py_None;
        tmp_fromlist_name_23 = const_tuple_str_plain_TreeNameDefinition_str_plain_ParamName_tuple;
        tmp_level_name_23 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 36;
        tmp_assign_source_31 = IMPORT_MODULE5( tmp_name_name_23, tmp_globals_name_23, tmp_locals_name_23, tmp_fromlist_name_23, tmp_level_name_23 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_31;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_import_name_from_21;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_21 = tmp_import_from_3__module;
        tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_TreeNameDefinition );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_import_name_from_22;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_22 = tmp_import_from_3__module;
        tmp_assign_source_33 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_ParamName );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_ParamName, tmp_assign_source_33 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_import_name_from_23;
        PyObject *tmp_name_name_24;
        PyObject *tmp_globals_name_24;
        PyObject *tmp_locals_name_24;
        PyObject *tmp_fromlist_name_24;
        PyObject *tmp_level_name_24;
        tmp_name_name_24 = const_str_digest_3e1604d04ce104740ab4a0cd2cf306db;
        tmp_globals_name_24 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_24 = Py_None;
        tmp_fromlist_name_24 = const_tuple_str_plain_tree_name_to_contexts_tuple;
        tmp_level_name_24 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 37;
        tmp_import_name_from_23 = IMPORT_MODULE5( tmp_name_name_24, tmp_globals_name_24, tmp_locals_name_24, tmp_fromlist_name_24, tmp_level_name_24 );
        if ( tmp_import_name_from_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_34 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_tree_name_to_contexts );
        Py_DECREF( tmp_import_name_from_23 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_tree_name_to_contexts, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_import_name_from_24;
        PyObject *tmp_name_name_25;
        PyObject *tmp_globals_name_25;
        PyObject *tmp_locals_name_25;
        PyObject *tmp_fromlist_name_25;
        PyObject *tmp_level_name_25;
        tmp_name_name_25 = const_str_digest_785ebe15053a4fa731c98e296de8edd7;
        tmp_globals_name_25 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_25 = Py_None;
        tmp_fromlist_name_25 = const_tuple_str_plain_ModuleContext_tuple;
        tmp_level_name_25 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 38;
        tmp_import_name_from_24 = IMPORT_MODULE5( tmp_name_name_25, tmp_globals_name_25, tmp_locals_name_25, tmp_fromlist_name_25, tmp_level_name_25 );
        if ( tmp_import_name_from_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_35 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_ModuleContext );
        Py_DECREF( tmp_import_name_from_24 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_ModuleContext, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_import_name_from_25;
        PyObject *tmp_name_name_26;
        PyObject *tmp_globals_name_26;
        PyObject *tmp_locals_name_26;
        PyObject *tmp_fromlist_name_26;
        PyObject *tmp_level_name_26;
        tmp_name_name_26 = const_str_digest_cd4a43d41990fe11ed21029f9707bb7b;
        tmp_globals_name_26 = (PyObject *)moduledict_jedi$api;
        tmp_locals_name_26 = Py_None;
        tmp_fromlist_name_26 = const_tuple_str_plain_unpack_tuple_to_dict_tuple;
        tmp_level_name_26 = const_int_0;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 39;
        tmp_import_name_from_25 = IMPORT_MODULE5( tmp_name_name_26, tmp_globals_name_26, tmp_locals_name_26, tmp_fromlist_name_26, tmp_level_name_26 );
        if ( tmp_import_name_from_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_36 = IMPORT_NAME( tmp_import_name_from_25, const_str_plain_unpack_tuple_to_dict );
        Py_DECREF( tmp_import_name_from_25 );
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_unpack_tuple_to_dict, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_3;
        frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 43;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_setrecursionlimit, &PyTuple_GET_ITEM( const_tuple_int_pos_3000_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_37 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_37;
    }
    {
        PyObject *tmp_assign_source_38;
        tmp_assign_source_38 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_38;
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_39 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_39;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_4;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_3 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_40;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_4 = tmp_class_creation_1__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_4;
            }
            tmp_tuple_element_1 = const_str_plain_Script;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 46;
            tmp_assign_source_40 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_40 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_40;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_5 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_4;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_6;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_4;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_6 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_6 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_6 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 46;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 46;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_41;
            tmp_assign_source_41 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_41;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_42;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jedi$api_46 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_8354bab93b2e72580e53a32ea4f9c404;
        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_digest_5956fa9b5c0a37254ac03448d15cc622;
        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain_Script;
        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_766e9b0d1c67d44adbc3e2a6ed011954_2, codeobj_766e9b0d1c67d44adbc3e2a6ed011954, module_jedi$api, sizeof(void *) );
        frame_766e9b0d1c67d44adbc3e2a6ed011954_2 = cache_frame_766e9b0d1c67d44adbc3e2a6ed011954_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_766e9b0d1c67d44adbc3e2a6ed011954_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_766e9b0d1c67d44adbc3e2a6ed011954_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_7593240d27bdc17c3ee17b00e733ae19_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_1___init__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_2__get_module(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain__get_module, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_3___repr__(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_4_completions(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain_completions, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_5_goto_definitions(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain_goto_definitions, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_false_false_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_6_goto_assignments( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain_goto_assignments, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 229;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_3;
            tmp_defaults_3 = const_tuple_tuple_empty_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_7_usages( tmp_defaults_3 );



            tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain_usages, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 277;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_8_call_signatures(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain_call_signatures, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_9__analysis(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain__analysis, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 352;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_766e9b0d1c67d44adbc3e2a6ed011954_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_766e9b0d1c67d44adbc3e2a6ed011954_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_766e9b0d1c67d44adbc3e2a6ed011954_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_766e9b0d1c67d44adbc3e2a6ed011954_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_766e9b0d1c67d44adbc3e2a6ed011954_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_766e9b0d1c67d44adbc3e2a6ed011954_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_766e9b0d1c67d44adbc3e2a6ed011954_2 == cache_frame_766e9b0d1c67d44adbc3e2a6ed011954_2 )
        {
            Py_DECREF( frame_766e9b0d1c67d44adbc3e2a6ed011954_2 );
        }
        cache_frame_766e9b0d1c67d44adbc3e2a6ed011954_2 = NULL;

        assertFrameObject( frame_766e9b0d1c67d44adbc3e2a6ed011954_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_6;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_jedi$api_46, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_6;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_43;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_4 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_Script;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_jedi$api_46;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 46;
            tmp_assign_source_43 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_6;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_43;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_42 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_42 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_jedi$api_46 );
        locals_jedi$api_46 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$api_46 );
        locals_jedi$api_46 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 46;
        goto try_except_handler_4;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Script, tmp_assign_source_42 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Script );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Script );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Script" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 387;

            goto try_except_handler_7;
        }

        tmp_tuple_element_4 = tmp_mvar_value_4;
        tmp_assign_source_44 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_assign_source_44, 0, tmp_tuple_element_4 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_44;
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_45 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_45;
    }
    {
        PyObject *tmp_assign_source_46;
        tmp_assign_source_46 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_46;
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_47 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_47;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_7;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_7 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_48;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_8;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_8 = tmp_class_creation_2__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 387;

                goto try_except_handler_7;
            }
            tmp_tuple_element_5 = const_str_plain_Interpreter;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_5 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 387;
            tmp_assign_source_48 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_48 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 387;

                goto try_except_handler_7;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_48;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_9 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 387;

                goto try_except_handler_7;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_10;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 387;

                    goto try_except_handler_7;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_10 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_10 == NULL) );
                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_10 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 387;

                    goto try_except_handler_7;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 387;

                    goto try_except_handler_7;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 387;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_7;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_49;
            tmp_assign_source_49 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_49;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_50;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_jedi$api_387 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_8354bab93b2e72580e53a32ea4f9c404;
        tmp_res = PyObject_SetItem( locals_jedi$api_387, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_digest_d7ae3998101be51c72e9e902e5692ad0;
        tmp_res = PyObject_SetItem( locals_jedi$api_387, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_plain_Interpreter;
        tmp_res = PyObject_SetItem( locals_jedi$api_387, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;

            goto try_except_handler_9;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_d9743976b47d07f2790838f244e39c16_3, codeobj_d9743976b47d07f2790838f244e39c16, module_jedi$api, sizeof(void *) );
        frame_d9743976b47d07f2790838f244e39c16_3 = cache_frame_d9743976b47d07f2790838f244e39c16_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_d9743976b47d07f2790838f244e39c16_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_d9743976b47d07f2790838f244e39c16_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_10___init__(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_387, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 403;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$api$$$function_11__get_module(  );



        tmp_res = PyObject_SetItem( locals_jedi$api_387, const_str_plain__get_module, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 432;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d9743976b47d07f2790838f244e39c16_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d9743976b47d07f2790838f244e39c16_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_d9743976b47d07f2790838f244e39c16_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_d9743976b47d07f2790838f244e39c16_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_d9743976b47d07f2790838f244e39c16_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_d9743976b47d07f2790838f244e39c16_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_d9743976b47d07f2790838f244e39c16_3 == cache_frame_d9743976b47d07f2790838f244e39c16_3 )
        {
            Py_DECREF( frame_d9743976b47d07f2790838f244e39c16_3 );
        }
        cache_frame_d9743976b47d07f2790838f244e39c16_3 = NULL;

        assertFrameObject( frame_d9743976b47d07f2790838f244e39c16_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_9;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 387;

                goto try_except_handler_9;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_jedi$api_387, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 387;

                goto try_except_handler_9;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_51;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_6 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_7 = const_str_plain_Interpreter;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_7 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
            tmp_tuple_element_7 = locals_jedi$api_387;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_adf1f479c98f4ee3189eef628359f34b->m_frame.f_lineno = 387;
            tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_51 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 387;

                goto try_except_handler_9;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_51;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_50 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_50 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        Py_DECREF( locals_jedi$api_387 );
        locals_jedi$api_387 = NULL;
        goto try_return_handler_8;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$api_387 );
        locals_jedi$api_387 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$api );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$api );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 387;
        goto try_except_handler_7;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_Interpreter, tmp_assign_source_50 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_defaults_4;
        tmp_defaults_4 = const_tuple_4a3d5841c2d16f378ea16063271f0a7e_tuple;
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_52 = MAKE_FUNCTION_jedi$api$$$function_12_names( tmp_defaults_4 );



        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_names, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        tmp_assign_source_53 = MAKE_FUNCTION_jedi$api$$$function_13_preload_module(  );



        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_preload_module, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_defaults_5;
        PyObject *tmp_tuple_element_8;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 497;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_5;
        tmp_tuple_element_8 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_print_to_stdout );
        if ( tmp_tuple_element_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 497;

            goto frame_exception_exit_1;
        }
        tmp_defaults_5 = PyTuple_New( 4 );
        PyTuple_SET_ITEM( tmp_defaults_5, 0, tmp_tuple_element_8 );
        tmp_tuple_element_8 = Py_True;
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_defaults_5, 1, tmp_tuple_element_8 );
        tmp_tuple_element_8 = Py_True;
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_defaults_5, 2, tmp_tuple_element_8 );
        tmp_tuple_element_8 = Py_True;
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_defaults_5, 3, tmp_tuple_element_8 );
        tmp_assign_source_54 = MAKE_FUNCTION_jedi$api$$$function_14_set_debug_function( tmp_defaults_5 );



        UPDATE_STRING_DICT1( moduledict_jedi$api, (Nuitka_StringObject *)const_str_plain_set_debug_function, tmp_assign_source_54 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_adf1f479c98f4ee3189eef628359f34b );
#endif
    popFrameStack();

    assertFrameObject( frame_adf1f479c98f4ee3189eef628359f34b );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_adf1f479c98f4ee3189eef628359f34b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_adf1f479c98f4ee3189eef628359f34b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_adf1f479c98f4ee3189eef628359f34b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_adf1f479c98f4ee3189eef628359f34b, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;

    return MOD_RETURN_VALUE( module_jedi$api );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
