/* Generated code for Python module 'matplotlib.tight_bbox'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$tight_bbox" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$tight_bbox;
PyDictObject *moduledict_matplotlib$tight_bbox;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_plain_zip;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_scale;
extern PyObject *const_str_plain_a;
static PyObject *const_str_digest_6a2fe96210f519e35d1f97234c2ccd42;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_frozen;
extern PyObject *const_str_plain_fixed_dpi;
extern PyObject *const_tuple_str_plain_auto_tuple;
extern PyObject *const_str_plain_auto;
extern PyObject *const_str_plain_transFigure;
static PyObject *const_str_plain_asp_list;
extern PyObject *const_str_plain_get_position;
static PyObject *const_str_plain_asp;
extern PyObject *const_str_plain_pos;
static PyObject *const_str_digest_d7f35a5205290c08f17a9a7bba78906a;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_bbox;
extern PyObject *const_str_plain_Affine2D;
extern PyObject *const_str_plain_set_tight_layout;
static PyObject *const_str_digest_82f249f3cba9d36a5f33710e9ace7b3d;
extern PyObject *const_str_digest_d28745151750b44e549d12f294293c52;
extern PyObject *const_str_plain_y0;
static PyObject *const_str_digest_6e372b2cd024bd000a83aa3f86ea0a36;
extern PyObject *const_str_plain_bbox_inches_restore;
extern PyObject *const_float_1_0;
extern PyObject *const_str_plain_restore_bbox;
static PyObject *const_dict_301f5150c4af32136a6e8de06d709207;
extern PyObject *const_str_plain__boxout;
extern PyObject *const_str_plain_x0;
extern PyObject *const_str_plain_height;
extern PyObject *const_str_plain_loc;
static PyObject *const_tuple_str_plain_a_str_plain_r_str_plain_pos_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_get_aspect;
static PyObject *const_str_digest_c0c7648ab6c33581e053a1408d758008;
extern PyObject *const_str_plain_get_axes_locator;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_r;
extern PyObject *const_str_plain_get_tight_layout;
extern PyObject *const_str_plain__l;
static PyObject *const_str_plain_orig_tight_layout;
static PyObject *const_tuple_int_0_int_0_int_pos_1_int_pos_1_tuple;
extern PyObject *const_str_plain_adjust_bbox;
static PyObject *const_tuple_str_plain_Bbox_str_plain_TransformedBbox_str_plain_Affine2D_tuple;
static PyObject *const_tuple_a708240f0215c403d4b1a876d147b73a_tuple;
extern PyObject *const_str_plain_False;
static PyObject *const_str_plain_process_figure_for_rasterizing;
extern PyObject *const_str_plain_set_axes_locator;
static PyObject *const_str_plain_locator_list;
extern PyObject *const_int_0;
static PyObject *const_str_digest_a7b1fb505ddedd950c695d3981f07dcf;
static PyObject *const_str_digest_43609372675c8acdd537a862ad3bf1fd;
static PyObject *const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_patch;
extern PyObject *const_str_plain_Bbox;
extern PyObject *const_str_plain_set_aspect;
static PyObject *const_str_plain_origBboxInches;
static PyObject *const_str_plain_origBbox;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_tuple_0297d235edf58b17aebb4b75e4796777_tuple;
extern PyObject *const_str_plain_dpi;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_width;
extern PyObject *const_str_plain_fig;
extern PyObject *const_str_plain_TransformedBbox;
extern PyObject *const_str_plain_axes;
extern PyObject *const_str_plain_original;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_set_bounds;
extern PyObject *const_str_plain_invalidate;
extern PyObject *const_str_plain_bbox_inches;
static PyObject *const_str_digest_b9bbf06547eb6c42fac1b4d1e80b3296;
extern PyObject *const_str_plain_ax;
extern PyObject *const_str_plain_from_bounds;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_6a2fe96210f519e35d1f97234c2ccd42 = UNSTREAM_STRING_ASCII( &constant_bin[ 2573827 ], 68, 0 );
    const_str_plain_asp_list = UNSTREAM_STRING_ASCII( &constant_bin[ 2573895 ], 8, 1 );
    const_str_plain_asp = UNSTREAM_STRING_ASCII( &constant_bin[ 6086 ], 3, 1 );
    const_str_digest_d7f35a5205290c08f17a9a7bba78906a = UNSTREAM_STRING_ASCII( &constant_bin[ 2573903 ], 335, 0 );
    const_str_digest_82f249f3cba9d36a5f33710e9ace7b3d = UNSTREAM_STRING_ASCII( &constant_bin[ 2574238 ], 160, 0 );
    const_str_digest_6e372b2cd024bd000a83aa3f86ea0a36 = UNSTREAM_STRING_ASCII( &constant_bin[ 2574398 ], 21, 0 );
    const_dict_301f5150c4af32136a6e8de06d709207 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_301f5150c4af32136a6e8de06d709207, const_str_plain_original, Py_False );
    assert( PyDict_Size( const_dict_301f5150c4af32136a6e8de06d709207 ) == 1 );
    const_tuple_str_plain_a_str_plain_r_str_plain_pos_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_r_str_plain_pos_tuple, 0, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_r_str_plain_pos_tuple, 1, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_r_str_plain_pos_tuple, 2, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    const_str_digest_c0c7648ab6c33581e053a1408d758008 = UNSTREAM_STRING_ASCII( &constant_bin[ 2574419 ], 30, 0 );
    const_str_plain_orig_tight_layout = UNSTREAM_STRING_ASCII( &constant_bin[ 2574449 ], 17, 1 );
    const_tuple_int_0_int_0_int_pos_1_int_pos_1_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_0_int_pos_1_int_pos_1_tuple, 0, const_int_0 ); Py_INCREF( const_int_0 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_0_int_pos_1_int_pos_1_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_0_int_pos_1_int_pos_1_tuple, 2, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_0_int_pos_1_int_pos_1_tuple, 3, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_tuple_str_plain_Bbox_str_plain_TransformedBbox_str_plain_Affine2D_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Bbox_str_plain_TransformedBbox_str_plain_Affine2D_tuple, 0, const_str_plain_Bbox ); Py_INCREF( const_str_plain_Bbox );
    PyTuple_SET_ITEM( const_tuple_str_plain_Bbox_str_plain_TransformedBbox_str_plain_Affine2D_tuple, 1, const_str_plain_TransformedBbox ); Py_INCREF( const_str_plain_TransformedBbox );
    PyTuple_SET_ITEM( const_tuple_str_plain_Bbox_str_plain_TransformedBbox_str_plain_Affine2D_tuple, 2, const_str_plain_Affine2D ); Py_INCREF( const_str_plain_Affine2D );
    const_tuple_a708240f0215c403d4b1a876d147b73a_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 2574466 ], 174 );
    const_str_plain_process_figure_for_rasterizing = UNSTREAM_STRING_ASCII( &constant_bin[ 2574640 ], 30, 1 );
    const_str_plain_locator_list = UNSTREAM_STRING_ASCII( &constant_bin[ 2574563 ], 12, 1 );
    const_str_digest_a7b1fb505ddedd950c695d3981f07dcf = UNSTREAM_STRING_ASCII( &constant_bin[ 2574670 ], 23, 0 );
    const_str_digest_43609372675c8acdd537a862ad3bf1fd = UNSTREAM_STRING_ASCII( &constant_bin[ 2574693 ], 24, 0 );
    const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple, 0, const_str_plain_fig ); Py_INCREF( const_str_plain_fig );
    PyTuple_SET_ITEM( const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple, 1, const_str_plain_bbox_inches_restore ); Py_INCREF( const_str_plain_bbox_inches_restore );
    PyTuple_SET_ITEM( const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple, 2, const_str_plain_fixed_dpi ); Py_INCREF( const_str_plain_fixed_dpi );
    PyTuple_SET_ITEM( const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple, 3, const_str_plain_bbox_inches ); Py_INCREF( const_str_plain_bbox_inches );
    PyTuple_SET_ITEM( const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple, 4, const_str_plain_restore_bbox ); Py_INCREF( const_str_plain_restore_bbox );
    PyTuple_SET_ITEM( const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple, 5, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    const_str_plain_origBboxInches = UNSTREAM_STRING_ASCII( &constant_bin[ 2574509 ], 14, 1 );
    const_str_plain_origBbox = UNSTREAM_STRING_ASCII( &constant_bin[ 2574499 ], 8, 1 );
    const_tuple_0297d235edf58b17aebb4b75e4796777_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 0, const_str_plain_ax ); Py_INCREF( const_str_plain_ax );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 1, const_str_plain_asp ); Py_INCREF( const_str_plain_asp );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 2, const_str_plain_loc ); Py_INCREF( const_str_plain_loc );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 3, const_str_plain_fig ); Py_INCREF( const_str_plain_fig );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 4, const_str_plain_asp_list ); Py_INCREF( const_str_plain_asp_list );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 5, const_str_plain_locator_list ); Py_INCREF( const_str_plain_locator_list );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 6, const_str_plain_origBbox ); Py_INCREF( const_str_plain_origBbox );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 7, const_str_plain_origBboxInches ); Py_INCREF( const_str_plain_origBboxInches );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 8, const_str_plain_orig_tight_layout ); Py_INCREF( const_str_plain_orig_tight_layout );
    PyTuple_SET_ITEM( const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 9, const_str_plain__boxout ); Py_INCREF( const_str_plain__boxout );
    const_str_digest_b9bbf06547eb6c42fac1b4d1e80b3296 = UNSTREAM_STRING_ASCII( &constant_bin[ 2574717 ], 33, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$tight_bbox( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_dd8c3156a25e808c62f59ac6659603fb;
static PyCodeObject *codeobj_bf5188d2998d603d813f4d26c46de4ed;
static PyCodeObject *codeobj_2e69edf8a6b5225b015cc0a4727fcb31;
static PyCodeObject *codeobj_c1e7424dd759653f413e9a3facd40f2e;
static PyCodeObject *codeobj_f6e1b5da763cc53fd7d3d8cdbbd40228;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_43609372675c8acdd537a862ad3bf1fd );
    codeobj_dd8c3156a25e808c62f59ac6659603fb = MAKE_CODEOBJ( module_filename_obj, const_str_digest_c0c7648ab6c33581e053a1408d758008, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_bf5188d2998d603d813f4d26c46de4ed = MAKE_CODEOBJ( module_filename_obj, const_str_plain__l, 33, const_tuple_str_plain_a_str_plain_r_str_plain_pos_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2e69edf8a6b5225b015cc0a4727fcb31 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_adjust_bbox, 8, const_tuple_a708240f0215c403d4b1a876d147b73a_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c1e7424dd759653f413e9a3facd40f2e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_process_figure_for_rasterizing, 74, const_tuple_bcc50d7b0de01e7fc43079670b120c6e_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f6e1b5da763cc53fd7d3d8cdbbd40228 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_restore_bbox, 38, const_tuple_0297d235edf58b17aebb4b75e4796777_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_1__l( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_2_restore_bbox(  );


static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_2_process_figure_for_rasterizing( PyObject *defaults );


// The module function definitions.
static PyObject *impl_matplotlib$tight_bbox$$$function_1_adjust_bbox( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_fig = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *par_bbox_inches = python_pars[ 1 ];
    PyObject *par_fixed_dpi = python_pars[ 2 ];
    struct Nuitka_CellObject *var_origBbox = PyCell_EMPTY();
    struct Nuitka_CellObject *var_origBboxInches = PyCell_EMPTY();
    struct Nuitka_CellObject *var_orig_tight_layout = PyCell_EMPTY();
    struct Nuitka_CellObject *var__boxout = PyCell_EMPTY();
    struct Nuitka_CellObject *var_asp_list = PyCell_EMPTY();
    struct Nuitka_CellObject *var_locator_list = PyCell_EMPTY();
    PyObject *var_ax = NULL;
    PyObject *var_pos = NULL;
    PyObject *var__l = NULL;
    PyObject *var_restore_bbox = NULL;
    PyObject *var_tr = NULL;
    PyObject *var_dpi_scale = NULL;
    PyObject *var__bbox = NULL;
    PyObject *var_x0 = NULL;
    PyObject *var_y0 = NULL;
    PyObject *var_w1 = NULL;
    PyObject *var_h1 = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_2e69edf8a6b5225b015cc0a4727fcb31;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_2e69edf8a6b5225b015cc0a4727fcb31 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2e69edf8a6b5225b015cc0a4727fcb31, codeobj_2e69edf8a6b5225b015cc0a4727fcb31, module_matplotlib$tight_bbox, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2e69edf8a6b5225b015cc0a4727fcb31 = cache_frame_2e69edf8a6b5225b015cc0a4727fcb31;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2e69edf8a6b5225b015cc0a4727fcb31 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2e69edf8a6b5225b015cc0a4727fcb31 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_1 = PyCell_GET( par_fig );
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_bbox );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_origBbox ) == NULL );
        PyCell_SET( var_origBbox, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_2 = PyCell_GET( par_fig );
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_bbox_inches );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_origBboxInches ) == NULL );
        PyCell_SET( var_origBboxInches, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_called_instance_1 = PyCell_GET( par_fig );
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 21;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_tight_layout );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_orig_tight_layout ) == NULL );
        PyCell_SET( var_orig_tight_layout, tmp_assign_source_3 );

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_4 = PyCell_GET( par_fig );
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_transFigure );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__boxout );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var__boxout ) == NULL );
        PyCell_SET( var__boxout, tmp_assign_source_4 );

    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_called_instance_2 = PyCell_GET( par_fig );
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 24;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_tight_layout, &PyTuple_GET_ITEM( const_tuple_false_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = PyList_New( 0 );
        assert( PyCell_GET( var_asp_list ) == NULL );
        PyCell_SET( var_asp_list, tmp_assign_source_5 );

    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = PyList_New( 0 );
        assert( PyCell_GET( var_locator_list ) == NULL );
        PyCell_SET( var_locator_list, tmp_assign_source_6 );

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_5 = PyCell_GET( par_fig );
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_axes );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_7;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "cooccccccooooooooooo";
                exception_lineno = 28;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_9 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_ax;
            var_ax = tmp_assign_source_9;
            Py_INCREF( var_ax );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( var_ax );
        tmp_source_name_6 = var_ax;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_get_position );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_301f5150c4af32136a6e8de06d709207 );
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 29;
        tmp_called_instance_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 29;
        tmp_assign_source_10 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_frozen );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_pos;
            var_pos = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( PyCell_GET( var_locator_list ) );
        tmp_source_name_7 = PyCell_GET( var_locator_list );
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_append );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_ax );
        tmp_called_instance_4 = var_ax;
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 30;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_get_axes_locator );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 30;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 30;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_8;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_5;
        CHECK_OBJECT( PyCell_GET( var_asp_list ) );
        tmp_source_name_8 = PyCell_GET( var_asp_list );
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_append );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_ax );
        tmp_called_instance_5 = var_ax;
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 31;
        tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_get_aspect );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 31;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 31;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_defaults_1;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( var_pos );
        tmp_tuple_element_1 = var_pos;
        tmp_defaults_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
        tmp_assign_source_11 = MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_1__l( tmp_defaults_1 );



        {
            PyObject *old = var__l;
            var__l = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_6;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( var_ax );
        tmp_called_instance_6 = var_ax;
        CHECK_OBJECT( var__l );
        tmp_args_element_name_3 = var__l;
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 35;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_set_axes_locator, call_args );
        }

        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_instance_7;
        PyObject *tmp_call_result_5;
        CHECK_OBJECT( var_ax );
        tmp_called_instance_7 = var_ax;
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 36;
        tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_set_aspect, &PyTuple_GET_ITEM( const_tuple_str_plain_auto_tuple, 0 ) );

        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 28;
        type_description_1 = "cooccccccooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_2_restore_bbox(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[0] = var__boxout;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[1] = var_asp_list;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[2] = par_fig;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[3] = var_locator_list;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[3] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[4] = var_origBbox;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[4] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[5] = var_origBboxInches;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[5] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[6] = var_orig_tight_layout;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_12)->m_closure[6] );


        assert( var_restore_bbox == NULL );
        var_restore_bbox = tmp_assign_source_12;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_fixed_dpi );
        tmp_compexpr_left_1 = par_fixed_dpi;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_called_instance_8;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_4;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_Affine2D );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Affine2D );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Affine2D" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 51;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_4 = tmp_mvar_value_1;
            frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 51;
            tmp_called_instance_8 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
            if ( tmp_called_instance_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_fixed_dpi );
            tmp_args_element_name_4 = par_fixed_dpi;
            frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 51;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_assign_source_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_scale, call_args );
            }

            Py_DECREF( tmp_called_instance_8 );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_tr == NULL );
            var_tr = tmp_assign_source_13;
        }
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( par_fixed_dpi );
            tmp_left_name_1 = par_fixed_dpi;
            CHECK_OBJECT( PyCell_GET( par_fig ) );
            tmp_source_name_9 = PyCell_GET( par_fig );
            tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_dpi );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_14 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_assign_source_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_dpi_scale == NULL );
            var_dpi_scale = tmp_assign_source_14;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_10;
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_source_name_11;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_Affine2D );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Affine2D );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Affine2D" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 54;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_6 = tmp_mvar_value_2;
            frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 54;
            tmp_source_name_10 = CALL_FUNCTION_NO_ARGS( tmp_called_name_6 );
            if ( tmp_source_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_scale );
            Py_DECREF( tmp_source_name_10 );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( PyCell_GET( par_fig ) );
            tmp_source_name_11 = PyCell_GET( par_fig );
            tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_dpi );
            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 54;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 54;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_assign_source_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;
                type_description_1 = "cooccccccooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_tr == NULL );
            var_tr = tmp_assign_source_15;
        }
        {
            PyObject *tmp_assign_source_16;
            tmp_assign_source_16 = const_float_1_0;
            assert( var_dpi_scale == NULL );
            Py_INCREF( tmp_assign_source_16 );
            var_dpi_scale = tmp_assign_source_16;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_TransformedBbox );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TransformedBbox );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TransformedBbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_3;
        CHECK_OBJECT( par_bbox_inches );
        tmp_args_element_name_6 = par_bbox_inches;
        CHECK_OBJECT( var_tr );
        tmp_args_element_name_7 = var_tr;
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var__bbox == NULL );
        var__bbox = tmp_assign_source_17;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_8;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_source_name_13;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_source_name_14;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_Bbox );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Bbox );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Bbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_4;
        tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_from_bounds );
        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_8 = const_int_0;
        tmp_args_element_name_9 = const_int_0;
        CHECK_OBJECT( par_bbox_inches );
        tmp_source_name_13 = par_bbox_inches;
        tmp_args_element_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_width );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 60;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_bbox_inches );
        tmp_source_name_14 = par_bbox_inches;
        tmp_args_element_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_height );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_10 );

            exception_lineno = 60;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 59;
        {
            PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_element_name_10 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_assattr_target_1 = PyCell_GET( par_fig );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_bbox_inches, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_source_name_15;
        PyObject *tmp_source_name_16;
        CHECK_OBJECT( var__bbox );
        tmp_source_name_15 = var__bbox;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_x0 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_3;
        }
        tmp_iter_arg_2 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_iter_arg_2, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( var__bbox );
        tmp_source_name_16 = var__bbox;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_y0 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_iter_arg_2 );

            exception_lineno = 61;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_3;
        }
        PyTuple_SET_ITEM( tmp_iter_arg_2, 1, tmp_tuple_element_2 );
        tmp_assign_source_18 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_18;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_19 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "cooccccccooooooooooo";
            exception_lineno = 61;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_19;
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_20 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_20 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "cooccccccooooooooooo";
            exception_lineno = 61;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_20;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_21;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_21 = tmp_tuple_unpack_1__element_1;
        assert( var_x0 == NULL );
        Py_INCREF( tmp_assign_source_21 );
        var_x0 = tmp_assign_source_21;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_22;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_22 = tmp_tuple_unpack_1__element_2;
        assert( var_y0 == NULL );
        Py_INCREF( tmp_assign_source_22 );
        var_y0 = tmp_assign_source_22;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_left_name_2;
        PyObject *tmp_source_name_17;
        PyObject *tmp_source_name_18;
        PyObject *tmp_right_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_source_name_19;
        PyObject *tmp_source_name_20;
        PyObject *tmp_right_name_3;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_18 = PyCell_GET( par_fig );
        tmp_source_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_bbox );
        if ( tmp_source_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_5;
        }
        tmp_left_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_width );
        Py_DECREF( tmp_source_name_17 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_5;
        }
        CHECK_OBJECT( var_dpi_scale );
        tmp_right_name_2 = var_dpi_scale;
        tmp_tuple_element_3 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_5;
        }
        tmp_iter_arg_3 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_iter_arg_3, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_20 = PyCell_GET( par_fig );
        tmp_source_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_bbox );
        if ( tmp_source_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_iter_arg_3 );

            exception_lineno = 62;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_5;
        }
        tmp_left_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_height );
        Py_DECREF( tmp_source_name_19 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_iter_arg_3 );

            exception_lineno = 62;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_5;
        }
        CHECK_OBJECT( var_dpi_scale );
        tmp_right_name_3 = var_dpi_scale;
        tmp_tuple_element_3 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_3 );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_iter_arg_3 );

            exception_lineno = 62;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_5;
        }
        PyTuple_SET_ITEM( tmp_iter_arg_3, 1, tmp_tuple_element_3 );
        tmp_assign_source_23 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "cooccccccooooooooooo";
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_2__source_iter == NULL );
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_23;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_24 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_24 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "cooccccccooooooooooo";
            exception_lineno = 62;
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_2__element_1 == NULL );
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_25 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_25 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "cooccccccooooooooooo";
            exception_lineno = 62;
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_2__element_2 == NULL );
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_25;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_5;
    // End of try:
    try_end_4:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_26;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_26 = tmp_tuple_unpack_2__element_1;
        assert( var_w1 == NULL );
        Py_INCREF( tmp_assign_source_26 );
        var_w1 = tmp_assign_source_26;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_27;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_27 = tmp_tuple_unpack_2__element_2;
        assert( var_h1 == NULL );
        Py_INCREF( tmp_assign_source_27 );
        var_h1 = tmp_assign_source_27;
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_21;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_source_name_22;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_Bbox );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Bbox );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Bbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_21 = tmp_mvar_value_5;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_from_bounds );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_x0 );
        tmp_operand_name_1 = var_x0;
        tmp_args_element_name_12 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );

            exception_lineno = 63;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_y0 );
        tmp_operand_name_2 = var_y0;
        tmp_args_element_name_13 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_2 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_element_name_12 );

            exception_lineno = 63;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_w1 );
        tmp_args_element_name_14 = var_w1;
        CHECK_OBJECT( var_h1 );
        tmp_args_element_name_15 = var_h1;
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15 };
            tmp_assattr_name_2 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_12 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_22 = PyCell_GET( par_fig );
        tmp_assattr_target_2 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_transFigure );
        if ( tmp_assattr_target_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_2 );

            exception_lineno = 63;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__boxout, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_target_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_9;
        PyObject *tmp_source_name_23;
        PyObject *tmp_call_result_6;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_23 = PyCell_GET( par_fig );
        tmp_called_instance_9 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_transFigure );
        if ( tmp_called_instance_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 64;
        tmp_call_result_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_invalidate );
        Py_DECREF( tmp_called_instance_9 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_source_name_24;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_assattr_target_3;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_TransformedBbox );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TransformedBbox );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TransformedBbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_6;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_24 = PyCell_GET( par_fig );
        tmp_args_element_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_bbox_inches );
        if ( tmp_args_element_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tr );
        tmp_args_element_name_17 = var_tr;
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_args_element_name_16, tmp_args_element_name_17 };
            tmp_assattr_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_assattr_target_3 = PyCell_GET( par_fig );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_bbox, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_11;
        PyObject *tmp_source_name_25;
        PyObject *tmp_source_name_26;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_5;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_left_name_6;
        PyObject *tmp_source_name_27;
        PyObject *tmp_source_name_28;
        PyObject *tmp_right_name_6;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_left_name_7;
        PyObject *tmp_source_name_29;
        PyObject *tmp_source_name_30;
        PyObject *tmp_right_name_7;
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_26 = PyCell_GET( par_fig );
        tmp_source_name_25 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_patch );
        if ( tmp_source_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_set_bounds );
        Py_DECREF( tmp_source_name_25 );
        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_x0 );
        tmp_left_name_4 = var_x0;
        CHECK_OBJECT( var_w1 );
        tmp_right_name_4 = var_w1;
        tmp_args_element_name_18 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_4 );
        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );

            exception_lineno = 68;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_y0 );
        tmp_left_name_5 = var_y0;
        CHECK_OBJECT( var_h1 );
        tmp_right_name_5 = var_h1;
        tmp_args_element_name_19 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_5 );
        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_18 );

            exception_lineno = 68;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_28 = PyCell_GET( par_fig );
        tmp_source_name_27 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_bbox );
        if ( tmp_source_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 69;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_width );
        Py_DECREF( tmp_source_name_27 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 69;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_w1 );
        tmp_right_name_6 = var_w1;
        tmp_args_element_name_20 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_6 );
        Py_DECREF( tmp_left_name_6 );
        if ( tmp_args_element_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 69;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_fig ) );
        tmp_source_name_30 = PyCell_GET( par_fig );
        tmp_source_name_29 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_bbox );
        if ( tmp_source_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_20 );

            exception_lineno = 69;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_height );
        Py_DECREF( tmp_source_name_29 );
        if ( tmp_left_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_20 );

            exception_lineno = 69;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_h1 );
        tmp_right_name_7 = var_h1;
        tmp_args_element_name_21 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_7 );
        Py_DECREF( tmp_left_name_7 );
        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_20 );

            exception_lineno = 69;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame.f_lineno = 68;
        {
            PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19, tmp_args_element_name_20, tmp_args_element_name_21 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        Py_DECREF( tmp_args_element_name_18 );
        Py_DECREF( tmp_args_element_name_19 );
        Py_DECREF( tmp_args_element_name_20 );
        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "cooccccccooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e69edf8a6b5225b015cc0a4727fcb31 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e69edf8a6b5225b015cc0a4727fcb31 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2e69edf8a6b5225b015cc0a4727fcb31, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2e69edf8a6b5225b015cc0a4727fcb31->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2e69edf8a6b5225b015cc0a4727fcb31, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2e69edf8a6b5225b015cc0a4727fcb31,
        type_description_1,
        par_fig,
        par_bbox_inches,
        par_fixed_dpi,
        var_origBbox,
        var_origBboxInches,
        var_orig_tight_layout,
        var__boxout,
        var_asp_list,
        var_locator_list,
        var_ax,
        var_pos,
        var__l,
        var_restore_bbox,
        var_tr,
        var_dpi_scale,
        var__bbox,
        var_x0,
        var_y0,
        var_w1,
        var_h1
    );


    // Release cached frame.
    if ( frame_2e69edf8a6b5225b015cc0a4727fcb31 == cache_frame_2e69edf8a6b5225b015cc0a4727fcb31 )
    {
        Py_DECREF( frame_2e69edf8a6b5225b015cc0a4727fcb31 );
    }
    cache_frame_2e69edf8a6b5225b015cc0a4727fcb31 = NULL;

    assertFrameObject( frame_2e69edf8a6b5225b015cc0a4727fcb31 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_restore_bbox );
    tmp_return_value = var_restore_bbox;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_1_adjust_bbox );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_bbox_inches );
    Py_DECREF( par_bbox_inches );
    par_bbox_inches = NULL;

    CHECK_OBJECT( (PyObject *)par_fixed_dpi );
    Py_DECREF( par_fixed_dpi );
    par_fixed_dpi = NULL;

    CHECK_OBJECT( (PyObject *)var_origBbox );
    Py_DECREF( var_origBbox );
    var_origBbox = NULL;

    CHECK_OBJECT( (PyObject *)var_origBboxInches );
    Py_DECREF( var_origBboxInches );
    var_origBboxInches = NULL;

    CHECK_OBJECT( (PyObject *)var_orig_tight_layout );
    Py_DECREF( var_orig_tight_layout );
    var_orig_tight_layout = NULL;

    CHECK_OBJECT( (PyObject *)var__boxout );
    Py_DECREF( var__boxout );
    var__boxout = NULL;

    CHECK_OBJECT( (PyObject *)var_asp_list );
    Py_DECREF( var_asp_list );
    var_asp_list = NULL;

    CHECK_OBJECT( (PyObject *)var_locator_list );
    Py_DECREF( var_locator_list );
    var_locator_list = NULL;

    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var__l );
    var__l = NULL;

    CHECK_OBJECT( (PyObject *)var_restore_bbox );
    Py_DECREF( var_restore_bbox );
    var_restore_bbox = NULL;

    CHECK_OBJECT( (PyObject *)var_tr );
    Py_DECREF( var_tr );
    var_tr = NULL;

    CHECK_OBJECT( (PyObject *)var_dpi_scale );
    Py_DECREF( var_dpi_scale );
    var_dpi_scale = NULL;

    CHECK_OBJECT( (PyObject *)var__bbox );
    Py_DECREF( var__bbox );
    var__bbox = NULL;

    CHECK_OBJECT( (PyObject *)var_x0 );
    Py_DECREF( var_x0 );
    var_x0 = NULL;

    CHECK_OBJECT( (PyObject *)var_y0 );
    Py_DECREF( var_y0 );
    var_y0 = NULL;

    CHECK_OBJECT( (PyObject *)var_w1 );
    Py_DECREF( var_w1 );
    var_w1 = NULL;

    CHECK_OBJECT( (PyObject *)var_h1 );
    Py_DECREF( var_h1 );
    var_h1 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_bbox_inches );
    Py_DECREF( par_bbox_inches );
    par_bbox_inches = NULL;

    CHECK_OBJECT( (PyObject *)par_fixed_dpi );
    Py_DECREF( par_fixed_dpi );
    par_fixed_dpi = NULL;

    CHECK_OBJECT( (PyObject *)var_origBbox );
    Py_DECREF( var_origBbox );
    var_origBbox = NULL;

    CHECK_OBJECT( (PyObject *)var_origBboxInches );
    Py_DECREF( var_origBboxInches );
    var_origBboxInches = NULL;

    CHECK_OBJECT( (PyObject *)var_orig_tight_layout );
    Py_DECREF( var_orig_tight_layout );
    var_orig_tight_layout = NULL;

    CHECK_OBJECT( (PyObject *)var__boxout );
    Py_DECREF( var__boxout );
    var__boxout = NULL;

    CHECK_OBJECT( (PyObject *)var_asp_list );
    Py_DECREF( var_asp_list );
    var_asp_list = NULL;

    CHECK_OBJECT( (PyObject *)var_locator_list );
    Py_DECREF( var_locator_list );
    var_locator_list = NULL;

    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var__l );
    var__l = NULL;

    Py_XDECREF( var_restore_bbox );
    var_restore_bbox = NULL;

    Py_XDECREF( var_tr );
    var_tr = NULL;

    Py_XDECREF( var_dpi_scale );
    var_dpi_scale = NULL;

    Py_XDECREF( var__bbox );
    var__bbox = NULL;

    Py_XDECREF( var_x0 );
    var_x0 = NULL;

    Py_XDECREF( var_y0 );
    var_y0 = NULL;

    Py_XDECREF( var_w1 );
    var_w1 = NULL;

    Py_XDECREF( var_h1 );
    var_h1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_1_adjust_bbox );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_1__l( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_a = python_pars[ 0 ];
    PyObject *par_r = python_pars[ 1 ];
    PyObject *par_pos = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_pos );
    tmp_return_value = par_pos;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_1__l );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_pos );
    Py_DECREF( par_pos );
    par_pos = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_1__l );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_2_restore_bbox( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_ax = NULL;
    PyObject *var_asp = NULL;
    PyObject *var_loc = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_f6e1b5da763cc53fd7d3d8cdbbd40228;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_f6e1b5da763cc53fd7d3d8cdbbd40228 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f6e1b5da763cc53fd7d3d8cdbbd40228, codeobj_f6e1b5da763cc53fd7d3d8cdbbd40228, module_matplotlib$tight_bbox, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f6e1b5da763cc53fd7d3d8cdbbd40228 = cache_frame_f6e1b5da763cc53fd7d3d8cdbbd40228;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f6e1b5da763cc53fd7d3d8cdbbd40228 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f6e1b5da763cc53fd7d3d8cdbbd40228 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_called_name_1 = (PyObject *)&PyZip_Type;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[2] );
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_axes );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "asp_list" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( self->m_closure[1] );
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "locator_list" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( self->m_closure[3] );
        frame_f6e1b5da763cc53fd7d3d8cdbbd40228->m_frame.f_lineno = 39;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooccccccc";
                exception_lineno = 39;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oooccccccc";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooccccccc";
            exception_lineno = 39;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooccccccc";
            exception_lineno = 39;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooccccccc";
            exception_lineno = 39;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_3;
            tmp_tuple_unpack_1__element_3 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooccccccc";
                    exception_lineno = 39;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooccccccc";
            exception_lineno = 39;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_ax;
            var_ax = tmp_assign_source_7;
            Py_INCREF( var_ax );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_asp;
            var_asp = tmp_assign_source_8;
            Py_INCREF( var_asp );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_3;
        {
            PyObject *old = var_loc;
            var_loc = tmp_assign_source_9;
            Py_INCREF( var_loc );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( var_ax );
        tmp_called_instance_1 = var_ax;
        CHECK_OBJECT( var_asp );
        tmp_args_element_name_4 = var_asp;
        frame_f6e1b5da763cc53fd7d3d8cdbbd40228->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_set_aspect, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "oooccccccc";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( var_ax );
        tmp_called_instance_2 = var_ax;
        CHECK_OBJECT( var_loc );
        tmp_args_element_name_5 = var_loc;
        frame_f6e1b5da763cc53fd7d3d8cdbbd40228->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_axes_locator, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "oooccccccc";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 39;
        type_description_1 = "oooccccccc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        if ( PyCell_GET( self->m_closure[4] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "origBbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_name_1 = PyCell_GET( self->m_closure[4] );
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = PyCell_GET( self->m_closure[2] );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_bbox, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        if ( PyCell_GET( self->m_closure[5] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "origBboxInches" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_name_2 = PyCell_GET( self->m_closure[5] );
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_2 = PyCell_GET( self->m_closure[2] );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_bbox_inches, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_6;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( self->m_closure[2] );
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_set_tight_layout );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[6] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "orig_tight_layout" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = PyCell_GET( self->m_closure[6] );
        frame_f6e1b5da763cc53fd7d3d8cdbbd40228->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_source_name_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "_boxout" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_name_3 = PyCell_GET( self->m_closure[0] );
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( self->m_closure[2] );
        tmp_assattr_target_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_transFigure );
        if ( tmp_assattr_target_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__boxout, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_target_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_4;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = PyCell_GET( self->m_closure[2] );
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_transFigure );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        frame_f6e1b5da763cc53fd7d3d8cdbbd40228->m_frame.f_lineno = 47;
        tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_invalidate );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_call_result_5;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = PyCell_GET( self->m_closure[2] );
        tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_patch );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        frame_f6e1b5da763cc53fd7d3d8cdbbd40228->m_frame.f_lineno = 48;
        tmp_call_result_5 = CALL_METHOD_WITH_ARGS4( tmp_called_instance_4, const_str_plain_set_bounds, &PyTuple_GET_ITEM( const_tuple_int_0_int_0_int_pos_1_int_pos_1_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooccccccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f6e1b5da763cc53fd7d3d8cdbbd40228 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f6e1b5da763cc53fd7d3d8cdbbd40228 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f6e1b5da763cc53fd7d3d8cdbbd40228, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f6e1b5da763cc53fd7d3d8cdbbd40228->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f6e1b5da763cc53fd7d3d8cdbbd40228, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f6e1b5da763cc53fd7d3d8cdbbd40228,
        type_description_1,
        var_ax,
        var_asp,
        var_loc,
        self->m_closure[2],
        self->m_closure[1],
        self->m_closure[3],
        self->m_closure[4],
        self->m_closure[5],
        self->m_closure[6],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_f6e1b5da763cc53fd7d3d8cdbbd40228 == cache_frame_f6e1b5da763cc53fd7d3d8cdbbd40228 )
    {
        Py_DECREF( frame_f6e1b5da763cc53fd7d3d8cdbbd40228 );
    }
    cache_frame_f6e1b5da763cc53fd7d3d8cdbbd40228 = NULL;

    assertFrameObject( frame_f6e1b5da763cc53fd7d3d8cdbbd40228 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_2_restore_bbox );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_asp );
    var_asp = NULL;

    Py_XDECREF( var_loc );
    var_loc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_ax );
    var_ax = NULL;

    Py_XDECREF( var_asp );
    var_asp = NULL;

    Py_XDECREF( var_loc );
    var_loc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_2_restore_bbox );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$tight_bbox$$$function_2_process_figure_for_rasterizing( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fig = python_pars[ 0 ];
    PyObject *par_bbox_inches_restore = python_pars[ 1 ];
    PyObject *par_fixed_dpi = python_pars[ 2 ];
    PyObject *var_bbox_inches = NULL;
    PyObject *var_restore_bbox = NULL;
    PyObject *var_r = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_c1e7424dd759653f413e9a3facd40f2e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_c1e7424dd759653f413e9a3facd40f2e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c1e7424dd759653f413e9a3facd40f2e, codeobj_c1e7424dd759653f413e9a3facd40f2e, module_matplotlib$tight_bbox, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c1e7424dd759653f413e9a3facd40f2e = cache_frame_c1e7424dd759653f413e9a3facd40f2e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c1e7424dd759653f413e9a3facd40f2e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c1e7424dd759653f413e9a3facd40f2e ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_bbox_inches_restore );
        tmp_iter_arg_1 = par_bbox_inches_restore;
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 81;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 81;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 81;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 81;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_bbox_inches == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_bbox_inches = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_restore_bbox == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_restore_bbox = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_restore_bbox );
        tmp_called_name_1 = var_restore_bbox;
        frame_c1e7424dd759653f413e9a3facd40f2e->m_frame.f_lineno = 82;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_adjust_bbox );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_adjust_bbox );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "adjust_bbox" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( par_fig );
        tmp_args_element_name_1 = par_fig;
        CHECK_OBJECT( var_bbox_inches );
        tmp_args_element_name_2 = var_bbox_inches;
        CHECK_OBJECT( par_fixed_dpi );
        tmp_args_element_name_3 = par_fixed_dpi;
        frame_c1e7424dd759653f413e9a3facd40f2e->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_r == NULL );
        var_r = tmp_assign_source_6;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c1e7424dd759653f413e9a3facd40f2e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c1e7424dd759653f413e9a3facd40f2e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c1e7424dd759653f413e9a3facd40f2e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c1e7424dd759653f413e9a3facd40f2e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c1e7424dd759653f413e9a3facd40f2e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c1e7424dd759653f413e9a3facd40f2e,
        type_description_1,
        par_fig,
        par_bbox_inches_restore,
        par_fixed_dpi,
        var_bbox_inches,
        var_restore_bbox,
        var_r
    );


    // Release cached frame.
    if ( frame_c1e7424dd759653f413e9a3facd40f2e == cache_frame_c1e7424dd759653f413e9a3facd40f2e )
    {
        Py_DECREF( frame_c1e7424dd759653f413e9a3facd40f2e );
    }
    cache_frame_c1e7424dd759653f413e9a3facd40f2e = NULL;

    assertFrameObject( frame_c1e7424dd759653f413e9a3facd40f2e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( var_bbox_inches );
        tmp_tuple_element_1 = var_bbox_inches;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_r );
        tmp_tuple_element_1 = var_r;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_2_process_figure_for_rasterizing );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_bbox_inches_restore );
    Py_DECREF( par_bbox_inches_restore );
    par_bbox_inches_restore = NULL;

    CHECK_OBJECT( (PyObject *)par_fixed_dpi );
    Py_DECREF( par_fixed_dpi );
    par_fixed_dpi = NULL;

    CHECK_OBJECT( (PyObject *)var_bbox_inches );
    Py_DECREF( var_bbox_inches );
    var_bbox_inches = NULL;

    CHECK_OBJECT( (PyObject *)var_restore_bbox );
    Py_DECREF( var_restore_bbox );
    var_restore_bbox = NULL;

    CHECK_OBJECT( (PyObject *)var_r );
    Py_DECREF( var_r );
    var_r = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    CHECK_OBJECT( (PyObject *)par_bbox_inches_restore );
    Py_DECREF( par_bbox_inches_restore );
    par_bbox_inches_restore = NULL;

    CHECK_OBJECT( (PyObject *)par_fixed_dpi );
    Py_DECREF( par_fixed_dpi );
    par_fixed_dpi = NULL;

    Py_XDECREF( var_bbox_inches );
    var_bbox_inches = NULL;

    Py_XDECREF( var_restore_bbox );
    var_restore_bbox = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tight_bbox$$$function_2_process_figure_for_rasterizing );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_bbox$$$function_1_adjust_bbox,
        const_str_plain_adjust_bbox,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2e69edf8a6b5225b015cc0a4727fcb31,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_bbox,
        const_str_digest_d7f35a5205290c08f17a9a7bba78906a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_1__l( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_1__l,
        const_str_plain__l,
#if PYTHON_VERSION >= 300
        const_str_digest_a7b1fb505ddedd950c695d3981f07dcf,
#endif
        codeobj_bf5188d2998d603d813f4d26c46de4ed,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_bbox,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_2_restore_bbox(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_bbox$$$function_1_adjust_bbox$$$function_2_restore_bbox,
        const_str_plain_restore_bbox,
#if PYTHON_VERSION >= 300
        const_str_digest_b9bbf06547eb6c42fac1b4d1e80b3296,
#endif
        codeobj_f6e1b5da763cc53fd7d3d8cdbbd40228,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_bbox,
        NULL,
        7
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$tight_bbox$$$function_2_process_figure_for_rasterizing( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tight_bbox$$$function_2_process_figure_for_rasterizing,
        const_str_plain_process_figure_for_rasterizing,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c1e7424dd759653f413e9a3facd40f2e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$tight_bbox,
        const_str_digest_82f249f3cba9d36a5f33710e9ace7b3d,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$tight_bbox =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib.tight_bbox",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$tight_bbox)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$tight_bbox)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$tight_bbox );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib.tight_bbox: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.tight_bbox: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.tight_bbox: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$tight_bbox" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$tight_bbox = Py_InitModule4(
        "matplotlib.tight_bbox",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$tight_bbox = PyModule_Create( &mdef_matplotlib$tight_bbox );
#endif

    moduledict_matplotlib$tight_bbox = MODULE_DICT( module_matplotlib$tight_bbox );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$tight_bbox,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$tight_bbox,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$tight_bbox,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$tight_bbox,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$tight_bbox );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_6e372b2cd024bd000a83aa3f86ea0a36, module_matplotlib$tight_bbox );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_dd8c3156a25e808c62f59ac6659603fb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_6a2fe96210f519e35d1f97234c2ccd42;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_dd8c3156a25e808c62f59ac6659603fb = MAKE_MODULE_FRAME( codeobj_dd8c3156a25e808c62f59ac6659603fb, module_matplotlib$tight_bbox );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_dd8c3156a25e808c62f59ac6659603fb );
    assert( Py_REFCNT( frame_dd8c3156a25e808c62f59ac6659603fb ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_d28745151750b44e549d12f294293c52;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$tight_bbox;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Bbox_str_plain_TransformedBbox_str_plain_Affine2D_tuple;
        tmp_level_name_1 = const_int_0;
        frame_dd8c3156a25e808c62f59ac6659603fb->m_frame.f_lineno = 5;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Bbox );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_Bbox, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_TransformedBbox );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_TransformedBbox, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_Affine2D );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_Affine2D, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dd8c3156a25e808c62f59ac6659603fb );
#endif
    popFrameStack();

    assertFrameObject( frame_dd8c3156a25e808c62f59ac6659603fb );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dd8c3156a25e808c62f59ac6659603fb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dd8c3156a25e808c62f59ac6659603fb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dd8c3156a25e808c62f59ac6659603fb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dd8c3156a25e808c62f59ac6659603fb, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_8 = MAKE_FUNCTION_matplotlib$tight_bbox$$$function_1_adjust_bbox( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_adjust_bbox, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_9 = MAKE_FUNCTION_matplotlib$tight_bbox$$$function_2_process_figure_for_rasterizing( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tight_bbox, (Nuitka_StringObject *)const_str_plain_process_figure_for_rasterizing, tmp_assign_source_9 );
    }

    return MOD_RETURN_VALUE( module_matplotlib$tight_bbox );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
