/* Generated code for Python module 'nbconvert.filters.markdown'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_nbconvert$filters$markdown" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_nbconvert$filters$markdown;
PyDictObject *moduledict_nbconvert$filters$markdown;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_none_tuple;
static PyObject *const_list_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7_list;
extern PyObject *const_str_plain_rst;
extern PyObject *const_str_plain_source;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_74e88f13430ac243f510936fb4ef17ff;
extern PyObject *const_str_plain_markdown2latex;
extern PyObject *const_str_plain_markdown2rst;
static PyObject *const_str_digest_1ee7ce96fcdc099d36a72e7821e96928;
extern PyObject *const_tuple_str_plain_source_tuple;
extern PyObject *const_str_plain_asciidoc;
static PyObject *const_str_digest_4b639991ebbb2622fd952a725efa060c;
static PyObject *const_str_digest_2bcc2ae198887f4979e57e86c26f77a3;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_plain_markdown2html_pandoc;
static PyObject *const_str_digest_5945a0ad7959b19255e86c493c87fb9b;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_markup;
static PyObject *const_str_digest_542ff9b6d089e7cf951abb26781c3d41;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_extra_args;
extern PyObject *const_str_plain___;
static PyObject *const_str_digest_cd2fbac026251ec6bc6b6df435989d44;
extern PyObject *const_str_plain_markdown2html_mistune;
static PyObject *const_tuple_str_plain_source_str_plain_extra_args_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_markdown2asciidoc;
static PyObject *const_tuple_str_plain_convert_pandoc_tuple;
static PyObject *const_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7;
static PyObject *const_str_plain_markdown_mistune;
static PyObject *const_list_973c7019e71af81eaa3da26f1753d0ae_list;
static PyObject *const_str_digest_d214d91f77fddc6708f2f0452c3b07f2;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_pandoc;
extern PyObject *const_str_plain_print_function;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_27190bd4d7f53af1c7d8d10126eff5e2;
static PyObject *const_str_digest_fe3ab07ff2922de96e37607f5cb301eb;
extern PyObject *const_str_plain_sub;
static PyObject *const_tuple_str_plain_markdown_none_tuple;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_html;
static PyObject *const_str_digest_3d8a6a59dab71c6bbfb587ed74addd8b;
extern PyObject *const_str_plain_markdown2html;
static PyObject *const_tuple_str_plain_source_str_plain_extra_args_str_plain_asciidoc_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_23646ce451c1abfc834323dfd9c54177;
static PyObject *const_tuple_str_plain_source_str_plain_markup_str_plain_extra_args_tuple;
static PyObject *const_str_digest_b8b38ac436c6a055ae769839c2986323;
extern PyObject *const_str_plain_convert_pandoc;
extern PyObject *const_str_plain_latex;
static PyObject *const_list_str_digest_1ee7ce96fcdc099d36a72e7821e96928_list;
static PyObject *const_str_digest_8840b2d72e52a4388a239fdaa17f0f10;
extern PyObject *const_str_plain_markdown;
static PyObject *const_str_digest_02df46b9435fd3398a0386c532745a64;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain__mistune_import_error;
static PyObject *const_tuple_str_plain_markdown2html_mistune_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_list_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7_list = PyList_New( 1 );
    const_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7 = UNSTREAM_STRING_ASCII( &constant_bin[ 2751897 ], 13, 0 );
    PyList_SET_ITEM( const_list_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7_list, 0, const_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7 ); Py_INCREF( const_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7 );
    const_str_digest_74e88f13430ac243f510936fb4ef17ff = UNSTREAM_STRING_ASCII( &constant_bin[ 2751910 ], 28, 0 );
    const_str_digest_1ee7ce96fcdc099d36a72e7821e96928 = UNSTREAM_STRING_ASCII( &constant_bin[ 2751938 ], 9, 0 );
    const_str_digest_4b639991ebbb2622fd952a725efa060c = UNSTREAM_STRING_ASCII( &constant_bin[ 2751947 ], 6, 0 );
    const_str_digest_2bcc2ae198887f4979e57e86c26f77a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2751953 ], 55, 0 );
    const_str_plain_markdown2html_pandoc = UNSTREAM_STRING_ASCII( &constant_bin[ 2752008 ], 20, 1 );
    const_str_digest_5945a0ad7959b19255e86c493c87fb9b = UNSTREAM_STRING_ASCII( &constant_bin[ 2752028 ], 34, 0 );
    const_str_digest_542ff9b6d089e7cf951abb26781c3d41 = UNSTREAM_STRING_ASCII( &constant_bin[ 2752062 ], 41, 0 );
    const_str_digest_cd2fbac026251ec6bc6b6df435989d44 = UNSTREAM_STRING_ASCII( &constant_bin[ 2752103 ], 6, 0 );
    const_tuple_str_plain_source_str_plain_extra_args_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_extra_args_tuple, 0, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_extra_args_tuple, 1, const_str_plain_extra_args ); Py_INCREF( const_str_plain_extra_args );
    const_tuple_str_plain_convert_pandoc_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_convert_pandoc_tuple, 0, const_str_plain_convert_pandoc ); Py_INCREF( const_str_plain_convert_pandoc );
    const_str_plain_markdown_mistune = UNSTREAM_STRING_ASCII( &constant_bin[ 2752109 ], 16, 1 );
    const_list_973c7019e71af81eaa3da26f1753d0ae_list = PyList_New( 6 );
    PyList_SET_ITEM( const_list_973c7019e71af81eaa3da26f1753d0ae_list, 0, const_str_plain_markdown2html ); Py_INCREF( const_str_plain_markdown2html );
    PyList_SET_ITEM( const_list_973c7019e71af81eaa3da26f1753d0ae_list, 1, const_str_plain_markdown2html_pandoc ); Py_INCREF( const_str_plain_markdown2html_pandoc );
    PyList_SET_ITEM( const_list_973c7019e71af81eaa3da26f1753d0ae_list, 2, const_str_plain_markdown2html_mistune ); Py_INCREF( const_str_plain_markdown2html_mistune );
    PyList_SET_ITEM( const_list_973c7019e71af81eaa3da26f1753d0ae_list, 3, const_str_plain_markdown2latex ); Py_INCREF( const_str_plain_markdown2latex );
    PyList_SET_ITEM( const_list_973c7019e71af81eaa3da26f1753d0ae_list, 4, const_str_plain_markdown2rst ); Py_INCREF( const_str_plain_markdown2rst );
    PyList_SET_ITEM( const_list_973c7019e71af81eaa3da26f1753d0ae_list, 5, const_str_plain_markdown2asciidoc ); Py_INCREF( const_str_plain_markdown2asciidoc );
    const_str_digest_d214d91f77fddc6708f2f0452c3b07f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2752125 ], 119, 0 );
    const_str_digest_27190bd4d7f53af1c7d8d10126eff5e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2752244 ], 35, 0 );
    const_str_digest_fe3ab07ff2922de96e37607f5cb301eb = UNSTREAM_STRING_ASCII( &constant_bin[ 2752279 ], 48, 0 );
    const_tuple_str_plain_markdown_none_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_markdown_none_tuple, 0, const_str_plain_markdown ); Py_INCREF( const_str_plain_markdown );
    PyTuple_SET_ITEM( const_tuple_str_plain_markdown_none_tuple, 1, Py_None ); Py_INCREF( Py_None );
    const_str_digest_3d8a6a59dab71c6bbfb587ed74addd8b = UNSTREAM_STRING_ASCII( &constant_bin[ 2752327 ], 367, 0 );
    const_tuple_str_plain_source_str_plain_extra_args_str_plain_asciidoc_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_extra_args_str_plain_asciidoc_tuple, 0, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_extra_args_str_plain_asciidoc_tuple, 1, const_str_plain_extra_args ); Py_INCREF( const_str_plain_extra_args );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_extra_args_str_plain_asciidoc_tuple, 2, const_str_plain_asciidoc ); Py_INCREF( const_str_plain_asciidoc );
    const_str_digest_23646ce451c1abfc834323dfd9c54177 = UNSTREAM_STRING_ASCII( &constant_bin[ 2752694 ], 525, 0 );
    const_tuple_str_plain_source_str_plain_markup_str_plain_extra_args_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_markup_str_plain_extra_args_tuple, 0, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_markup_str_plain_extra_args_tuple, 1, const_str_plain_markup ); Py_INCREF( const_str_plain_markup );
    PyTuple_SET_ITEM( const_tuple_str_plain_source_str_plain_markup_str_plain_extra_args_tuple, 2, const_str_plain_extra_args ); Py_INCREF( const_str_plain_extra_args );
    const_str_digest_b8b38ac436c6a055ae769839c2986323 = UNSTREAM_STRING_ASCII( &constant_bin[ 2752252 ], 26, 0 );
    const_list_str_digest_1ee7ce96fcdc099d36a72e7821e96928_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_digest_1ee7ce96fcdc099d36a72e7821e96928_list, 0, const_str_digest_1ee7ce96fcdc099d36a72e7821e96928 ); Py_INCREF( const_str_digest_1ee7ce96fcdc099d36a72e7821e96928 );
    const_str_digest_8840b2d72e52a4388a239fdaa17f0f10 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753219 ], 21, 0 );
    const_str_digest_02df46b9435fd3398a0386c532745a64 = UNSTREAM_STRING_ASCII( &constant_bin[ 2753240 ], 29, 0 );
    const_str_plain__mistune_import_error = UNSTREAM_STRING_ASCII( &constant_bin[ 2753269 ], 21, 1 );
    const_tuple_str_plain_markdown2html_mistune_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_markdown2html_mistune_tuple, 0, const_str_plain_markdown2html_mistune ); Py_INCREF( const_str_plain_markdown2html_mistune );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_nbconvert$filters$markdown( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_099e769cb6f7546a514cb58a9c79c78f;
static PyCodeObject *codeobj_babeac2b99aff2ce5690bd275c56f8c9;
static PyCodeObject *codeobj_65962486948dd2c24646a76f1b28a67a;
static PyCodeObject *codeobj_8c800d967ca43e249b7550ffa719b907;
static PyCodeObject *codeobj_4762798e4cc62ba74dcca5f6eb198a86;
static PyCodeObject *codeobj_0c0862927e3e7e13b9e674ee3be85abe;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_02df46b9435fd3398a0386c532745a64 );
    codeobj_099e769cb6f7546a514cb58a9c79c78f = MAKE_CODEOBJ( module_filename_obj, const_str_digest_27190bd4d7f53af1c7d8d10126eff5e2, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_babeac2b99aff2ce5690bd275c56f8c9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_markdown2asciidoc, 68, const_tuple_str_plain_source_str_plain_extra_args_str_plain_asciidoc_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_65962486948dd2c24646a76f1b28a67a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_markdown2html_mistune, 18, const_tuple_str_plain_source_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8c800d967ca43e249b7550ffa719b907 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_markdown2html_pandoc, 60, const_tuple_str_plain_source_str_plain_extra_args_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4762798e4cc62ba74dcca5f6eb198a86 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_markdown2latex, 36, const_tuple_str_plain_source_str_plain_markup_str_plain_extra_args_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0c0862927e3e7e13b9e674ee3be85abe = MAKE_CODEOBJ( module_filename_obj, const_str_plain_markdown2rst, 86, const_tuple_str_plain_source_str_plain_extra_args_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_1_markdown2html_mistune(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_2_markdown2latex( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_3_markdown2html_pandoc( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_4_markdown2asciidoc( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_5_markdown2rst( PyObject *defaults );


// The module function definitions.
static PyObject *impl_nbconvert$filters$markdown$$$function_1_markdown2html_mistune( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_65962486948dd2c24646a76f1b28a67a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_65962486948dd2c24646a76f1b28a67a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_65962486948dd2c24646a76f1b28a67a, codeobj_65962486948dd2c24646a76f1b28a67a, module_nbconvert$filters$markdown, sizeof(void *) );
    frame_65962486948dd2c24646a76f1b28a67a = cache_frame_65962486948dd2c24646a76f1b28a67a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_65962486948dd2c24646a76f1b28a67a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_65962486948dd2c24646a76f1b28a67a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        PyObject *tmp_make_exception_arg_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_left_name_1 = const_str_digest_5945a0ad7959b19255e86c493c87fb9b;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain__mistune_import_error );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__mistune_import_error );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_mistune_import_error" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 21;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_right_name_1 = tmp_mvar_value_1;
        tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_make_exception_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_65962486948dd2c24646a76f1b28a67a->m_frame.f_lineno = 20;
        {
            PyObject *call_args[] = { tmp_make_exception_arg_1 };
            tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ImportError, call_args );
        }

        Py_DECREF( tmp_make_exception_arg_1 );
        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 20;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_65962486948dd2c24646a76f1b28a67a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_65962486948dd2c24646a76f1b28a67a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_65962486948dd2c24646a76f1b28a67a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_65962486948dd2c24646a76f1b28a67a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_65962486948dd2c24646a76f1b28a67a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_65962486948dd2c24646a76f1b28a67a,
        type_description_1,
        par_source
    );


    // Release cached frame.
    if ( frame_65962486948dd2c24646a76f1b28a67a == cache_frame_65962486948dd2c24646a76f1b28a67a )
    {
        Py_DECREF( frame_65962486948dd2c24646a76f1b28a67a );
    }
    cache_frame_65962486948dd2c24646a76f1b28a67a = NULL;

    assertFrameObject( frame_65962486948dd2c24646a76f1b28a67a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_1_markdown2html_mistune );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_1_markdown2html_mistune );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_nbconvert$filters$markdown$$$function_2_markdown2latex( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    PyObject *par_markup = python_pars[ 1 ];
    PyObject *par_extra_args = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_4762798e4cc62ba74dcca5f6eb198a86;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_4762798e4cc62ba74dcca5f6eb198a86 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4762798e4cc62ba74dcca5f6eb198a86, codeobj_4762798e4cc62ba74dcca5f6eb198a86, module_nbconvert$filters$markdown, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4762798e4cc62ba74dcca5f6eb198a86 = cache_frame_4762798e4cc62ba74dcca5f6eb198a86;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4762798e4cc62ba74dcca5f6eb198a86 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4762798e4cc62ba74dcca5f6eb198a86 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_convert_pandoc );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_convert_pandoc );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "convert_pandoc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_source );
        tmp_tuple_element_1 = par_source;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_markup );
        tmp_tuple_element_1 = par_markup;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_latex;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_extra_args;
        CHECK_OBJECT( par_extra_args );
        tmp_dict_value_1 = par_extra_args;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_4762798e4cc62ba74dcca5f6eb198a86->m_frame.f_lineno = 57;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4762798e4cc62ba74dcca5f6eb198a86 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4762798e4cc62ba74dcca5f6eb198a86 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4762798e4cc62ba74dcca5f6eb198a86 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4762798e4cc62ba74dcca5f6eb198a86, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4762798e4cc62ba74dcca5f6eb198a86->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4762798e4cc62ba74dcca5f6eb198a86, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4762798e4cc62ba74dcca5f6eb198a86,
        type_description_1,
        par_source,
        par_markup,
        par_extra_args
    );


    // Release cached frame.
    if ( frame_4762798e4cc62ba74dcca5f6eb198a86 == cache_frame_4762798e4cc62ba74dcca5f6eb198a86 )
    {
        Py_DECREF( frame_4762798e4cc62ba74dcca5f6eb198a86 );
    }
    cache_frame_4762798e4cc62ba74dcca5f6eb198a86 = NULL;

    assertFrameObject( frame_4762798e4cc62ba74dcca5f6eb198a86 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_2_markdown2latex );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_markup );
    Py_DECREF( par_markup );
    par_markup = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_markup );
    Py_DECREF( par_markup );
    par_markup = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_2_markdown2latex );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown$$$function_3_markdown2html_pandoc( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    PyObject *par_extra_args = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_8c800d967ca43e249b7550ffa719b907;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_8c800d967ca43e249b7550ffa719b907 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8c800d967ca43e249b7550ffa719b907, codeobj_8c800d967ca43e249b7550ffa719b907, module_nbconvert$filters$markdown, sizeof(void *)+sizeof(void *) );
    frame_8c800d967ca43e249b7550ffa719b907 = cache_frame_8c800d967ca43e249b7550ffa719b907;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8c800d967ca43e249b7550ffa719b907 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8c800d967ca43e249b7550ffa719b907 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        CHECK_OBJECT( par_extra_args );
        tmp_or_left_value_1 = par_extra_args;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_or_right_value_1 = LIST_COPY( const_list_str_digest_1ee7ce96fcdc099d36a72e7821e96928_list );
        tmp_assign_source_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_assign_source_1 = tmp_or_left_value_1;
        or_end_1:;
        {
            PyObject *old = par_extra_args;
            assert( old != NULL );
            par_extra_args = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_convert_pandoc );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_convert_pandoc );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "convert_pandoc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 65;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_source );
        tmp_tuple_element_1 = par_source;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_markdown;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_html;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_extra_args;
        CHECK_OBJECT( par_extra_args );
        tmp_dict_value_1 = par_extra_args;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_8c800d967ca43e249b7550ffa719b907->m_frame.f_lineno = 65;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c800d967ca43e249b7550ffa719b907 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c800d967ca43e249b7550ffa719b907 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c800d967ca43e249b7550ffa719b907 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8c800d967ca43e249b7550ffa719b907, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8c800d967ca43e249b7550ffa719b907->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8c800d967ca43e249b7550ffa719b907, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8c800d967ca43e249b7550ffa719b907,
        type_description_1,
        par_source,
        par_extra_args
    );


    // Release cached frame.
    if ( frame_8c800d967ca43e249b7550ffa719b907 == cache_frame_8c800d967ca43e249b7550ffa719b907 )
    {
        Py_DECREF( frame_8c800d967ca43e249b7550ffa719b907 );
    }
    cache_frame_8c800d967ca43e249b7550ffa719b907 = NULL;

    assertFrameObject( frame_8c800d967ca43e249b7550ffa719b907 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_3_markdown2html_pandoc );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_3_markdown2html_pandoc );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown$$$function_4_markdown2asciidoc( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    PyObject *par_extra_args = python_pars[ 1 ];
    PyObject *var_asciidoc = NULL;
    struct Nuitka_FrameObject *frame_babeac2b99aff2ce5690bd275c56f8c9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_babeac2b99aff2ce5690bd275c56f8c9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_babeac2b99aff2ce5690bd275c56f8c9, codeobj_babeac2b99aff2ce5690bd275c56f8c9, module_nbconvert$filters$markdown, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_babeac2b99aff2ce5690bd275c56f8c9 = cache_frame_babeac2b99aff2ce5690bd275c56f8c9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_babeac2b99aff2ce5690bd275c56f8c9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_babeac2b99aff2ce5690bd275c56f8c9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        CHECK_OBJECT( par_extra_args );
        tmp_or_left_value_1 = par_extra_args;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_or_right_value_1 = LIST_COPY( const_list_str_digest_c1bcb7e0f08c4047cfa7ce787b901ea7_list );
        tmp_assign_source_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_assign_source_1 = tmp_or_left_value_1;
        or_end_1:;
        {
            PyObject *old = par_extra_args;
            assert( old != NULL );
            par_extra_args = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_convert_pandoc );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_convert_pandoc );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "convert_pandoc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_source );
        tmp_tuple_element_1 = par_source;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_markdown;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_asciidoc;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_extra_args;
        CHECK_OBJECT( par_extra_args );
        tmp_dict_value_1 = par_extra_args;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_babeac2b99aff2ce5690bd275c56f8c9->m_frame.f_lineno = 71;
        tmp_assign_source_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_asciidoc == NULL );
        var_asciidoc = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = const_str_plain___;
        CHECK_OBJECT( var_asciidoc );
        tmp_compexpr_right_1 = var_asciidoc;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_re );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 75;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            tmp_args_element_name_1 = const_str_digest_74e88f13430ac243f510936fb4ef17ff;
            tmp_args_element_name_2 = const_str_digest_4b639991ebbb2622fd952a725efa060c;
            CHECK_OBJECT( var_asciidoc );
            tmp_args_element_name_3 = var_asciidoc;
            frame_babeac2b99aff2ce5690bd275c56f8c9->m_frame.f_lineno = 75;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_assign_source_3 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain_sub, call_args );
            }

            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 75;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_asciidoc;
                assert( old != NULL );
                var_asciidoc = tmp_assign_source_3;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_re );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 77;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_2 = tmp_mvar_value_3;
            tmp_args_element_name_4 = const_str_digest_8840b2d72e52a4388a239fdaa17f0f10;
            tmp_args_element_name_5 = const_str_digest_cd2fbac026251ec6bc6b6df435989d44;
            CHECK_OBJECT( var_asciidoc );
            tmp_args_element_name_6 = var_asciidoc;
            frame_babeac2b99aff2ce5690bd275c56f8c9->m_frame.f_lineno = 77;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_assign_source_4 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain_sub, call_args );
            }

            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 77;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_asciidoc;
                assert( old != NULL );
                var_asciidoc = tmp_assign_source_4;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_babeac2b99aff2ce5690bd275c56f8c9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_babeac2b99aff2ce5690bd275c56f8c9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_babeac2b99aff2ce5690bd275c56f8c9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_babeac2b99aff2ce5690bd275c56f8c9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_babeac2b99aff2ce5690bd275c56f8c9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_babeac2b99aff2ce5690bd275c56f8c9,
        type_description_1,
        par_source,
        par_extra_args,
        var_asciidoc
    );


    // Release cached frame.
    if ( frame_babeac2b99aff2ce5690bd275c56f8c9 == cache_frame_babeac2b99aff2ce5690bd275c56f8c9 )
    {
        Py_DECREF( frame_babeac2b99aff2ce5690bd275c56f8c9 );
    }
    cache_frame_babeac2b99aff2ce5690bd275c56f8c9 = NULL;

    assertFrameObject( frame_babeac2b99aff2ce5690bd275c56f8c9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_asciidoc );
    tmp_return_value = var_asciidoc;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_4_markdown2asciidoc );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    CHECK_OBJECT( (PyObject *)var_asciidoc );
    Py_DECREF( var_asciidoc );
    var_asciidoc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    Py_XDECREF( var_asciidoc );
    var_asciidoc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_4_markdown2asciidoc );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$markdown$$$function_5_markdown2rst( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    PyObject *par_extra_args = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_0c0862927e3e7e13b9e674ee3be85abe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_0c0862927e3e7e13b9e674ee3be85abe = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0c0862927e3e7e13b9e674ee3be85abe, codeobj_0c0862927e3e7e13b9e674ee3be85abe, module_nbconvert$filters$markdown, sizeof(void *)+sizeof(void *) );
    frame_0c0862927e3e7e13b9e674ee3be85abe = cache_frame_0c0862927e3e7e13b9e674ee3be85abe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0c0862927e3e7e13b9e674ee3be85abe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0c0862927e3e7e13b9e674ee3be85abe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_convert_pandoc );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_convert_pandoc );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "convert_pandoc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 103;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_source );
        tmp_tuple_element_1 = par_source;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_markdown;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_rst;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_extra_args;
        CHECK_OBJECT( par_extra_args );
        tmp_dict_value_1 = par_extra_args;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_0c0862927e3e7e13b9e674ee3be85abe->m_frame.f_lineno = 103;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c0862927e3e7e13b9e674ee3be85abe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c0862927e3e7e13b9e674ee3be85abe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c0862927e3e7e13b9e674ee3be85abe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0c0862927e3e7e13b9e674ee3be85abe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0c0862927e3e7e13b9e674ee3be85abe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0c0862927e3e7e13b9e674ee3be85abe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0c0862927e3e7e13b9e674ee3be85abe,
        type_description_1,
        par_source,
        par_extra_args
    );


    // Release cached frame.
    if ( frame_0c0862927e3e7e13b9e674ee3be85abe == cache_frame_0c0862927e3e7e13b9e674ee3be85abe )
    {
        Py_DECREF( frame_0c0862927e3e7e13b9e674ee3be85abe );
    }
    cache_frame_0c0862927e3e7e13b9e674ee3be85abe = NULL;

    assertFrameObject( frame_0c0862927e3e7e13b9e674ee3be85abe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_5_markdown2rst );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_extra_args );
    Py_DECREF( par_extra_args );
    par_extra_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown$$$function_5_markdown2rst );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_1_markdown2html_mistune(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown$$$function_1_markdown2html_mistune,
        const_str_plain_markdown2html_mistune,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_65962486948dd2c24646a76f1b28a67a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown,
        const_str_digest_542ff9b6d089e7cf951abb26781c3d41,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_2_markdown2latex( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown$$$function_2_markdown2latex,
        const_str_plain_markdown2latex,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4762798e4cc62ba74dcca5f6eb198a86,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown,
        const_str_digest_23646ce451c1abfc834323dfd9c54177,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_3_markdown2html_pandoc( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown$$$function_3_markdown2html_pandoc,
        const_str_plain_markdown2html_pandoc,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8c800d967ca43e249b7550ffa719b907,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown,
        const_str_digest_2bcc2ae198887f4979e57e86c26f77a3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_4_markdown2asciidoc( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown$$$function_4_markdown2asciidoc,
        const_str_plain_markdown2asciidoc,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_babeac2b99aff2ce5690bd275c56f8c9,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown,
        const_str_digest_fe3ab07ff2922de96e37607f5cb301eb,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$markdown$$$function_5_markdown2rst( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$markdown$$$function_5_markdown2rst,
        const_str_plain_markdown2rst,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0c0862927e3e7e13b9e674ee3be85abe,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$markdown,
        const_str_digest_3d8a6a59dab71c6bbfb587ed74addd8b,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_nbconvert$filters$markdown =
{
    PyModuleDef_HEAD_INIT,
    "nbconvert.filters.markdown",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(nbconvert$filters$markdown)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(nbconvert$filters$markdown)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_nbconvert$filters$markdown );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.markdown: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.markdown: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.markdown: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnbconvert$filters$markdown" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_nbconvert$filters$markdown = Py_InitModule4(
        "nbconvert.filters.markdown",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_nbconvert$filters$markdown = PyModule_Create( &mdef_nbconvert$filters$markdown );
#endif

    moduledict_nbconvert$filters$markdown = MODULE_DICT( module_nbconvert$filters$markdown );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_nbconvert$filters$markdown,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_nbconvert$filters$markdown,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$filters$markdown,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$filters$markdown,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_nbconvert$filters$markdown );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_b8b38ac436c6a055ae769839c2986323, module_nbconvert$filters$markdown );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_099e769cb6f7546a514cb58a9c79c78f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_d214d91f77fddc6708f2f0452c3b07f2;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_099e769cb6f7546a514cb58a9c79c78f = MAKE_MODULE_FRAME( codeobj_099e769cb6f7546a514cb58a9c79c78f, module_nbconvert$filters$markdown );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_099e769cb6f7546a514cb58a9c79c78f );
    assert( Py_REFCNT( frame_099e769cb6f7546a514cb58a9c79c78f ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_099e769cb6f7546a514cb58a9c79c78f->m_frame.f_lineno = 9;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_print_function );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_nbconvert$filters$markdown;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_099e769cb6f7546a514cb58a9c79c78f->m_frame.f_lineno = 10;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_5 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_markdown_mistune;
        tmp_globals_name_2 = (PyObject *)moduledict_nbconvert$filters$markdown;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_markdown2html_mistune_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_099e769cb6f7546a514cb58a9c79c78f->m_frame.f_lineno = 13;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_6 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_nbconvert$filters$markdown,
                const_str_plain_markdown2html_mistune,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_markdown2html_mistune );
        }

        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2html_mistune, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_099e769cb6f7546a514cb58a9c79c78f, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_099e769cb6f7546a514cb58a9c79c78f, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = EXC_VALUE(PyThreadState_GET());
            UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_e, tmp_assign_source_7 );
        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_mvar_value_3;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_e );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_e );
            }

            CHECK_OBJECT( tmp_mvar_value_3 );
            tmp_assign_source_8 = tmp_mvar_value_3;
            UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain__mistune_import_error, tmp_assign_source_8 );
        }
        {
            PyObject *tmp_assign_source_9;
            tmp_assign_source_9 = MAKE_FUNCTION_nbconvert$filters$markdown$$$function_1_markdown2html_mistune(  );



            UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2html_mistune, tmp_assign_source_9 );
        }
        tmp_res = PyDict_DelItem( (PyObject *)moduledict_nbconvert$filters$markdown, const_str_plain_e );
        if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 12;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_099e769cb6f7546a514cb58a9c79c78f->m_frame) frame_099e769cb6f7546a514cb58a9c79c78f->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_2;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$markdown );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_pandoc;
        tmp_globals_name_3 = (PyObject *)moduledict_nbconvert$filters$markdown;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_convert_pandoc_tuple;
        tmp_level_name_3 = const_int_pos_1;
        frame_099e769cb6f7546a514cb58a9c79c78f->m_frame.f_lineno = 23;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_nbconvert$filters$markdown,
                const_str_plain_convert_pandoc,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_convert_pandoc );
        }

        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_convert_pandoc, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = LIST_COPY( const_list_973c7019e71af81eaa3da26f1753d0ae_list );
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_str_plain_markdown_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_12 = MAKE_FUNCTION_nbconvert$filters$markdown$$$function_2_markdown2latex( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2latex, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_13 = MAKE_FUNCTION_nbconvert$filters$markdown$$$function_3_markdown2html_pandoc( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2html_pandoc, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_14 = MAKE_FUNCTION_nbconvert$filters$markdown$$$function_4_markdown2asciidoc( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2asciidoc, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2html_mistune );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_markdown2html_mistune );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "markdown2html_mistune" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_15 = tmp_mvar_value_4;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2html, tmp_assign_source_15 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_099e769cb6f7546a514cb58a9c79c78f );
#endif
    popFrameStack();

    assertFrameObject( frame_099e769cb6f7546a514cb58a9c79c78f );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_099e769cb6f7546a514cb58a9c79c78f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_099e769cb6f7546a514cb58a9c79c78f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_099e769cb6f7546a514cb58a9c79c78f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_099e769cb6f7546a514cb58a9c79c78f, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_defaults_4;
        tmp_defaults_4 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_16 = MAKE_FUNCTION_nbconvert$filters$markdown$$$function_5_markdown2rst( tmp_defaults_4 );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$markdown, (Nuitka_StringObject *)const_str_plain_markdown2rst, tmp_assign_source_16 );
    }

    return MOD_RETURN_VALUE( module_nbconvert$filters$markdown );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
