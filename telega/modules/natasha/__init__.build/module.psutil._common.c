/* Generated code for Python module 'psutil._common'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_psutil$_common" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_psutil$_common;
PyDictObject *moduledict_psutil$_common;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain_FIN_WAIT1;
extern PyObject *const_str_plain_inner;
extern PyObject *const_str_plain_family;
static PyObject *const_str_digest_4571a7d7297fb61bce152a7b77fd65db;
extern PyObject *const_str_plain___spec__;
static PyObject *const_list_04200af437d3ca9afb64727d4cd1a1b6_list;
static PyObject *const_str_digest_85a627319c236bc8f9aa4f6ec2242376;
extern PyObject *const_str_plain_linux;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_outer;
extern PyObject *const_str_plain_sorted;
static PyObject *const_tuple_str_plain_darwin_tuple;
static PyObject *const_str_digest_91bde82034c2ba995a6e7ea81b4ee6c4;
extern PyObject *const_str_plain_tcp;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_SUNOS;
extern PyObject *const_str_plain_max;
extern PyObject *const_str_plain_err;
static PyObject *const_str_digest_6230f983e0ca3f1521efd26013c14e9b;
extern PyObject *const_str_plain_laddr;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_getfilesystemencoding;
extern PyObject *const_str_plain_sdiskpart;
static PyObject *const_str_digest_95896ac3fdecfb6ca65cbe556616215c;
extern PyObject *const_str_plain_popenfile;
static PyObject *const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple;
extern PyObject *const_str_plain_bind;
extern PyObject *const_str_plain_Y;
extern PyObject *const_tuple_none_none_none_tuple;
static PyObject *const_str_digest_af5c0b95f5f11bc66744e3de077488bb;
extern PyObject *const_tuple_str_plain_cache_tuple;
extern PyObject *const_str_plain_contextlib;
extern PyObject *const_str_plain_STATUS_SUSPENDED;
extern PyObject *const_str_plain___debug__;
static PyObject *const_list_str_plain_path_str_plain_fd_list;
extern PyObject *const_str_plain_errno;
extern PyObject *const_str_plain_CONN_TIME_WAIT;
static PyObject *const_list_66ba68b9593ee86a3b33193aff923a5b_list;
extern PyObject *const_str_plain_running;
extern PyObject *const_str_plain_CONN_CLOSE_WAIT;
extern PyObject *const_str_plain_high;
static PyObject *const_str_digest_fc55a6ec1010a1560f644da71f4b3cba;
extern PyObject *const_str_plain_Z;
extern PyObject *const_int_neg_2;
static PyObject *const_str_digest_8c8c0124d3f75932fd5693c97f1e2e27;
static PyObject *const_str_plain_netbsd;
extern PyObject *const_str_plain_min;
extern PyObject *const_str_plain_sock;
extern PyObject *const_str_plain_path;
static PyObject *const_str_plain_WINDOWS_;
extern PyObject *const_str_plain_udp;
static PyObject *const_str_plain_sleeping;
extern PyObject *const_str_plain_IntEnum;
extern PyObject *const_str_plain_sout;
extern PyObject *const_str_plain_duplex;
extern PyObject *const_str_plain_POWER_TIME_UNLIMITED;
static PyObject *const_tuple_str_plain_netbsd_tuple;
extern PyObject *const_str_plain_new_dict;
extern PyObject *const_str_plain_CONN_LISTEN;
static PyObject *const_str_plain__wn;
extern PyObject *const_str_plain_puids;
extern PyObject *const_str_plain_reversed;
extern PyObject *const_str_plain_read_count;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_open_text;
extern PyObject *const_str_plain_usage_percent;
extern PyObject *const_str_plain_False;
static PyObject *const_tuple_str_plain_freebsd_tuple;
static PyObject *const_list_ce761954676b292932e8dabc7e5b1c77_list;
extern PyObject *const_str_plain_freebsd;
extern PyObject *const_str_plain_snicaddr;
extern PyObject *const_tuple_str_plain_AF_INET6_tuple;
extern PyObject *const_str_plain_pop;
static PyObject *const_tuple_str_plain_SOCK_DGRAM_tuple;
static PyObject *const_str_digest_f1920876d07de7b4ca17e71fd0a07cbe;
extern PyObject *const_int_0;
static PyObject *const_str_plain_remkey;
extern PyObject *const_str_plain___members__;
static PyObject *const_str_plain__WrapNumbers;
extern PyObject *const_str_plain_B;
extern PyObject *const_str_plain_BSD;
extern PyObject *const_str_plain_isup;
static PyObject *const_str_plain_gone_key;
static PyObject *const_list_e4eda79a105ca56e947928094e495dab_list;
extern PyObject *const_str_plain_socktype_to_enum;
extern PyObject *const_str_plain_setdefault;
static PyObject *const_tuple_str_plain_openbsd_tuple;
extern PyObject *const_int_pos_4;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_CONN_NONE;
extern PyObject *const_str_plain_LINUX;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_CONN_CLOSE;
extern PyObject *const_str_plain_memoize_when_activated;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_functools;
extern PyObject *const_str_plain_pctxsw;
extern PyObject *const_str_plain_udp4;
extern PyObject *const_str_plain_AF_INET6;
static PyObject *const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple;
static PyObject *const_str_plain_errout;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_write_count;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_plain_port;
static PyObject *const_str_digest_cb4806a92f0a38e89c5326d8bded12ed;
extern PyObject *const_str_plain_open;
extern PyObject *const_str_plain_snicstats;
extern PyObject *const_tuple_str_plain_linux_tuple;
extern PyObject *const_tuple_str_plain_defaultdict_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_pid;
static PyObject *const_str_plain_old_tuple;
extern PyObject *const_str_plain_EPERM;
extern PyObject *const_str_plain_AF_INET;
extern PyObject *const_str_plain_T;
extern PyObject *const_str_plain_kwargs;
static PyObject *const_tuple_str_plain_AF_UNIX_tuple;
extern PyObject *const_str_plain_E;
extern PyObject *const_str_plain_tcp6;
extern PyObject *const_str_plain_format;
static PyObject *const_str_plain_LISTEN;
extern PyObject *const_str_plain_startswith;
static PyObject *const_list_str_plain_percent_str_plain_secsleft_str_plain_power_plugged_list;
extern PyObject *const_str_plain_find;
static PyObject *const_str_plain_ESTABLISHED;
static PyObject *const_str_plain_system_time;
extern PyObject *const_str_plain_threading;
extern PyObject *const_str_plain_set;
static PyObject *const_tuple_str_plain_sock_tuple;
static PyObject *const_list_str_plain_voluntary_str_plain_involuntary_list;
static PyObject *const_list_str_plain_label_str_plain_current_list;
static PyObject *const_tuple_str_plain_self_str_plain_ret_str_plain_fun_tuple;
extern PyObject *const_str_plain_STATUS_PARKED;
extern PyObject *const_str_plain_PROCFS_PATH;
extern PyObject *const_str_plain_waiting;
extern PyObject *const_str_plain_snetio;
static PyObject *const_tuple_9798201f399d66f2ff5e0bbe3aa1d23f_tuple;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_d322b24329b5f9cbfeea0da3ac450745;
static PyObject *const_list_487a7b5aabbaa0a3116539a420c71666_list;
extern PyObject *const_tuple_str_plain_num_tuple;
extern PyObject *const_str_plain_broadcast;
extern PyObject *const_str_plain_sconn;
static PyObject *const_str_digest_7f773a2c61aafb6080b6e3fa0c482cee;
extern PyObject *const_str_plain_proc;
extern PyObject *const_str_plain_OPENBSD;
extern PyObject *const_str_plain_psutil;
extern PyObject *const_str_plain_num;
static PyObject *const_str_plain_input_tuple;
static PyObject *const_list_4ce112f110e3f44d360b8e23a82a18a9_list;
extern PyObject *const_str_plain_udp6;
extern PyObject *const_str_plain_children_user;
static PyObject *const_str_plain_CLOSE;
extern PyObject *const_str_chr_61;
extern PyObject *const_str_plain_STATUS_RUNNING;
static PyObject *const_list_f504379bcc563b1d896d033185a6607f_list;
extern PyObject *const_str_plain_saved;
extern PyObject *const_str_plain_category;
extern PyObject *const_str_plain_mountpoint;
static PyObject *const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple;
static PyObject *const_str_digest_b2b793ac5e593d3efbfbe861ccb88056;
extern PyObject *const_str_plain_inet4;
extern PyObject *const_str_plain_opts;
static PyObject *const_str_digest_bbd7ba56388169475bb178ec7024ca39;
extern PyObject *const_str_plain_cache;
extern PyObject *const_str_plain_wrapper;
extern PyObject *const_str_plain_platform;
extern PyObject *const_str_plain_NicDuplex;
static PyObject *const_str_plain__add_dict;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_suspended;
extern PyObject *const_str_plain_CONN_LAST_ACK;
extern PyObject *const_str_plain_effective;
extern PyObject *const_str_plain_AF_UNIX;
extern PyObject *const_str_plain_rt;
extern PyObject *const_str_plain_get_procfs_path;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_99410fa7664fe22ccad236d080673163;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_wraps;
extern PyObject *const_str_plain_SOCK_DGRAM;
static PyObject *const_tuple_str_plain_aix_tuple;
extern PyObject *const_str_plain_sockfam_to_enum;
static PyObject *const_str_plain_FIN_WAIT2;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_collections;
static PyObject *const_str_plain_stopped;
extern PyObject *const_str_plain_all;
static PyObject *const_str_plain_packets_recv;
extern PyObject *const_tuple_str_plain_AF_INET_tuple;
static PyObject *const_str_plain_aix;
extern PyObject *const_str_plain_sdiskusage;
static PyObject *const_str_digest_5cc34510cf5423be6b3dd305d1fab306;
static PyObject *const_str_plain_getfilesystemencodeerrors;
extern PyObject *const_str_plain_sfan;
static PyObject *const_tuple_tuple_str_plain_sunos_str_plain_solaris_tuple_tuple;
extern PyObject *const_str_plain_enumerate;
extern PyObject *const_str_plain_pionice;
extern PyObject *const_str_plain_STATUS_IDLE;
extern PyObject *const_str_plain_errors;
extern PyObject *const_str_plain_P;
static PyObject *const_str_digest_93046e394fea44e344d65326f6299d5e;
static PyObject *const_str_plain_waking;
extern PyObject *const_str_plain_secsleft;
extern PyObject *const_str_plain_STATUS_DEAD;
extern PyObject *const_str_plain_conn_tmap;
extern PyObject *const_str_plain_error;
extern PyObject *const_str_plain_stacklevel;
extern PyObject *const_str_plain_ENCODING_ERRS;
extern PyObject *const_str_plain_ip;
extern PyObject *const_str_plain_decode;
extern PyObject *const_str_plain_nt;
static PyObject *const_str_digest_af97312a62c0414513bc3cf713b1b59a;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain___all__;
static PyObject *const_tuple_str_plain_path_str_plain_st_str_plain_err_tuple;
static PyObject *const_tuple_cff0ce383940b17a56ca81acef7d3732_tuple;
extern PyObject *const_str_plain_replacement;
static PyObject *const_tuple_f461192a65f6ebc08a476f66d211dbef_tuple;
extern PyObject *const_str_plain_upper;
extern PyObject *const_str_plain_NETBSD;
extern PyObject *const_str_plain_inet;
static PyObject *const_tuple_eaafdf540f6164da2d4a3e09760b2b4a_tuple;
extern PyObject *const_str_plain_origin;
extern PyObject *const_tuple_str_plain_self_str_plain_name_tuple;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_list_str_plain_ip_str_plain_port_list;
extern PyObject *const_str_plain_namedtuple;
extern PyObject *const_str_plain_pconn;
extern PyObject *const_str_plain_CONN_FIN_WAIT2;
extern PyObject *const_str_plain_soft_interrupts;
static PyObject *const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple;
extern PyObject *const_str_plain_STATUS_DISK_SLEEP;
extern PyObject *const_str_plain_sys;
static PyObject *const_str_digest_9d5da3b1c04b6be63df52845d9808c46;
extern PyObject *const_str_plain_socket;
static PyObject *const_tuple_str_plain_input_dict_str_plain_name_tuple;
static PyObject *const_list_e817dfda20ddb186bde2c54cef499933_list;
extern PyObject *const_str_plain_M;
extern PyObject *const_str_plain_key;
static PyObject *const_str_plain_SYN_SENT;
static PyObject *const_str_plain_old_dict;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_CONN_CLOSING;
static PyObject *const_str_plain_BatteryTime;
extern PyObject *const_str_plain_STATUS_TRACING_STOP;
static PyObject *const_str_digest_3cd9631d27a7d42f4e2a7520ff729c49;
extern PyObject *const_str_plain_fname;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_CONN_ESTABLISHED;
extern PyObject *const_str_plain_darwin;
static PyObject *const_str_digest_49ff0a35e03d63667c353f2c0a9c7733;
extern PyObject *const_float_0_0;
extern PyObject *const_str_plain_closing;
extern PyObject *const_str_plain_NONE;
extern PyObject *const_int_pos_100;
static PyObject *const_str_plain_openbsd;
static PyObject *const_list_str_plain_id_str_plain_user_time_str_plain_system_time_list;
static PyObject *const_str_digest_f6856f5d282ebea61302c4adae8f9ada;
extern PyObject *const_str_plain_mtu;
extern PyObject *const_list_c1e1ba8280b2052467c1c38da9f4c5e2_list;
extern PyObject *const_str_plain_interrupts;
extern PyObject *const_str_plain_device;
static PyObject *const_str_plain_LAST_ACK;
extern PyObject *const_str_plain_tcp4;
static PyObject *const_str_digest_1d916fa90b3796c4ab91e848071040be;
extern PyObject *const_str_plain_pgids;
static PyObject *const_str_plain_parked;
static PyObject *const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple;
extern PyObject *const_str_plain_terminal;
static PyObject *const_tuple_str_plain__WrapNumbers_tuple_empty_tuple;
extern PyObject *const_str_plain_pthread;
static PyObject *const_str_plain_CLOSING;
extern PyObject *const_str_plain_prefix;
static PyObject *const_tuple_05d8568253270babe3d513d8bda8531d_tuple;
extern PyObject *const_str_plain_write_bytes;
extern PyObject *const_str_plain_division;
extern PyObject *const_str_plain_cache_clear;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_pcputimes;
extern PyObject *const_str_plain_STATUS_ZOMBIE;
extern PyObject *const_str_plain_sbattery;
extern PyObject *const_str_plain_id;
extern PyObject *const_str_chr_0;
static PyObject *const_tuple_str_plain_replacement_str_plain_outer_tuple;
extern PyObject *const_str_plain_locked;
extern PyObject *const_str_plain_real;
static PyObject *const_str_plain_SYN_RECV;
extern PyObject *const_str_plain_CONN_SYN_RECV;
static PyObject *const_str_digest_457d32d6501e34673ad4aa2f55c371a6;
extern PyObject *const_str_plain_PY3;
extern PyObject *const_str_plain_isfile_strict;
extern PyObject *const_str_plain_pos;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_user_time;
static PyObject *const_str_digest_d738b7bb5a4adf77b8882bd7cfde13ad;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain_add;
extern PyObject *const_str_plain_warnings;
extern PyObject *const_str_plain_ioclass;
extern PyObject *const_str_plain_label;
extern PyObject *const_str_plain_ENCODING;
extern PyObject *const_tuple_str_plain_namedtuple_tuple;
extern PyObject *const_str_plain__cache;
static PyObject *const_str_plain_input_value;
static PyObject *const_str_digest_28a10ab0cf555529690fb2e57e845886;
extern PyObject *const_str_plain_NIC_DUPLEX_HALF;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_old_value;
extern PyObject *const_str_plain_float;
static PyObject *const_str_digest_4f68eb82902a9b7b547feaa053e622b7;
extern PyObject *const_str_plain_WINDOWS;
extern PyObject *const_str_plain_cache_activate;
static PyObject *const_tuple_str_plain_proc_tuple;
extern PyObject *const_str_plain_shwtemp;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_NIC_DUPLEX_FULL;
extern PyObject *const_str_plain_fun;
extern PyObject *const_str_plain_has_ipv6;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_STATUS_WAKING;
extern PyObject *const_str_plain_solaris;
extern PyObject *const_int_pos_10;
extern PyObject *const_str_plain_CONN_FIN_WAIT1;
extern PyObject *const_str_plain_cache_deactivate;
extern PyObject *const_str_plain_scpustats;
extern PyObject *const_tuple_str_plain_fname_str_plain_kwargs_tuple;
static PyObject *const_str_plain_reminders;
static PyObject *const_str_plain_dropout;
extern PyObject *const_str_plain_MACOS;
extern PyObject *const_str_plain_msg;
extern PyObject *const_str_plain_open_binary;
extern PyObject *const_str_plain_current;
extern PyObject *const_tuple_type_int_tuple;
static PyObject *const_str_plain_involuntary;
static PyObject *const_str_plain_bytes_recv;
extern PyObject *const_str_plain_system;
extern PyObject *const_str_plain_modules;
static PyObject *const_tuple_type_ValueError_type_AttributeError_tuple;
extern PyObject *const_list_a5ad0ede6528da49c1442b81ef020e8e_list;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_round_;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_syscalls;
extern PyObject *const_str_plain_STATUS_SLEEPING;
extern PyObject *const_str_plain_raddr;
static PyObject *const_str_plain_errin;
extern PyObject *const_str_plain_STATUS_LOCKED;
static PyObject *const_str_digest_f1996bba9b05fd6185e0a3aa39f0f0e9;
extern PyObject *const_str_plain_user;
extern PyObject *const_str_plain_posix;
static PyObject *const_list_7269f1f5bbfaf7e849943ff948940288_list;
extern PyObject *const_str_plain_update;
extern PyObject *const_str_plain_Lock;
extern PyObject *const_str_plain_POWER_TIME_UNKNOWN;
extern PyObject *const_str_plain_address;
extern PyObject *const_str_plain_enum;
static PyObject *const_str_plain_next_pos;
extern PyObject *const_str_plain_write_time;
extern PyObject *const_str_plain_version_info;
static PyObject *const_str_digest_c5c04d92c90c74d705f2d90dc328b3dd;
static PyObject *const_str_plain_input_dict;
extern PyObject *const_str_plain_used;
extern PyObject *const_str_plain_speed;
static PyObject *const_str_plain_equal_pos;
extern PyObject *const_str_plain_started;
static PyObject *const_list_9db5dd841fb6eacf7ec042df99116523_list;
extern PyObject *const_str_plain_read_bytes;
static PyObject *const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list;
static PyObject *const_list_80a0510781d2d124eb0a6af3f3458443_list;
extern PyObject *const_str_plain_STATUS_STOPPED;
extern PyObject *const_str_plain_zombie;
static PyObject *const_str_digest_c21138c8d575fc0b2b6ff4782a202f72;
extern PyObject *const_str_plain_warn;
extern PyObject *const_str_plain_run;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_addr;
extern PyObject *const_str_plain_G;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_digest_d2a1a54d8c7e0284a0763d37c4a6b2ab;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain___exit__;
extern PyObject *const_str_plain_total;
extern PyObject *const_str_plain_ret;
static PyObject *const_tuple_str_plain_SOCK_STREAM_tuple;
extern PyObject *const_str_plain_pio;
extern PyObject *const_str_plain_bits;
extern PyObject *const_str_plain_items;
extern PyObject *const_str_plain_scpufreq;
extern PyObject *const_tuple_str_plain_s_tuple;
static PyObject *const_tuple_str_plain_path_str_plain_err_tuple;
extern PyObject *const_str_plain_sin;
extern PyObject *const_str_plain_defaultdict;
static PyObject *const_str_digest_386170845bb8c4a4ab83068de03309f2;
extern PyObject *const_str_plain_ptp;
static PyObject *const_str_digest_3edef1fa3eed954f4e1fabc8924bb36f;
extern PyObject *const_str_plain_fstype;
extern PyObject *const_str_plain_stat;
extern PyObject *const_str_plain___enter__;
static PyObject *const_str_digest_716f8d9d7522e1bc954f2cca2624926c;
extern PyObject *const_tuple_type_set_tuple;
static PyObject *const_tuple_5174cec25d92df58c36214d4067bc535_tuple;
static PyObject *const_tuple_str_digest_3f9fec55820f857461da4f1e12840da5_int_0_tuple;
static PyObject *const_str_digest_ad233afd4f5eda5adddba312e8fe0a80;
extern PyObject *const_str_plain_suser;
extern PyObject *const_str_plain_sunos;
static PyObject *const_tuple_str_plain_self_str_plain_input_dict_str_plain_name_tuple;
static PyObject *const_tuple_str_plain_sunos_str_plain_solaris_tuple;
extern PyObject *const_str_plain_status;
extern PyObject *const_str_plain_host;
static PyObject *const_str_plain__remove_dead_reminders;
extern PyObject *const_str_plain_memoize;
extern PyObject *const_str_plain_int;
static PyObject *const_str_plain_TIME_WAIT;
extern PyObject *const_str_plain_read_time;
extern PyObject *const_str_plain_DeprecationWarning;
extern PyObject *const_str_plain_symbol;
extern PyObject *const_str_plain_surrogateescape;
extern PyObject *const_tuple_int_pos_3_int_pos_4_tuple;
static PyObject *const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple;
extern PyObject *const_str_plain_critical;
extern PyObject *const_str_plain_SOCK_STREAM;
extern PyObject *const_str_plain_wrap_numbers;
extern PyObject *const_str_plain_EACCES;
extern PyObject *const_str_plain_deprecated_method;
extern PyObject *const_str_plain_parse_environ_block;
extern PyObject *const_str_plain_keys;
extern PyObject *const_str_plain_rb;
extern PyObject *const_str_plain_fd;
static PyObject *const_str_digest_2045132428b7bed20b21befde72ba292;
extern PyObject *const_str_plain_STATUS_WAITING;
static PyObject *const_str_digest_e504a1c5fb44d69b43b64780b96360d6;
extern PyObject *const_str_plain_sdiskio;
static PyObject *const_str_plain_voluntary;
extern PyObject *const_str_plain_NIC_DUPLEX_UNKNOWN;
static PyObject *const_str_plain_reminder_keys;
extern PyObject *const_str_plain_AIX;
extern PyObject *const_str_plain_st_mode;
extern PyObject *const_str_plain_STATUS_WAKE_KILL;
static PyObject *const_str_plain_packets_sent;
extern PyObject *const_str_plain_percent;
extern PyObject *const_str_plain_AddressFamily;
extern PyObject *const_str_plain_bytes2human;
extern PyObject *const_str_plain_OSX;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_digest_8154c1b785576b641e6ce5bfaf028ff5;
extern PyObject *const_str_plain_symbols;
extern PyObject *const_str_plain_K;
extern PyObject *const_str_plain_ctx_switches;
static PyObject *const_tuple_str_digest_d322b24329b5f9cbfeea0da3ac450745_tuple;
extern PyObject *const_str_digest_3f9fec55820f857461da4f1e12840da5;
extern PyObject *const_str_plain_sswap;
extern PyObject *const_str_plain_idle;
extern PyObject *const_str_plain_path_exists_strict;
extern PyObject *const_str_plain_children_system;
static PyObject *const_str_digest_230f09e45b705cbc9bc92403ddd4c5f2;
static PyObject *const_str_digest_5d6d330dc4d79bec1ba9037cf33d5c79;
static PyObject *const_list_str_plain_ioclass_str_plain_value_list;
extern PyObject *const_str_plain_FREEBSD;
extern PyObject *const_str_plain_inet6;
extern PyObject *const_str_plain_lock;
static PyObject *const_list_str_plain_real_str_plain_effective_str_plain_saved_list;
static PyObject *const_str_plain_AddressType;
extern PyObject *const_str_plain_netmask;
extern PyObject *const_str_plain_st;
extern PyObject *const_str_plain_power_plugged;
static PyObject *const_str_digest_115252dd9884ce3050fc49a9776440d5;
extern PyObject *const_str_plain_CONN_SYN_SENT;
extern PyObject *const_str_plain_supports_ipv6;
static PyObject *const_str_plain_gone_keys;
static PyObject *const_str_digest_e53dfc57c7e14e31d0998887df0f3e21;
static PyObject *const_str_plain_dropin;
extern PyObject *const_str_plain_S_ISREG;
static PyObject *const_str_plain_bytes_sent;
extern PyObject *const_str_plain_free;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_POSIX;
static PyObject *const_str_plain_CLOSE_WAIT;
extern PyObject *const_str_plain_unix;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_clear;
static PyObject *const_list_1f83fcd5f40aa6957d8a184c26f07a11_list;
extern PyObject *const_str_plain_dead;
extern PyObject *const_str_plain_round;
extern PyObject *const_int_pos_2;
static PyObject *const_list_str_plain_current_str_plain_min_str_plain_max_list;
static PyObject *const_str_digest_6c181c57aa262d51beb7a1726d9302b5;
extern PyObject *const_str_plain_cache_info;
static PyObject *const_str_digest_4ec72dfd8b162da383cac847fd2af2fc;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_FIN_WAIT1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4913900 ], 9, 1 );
    const_str_digest_4571a7d7297fb61bce152a7b77fd65db = UNSTREAM_STRING_ASCII( &constant_bin[ 4925517 ], 39, 0 );
    const_list_04200af437d3ca9afb64727d4cd1a1b6_list = PyList_New( 6 );
    PyList_SET_ITEM( const_list_04200af437d3ca9afb64727d4cd1a1b6_list, 0, const_str_plain_read_count ); Py_INCREF( const_str_plain_read_count );
    PyList_SET_ITEM( const_list_04200af437d3ca9afb64727d4cd1a1b6_list, 1, const_str_plain_write_count ); Py_INCREF( const_str_plain_write_count );
    PyList_SET_ITEM( const_list_04200af437d3ca9afb64727d4cd1a1b6_list, 2, const_str_plain_read_bytes ); Py_INCREF( const_str_plain_read_bytes );
    PyList_SET_ITEM( const_list_04200af437d3ca9afb64727d4cd1a1b6_list, 3, const_str_plain_write_bytes ); Py_INCREF( const_str_plain_write_bytes );
    PyList_SET_ITEM( const_list_04200af437d3ca9afb64727d4cd1a1b6_list, 4, const_str_plain_read_time ); Py_INCREF( const_str_plain_read_time );
    PyList_SET_ITEM( const_list_04200af437d3ca9afb64727d4cd1a1b6_list, 5, const_str_plain_write_time ); Py_INCREF( const_str_plain_write_time );
    const_str_digest_85a627319c236bc8f9aa4f6ec2242376 = UNSTREAM_STRING_ASCII( &constant_bin[ 4925556 ], 27, 0 );
    const_tuple_str_plain_darwin_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_darwin_tuple, 0, const_str_plain_darwin ); Py_INCREF( const_str_plain_darwin );
    const_str_digest_91bde82034c2ba995a6e7ea81b4ee6c4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4925583 ], 131, 0 );
    const_str_digest_6230f983e0ca3f1521efd26013c14e9b = UNSTREAM_STRING_ASCII( &constant_bin[ 4925714 ], 78, 0 );
    const_str_digest_95896ac3fdecfb6ca65cbe556616215c = UNSTREAM_STRING_ASCII( &constant_bin[ 4925792 ], 140, 0 );
    const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_input_dict = UNSTREAM_STRING_ASCII( &constant_bin[ 4925932 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 1, const_str_plain_input_dict ); Py_INCREF( const_str_plain_input_dict );
    PyTuple_SET_ITEM( const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain_old_dict = UNSTREAM_STRING_ASCII( &constant_bin[ 4925942 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 3, const_str_plain_old_dict ); Py_INCREF( const_str_plain_old_dict );
    const_str_plain_gone_keys = UNSTREAM_STRING_ASCII( &constant_bin[ 4925950 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 4, const_str_plain_gone_keys ); Py_INCREF( const_str_plain_gone_keys );
    const_str_plain_gone_key = UNSTREAM_STRING_ASCII( &constant_bin[ 4925950 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 5, const_str_plain_gone_key ); Py_INCREF( const_str_plain_gone_key );
    const_str_plain_remkey = UNSTREAM_STRING_ASCII( &constant_bin[ 4925959 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 6, const_str_plain_remkey ); Py_INCREF( const_str_plain_remkey );
    const_str_digest_af5c0b95f5f11bc66744e3de077488bb = UNSTREAM_STRING_ASCII( &constant_bin[ 4925965 ], 32, 0 );
    const_list_str_plain_path_str_plain_fd_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_path_str_plain_fd_list, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyList_SET_ITEM( const_list_str_plain_path_str_plain_fd_list, 1, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    const_list_66ba68b9593ee86a3b33193aff923a5b_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_66ba68b9593ee86a3b33193aff923a5b_list, 0, const_str_plain_ctx_switches ); Py_INCREF( const_str_plain_ctx_switches );
    PyList_SET_ITEM( const_list_66ba68b9593ee86a3b33193aff923a5b_list, 1, const_str_plain_interrupts ); Py_INCREF( const_str_plain_interrupts );
    PyList_SET_ITEM( const_list_66ba68b9593ee86a3b33193aff923a5b_list, 2, const_str_plain_soft_interrupts ); Py_INCREF( const_str_plain_soft_interrupts );
    PyList_SET_ITEM( const_list_66ba68b9593ee86a3b33193aff923a5b_list, 3, const_str_plain_syscalls ); Py_INCREF( const_str_plain_syscalls );
    const_str_digest_fc55a6ec1010a1560f644da71f4b3cba = UNSTREAM_STRING_ASCII( &constant_bin[ 4925997 ], 16, 0 );
    const_str_digest_8c8c0124d3f75932fd5693c97f1e2e27 = UNSTREAM_STRING_ASCII( &constant_bin[ 4926013 ], 12, 0 );
    const_str_plain_netbsd = UNSTREAM_STRING_ASCII( &constant_bin[ 4926025 ], 6, 1 );
    const_str_plain_WINDOWS_ = UNSTREAM_STRING_ASCII( &constant_bin[ 4926031 ], 8, 1 );
    const_str_plain_sleeping = UNSTREAM_STRING_ASCII( &constant_bin[ 4926039 ], 8, 1 );
    const_tuple_str_plain_netbsd_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_netbsd_tuple, 0, const_str_plain_netbsd ); Py_INCREF( const_str_plain_netbsd );
    const_str_plain__wn = UNSTREAM_STRING_ASCII( &constant_bin[ 4926047 ], 3, 1 );
    const_tuple_str_plain_freebsd_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_freebsd_tuple, 0, const_str_plain_freebsd ); Py_INCREF( const_str_plain_freebsd );
    const_list_ce761954676b292932e8dabc7e5b1c77_list = PyList_New( 5 );
    PyList_SET_ITEM( const_list_ce761954676b292932e8dabc7e5b1c77_list, 0, const_str_plain_family ); Py_INCREF( const_str_plain_family );
    PyList_SET_ITEM( const_list_ce761954676b292932e8dabc7e5b1c77_list, 1, const_str_plain_address ); Py_INCREF( const_str_plain_address );
    PyList_SET_ITEM( const_list_ce761954676b292932e8dabc7e5b1c77_list, 2, const_str_plain_netmask ); Py_INCREF( const_str_plain_netmask );
    PyList_SET_ITEM( const_list_ce761954676b292932e8dabc7e5b1c77_list, 3, const_str_plain_broadcast ); Py_INCREF( const_str_plain_broadcast );
    PyList_SET_ITEM( const_list_ce761954676b292932e8dabc7e5b1c77_list, 4, const_str_plain_ptp ); Py_INCREF( const_str_plain_ptp );
    const_tuple_str_plain_SOCK_DGRAM_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SOCK_DGRAM_tuple, 0, const_str_plain_SOCK_DGRAM ); Py_INCREF( const_str_plain_SOCK_DGRAM );
    const_str_digest_f1920876d07de7b4ca17e71fd0a07cbe = UNSTREAM_STRING_ASCII( &constant_bin[ 4926050 ], 23, 0 );
    const_str_plain__WrapNumbers = UNSTREAM_STRING_ASCII( &constant_bin[ 4925997 ], 12, 1 );
    const_list_e4eda79a105ca56e947928094e495dab_list = PyList_New( 5 );
    PyList_SET_ITEM( const_list_e4eda79a105ca56e947928094e495dab_list, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyList_SET_ITEM( const_list_e4eda79a105ca56e947928094e495dab_list, 1, const_str_plain_terminal ); Py_INCREF( const_str_plain_terminal );
    PyList_SET_ITEM( const_list_e4eda79a105ca56e947928094e495dab_list, 2, const_str_plain_host ); Py_INCREF( const_str_plain_host );
    PyList_SET_ITEM( const_list_e4eda79a105ca56e947928094e495dab_list, 3, const_str_plain_started ); Py_INCREF( const_str_plain_started );
    PyList_SET_ITEM( const_list_e4eda79a105ca56e947928094e495dab_list, 4, const_str_plain_pid ); Py_INCREF( const_str_plain_pid );
    const_tuple_str_plain_openbsd_tuple = PyTuple_New( 1 );
    const_str_plain_openbsd = UNSTREAM_STRING_ASCII( &constant_bin[ 4926073 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_openbsd_tuple, 0, const_str_plain_openbsd ); Py_INCREF( const_str_plain_openbsd );
    const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple = PyTuple_New( 13 );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 1, const_str_plain_input_dict ); Py_INCREF( const_str_plain_input_dict );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 3, const_str_plain_old_dict ); Py_INCREF( const_str_plain_old_dict );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 4, const_str_plain_new_dict ); Py_INCREF( const_str_plain_new_dict );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 5, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    const_str_plain_input_tuple = UNSTREAM_STRING_ASCII( &constant_bin[ 4926080 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 6, const_str_plain_input_tuple ); Py_INCREF( const_str_plain_input_tuple );
    const_str_plain_old_tuple = UNSTREAM_STRING_ASCII( &constant_bin[ 4926091 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 7, const_str_plain_old_tuple ); Py_INCREF( const_str_plain_old_tuple );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 8, const_str_plain_bits ); Py_INCREF( const_str_plain_bits );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 9, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_plain_input_value = UNSTREAM_STRING_ASCII( &constant_bin[ 4926100 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 10, const_str_plain_input_value ); Py_INCREF( const_str_plain_input_value );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 11, const_str_plain_old_value ); Py_INCREF( const_str_plain_old_value );
    PyTuple_SET_ITEM( const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 12, const_str_plain_remkey ); Py_INCREF( const_str_plain_remkey );
    const_str_plain_errout = UNSTREAM_STRING_ASCII( &constant_bin[ 4919629 ], 6, 1 );
    const_str_digest_cb4806a92f0a38e89c5326d8bded12ed = UNSTREAM_STRING_ASCII( &constant_bin[ 4926111 ], 107, 0 );
    const_tuple_str_plain_AF_UNIX_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_AF_UNIX_tuple, 0, const_str_plain_AF_UNIX ); Py_INCREF( const_str_plain_AF_UNIX );
    const_str_plain_LISTEN = UNSTREAM_STRING_ASCII( &constant_bin[ 4913992 ], 6, 1 );
    const_list_str_plain_percent_str_plain_secsleft_str_plain_power_plugged_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_percent_str_plain_secsleft_str_plain_power_plugged_list, 0, const_str_plain_percent ); Py_INCREF( const_str_plain_percent );
    PyList_SET_ITEM( const_list_str_plain_percent_str_plain_secsleft_str_plain_power_plugged_list, 1, const_str_plain_secsleft ); Py_INCREF( const_str_plain_secsleft );
    PyList_SET_ITEM( const_list_str_plain_percent_str_plain_secsleft_str_plain_power_plugged_list, 2, const_str_plain_power_plugged ); Py_INCREF( const_str_plain_power_plugged );
    const_str_plain_ESTABLISHED = UNSTREAM_STRING_ASCII( &constant_bin[ 4913852 ], 11, 1 );
    const_str_plain_system_time = UNSTREAM_STRING_ASCII( &constant_bin[ 4920702 ], 11, 1 );
    const_tuple_str_plain_sock_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_sock_tuple, 0, const_str_plain_sock ); Py_INCREF( const_str_plain_sock );
    const_list_str_plain_voluntary_str_plain_involuntary_list = PyList_New( 2 );
    const_str_plain_voluntary = UNSTREAM_STRING_ASCII( &constant_bin[ 4920917 ], 9, 1 );
    PyList_SET_ITEM( const_list_str_plain_voluntary_str_plain_involuntary_list, 0, const_str_plain_voluntary ); Py_INCREF( const_str_plain_voluntary );
    const_str_plain_involuntary = UNSTREAM_STRING_ASCII( &constant_bin[ 4920931 ], 11, 1 );
    PyList_SET_ITEM( const_list_str_plain_voluntary_str_plain_involuntary_list, 1, const_str_plain_involuntary ); Py_INCREF( const_str_plain_involuntary );
    const_list_str_plain_label_str_plain_current_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_label_str_plain_current_list, 0, const_str_plain_label ); Py_INCREF( const_str_plain_label );
    PyList_SET_ITEM( const_list_str_plain_label_str_plain_current_list, 1, const_str_plain_current ); Py_INCREF( const_str_plain_current );
    const_tuple_str_plain_self_str_plain_ret_str_plain_fun_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_ret_str_plain_fun_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_ret_str_plain_fun_tuple, 1, const_str_plain_ret ); Py_INCREF( const_str_plain_ret );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_ret_str_plain_fun_tuple, 2, const_str_plain_fun ); Py_INCREF( const_str_plain_fun );
    const_tuple_9798201f399d66f2ff5e0bbe3aa1d23f_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_9798201f399d66f2ff5e0bbe3aa1d23f_tuple, 0, const_str_plain_fun ); Py_INCREF( const_str_plain_fun );
    PyTuple_SET_ITEM( const_tuple_9798201f399d66f2ff5e0bbe3aa1d23f_tuple, 1, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    PyTuple_SET_ITEM( const_tuple_9798201f399d66f2ff5e0bbe3aa1d23f_tuple, 2, const_str_plain_cache_clear ); Py_INCREF( const_str_plain_cache_clear );
    PyTuple_SET_ITEM( const_tuple_9798201f399d66f2ff5e0bbe3aa1d23f_tuple, 3, const_str_plain_cache ); Py_INCREF( const_str_plain_cache );
    const_str_digest_d322b24329b5f9cbfeea0da3ac450745 = UNSTREAM_STRING_ASCII( &constant_bin[ 4926218 ], 21, 0 );
    const_list_487a7b5aabbaa0a3116539a420c71666_list = PyList_New( 6 );
    PyList_SET_ITEM( const_list_487a7b5aabbaa0a3116539a420c71666_list, 0, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyList_SET_ITEM( const_list_487a7b5aabbaa0a3116539a420c71666_list, 1, const_str_plain_family ); Py_INCREF( const_str_plain_family );
    PyList_SET_ITEM( const_list_487a7b5aabbaa0a3116539a420c71666_list, 2, const_str_plain_type ); Py_INCREF( const_str_plain_type );
    PyList_SET_ITEM( const_list_487a7b5aabbaa0a3116539a420c71666_list, 3, const_str_plain_laddr ); Py_INCREF( const_str_plain_laddr );
    PyList_SET_ITEM( const_list_487a7b5aabbaa0a3116539a420c71666_list, 4, const_str_plain_raddr ); Py_INCREF( const_str_plain_raddr );
    PyList_SET_ITEM( const_list_487a7b5aabbaa0a3116539a420c71666_list, 5, const_str_plain_status ); Py_INCREF( const_str_plain_status );
    const_str_digest_7f773a2c61aafb6080b6e3fa0c482cee = UNSTREAM_STRING_ASCII( &constant_bin[ 4608036 ], 12, 0 );
    const_list_4ce112f110e3f44d360b8e23a82a18a9_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_4ce112f110e3f44d360b8e23a82a18a9_list, 0, const_str_plain_device ); Py_INCREF( const_str_plain_device );
    PyList_SET_ITEM( const_list_4ce112f110e3f44d360b8e23a82a18a9_list, 1, const_str_plain_mountpoint ); Py_INCREF( const_str_plain_mountpoint );
    PyList_SET_ITEM( const_list_4ce112f110e3f44d360b8e23a82a18a9_list, 2, const_str_plain_fstype ); Py_INCREF( const_str_plain_fstype );
    PyList_SET_ITEM( const_list_4ce112f110e3f44d360b8e23a82a18a9_list, 3, const_str_plain_opts ); Py_INCREF( const_str_plain_opts );
    const_str_plain_CLOSE = UNSTREAM_STRING_ASCII( &constant_bin[ 2345807 ], 5, 1 );
    const_list_f504379bcc563b1d896d033185a6607f_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_f504379bcc563b1d896d033185a6607f_list, 0, const_str_plain_label ); Py_INCREF( const_str_plain_label );
    PyList_SET_ITEM( const_list_f504379bcc563b1d896d033185a6607f_list, 1, const_str_plain_current ); Py_INCREF( const_str_plain_current );
    PyList_SET_ITEM( const_list_f504379bcc563b1d896d033185a6607f_list, 2, const_str_plain_high ); Py_INCREF( const_str_plain_high );
    PyList_SET_ITEM( const_list_f504379bcc563b1d896d033185a6607f_list, 3, const_str_plain_critical ); Py_INCREF( const_str_plain_critical );
    const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 0, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 1, const_str_plain_ret ); Py_INCREF( const_str_plain_ret );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 2, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 3, const_str_plain_WINDOWS_ ); Py_INCREF( const_str_plain_WINDOWS_ );
    const_str_plain_next_pos = UNSTREAM_STRING_ASCII( &constant_bin[ 4926239 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 4, const_str_plain_next_pos ); Py_INCREF( const_str_plain_next_pos );
    const_str_plain_equal_pos = UNSTREAM_STRING_ASCII( &constant_bin[ 4926247 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 5, const_str_plain_equal_pos ); Py_INCREF( const_str_plain_equal_pos );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 6, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 7, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_str_digest_b2b793ac5e593d3efbfbe861ccb88056 = UNSTREAM_STRING_ASCII( &constant_bin[ 4926256 ], 134, 0 );
    const_str_digest_bbd7ba56388169475bb178ec7024ca39 = UNSTREAM_STRING_ASCII( &constant_bin[ 4926390 ], 21, 0 );
    const_str_plain__add_dict = UNSTREAM_STRING_ASCII( &constant_bin[ 4926411 ], 9, 1 );
    const_str_digest_99410fa7664fe22ccad236d080673163 = UNSTREAM_STRING_ASCII( &constant_bin[ 4926420 ], 53, 0 );
    const_tuple_str_plain_aix_tuple = PyTuple_New( 1 );
    const_str_plain_aix = UNSTREAM_STRING_ASCII( &constant_bin[ 4902499 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_aix_tuple, 0, const_str_plain_aix ); Py_INCREF( const_str_plain_aix );
    const_str_plain_FIN_WAIT2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4913916 ], 9, 1 );
    const_str_plain_stopped = UNSTREAM_STRING_ASCII( &constant_bin[ 1181565 ], 7, 1 );
    const_str_plain_packets_recv = UNSTREAM_STRING_ASCII( &constant_bin[ 4919521 ], 12, 1 );
    const_str_digest_5cc34510cf5423be6b3dd305d1fab306 = UNSTREAM_STRING_ASCII( &constant_bin[ 4926473 ], 22, 0 );
    const_str_plain_getfilesystemencodeerrors = UNSTREAM_STRING_ASCII( &constant_bin[ 4926495 ], 25, 1 );
    const_tuple_tuple_str_plain_sunos_str_plain_solaris_tuple_tuple = PyTuple_New( 1 );
    const_tuple_str_plain_sunos_str_plain_solaris_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_sunos_str_plain_solaris_tuple, 0, const_str_plain_sunos ); Py_INCREF( const_str_plain_sunos );
    PyTuple_SET_ITEM( const_tuple_str_plain_sunos_str_plain_solaris_tuple, 1, const_str_plain_solaris ); Py_INCREF( const_str_plain_solaris );
    PyTuple_SET_ITEM( const_tuple_tuple_str_plain_sunos_str_plain_solaris_tuple_tuple, 0, const_tuple_str_plain_sunos_str_plain_solaris_tuple ); Py_INCREF( const_tuple_str_plain_sunos_str_plain_solaris_tuple );
    const_str_digest_93046e394fea44e344d65326f6299d5e = UNSTREAM_STRING_ASCII( &constant_bin[ 4926520 ], 506, 0 );
    const_str_plain_waking = UNSTREAM_STRING_ASCII( &constant_bin[ 4927026 ], 6, 1 );
    const_str_digest_af97312a62c0414513bc3cf713b1b59a = UNSTREAM_STRING_ASCII( &constant_bin[ 4927032 ], 156, 0 );
    const_tuple_str_plain_path_str_plain_st_str_plain_err_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_st_str_plain_err_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_st_str_plain_err_tuple, 1, const_str_plain_st ); Py_INCREF( const_str_plain_st );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_st_str_plain_err_tuple, 2, const_str_plain_err ); Py_INCREF( const_str_plain_err );
    const_tuple_cff0ce383940b17a56ca81acef7d3732_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_cff0ce383940b17a56ca81acef7d3732_tuple, 0, const_str_plain_fun ); Py_INCREF( const_str_plain_fun );
    PyTuple_SET_ITEM( const_tuple_cff0ce383940b17a56ca81acef7d3732_tuple, 1, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    PyTuple_SET_ITEM( const_tuple_cff0ce383940b17a56ca81acef7d3732_tuple, 2, const_str_plain_cache_activate ); Py_INCREF( const_str_plain_cache_activate );
    PyTuple_SET_ITEM( const_tuple_cff0ce383940b17a56ca81acef7d3732_tuple, 3, const_str_plain_cache_deactivate ); Py_INCREF( const_str_plain_cache_deactivate );
    const_tuple_f461192a65f6ebc08a476f66d211dbef_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_f461192a65f6ebc08a476f66d211dbef_tuple, 0, const_str_plain_fun ); Py_INCREF( const_str_plain_fun );
    PyTuple_SET_ITEM( const_tuple_f461192a65f6ebc08a476f66d211dbef_tuple, 1, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    PyTuple_SET_ITEM( const_tuple_f461192a65f6ebc08a476f66d211dbef_tuple, 2, const_str_plain_inner ); Py_INCREF( const_str_plain_inner );
    PyTuple_SET_ITEM( const_tuple_f461192a65f6ebc08a476f66d211dbef_tuple, 3, const_str_plain_replacement ); Py_INCREF( const_str_plain_replacement );
    const_tuple_eaafdf540f6164da2d4a3e09760b2b4a_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_eaafdf540f6164da2d4a3e09760b2b4a_tuple, 0, const_str_plain_used ); Py_INCREF( const_str_plain_used );
    PyTuple_SET_ITEM( const_tuple_eaafdf540f6164da2d4a3e09760b2b4a_tuple, 1, const_str_plain_total ); Py_INCREF( const_str_plain_total );
    PyTuple_SET_ITEM( const_tuple_eaafdf540f6164da2d4a3e09760b2b4a_tuple, 2, const_str_plain_round_ ); Py_INCREF( const_str_plain_round_ );
    PyTuple_SET_ITEM( const_tuple_eaafdf540f6164da2d4a3e09760b2b4a_tuple, 3, const_str_plain_ret ); Py_INCREF( const_str_plain_ret );
    const_list_str_plain_ip_str_plain_port_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_ip_str_plain_port_list, 0, const_str_plain_ip ); Py_INCREF( const_str_plain_ip );
    PyList_SET_ITEM( const_list_str_plain_ip_str_plain_port_list, 1, const_str_plain_port ); Py_INCREF( const_str_plain_port );
    const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 0, const_str_plain_B ); Py_INCREF( const_str_plain_B );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 1, const_str_plain_K ); Py_INCREF( const_str_plain_K );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 2, const_str_plain_M ); Py_INCREF( const_str_plain_M );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 3, const_str_plain_G ); Py_INCREF( const_str_plain_G );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 4, const_str_plain_T ); Py_INCREF( const_str_plain_T );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 5, const_str_plain_P ); Py_INCREF( const_str_plain_P );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 6, const_str_plain_E ); Py_INCREF( const_str_plain_E );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 7, const_str_plain_Z ); Py_INCREF( const_str_plain_Z );
    PyTuple_SET_ITEM( const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple, 8, const_str_plain_Y ); Py_INCREF( const_str_plain_Y );
    const_str_digest_9d5da3b1c04b6be63df52845d9808c46 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927188 ], 46, 0 );
    const_tuple_str_plain_input_dict_str_plain_name_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_input_dict_str_plain_name_tuple, 0, const_str_plain_input_dict ); Py_INCREF( const_str_plain_input_dict );
    PyTuple_SET_ITEM( const_tuple_str_plain_input_dict_str_plain_name_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_list_e817dfda20ddb186bde2c54cef499933_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_e817dfda20ddb186bde2c54cef499933_list, 0, const_str_plain_total ); Py_INCREF( const_str_plain_total );
    PyList_SET_ITEM( const_list_e817dfda20ddb186bde2c54cef499933_list, 1, const_str_plain_used ); Py_INCREF( const_str_plain_used );
    PyList_SET_ITEM( const_list_e817dfda20ddb186bde2c54cef499933_list, 2, const_str_plain_free ); Py_INCREF( const_str_plain_free );
    PyList_SET_ITEM( const_list_e817dfda20ddb186bde2c54cef499933_list, 3, const_str_plain_percent ); Py_INCREF( const_str_plain_percent );
    const_str_plain_SYN_SENT = UNSTREAM_STRING_ASCII( &constant_bin[ 4913870 ], 8, 1 );
    const_str_plain_BatteryTime = UNSTREAM_STRING_ASCII( &constant_bin[ 4927234 ], 11, 1 );
    const_str_digest_3cd9631d27a7d42f4e2a7520ff729c49 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927245 ], 9, 0 );
    const_str_digest_49ff0a35e03d63667c353f2c0a9c7733 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927254 ], 23, 0 );
    const_list_str_plain_id_str_plain_user_time_str_plain_system_time_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_id_str_plain_user_time_str_plain_system_time_list, 0, const_str_plain_id ); Py_INCREF( const_str_plain_id );
    PyList_SET_ITEM( const_list_str_plain_id_str_plain_user_time_str_plain_system_time_list, 1, const_str_plain_user_time ); Py_INCREF( const_str_plain_user_time );
    PyList_SET_ITEM( const_list_str_plain_id_str_plain_user_time_str_plain_system_time_list, 2, const_str_plain_system_time ); Py_INCREF( const_str_plain_system_time );
    const_str_digest_f6856f5d282ebea61302c4adae8f9ada = UNSTREAM_STRING_ASCII( &constant_bin[ 4927277 ], 43, 0 );
    const_str_plain_LAST_ACK = UNSTREAM_STRING_ASCII( &constant_bin[ 4913977 ], 8, 1 );
    const_str_digest_1d916fa90b3796c4ab91e848071040be = UNSTREAM_STRING_ASCII( &constant_bin[ 4927320 ], 24, 0 );
    const_str_plain_parked = UNSTREAM_STRING_ASCII( &constant_bin[ 4927344 ], 6, 1 );
    const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple, 3, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    PyTuple_SET_ITEM( const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple, 4, const_str_plain_replacement ); Py_INCREF( const_str_plain_replacement );
    const_tuple_str_plain__WrapNumbers_tuple_empty_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain__WrapNumbers_tuple_empty_tuple, 0, const_str_plain__WrapNumbers ); Py_INCREF( const_str_plain__WrapNumbers );
    PyTuple_SET_ITEM( const_tuple_str_plain__WrapNumbers_tuple_empty_tuple, 1, const_tuple_empty ); Py_INCREF( const_tuple_empty );
    const_str_plain_CLOSING = UNSTREAM_STRING_ASCII( &constant_bin[ 4546967 ], 7, 1 );
    const_tuple_05d8568253270babe3d513d8bda8531d_tuple = PyTuple_New( 1 );
    const_tuple_str_digest_3f9fec55820f857461da4f1e12840da5_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_3f9fec55820f857461da4f1e12840da5_int_0_tuple, 0, const_str_digest_3f9fec55820f857461da4f1e12840da5 ); Py_INCREF( const_str_digest_3f9fec55820f857461da4f1e12840da5 );
    PyTuple_SET_ITEM( const_tuple_str_digest_3f9fec55820f857461da4f1e12840da5_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    PyTuple_SET_ITEM( const_tuple_05d8568253270babe3d513d8bda8531d_tuple, 0, const_tuple_str_digest_3f9fec55820f857461da4f1e12840da5_int_0_tuple ); Py_INCREF( const_tuple_str_digest_3f9fec55820f857461da4f1e12840da5_int_0_tuple );
    const_tuple_str_plain_replacement_str_plain_outer_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_replacement_str_plain_outer_tuple, 0, const_str_plain_replacement ); Py_INCREF( const_str_plain_replacement );
    PyTuple_SET_ITEM( const_tuple_str_plain_replacement_str_plain_outer_tuple, 1, const_str_plain_outer ); Py_INCREF( const_str_plain_outer );
    const_str_plain_SYN_RECV = UNSTREAM_STRING_ASCII( &constant_bin[ 4913885 ], 8, 1 );
    const_str_digest_457d32d6501e34673ad4aa2f55c371a6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927350 ], 35, 0 );
    const_str_digest_d738b7bb5a4adf77b8882bd7cfde13ad = UNSTREAM_STRING_ASCII( &constant_bin[ 4927385 ], 197, 0 );
    const_str_digest_28a10ab0cf555529690fb2e57e845886 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927582 ], 28, 0 );
    const_str_digest_4f68eb82902a9b7b547feaa053e622b7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927610 ], 156, 0 );
    const_tuple_str_plain_proc_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_proc_tuple, 0, const_str_plain_proc ); Py_INCREF( const_str_plain_proc );
    const_str_plain_reminders = UNSTREAM_STRING_ASCII( &constant_bin[ 4926371 ], 9, 1 );
    const_str_plain_dropout = UNSTREAM_STRING_ASCII( &constant_bin[ 4919760 ], 7, 1 );
    const_str_plain_bytes_recv = UNSTREAM_STRING_ASCII( &constant_bin[ 4919431 ], 10, 1 );
    const_tuple_type_ValueError_type_AttributeError_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_type_ValueError_type_AttributeError_tuple, 0, (PyObject *)PyExc_ValueError ); Py_INCREF( (PyObject *)PyExc_ValueError );
    PyTuple_SET_ITEM( const_tuple_type_ValueError_type_AttributeError_tuple, 1, (PyObject *)PyExc_AttributeError ); Py_INCREF( (PyObject *)PyExc_AttributeError );
    const_str_plain_errin = UNSTREAM_STRING_ASCII( &constant_bin[ 646718 ], 5, 1 );
    const_str_digest_f1996bba9b05fd6185e0a3aa39f0f0e9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927766 ], 17, 0 );
    const_list_7269f1f5bbfaf7e849943ff948940288_list = PyList_New( 6 );
    PyList_SET_ITEM( const_list_7269f1f5bbfaf7e849943ff948940288_list, 0, const_str_plain_total ); Py_INCREF( const_str_plain_total );
    PyList_SET_ITEM( const_list_7269f1f5bbfaf7e849943ff948940288_list, 1, const_str_plain_used ); Py_INCREF( const_str_plain_used );
    PyList_SET_ITEM( const_list_7269f1f5bbfaf7e849943ff948940288_list, 2, const_str_plain_free ); Py_INCREF( const_str_plain_free );
    PyList_SET_ITEM( const_list_7269f1f5bbfaf7e849943ff948940288_list, 3, const_str_plain_percent ); Py_INCREF( const_str_plain_percent );
    PyList_SET_ITEM( const_list_7269f1f5bbfaf7e849943ff948940288_list, 4, const_str_plain_sin ); Py_INCREF( const_str_plain_sin );
    PyList_SET_ITEM( const_list_7269f1f5bbfaf7e849943ff948940288_list, 5, const_str_plain_sout ); Py_INCREF( const_str_plain_sout );
    const_str_digest_c5c04d92c90c74d705f2d90dc328b3dd = UNSTREAM_STRING_ASCII( &constant_bin[ 4927783 ], 57, 0 );
    const_list_9db5dd841fb6eacf7ec042df99116523_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_9db5dd841fb6eacf7ec042df99116523_list, 0, const_str_plain_isup ); Py_INCREF( const_str_plain_isup );
    PyList_SET_ITEM( const_list_9db5dd841fb6eacf7ec042df99116523_list, 1, const_str_plain_duplex ); Py_INCREF( const_str_plain_duplex );
    PyList_SET_ITEM( const_list_9db5dd841fb6eacf7ec042df99116523_list, 2, const_str_plain_speed ); Py_INCREF( const_str_plain_speed );
    PyList_SET_ITEM( const_list_9db5dd841fb6eacf7ec042df99116523_list, 3, const_str_plain_mtu ); Py_INCREF( const_str_plain_mtu );
    const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list = PyList_New( 8 );
    const_str_plain_bytes_sent = UNSTREAM_STRING_ASCII( &constant_bin[ 4919389 ], 10, 1 );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 0, const_str_plain_bytes_sent ); Py_INCREF( const_str_plain_bytes_sent );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 1, const_str_plain_bytes_recv ); Py_INCREF( const_str_plain_bytes_recv );
    const_str_plain_packets_sent = UNSTREAM_STRING_ASCII( &constant_bin[ 4919477 ], 12, 1 );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 2, const_str_plain_packets_sent ); Py_INCREF( const_str_plain_packets_sent );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 3, const_str_plain_packets_recv ); Py_INCREF( const_str_plain_packets_recv );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 4, const_str_plain_errin ); Py_INCREF( const_str_plain_errin );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 5, const_str_plain_errout ); Py_INCREF( const_str_plain_errout );
    const_str_plain_dropin = UNSTREAM_STRING_ASCII( &constant_bin[ 4919687 ], 6, 1 );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 6, const_str_plain_dropin ); Py_INCREF( const_str_plain_dropin );
    PyList_SET_ITEM( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list, 7, const_str_plain_dropout ); Py_INCREF( const_str_plain_dropout );
    const_list_80a0510781d2d124eb0a6af3f3458443_list = PyList_New( 7 );
    PyList_SET_ITEM( const_list_80a0510781d2d124eb0a6af3f3458443_list, 0, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyList_SET_ITEM( const_list_80a0510781d2d124eb0a6af3f3458443_list, 1, const_str_plain_family ); Py_INCREF( const_str_plain_family );
    PyList_SET_ITEM( const_list_80a0510781d2d124eb0a6af3f3458443_list, 2, const_str_plain_type ); Py_INCREF( const_str_plain_type );
    PyList_SET_ITEM( const_list_80a0510781d2d124eb0a6af3f3458443_list, 3, const_str_plain_laddr ); Py_INCREF( const_str_plain_laddr );
    PyList_SET_ITEM( const_list_80a0510781d2d124eb0a6af3f3458443_list, 4, const_str_plain_raddr ); Py_INCREF( const_str_plain_raddr );
    PyList_SET_ITEM( const_list_80a0510781d2d124eb0a6af3f3458443_list, 5, const_str_plain_status ); Py_INCREF( const_str_plain_status );
    PyList_SET_ITEM( const_list_80a0510781d2d124eb0a6af3f3458443_list, 6, const_str_plain_pid ); Py_INCREF( const_str_plain_pid );
    const_str_digest_c21138c8d575fc0b2b6ff4782a202f72 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927262 ], 14, 0 );
    const_str_digest_d2a1a54d8c7e0284a0763d37c4a6b2ab = UNSTREAM_STRING_ASCII( &constant_bin[ 4927840 ], 127, 0 );
    const_tuple_str_plain_SOCK_STREAM_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SOCK_STREAM_tuple, 0, const_str_plain_SOCK_STREAM ); Py_INCREF( const_str_plain_SOCK_STREAM );
    const_tuple_str_plain_path_str_plain_err_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_err_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_err_tuple, 1, const_str_plain_err ); Py_INCREF( const_str_plain_err );
    const_str_digest_386170845bb8c4a4ab83068de03309f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4927967 ], 48, 0 );
    const_str_digest_3edef1fa3eed954f4e1fabc8924bb36f = UNSTREAM_STRING_ASCII( &constant_bin[ 4928015 ], 62, 0 );
    const_str_digest_716f8d9d7522e1bc954f2cca2624926c = UNSTREAM_STRING_ASCII( &constant_bin[ 4928077 ], 47, 0 );
    const_tuple_5174cec25d92df58c36214d4067bc535_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 0, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 1, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 2, const_str_plain_symbols ); Py_INCREF( const_str_plain_symbols );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 3, const_str_plain_prefix ); Py_INCREF( const_str_plain_prefix );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 4, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 5, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 6, const_str_plain_symbol ); Py_INCREF( const_str_plain_symbol );
    PyTuple_SET_ITEM( const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 7, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_str_digest_ad233afd4f5eda5adddba312e8fe0a80 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928124 ], 10, 0 );
    const_tuple_str_plain_self_str_plain_input_dict_str_plain_name_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_input_dict_str_plain_name_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_input_dict_str_plain_name_tuple, 1, const_str_plain_input_dict ); Py_INCREF( const_str_plain_input_dict );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_input_dict_str_plain_name_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain__remove_dead_reminders = UNSTREAM_STRING_ASCII( &constant_bin[ 4927363 ], 22, 1 );
    const_str_plain_TIME_WAIT = UNSTREAM_STRING_ASCII( &constant_bin[ 4913932 ], 9, 1 );
    const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple, 2, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple, 3, const_str_plain_ret ); Py_INCREF( const_str_plain_ret );
    PyTuple_SET_ITEM( const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple, 4, const_str_plain_cache ); Py_INCREF( const_str_plain_cache );
    PyTuple_SET_ITEM( const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple, 5, const_str_plain_fun ); Py_INCREF( const_str_plain_fun );
    const_str_digest_2045132428b7bed20b21befde72ba292 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928134 ], 106, 0 );
    const_str_digest_e504a1c5fb44d69b43b64780b96360d6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928240 ], 168, 0 );
    const_str_plain_reminder_keys = UNSTREAM_STRING_ASCII( &constant_bin[ 4928408 ], 13, 1 );
    const_str_digest_8154c1b785576b641e6ce5bfaf028ff5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928421 ], 53, 0 );
    const_tuple_str_digest_d322b24329b5f9cbfeea0da3ac450745_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_d322b24329b5f9cbfeea0da3ac450745_tuple, 0, const_str_digest_d322b24329b5f9cbfeea0da3ac450745 ); Py_INCREF( const_str_digest_d322b24329b5f9cbfeea0da3ac450745 );
    const_str_digest_230f09e45b705cbc9bc92403ddd4c5f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928474 ], 129, 0 );
    const_str_digest_5d6d330dc4d79bec1ba9037cf33d5c79 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928603 ], 50, 0 );
    const_list_str_plain_ioclass_str_plain_value_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_ioclass_str_plain_value_list, 0, const_str_plain_ioclass ); Py_INCREF( const_str_plain_ioclass );
    PyList_SET_ITEM( const_list_str_plain_ioclass_str_plain_value_list, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_list_str_plain_real_str_plain_effective_str_plain_saved_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_real_str_plain_effective_str_plain_saved_list, 0, const_str_plain_real ); Py_INCREF( const_str_plain_real );
    PyList_SET_ITEM( const_list_str_plain_real_str_plain_effective_str_plain_saved_list, 1, const_str_plain_effective ); Py_INCREF( const_str_plain_effective );
    PyList_SET_ITEM( const_list_str_plain_real_str_plain_effective_str_plain_saved_list, 2, const_str_plain_saved ); Py_INCREF( const_str_plain_saved );
    const_str_plain_AddressType = UNSTREAM_STRING_ASCII( &constant_bin[ 4928653 ], 11, 1 );
    const_str_digest_115252dd9884ce3050fc49a9776440d5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928664 ], 24, 0 );
    const_str_digest_e53dfc57c7e14e31d0998887df0f3e21 = UNSTREAM_STRING_ASCII( &constant_bin[ 4928688 ], 67, 0 );
    const_str_plain_CLOSE_WAIT = UNSTREAM_STRING_ASCII( &constant_bin[ 4913960 ], 10, 1 );
    const_list_1f83fcd5f40aa6957d8a184c26f07a11_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 4928755 ], 926 );
    const_list_str_plain_current_str_plain_min_str_plain_max_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_current_str_plain_min_str_plain_max_list, 0, const_str_plain_current ); Py_INCREF( const_str_plain_current );
    PyList_SET_ITEM( const_list_str_plain_current_str_plain_min_str_plain_max_list, 1, const_str_plain_min ); Py_INCREF( const_str_plain_min );
    PyList_SET_ITEM( const_list_str_plain_current_str_plain_min_str_plain_max_list, 2, const_str_plain_max ); Py_INCREF( const_str_plain_max );
    const_str_digest_6c181c57aa262d51beb7a1726d9302b5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4929681 ], 286, 0 );
    const_str_digest_4ec72dfd8b162da383cac847fd2af2fc = UNSTREAM_STRING_ASCII( &constant_bin[ 4929967 ], 56, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_psutil$_common( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_641a34ae48b2aa2a3be9421131bc3092;
static PyCodeObject *codeobj_3eb634e8d4080b8c4c77a2731a7d5b07;
static PyCodeObject *codeobj_eb427e220e252bc7ee7380d6c9a0dd4c;
static PyCodeObject *codeobj_0928eb9f20a13780a4806d89700f19fe;
static PyCodeObject *codeobj_17cd2ecfb0288cbaa7706a9e05df80cc;
static PyCodeObject *codeobj_ef16a5a470b8dcb15406d714b9e72dfc;
static PyCodeObject *codeobj_19ab808ff3653aac5d36f73d1d924f02;
static PyCodeObject *codeobj_5b6934d7ca2e72fbb2fbe1426870eb4b;
static PyCodeObject *codeobj_589f1beda036b7ac243111b1399dc433;
static PyCodeObject *codeobj_69a9ead8ba6e6ca78460c619c25a540c;
static PyCodeObject *codeobj_950f39cfb48f10fc56597463c2229192;
static PyCodeObject *codeobj_5a4482fce79cd9cb968a4339ebbc16fb;
static PyCodeObject *codeobj_06c6ff0ff3b5724f864321db3870e1a4;
static PyCodeObject *codeobj_bf20f80eec9973461bde9cd8d9b8a9d5;
static PyCodeObject *codeobj_015df25e645212257ddbaaf1f497720b;
static PyCodeObject *codeobj_f435564dcf5a9e89cb4b81ae6f5553f5;
static PyCodeObject *codeobj_f7fc201a081ec45c9e3477e92757801b;
static PyCodeObject *codeobj_04839f818328ef15580890c4cd786f57;
static PyCodeObject *codeobj_3bd930188379a197c6f391320fec4502;
static PyCodeObject *codeobj_1d0c897b6dd90f96bd44e10f008a1661;
static PyCodeObject *codeobj_a0e916af95934d7eab7741981deb2f51;
static PyCodeObject *codeobj_896e38fe86b92d930b83395b0b2a72e3;
static PyCodeObject *codeobj_901790350ed744472583113cd8b2bff7;
static PyCodeObject *codeobj_8b587c31f069329851643cb68c828c33;
static PyCodeObject *codeobj_57950a6fa80e5368b9a8128bcd9382da;
static PyCodeObject *codeobj_ead688a2167ee1cf40fa04f3f17e9f12;
static PyCodeObject *codeobj_8259023325814c68d4e851583a18d148;
static PyCodeObject *codeobj_53d9763bad7ab7dc936ba3bf85cfa331;
static PyCodeObject *codeobj_ebbee4ed4136870a4f406fd32eea55b2;
static PyCodeObject *codeobj_02c36612f9a1e0db15c02518dfab19ce;
static PyCodeObject *codeobj_e04608805b76764e8ec9b47cdfb85c3a;
static PyCodeObject *codeobj_38c8ef5f90fd9de5b95bf5d0f86c9543;
static PyCodeObject *codeobj_c51fa982b1f0c2958d4822a1691e57aa;
static PyCodeObject *codeobj_53859f37c137aa1d0a547e9b72865b8c;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_f1996bba9b05fd6185e0a3aa39f0f0e9 );
    codeobj_641a34ae48b2aa2a3be9421131bc3092 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_49ff0a35e03d63667c353f2c0a9c7733, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_3eb634e8d4080b8c4c77a2731a7d5b07 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_BatteryTime, 142, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_eb427e220e252bc7ee7380d6c9a0dd4c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_NicDuplex, 130, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_0928eb9f20a13780a4806d89700f19fe = MAKE_CODEOBJ( module_filename_obj, const_str_plain__WrapNumbers, 485, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_17cd2ecfb0288cbaa7706a9e05df80cc = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 490, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ef16a5a470b8dcb15406d714b9e72dfc = MAKE_CODEOBJ( module_filename_obj, const_str_plain__add_dict, 496, const_tuple_str_plain_self_str_plain_input_dict_str_plain_name_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_19ab808ff3653aac5d36f73d1d924f02 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__remove_dead_reminders, 504, const_tuple_ecf67edc36d7472c976af0d8c0bec103_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5b6934d7ca2e72fbb2fbe1426870eb4b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_bytes2human, 604, const_tuple_5174cec25d92df58c36214d4067bc535_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_589f1beda036b7ac243111b1399dc433 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cache_activate, 352, const_tuple_str_plain_proc_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_69a9ead8ba6e6ca78460c619c25a540c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cache_clear, 303, const_tuple_str_plain_cache_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_950f39cfb48f10fc56597463c2229192 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cache_clear, 554, const_tuple_str_plain_self_str_plain_name_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5a4482fce79cd9cb968a4339ebbc16fb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cache_deactivate, 357, const_tuple_str_plain_proc_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_06c6ff0ff3b5724f864321db3870e1a4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cache_info, 566, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bf20f80eec9973461bde9cd8d9b8a9d5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_decode, 630, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_015df25e645212257ddbaaf1f497720b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_decode, 633, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f435564dcf5a9e89cb4b81ae6f5553f5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_deprecated_method, 467, const_tuple_str_plain_replacement_str_plain_outer_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f7fc201a081ec45c9e3477e92757801b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_procfs_path, 624, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_04839f818328ef15580890c4cd786f57 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_inner, 477, const_tuple_37a82b06eb4ef736c53ebb3317d690ae_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_3bd930188379a197c6f391320fec4502 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_isfile_strict, 369, const_tuple_str_plain_path_str_plain_st_str_plain_err_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1d0c897b6dd90f96bd44e10f008a1661 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_memoize, 280, const_tuple_9798201f399d66f2ff5e0bbe3aa1d23f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a0e916af95934d7eab7741981deb2f51 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_memoize_when_activated, 312, const_tuple_cff0ce383940b17a56ca81acef7d3732_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_896e38fe86b92d930b83395b0b2a72e3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_open_binary, 586, const_tuple_str_plain_fname_str_plain_kwargs_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_901790350ed744472583113cd8b2bff7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_open_text, 590, const_tuple_str_plain_fname_str_plain_kwargs_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_8b587c31f069329851643cb68c828c33 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_outer, 471, const_tuple_f461192a65f6ebc08a476f66d211dbef_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_57950a6fa80e5368b9a8128bcd9382da = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parse_environ_block, 413, const_tuple_e912ba316c8d3903e827245b27ff7b7f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ead688a2167ee1cf40fa04f3f17e9f12 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_path_exists_strict, 384, const_tuple_str_plain_path_str_plain_err_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8259023325814c68d4e851583a18d148 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_run, 515, const_tuple_48f3860d9ff20952f2fd13f6e8dc4399_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_53d9763bad7ab7dc936ba3bf85cfa331 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_sockfam_to_enum, 441, const_tuple_str_plain_num_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ebbee4ed4136870a4f406fd32eea55b2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_socktype_to_enum, 454, const_tuple_str_plain_num_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_02c36612f9a1e0db15c02518dfab19ce = MAKE_CODEOBJ( module_filename_obj, const_str_plain_supports_ipv6, 399, const_tuple_str_plain_sock_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e04608805b76764e8ec9b47cdfb85c3a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_usage_percent, 268, const_tuple_eaafdf540f6164da2d4a3e09760b2b4a_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_38c8ef5f90fd9de5b95bf5d0f86c9543 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrap_numbers, 572, const_tuple_str_plain_input_dict_str_plain_name_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c51fa982b1f0c2958d4822a1691e57aa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 294, const_tuple_2505a37e66525989bd16a63d6d1d0c29_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_53859f37c137aa1d0a547e9b72865b8c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 338, const_tuple_str_plain_self_str_plain_ret_str_plain_fun_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method$$$function_1_outer(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method$$$function_1_outer$$$function_1_inner(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_11___init__(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_12__add_dict(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_13__remove_dead_reminders(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_14_run(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_15_cache_clear( PyObject *defaults );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_16_cache_info(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_17_wrap_numbers(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_18_open_binary(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_19_open_text(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_1_usage_percent( PyObject *defaults );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_20_bytes2human( PyObject *defaults );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_21_get_procfs_path(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_22_decode(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_23_decode(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_2_memoize(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_2_memoize$$$function_1_wrapper(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_2_memoize$$$function_2_cache_clear(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_2_cache_activate(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_4_isfile_strict(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_5_path_exists_strict(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_6_supports_ipv6(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_7_parse_environ_block(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_8_sockfam_to_enum(  );


static PyObject *MAKE_FUNCTION_psutil$_common$$$function_9_socktype_to_enum(  );


// The module function definitions.
static PyObject *impl_psutil$_common$$$function_1_usage_percent( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_used = python_pars[ 0 ];
    PyObject *par_total = python_pars[ 1 ];
    PyObject *par_round_ = python_pars[ 2 ];
    PyObject *var_ret = NULL;
    struct Nuitka_FrameObject *frame_e04608805b76764e8ec9b47cdfb85c3a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_e04608805b76764e8ec9b47cdfb85c3a = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e04608805b76764e8ec9b47cdfb85c3a, codeobj_e04608805b76764e8ec9b47cdfb85c3a, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e04608805b76764e8ec9b47cdfb85c3a = cache_frame_e04608805b76764e8ec9b47cdfb85c3a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e04608805b76764e8ec9b47cdfb85c3a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e04608805b76764e8ec9b47cdfb85c3a ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( par_used );
        tmp_left_name_2 = par_used;
        CHECK_OBJECT( par_total );
        tmp_right_name_1 = par_total;
        tmp_left_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_right_name_2 = const_int_pos_100;
        tmp_assign_source_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( var_ret == NULL );
        var_ret = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_e04608805b76764e8ec9b47cdfb85c3a, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_e04608805b76764e8ec9b47cdfb85c3a, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ZeroDivisionError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            nuitka_bool tmp_condition_result_2;
            int tmp_or_left_truth_1;
            nuitka_bool tmp_or_left_value_1;
            nuitka_bool tmp_or_right_value_1;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            CHECK_OBJECT( par_used );
            tmp_isinstance_inst_1 = par_used;
            tmp_isinstance_cls_1 = (PyObject *)&PyFloat_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
            tmp_or_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            CHECK_OBJECT( par_total );
            tmp_isinstance_inst_2 = par_total;
            tmp_isinstance_cls_2 = (PyObject *)&PyFloat_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
            tmp_or_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            tmp_condition_result_2 = tmp_or_left_value_1;
            or_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            tmp_assign_source_2 = const_float_0_0;
            goto condexpr_end_1;
            condexpr_false_1:;
            tmp_assign_source_2 = const_int_0;
            condexpr_end_1:;
            assert( var_ret == NULL );
            Py_INCREF( tmp_assign_source_2 );
            var_ret = tmp_assign_source_2;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 270;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_e04608805b76764e8ec9b47cdfb85c3a->m_frame) frame_e04608805b76764e8ec9b47cdfb85c3a->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_1_usage_percent );
    return NULL;
    // End of try:
    try_end_1:;
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_round_ );
        tmp_compexpr_left_2 = par_round_;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_round );
            assert( tmp_called_name_1 != NULL );
            if ( var_ret == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ret" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 275;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = var_ret;
            CHECK_OBJECT( par_round_ );
            tmp_args_element_name_2 = par_round_;
            frame_e04608805b76764e8ec9b47cdfb85c3a->m_frame.f_lineno = 275;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_2;
        branch_no_2:;
        if ( var_ret == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ret" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 277;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = var_ret;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_end_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e04608805b76764e8ec9b47cdfb85c3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e04608805b76764e8ec9b47cdfb85c3a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e04608805b76764e8ec9b47cdfb85c3a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e04608805b76764e8ec9b47cdfb85c3a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e04608805b76764e8ec9b47cdfb85c3a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e04608805b76764e8ec9b47cdfb85c3a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e04608805b76764e8ec9b47cdfb85c3a,
        type_description_1,
        par_used,
        par_total,
        par_round_,
        var_ret
    );


    // Release cached frame.
    if ( frame_e04608805b76764e8ec9b47cdfb85c3a == cache_frame_e04608805b76764e8ec9b47cdfb85c3a )
    {
        Py_DECREF( frame_e04608805b76764e8ec9b47cdfb85c3a );
    }
    cache_frame_e04608805b76764e8ec9b47cdfb85c3a = NULL;

    assertFrameObject( frame_e04608805b76764e8ec9b47cdfb85c3a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_1_usage_percent );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_used );
    Py_DECREF( par_used );
    par_used = NULL;

    CHECK_OBJECT( (PyObject *)par_total );
    Py_DECREF( par_total );
    par_total = NULL;

    CHECK_OBJECT( (PyObject *)par_round_ );
    Py_DECREF( par_round_ );
    par_round_ = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_used );
    Py_DECREF( par_used );
    par_used = NULL;

    CHECK_OBJECT( (PyObject *)par_total );
    Py_DECREF( par_total );
    par_total = NULL;

    CHECK_OBJECT( (PyObject *)par_round_ );
    Py_DECREF( par_round_ );
    par_round_ = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_1_usage_percent );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_2_memoize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_fun = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_wrapper = NULL;
    PyObject *var_cache_clear = NULL;
    struct Nuitka_CellObject *var_cache = PyCell_EMPTY();
    struct Nuitka_FrameObject *frame_1d0c897b6dd90f96bd44e10f008a1661;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_1d0c897b6dd90f96bd44e10f008a1661 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1d0c897b6dd90f96bd44e10f008a1661, codeobj_1d0c897b6dd90f96bd44e10f008a1661, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1d0c897b6dd90f96bd44e10f008a1661 = cache_frame_1d0c897b6dd90f96bd44e10f008a1661;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1d0c897b6dd90f96bd44e10f008a1661 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1d0c897b6dd90f96bd44e10f008a1661 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 294;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( PyCell_GET( par_fun ) );
        tmp_args_element_name_1 = PyCell_GET( par_fun );
        frame_1d0c897b6dd90f96bd44e10f008a1661->m_frame.f_lineno = 294;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_wraps, call_args );
        }

        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_psutil$_common$$$function_2_memoize$$$function_1_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] = var_cache;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] = par_fun;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] );


        frame_1d0c897b6dd90f96bd44e10f008a1661->m_frame.f_lineno = 294;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }
        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_psutil$_common$$$function_2_memoize$$$function_2_cache_clear(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = var_cache;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );


        assert( var_cache_clear == NULL );
        var_cache_clear = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyDict_New();
        assert( PyCell_GET( var_cache ) == NULL );
        PyCell_SET( var_cache, tmp_assign_source_3 );

    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_cache_clear );
        tmp_assattr_name_1 = var_cache_clear;
        CHECK_OBJECT( var_wrapper );
        tmp_assattr_target_1 = var_wrapper;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cache_clear, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 308;
            type_description_1 = "cooc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d0c897b6dd90f96bd44e10f008a1661 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d0c897b6dd90f96bd44e10f008a1661 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1d0c897b6dd90f96bd44e10f008a1661, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1d0c897b6dd90f96bd44e10f008a1661->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1d0c897b6dd90f96bd44e10f008a1661, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1d0c897b6dd90f96bd44e10f008a1661,
        type_description_1,
        par_fun,
        var_wrapper,
        var_cache_clear,
        var_cache
    );


    // Release cached frame.
    if ( frame_1d0c897b6dd90f96bd44e10f008a1661 == cache_frame_1d0c897b6dd90f96bd44e10f008a1661 )
    {
        Py_DECREF( frame_1d0c897b6dd90f96bd44e10f008a1661 );
    }
    cache_frame_1d0c897b6dd90f96bd44e10f008a1661 = NULL;

    assertFrameObject( frame_1d0c897b6dd90f96bd44e10f008a1661 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_2_memoize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fun );
    Py_DECREF( par_fun );
    par_fun = NULL;

    CHECK_OBJECT( (PyObject *)var_wrapper );
    Py_DECREF( var_wrapper );
    var_wrapper = NULL;

    CHECK_OBJECT( (PyObject *)var_cache_clear );
    Py_DECREF( var_cache_clear );
    var_cache_clear = NULL;

    CHECK_OBJECT( (PyObject *)var_cache );
    Py_DECREF( var_cache );
    var_cache = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fun );
    Py_DECREF( par_fun );
    par_fun = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    Py_XDECREF( var_cache_clear );
    var_cache_clear = NULL;

    CHECK_OBJECT( (PyObject *)var_cache );
    Py_DECREF( var_cache );
    var_cache = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_2_memoize );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_2_memoize$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_key = NULL;
    PyObject *var_ret = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    struct Nuitka_FrameObject *frame_c51fa982b1f0c2958d4822a1691e57aa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_c51fa982b1f0c2958d4822a1691e57aa = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c51fa982b1f0c2958d4822a1691e57aa, codeobj_c51fa982b1f0c2958d4822a1691e57aa, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c51fa982b1f0c2958d4822a1691e57aa = cache_frame_c51fa982b1f0c2958d4822a1691e57aa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c51fa982b1f0c2958d4822a1691e57aa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c51fa982b1f0c2958d4822a1691e57aa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_frozenset_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_args );
        tmp_tuple_element_1 = par_args;
        tmp_assign_source_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_1, 0, tmp_tuple_element_1 );
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( par_kwargs );
        tmp_called_instance_1 = par_kwargs;
        frame_c51fa982b1f0c2958d4822a1691e57aa->m_frame.f_lineno = 296;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_1 );

            exception_lineno = 296;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        frame_c51fa982b1f0c2958d4822a1691e57aa->m_frame.f_lineno = 296;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_frozenset_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_frozenset_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_1 );

            exception_lineno = 296;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = PyFrozenSet_New( tmp_frozenset_arg_1 );
        Py_DECREF( tmp_frozenset_arg_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_1 );

            exception_lineno = 296;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_assign_source_1, 1, tmp_tuple_element_1 );
        assert( var_key == NULL );
        var_key = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "cache" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 298;
            type_description_1 = "oooocc";
            goto try_except_handler_2;
        }

        tmp_subscribed_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( var_key );
        tmp_subscript_name_1 = var_key;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 298;
            type_description_1 = "oooocc";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_2_memoize$$$function_1_wrapper );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_c51fa982b1f0c2958d4822a1691e57aa, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_c51fa982b1f0c2958d4822a1691e57aa, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "oooocc";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_dircall_arg2_1;
            PyObject *tmp_dircall_arg3_1;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fun" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 300;
                type_description_1 = "oooocc";
                goto try_except_handler_4;
            }

            tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[1] );
            CHECK_OBJECT( par_args );
            tmp_dircall_arg2_1 = par_args;
            CHECK_OBJECT( par_kwargs );
            tmp_dircall_arg3_1 = par_kwargs;
            Py_INCREF( tmp_dircall_arg1_1 );
            Py_INCREF( tmp_dircall_arg2_1 );
            Py_INCREF( tmp_dircall_arg3_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
                tmp_assign_source_2 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
            }
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 300;
                type_description_1 = "oooocc";
                goto try_except_handler_4;
            }
            assert( tmp_assign_unpack_1__assign_source == NULL );
            tmp_assign_unpack_1__assign_source = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assign_source_3 = tmp_assign_unpack_1__assign_source;
            assert( var_ret == NULL );
            Py_INCREF( tmp_assign_source_3 );
            var_ret = tmp_assign_source_3;
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_ass_subvalue_1 = tmp_assign_unpack_1__assign_source;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "cache" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 300;
                type_description_1 = "oooocc";
                goto try_except_handler_4;
            }

            tmp_ass_subscribed_1 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( var_key );
            tmp_ass_subscript_1 = var_key;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 300;
                type_description_1 = "oooocc";
                goto try_except_handler_4;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_1:;
        CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
        Py_DECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        CHECK_OBJECT( var_ret );
        tmp_return_value = var_ret;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_3;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 297;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_c51fa982b1f0c2958d4822a1691e57aa->m_frame) frame_c51fa982b1f0c2958d4822a1691e57aa->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooocc";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_2_memoize$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c51fa982b1f0c2958d4822a1691e57aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c51fa982b1f0c2958d4822a1691e57aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c51fa982b1f0c2958d4822a1691e57aa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c51fa982b1f0c2958d4822a1691e57aa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c51fa982b1f0c2958d4822a1691e57aa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c51fa982b1f0c2958d4822a1691e57aa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c51fa982b1f0c2958d4822a1691e57aa,
        type_description_1,
        par_args,
        par_kwargs,
        var_key,
        var_ret,
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_c51fa982b1f0c2958d4822a1691e57aa == cache_frame_c51fa982b1f0c2958d4822a1691e57aa )
    {
        Py_DECREF( frame_c51fa982b1f0c2958d4822a1691e57aa );
    }
    cache_frame_c51fa982b1f0c2958d4822a1691e57aa = NULL;

    assertFrameObject( frame_c51fa982b1f0c2958d4822a1691e57aa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_2_memoize$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_key );
    Py_DECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_2_memoize$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_2_memoize$$$function_2_cache_clear( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_69a9ead8ba6e6ca78460c619c25a540c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_69a9ead8ba6e6ca78460c619c25a540c = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_69a9ead8ba6e6ca78460c619c25a540c, codeobj_69a9ead8ba6e6ca78460c619c25a540c, module_psutil$_common, sizeof(void *) );
    frame_69a9ead8ba6e6ca78460c619c25a540c = cache_frame_69a9ead8ba6e6ca78460c619c25a540c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_69a9ead8ba6e6ca78460c619c25a540c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_69a9ead8ba6e6ca78460c619c25a540c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "cache" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 305;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
        frame_69a9ead8ba6e6ca78460c619c25a540c->m_frame.f_lineno = 305;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_clear );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 305;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_69a9ead8ba6e6ca78460c619c25a540c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_69a9ead8ba6e6ca78460c619c25a540c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_69a9ead8ba6e6ca78460c619c25a540c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_69a9ead8ba6e6ca78460c619c25a540c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_69a9ead8ba6e6ca78460c619c25a540c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_69a9ead8ba6e6ca78460c619c25a540c,
        type_description_1,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_69a9ead8ba6e6ca78460c619c25a540c == cache_frame_69a9ead8ba6e6ca78460c619c25a540c )
    {
        Py_DECREF( frame_69a9ead8ba6e6ca78460c619c25a540c );
    }
    cache_frame_69a9ead8ba6e6ca78460c619c25a540c = NULL;

    assertFrameObject( frame_69a9ead8ba6e6ca78460c619c25a540c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_2_memoize$$$function_2_cache_clear );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_3_memoize_when_activated( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_fun = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_wrapper = NULL;
    PyObject *var_cache_activate = NULL;
    PyObject *var_cache_deactivate = NULL;
    struct Nuitka_FrameObject *frame_a0e916af95934d7eab7741981deb2f51;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_a0e916af95934d7eab7741981deb2f51 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a0e916af95934d7eab7741981deb2f51, codeobj_a0e916af95934d7eab7741981deb2f51, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a0e916af95934d7eab7741981deb2f51 = cache_frame_a0e916af95934d7eab7741981deb2f51;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a0e916af95934d7eab7741981deb2f51 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a0e916af95934d7eab7741981deb2f51 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 338;
            type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( PyCell_GET( par_fun ) );
        tmp_args_element_name_1 = PyCell_GET( par_fun );
        frame_a0e916af95934d7eab7741981deb2f51->m_frame.f_lineno = 338;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_wraps, call_args );
        }

        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] = par_fun;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] );


        frame_a0e916af95934d7eab7741981deb2f51->m_frame.f_lineno = 338;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_2_cache_activate(  );



        assert( var_cache_activate == NULL );
        var_cache_activate = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate(  );



        assert( var_cache_deactivate == NULL );
        var_cache_deactivate = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_cache_activate );
        tmp_assattr_name_1 = var_cache_activate;
        CHECK_OBJECT( var_wrapper );
        tmp_assattr_target_1 = var_wrapper;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cache_activate, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;
            type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( var_cache_deactivate );
        tmp_assattr_name_2 = var_cache_deactivate;
        CHECK_OBJECT( var_wrapper );
        tmp_assattr_target_2 = var_wrapper;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_cache_deactivate, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;
            type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a0e916af95934d7eab7741981deb2f51 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a0e916af95934d7eab7741981deb2f51 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a0e916af95934d7eab7741981deb2f51, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a0e916af95934d7eab7741981deb2f51->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a0e916af95934d7eab7741981deb2f51, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a0e916af95934d7eab7741981deb2f51,
        type_description_1,
        par_fun,
        var_wrapper,
        var_cache_activate,
        var_cache_deactivate
    );


    // Release cached frame.
    if ( frame_a0e916af95934d7eab7741981deb2f51 == cache_frame_a0e916af95934d7eab7741981deb2f51 )
    {
        Py_DECREF( frame_a0e916af95934d7eab7741981deb2f51 );
    }
    cache_frame_a0e916af95934d7eab7741981deb2f51 = NULL;

    assertFrameObject( frame_a0e916af95934d7eab7741981deb2f51 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fun );
    Py_DECREF( par_fun );
    par_fun = NULL;

    CHECK_OBJECT( (PyObject *)var_wrapper );
    Py_DECREF( var_wrapper );
    var_wrapper = NULL;

    CHECK_OBJECT( (PyObject *)var_cache_activate );
    Py_DECREF( var_cache_activate );
    var_cache_activate = NULL;

    CHECK_OBJECT( (PyObject *)var_cache_deactivate );
    Py_DECREF( var_cache_deactivate );
    var_cache_deactivate = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fun );
    Py_DECREF( par_fun );
    par_fun = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    Py_XDECREF( var_cache_activate );
    var_cache_activate = NULL;

    Py_XDECREF( var_cache_deactivate );
    var_cache_deactivate = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_ret = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    struct Nuitka_FrameObject *frame_53859f37c137aa1d0a547e9b72865b8c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_53859f37c137aa1d0a547e9b72865b8c = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_53859f37c137aa1d0a547e9b72865b8c, codeobj_53859f37c137aa1d0a547e9b72865b8c, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_53859f37c137aa1d0a547e9b72865b8c = cache_frame_53859f37c137aa1d0a547e9b72865b8c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_53859f37c137aa1d0a547e9b72865b8c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_53859f37c137aa1d0a547e9b72865b8c ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__cache );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 342;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_subscribed_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fun" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 342;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }

        tmp_subscript_name_1 = PyCell_GET( self->m_closure[0] );
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 342;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }
        assert( var_ret == NULL );
        var_ret = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_53859f37c137aa1d0a547e9b72865b8c, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_53859f37c137aa1d0a547e9b72865b8c, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 343;
            type_description_1 = "ooc";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fun" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 345;
                type_description_1 = "ooc";
                goto try_except_handler_3;
            }

            tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( par_self );
            tmp_args_element_name_1 = par_self;
            frame_53859f37c137aa1d0a547e9b72865b8c->m_frame.f_lineno = 345;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_1 = "ooc";
                goto try_except_handler_3;
            }
            goto try_return_handler_3;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_KeyError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 346;
                type_description_1 = "ooc";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            // Tried code:
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_element_name_2;
                if ( PyCell_GET( self->m_closure[0] ) == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fun" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 349;
                    type_description_1 = "ooc";
                    goto try_except_handler_4;
                }

                tmp_called_name_2 = PyCell_GET( self->m_closure[0] );
                CHECK_OBJECT( par_self );
                tmp_args_element_name_2 = par_self;
                frame_53859f37c137aa1d0a547e9b72865b8c->m_frame.f_lineno = 349;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooc";
                    goto try_except_handler_4;
                }
                assert( tmp_assign_unpack_1__assign_source == NULL );
                tmp_assign_unpack_1__assign_source = tmp_assign_source_2;
            }
            {
                PyObject *tmp_assign_source_3;
                CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
                tmp_assign_source_3 = tmp_assign_unpack_1__assign_source;
                assert( var_ret == NULL );
                Py_INCREF( tmp_assign_source_3 );
                var_ret = tmp_assign_source_3;
            }
            {
                PyObject *tmp_ass_subvalue_1;
                PyObject *tmp_ass_subscribed_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_ass_subscript_1;
                CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
                tmp_ass_subvalue_1 = tmp_assign_unpack_1__assign_source;
                CHECK_OBJECT( par_self );
                tmp_source_name_2 = par_self;
                tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__cache );
                if ( tmp_ass_subscribed_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooc";
                    goto try_except_handler_4;
                }
                if ( PyCell_GET( self->m_closure[0] ) == NULL )
                {
                    Py_DECREF( tmp_ass_subscribed_1 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fun" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 349;
                    type_description_1 = "ooc";
                    goto try_except_handler_4;
                }

                tmp_ass_subscript_1 = PyCell_GET( self->m_closure[0] );
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                Py_DECREF( tmp_ass_subscribed_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooc";
                    goto try_except_handler_4;
                }
            }
            goto try_end_2;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_assign_unpack_1__assign_source );
            tmp_assign_unpack_1__assign_source = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_3;
            // End of try:
            try_end_2:;
            CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
            Py_DECREF( tmp_assign_unpack_1__assign_source );
            tmp_assign_unpack_1__assign_source = NULL;

            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 340;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_53859f37c137aa1d0a547e9b72865b8c->m_frame) frame_53859f37c137aa1d0a547e9b72865b8c->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooc";
            goto try_except_handler_3;
            branch_end_2:;
        }
        branch_end_1:;
    }
    goto try_end_3;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper );
    return NULL;
    // End of try:
    try_end_1:;
    if ( var_ret == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ret" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 350;
        type_description_1 = "ooc";
        goto frame_exception_exit_1;
    }

    tmp_return_value = var_ret;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53859f37c137aa1d0a547e9b72865b8c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_53859f37c137aa1d0a547e9b72865b8c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53859f37c137aa1d0a547e9b72865b8c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_53859f37c137aa1d0a547e9b72865b8c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_53859f37c137aa1d0a547e9b72865b8c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_53859f37c137aa1d0a547e9b72865b8c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_53859f37c137aa1d0a547e9b72865b8c,
        type_description_1,
        par_self,
        var_ret,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_53859f37c137aa1d0a547e9b72865b8c == cache_frame_53859f37c137aa1d0a547e9b72865b8c )
    {
        Py_DECREF( frame_53859f37c137aa1d0a547e9b72865b8c );
    }
    cache_frame_53859f37c137aa1d0a547e9b72865b8c = NULL;

    assertFrameObject( frame_53859f37c137aa1d0a547e9b72865b8c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_3_memoize_when_activated$$$function_2_cache_activate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_proc = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_589f1beda036b7ac243111b1399dc433;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_589f1beda036b7ac243111b1399dc433 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_589f1beda036b7ac243111b1399dc433, codeobj_589f1beda036b7ac243111b1399dc433, module_psutil$_common, sizeof(void *) );
    frame_589f1beda036b7ac243111b1399dc433 = cache_frame_589f1beda036b7ac243111b1399dc433;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_589f1beda036b7ac243111b1399dc433 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_589f1beda036b7ac243111b1399dc433 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = PyDict_New();
        CHECK_OBJECT( par_proc );
        tmp_assattr_target_1 = par_proc;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__cache, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 355;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_589f1beda036b7ac243111b1399dc433 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_589f1beda036b7ac243111b1399dc433 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_589f1beda036b7ac243111b1399dc433, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_589f1beda036b7ac243111b1399dc433->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_589f1beda036b7ac243111b1399dc433, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_589f1beda036b7ac243111b1399dc433,
        type_description_1,
        par_proc
    );


    // Release cached frame.
    if ( frame_589f1beda036b7ac243111b1399dc433 == cache_frame_589f1beda036b7ac243111b1399dc433 )
    {
        Py_DECREF( frame_589f1beda036b7ac243111b1399dc433 );
    }
    cache_frame_589f1beda036b7ac243111b1399dc433 = NULL;

    assertFrameObject( frame_589f1beda036b7ac243111b1399dc433 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_2_cache_activate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_proc );
    Py_DECREF( par_proc );
    par_proc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_proc );
    Py_DECREF( par_proc );
    par_proc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_2_cache_activate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_proc = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5a4482fce79cd9cb968a4339ebbc16fb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_5a4482fce79cd9cb968a4339ebbc16fb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5a4482fce79cd9cb968a4339ebbc16fb, codeobj_5a4482fce79cd9cb968a4339ebbc16fb, module_psutil$_common, sizeof(void *) );
    frame_5a4482fce79cd9cb968a4339ebbc16fb = cache_frame_5a4482fce79cd9cb968a4339ebbc16fb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5a4482fce79cd9cb968a4339ebbc16fb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5a4482fce79cd9cb968a4339ebbc16fb ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_attrdel_target_1;
        CHECK_OBJECT( par_proc );
        tmp_attrdel_target_1 = par_proc;
        tmp_res = PyObject_DelAttr( tmp_attrdel_target_1, const_str_plain__cache );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_5a4482fce79cd9cb968a4339ebbc16fb, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_5a4482fce79cd9cb968a4339ebbc16fb, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 359;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_5a4482fce79cd9cb968a4339ebbc16fb->m_frame) frame_5a4482fce79cd9cb968a4339ebbc16fb->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "o";
        goto try_except_handler_3;
        branch_no_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate );
    return NULL;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a4482fce79cd9cb968a4339ebbc16fb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a4482fce79cd9cb968a4339ebbc16fb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5a4482fce79cd9cb968a4339ebbc16fb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5a4482fce79cd9cb968a4339ebbc16fb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5a4482fce79cd9cb968a4339ebbc16fb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5a4482fce79cd9cb968a4339ebbc16fb,
        type_description_1,
        par_proc
    );


    // Release cached frame.
    if ( frame_5a4482fce79cd9cb968a4339ebbc16fb == cache_frame_5a4482fce79cd9cb968a4339ebbc16fb )
    {
        Py_DECREF( frame_5a4482fce79cd9cb968a4339ebbc16fb );
    }
    cache_frame_5a4482fce79cd9cb968a4339ebbc16fb = NULL;

    assertFrameObject( frame_5a4482fce79cd9cb968a4339ebbc16fb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_proc );
    Py_DECREF( par_proc );
    par_proc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_proc );
    Py_DECREF( par_proc );
    par_proc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_4_isfile_strict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *var_st = NULL;
    PyObject *var_err = NULL;
    struct Nuitka_FrameObject *frame_3bd930188379a197c6f391320fec4502;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_3bd930188379a197c6f391320fec4502 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3bd930188379a197c6f391320fec4502, codeobj_3bd930188379a197c6f391320fec4502, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3bd930188379a197c6f391320fec4502 = cache_frame_3bd930188379a197c6f391320fec4502;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3bd930188379a197c6f391320fec4502 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3bd930188379a197c6f391320fec4502 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 375;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_3bd930188379a197c6f391320fec4502->m_frame.f_lineno = 375;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_stat, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 375;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_3bd930188379a197c6f391320fec4502, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_3bd930188379a197c6f391320fec4502, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_OSError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 376;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = EXC_VALUE(PyThreadState_GET());
            assert( var_err == NULL );
            Py_INCREF( tmp_assign_source_2 );
            var_err = tmp_assign_source_2;
        }
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( var_err );
            tmp_source_name_1 = var_err;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_errno );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 377;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_errno );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_errno );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_compexpr_left_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "errno" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 377;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }

            tmp_source_name_2 = tmp_mvar_value_2;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_EPERM );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_2 );

                exception_lineno = 377;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            tmp_compexpr_right_2 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_compexpr_right_2, 0, tmp_tuple_element_1 );
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_errno );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_errno );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_compexpr_left_2 );
                Py_DECREF( tmp_compexpr_right_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "errno" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 377;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }

            tmp_source_name_3 = tmp_mvar_value_3;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_EACCES );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_2 );
                Py_DECREF( tmp_compexpr_right_2 );

                exception_lineno = 377;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            PyTuple_SET_ITEM( tmp_compexpr_right_2, 1, tmp_tuple_element_1 );
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 377;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 378;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_3bd930188379a197c6f391320fec4502->m_frame) frame_3bd930188379a197c6f391320fec4502->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooo";
            goto try_except_handler_4;
            branch_no_2:;
        }
        tmp_return_value = Py_False;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_4_isfile_strict );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_XDECREF( var_err );
        var_err = NULL;

        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_err );
        var_err = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 374;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_3bd930188379a197c6f391320fec4502->m_frame) frame_3bd930188379a197c6f391320fec4502->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_4_isfile_strict );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 381;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_4;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_S_ISREG );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 381;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_5 = var_st;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_st_mode );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 381;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_3bd930188379a197c6f391320fec4502->m_frame.f_lineno = 381;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 381;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3bd930188379a197c6f391320fec4502 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3bd930188379a197c6f391320fec4502 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3bd930188379a197c6f391320fec4502 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3bd930188379a197c6f391320fec4502, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3bd930188379a197c6f391320fec4502->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3bd930188379a197c6f391320fec4502, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3bd930188379a197c6f391320fec4502,
        type_description_1,
        par_path,
        var_st,
        var_err
    );


    // Release cached frame.
    if ( frame_3bd930188379a197c6f391320fec4502 == cache_frame_3bd930188379a197c6f391320fec4502 )
    {
        Py_DECREF( frame_3bd930188379a197c6f391320fec4502 );
    }
    cache_frame_3bd930188379a197c6f391320fec4502 = NULL;

    assertFrameObject( frame_3bd930188379a197c6f391320fec4502 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_4_isfile_strict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_4_isfile_strict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_5_path_exists_strict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *var_err = NULL;
    struct Nuitka_FrameObject *frame_ead688a2167ee1cf40fa04f3f17e9f12;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_ead688a2167ee1cf40fa04f3f17e9f12 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ead688a2167ee1cf40fa04f3f17e9f12, codeobj_ead688a2167ee1cf40fa04f3f17e9f12, module_psutil$_common, sizeof(void *)+sizeof(void *) );
    frame_ead688a2167ee1cf40fa04f3f17e9f12 = cache_frame_ead688a2167ee1cf40fa04f3f17e9f12;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ead688a2167ee1cf40fa04f3f17e9f12 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ead688a2167ee1cf40fa04f3f17e9f12 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 390;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_ead688a2167ee1cf40fa04f3f17e9f12->m_frame.f_lineno = 390;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_stat, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_ead688a2167ee1cf40fa04f3f17e9f12, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_ead688a2167ee1cf40fa04f3f17e9f12, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_OSError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = EXC_VALUE(PyThreadState_GET());
            assert( var_err == NULL );
            Py_INCREF( tmp_assign_source_1 );
            var_err = tmp_assign_source_1;
        }
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( var_err );
            tmp_source_name_1 = var_err;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_errno );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 392;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_errno );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_errno );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_compexpr_left_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "errno" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 392;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }

            tmp_source_name_2 = tmp_mvar_value_2;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_EPERM );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_2 );

                exception_lineno = 392;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            tmp_compexpr_right_2 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_compexpr_right_2, 0, tmp_tuple_element_1 );
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_errno );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_errno );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_compexpr_left_2 );
                Py_DECREF( tmp_compexpr_right_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "errno" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 392;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }

            tmp_source_name_3 = tmp_mvar_value_3;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_EACCES );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_2 );
                Py_DECREF( tmp_compexpr_right_2 );

                exception_lineno = 392;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            PyTuple_SET_ITEM( tmp_compexpr_right_2, 1, tmp_tuple_element_1 );
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 392;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 393;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ead688a2167ee1cf40fa04f3f17e9f12->m_frame) frame_ead688a2167ee1cf40fa04f3f17e9f12->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oo";
            goto try_except_handler_4;
            branch_no_2:;
        }
        tmp_return_value = Py_False;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_5_path_exists_strict );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_XDECREF( var_err );
        var_err = NULL;

        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_err );
        var_err = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 389;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_ead688a2167ee1cf40fa04f3f17e9f12->m_frame) frame_ead688a2167ee1cf40fa04f3f17e9f12->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_5_path_exists_strict );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ead688a2167ee1cf40fa04f3f17e9f12 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ead688a2167ee1cf40fa04f3f17e9f12 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ead688a2167ee1cf40fa04f3f17e9f12 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ead688a2167ee1cf40fa04f3f17e9f12, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ead688a2167ee1cf40fa04f3f17e9f12->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ead688a2167ee1cf40fa04f3f17e9f12, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ead688a2167ee1cf40fa04f3f17e9f12,
        type_description_1,
        par_path,
        var_err
    );


    // Release cached frame.
    if ( frame_ead688a2167ee1cf40fa04f3f17e9f12 == cache_frame_ead688a2167ee1cf40fa04f3f17e9f12 )
    {
        Py_DECREF( frame_ead688a2167ee1cf40fa04f3f17e9f12 );
    }
    cache_frame_ead688a2167ee1cf40fa04f3f17e9f12 = NULL;

    assertFrameObject( frame_ead688a2167ee1cf40fa04f3f17e9f12 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_5_path_exists_strict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_5_path_exists_strict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_6_supports_ipv6( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_sock = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_02c36612f9a1e0db15c02518dfab19ce;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_02c36612f9a1e0db15c02518dfab19ce = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_02c36612f9a1e0db15c02518dfab19ce, codeobj_02c36612f9a1e0db15c02518dfab19ce, module_psutil$_common, sizeof(void *) );
    frame_02c36612f9a1e0db15c02518dfab19ce = cache_frame_02c36612f9a1e0db15c02518dfab19ce;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_02c36612f9a1e0db15c02518dfab19ce );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_02c36612f9a1e0db15c02518dfab19ce ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socket );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 402;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_has_ipv6 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 402;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 402;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 402;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = tmp_mvar_value_2;
        tmp_compexpr_right_1 = Py_None;
        tmp_or_right_value_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socket );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 405;
            type_description_1 = "o";
            goto try_except_handler_2;
        }

        tmp_source_name_2 = tmp_mvar_value_3;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_socket );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 405;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 405;
            type_description_1 = "o";
            goto try_except_handler_2;
        }

        tmp_args_element_name_1 = tmp_mvar_value_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socket );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 405;
            type_description_1 = "o";
            goto try_except_handler_2;
        }

        tmp_source_name_3 = tmp_mvar_value_5;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_SOCK_STREAM );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 405;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = 405;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 405;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( var_sock == NULL );
        var_sock = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_contextlib );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_contextlib );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "contextlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 406;
            type_description_1 = "o";
            goto try_except_handler_3;
        }

        tmp_called_instance_1 = tmp_mvar_value_6;
        CHECK_OBJECT( var_sock );
        tmp_args_element_name_3 = var_sock;
        frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = 406;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_closing, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 406;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_4 = tmp_with_1__source;
        tmp_called_name_2 = LOOKUP_SPECIAL( tmp_source_name_4, const_str_plain___enter__ );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 406;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = 406;
        tmp_assign_source_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        Py_DECREF( tmp_called_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 406;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_5 = tmp_with_1__source;
        tmp_assign_source_4 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___exit__ );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 406;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_assign_source_5;
        tmp_assign_source_5 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_5;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_sock );
        tmp_called_instance_2 = var_sock;
        frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = 407;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_bind, &PyTuple_GET_ITEM( const_tuple_05d8568253270babe3d513d8bda8531d_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 407;
            type_description_1 = "o";
            goto try_except_handler_5;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_02c36612f9a1e0db15c02518dfab19ce, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_02c36612f9a1e0db15c02518dfab19ce, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 406;
            type_description_1 = "o";
            goto try_except_handler_6;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_assign_source_6;
            tmp_assign_source_6 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_6;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_3 = tmp_with_1__exit;
            tmp_args_element_name_4 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_5 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_6 = EXC_TRACEBACK(PyThreadState_GET());
            frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = 407;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
            }

            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 407;
                type_description_1 = "o";
                goto try_except_handler_6;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 407;
                type_description_1 = "o";
                goto try_except_handler_6;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 407;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_02c36612f9a1e0db15c02518dfab19ce->m_frame) frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "o";
            goto try_except_handler_6;
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 406;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_02c36612f9a1e0db15c02518dfab19ce->m_frame) frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "o";
        goto try_except_handler_6;
        branch_end_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_4;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_6_supports_ipv6 );
    return NULL;
    // End of try:
    try_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = 407;
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_3 );
                Py_XDECREF( exception_keeper_value_3 );
                Py_XDECREF( exception_keeper_tb_3 );

                exception_lineno = 407;
                type_description_1 = "o";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_4:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_5;
        nuitka_bool tmp_compexpr_left_4;
        nuitka_bool tmp_compexpr_right_4;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_4 = tmp_with_1__indicator;
        tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
        tmp_condition_result_5 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = 407;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 407;
                type_description_1 = "o";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_5:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_5 == NULL )
    {
        exception_keeper_tb_5 = MAKE_TRACEBACK( frame_02c36612f9a1e0db15c02518dfab19ce, exception_keeper_lineno_5 );
    }
    else if ( exception_keeper_lineno_5 != 0 )
    {
        exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_02c36612f9a1e0db15c02518dfab19ce, exception_keeper_lineno_5 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    PyException_SetTraceback( exception_keeper_value_5, (PyObject *)exception_keeper_tb_5 );
    PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_7;
        tmp_compexpr_left_5 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socket );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 409;
            type_description_1 = "o";
            goto try_except_handler_7;
        }

        tmp_source_name_6 = tmp_mvar_value_7;
        tmp_compexpr_right_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_error );
        if ( tmp_compexpr_right_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 409;
            type_description_1 = "o";
            goto try_except_handler_7;
        }
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        Py_DECREF( tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 409;
            type_description_1 = "o";
            goto try_except_handler_7;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        tmp_return_value = Py_False;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_7;
        goto branch_end_6;
        branch_no_6:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 404;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_02c36612f9a1e0db15c02518dfab19ce->m_frame) frame_02c36612f9a1e0db15c02518dfab19ce->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "o";
        goto try_except_handler_7;
        branch_end_6:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_6_supports_ipv6 );
    return NULL;
    // Return handler code:
    try_return_handler_7:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_5:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02c36612f9a1e0db15c02518dfab19ce );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_02c36612f9a1e0db15c02518dfab19ce );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02c36612f9a1e0db15c02518dfab19ce );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_02c36612f9a1e0db15c02518dfab19ce, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_02c36612f9a1e0db15c02518dfab19ce->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_02c36612f9a1e0db15c02518dfab19ce, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_02c36612f9a1e0db15c02518dfab19ce,
        type_description_1,
        var_sock
    );


    // Release cached frame.
    if ( frame_02c36612f9a1e0db15c02518dfab19ce == cache_frame_02c36612f9a1e0db15c02518dfab19ce )
    {
        Py_DECREF( frame_02c36612f9a1e0db15c02518dfab19ce );
    }
    cache_frame_02c36612f9a1e0db15c02518dfab19ce = NULL;

    assertFrameObject( frame_02c36612f9a1e0db15c02518dfab19ce );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_6_supports_ipv6 );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_sock );
    var_sock = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_sock );
    var_sock = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_6_supports_ipv6 );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_7_parse_environ_block( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_data = python_pars[ 0 ];
    PyObject *var_ret = NULL;
    PyObject *var_pos = NULL;
    PyObject *var_WINDOWS_ = NULL;
    PyObject *var_next_pos = NULL;
    PyObject *var_equal_pos = NULL;
    PyObject *var_key = NULL;
    PyObject *var_value = NULL;
    struct Nuitka_FrameObject *frame_57950a6fa80e5368b9a8128bcd9382da;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    static struct Nuitka_FrameObject *cache_frame_57950a6fa80e5368b9a8128bcd9382da = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyDict_New();
        assert( var_ret == NULL );
        var_ret = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = const_int_0;
        assert( var_pos == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_pos = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_57950a6fa80e5368b9a8128bcd9382da, codeobj_57950a6fa80e5368b9a8128bcd9382da, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_57950a6fa80e5368b9a8128bcd9382da = cache_frame_57950a6fa80e5368b9a8128bcd9382da;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_57950a6fa80e5368b9a8128bcd9382da );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_57950a6fa80e5368b9a8128bcd9382da ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_WINDOWS );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WINDOWS );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WINDOWS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 421;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_3 = tmp_mvar_value_1;
        assert( var_WINDOWS_ == NULL );
        Py_INCREF( tmp_assign_source_3 );
        var_WINDOWS_ = tmp_assign_source_3;
    }
    loop_start_1:;
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_data );
        tmp_called_instance_1 = par_data;
        tmp_args_element_name_1 = const_str_chr_0;
        CHECK_OBJECT( var_pos );
        tmp_args_element_name_2 = var_pos;
        frame_57950a6fa80e5368b9a8128bcd9382da->m_frame.f_lineno = 423;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_find, call_args );
        }

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 423;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_next_pos;
            var_next_pos = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_next_pos );
        tmp_compexpr_left_1 = var_next_pos;
        CHECK_OBJECT( var_pos );
        tmp_compexpr_right_1 = var_pos;
        tmp_res = RICH_COMPARE_BOOL_LTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 425;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( par_data );
        tmp_called_instance_2 = par_data;
        tmp_args_element_name_3 = const_str_chr_61;
        CHECK_OBJECT( var_pos );
        tmp_args_element_name_4 = var_pos;
        CHECK_OBJECT( var_next_pos );
        tmp_args_element_name_5 = var_next_pos;
        frame_57950a6fa80e5368b9a8128bcd9382da->m_frame.f_lineno = 428;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assign_source_5 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain_find, call_args );
        }

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 428;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_equal_pos;
            var_equal_pos = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_equal_pos );
        tmp_compexpr_left_2 = var_equal_pos;
        CHECK_OBJECT( var_pos );
        tmp_compexpr_right_2 = var_pos;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 429;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_start_name_1;
            PyObject *tmp_stop_name_1;
            PyObject *tmp_step_name_1;
            CHECK_OBJECT( par_data );
            tmp_subscribed_name_1 = par_data;
            CHECK_OBJECT( var_pos );
            tmp_start_name_1 = var_pos;
            CHECK_OBJECT( var_equal_pos );
            tmp_stop_name_1 = var_equal_pos;
            tmp_step_name_1 = Py_None;
            tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
            assert( !(tmp_subscript_name_1 == NULL) );
            tmp_assign_source_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 430;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_key;
                var_key = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_start_name_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_stop_name_2;
            PyObject *tmp_step_name_2;
            CHECK_OBJECT( par_data );
            tmp_subscribed_name_2 = par_data;
            CHECK_OBJECT( var_equal_pos );
            tmp_left_name_1 = var_equal_pos;
            tmp_right_name_1 = const_int_pos_1;
            tmp_start_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_start_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 431;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_next_pos );
            tmp_stop_name_2 = var_next_pos;
            tmp_step_name_2 = Py_None;
            tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
            Py_DECREF( tmp_start_name_2 );
            assert( !(tmp_subscript_name_2 == NULL) );
            tmp_assign_source_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 431;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_value;
                var_value = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_truth_name_1;
            CHECK_OBJECT( var_WINDOWS_ );
            tmp_truth_name_1 = CHECK_IF_TRUE( var_WINDOWS_ );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 433;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_called_instance_3;
                CHECK_OBJECT( var_key );
                tmp_called_instance_3 = var_key;
                frame_57950a6fa80e5368b9a8128bcd9382da->m_frame.f_lineno = 434;
                tmp_assign_source_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_upper );
                if ( tmp_assign_source_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 434;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_key;
                    assert( old != NULL );
                    var_key = tmp_assign_source_8;
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        CHECK_OBJECT( var_value );
        tmp_dictset_value = var_value;
        CHECK_OBJECT( var_ret );
        tmp_dictset_dict = var_ret;
        CHECK_OBJECT( var_key );
        tmp_dictset_key = var_key;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 435;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( var_next_pos );
        tmp_left_name_2 = var_next_pos;
        tmp_right_name_2 = const_int_pos_1;
        tmp_assign_source_9 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 436;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_pos;
            assert( old != NULL );
            var_pos = tmp_assign_source_9;
            Py_DECREF( old );
        }

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 422;
        type_description_1 = "oooooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_57950a6fa80e5368b9a8128bcd9382da );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_57950a6fa80e5368b9a8128bcd9382da );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_57950a6fa80e5368b9a8128bcd9382da, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_57950a6fa80e5368b9a8128bcd9382da->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_57950a6fa80e5368b9a8128bcd9382da, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_57950a6fa80e5368b9a8128bcd9382da,
        type_description_1,
        par_data,
        var_ret,
        var_pos,
        var_WINDOWS_,
        var_next_pos,
        var_equal_pos,
        var_key,
        var_value
    );


    // Release cached frame.
    if ( frame_57950a6fa80e5368b9a8128bcd9382da == cache_frame_57950a6fa80e5368b9a8128bcd9382da )
    {
        Py_DECREF( frame_57950a6fa80e5368b9a8128bcd9382da );
    }
    cache_frame_57950a6fa80e5368b9a8128bcd9382da = NULL;

    assertFrameObject( frame_57950a6fa80e5368b9a8128bcd9382da );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_ret );
    tmp_return_value = var_ret;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_7_parse_environ_block );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    CHECK_OBJECT( (PyObject *)var_ret );
    Py_DECREF( var_ret );
    var_ret = NULL;

    CHECK_OBJECT( (PyObject *)var_pos );
    Py_DECREF( var_pos );
    var_pos = NULL;

    CHECK_OBJECT( (PyObject *)var_WINDOWS_ );
    Py_DECREF( var_WINDOWS_ );
    var_WINDOWS_ = NULL;

    CHECK_OBJECT( (PyObject *)var_next_pos );
    Py_DECREF( var_next_pos );
    var_next_pos = NULL;

    Py_XDECREF( var_equal_pos );
    var_equal_pos = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    CHECK_OBJECT( (PyObject *)var_ret );
    Py_DECREF( var_ret );
    var_ret = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_WINDOWS_ );
    var_WINDOWS_ = NULL;

    Py_XDECREF( var_next_pos );
    var_next_pos = NULL;

    Py_XDECREF( var_equal_pos );
    var_equal_pos = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_7_parse_environ_block );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_8_sockfam_to_enum( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_num = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_53d9763bad7ab7dc936ba3bf85cfa331;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_53d9763bad7ab7dc936ba3bf85cfa331 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_53d9763bad7ab7dc936ba3bf85cfa331, codeobj_53d9763bad7ab7dc936ba3bf85cfa331, module_psutil$_common, sizeof(void *) );
    frame_53d9763bad7ab7dc936ba3bf85cfa331 = cache_frame_53d9763bad7ab7dc936ba3bf85cfa331;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_53d9763bad7ab7dc936ba3bf85cfa331 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_53d9763bad7ab7dc936ba3bf85cfa331 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_enum );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "enum" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 445;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = tmp_mvar_value_1;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_num );
        tmp_return_value = par_num;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        // Tried code:
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socket );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 449;
                type_description_1 = "o";
                goto try_except_handler_2;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            CHECK_OBJECT( par_num );
            tmp_args_element_name_1 = par_num;
            frame_53d9763bad7ab7dc936ba3bf85cfa331->m_frame.f_lineno = 449;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_AddressFamily, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 449;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            goto frame_return_exit_1;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_8_sockfam_to_enum );
        return NULL;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_53d9763bad7ab7dc936ba3bf85cfa331, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_53d9763bad7ab7dc936ba3bf85cfa331, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = const_tuple_type_ValueError_type_AttributeError_tuple;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 450;
                type_description_1 = "o";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            CHECK_OBJECT( par_num );
            tmp_return_value = par_num;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_3;
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 448;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_53d9763bad7ab7dc936ba3bf85cfa331->m_frame) frame_53d9763bad7ab7dc936ba3bf85cfa331->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "o";
            goto try_except_handler_3;
            branch_end_2:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_8_sockfam_to_enum );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53d9763bad7ab7dc936ba3bf85cfa331 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_53d9763bad7ab7dc936ba3bf85cfa331 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53d9763bad7ab7dc936ba3bf85cfa331 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_53d9763bad7ab7dc936ba3bf85cfa331, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_53d9763bad7ab7dc936ba3bf85cfa331->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_53d9763bad7ab7dc936ba3bf85cfa331, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_53d9763bad7ab7dc936ba3bf85cfa331,
        type_description_1,
        par_num
    );


    // Release cached frame.
    if ( frame_53d9763bad7ab7dc936ba3bf85cfa331 == cache_frame_53d9763bad7ab7dc936ba3bf85cfa331 )
    {
        Py_DECREF( frame_53d9763bad7ab7dc936ba3bf85cfa331 );
    }
    cache_frame_53d9763bad7ab7dc936ba3bf85cfa331 = NULL;

    assertFrameObject( frame_53d9763bad7ab7dc936ba3bf85cfa331 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_8_sockfam_to_enum );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_8_sockfam_to_enum );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_9_socktype_to_enum( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_num = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ebbee4ed4136870a4f406fd32eea55b2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_ebbee4ed4136870a4f406fd32eea55b2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ebbee4ed4136870a4f406fd32eea55b2, codeobj_ebbee4ed4136870a4f406fd32eea55b2, module_psutil$_common, sizeof(void *) );
    frame_ebbee4ed4136870a4f406fd32eea55b2 = cache_frame_ebbee4ed4136870a4f406fd32eea55b2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ebbee4ed4136870a4f406fd32eea55b2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ebbee4ed4136870a4f406fd32eea55b2 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_enum );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "enum" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 458;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = tmp_mvar_value_1;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_num );
        tmp_return_value = par_num;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        // Tried code:
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socket );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_socket );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "socket" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 462;
                type_description_1 = "o";
                goto try_except_handler_2;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            CHECK_OBJECT( par_num );
            tmp_args_element_name_1 = par_num;
            frame_ebbee4ed4136870a4f406fd32eea55b2->m_frame.f_lineno = 462;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_AddressType, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 462;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            goto frame_return_exit_1;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_9_socktype_to_enum );
        return NULL;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_ebbee4ed4136870a4f406fd32eea55b2, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_ebbee4ed4136870a4f406fd32eea55b2, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = const_tuple_type_ValueError_type_AttributeError_tuple;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 463;
                type_description_1 = "o";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            CHECK_OBJECT( par_num );
            tmp_return_value = par_num;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_3;
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 461;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ebbee4ed4136870a4f406fd32eea55b2->m_frame) frame_ebbee4ed4136870a4f406fd32eea55b2->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "o";
            goto try_except_handler_3;
            branch_end_2:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_9_socktype_to_enum );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ebbee4ed4136870a4f406fd32eea55b2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ebbee4ed4136870a4f406fd32eea55b2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ebbee4ed4136870a4f406fd32eea55b2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ebbee4ed4136870a4f406fd32eea55b2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ebbee4ed4136870a4f406fd32eea55b2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ebbee4ed4136870a4f406fd32eea55b2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ebbee4ed4136870a4f406fd32eea55b2,
        type_description_1,
        par_num
    );


    // Release cached frame.
    if ( frame_ebbee4ed4136870a4f406fd32eea55b2 == cache_frame_ebbee4ed4136870a4f406fd32eea55b2 )
    {
        Py_DECREF( frame_ebbee4ed4136870a4f406fd32eea55b2 );
    }
    cache_frame_ebbee4ed4136870a4f406fd32eea55b2 = NULL;

    assertFrameObject( frame_ebbee4ed4136870a4f406fd32eea55b2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_9_socktype_to_enum );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_9_socktype_to_enum );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_10_deprecated_method( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_replacement = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_outer = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method$$$function_1_outer(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_replacement;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_outer == NULL );
        var_outer = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_outer );
    tmp_return_value = var_outer;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_10_deprecated_method );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_replacement );
    Py_DECREF( par_replacement );
    par_replacement = NULL;

    CHECK_OBJECT( (PyObject *)var_outer );
    Py_DECREF( var_outer );
    var_outer = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_10_deprecated_method );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_10_deprecated_method$$$function_1_outer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fun = python_pars[ 0 ];
    struct Nuitka_CellObject *var_msg = PyCell_EMPTY();
    PyObject *var_inner = NULL;
    struct Nuitka_FrameObject *frame_8b587c31f069329851643cb68c828c33;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_8b587c31f069329851643cb68c828c33 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8b587c31f069329851643cb68c828c33, codeobj_8b587c31f069329851643cb68c828c33, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8b587c31f069329851643cb68c828c33 = cache_frame_8b587c31f069329851643cb68c828c33;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8b587c31f069329851643cb68c828c33 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8b587c31f069329851643cb68c828c33 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        tmp_left_name_1 = const_str_digest_4ec72dfd8b162da383cac847fd2af2fc;
        CHECK_OBJECT( par_fun );
        tmp_source_name_1 = par_fun;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 473;
            type_description_1 = "ococ";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "replacement" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 473;
            type_description_1 = "ococ";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = PyCell_GET( self->m_closure[0] );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_assign_source_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 472;
            type_description_1 = "ococ";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_msg ) == NULL );
        PyCell_SET( var_msg, tmp_assign_source_1 );

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_fun );
        tmp_source_name_2 = par_fun;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___doc__ );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 474;
            type_description_1 = "ococ";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( PyCell_GET( var_msg ) );
            tmp_assattr_name_1 = PyCell_GET( var_msg );
            CHECK_OBJECT( par_fun );
            tmp_assattr_target_1 = par_fun;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___doc__, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 475;
                type_description_1 = "ococ";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 477;
            type_description_1 = "ococ";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_fun );
        tmp_args_element_name_1 = par_fun;
        frame_8b587c31f069329851643cb68c828c33->m_frame.f_lineno = 477;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_wraps, call_args );
        }

        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 477;
            type_description_1 = "ococ";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method$$$function_1_outer$$$function_1_inner(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] = var_msg;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] = self->m_closure[0];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_2)->m_closure[1] );


        frame_8b587c31f069329851643cb68c828c33->m_frame.f_lineno = 477;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 477;
            type_description_1 = "ococ";
            goto frame_exception_exit_1;
        }
        assert( var_inner == NULL );
        var_inner = tmp_assign_source_2;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b587c31f069329851643cb68c828c33 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b587c31f069329851643cb68c828c33 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8b587c31f069329851643cb68c828c33, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8b587c31f069329851643cb68c828c33->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8b587c31f069329851643cb68c828c33, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8b587c31f069329851643cb68c828c33,
        type_description_1,
        par_fun,
        var_msg,
        var_inner,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_8b587c31f069329851643cb68c828c33 == cache_frame_8b587c31f069329851643cb68c828c33 )
    {
        Py_DECREF( frame_8b587c31f069329851643cb68c828c33 );
    }
    cache_frame_8b587c31f069329851643cb68c828c33 = NULL;

    assertFrameObject( frame_8b587c31f069329851643cb68c828c33 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_inner );
    tmp_return_value = var_inner;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_10_deprecated_method$$$function_1_outer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fun );
    Py_DECREF( par_fun );
    par_fun = NULL;

    CHECK_OBJECT( (PyObject *)var_msg );
    Py_DECREF( var_msg );
    var_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_inner );
    Py_DECREF( var_inner );
    var_inner = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fun );
    Py_DECREF( par_fun );
    par_fun = NULL;

    CHECK_OBJECT( (PyObject *)var_msg );
    Py_DECREF( var_msg );
    var_msg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_10_deprecated_method$$$function_1_outer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_10_deprecated_method$$$function_1_outer$$$function_1_inner( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_04839f818328ef15580890c4cd786f57;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_04839f818328ef15580890c4cd786f57 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_04839f818328ef15580890c4cd786f57, codeobj_04839f818328ef15580890c4cd786f57, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_04839f818328ef15580890c4cd786f57 = cache_frame_04839f818328ef15580890c4cd786f57;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_04839f818328ef15580890c4cd786f57 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_04839f818328ef15580890c4cd786f57 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_warnings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 479;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 479;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "msg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 479;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = PyCell_GET( self->m_closure[0] );
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_category;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DeprecationWarning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 479;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_1 = tmp_mvar_value_2;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_stacklevel;
        tmp_dict_value_2 = const_int_pos_2;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_04839f818328ef15580890c4cd786f57->m_frame.f_lineno = 479;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 479;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_getattr_target_1 = par_self;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "replacement" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 480;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_getattr_attr_1 = PyCell_GET( self->m_closure[1] );
        tmp_dircall_arg1_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 480;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 480;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_04839f818328ef15580890c4cd786f57 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_04839f818328ef15580890c4cd786f57 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_04839f818328ef15580890c4cd786f57 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_04839f818328ef15580890c4cd786f57, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_04839f818328ef15580890c4cd786f57->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_04839f818328ef15580890c4cd786f57, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_04839f818328ef15580890c4cd786f57,
        type_description_1,
        par_self,
        par_args,
        par_kwargs,
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_04839f818328ef15580890c4cd786f57 == cache_frame_04839f818328ef15580890c4cd786f57 )
    {
        Py_DECREF( frame_04839f818328ef15580890c4cd786f57 );
    }
    cache_frame_04839f818328ef15580890c4cd786f57 = NULL;

    assertFrameObject( frame_04839f818328ef15580890c4cd786f57 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_10_deprecated_method$$$function_1_outer$$$function_1_inner );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_10_deprecated_method$$$function_1_outer$$$function_1_inner );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_11___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_17cd2ecfb0288cbaa7706a9e05df80cc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_17cd2ecfb0288cbaa7706a9e05df80cc = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_17cd2ecfb0288cbaa7706a9e05df80cc, codeobj_17cd2ecfb0288cbaa7706a9e05df80cc, module_psutil$_common, sizeof(void *) );
    frame_17cd2ecfb0288cbaa7706a9e05df80cc = cache_frame_17cd2ecfb0288cbaa7706a9e05df80cc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_17cd2ecfb0288cbaa7706a9e05df80cc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_17cd2ecfb0288cbaa7706a9e05df80cc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_threading );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_threading );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "threading" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 491;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_17cd2ecfb0288cbaa7706a9e05df80cc->m_frame.f_lineno = 491;
        tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_Lock );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 491;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_lock, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 491;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_cache, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 492;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        tmp_assattr_name_3 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_reminders, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 493;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        tmp_assattr_name_4 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_reminder_keys, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 494;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_17cd2ecfb0288cbaa7706a9e05df80cc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_17cd2ecfb0288cbaa7706a9e05df80cc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_17cd2ecfb0288cbaa7706a9e05df80cc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_17cd2ecfb0288cbaa7706a9e05df80cc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_17cd2ecfb0288cbaa7706a9e05df80cc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_17cd2ecfb0288cbaa7706a9e05df80cc,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_17cd2ecfb0288cbaa7706a9e05df80cc == cache_frame_17cd2ecfb0288cbaa7706a9e05df80cc )
    {
        Py_DECREF( frame_17cd2ecfb0288cbaa7706a9e05df80cc );
    }
    cache_frame_17cd2ecfb0288cbaa7706a9e05df80cc = NULL;

    assertFrameObject( frame_17cd2ecfb0288cbaa7706a9e05df80cc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_11___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_11___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_12__add_dict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_input_dict = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_ef16a5a470b8dcb15406d714b9e72dfc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_ef16a5a470b8dcb15406d714b9e72dfc = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ef16a5a470b8dcb15406d714b9e72dfc, codeobj_ef16a5a470b8dcb15406d714b9e72dfc, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ef16a5a470b8dcb15406d714b9e72dfc = cache_frame_ef16a5a470b8dcb15406d714b9e72dfc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ef16a5a470b8dcb15406d714b9e72dfc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ef16a5a470b8dcb15406d714b9e72dfc ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_name );
        tmp_compexpr_left_1 = par_name;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_cache );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 497;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 497;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 497;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_name );
        tmp_compexpr_left_2 = par_name;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_reminders );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 498;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 498;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_2;
            tmp_raise_type_2 = PyExc_AssertionError;
            exception_type = tmp_raise_type_2;
            Py_INCREF( tmp_raise_type_2 );
            exception_lineno = 498;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_name );
        tmp_compexpr_left_3 = par_name;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_compexpr_right_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_reminder_keys );
        if ( tmp_compexpr_right_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 499;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
        Py_DECREF( tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 499;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_raise_type_3;
            tmp_raise_type_3 = PyExc_AssertionError;
            exception_type = tmp_raise_type_3;
            Py_INCREF( tmp_raise_type_3 );
            exception_lineno = 499;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( par_input_dict );
        tmp_ass_subvalue_1 = par_input_dict;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_cache );
        if ( tmp_ass_subscribed_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 500;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_ass_subscript_1 = par_name;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subscribed_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 500;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_ass_subvalue_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_ass_subscribed_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_ass_subscript_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_defaultdict );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_defaultdict );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "defaultdict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 501;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_ef16a5a470b8dcb15406d714b9e72dfc->m_frame.f_lineno = 501;
        tmp_ass_subvalue_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_type_int_tuple, 0 ) );

        if ( tmp_ass_subvalue_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 501;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_ass_subscribed_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_reminders );
        if ( tmp_ass_subscribed_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_ass_subvalue_2 );

            exception_lineno = 501;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_ass_subscript_2 = par_name;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
        Py_DECREF( tmp_ass_subscribed_2 );
        Py_DECREF( tmp_ass_subvalue_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 501;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_ass_subvalue_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_ass_subscribed_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_ass_subscript_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_defaultdict );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_defaultdict );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "defaultdict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 502;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        frame_ef16a5a470b8dcb15406d714b9e72dfc->m_frame.f_lineno = 502;
        tmp_ass_subvalue_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_type_set_tuple, 0 ) );

        if ( tmp_ass_subvalue_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 502;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_ass_subscribed_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_reminder_keys );
        if ( tmp_ass_subscribed_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_ass_subvalue_3 );

            exception_lineno = 502;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_ass_subscript_3 = par_name;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
        Py_DECREF( tmp_ass_subscribed_3 );
        Py_DECREF( tmp_ass_subvalue_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 502;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef16a5a470b8dcb15406d714b9e72dfc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef16a5a470b8dcb15406d714b9e72dfc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ef16a5a470b8dcb15406d714b9e72dfc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ef16a5a470b8dcb15406d714b9e72dfc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ef16a5a470b8dcb15406d714b9e72dfc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ef16a5a470b8dcb15406d714b9e72dfc,
        type_description_1,
        par_self,
        par_input_dict,
        par_name
    );


    // Release cached frame.
    if ( frame_ef16a5a470b8dcb15406d714b9e72dfc == cache_frame_ef16a5a470b8dcb15406d714b9e72dfc )
    {
        Py_DECREF( frame_ef16a5a470b8dcb15406d714b9e72dfc );
    }
    cache_frame_ef16a5a470b8dcb15406d714b9e72dfc = NULL;

    assertFrameObject( frame_ef16a5a470b8dcb15406d714b9e72dfc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_12__add_dict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_12__add_dict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_13__remove_dead_reminders( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_input_dict = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *var_old_dict = NULL;
    PyObject *var_gone_keys = NULL;
    PyObject *var_gone_key = NULL;
    PyObject *var_remkey = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    struct Nuitka_FrameObject *frame_19ab808ff3653aac5d36f73d1d924f02;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_19ab808ff3653aac5d36f73d1d924f02 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_19ab808ff3653aac5d36f73d1d924f02, codeobj_19ab808ff3653aac5d36f73d1d924f02, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_19ab808ff3653aac5d36f73d1d924f02 = cache_frame_19ab808ff3653aac5d36f73d1d924f02;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_19ab808ff3653aac5d36f73d1d924f02 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_19ab808ff3653aac5d36f73d1d924f02 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_cache );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 508;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_subscript_name_1 = par_name;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 508;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_old_dict == NULL );
        var_old_dict = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_set_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_set_arg_2;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_old_dict );
        tmp_called_instance_1 = var_old_dict;
        frame_19ab808ff3653aac5d36f73d1d924f02->m_frame.f_lineno = 509;
        tmp_set_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_keys );
        if ( tmp_set_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 509;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = PySet_New( tmp_set_arg_1 );
        Py_DECREF( tmp_set_arg_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 509;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_input_dict );
        tmp_called_instance_2 = par_input_dict;
        frame_19ab808ff3653aac5d36f73d1d924f02->m_frame.f_lineno = 509;
        tmp_set_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_keys );
        if ( tmp_set_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 509;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PySet_New( tmp_set_arg_2 );
        Py_DECREF( tmp_set_arg_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 509;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        assert( !(tmp_assign_source_2 == NULL) );
        assert( var_gone_keys == NULL );
        var_gone_keys = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_gone_keys );
        tmp_iter_arg_1 = var_gone_keys;
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 510;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 510;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_gone_key;
            var_gone_key = tmp_assign_source_5;
            Py_INCREF( var_gone_key );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_subscript_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_reminder_keys );
        if ( tmp_subscribed_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 511;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_name );
        tmp_subscript_name_2 = par_name;
        tmp_subscribed_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscribed_name_3 );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 511;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_gone_key );
        tmp_subscript_name_3 = var_gone_key;
        tmp_iter_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_3 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 511;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 511;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 511;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_8 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_remkey;
            var_remkey = tmp_assign_source_8;
            Py_INCREF( var_remkey );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_delsubscr_target_1;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_source_name_3;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_delsubscr_subscript_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_reminders );
        if ( tmp_subscribed_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 512;
            type_description_1 = "ooooooo";
            goto try_except_handler_3;
        }
        CHECK_OBJECT( par_name );
        tmp_subscript_name_4 = par_name;
        tmp_delsubscr_target_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
        Py_DECREF( tmp_subscribed_name_4 );
        if ( tmp_delsubscr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 512;
            type_description_1 = "ooooooo";
            goto try_except_handler_3;
        }
        CHECK_OBJECT( var_remkey );
        tmp_delsubscr_subscript_1 = var_remkey;
        tmp_result = DEL_SUBSCRIPT( tmp_delsubscr_target_1, tmp_delsubscr_subscript_1 );
        Py_DECREF( tmp_delsubscr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 512;
            type_description_1 = "ooooooo";
            goto try_except_handler_3;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 511;
        type_description_1 = "ooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_delsubscr_target_2;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_source_name_4;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_delsubscr_subscript_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_subscribed_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_reminder_keys );
        if ( tmp_subscribed_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 513;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_name );
        tmp_subscript_name_5 = par_name;
        tmp_delsubscr_target_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
        Py_DECREF( tmp_subscribed_name_5 );
        if ( tmp_delsubscr_target_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 513;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_gone_key );
        tmp_delsubscr_subscript_2 = var_gone_key;
        tmp_result = DEL_SUBSCRIPT( tmp_delsubscr_target_2, tmp_delsubscr_subscript_2 );
        Py_DECREF( tmp_delsubscr_target_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 513;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 510;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_19ab808ff3653aac5d36f73d1d924f02 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_19ab808ff3653aac5d36f73d1d924f02 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_19ab808ff3653aac5d36f73d1d924f02, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_19ab808ff3653aac5d36f73d1d924f02->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_19ab808ff3653aac5d36f73d1d924f02, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_19ab808ff3653aac5d36f73d1d924f02,
        type_description_1,
        par_self,
        par_input_dict,
        par_name,
        var_old_dict,
        var_gone_keys,
        var_gone_key,
        var_remkey
    );


    // Release cached frame.
    if ( frame_19ab808ff3653aac5d36f73d1d924f02 == cache_frame_19ab808ff3653aac5d36f73d1d924f02 )
    {
        Py_DECREF( frame_19ab808ff3653aac5d36f73d1d924f02 );
    }
    cache_frame_19ab808ff3653aac5d36f73d1d924f02 = NULL;

    assertFrameObject( frame_19ab808ff3653aac5d36f73d1d924f02 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_13__remove_dead_reminders );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)var_old_dict );
    Py_DECREF( var_old_dict );
    var_old_dict = NULL;

    CHECK_OBJECT( (PyObject *)var_gone_keys );
    Py_DECREF( var_gone_keys );
    var_gone_keys = NULL;

    Py_XDECREF( var_gone_key );
    var_gone_key = NULL;

    Py_XDECREF( var_remkey );
    var_remkey = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_old_dict );
    var_old_dict = NULL;

    Py_XDECREF( var_gone_keys );
    var_gone_keys = NULL;

    Py_XDECREF( var_gone_key );
    var_gone_key = NULL;

    Py_XDECREF( var_remkey );
    var_remkey = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_13__remove_dead_reminders );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_14_run( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_input_dict = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *var_old_dict = NULL;
    PyObject *var_new_dict = NULL;
    PyObject *var_key = NULL;
    PyObject *var_input_tuple = NULL;
    PyObject *var_old_tuple = NULL;
    PyObject *var_bits = NULL;
    PyObject *var_i = NULL;
    PyObject *var_input_value = NULL;
    PyObject *var_old_value = NULL;
    PyObject *var_remkey = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_inplace_assign_subscr_1__subscript = NULL;
    PyObject *tmp_inplace_assign_subscr_1__target = NULL;
    struct Nuitka_FrameObject *frame_8259023325814c68d4e851583a18d148;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_8259023325814c68d4e851583a18d148 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8259023325814c68d4e851583a18d148, codeobj_8259023325814c68d4e851583a18d148, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8259023325814c68d4e851583a18d148 = cache_frame_8259023325814c68d4e851583a18d148;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8259023325814c68d4e851583a18d148 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8259023325814c68d4e851583a18d148 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_name );
        tmp_compexpr_left_1 = par_name;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_cache );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 519;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 519;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            CHECK_OBJECT( par_input_dict );
            tmp_args_element_name_1 = par_input_dict;
            CHECK_OBJECT( par_name );
            tmp_args_element_name_2 = par_name;
            frame_8259023325814c68d4e851583a18d148->m_frame.f_lineno = 521;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain__add_dict, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 521;
                type_description_1 = "ooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        CHECK_OBJECT( par_input_dict );
        tmp_return_value = par_input_dict;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( par_input_dict );
        tmp_args_element_name_3 = par_input_dict;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_4 = par_name;
        frame_8259023325814c68d4e851583a18d148->m_frame.f_lineno = 524;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain__remove_dead_reminders, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 524;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cache );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 526;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_subscript_name_1 = par_name;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 526;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_old_dict == NULL );
        var_old_dict = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( var_new_dict == NULL );
        var_new_dict = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( par_input_dict );
        tmp_called_instance_3 = par_input_dict;
        frame_8259023325814c68d4e851583a18d148->m_frame.f_lineno = 528;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_keys );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 528;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 528;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooo";
                exception_lineno = 528;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_key;
            var_key = tmp_assign_source_5;
            Py_INCREF( var_key );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( par_input_dict );
        tmp_subscribed_name_2 = par_input_dict;
        CHECK_OBJECT( var_key );
        tmp_subscript_name_2 = var_key;
        tmp_assign_source_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 529;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_input_tuple;
            var_input_tuple = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        CHECK_OBJECT( var_old_dict );
        tmp_subscribed_name_3 = var_old_dict;
        CHECK_OBJECT( var_key );
        tmp_subscript_name_3 = var_key;
        tmp_assign_source_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 531;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = var_old_tuple;
            var_old_tuple = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_8259023325814c68d4e851583a18d148, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_8259023325814c68d4e851583a18d148, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 532;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( var_input_tuple );
        tmp_dictset_value = var_input_tuple;
        CHECK_OBJECT( var_new_dict );
        tmp_dictset_dict = var_new_dict;
        CHECK_OBJECT( var_key );
        tmp_dictset_key = var_key;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 535;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_4;
        }
        goto try_continue_handler_4;
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 530;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_8259023325814c68d4e851583a18d148->m_frame) frame_8259023325814c68d4e851583a18d148->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_4;
        branch_end_2:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_14_run );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // try continue handler code:
    try_continue_handler_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto loop_start_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = PyList_New( 0 );
        {
            PyObject *old = var_bits;
            var_bits = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( var_input_tuple );
        tmp_len_arg_1 = var_input_tuple;
        tmp_xrange_low_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_xrange_low_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 539;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_iter_arg_2 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        Py_DECREF( tmp_xrange_low_1 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 539;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 539;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooo";
                exception_lineno = 539;
                goto try_except_handler_5;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_11 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_11;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        CHECK_OBJECT( var_input_tuple );
        tmp_subscribed_name_4 = var_input_tuple;
        CHECK_OBJECT( var_i );
        tmp_subscript_name_4 = var_i;
        tmp_assign_source_12 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 540;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = var_input_value;
            var_input_value = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscript_name_5;
        CHECK_OBJECT( var_old_tuple );
        tmp_subscribed_name_5 = var_old_tuple;
        CHECK_OBJECT( var_i );
        tmp_subscript_name_5 = var_i;
        tmp_assign_source_13 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 541;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = var_old_value;
            var_old_value = tmp_assign_source_13;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( var_key );
        tmp_tuple_element_1 = var_key;
        tmp_assign_source_14 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_14, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_i );
        tmp_tuple_element_1 = var_i;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_14, 1, tmp_tuple_element_1 );
        {
            PyObject *old = var_remkey;
            var_remkey = tmp_assign_source_14;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( var_input_value );
        tmp_compexpr_left_3 = var_input_value;
        CHECK_OBJECT( var_old_value );
        tmp_compexpr_right_3 = var_old_value;
        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 543;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_subscribed_name_6;
            PyObject *tmp_source_name_3;
            PyObject *tmp_subscript_name_6;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_subscribed_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_reminders );
            if ( tmp_subscribed_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( par_name );
            tmp_subscript_name_6 = par_name;
            tmp_assign_source_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
            Py_DECREF( tmp_subscribed_name_6 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_inplace_assign_subscr_1__target;
                tmp_inplace_assign_subscr_1__target = tmp_assign_source_15;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_16;
            CHECK_OBJECT( var_remkey );
            tmp_assign_source_16 = var_remkey;
            {
                PyObject *old = tmp_inplace_assign_subscr_1__subscript;
                tmp_inplace_assign_subscr_1__subscript = tmp_assign_source_16;
                Py_INCREF( tmp_inplace_assign_subscr_1__subscript );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_subscribed_name_7;
            PyObject *tmp_subscript_name_7;
            PyObject *tmp_right_name_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( tmp_inplace_assign_subscr_1__target );
            tmp_subscribed_name_7 = tmp_inplace_assign_subscr_1__target;
            CHECK_OBJECT( tmp_inplace_assign_subscr_1__subscript );
            tmp_subscript_name_7 = tmp_inplace_assign_subscr_1__subscript;
            tmp_left_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_6;
            }
            CHECK_OBJECT( var_old_value );
            tmp_right_name_1 = var_old_value;
            tmp_ass_subvalue_1 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_6;
            }
            CHECK_OBJECT( tmp_inplace_assign_subscr_1__target );
            tmp_ass_subscribed_1 = tmp_inplace_assign_subscr_1__target;
            CHECK_OBJECT( tmp_inplace_assign_subscr_1__subscript );
            tmp_ass_subscript_1 = tmp_inplace_assign_subscr_1__subscript;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_6;
            }
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_subscr_1__target );
        Py_DECREF( tmp_inplace_assign_subscr_1__target );
        tmp_inplace_assign_subscr_1__target = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_subscr_1__subscript );
        Py_DECREF( tmp_inplace_assign_subscr_1__subscript );
        tmp_inplace_assign_subscr_1__subscript = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_5;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_subscr_1__target );
        Py_DECREF( tmp_inplace_assign_subscr_1__target );
        tmp_inplace_assign_subscr_1__target = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_subscr_1__subscript );
        Py_DECREF( tmp_inplace_assign_subscr_1__subscript );
        tmp_inplace_assign_subscr_1__subscript = NULL;

        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_subscribed_name_8;
            PyObject *tmp_subscribed_name_9;
            PyObject *tmp_source_name_4;
            PyObject *tmp_subscript_name_8;
            PyObject *tmp_subscript_name_9;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_subscribed_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_reminder_keys );
            if ( tmp_subscribed_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 546;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( par_name );
            tmp_subscript_name_8 = par_name;
            tmp_subscribed_name_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_8 );
            Py_DECREF( tmp_subscribed_name_9 );
            if ( tmp_subscribed_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 546;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( var_key );
            tmp_subscript_name_9 = var_key;
            tmp_called_instance_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_9 );
            Py_DECREF( tmp_subscribed_name_8 );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 546;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( var_remkey );
            tmp_args_element_name_5 = var_remkey;
            frame_8259023325814c68d4e851583a18d148->m_frame.f_lineno = 546;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_add, call_args );
            }

            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 546;
                type_description_1 = "ooooooooooooo";
                goto try_except_handler_5;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_subscribed_name_11;
        PyObject *tmp_source_name_6;
        PyObject *tmp_subscript_name_10;
        PyObject *tmp_subscript_name_11;
        CHECK_OBJECT( var_bits );
        tmp_source_name_5 = var_bits;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_append );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 547;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        CHECK_OBJECT( var_input_value );
        tmp_left_name_2 = var_input_value;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_subscribed_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_reminders );
        if ( tmp_subscribed_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 547;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        CHECK_OBJECT( par_name );
        tmp_subscript_name_10 = par_name;
        tmp_subscribed_name_10 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_10 );
        Py_DECREF( tmp_subscribed_name_11 );
        if ( tmp_subscribed_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 547;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        CHECK_OBJECT( var_remkey );
        tmp_subscript_name_11 = var_remkey;
        tmp_right_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_10, tmp_subscript_name_11 );
        Py_DECREF( tmp_subscribed_name_10 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 547;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        tmp_args_element_name_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 547;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        frame_8259023325814c68d4e851583a18d148->m_frame.f_lineno = 547;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 547;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_5;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 539;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_5;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_tuple_arg_1;
        CHECK_OBJECT( var_bits );
        tmp_tuple_arg_1 = var_bits;
        tmp_dictset_value = PySequence_Tuple( tmp_tuple_arg_1 );
        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 549;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_new_dict );
        tmp_dictset_dict = var_new_dict;
        CHECK_OBJECT( var_key );
        tmp_dictset_key = var_key;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 549;
            type_description_1 = "ooooooooooooo";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 528;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_ass_subvalue_2;
        PyObject *tmp_ass_subscribed_2;
        PyObject *tmp_source_name_7;
        PyObject *tmp_ass_subscript_2;
        CHECK_OBJECT( par_input_dict );
        tmp_ass_subvalue_2 = par_input_dict;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_ass_subscribed_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_cache );
        if ( tmp_ass_subscribed_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 551;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_ass_subscript_2 = par_name;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
        Py_DECREF( tmp_ass_subscribed_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 551;
            type_description_1 = "ooooooooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8259023325814c68d4e851583a18d148 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8259023325814c68d4e851583a18d148 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8259023325814c68d4e851583a18d148 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8259023325814c68d4e851583a18d148, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8259023325814c68d4e851583a18d148->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8259023325814c68d4e851583a18d148, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8259023325814c68d4e851583a18d148,
        type_description_1,
        par_self,
        par_input_dict,
        par_name,
        var_old_dict,
        var_new_dict,
        var_key,
        var_input_tuple,
        var_old_tuple,
        var_bits,
        var_i,
        var_input_value,
        var_old_value,
        var_remkey
    );


    // Release cached frame.
    if ( frame_8259023325814c68d4e851583a18d148 == cache_frame_8259023325814c68d4e851583a18d148 )
    {
        Py_DECREF( frame_8259023325814c68d4e851583a18d148 );
    }
    cache_frame_8259023325814c68d4e851583a18d148 = NULL;

    assertFrameObject( frame_8259023325814c68d4e851583a18d148 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_new_dict );
    tmp_return_value = var_new_dict;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_14_run );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_old_dict );
    var_old_dict = NULL;

    Py_XDECREF( var_new_dict );
    var_new_dict = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_input_tuple );
    var_input_tuple = NULL;

    Py_XDECREF( var_old_tuple );
    var_old_tuple = NULL;

    Py_XDECREF( var_bits );
    var_bits = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_input_value );
    var_input_value = NULL;

    Py_XDECREF( var_old_value );
    var_old_value = NULL;

    Py_XDECREF( var_remkey );
    var_remkey = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_old_dict );
    var_old_dict = NULL;

    Py_XDECREF( var_new_dict );
    var_new_dict = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_input_tuple );
    var_input_tuple = NULL;

    Py_XDECREF( var_old_tuple );
    var_old_tuple = NULL;

    Py_XDECREF( var_bits );
    var_bits = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_input_value );
    var_input_value = NULL;

    Py_XDECREF( var_old_value );
    var_old_value = NULL;

    Py_XDECREF( var_remkey );
    var_remkey = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_14_run );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_15_cache_clear( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_950f39cfb48f10fc56597463c2229192;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_950f39cfb48f10fc56597463c2229192 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_950f39cfb48f10fc56597463c2229192, codeobj_950f39cfb48f10fc56597463c2229192, module_psutil$_common, sizeof(void *)+sizeof(void *) );
    frame_950f39cfb48f10fc56597463c2229192 = cache_frame_950f39cfb48f10fc56597463c2229192;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_950f39cfb48f10fc56597463c2229192 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_950f39cfb48f10fc56597463c2229192 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_lock );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 556;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_2 = tmp_with_1__source;
        tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___enter__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 556;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 556;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 556;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_3 = tmp_with_1__source;
        tmp_assign_source_3 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___exit__ );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 556;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_assign_source_4;
        tmp_assign_source_4 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_4;
    }
    // Tried code:
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_name );
        tmp_compexpr_left_1 = par_name;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_cache );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 558;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 558;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_clear );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 558;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_reminders );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 559;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_clear );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_reminder_keys );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 560;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 560;
            tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_clear );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 560;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_source_name_7;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_cache );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 562;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( par_name );
            tmp_args_element_name_1 = par_name;
            tmp_args_element_name_2 = Py_None;
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 562;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_call_result_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_pop, call_args );
            }

            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 562;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        {
            PyObject *tmp_called_instance_5;
            PyObject *tmp_source_name_8;
            PyObject *tmp_call_result_5;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( par_self );
            tmp_source_name_8 = par_self;
            tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_reminders );
            if ( tmp_called_instance_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 563;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( par_name );
            tmp_args_element_name_3 = par_name;
            tmp_args_element_name_4 = Py_None;
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 563;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_call_result_5 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_5, const_str_plain_pop, call_args );
            }

            Py_DECREF( tmp_called_instance_5 );
            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 563;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        {
            PyObject *tmp_called_instance_6;
            PyObject *tmp_source_name_9;
            PyObject *tmp_call_result_6;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( par_self );
            tmp_source_name_9 = par_self;
            tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_reminder_keys );
            if ( tmp_called_instance_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 564;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( par_name );
            tmp_args_element_name_5 = par_name;
            tmp_args_element_name_6 = Py_None;
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 564;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_call_result_6 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain_pop, call_args );
            }

            Py_DECREF( tmp_called_instance_6 );
            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 564;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        branch_end_1:;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_950f39cfb48f10fc56597463c2229192, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_950f39cfb48f10fc56597463c2229192, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 556;
            type_description_1 = "oo";
            goto try_except_handler_5;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_assign_source_5;
            tmp_assign_source_5 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_5;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_2 = tmp_with_1__exit;
            tmp_args_element_name_7 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_8 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_9 = EXC_TRACEBACK(PyThreadState_GET());
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 564;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 564;
                type_description_1 = "oo";
                goto try_except_handler_5;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 564;
                type_description_1 = "oo";
                goto try_except_handler_5;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 564;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_950f39cfb48f10fc56597463c2229192->m_frame) frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oo";
            goto try_except_handler_5;
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 556;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_950f39cfb48f10fc56597463c2229192->m_frame) frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oo";
        goto try_except_handler_5;
        branch_end_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_15_cache_clear );
    return NULL;
    // End of try:
    try_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_call_result_7;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_3 = tmp_with_1__exit;
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 564;
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_3 );
                Py_XDECREF( exception_keeper_value_3 );
                Py_XDECREF( exception_keeper_tb_3 );

                exception_lineno = 564;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        branch_no_4:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_5;
        nuitka_bool tmp_compexpr_left_4;
        nuitka_bool tmp_compexpr_right_4;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_4 = tmp_with_1__indicator;
        tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
        tmp_condition_result_5 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_8;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            frame_950f39cfb48f10fc56597463c2229192->m_frame.f_lineno = 564;
            tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 564;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_no_5:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_950f39cfb48f10fc56597463c2229192 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_950f39cfb48f10fc56597463c2229192 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_950f39cfb48f10fc56597463c2229192, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_950f39cfb48f10fc56597463c2229192->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_950f39cfb48f10fc56597463c2229192, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_950f39cfb48f10fc56597463c2229192,
        type_description_1,
        par_self,
        par_name
    );


    // Release cached frame.
    if ( frame_950f39cfb48f10fc56597463c2229192 == cache_frame_950f39cfb48f10fc56597463c2229192 )
    {
        Py_DECREF( frame_950f39cfb48f10fc56597463c2229192 );
    }
    cache_frame_950f39cfb48f10fc56597463c2229192 = NULL;

    assertFrameObject( frame_950f39cfb48f10fc56597463c2229192 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_15_cache_clear );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_15_cache_clear );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_16_cache_info( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_06c6ff0ff3b5724f864321db3870e1a4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_06c6ff0ff3b5724f864321db3870e1a4 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_06c6ff0ff3b5724f864321db3870e1a4, codeobj_06c6ff0ff3b5724f864321db3870e1a4, module_psutil$_common, sizeof(void *) );
    frame_06c6ff0ff3b5724f864321db3870e1a4 = cache_frame_06c6ff0ff3b5724f864321db3870e1a4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_06c6ff0ff3b5724f864321db3870e1a4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_06c6ff0ff3b5724f864321db3870e1a4 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_lock );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_2 = tmp_with_1__source;
        tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___enter__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame.f_lineno = 568;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_3 = tmp_with_1__source;
        tmp_assign_source_3 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___exit__ );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_assign_source_4;
        tmp_assign_source_4 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_4;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_cache );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 569;
            type_description_1 = "o";
            goto try_except_handler_4;
        }
        tmp_return_value = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_reminders );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 569;
            type_description_1 = "o";
            goto try_except_handler_4;
        }
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_reminder_keys );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 569;
            type_description_1 = "o";
            goto try_except_handler_4;
        }
        PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_1 );
        goto try_return_handler_3;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_16_cache_info );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_06c6ff0ff3b5724f864321db3870e1a4, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_06c6ff0ff3b5724f864321db3870e1a4, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;
            type_description_1 = "o";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_5;
            tmp_assign_source_5 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_5;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_2 = tmp_with_1__exit;
            tmp_args_element_name_1 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_2 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_3 = EXC_TRACEBACK(PyThreadState_GET());
            frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame.f_lineno = 569;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 569;
                type_description_1 = "o";
                goto try_except_handler_5;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 569;
                type_description_1 = "o";
                goto try_except_handler_5;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 569;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame) frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "o";
            goto try_except_handler_5;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 568;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame) frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "o";
        goto try_except_handler_5;
        branch_end_1:;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_16_cache_info );
    return NULL;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Return handler code:
    try_return_handler_3:;
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( tmp_with_1__exit );
        tmp_called_name_3 = tmp_with_1__exit;
        frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame.f_lineno = 569;
        tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 569;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_return_handler_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame.f_lineno = 569;
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_3 );
                Py_XDECREF( exception_keeper_value_3 );
                Py_XDECREF( exception_keeper_tb_3 );

                exception_lineno = 569;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_3:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame.f_lineno = 569;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 569;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_4:;
    }
    goto try_end_4;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_06c6ff0ff3b5724f864321db3870e1a4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_06c6ff0ff3b5724f864321db3870e1a4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_06c6ff0ff3b5724f864321db3870e1a4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_06c6ff0ff3b5724f864321db3870e1a4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_06c6ff0ff3b5724f864321db3870e1a4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_06c6ff0ff3b5724f864321db3870e1a4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_06c6ff0ff3b5724f864321db3870e1a4,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_06c6ff0ff3b5724f864321db3870e1a4 == cache_frame_06c6ff0ff3b5724f864321db3870e1a4 )
    {
        Py_DECREF( frame_06c6ff0ff3b5724f864321db3870e1a4 );
    }
    cache_frame_06c6ff0ff3b5724f864321db3870e1a4 = NULL;

    assertFrameObject( frame_06c6ff0ff3b5724f864321db3870e1a4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_16_cache_info );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_16_cache_info );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_17_wrap_numbers( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_input_dict = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_38c8ef5f90fd9de5b95bf5d0f86c9543;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_38c8ef5f90fd9de5b95bf5d0f86c9543 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_38c8ef5f90fd9de5b95bf5d0f86c9543, codeobj_38c8ef5f90fd9de5b95bf5d0f86c9543, module_psutil$_common, sizeof(void *)+sizeof(void *) );
    frame_38c8ef5f90fd9de5b95bf5d0f86c9543 = cache_frame_38c8ef5f90fd9de5b95bf5d0f86c9543;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain__wn );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__wn );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_wn" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 577;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_lock );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 577;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_2 = tmp_with_1__source;
        tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___enter__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 577;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = 577;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 577;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_3 = tmp_with_1__source;
        tmp_assign_source_3 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___exit__ );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 577;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_assign_source_4;
        tmp_assign_source_4 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_4;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain__wn );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__wn );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_wn" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 578;
            type_description_1 = "oo";
            goto try_except_handler_4;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_input_dict );
        tmp_args_element_name_1 = par_input_dict;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_2 = par_name;
        frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = 578;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_run, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 578;
            type_description_1 = "oo";
            goto try_except_handler_4;
        }
        goto try_return_handler_3;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_17_wrap_numbers );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_38c8ef5f90fd9de5b95bf5d0f86c9543, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_38c8ef5f90fd9de5b95bf5d0f86c9543, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 577;
            type_description_1 = "oo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_5;
            tmp_assign_source_5 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_5;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_2 = tmp_with_1__exit;
            tmp_args_element_name_3 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_4 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_5 = EXC_TRACEBACK(PyThreadState_GET());
            frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = 578;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 578;
                type_description_1 = "oo";
                goto try_except_handler_5;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 578;
                type_description_1 = "oo";
                goto try_except_handler_5;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 578;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame) frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oo";
            goto try_except_handler_5;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 577;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame) frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oo";
        goto try_except_handler_5;
        branch_end_1:;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_17_wrap_numbers );
    return NULL;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Return handler code:
    try_return_handler_3:;
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( tmp_with_1__exit );
        tmp_called_name_3 = tmp_with_1__exit;
        frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = 578;
        tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 578;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_return_handler_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = 578;
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_3 );
                Py_XDECREF( exception_keeper_value_3 );
                Py_XDECREF( exception_keeper_tb_3 );

                exception_lineno = 578;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_3:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame.f_lineno = 578;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 578;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_4:;
    }
    goto try_end_4;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_38c8ef5f90fd9de5b95bf5d0f86c9543, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_38c8ef5f90fd9de5b95bf5d0f86c9543->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_38c8ef5f90fd9de5b95bf5d0f86c9543, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_38c8ef5f90fd9de5b95bf5d0f86c9543,
        type_description_1,
        par_input_dict,
        par_name
    );


    // Release cached frame.
    if ( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 == cache_frame_38c8ef5f90fd9de5b95bf5d0f86c9543 )
    {
        Py_DECREF( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 );
    }
    cache_frame_38c8ef5f90fd9de5b95bf5d0f86c9543 = NULL;

    assertFrameObject( frame_38c8ef5f90fd9de5b95bf5d0f86c9543 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_17_wrap_numbers );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_input_dict );
    Py_DECREF( par_input_dict );
    par_input_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_17_wrap_numbers );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_18_open_binary( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fname = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_896e38fe86b92d930b83395b0b2a72e3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_896e38fe86b92d930b83395b0b2a72e3 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_896e38fe86b92d930b83395b0b2a72e3, codeobj_896e38fe86b92d930b83395b0b2a72e3, module_psutil$_common, sizeof(void *)+sizeof(void *) );
    frame_896e38fe86b92d930b83395b0b2a72e3 = cache_frame_896e38fe86b92d930b83395b0b2a72e3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_896e38fe86b92d930b83395b0b2a72e3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_896e38fe86b92d930b83395b0b2a72e3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_dircall_arg1_1 = LOOKUP_BUILTIN( const_str_plain_open );
        assert( tmp_dircall_arg1_1 != NULL );
        CHECK_OBJECT( par_fname );
        tmp_tuple_element_1 = par_fname;
        tmp_dircall_arg2_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_rb;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 587;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_896e38fe86b92d930b83395b0b2a72e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_896e38fe86b92d930b83395b0b2a72e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_896e38fe86b92d930b83395b0b2a72e3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_896e38fe86b92d930b83395b0b2a72e3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_896e38fe86b92d930b83395b0b2a72e3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_896e38fe86b92d930b83395b0b2a72e3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_896e38fe86b92d930b83395b0b2a72e3,
        type_description_1,
        par_fname,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_896e38fe86b92d930b83395b0b2a72e3 == cache_frame_896e38fe86b92d930b83395b0b2a72e3 )
    {
        Py_DECREF( frame_896e38fe86b92d930b83395b0b2a72e3 );
    }
    cache_frame_896e38fe86b92d930b83395b0b2a72e3 = NULL;

    assertFrameObject( frame_896e38fe86b92d930b83395b0b2a72e3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_18_open_binary );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fname );
    Py_DECREF( par_fname );
    par_fname = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fname );
    Py_DECREF( par_fname );
    par_fname = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_18_open_binary );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_19_open_text( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fname = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_901790350ed744472583113cd8b2bff7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_901790350ed744472583113cd8b2bff7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_901790350ed744472583113cd8b2bff7, codeobj_901790350ed744472583113cd8b2bff7, module_psutil$_common, sizeof(void *)+sizeof(void *) );
    frame_901790350ed744472583113cd8b2bff7 = cache_frame_901790350ed744472583113cd8b2bff7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_901790350ed744472583113cd8b2bff7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_901790350ed744472583113cd8b2bff7 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_mvar_value_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 595;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 595;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_mvar_value_2;
            CHECK_OBJECT( par_kwargs );
            tmp_source_name_1 = par_kwargs;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_setdefault );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 599;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_1 = const_str_plain_encoding;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ENCODING );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ENCODING" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 599;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_2 = tmp_mvar_value_2;
            frame_901790350ed744472583113cd8b2bff7->m_frame.f_lineno = 599;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 599;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( par_kwargs );
            tmp_source_name_2 = par_kwargs;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_setdefault );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 600;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_3 = const_str_plain_errors;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING_ERRS );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ENCODING_ERRS );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ENCODING_ERRS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 600;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_4 = tmp_mvar_value_3;
            frame_901790350ed744472583113cd8b2bff7->m_frame.f_lineno = 600;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 600;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_dircall_arg1_1 = LOOKUP_BUILTIN( const_str_plain_open );
        assert( tmp_dircall_arg1_1 != NULL );
        CHECK_OBJECT( par_fname );
        tmp_tuple_element_1 = par_fname;
        tmp_dircall_arg2_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_rt;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 601;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_901790350ed744472583113cd8b2bff7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_901790350ed744472583113cd8b2bff7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_901790350ed744472583113cd8b2bff7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_901790350ed744472583113cd8b2bff7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_901790350ed744472583113cd8b2bff7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_901790350ed744472583113cd8b2bff7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_901790350ed744472583113cd8b2bff7,
        type_description_1,
        par_fname,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_901790350ed744472583113cd8b2bff7 == cache_frame_901790350ed744472583113cd8b2bff7 )
    {
        Py_DECREF( frame_901790350ed744472583113cd8b2bff7 );
    }
    cache_frame_901790350ed744472583113cd8b2bff7 = NULL;

    assertFrameObject( frame_901790350ed744472583113cd8b2bff7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_19_open_text );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fname );
    Py_DECREF( par_fname );
    par_fname = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fname );
    Py_DECREF( par_fname );
    par_fname = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_19_open_text );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_20_bytes2human( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_n = python_pars[ 0 ];
    PyObject *par_format = python_pars[ 1 ];
    PyObject *var_symbols = NULL;
    PyObject *var_prefix = NULL;
    PyObject *var_i = NULL;
    PyObject *var_s = NULL;
    PyObject *var_symbol = NULL;
    PyObject *var_value = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_5b6934d7ca2e72fbb2fbe1426870eb4b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    PyObject *locals_psutil$_common$$$function_20_bytes2human = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_5b6934d7ca2e72fbb2fbe1426870eb4b = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_tuple_f0f03d937c5a4d26ac0593ac34f32f43_tuple;
        assert( var_symbols == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_symbols = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( var_prefix == NULL );
        var_prefix = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5b6934d7ca2e72fbb2fbe1426870eb4b, codeobj_5b6934d7ca2e72fbb2fbe1426870eb4b, module_psutil$_common, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5b6934d7ca2e72fbb2fbe1426870eb4b = cache_frame_5b6934d7ca2e72fbb2fbe1426870eb4b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5b6934d7ca2e72fbb2fbe1426870eb4b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5b6934d7ca2e72fbb2fbe1426870eb4b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        tmp_called_name_1 = (PyObject *)&PyEnum_Type;
        CHECK_OBJECT( var_symbols );
        tmp_subscribed_name_1 = var_symbols;
        tmp_subscript_name_1 = const_slice_int_pos_1_none_none;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 615;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_5b6934d7ca2e72fbb2fbe1426870eb4b->m_frame.f_lineno = 615;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 615;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 615;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 615;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_5 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 615;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 615;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 615;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooo";
                    exception_lineno = 615;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooo";
            exception_lineno = 615;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_8;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_s;
            var_s = tmp_assign_source_9;
            Py_INCREF( var_s );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        tmp_left_name_1 = const_int_pos_1;
        CHECK_OBJECT( var_i );
        tmp_left_name_3 = var_i;
        tmp_right_name_2 = const_int_pos_1;
        tmp_left_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_3, tmp_right_name_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 616;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_right_name_3 = const_int_pos_10;
        tmp_right_name_1 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 616;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_dictset_value = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 616;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_prefix );
        tmp_dictset_dict = var_prefix;
        CHECK_OBJECT( var_s );
        tmp_dictset_key = var_s;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 616;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 615;
        type_description_1 = "oooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        tmp_called_name_2 = (PyObject *)&PyReversed_Type;
        CHECK_OBJECT( var_symbols );
        tmp_subscribed_name_2 = var_symbols;
        tmp_subscript_name_2 = const_slice_int_pos_1_none_none;
        tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 617;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_5b6934d7ca2e72fbb2fbe1426870eb4b->m_frame.f_lineno = 617;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 617;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 617;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_10;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 617;
                goto try_except_handler_5;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_12 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_symbol;
            var_symbol = tmp_assign_source_12;
            Py_INCREF( var_symbol );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_key_name_1;
        CHECK_OBJECT( par_n );
        tmp_compexpr_left_1 = par_n;
        CHECK_OBJECT( var_prefix );
        tmp_dict_name_1 = var_prefix;
        CHECK_OBJECT( var_symbol );
        tmp_key_name_1 = var_symbol;
        tmp_compexpr_right_1 = DICT_GET_ITEM( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 618;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 618;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_left_name_4;
            PyObject *tmp_float_arg_1;
            PyObject *tmp_right_name_4;
            PyObject *tmp_dict_name_2;
            PyObject *tmp_key_name_2;
            CHECK_OBJECT( par_n );
            tmp_float_arg_1 = par_n;
            tmp_left_name_4 = TO_FLOAT( tmp_float_arg_1 );
            if ( tmp_left_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 619;
                type_description_1 = "oooooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( var_prefix );
            tmp_dict_name_2 = var_prefix;
            CHECK_OBJECT( var_symbol );
            tmp_key_name_2 = var_symbol;
            tmp_right_name_4 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
            if ( tmp_right_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_4 );

                exception_lineno = 619;
                type_description_1 = "oooooooo";
                goto try_except_handler_5;
            }
            tmp_assign_source_13 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_4 );
            Py_DECREF( tmp_left_name_4 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 619;
                type_description_1 = "oooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = var_value;
                var_value = tmp_assign_source_13;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_left_name_5;
            PyObject *tmp_right_name_5;
            CHECK_OBJECT( par_format );
            tmp_left_name_5 = par_format;
            if (locals_psutil$_common$$$function_20_bytes2human == NULL) locals_psutil$_common$$$function_20_bytes2human = PyDict_New();
            tmp_right_name_5 = locals_psutil$_common$$$function_20_bytes2human;
            Py_INCREF( tmp_right_name_5 );
            if ( par_n != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par_n );
                value = par_n;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_n, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_n );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( par_format != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par_format );
                value = par_format;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_format, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_format );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_symbols != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_symbols );
                value = var_symbols;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_symbols, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_symbols );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_prefix != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_prefix );
                value = var_prefix;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_prefix, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_prefix );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_i != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_i );
                value = var_i;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_i, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_i );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_s != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_s );
                value = var_s;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_s, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_s );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_symbol != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_symbol );
                value = var_symbol;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_symbol, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_symbol );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_value != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_value );
                value = var_value;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_right_name_5, (Nuitka_StringObject *)const_str_plain_value, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_right_name_5, const_str_plain_value );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
            Py_DECREF( tmp_right_name_5 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 620;
                type_description_1 = "oooooooo";
                goto try_except_handler_5;
            }
            goto try_return_handler_5;
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 617;
        type_description_1 = "oooooooo";
        goto try_except_handler_5;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_4;
    // Return handler code:
    try_return_handler_5:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__iter_value );
    Py_DECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_left_name_6;
        PyObject *tmp_right_name_6;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        CHECK_OBJECT( par_format );
        tmp_left_name_6 = par_format;
        tmp_dict_key_1 = const_str_plain_symbol;
        CHECK_OBJECT( var_symbols );
        tmp_subscribed_name_3 = var_symbols;
        tmp_subscript_name_3 = const_int_0;
        tmp_dict_value_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 621;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_6 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_value;
        CHECK_OBJECT( par_n );
        tmp_dict_value_2 = par_n;
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
        Py_DECREF( tmp_right_name_6 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 621;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5b6934d7ca2e72fbb2fbe1426870eb4b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5b6934d7ca2e72fbb2fbe1426870eb4b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5b6934d7ca2e72fbb2fbe1426870eb4b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5b6934d7ca2e72fbb2fbe1426870eb4b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5b6934d7ca2e72fbb2fbe1426870eb4b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5b6934d7ca2e72fbb2fbe1426870eb4b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5b6934d7ca2e72fbb2fbe1426870eb4b,
        type_description_1,
        par_n,
        par_format,
        var_symbols,
        var_prefix,
        var_i,
        var_s,
        var_symbol,
        var_value
    );


    // Release cached frame.
    if ( frame_5b6934d7ca2e72fbb2fbe1426870eb4b == cache_frame_5b6934d7ca2e72fbb2fbe1426870eb4b )
    {
        Py_DECREF( frame_5b6934d7ca2e72fbb2fbe1426870eb4b );
    }
    cache_frame_5b6934d7ca2e72fbb2fbe1426870eb4b = NULL;

    assertFrameObject( frame_5b6934d7ca2e72fbb2fbe1426870eb4b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_20_bytes2human );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_n );
    Py_DECREF( par_n );
    par_n = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)var_symbols );
    Py_DECREF( var_symbols );
    var_symbols = NULL;

    CHECK_OBJECT( (PyObject *)var_prefix );
    Py_DECREF( var_prefix );
    var_prefix = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_symbol );
    var_symbol = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_n );
    Py_DECREF( par_n );
    par_n = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)var_symbols );
    Py_DECREF( var_symbols );
    var_symbols = NULL;

    CHECK_OBJECT( (PyObject *)var_prefix );
    Py_DECREF( var_prefix );
    var_prefix = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_symbol );
    var_symbol = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_20_bytes2human );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_psutil$_common$$$function_20_bytes2human );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.
    Py_XDECREF( locals_psutil$_common$$$function_20_bytes2human );


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_21_get_procfs_path( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_f7fc201a081ec45c9e3477e92757801b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f7fc201a081ec45c9e3477e92757801b = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_f7fc201a081ec45c9e3477e92757801b, codeobj_f7fc201a081ec45c9e3477e92757801b, module_psutil$_common, 0 );
    frame_f7fc201a081ec45c9e3477e92757801b = cache_frame_f7fc201a081ec45c9e3477e92757801b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f7fc201a081ec45c9e3477e92757801b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f7fc201a081ec45c9e3477e92757801b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 626;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_modules );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 626;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_str_plain_psutil;
        tmp_source_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 626;

            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_PROCFS_PATH );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 626;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7fc201a081ec45c9e3477e92757801b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7fc201a081ec45c9e3477e92757801b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7fc201a081ec45c9e3477e92757801b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f7fc201a081ec45c9e3477e92757801b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f7fc201a081ec45c9e3477e92757801b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f7fc201a081ec45c9e3477e92757801b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f7fc201a081ec45c9e3477e92757801b,
        type_description_1
    );


    // Release cached frame.
    if ( frame_f7fc201a081ec45c9e3477e92757801b == cache_frame_f7fc201a081ec45c9e3477e92757801b )
    {
        Py_DECREF( frame_f7fc201a081ec45c9e3477e92757801b );
    }
    cache_frame_f7fc201a081ec45c9e3477e92757801b = NULL;

    assertFrameObject( frame_f7fc201a081ec45c9e3477e92757801b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_21_get_procfs_path );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_22_decode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_bf20f80eec9973461bde9cd8d9b8a9d5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_bf20f80eec9973461bde9cd8d9b8a9d5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bf20f80eec9973461bde9cd8d9b8a9d5, codeobj_bf20f80eec9973461bde9cd8d9b8a9d5, module_psutil$_common, sizeof(void *) );
    frame_bf20f80eec9973461bde9cd8d9b8a9d5 = cache_frame_bf20f80eec9973461bde9cd8d9b8a9d5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bf20f80eec9973461bde9cd8d9b8a9d5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bf20f80eec9973461bde9cd8d9b8a9d5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_s );
        tmp_source_name_1 = par_s;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_decode );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 631;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_encoding;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ENCODING );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ENCODING" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 631;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_1 = tmp_mvar_value_1;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_errors;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING_ERRS );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ENCODING_ERRS );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ENCODING_ERRS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 631;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_2 = tmp_mvar_value_2;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_bf20f80eec9973461bde9cd8d9b8a9d5->m_frame.f_lineno = 631;
        tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 631;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf20f80eec9973461bde9cd8d9b8a9d5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf20f80eec9973461bde9cd8d9b8a9d5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf20f80eec9973461bde9cd8d9b8a9d5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bf20f80eec9973461bde9cd8d9b8a9d5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bf20f80eec9973461bde9cd8d9b8a9d5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bf20f80eec9973461bde9cd8d9b8a9d5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bf20f80eec9973461bde9cd8d9b8a9d5,
        type_description_1,
        par_s
    );


    // Release cached frame.
    if ( frame_bf20f80eec9973461bde9cd8d9b8a9d5 == cache_frame_bf20f80eec9973461bde9cd8d9b8a9d5 )
    {
        Py_DECREF( frame_bf20f80eec9973461bde9cd8d9b8a9d5 );
    }
    cache_frame_bf20f80eec9973461bde9cd8d9b8a9d5 = NULL;

    assertFrameObject( frame_bf20f80eec9973461bde9cd8d9b8a9d5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_22_decode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_22_decode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_psutil$_common$$$function_23_decode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_s );
    tmp_return_value = par_s;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_23_decode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( psutil$_common$$$function_23_decode );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_10_deprecated_method,
        const_str_plain_deprecated_method,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f435564dcf5a9e89cb4b81ae6f5553f5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_91bde82034c2ba995a6e7ea81b4ee6c4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method$$$function_1_outer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_10_deprecated_method$$$function_1_outer,
        const_str_plain_outer,
#if PYTHON_VERSION >= 300
        const_str_digest_af5c0b95f5f11bc66744e3de077488bb,
#endif
        codeobj_8b587c31f069329851643cb68c828c33,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method$$$function_1_outer$$$function_1_inner(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_10_deprecated_method$$$function_1_outer$$$function_1_inner,
        const_str_plain_inner,
#if PYTHON_VERSION >= 300
        const_str_digest_716f8d9d7522e1bc954f2cca2624926c,
#endif
        codeobj_04839f818328ef15580890c4cd786f57,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_11___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_11___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_bbd7ba56388169475bb178ec7024ca39,
#endif
        codeobj_17cd2ecfb0288cbaa7706a9e05df80cc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_12__add_dict(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_12__add_dict,
        const_str_plain__add_dict,
#if PYTHON_VERSION >= 300
        const_str_digest_5cc34510cf5423be6b3dd305d1fab306,
#endif
        codeobj_ef16a5a470b8dcb15406d714b9e72dfc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_13__remove_dead_reminders(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_13__remove_dead_reminders,
        const_str_plain__remove_dead_reminders,
#if PYTHON_VERSION >= 300
        const_str_digest_457d32d6501e34673ad4aa2f55c371a6,
#endif
        codeobj_19ab808ff3653aac5d36f73d1d924f02,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_b2b793ac5e593d3efbfbe861ccb88056,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_14_run(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_14_run,
        const_str_plain_run,
#if PYTHON_VERSION >= 300
        const_str_digest_fc55a6ec1010a1560f644da71f4b3cba,
#endif
        codeobj_8259023325814c68d4e851583a18d148,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_cb4806a92f0a38e89c5326d8bded12ed,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_15_cache_clear( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_15_cache_clear,
        const_str_plain_cache_clear,
#if PYTHON_VERSION >= 300
        const_str_digest_115252dd9884ce3050fc49a9776440d5,
#endif
        codeobj_950f39cfb48f10fc56597463c2229192,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_3edef1fa3eed954f4e1fabc8924bb36f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_16_cache_info(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_16_cache_info,
        const_str_plain_cache_info,
#if PYTHON_VERSION >= 300
        const_str_digest_f1920876d07de7b4ca17e71fd0a07cbe,
#endif
        codeobj_06c6ff0ff3b5724f864321db3870e1a4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_8154c1b785576b641e6ce5bfaf028ff5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_17_wrap_numbers(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_17_wrap_numbers,
        const_str_plain_wrap_numbers,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_38c8ef5f90fd9de5b95bf5d0f86c9543,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_d738b7bb5a4adf77b8882bd7cfde13ad,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_18_open_binary(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_18_open_binary,
        const_str_plain_open_binary,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_896e38fe86b92d930b83395b0b2a72e3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_19_open_text(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_19_open_text,
        const_str_plain_open_text,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_901790350ed744472583113cd8b2bff7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_e504a1c5fb44d69b43b64780b96360d6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_1_usage_percent( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_1_usage_percent,
        const_str_plain_usage_percent,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e04608805b76764e8ec9b47cdfb85c3a,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_99410fa7664fe22ccad236d080673163,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_20_bytes2human( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_20_bytes2human,
        const_str_plain_bytes2human,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5b6934d7ca2e72fbb2fbe1426870eb4b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_95896ac3fdecfb6ca65cbe556616215c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_21_get_procfs_path(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_21_get_procfs_path,
        const_str_plain_get_procfs_path,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f7fc201a081ec45c9e3477e92757801b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_f6856f5d282ebea61302c4adae8f9ada,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_22_decode(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_22_decode,
        const_str_plain_decode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bf20f80eec9973461bde9cd8d9b8a9d5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_23_decode(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_23_decode,
        const_str_plain_decode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_015df25e645212257ddbaaf1f497720b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_2_memoize(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_2_memoize,
        const_str_plain_memoize,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1d0c897b6dd90f96bd44e10f008a1661,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_6c181c57aa262d51beb7a1726d9302b5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_2_memoize$$$function_1_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_2_memoize$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_1d916fa90b3796c4ab91e848071040be,
#endif
        codeobj_c51fa982b1f0c2958d4822a1691e57aa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_2_memoize$$$function_2_cache_clear(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_2_memoize$$$function_2_cache_clear,
        const_str_plain_cache_clear,
#if PYTHON_VERSION >= 300
        const_str_digest_28a10ab0cf555529690fb2e57e845886,
#endif
        codeobj_69a9ead8ba6e6ca78460c619c25a540c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_7f773a2c61aafb6080b6e3fa0c482cee,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_3_memoize_when_activated,
        const_str_plain_memoize_when_activated,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a0e916af95934d7eab7741981deb2f51,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_93046e394fea44e344d65326f6299d5e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_3_memoize_when_activated$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_4571a7d7297fb61bce152a7b77fd65db,
#endif
        codeobj_53859f37c137aa1d0a547e9b72865b8c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_2_cache_activate(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_3_memoize_when_activated$$$function_2_cache_activate,
        const_str_plain_cache_activate,
#if PYTHON_VERSION >= 300
        const_str_digest_9d5da3b1c04b6be63df52845d9808c46,
#endif
        codeobj_589f1beda036b7ac243111b1399dc433,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_2045132428b7bed20b21befde72ba292,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_3_memoize_when_activated$$$function_3_cache_deactivate,
        const_str_plain_cache_deactivate,
#if PYTHON_VERSION >= 300
        const_str_digest_386170845bb8c4a4ab83068de03309f2,
#endif
        codeobj_5a4482fce79cd9cb968a4339ebbc16fb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_85a627319c236bc8f9aa4f6ec2242376,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_4_isfile_strict(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_4_isfile_strict,
        const_str_plain_isfile_strict,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3bd930188379a197c6f391320fec4502,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_4f68eb82902a9b7b547feaa053e622b7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_5_path_exists_strict(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_5_path_exists_strict,
        const_str_plain_path_exists_strict,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ead688a2167ee1cf40fa04f3f17e9f12,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_af97312a62c0414513bc3cf713b1b59a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_6_supports_ipv6(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_6_supports_ipv6,
        const_str_plain_supports_ipv6,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_02c36612f9a1e0db15c02518dfab19ce,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_5d6d330dc4d79bec1ba9037cf33d5c79,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_7_parse_environ_block(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_7_parse_environ_block,
        const_str_plain_parse_environ_block,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_57950a6fa80e5368b9a8128bcd9382da,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_e53dfc57c7e14e31d0998887df0f3e21,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_8_sockfam_to_enum(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_8_sockfam_to_enum,
        const_str_plain_sockfam_to_enum,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_53d9763bad7ab7dc936ba3bf85cfa331,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_230f09e45b705cbc9bc92403ddd4c5f2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_psutil$_common$$$function_9_socktype_to_enum(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_psutil$_common$$$function_9_socktype_to_enum,
        const_str_plain_socktype_to_enum,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ebbee4ed4136870a4f406fd32eea55b2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_psutil$_common,
        const_str_digest_d2a1a54d8c7e0284a0763d37c4a6b2ab,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_psutil$_common =
{
    PyModuleDef_HEAD_INIT,
    "psutil._common",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(psutil$_common)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(psutil$_common)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_psutil$_common );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("psutil._common: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("psutil._common: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("psutil._common: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpsutil$_common" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_psutil$_common = Py_InitModule4(
        "psutil._common",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_psutil$_common = PyModule_Create( &mdef_psutil$_common );
#endif

    moduledict_psutil$_common = MODULE_DICT( module_psutil$_common );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_psutil$_common,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_psutil$_common,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_psutil$_common,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_psutil$_common,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_psutil$_common );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_c21138c8d575fc0b2b6ff4782a202f72, module_psutil$_common );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    struct Nuitka_FrameObject *frame_641a34ae48b2aa2a3be9421131bc3092;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_psutil$_common_130 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_eb427e220e252bc7ee7380d6c9a0dd4c_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *locals_psutil$_common_142 = NULL;
    struct Nuitka_FrameObject *frame_3eb634e8d4080b8c4c77a2731a7d5b07_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *locals_psutil$_common_485 = NULL;
    struct Nuitka_FrameObject *frame_0928eb9f20a13780a4806d89700f19fe_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_0928eb9f20a13780a4806d89700f19fe_4 = NULL;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_c5c04d92c90c74d705f2d90dc328b3dd;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_641a34ae48b2aa2a3be9421131bc3092 = MAKE_MODULE_FRAME( codeobj_641a34ae48b2aa2a3be9421131bc3092, module_psutil$_common );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_641a34ae48b2aa2a3be9421131bc3092 );
    assert( Py_REFCNT( frame_641a34ae48b2aa2a3be9421131bc3092 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 10;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_division );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_division, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_contextlib;
        tmp_globals_name_1 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 12;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_contextlib, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_errno;
        tmp_globals_name_2 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 13;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        assert( !(tmp_assign_source_6 == NULL) );
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_errno, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_functools;
        tmp_globals_name_3 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 14;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_functools, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_os;
        tmp_globals_name_4 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 15;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_socket;
        tmp_globals_name_5 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 16;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socket, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_stat;
        tmp_globals_name_6 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 17;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_stat, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_sys;
        tmp_globals_name_7 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = Py_None;
        tmp_level_name_7 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 18;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        assert( !(tmp_assign_source_11 == NULL) );
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_threading;
        tmp_globals_name_8 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = Py_None;
        tmp_level_name_8 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 19;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_threading, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_warnings;
        tmp_globals_name_9 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = Py_None;
        tmp_level_name_9 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 20;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_warnings, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_plain_collections;
        tmp_globals_name_10 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_defaultdict_tuple;
        tmp_level_name_10 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 21;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_defaultdict );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_defaultdict, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_plain_collections;
        tmp_globals_name_11 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_namedtuple_tuple;
        tmp_level_name_11 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 22;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_namedtuple );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_plain_socket;
        tmp_globals_name_12 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_AF_INET_tuple;
        tmp_level_name_12 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 23;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_AF_INET );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_plain_socket;
        tmp_globals_name_13 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = const_tuple_str_plain_SOCK_DGRAM_tuple;
        tmp_level_name_13 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 24;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_SOCK_DGRAM );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_14;
        PyObject *tmp_globals_name_14;
        PyObject *tmp_locals_name_14;
        PyObject *tmp_fromlist_name_14;
        PyObject *tmp_level_name_14;
        tmp_name_name_14 = const_str_plain_socket;
        tmp_globals_name_14 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_14 = Py_None;
        tmp_fromlist_name_14 = const_tuple_str_plain_SOCK_STREAM_tuple;
        tmp_level_name_14 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 25;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_SOCK_STREAM );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM, tmp_assign_source_18 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_15;
        PyObject *tmp_globals_name_15;
        PyObject *tmp_locals_name_15;
        PyObject *tmp_fromlist_name_15;
        PyObject *tmp_level_name_15;
        tmp_name_name_15 = const_str_plain_socket;
        tmp_globals_name_15 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_15 = Py_None;
        tmp_fromlist_name_15 = const_tuple_str_plain_AF_INET6_tuple;
        tmp_level_name_15 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 27;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto try_except_handler_1;
        }
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_AF_INET6 );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6, tmp_assign_source_19 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_641a34ae48b2aa2a3be9421131bc3092, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_641a34ae48b2aa2a3be9421131bc3092, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_20;
            tmp_assign_source_20 = Py_None;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6, tmp_assign_source_20 );
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 26;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_641a34ae48b2aa2a3be9421131bc3092->m_frame) frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_2;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_16;
        PyObject *tmp_globals_name_16;
        PyObject *tmp_locals_name_16;
        PyObject *tmp_fromlist_name_16;
        PyObject *tmp_level_name_16;
        tmp_name_name_16 = const_str_plain_socket;
        tmp_globals_name_16 = (PyObject *)moduledict_psutil$_common;
        tmp_locals_name_16 = Py_None;
        tmp_fromlist_name_16 = const_tuple_str_plain_AF_UNIX_tuple;
        tmp_level_name_16 = const_int_0;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 31;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_16, tmp_globals_name_16, tmp_locals_name_16, tmp_fromlist_name_16, tmp_level_name_16 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_AF_UNIX );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_UNIX, tmp_assign_source_21 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_641a34ae48b2aa2a3be9421131bc3092, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_641a34ae48b2aa2a3be9421131bc3092, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_22;
            tmp_assign_source_22 = Py_None;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_UNIX, tmp_assign_source_22 );
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 30;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_641a34ae48b2aa2a3be9421131bc3092->m_frame) frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_4;
        branch_end_2:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_3;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( psutil$_common );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 35;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version_info );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = const_tuple_int_pos_3_int_pos_4_tuple;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_name_name_17;
            PyObject *tmp_globals_name_17;
            PyObject *tmp_locals_name_17;
            PyObject *tmp_fromlist_name_17;
            PyObject *tmp_level_name_17;
            tmp_name_name_17 = const_str_plain_enum;
            tmp_globals_name_17 = (PyObject *)moduledict_psutil$_common;
            tmp_locals_name_17 = Py_None;
            tmp_fromlist_name_17 = Py_None;
            tmp_level_name_17 = const_int_0;
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 36;
            tmp_assign_source_23 = IMPORT_MODULE5( tmp_name_name_17, tmp_globals_name_17, tmp_locals_name_17, tmp_fromlist_name_17, tmp_level_name_17 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum, tmp_assign_source_23 );
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = Py_None;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum, tmp_assign_source_24 );
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_version_info );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = const_int_pos_3;
        tmp_assign_source_25 = RICH_COMPARE_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_PY3, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = LIST_COPY( const_list_1f83fcd5f40aa6957d8a184c26f07a11_list );
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_5;
        tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_name );
        if ( tmp_compexpr_left_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_5 = const_str_plain_posix;
        tmp_assign_source_27 = RICH_COMPARE_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        Py_DECREF( tmp_compexpr_left_5 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_POSIX, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_6;
        tmp_compexpr_left_6 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
        if ( tmp_compexpr_left_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_6 = const_str_plain_nt;
        tmp_assign_source_28 = RICH_COMPARE_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        Py_DECREF( tmp_compexpr_left_6 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_WINDOWS, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 78;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_7;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_platform );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;

            goto frame_exception_exit_1;
        }
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 78;
        tmp_assign_source_29 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_linux_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_LINUX, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_8;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_platform );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 79;
        tmp_assign_source_30 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_darwin_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_MACOS, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_MACOS );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MACOS );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_assign_source_31 = tmp_mvar_value_9;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_OSX, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_10;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_platform );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto frame_exception_exit_1;
        }
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 81;
        tmp_assign_source_32 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_freebsd_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_FREEBSD, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;

            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_11;
        tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_platform );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 82;
        tmp_assign_source_33 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_openbsd_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_OPENBSD, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;

            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_12;
        tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_platform );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 83;
        tmp_assign_source_34 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_netbsd_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_NETBSD, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_mvar_value_13;
        int tmp_or_left_truth_2;
        PyObject *tmp_or_left_value_2;
        PyObject *tmp_or_right_value_2;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_FREEBSD );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FREEBSD );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FREEBSD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }

        tmp_or_left_value_1 = tmp_mvar_value_13;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_OPENBSD );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OPENBSD );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OPENBSD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }

        tmp_or_left_value_2 = tmp_mvar_value_14;
        tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
        if ( tmp_or_left_truth_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_2 == 1 )
        {
            goto or_left_2;
        }
        else
        {
            goto or_right_2;
        }
        or_right_2:;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_NETBSD );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NETBSD );
        }

        CHECK_OBJECT( tmp_mvar_value_15 );
        tmp_or_right_value_2 = tmp_mvar_value_15;
        tmp_or_right_value_1 = tmp_or_right_value_2;
        goto or_end_2;
        or_left_2:;
        tmp_or_right_value_1 = tmp_or_left_value_2;
        or_end_2:;
        tmp_assign_source_35 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_assign_source_35 = tmp_or_left_value_1;
        or_end_1:;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_BSD, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_16;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_16;
        tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_platform );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 85;
        tmp_assign_source_36 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_tuple_str_plain_sunos_str_plain_solaris_tuple_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SUNOS, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_17;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_17;
        tmp_called_instance_7 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_platform );
        if ( tmp_called_instance_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 86;
        tmp_assign_source_37 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_aix_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_7 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AIX, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        tmp_assign_source_38 = const_str_plain_running;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_RUNNING, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        tmp_assign_source_39 = const_str_plain_sleeping;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_SLEEPING, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        tmp_assign_source_40 = const_str_digest_ad233afd4f5eda5adddba312e8fe0a80;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_DISK_SLEEP, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = const_str_plain_stopped;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_STOPPED, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = const_str_digest_8c8c0124d3f75932fd5693c97f1e2e27;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_TRACING_STOP, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        tmp_assign_source_43 = const_str_plain_zombie;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_ZOMBIE, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        tmp_assign_source_44 = const_str_plain_dead;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_DEAD, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        tmp_assign_source_45 = const_str_digest_3cd9631d27a7d42f4e2a7520ff729c49;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_WAKE_KILL, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        tmp_assign_source_46 = const_str_plain_waking;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_WAKING, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        tmp_assign_source_47 = const_str_plain_idle;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_IDLE, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        tmp_assign_source_48 = const_str_plain_locked;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_LOCKED, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        tmp_assign_source_49 = const_str_plain_waiting;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_WAITING, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        tmp_assign_source_50 = const_str_plain_suspended;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_SUSPENDED, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        tmp_assign_source_51 = const_str_plain_parked;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_STATUS_PARKED, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        tmp_assign_source_52 = const_str_plain_ESTABLISHED;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_ESTABLISHED, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        tmp_assign_source_53 = const_str_plain_SYN_SENT;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_SYN_SENT, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        tmp_assign_source_54 = const_str_plain_SYN_RECV;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_SYN_RECV, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        tmp_assign_source_55 = const_str_plain_FIN_WAIT1;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_FIN_WAIT1, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = const_str_plain_FIN_WAIT2;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_FIN_WAIT2, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        tmp_assign_source_57 = const_str_plain_TIME_WAIT;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_TIME_WAIT, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        tmp_assign_source_58 = const_str_plain_CLOSE;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_CLOSE, tmp_assign_source_58 );
    }
    {
        PyObject *tmp_assign_source_59;
        tmp_assign_source_59 = const_str_plain_CLOSE_WAIT;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_CLOSE_WAIT, tmp_assign_source_59 );
    }
    {
        PyObject *tmp_assign_source_60;
        tmp_assign_source_60 = const_str_plain_LAST_ACK;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_LAST_ACK, tmp_assign_source_60 );
    }
    {
        PyObject *tmp_assign_source_61;
        tmp_assign_source_61 = const_str_plain_LISTEN;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_LISTEN, tmp_assign_source_61 );
    }
    {
        PyObject *tmp_assign_source_62;
        tmp_assign_source_62 = const_str_plain_CLOSING;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_CLOSING, tmp_assign_source_62 );
    }
    {
        PyObject *tmp_assign_source_63;
        tmp_assign_source_63 = const_str_plain_NONE;
        UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_CONN_NONE, tmp_assign_source_63 );
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        PyObject *tmp_mvar_value_18;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_enum );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "enum" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;

            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_7 = tmp_mvar_value_18;
        tmp_compexpr_right_7 = Py_None;
        tmp_condition_result_4 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_64;
            tmp_assign_source_64 = const_int_pos_2;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_NIC_DUPLEX_FULL, tmp_assign_source_64 );
        }
        {
            PyObject *tmp_assign_source_65;
            tmp_assign_source_65 = const_int_pos_1;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_NIC_DUPLEX_HALF, tmp_assign_source_65 );
        }
        {
            PyObject *tmp_assign_source_66;
            tmp_assign_source_66 = const_int_0;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_NIC_DUPLEX_UNKNOWN, tmp_assign_source_66 );
        }
        goto branch_end_4;
        branch_no_4:;
        // Tried code:
        {
            PyObject *tmp_assign_source_67;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_19;
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum );

            if (unlikely( tmp_mvar_value_19 == NULL ))
            {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_enum );
            }

            if ( tmp_mvar_value_19 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "enum" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 130;

                goto try_except_handler_5;
            }

            tmp_source_name_12 = tmp_mvar_value_19;
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_IntEnum );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            tmp_assign_source_67 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_67, 0, tmp_tuple_element_1 );
            assert( tmp_class_creation_1__bases_orig == NULL );
            tmp_class_creation_1__bases_orig = tmp_assign_source_67;
        }
        {
            PyObject *tmp_assign_source_68;
            PyObject *tmp_dircall_arg1_1;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
            Py_INCREF( tmp_dircall_arg1_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
                tmp_assign_source_68 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_68 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__bases == NULL );
            tmp_class_creation_1__bases = tmp_assign_source_68;
        }
        {
            PyObject *tmp_assign_source_69;
            tmp_assign_source_69 = PyDict_New();
            assert( tmp_class_creation_1__class_decl_dict == NULL );
            tmp_class_creation_1__class_decl_dict = tmp_assign_source_69;
        }
        {
            PyObject *tmp_assign_source_70;
            PyObject *tmp_metaclass_name_1;
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_key_name_1;
            PyObject *tmp_dict_name_1;
            PyObject *tmp_dict_name_2;
            PyObject *tmp_key_name_2;
            nuitka_bool tmp_condition_result_6;
            int tmp_truth_name_1;
            PyObject *tmp_type_arg_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_bases_name_1;
            tmp_key_name_1 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
            tmp_key_name_2 = const_str_plain_metaclass;
            tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
            if ( tmp_metaclass_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            goto condexpr_end_1;
            condexpr_false_1:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            tmp_condition_result_6 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_2;
            }
            else
            {
                goto condexpr_false_2;
            }
            condexpr_true_2:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_subscribed_name_2 = tmp_class_creation_1__bases;
            tmp_subscript_name_2 = const_int_0;
            tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
            if ( tmp_type_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
            Py_DECREF( tmp_type_arg_1 );
            if ( tmp_metaclass_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            goto condexpr_end_2;
            condexpr_false_2:;
            tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_1 );
            condexpr_end_2:;
            condexpr_end_1:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_bases_name_1 = tmp_class_creation_1__bases;
            tmp_assign_source_70 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
            Py_DECREF( tmp_metaclass_name_1 );
            if ( tmp_assign_source_70 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__metaclass == NULL );
            tmp_class_creation_1__metaclass = tmp_assign_source_70;
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_key_name_3;
            PyObject *tmp_dict_name_3;
            tmp_key_name_3 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_5;
            }
            branch_no_5:;
        }
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_source_name_13;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_13 = tmp_class_creation_1__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_13, const_str_plain___prepare__ );
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assign_source_71;
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_14;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_kw_name_1;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_source_name_14 = tmp_class_creation_1__metaclass;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain___prepare__ );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 130;

                    goto try_except_handler_5;
                }
                tmp_tuple_element_2 = const_str_plain_NicDuplex;
                tmp_args_name_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_tuple_element_2 = tmp_class_creation_1__bases;
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
                frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 130;
                tmp_assign_source_71 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_name_1 );
                if ( tmp_assign_source_71 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 130;

                    goto try_except_handler_5;
                }
                assert( tmp_class_creation_1__prepared == NULL );
                tmp_class_creation_1__prepared = tmp_assign_source_71;
            }
            {
                nuitka_bool tmp_condition_result_9;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_source_name_15;
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_source_name_15 = tmp_class_creation_1__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___getitem__ );
                tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 130;

                    goto try_except_handler_5;
                }
                tmp_condition_result_9 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_raise_value_1;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_getattr_target_1;
                    PyObject *tmp_getattr_attr_1;
                    PyObject *tmp_getattr_default_1;
                    PyObject *tmp_source_name_16;
                    PyObject *tmp_type_arg_2;
                    tmp_raise_type_1 = PyExc_TypeError;
                    tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                    tmp_getattr_attr_1 = const_str_plain___name__;
                    tmp_getattr_default_1 = const_str_angle_metaclass;
                    tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 130;

                        goto try_except_handler_5;
                    }
                    tmp_right_name_1 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_type_arg_2 = tmp_class_creation_1__prepared;
                    tmp_source_name_16 = BUILTIN_TYPE1( tmp_type_arg_2 );
                    assert( !(tmp_source_name_16 == NULL) );
                    tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_16 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_1 );

                        exception_lineno = 130;

                        goto try_except_handler_5;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                    tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_raise_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 130;

                        goto try_except_handler_5;
                    }
                    exception_type = tmp_raise_type_1;
                    Py_INCREF( tmp_raise_type_1 );
                    exception_value = tmp_raise_value_1;
                    exception_lineno = 130;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                    goto try_except_handler_5;
                }
                branch_no_7:;
            }
            goto branch_end_6;
            branch_no_6:;
            {
                PyObject *tmp_assign_source_72;
                tmp_assign_source_72 = PyDict_New();
                assert( tmp_class_creation_1__prepared == NULL );
                tmp_class_creation_1__prepared = tmp_assign_source_72;
            }
            branch_end_6:;
        }
        {
            PyObject *tmp_assign_source_73;
            {
                PyObject *tmp_set_locals_1;
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_set_locals_1 = tmp_class_creation_1__prepared;
                locals_psutil$_common_130 = tmp_set_locals_1;
                Py_INCREF( tmp_set_locals_1 );
            }
            // Tried code:
            // Tried code:
            tmp_dictset_value = const_str_digest_c21138c8d575fc0b2b6ff4782a202f72;
            tmp_res = PyObject_SetItem( locals_psutil$_common_130, const_str_plain___module__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_7;
            }
            tmp_dictset_value = const_str_plain_NicDuplex;
            tmp_res = PyObject_SetItem( locals_psutil$_common_130, const_str_plain___qualname__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;

                goto try_except_handler_7;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_eb427e220e252bc7ee7380d6c9a0dd4c_2, codeobj_eb427e220e252bc7ee7380d6c9a0dd4c, module_psutil$_common, sizeof(void *) );
            frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 = cache_frame_eb427e220e252bc7ee7380d6c9a0dd4c_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 ) == 2 ); // Frame stack

            // Framed code:
            tmp_dictset_value = const_int_pos_2;
            tmp_res = PyObject_SetItem( locals_psutil$_common_130, const_str_plain_NIC_DUPLEX_FULL, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dictset_value = const_int_pos_1;
            tmp_res = PyObject_SetItem( locals_psutil$_common_130, const_str_plain_NIC_DUPLEX_HALF, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 132;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dictset_value = const_int_0;
            tmp_res = PyObject_SetItem( locals_psutil$_common_130, const_str_plain_NIC_DUPLEX_UNKNOWN, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

#if 0
            RESTORE_FRAME_EXCEPTION( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_eb427e220e252bc7ee7380d6c9a0dd4c_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_eb427e220e252bc7ee7380d6c9a0dd4c_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_eb427e220e252bc7ee7380d6c9a0dd4c_2,
                type_description_2,
                outline_0_var___class__
            );


            // Release cached frame.
            if ( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 == cache_frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 )
            {
                Py_DECREF( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 );
            }
            cache_frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 = NULL;

            assertFrameObject( frame_eb427e220e252bc7ee7380d6c9a0dd4c_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;

            goto try_except_handler_7;
            skip_nested_handling_1:;
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_compexpr_left_8 = tmp_class_creation_1__bases;
                CHECK_OBJECT( tmp_class_creation_1__bases_orig );
                tmp_compexpr_right_8 = tmp_class_creation_1__bases_orig;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 130;

                    goto try_except_handler_7;
                }
                tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                CHECK_OBJECT( tmp_class_creation_1__bases_orig );
                tmp_dictset_value = tmp_class_creation_1__bases_orig;
                tmp_res = PyObject_SetItem( locals_psutil$_common_130, const_str_plain___orig_bases__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 130;

                    goto try_except_handler_7;
                }
                branch_no_8:;
            }
            {
                PyObject *tmp_assign_source_74;
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_name_2;
                PyObject *tmp_tuple_element_4;
                PyObject *tmp_kw_name_2;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_called_name_2 = tmp_class_creation_1__metaclass;
                tmp_tuple_element_4 = const_str_plain_NicDuplex;
                tmp_args_name_2 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_tuple_element_4 = tmp_class_creation_1__bases;
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
                tmp_tuple_element_4 = locals_psutil$_common_130;
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
                frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 130;
                tmp_assign_source_74 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
                Py_DECREF( tmp_args_name_2 );
                if ( tmp_assign_source_74 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 130;

                    goto try_except_handler_7;
                }
                assert( outline_0_var___class__ == NULL );
                outline_0_var___class__ = tmp_assign_source_74;
            }
            CHECK_OBJECT( outline_0_var___class__ );
            tmp_assign_source_73 = outline_0_var___class__;
            Py_INCREF( tmp_assign_source_73 );
            goto try_return_handler_7;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( psutil$_common );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_7:;
            Py_DECREF( locals_psutil$_common_130 );
            locals_psutil$_common_130 = NULL;
            goto try_return_handler_6;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_DECREF( locals_psutil$_common_130 );
            locals_psutil$_common_130 = NULL;
            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto try_except_handler_6;
            // End of try:
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( psutil$_common );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_6:;
            CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
            Py_DECREF( outline_0_var___class__ );
            outline_0_var___class__ = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_6:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( psutil$_common );
            return MOD_RETURN_VALUE( NULL );
            outline_exception_1:;
            exception_lineno = 130;
            goto try_except_handler_5;
            outline_result_1:;
            UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_NicDuplex, tmp_assign_source_73 );
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_class_creation_1__bases_orig );
        tmp_class_creation_1__bases_orig = NULL;

        Py_XDECREF( tmp_class_creation_1__bases );
        tmp_class_creation_1__bases = NULL;

        Py_XDECREF( tmp_class_creation_1__class_decl_dict );
        tmp_class_creation_1__class_decl_dict = NULL;

        Py_XDECREF( tmp_class_creation_1__metaclass );
        tmp_class_creation_1__metaclass = NULL;

        Py_XDECREF( tmp_class_creation_1__prepared );
        tmp_class_creation_1__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto frame_exception_exit_1;
        // End of try:
        try_end_5:;
        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
        Py_DECREF( tmp_class_creation_1__bases_orig );
        tmp_class_creation_1__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
        Py_DECREF( tmp_class_creation_1__bases );
        tmp_class_creation_1__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
        Py_DECREF( tmp_class_creation_1__class_decl_dict );
        tmp_class_creation_1__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
        Py_DECREF( tmp_class_creation_1__metaclass );
        tmp_class_creation_1__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
        Py_DECREF( tmp_class_creation_1__prepared );
        tmp_class_creation_1__prepared = NULL;

        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_17;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_18;
            PyObject *tmp_mvar_value_20;
            tmp_source_name_17 = (PyObject *)moduledict_psutil$_common;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_update );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;

                goto frame_exception_exit_1;
            }
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_NicDuplex );

            if (unlikely( tmp_mvar_value_20 == NULL ))
            {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NicDuplex );
            }

            if ( tmp_mvar_value_20 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NicDuplex" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 135;

                goto frame_exception_exit_1;
            }

            tmp_source_name_18 = tmp_mvar_value_20;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain___members__ );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 135;

                goto frame_exception_exit_1;
            }
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 135;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_end_4:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_compexpr_left_9;
        PyObject *tmp_compexpr_right_9;
        PyObject *tmp_mvar_value_21;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_enum );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "enum" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;

            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_9 = tmp_mvar_value_21;
        tmp_compexpr_right_9 = Py_None;
        tmp_condition_result_11 = ( tmp_compexpr_left_9 == tmp_compexpr_right_9 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_assign_source_75;
            tmp_assign_source_75 = const_int_neg_1;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_POWER_TIME_UNKNOWN, tmp_assign_source_75 );
        }
        {
            PyObject *tmp_assign_source_76;
            tmp_assign_source_76 = const_int_neg_2;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_POWER_TIME_UNLIMITED, tmp_assign_source_76 );
        }
        goto branch_end_9;
        branch_no_9:;
        // Tried code:
        {
            PyObject *tmp_assign_source_77;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_source_name_19;
            PyObject *tmp_mvar_value_22;
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_enum );

            if (unlikely( tmp_mvar_value_22 == NULL ))
            {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_enum );
            }

            if ( tmp_mvar_value_22 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "enum" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 142;

                goto try_except_handler_8;
            }

            tmp_source_name_19 = tmp_mvar_value_22;
            tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_IntEnum );
            if ( tmp_tuple_element_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            tmp_assign_source_77 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_assign_source_77, 0, tmp_tuple_element_5 );
            assert( tmp_class_creation_2__bases_orig == NULL );
            tmp_class_creation_2__bases_orig = tmp_assign_source_77;
        }
        {
            PyObject *tmp_assign_source_78;
            PyObject *tmp_dircall_arg1_2;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
            Py_INCREF( tmp_dircall_arg1_2 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
                tmp_assign_source_78 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_78 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            assert( tmp_class_creation_2__bases == NULL );
            tmp_class_creation_2__bases = tmp_assign_source_78;
        }
        {
            PyObject *tmp_assign_source_79;
            tmp_assign_source_79 = PyDict_New();
            assert( tmp_class_creation_2__class_decl_dict == NULL );
            tmp_class_creation_2__class_decl_dict = tmp_assign_source_79;
        }
        {
            PyObject *tmp_assign_source_80;
            PyObject *tmp_metaclass_name_2;
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_key_name_4;
            PyObject *tmp_dict_name_4;
            PyObject *tmp_dict_name_5;
            PyObject *tmp_key_name_5;
            nuitka_bool tmp_condition_result_13;
            int tmp_truth_name_2;
            PyObject *tmp_type_arg_3;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_bases_name_2;
            tmp_key_name_4 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
            tmp_key_name_5 = const_str_plain_metaclass;
            tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
            if ( tmp_metaclass_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            tmp_condition_result_13 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_subscribed_name_3 = tmp_class_creation_2__bases;
            tmp_subscript_name_3 = const_int_0;
            tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
            if ( tmp_type_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
            Py_DECREF( tmp_type_arg_3 );
            if ( tmp_metaclass_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            goto condexpr_end_4;
            condexpr_false_4:;
            tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_2 );
            condexpr_end_4:;
            condexpr_end_3:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_bases_name_2 = tmp_class_creation_2__bases;
            tmp_assign_source_80 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
            Py_DECREF( tmp_metaclass_name_2 );
            if ( tmp_assign_source_80 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            assert( tmp_class_creation_2__metaclass == NULL );
            tmp_class_creation_2__metaclass = tmp_assign_source_80;
        }
        {
            nuitka_bool tmp_condition_result_14;
            PyObject *tmp_key_name_6;
            PyObject *tmp_dict_name_6;
            tmp_key_name_6 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_10;
            }
            else
            {
                goto branch_no_10;
            }
            branch_yes_10:;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_8;
            }
            branch_no_10:;
        }
        {
            nuitka_bool tmp_condition_result_15;
            PyObject *tmp_source_name_20;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_20 = tmp_class_creation_2__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_20, const_str_plain___prepare__ );
            tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_assign_source_81;
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_21;
                PyObject *tmp_args_name_3;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_kw_name_3;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_source_name_21 = tmp_class_creation_2__metaclass;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain___prepare__ );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_8;
                }
                tmp_tuple_element_6 = const_str_plain_BatteryTime;
                tmp_args_name_3 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_6 );
                PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_tuple_element_6 = tmp_class_creation_2__bases;
                Py_INCREF( tmp_tuple_element_6 );
                PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
                tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
                frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 142;
                tmp_assign_source_81 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_3 );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_name_3 );
                if ( tmp_assign_source_81 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_8;
                }
                assert( tmp_class_creation_2__prepared == NULL );
                tmp_class_creation_2__prepared = tmp_assign_source_81;
            }
            {
                nuitka_bool tmp_condition_result_16;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_source_name_22;
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_source_name_22 = tmp_class_creation_2__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_22, const_str_plain___getitem__ );
                tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_8;
                }
                tmp_condition_result_16 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_12;
                }
                else
                {
                    goto branch_no_12;
                }
                branch_yes_12:;
                {
                    PyObject *tmp_raise_type_2;
                    PyObject *tmp_raise_value_2;
                    PyObject *tmp_left_name_2;
                    PyObject *tmp_right_name_2;
                    PyObject *tmp_tuple_element_7;
                    PyObject *tmp_getattr_target_2;
                    PyObject *tmp_getattr_attr_2;
                    PyObject *tmp_getattr_default_2;
                    PyObject *tmp_source_name_23;
                    PyObject *tmp_type_arg_4;
                    tmp_raise_type_2 = PyExc_TypeError;
                    tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_class_creation_2__metaclass );
                    tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                    tmp_getattr_attr_2 = const_str_plain___name__;
                    tmp_getattr_default_2 = const_str_angle_metaclass;
                    tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                    if ( tmp_tuple_element_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 142;

                        goto try_except_handler_8;
                    }
                    tmp_right_name_2 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                    CHECK_OBJECT( tmp_class_creation_2__prepared );
                    tmp_type_arg_4 = tmp_class_creation_2__prepared;
                    tmp_source_name_23 = BUILTIN_TYPE1( tmp_type_arg_4 );
                    assert( !(tmp_source_name_23 == NULL) );
                    tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_23 );
                    if ( tmp_tuple_element_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_2 );

                        exception_lineno = 142;

                        goto try_except_handler_8;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                    tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                    Py_DECREF( tmp_right_name_2 );
                    if ( tmp_raise_value_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 142;

                        goto try_except_handler_8;
                    }
                    exception_type = tmp_raise_type_2;
                    Py_INCREF( tmp_raise_type_2 );
                    exception_value = tmp_raise_value_2;
                    exception_lineno = 142;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                    goto try_except_handler_8;
                }
                branch_no_12:;
            }
            goto branch_end_11;
            branch_no_11:;
            {
                PyObject *tmp_assign_source_82;
                tmp_assign_source_82 = PyDict_New();
                assert( tmp_class_creation_2__prepared == NULL );
                tmp_class_creation_2__prepared = tmp_assign_source_82;
            }
            branch_end_11:;
        }
        {
            PyObject *tmp_assign_source_83;
            {
                PyObject *tmp_set_locals_2;
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_set_locals_2 = tmp_class_creation_2__prepared;
                locals_psutil$_common_142 = tmp_set_locals_2;
                Py_INCREF( tmp_set_locals_2 );
            }
            // Tried code:
            // Tried code:
            tmp_dictset_value = const_str_digest_c21138c8d575fc0b2b6ff4782a202f72;
            tmp_res = PyObject_SetItem( locals_psutil$_common_142, const_str_plain___module__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_10;
            }
            tmp_dictset_value = const_str_plain_BatteryTime;
            tmp_res = PyObject_SetItem( locals_psutil$_common_142, const_str_plain___qualname__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_10;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_3eb634e8d4080b8c4c77a2731a7d5b07_3, codeobj_3eb634e8d4080b8c4c77a2731a7d5b07, module_psutil$_common, sizeof(void *) );
            frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 = cache_frame_3eb634e8d4080b8c4c77a2731a7d5b07_3;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 ) == 2 ); // Frame stack

            // Framed code:
            tmp_dictset_value = const_int_neg_1;
            tmp_res = PyObject_SetItem( locals_psutil$_common_142, const_str_plain_POWER_TIME_UNKNOWN, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dictset_value = const_int_neg_2;
            tmp_res = PyObject_SetItem( locals_psutil$_common_142, const_str_plain_POWER_TIME_UNLIMITED, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

#if 0
            RESTORE_FRAME_EXCEPTION( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_2;

            frame_exception_exit_3:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_3eb634e8d4080b8c4c77a2731a7d5b07_3->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_3eb634e8d4080b8c4c77a2731a7d5b07_3, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_3eb634e8d4080b8c4c77a2731a7d5b07_3,
                type_description_2,
                outline_1_var___class__
            );


            // Release cached frame.
            if ( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 == cache_frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 )
            {
                Py_DECREF( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 );
            }
            cache_frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 = NULL;

            assertFrameObject( frame_3eb634e8d4080b8c4c77a2731a7d5b07_3 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_2;

            frame_no_exception_2:;
            goto skip_nested_handling_2;
            nested_frame_exit_2:;

            goto try_except_handler_10;
            skip_nested_handling_2:;
            {
                nuitka_bool tmp_condition_result_17;
                PyObject *tmp_compexpr_left_10;
                PyObject *tmp_compexpr_right_10;
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_compexpr_left_10 = tmp_class_creation_2__bases;
                CHECK_OBJECT( tmp_class_creation_2__bases_orig );
                tmp_compexpr_right_10 = tmp_class_creation_2__bases_orig;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_10;
                }
                tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_13;
                }
                else
                {
                    goto branch_no_13;
                }
                branch_yes_13:;
                CHECK_OBJECT( tmp_class_creation_2__bases_orig );
                tmp_dictset_value = tmp_class_creation_2__bases_orig;
                tmp_res = PyObject_SetItem( locals_psutil$_common_142, const_str_plain___orig_bases__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_10;
                }
                branch_no_13:;
            }
            {
                PyObject *tmp_assign_source_84;
                PyObject *tmp_called_name_5;
                PyObject *tmp_args_name_4;
                PyObject *tmp_tuple_element_8;
                PyObject *tmp_kw_name_4;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_called_name_5 = tmp_class_creation_2__metaclass;
                tmp_tuple_element_8 = const_str_plain_BatteryTime;
                tmp_args_name_4 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_tuple_element_8 = tmp_class_creation_2__bases;
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
                tmp_tuple_element_8 = locals_psutil$_common_142;
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
                CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
                tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
                frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 142;
                tmp_assign_source_84 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_4, tmp_kw_name_4 );
                Py_DECREF( tmp_args_name_4 );
                if ( tmp_assign_source_84 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_10;
                }
                assert( outline_1_var___class__ == NULL );
                outline_1_var___class__ = tmp_assign_source_84;
            }
            CHECK_OBJECT( outline_1_var___class__ );
            tmp_assign_source_83 = outline_1_var___class__;
            Py_INCREF( tmp_assign_source_83 );
            goto try_return_handler_10;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( psutil$_common );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_10:;
            Py_DECREF( locals_psutil$_common_142 );
            locals_psutil$_common_142 = NULL;
            goto try_return_handler_9;
            // Exception handler code:
            try_except_handler_10:;
            exception_keeper_type_8 = exception_type;
            exception_keeper_value_8 = exception_value;
            exception_keeper_tb_8 = exception_tb;
            exception_keeper_lineno_8 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_DECREF( locals_psutil$_common_142 );
            locals_psutil$_common_142 = NULL;
            // Re-raise.
            exception_type = exception_keeper_type_8;
            exception_value = exception_keeper_value_8;
            exception_tb = exception_keeper_tb_8;
            exception_lineno = exception_keeper_lineno_8;

            goto try_except_handler_9;
            // End of try:
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( psutil$_common );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_9:;
            CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
            Py_DECREF( outline_1_var___class__ );
            outline_1_var___class__ = NULL;

            goto outline_result_2;
            // Exception handler code:
            try_except_handler_9:;
            exception_keeper_type_9 = exception_type;
            exception_keeper_value_9 = exception_value;
            exception_keeper_tb_9 = exception_tb;
            exception_keeper_lineno_9 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Re-raise.
            exception_type = exception_keeper_type_9;
            exception_value = exception_keeper_value_9;
            exception_tb = exception_keeper_tb_9;
            exception_lineno = exception_keeper_lineno_9;

            goto outline_exception_2;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( psutil$_common );
            return MOD_RETURN_VALUE( NULL );
            outline_exception_2:;
            exception_lineno = 142;
            goto try_except_handler_8;
            outline_result_2:;
            UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_BatteryTime, tmp_assign_source_83 );
        }
        goto try_end_6;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_class_creation_2__bases_orig );
        tmp_class_creation_2__bases_orig = NULL;

        Py_XDECREF( tmp_class_creation_2__bases );
        tmp_class_creation_2__bases = NULL;

        Py_XDECREF( tmp_class_creation_2__class_decl_dict );
        tmp_class_creation_2__class_decl_dict = NULL;

        Py_XDECREF( tmp_class_creation_2__metaclass );
        tmp_class_creation_2__metaclass = NULL;

        Py_XDECREF( tmp_class_creation_2__prepared );
        tmp_class_creation_2__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto frame_exception_exit_1;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
        Py_DECREF( tmp_class_creation_2__bases_orig );
        tmp_class_creation_2__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
        Py_DECREF( tmp_class_creation_2__bases );
        tmp_class_creation_2__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
        Py_DECREF( tmp_class_creation_2__class_decl_dict );
        tmp_class_creation_2__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
        Py_DECREF( tmp_class_creation_2__metaclass );
        tmp_class_creation_2__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
        Py_DECREF( tmp_class_creation_2__prepared );
        tmp_class_creation_2__prepared = NULL;

        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_source_name_24;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_25;
            PyObject *tmp_mvar_value_23;
            tmp_source_name_24 = (PyObject *)moduledict_psutil$_common;
            tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_update );
            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 146;

                goto frame_exception_exit_1;
            }
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_BatteryTime );

            if (unlikely( tmp_mvar_value_23 == NULL ))
            {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BatteryTime );
            }

            if ( tmp_mvar_value_23 == NULL )
            {
                Py_DECREF( tmp_called_name_6 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BatteryTime" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 146;

                goto frame_exception_exit_1;
            }

            tmp_source_name_25 = tmp_mvar_value_23;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain___members__ );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_6 );

                exception_lineno = 146;

                goto frame_exception_exit_1;
            }
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 146;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 146;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_end_9:;
    }
    {
        PyObject *tmp_assign_source_85;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_mvar_value_24;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_8 = tmp_mvar_value_24;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 150;
        tmp_assign_source_85 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_getfilesystemencoding );
        if ( tmp_assign_source_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING, tmp_assign_source_85 );
    }
    {
        nuitka_bool tmp_condition_result_18;
        PyObject *tmp_operand_name_3;
        PyObject *tmp_mvar_value_25;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 151;

            goto frame_exception_exit_1;
        }

        tmp_operand_name_3 = tmp_mvar_value_25;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_18 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_assign_source_86;
            tmp_assign_source_86 = const_str_plain_replace;
            UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING_ERRS, tmp_assign_source_86 );
        }
        goto branch_end_14;
        branch_no_14:;
        // Tried code:
        {
            PyObject *tmp_assign_source_87;
            PyObject *tmp_called_instance_9;
            PyObject *tmp_mvar_value_26;
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_26 == NULL ))
            {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_26 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 155;

                goto try_except_handler_11;
            }

            tmp_called_instance_9 = tmp_mvar_value_26;
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 155;
            tmp_assign_source_87 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_getfilesystemencodeerrors );
            if ( tmp_assign_source_87 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;

                goto try_except_handler_11;
            }
            UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING_ERRS, tmp_assign_source_87 );
        }
        goto try_end_7;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_3 );
        exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_3 );
        exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_3 );

        if ( exception_keeper_tb_11 == NULL )
        {
            exception_keeper_tb_11 = MAKE_TRACEBACK( frame_641a34ae48b2aa2a3be9421131bc3092, exception_keeper_lineno_11 );
        }
        else if ( exception_keeper_lineno_11 != 0 )
        {
            exception_keeper_tb_11 = ADD_TRACEBACK( exception_keeper_tb_11, frame_641a34ae48b2aa2a3be9421131bc3092, exception_keeper_lineno_11 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_11, &exception_keeper_value_11, &exception_keeper_tb_11 );
        PyException_SetTraceback( exception_keeper_value_11, (PyObject *)exception_keeper_tb_11 );
        PUBLISH_EXCEPTION( &exception_keeper_type_11, &exception_keeper_value_11, &exception_keeper_tb_11 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            tmp_compexpr_left_11 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_11 = PyExc_AttributeError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_11, tmp_compexpr_right_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 156;

                goto try_except_handler_12;
            }
            tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_assign_source_88;
                nuitka_bool tmp_condition_result_20;
                PyObject *tmp_mvar_value_27;
                int tmp_truth_name_3;
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_POSIX );

                if (unlikely( tmp_mvar_value_27 == NULL ))
                {
                    tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_POSIX );
                }

                if ( tmp_mvar_value_27 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "POSIX" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 157;

                    goto try_except_handler_12;
                }

                tmp_truth_name_3 = CHECK_IF_TRUE( tmp_mvar_value_27 );
                if ( tmp_truth_name_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 157;

                    goto try_except_handler_12;
                }
                tmp_condition_result_20 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_5;
                }
                else
                {
                    goto condexpr_false_5;
                }
                condexpr_true_5:;
                tmp_assign_source_88 = const_str_plain_surrogateescape;
                goto condexpr_end_5;
                condexpr_false_5:;
                tmp_assign_source_88 = const_str_plain_replace;
                condexpr_end_5:;
                UPDATE_STRING_DICT0( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_ENCODING_ERRS, tmp_assign_source_88 );
            }
            goto branch_end_15;
            branch_no_15:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 154;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_641a34ae48b2aa2a3be9421131bc3092->m_frame) frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = exception_tb->tb_lineno;

            goto try_except_handler_12;
            branch_end_15:;
        }
        goto try_end_8;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto frame_exception_exit_1;
        // End of try:
        try_end_8:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
        goto try_end_7;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common );
        return MOD_RETURN_VALUE( NULL );
        // End of try:
        try_end_7:;
        branch_end_14:;
    }
    {
        PyObject *tmp_assign_source_89;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_28 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_28;
        tmp_call_arg_element_1 = const_str_plain_sswap;
        tmp_call_arg_element_2 = LIST_COPY( const_list_7269f1f5bbfaf7e849943ff948940288_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 167;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_89 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_89 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sswap, tmp_assign_source_89 );
    }
    {
        PyObject *tmp_assign_source_90;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_call_arg_element_3;
        PyObject *tmp_call_arg_element_4;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_29;
        tmp_call_arg_element_3 = const_str_plain_sdiskusage;
        tmp_call_arg_element_4 = LIST_COPY( const_list_e817dfda20ddb186bde2c54cef499933_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 170;
        {
            PyObject *call_args[] = { tmp_call_arg_element_3, tmp_call_arg_element_4 };
            tmp_assign_source_90 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_call_arg_element_4 );
        if ( tmp_assign_source_90 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sdiskusage, tmp_assign_source_90 );
    }
    {
        PyObject *tmp_assign_source_91;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_call_arg_element_5;
        PyObject *tmp_call_arg_element_6;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_30 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_30;
        tmp_call_arg_element_5 = const_str_plain_sdiskio;
        tmp_call_arg_element_6 = LIST_COPY( const_list_04200af437d3ca9afb64727d4cd1a1b6_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 172;
        {
            PyObject *call_args[] = { tmp_call_arg_element_5, tmp_call_arg_element_6 };
            tmp_assign_source_91 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_call_arg_element_6 );
        if ( tmp_assign_source_91 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sdiskio, tmp_assign_source_91 );
    }
    {
        PyObject *tmp_assign_source_92;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_call_arg_element_7;
        PyObject *tmp_call_arg_element_8;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;

            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_31;
        tmp_call_arg_element_7 = const_str_plain_sdiskpart;
        tmp_call_arg_element_8 = LIST_COPY( const_list_4ce112f110e3f44d360b8e23a82a18a9_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 176;
        {
            PyObject *call_args[] = { tmp_call_arg_element_7, tmp_call_arg_element_8 };
            tmp_assign_source_92 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_call_arg_element_8 );
        if ( tmp_assign_source_92 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sdiskpart, tmp_assign_source_92 );
    }
    {
        PyObject *tmp_assign_source_93;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_call_arg_element_9;
        PyObject *tmp_call_arg_element_10;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_32 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 178;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_32;
        tmp_call_arg_element_9 = const_str_plain_snetio;
        tmp_call_arg_element_10 = LIST_COPY( const_list_63a224d8fa5fb3e541f9068e1fa22ec4_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 178;
        {
            PyObject *call_args[] = { tmp_call_arg_element_9, tmp_call_arg_element_10 };
            tmp_assign_source_93 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_call_arg_element_10 );
        if ( tmp_assign_source_93 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_snetio, tmp_assign_source_93 );
    }
    {
        PyObject *tmp_assign_source_94;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_call_arg_element_11;
        PyObject *tmp_call_arg_element_12;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_33 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_33;
        tmp_call_arg_element_11 = const_str_plain_suser;
        tmp_call_arg_element_12 = LIST_COPY( const_list_e4eda79a105ca56e947928094e495dab_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 183;
        {
            PyObject *call_args[] = { tmp_call_arg_element_11, tmp_call_arg_element_12 };
            tmp_assign_source_94 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_call_arg_element_12 );
        if ( tmp_assign_source_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_suser, tmp_assign_source_94 );
    }
    {
        PyObject *tmp_assign_source_95;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_call_arg_element_13;
        PyObject *tmp_call_arg_element_14;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_34 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_34;
        tmp_call_arg_element_13 = const_str_plain_sconn;
        tmp_call_arg_element_14 = LIST_COPY( const_list_80a0510781d2d124eb0a6af3f3458443_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 185;
        {
            PyObject *call_args[] = { tmp_call_arg_element_13, tmp_call_arg_element_14 };
            tmp_assign_source_95 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_call_arg_element_14 );
        if ( tmp_assign_source_95 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sconn, tmp_assign_source_95 );
    }
    {
        PyObject *tmp_assign_source_96;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_call_arg_element_15;
        PyObject *tmp_call_arg_element_16;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_35;
        tmp_call_arg_element_15 = const_str_plain_snicaddr;
        tmp_call_arg_element_16 = LIST_COPY( const_list_ce761954676b292932e8dabc7e5b1c77_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 188;
        {
            PyObject *call_args[] = { tmp_call_arg_element_15, tmp_call_arg_element_16 };
            tmp_assign_source_96 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_call_arg_element_16 );
        if ( tmp_assign_source_96 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_snicaddr, tmp_assign_source_96 );
    }
    {
        PyObject *tmp_assign_source_97;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_call_arg_element_17;
        PyObject *tmp_call_arg_element_18;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_36 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_36;
        tmp_call_arg_element_17 = const_str_plain_snicstats;
        tmp_call_arg_element_18 = LIST_COPY( const_list_9db5dd841fb6eacf7ec042df99116523_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_call_arg_element_17, tmp_call_arg_element_18 };
            tmp_assign_source_97 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_call_arg_element_18 );
        if ( tmp_assign_source_97 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_snicstats, tmp_assign_source_97 );
    }
    {
        PyObject *tmp_assign_source_98;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_call_arg_element_19;
        PyObject *tmp_call_arg_element_20;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_37 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_37;
        tmp_call_arg_element_19 = const_str_plain_scpustats;
        tmp_call_arg_element_20 = LIST_COPY( const_list_66ba68b9593ee86a3b33193aff923a5b_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_call_arg_element_19, tmp_call_arg_element_20 };
            tmp_assign_source_98 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_call_arg_element_20 );
        if ( tmp_assign_source_98 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_scpustats, tmp_assign_source_98 );
    }
    {
        PyObject *tmp_assign_source_99;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_call_arg_element_21;
        PyObject *tmp_call_arg_element_22;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_38 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 196;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_38;
        tmp_call_arg_element_21 = const_str_plain_scpufreq;
        tmp_call_arg_element_22 = LIST_COPY( const_list_str_plain_current_str_plain_min_str_plain_max_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 196;
        {
            PyObject *call_args[] = { tmp_call_arg_element_21, tmp_call_arg_element_22 };
            tmp_assign_source_99 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_call_arg_element_22 );
        if ( tmp_assign_source_99 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_scpufreq, tmp_assign_source_99 );
    }
    {
        PyObject *tmp_assign_source_100;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_call_arg_element_23;
        PyObject *tmp_call_arg_element_24;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_39 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 198;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_39;
        tmp_call_arg_element_23 = const_str_plain_shwtemp;
        tmp_call_arg_element_24 = LIST_COPY( const_list_f504379bcc563b1d896d033185a6607f_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 198;
        {
            PyObject *call_args[] = { tmp_call_arg_element_23, tmp_call_arg_element_24 };
            tmp_assign_source_100 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_call_arg_element_24 );
        if ( tmp_assign_source_100 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_shwtemp, tmp_assign_source_100 );
    }
    {
        PyObject *tmp_assign_source_101;
        PyObject *tmp_called_name_19;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_call_arg_element_25;
        PyObject *tmp_call_arg_element_26;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_40 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 201;

            goto frame_exception_exit_1;
        }

        tmp_called_name_19 = tmp_mvar_value_40;
        tmp_call_arg_element_25 = const_str_plain_sbattery;
        tmp_call_arg_element_26 = LIST_COPY( const_list_str_plain_percent_str_plain_secsleft_str_plain_power_plugged_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 201;
        {
            PyObject *call_args[] = { tmp_call_arg_element_25, tmp_call_arg_element_26 };
            tmp_assign_source_101 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_call_arg_element_26 );
        if ( tmp_assign_source_101 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sbattery, tmp_assign_source_101 );
    }
    {
        PyObject *tmp_assign_source_102;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_call_arg_element_27;
        PyObject *tmp_call_arg_element_28;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_41 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 203;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_41;
        tmp_call_arg_element_27 = const_str_plain_sfan;
        tmp_call_arg_element_28 = LIST_COPY( const_list_str_plain_label_str_plain_current_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 203;
        {
            PyObject *call_args[] = { tmp_call_arg_element_27, tmp_call_arg_element_28 };
            tmp_assign_source_102 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_call_arg_element_28 );
        if ( tmp_assign_source_102 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sfan, tmp_assign_source_102 );
    }
    {
        PyObject *tmp_assign_source_103;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_call_arg_element_29;
        PyObject *tmp_call_arg_element_30;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_42 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 208;

            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_42;
        tmp_call_arg_element_29 = const_str_plain_pcputimes;
        tmp_call_arg_element_30 = LIST_COPY( const_list_c1e1ba8280b2052467c1c38da9f4c5e2_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 208;
        {
            PyObject *call_args[] = { tmp_call_arg_element_29, tmp_call_arg_element_30 };
            tmp_assign_source_103 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_21, call_args );
        }

        Py_DECREF( tmp_call_arg_element_30 );
        if ( tmp_assign_source_103 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_pcputimes, tmp_assign_source_103 );
    }
    {
        PyObject *tmp_assign_source_104;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_call_arg_element_31;
        PyObject *tmp_call_arg_element_32;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_43 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 211;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_43;
        tmp_call_arg_element_31 = const_str_plain_popenfile;
        tmp_call_arg_element_32 = LIST_COPY( const_list_str_plain_path_str_plain_fd_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 211;
        {
            PyObject *call_args[] = { tmp_call_arg_element_31, tmp_call_arg_element_32 };
            tmp_assign_source_104 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_call_arg_element_32 );
        if ( tmp_assign_source_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_popenfile, tmp_assign_source_104 );
    }
    {
        PyObject *tmp_assign_source_105;
        PyObject *tmp_called_name_23;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_call_arg_element_33;
        PyObject *tmp_call_arg_element_34;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_44 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 213;

            goto frame_exception_exit_1;
        }

        tmp_called_name_23 = tmp_mvar_value_44;
        tmp_call_arg_element_33 = const_str_plain_pthread;
        tmp_call_arg_element_34 = LIST_COPY( const_list_str_plain_id_str_plain_user_time_str_plain_system_time_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 213;
        {
            PyObject *call_args[] = { tmp_call_arg_element_33, tmp_call_arg_element_34 };
            tmp_assign_source_105 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_call_arg_element_34 );
        if ( tmp_assign_source_105 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_pthread, tmp_assign_source_105 );
    }
    {
        PyObject *tmp_assign_source_106;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_call_arg_element_35;
        PyObject *tmp_call_arg_element_36;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_45 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 215;

            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_45;
        tmp_call_arg_element_35 = const_str_plain_puids;
        tmp_call_arg_element_36 = LIST_COPY( const_list_str_plain_real_str_plain_effective_str_plain_saved_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 215;
        {
            PyObject *call_args[] = { tmp_call_arg_element_35, tmp_call_arg_element_36 };
            tmp_assign_source_106 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_call_arg_element_36 );
        if ( tmp_assign_source_106 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_puids, tmp_assign_source_106 );
    }
    {
        PyObject *tmp_assign_source_107;
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_call_arg_element_37;
        PyObject *tmp_call_arg_element_38;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_46 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 217;

            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_46;
        tmp_call_arg_element_37 = const_str_plain_pgids;
        tmp_call_arg_element_38 = LIST_COPY( const_list_str_plain_real_str_plain_effective_str_plain_saved_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 217;
        {
            PyObject *call_args[] = { tmp_call_arg_element_37, tmp_call_arg_element_38 };
            tmp_assign_source_107 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_call_arg_element_38 );
        if ( tmp_assign_source_107 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_pgids, tmp_assign_source_107 );
    }
    {
        PyObject *tmp_assign_source_108;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_call_arg_element_39;
        PyObject *tmp_call_arg_element_40;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_47 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 219;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_47;
        tmp_call_arg_element_39 = const_str_plain_pio;
        tmp_call_arg_element_40 = LIST_COPY( const_list_a5ad0ede6528da49c1442b81ef020e8e_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 219;
        {
            PyObject *call_args[] = { tmp_call_arg_element_39, tmp_call_arg_element_40 };
            tmp_assign_source_108 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_26, call_args );
        }

        Py_DECREF( tmp_call_arg_element_40 );
        if ( tmp_assign_source_108 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_pio, tmp_assign_source_108 );
    }
    {
        PyObject *tmp_assign_source_109;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_call_arg_element_41;
        PyObject *tmp_call_arg_element_42;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_48 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 222;

            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_48;
        tmp_call_arg_element_41 = const_str_plain_pionice;
        tmp_call_arg_element_42 = LIST_COPY( const_list_str_plain_ioclass_str_plain_value_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 222;
        {
            PyObject *call_args[] = { tmp_call_arg_element_41, tmp_call_arg_element_42 };
            tmp_assign_source_109 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_27, call_args );
        }

        Py_DECREF( tmp_call_arg_element_42 );
        if ( tmp_assign_source_109 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_pionice, tmp_assign_source_109 );
    }
    {
        PyObject *tmp_assign_source_110;
        PyObject *tmp_called_name_28;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_call_arg_element_43;
        PyObject *tmp_call_arg_element_44;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_49 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 224;

            goto frame_exception_exit_1;
        }

        tmp_called_name_28 = tmp_mvar_value_49;
        tmp_call_arg_element_43 = const_str_plain_pctxsw;
        tmp_call_arg_element_44 = LIST_COPY( const_list_str_plain_voluntary_str_plain_involuntary_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 224;
        {
            PyObject *call_args[] = { tmp_call_arg_element_43, tmp_call_arg_element_44 };
            tmp_assign_source_110 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_call_arg_element_44 );
        if ( tmp_assign_source_110 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_pctxsw, tmp_assign_source_110 );
    }
    {
        PyObject *tmp_assign_source_111;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_call_arg_element_45;
        PyObject *tmp_call_arg_element_46;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_50 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 226;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_50;
        tmp_call_arg_element_45 = const_str_plain_pconn;
        tmp_call_arg_element_46 = LIST_COPY( const_list_487a7b5aabbaa0a3116539a420c71666_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 226;
        {
            PyObject *call_args[] = { tmp_call_arg_element_45, tmp_call_arg_element_46 };
            tmp_assign_source_111 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_29, call_args );
        }

        Py_DECREF( tmp_call_arg_element_46 );
        if ( tmp_assign_source_111 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_pconn, tmp_assign_source_111 );
    }
    {
        PyObject *tmp_assign_source_112;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_call_arg_element_47;
        PyObject *tmp_call_arg_element_48;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        if ( tmp_mvar_value_51 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 230;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_51;
        tmp_call_arg_element_47 = const_str_plain_addr;
        tmp_call_arg_element_48 = LIST_COPY( const_list_str_plain_ip_str_plain_port_list );
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 230;
        {
            PyObject *call_args[] = { tmp_call_arg_element_47, tmp_call_arg_element_48 };
            tmp_assign_source_112 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_30, call_args );
        }

        Py_DECREF( tmp_call_arg_element_48 );
        if ( tmp_assign_source_112 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 230;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_addr, tmp_assign_source_112 );
    }
    {
        PyObject *tmp_assign_source_113;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_tuple_element_9;
        PyObject *tmp_list_element_1;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_list_element_2;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_tuple_element_10;
        PyObject *tmp_list_element_3;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_list_element_4;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_tuple_element_11;
        PyObject *tmp_list_element_5;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_list_element_6;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_tuple_element_12;
        PyObject *tmp_list_element_7;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_list_element_8;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_tuple_element_13;
        PyObject *tmp_list_element_9;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_list_element_10;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_tuple_element_14;
        PyObject *tmp_list_element_11;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_list_element_12;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_tuple_element_15;
        PyObject *tmp_list_element_13;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_list_element_14;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_tuple_element_16;
        PyObject *tmp_list_element_15;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_list_element_16;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_mvar_value_76;
        tmp_dict_key_1 = const_str_plain_all;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET );
        }

        if ( tmp_mvar_value_52 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 239;

            goto frame_exception_exit_1;
        }

        tmp_list_element_1 = tmp_mvar_value_52;
        tmp_tuple_element_9 = PyList_New( 3 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_tuple_element_9, 0, tmp_list_element_1 );
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_tuple_element_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 239;

            goto frame_exception_exit_1;
        }

        tmp_list_element_1 = tmp_mvar_value_53;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_tuple_element_9, 1, tmp_list_element_1 );
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_UNIX );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_UNIX );
        }

        if ( tmp_mvar_value_54 == NULL )
        {
            Py_DECREF( tmp_tuple_element_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_UNIX" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 239;

            goto frame_exception_exit_1;
        }

        tmp_list_element_1 = tmp_mvar_value_54;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_tuple_element_9, 2, tmp_list_element_1 );
        tmp_dict_value_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_1, 0, tmp_tuple_element_9 );
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
        }

        if ( tmp_mvar_value_55 == NULL )
        {
            Py_DECREF( tmp_dict_value_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 239;

            goto frame_exception_exit_1;
        }

        tmp_list_element_2 = tmp_mvar_value_55;
        tmp_tuple_element_9 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_tuple_element_9, 0, tmp_list_element_2 );
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
        }

        if ( tmp_mvar_value_56 == NULL )
        {
            Py_DECREF( tmp_dict_value_1 );
            Py_DECREF( tmp_tuple_element_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 239;

            goto frame_exception_exit_1;
        }

        tmp_list_element_2 = tmp_mvar_value_56;
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_tuple_element_9, 1, tmp_list_element_2 );
        PyTuple_SET_ITEM( tmp_dict_value_1, 1, tmp_tuple_element_9 );
        tmp_assign_source_113 = _PyDict_NewPresized( 8 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_tcp;
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;

            goto frame_exception_exit_1;
        }

        tmp_list_element_3 = tmp_mvar_value_57;
        tmp_tuple_element_10 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_3 );
        PyList_SET_ITEM( tmp_tuple_element_10, 0, tmp_list_element_3 );
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_58 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_tuple_element_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;

            goto frame_exception_exit_1;
        }

        tmp_list_element_3 = tmp_mvar_value_58;
        Py_INCREF( tmp_list_element_3 );
        PyList_SET_ITEM( tmp_tuple_element_10, 1, tmp_list_element_3 );
        tmp_dict_value_2 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_2, 0, tmp_tuple_element_10 );
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
        }

        if ( tmp_mvar_value_59 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;

            goto frame_exception_exit_1;
        }

        tmp_list_element_4 = tmp_mvar_value_59;
        tmp_tuple_element_10 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_4 );
        PyList_SET_ITEM( tmp_tuple_element_10, 0, tmp_list_element_4 );
        PyTuple_SET_ITEM( tmp_dict_value_2, 1, tmp_tuple_element_10 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_tcp4;
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET );
        }

        if ( tmp_mvar_value_60 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 241;

            goto frame_exception_exit_1;
        }

        tmp_list_element_5 = tmp_mvar_value_60;
        tmp_tuple_element_11 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_5 );
        PyList_SET_ITEM( tmp_tuple_element_11, 0, tmp_list_element_5 );
        tmp_dict_value_3 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_3, 0, tmp_tuple_element_11 );
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 241;

            goto frame_exception_exit_1;
        }

        tmp_list_element_6 = tmp_mvar_value_61;
        tmp_tuple_element_11 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_6 );
        PyList_SET_ITEM( tmp_tuple_element_11, 0, tmp_list_element_6 );
        PyTuple_SET_ITEM( tmp_dict_value_3, 1, tmp_tuple_element_11 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_udp;
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 242;

            goto frame_exception_exit_1;
        }

        tmp_list_element_7 = tmp_mvar_value_62;
        tmp_tuple_element_12 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_7 );
        PyList_SET_ITEM( tmp_tuple_element_12, 0, tmp_list_element_7 );
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_tuple_element_12 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 242;

            goto frame_exception_exit_1;
        }

        tmp_list_element_7 = tmp_mvar_value_63;
        Py_INCREF( tmp_list_element_7 );
        PyList_SET_ITEM( tmp_tuple_element_12, 1, tmp_list_element_7 );
        tmp_dict_value_4 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_4, 0, tmp_tuple_element_12 );
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 242;

            goto frame_exception_exit_1;
        }

        tmp_list_element_8 = tmp_mvar_value_64;
        tmp_tuple_element_12 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_8 );
        PyList_SET_ITEM( tmp_tuple_element_12, 0, tmp_list_element_8 );
        PyTuple_SET_ITEM( tmp_dict_value_4, 1, tmp_tuple_element_12 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_udp4;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET );
        }

        if ( tmp_mvar_value_65 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;

            goto frame_exception_exit_1;
        }

        tmp_list_element_9 = tmp_mvar_value_65;
        tmp_tuple_element_13 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_9 );
        PyList_SET_ITEM( tmp_tuple_element_13, 0, tmp_list_element_9 );
        tmp_dict_value_5 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_5, 0, tmp_tuple_element_13 );
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;

            goto frame_exception_exit_1;
        }

        tmp_list_element_10 = tmp_mvar_value_66;
        tmp_tuple_element_13 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_10 );
        PyList_SET_ITEM( tmp_tuple_element_13, 0, tmp_list_element_10 );
        PyTuple_SET_ITEM( tmp_dict_value_5, 1, tmp_tuple_element_13 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_inet;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 244;

            goto frame_exception_exit_1;
        }

        tmp_list_element_11 = tmp_mvar_value_67;
        tmp_tuple_element_14 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_11 );
        PyList_SET_ITEM( tmp_tuple_element_14, 0, tmp_list_element_11 );
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_tuple_element_14 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 244;

            goto frame_exception_exit_1;
        }

        tmp_list_element_11 = tmp_mvar_value_68;
        Py_INCREF( tmp_list_element_11 );
        PyList_SET_ITEM( tmp_tuple_element_14, 1, tmp_list_element_11 );
        tmp_dict_value_6 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_6, 0, tmp_tuple_element_14 );
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 244;

            goto frame_exception_exit_1;
        }

        tmp_list_element_12 = tmp_mvar_value_69;
        tmp_tuple_element_14 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_12 );
        PyList_SET_ITEM( tmp_tuple_element_14, 0, tmp_list_element_12 );
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_6 );
            Py_DECREF( tmp_tuple_element_14 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 244;

            goto frame_exception_exit_1;
        }

        tmp_list_element_12 = tmp_mvar_value_70;
        Py_INCREF( tmp_list_element_12 );
        PyList_SET_ITEM( tmp_tuple_element_14, 1, tmp_list_element_12 );
        PyTuple_SET_ITEM( tmp_dict_value_6, 1, tmp_tuple_element_14 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_inet4;
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 245;

            goto frame_exception_exit_1;
        }

        tmp_list_element_13 = tmp_mvar_value_71;
        tmp_tuple_element_15 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_13 );
        PyList_SET_ITEM( tmp_tuple_element_15, 0, tmp_list_element_13 );
        tmp_dict_value_7 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_7, 0, tmp_tuple_element_15 );
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
        }

        if ( tmp_mvar_value_72 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 245;

            goto frame_exception_exit_1;
        }

        tmp_list_element_14 = tmp_mvar_value_72;
        tmp_tuple_element_15 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_14 );
        PyList_SET_ITEM( tmp_tuple_element_15, 0, tmp_list_element_14 );
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
        }

        if ( tmp_mvar_value_73 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_7 );
            Py_DECREF( tmp_tuple_element_15 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 245;

            goto frame_exception_exit_1;
        }

        tmp_list_element_14 = tmp_mvar_value_73;
        Py_INCREF( tmp_list_element_14 );
        PyList_SET_ITEM( tmp_tuple_element_15, 1, tmp_list_element_14 );
        PyTuple_SET_ITEM( tmp_dict_value_7, 1, tmp_tuple_element_15 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_inet6;
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_74 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 246;

            goto frame_exception_exit_1;
        }

        tmp_list_element_15 = tmp_mvar_value_74;
        tmp_tuple_element_16 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_15 );
        PyList_SET_ITEM( tmp_tuple_element_16, 0, tmp_list_element_15 );
        tmp_dict_value_8 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dict_value_8, 0, tmp_tuple_element_16 );
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
        }

        if ( tmp_mvar_value_75 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 246;

            goto frame_exception_exit_1;
        }

        tmp_list_element_16 = tmp_mvar_value_75;
        tmp_tuple_element_16 = PyList_New( 2 );
        Py_INCREF( tmp_list_element_16 );
        PyList_SET_ITEM( tmp_tuple_element_16, 0, tmp_list_element_16 );
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
        }

        if ( tmp_mvar_value_76 == NULL )
        {
            Py_DECREF( tmp_assign_source_113 );
            Py_DECREF( tmp_dict_value_8 );
            Py_DECREF( tmp_tuple_element_16 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 246;

            goto frame_exception_exit_1;
        }

        tmp_list_element_16 = tmp_mvar_value_76;
        Py_INCREF( tmp_list_element_16 );
        PyList_SET_ITEM( tmp_tuple_element_16, 1, tmp_list_element_16 );
        PyTuple_SET_ITEM( tmp_dict_value_8, 1, tmp_tuple_element_16 );
        tmp_res = PyDict_SetItem( tmp_assign_source_113, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_conn_tmap, tmp_assign_source_113 );
    }
    {
        nuitka_bool tmp_condition_result_21;
        PyObject *tmp_compexpr_left_12;
        PyObject *tmp_compexpr_right_12;
        PyObject *tmp_mvar_value_77;
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
        }

        if ( tmp_mvar_value_77 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 249;

            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_12 = tmp_mvar_value_77;
        tmp_compexpr_right_12 = Py_None;
        tmp_condition_result_21 = ( tmp_compexpr_left_12 != tmp_compexpr_right_12 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_16;
        }
        else
        {
            goto branch_no_16;
        }
        branch_yes_16:;
        {
            PyObject *tmp_called_name_31;
            PyObject *tmp_source_name_26;
            PyObject *tmp_mvar_value_78;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            PyObject *tmp_tuple_element_17;
            PyObject *tmp_list_element_17;
            PyObject *tmp_mvar_value_79;
            PyObject *tmp_list_element_18;
            PyObject *tmp_mvar_value_80;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            PyObject *tmp_tuple_element_18;
            PyObject *tmp_list_element_19;
            PyObject *tmp_mvar_value_81;
            PyObject *tmp_list_element_20;
            PyObject *tmp_mvar_value_82;
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_conn_tmap );

            if (unlikely( tmp_mvar_value_78 == NULL ))
            {
                tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_conn_tmap );
            }

            if ( tmp_mvar_value_78 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "conn_tmap" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 250;

                goto frame_exception_exit_1;
            }

            tmp_source_name_26 = tmp_mvar_value_78;
            tmp_called_name_31 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_update );
            if ( tmp_called_name_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 250;

                goto frame_exception_exit_1;
            }
            tmp_dict_key_9 = const_str_plain_tcp6;
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

            if (unlikely( tmp_mvar_value_79 == NULL ))
            {
                tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
            }

            if ( tmp_mvar_value_79 == NULL )
            {
                Py_DECREF( tmp_called_name_31 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 251;

                goto frame_exception_exit_1;
            }

            tmp_list_element_17 = tmp_mvar_value_79;
            tmp_tuple_element_17 = PyList_New( 1 );
            Py_INCREF( tmp_list_element_17 );
            PyList_SET_ITEM( tmp_tuple_element_17, 0, tmp_list_element_17 );
            tmp_dict_value_9 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_9, 0, tmp_tuple_element_17 );
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

            if (unlikely( tmp_mvar_value_80 == NULL ))
            {
                tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
            }

            if ( tmp_mvar_value_80 == NULL )
            {
                Py_DECREF( tmp_called_name_31 );
                Py_DECREF( tmp_dict_value_9 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 251;

                goto frame_exception_exit_1;
            }

            tmp_list_element_18 = tmp_mvar_value_80;
            tmp_tuple_element_17 = PyList_New( 1 );
            Py_INCREF( tmp_list_element_18 );
            PyList_SET_ITEM( tmp_tuple_element_17, 0, tmp_list_element_18 );
            PyTuple_SET_ITEM( tmp_dict_value_9, 1, tmp_tuple_element_17 );
            tmp_args_element_name_3 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_args_element_name_3, tmp_dict_key_9, tmp_dict_value_9 );
            Py_DECREF( tmp_dict_value_9 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_10 = const_str_plain_udp6;
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_INET6 );

            if (unlikely( tmp_mvar_value_81 == NULL ))
            {
                tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_INET6 );
            }

            if ( tmp_mvar_value_81 == NULL )
            {
                Py_DECREF( tmp_called_name_31 );
                Py_DECREF( tmp_args_element_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET6" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 252;

                goto frame_exception_exit_1;
            }

            tmp_list_element_19 = tmp_mvar_value_81;
            tmp_tuple_element_18 = PyList_New( 1 );
            Py_INCREF( tmp_list_element_19 );
            PyList_SET_ITEM( tmp_tuple_element_18, 0, tmp_list_element_19 );
            tmp_dict_value_10 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_10, 0, tmp_tuple_element_18 );
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

            if (unlikely( tmp_mvar_value_82 == NULL ))
            {
                tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
            }

            if ( tmp_mvar_value_82 == NULL )
            {
                Py_DECREF( tmp_called_name_31 );
                Py_DECREF( tmp_args_element_name_3 );
                Py_DECREF( tmp_dict_value_10 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 252;

                goto frame_exception_exit_1;
            }

            tmp_list_element_20 = tmp_mvar_value_82;
            tmp_tuple_element_18 = PyList_New( 1 );
            Py_INCREF( tmp_list_element_20 );
            PyList_SET_ITEM( tmp_tuple_element_18, 0, tmp_list_element_20 );
            PyTuple_SET_ITEM( tmp_dict_value_10, 1, tmp_tuple_element_18 );
            tmp_res = PyDict_SetItem( tmp_args_element_name_3, tmp_dict_key_10, tmp_dict_value_10 );
            Py_DECREF( tmp_dict_value_10 );
            assert( !(tmp_res != 0) );
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 250;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, call_args );
            }

            Py_DECREF( tmp_called_name_31 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 250;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_16:;
    }
    {
        nuitka_bool tmp_condition_result_22;
        PyObject *tmp_compexpr_left_13;
        PyObject *tmp_compexpr_right_13;
        PyObject *tmp_mvar_value_83;
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_UNIX );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_UNIX );
        }

        if ( tmp_mvar_value_83 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_UNIX" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 255;

            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_13 = tmp_mvar_value_83;
        tmp_compexpr_right_13 = Py_None;
        tmp_condition_result_22 = ( tmp_compexpr_left_13 != tmp_compexpr_right_13 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_17;
        }
        else
        {
            goto branch_no_17;
        }
        branch_yes_17:;
        {
            PyObject *tmp_called_name_32;
            PyObject *tmp_source_name_27;
            PyObject *tmp_mvar_value_84;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            PyObject *tmp_tuple_element_19;
            PyObject *tmp_list_element_21;
            PyObject *tmp_mvar_value_85;
            PyObject *tmp_list_element_22;
            PyObject *tmp_mvar_value_86;
            PyObject *tmp_mvar_value_87;
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_conn_tmap );

            if (unlikely( tmp_mvar_value_84 == NULL ))
            {
                tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_conn_tmap );
            }

            if ( tmp_mvar_value_84 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "conn_tmap" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 256;

                goto frame_exception_exit_1;
            }

            tmp_source_name_27 = tmp_mvar_value_84;
            tmp_called_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_update );
            if ( tmp_called_name_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 256;

                goto frame_exception_exit_1;
            }
            tmp_dict_key_11 = const_str_plain_unix;
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_AF_UNIX );

            if (unlikely( tmp_mvar_value_85 == NULL ))
            {
                tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AF_UNIX );
            }

            if ( tmp_mvar_value_85 == NULL )
            {
                Py_DECREF( tmp_called_name_32 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_UNIX" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 257;

                goto frame_exception_exit_1;
            }

            tmp_list_element_21 = tmp_mvar_value_85;
            tmp_tuple_element_19 = PyList_New( 1 );
            Py_INCREF( tmp_list_element_21 );
            PyList_SET_ITEM( tmp_tuple_element_19, 0, tmp_list_element_21 );
            tmp_dict_value_11 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_dict_value_11, 0, tmp_tuple_element_19 );
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );

            if (unlikely( tmp_mvar_value_86 == NULL ))
            {
                tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_STREAM );
            }

            if ( tmp_mvar_value_86 == NULL )
            {
                Py_DECREF( tmp_called_name_32 );
                Py_DECREF( tmp_dict_value_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 257;

                goto frame_exception_exit_1;
            }

            tmp_list_element_22 = tmp_mvar_value_86;
            tmp_tuple_element_19 = PyList_New( 2 );
            Py_INCREF( tmp_list_element_22 );
            PyList_SET_ITEM( tmp_tuple_element_19, 0, tmp_list_element_22 );
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );

            if (unlikely( tmp_mvar_value_87 == NULL ))
            {
                tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SOCK_DGRAM );
            }

            if ( tmp_mvar_value_87 == NULL )
            {
                Py_DECREF( tmp_called_name_32 );
                Py_DECREF( tmp_dict_value_11 );
                Py_DECREF( tmp_tuple_element_19 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 257;

                goto frame_exception_exit_1;
            }

            tmp_list_element_22 = tmp_mvar_value_87;
            Py_INCREF( tmp_list_element_22 );
            PyList_SET_ITEM( tmp_tuple_element_19, 1, tmp_list_element_22 );
            PyTuple_SET_ITEM( tmp_dict_value_11, 1, tmp_tuple_element_19 );
            tmp_args_element_name_4 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_args_element_name_4, tmp_dict_key_11, tmp_dict_value_11 );
            Py_DECREF( tmp_dict_value_11 );
            assert( !(tmp_res != 0) );
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 256;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
            }

            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 256;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_17:;
    }
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_psutil$_common, const_str_plain_AF_INET );
    tmp_result = tmp_res != -1;
    if ( tmp_result == false ) CLEAR_ERROR_OCCURRED();

    if ( tmp_result == false )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_INET" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 260;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_psutil$_common, const_str_plain_AF_UNIX );
    tmp_result = tmp_res != -1;
    if ( tmp_result == false ) CLEAR_ERROR_OCCURRED();

    if ( tmp_result == false )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AF_UNIX" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 260;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_psutil$_common, const_str_plain_SOCK_STREAM );
    tmp_result = tmp_res != -1;
    if ( tmp_result == false ) CLEAR_ERROR_OCCURRED();

    if ( tmp_result == false )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_STREAM" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 260;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_psutil$_common, const_str_plain_SOCK_DGRAM );
    tmp_result = tmp_res != -1;
    if ( tmp_result == false ) CLEAR_ERROR_OCCURRED();

    if ( tmp_result == false )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SOCK_DGRAM" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 260;

        goto frame_exception_exit_1;
    }

    {
        PyObject *tmp_assign_source_114;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_114 = MAKE_FUNCTION_psutil$_common$$$function_1_usage_percent( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_usage_percent, tmp_assign_source_114 );
    }
    {
        PyObject *tmp_assign_source_115;
        tmp_assign_source_115 = MAKE_FUNCTION_psutil$_common$$$function_2_memoize(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_memoize, tmp_assign_source_115 );
    }
    {
        PyObject *tmp_assign_source_116;
        tmp_assign_source_116 = MAKE_FUNCTION_psutil$_common$$$function_3_memoize_when_activated(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_memoize_when_activated, tmp_assign_source_116 );
    }
    {
        PyObject *tmp_assign_source_117;
        tmp_assign_source_117 = MAKE_FUNCTION_psutil$_common$$$function_4_isfile_strict(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_isfile_strict, tmp_assign_source_117 );
    }
    {
        PyObject *tmp_assign_source_118;
        tmp_assign_source_118 = MAKE_FUNCTION_psutil$_common$$$function_5_path_exists_strict(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_path_exists_strict, tmp_assign_source_118 );
    }
    {
        PyObject *tmp_assign_source_119;
        PyObject *tmp_called_name_33;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_memoize );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_memoize );
        }

        CHECK_OBJECT( tmp_mvar_value_88 );
        tmp_called_name_33 = tmp_mvar_value_88;
        tmp_args_element_name_5 = MAKE_FUNCTION_psutil$_common$$$function_6_supports_ipv6(  );



        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 399;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_119 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_119 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 399;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_supports_ipv6, tmp_assign_source_119 );
    }
    {
        PyObject *tmp_assign_source_120;
        tmp_assign_source_120 = MAKE_FUNCTION_psutil$_common$$$function_7_parse_environ_block(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_parse_environ_block, tmp_assign_source_120 );
    }
    {
        PyObject *tmp_assign_source_121;
        tmp_assign_source_121 = MAKE_FUNCTION_psutil$_common$$$function_8_sockfam_to_enum(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_sockfam_to_enum, tmp_assign_source_121 );
    }
    {
        PyObject *tmp_assign_source_122;
        tmp_assign_source_122 = MAKE_FUNCTION_psutil$_common$$$function_9_socktype_to_enum(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_socktype_to_enum, tmp_assign_source_122 );
    }
    {
        PyObject *tmp_assign_source_123;
        tmp_assign_source_123 = MAKE_FUNCTION_psutil$_common$$$function_10_deprecated_method(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_deprecated_method, tmp_assign_source_123 );
    }
    {
        PyObject *tmp_assign_source_124;
        tmp_assign_source_124 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_124;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_125;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_23;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_13;
        }
        tmp_condition_result_23 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_13;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_6:;
        tmp_bases_name_3 = const_tuple_empty;
        tmp_assign_source_125 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_125 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_13;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_125;
    }
    {
        nuitka_bool tmp_condition_result_24;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_13;
        }
        tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_18;
        }
        else
        {
            goto branch_no_18;
        }
        branch_yes_18:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_13;
        }
        branch_no_18:;
    }
    {
        nuitka_bool tmp_condition_result_25;
        PyObject *tmp_source_name_28;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_28 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_28, const_str_plain___prepare__ );
        tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_19;
        }
        else
        {
            goto branch_no_19;
        }
        branch_yes_19:;
        {
            PyObject *tmp_assign_source_126;
            PyObject *tmp_called_name_34;
            PyObject *tmp_source_name_29;
            PyObject *tmp_args_name_5;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_29 = tmp_class_creation_3__metaclass;
            tmp_called_name_34 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain___prepare__ );
            if ( tmp_called_name_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 485;

                goto try_except_handler_13;
            }
            tmp_args_name_5 = const_tuple_str_plain__WrapNumbers_tuple_empty_tuple;
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 485;
            tmp_assign_source_126 = CALL_FUNCTION( tmp_called_name_34, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_34 );
            if ( tmp_assign_source_126 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 485;

                goto try_except_handler_13;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_126;
        }
        {
            nuitka_bool tmp_condition_result_26;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_30;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_30 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_30, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 485;

                goto try_except_handler_13;
            }
            tmp_condition_result_26 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_20;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_31;
                PyObject *tmp_type_arg_5;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_20 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 485;

                    goto try_except_handler_13;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_20 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_5 = tmp_class_creation_3__prepared;
                tmp_source_name_31 = BUILTIN_TYPE1( tmp_type_arg_5 );
                assert( !(tmp_source_name_31 == NULL) );
                tmp_tuple_element_20 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_31 );
                if ( tmp_tuple_element_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 485;

                    goto try_except_handler_13;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_20 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 485;

                    goto try_except_handler_13;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 485;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_13;
            }
            branch_no_20:;
        }
        goto branch_end_19;
        branch_no_19:;
        {
            PyObject *tmp_assign_source_127;
            tmp_assign_source_127 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_127;
        }
        branch_end_19:;
    }
    {
        PyObject *tmp_assign_source_128;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_psutil$_common_485 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c21138c8d575fc0b2b6ff4782a202f72;
        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_15;
        }
        tmp_dictset_value = const_str_digest_6230f983e0ca3f1521efd26013c14e9b;
        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_15;
        }
        tmp_dictset_value = const_str_plain__WrapNumbers;
        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;

            goto try_except_handler_15;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_0928eb9f20a13780a4806d89700f19fe_4, codeobj_0928eb9f20a13780a4806d89700f19fe, module_psutil$_common, sizeof(void *) );
        frame_0928eb9f20a13780a4806d89700f19fe_4 = cache_frame_0928eb9f20a13780a4806d89700f19fe_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_0928eb9f20a13780a4806d89700f19fe_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_0928eb9f20a13780a4806d89700f19fe_4 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_psutil$_common$$$function_11___init__(  );



        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 490;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = MAKE_FUNCTION_psutil$_common$$$function_12__add_dict(  );



        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain__add_dict, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 496;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = MAKE_FUNCTION_psutil$_common$$$function_13__remove_dead_reminders(  );



        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain__remove_dead_reminders, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 504;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = MAKE_FUNCTION_psutil$_common$$$function_14_run(  );



        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain_run, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 515;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        {
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_psutil$_common$$$function_15_cache_clear( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain_cache_clear, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 554;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_psutil$_common$$$function_16_cache_info(  );



        tmp_res = PyObject_SetItem( locals_psutil$_common_485, const_str_plain_cache_info, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 566;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0928eb9f20a13780a4806d89700f19fe_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0928eb9f20a13780a4806d89700f19fe_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_0928eb9f20a13780a4806d89700f19fe_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_0928eb9f20a13780a4806d89700f19fe_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_0928eb9f20a13780a4806d89700f19fe_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_0928eb9f20a13780a4806d89700f19fe_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_0928eb9f20a13780a4806d89700f19fe_4 == cache_frame_0928eb9f20a13780a4806d89700f19fe_4 )
        {
            Py_DECREF( frame_0928eb9f20a13780a4806d89700f19fe_4 );
        }
        cache_frame_0928eb9f20a13780a4806d89700f19fe_4 = NULL;

        assertFrameObject( frame_0928eb9f20a13780a4806d89700f19fe_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_15;
        skip_nested_handling_3:;
        {
            PyObject *tmp_assign_source_129;
            PyObject *tmp_called_name_35;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_21;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_35 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_21 = const_str_plain__WrapNumbers;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_21 );
            tmp_tuple_element_21 = const_tuple_empty;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_21 );
            tmp_tuple_element_21 = locals_psutil$_common_485;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_21 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 485;
            tmp_assign_source_129 = CALL_FUNCTION( tmp_called_name_35, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_129 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 485;

                goto try_except_handler_15;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_129;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_128 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_128 );
        goto try_return_handler_15;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_15:;
        Py_DECREF( locals_psutil$_common_485 );
        locals_psutil$_common_485 = NULL;
        goto try_return_handler_14;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_psutil$_common_485 );
        locals_psutil$_common_485 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto try_except_handler_14;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( psutil$_common );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( psutil$_common );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 485;
        goto try_except_handler_13;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain__WrapNumbers, tmp_assign_source_128 );
    }
    goto try_end_9;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    {
        PyObject *tmp_assign_source_130;
        tmp_assign_source_130 = MAKE_FUNCTION_psutil$_common$$$function_17_wrap_numbers(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_wrap_numbers, tmp_assign_source_130 );
    }
    {
        PyObject *tmp_assign_source_131;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_89;
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain__WrapNumbers );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__WrapNumbers );
        }

        if ( tmp_mvar_value_89 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_WrapNumbers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 581;

            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_89;
        frame_641a34ae48b2aa2a3be9421131bc3092->m_frame.f_lineno = 581;
        tmp_assign_source_131 = CALL_FUNCTION_NO_ARGS( tmp_called_name_36 );
        if ( tmp_assign_source_131 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 581;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain__wn, tmp_assign_source_131 );
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_source_name_32;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_mvar_value_91;
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain__wn );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__wn );
        }

        CHECK_OBJECT( tmp_mvar_value_90 );
        tmp_source_name_32 = tmp_mvar_value_90;
        tmp_assattr_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_cache_clear );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 582;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_wrap_numbers );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wrap_numbers );
        }

        if ( tmp_mvar_value_91 == NULL )
        {
            Py_DECREF( tmp_assattr_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wrap_numbers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 582;

            goto frame_exception_exit_1;
        }

        tmp_assattr_target_3 = tmp_mvar_value_91;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_cache_clear, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 582;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_source_name_33;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_mvar_value_93;
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain__wn );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__wn );
        }

        if ( tmp_mvar_value_92 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_wn" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 583;

            goto frame_exception_exit_1;
        }

        tmp_source_name_33 = tmp_mvar_value_92;
        tmp_assattr_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_cache_info );
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 583;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_wrap_numbers );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wrap_numbers );
        }

        if ( tmp_mvar_value_93 == NULL )
        {
            Py_DECREF( tmp_assattr_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wrap_numbers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 583;

            goto frame_exception_exit_1;
        }

        tmp_assattr_target_4 = tmp_mvar_value_93;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_cache_info, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 583;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_132;
        tmp_assign_source_132 = MAKE_FUNCTION_psutil$_common$$$function_18_open_binary(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_open_binary, tmp_assign_source_132 );
    }
    {
        PyObject *tmp_assign_source_133;
        tmp_assign_source_133 = MAKE_FUNCTION_psutil$_common$$$function_19_open_text(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_open_text, tmp_assign_source_133 );
    }
    {
        PyObject *tmp_assign_source_134;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_str_digest_d322b24329b5f9cbfeea0da3ac450745_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_134 = MAKE_FUNCTION_psutil$_common$$$function_20_bytes2human( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_bytes2human, tmp_assign_source_134 );
    }
    {
        PyObject *tmp_assign_source_135;
        tmp_assign_source_135 = MAKE_FUNCTION_psutil$_common$$$function_21_get_procfs_path(  );



        UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_get_procfs_path, tmp_assign_source_135 );
    }
    {
        nuitka_bool tmp_condition_result_27;
        PyObject *tmp_mvar_value_94;
        int tmp_truth_name_4;
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        if ( tmp_mvar_value_94 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 629;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_mvar_value_94 );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 629;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_27 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_21;
        }
        else
        {
            goto branch_no_21;
        }
        branch_yes_21:;
        {
            PyObject *tmp_assign_source_136;
            tmp_assign_source_136 = MAKE_FUNCTION_psutil$_common$$$function_22_decode(  );



            UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_decode, tmp_assign_source_136 );
        }
        goto branch_end_21;
        branch_no_21:;
        {
            PyObject *tmp_assign_source_137;
            tmp_assign_source_137 = MAKE_FUNCTION_psutil$_common$$$function_23_decode(  );



            UPDATE_STRING_DICT1( moduledict_psutil$_common, (Nuitka_StringObject *)const_str_plain_decode, tmp_assign_source_137 );
        }
        branch_end_21:;
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_641a34ae48b2aa2a3be9421131bc3092 );
#endif
    popFrameStack();

    assertFrameObject( frame_641a34ae48b2aa2a3be9421131bc3092 );

    goto frame_no_exception_4;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_641a34ae48b2aa2a3be9421131bc3092 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_641a34ae48b2aa2a3be9421131bc3092, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_641a34ae48b2aa2a3be9421131bc3092->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_641a34ae48b2aa2a3be9421131bc3092, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_4:;

    return MOD_RETURN_VALUE( module_psutil$_common );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
