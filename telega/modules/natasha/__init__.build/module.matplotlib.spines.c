/* Generated code for Python module 'matplotlib.spines'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$spines" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$spines;
PyDictObject *moduledict_matplotlib$spines;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_d91088e024bb306c7c9816e9c6e3ea41;
extern PyObject *const_str_plain_intervaly;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_4eb5c8b8ae4c516a96c86250167f0763;
extern PyObject *const_str_plain_stale;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_str_plain_yaxis;
extern PyObject *const_str_plain_blended_transform_factory;
extern PyObject *const_str_plain_scale;
extern PyObject *const_str_plain_max;
static PyObject *const_str_digest_43a0d20a36c74a4f0cc7701ef5b07729;
extern PyObject *const_str_plain_set_linewidth;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_rmin;
static PyObject *const_list_str_plain_left_list;
extern PyObject *const_str_plain__size;
extern PyObject *const_str_digest_504ff24db154606f1bfc1985dd5cc9e4;
static PyObject *const_tuple_str_plain_allow_rasterization_tuple;
static PyObject *const_tuple_87ed802b88cc6ec2549178254512f498_tuple;
static PyObject *const_str_plain_tickl;
static PyObject *const_str_digest_d1d094a2ccd8ffcb8a48b5737424aa70;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_transLimits;
extern PyObject *const_str_plain_tick1line;
extern PyObject *const_str_plain_rorigin;
static PyObject *const_str_digest_4cde890927cc2b88beac2083dde55dbb;
extern PyObject *const_str_plain_matplotlib;
static PyObject *const_str_digest_d7df2bc1ee2d5e920ce5e74ee0a59514;
extern PyObject *const_str_plain_projecting;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain_numpy;
extern PyObject *const_str_plain_high;
static PyObject *const_str_plain_offset_vec;
extern PyObject *const_str_plain_none;
static PyObject *const_str_digest_05884c283e9966b055226c4ef8509afc;
extern PyObject *const_str_plain_min;
static PyObject *const_str_digest_60b6c7d6206290ea6562a9ab363af740;
extern PyObject *const_float_1_0;
extern PyObject *const_str_plain_path;
static PyObject *const_str_digest_ec2beb12f88ffcdb17573ee7e2632e33;
static PyObject *const_str_digest_bdb87b76fac327390dcf9d24b80e6456;
extern PyObject *const_str_plain_patches;
static PyObject *const_str_digest_ab1bc000ced809648b71ee9f8efb43ad;
static PyObject *const_str_digest_acebd7f4ec0b9d1cad545899e69bd71b;
static PyObject *const_str_digest_4e850d568760852e97a1610f6e76f79c;
extern PyObject *const_str_plain_reset_ticks;
extern PyObject *const_str_plain_y1;
extern PyObject *const_str_plain_rcParams;
static PyObject *const_str_plain_data_xform;
static PyObject *const_str_digest_e625b20cc5b20388c7b371959b1ae678;
extern PyObject *const_str_plain_cond;
extern PyObject *const_str_digest_d68f16e49ad8add4cd6ce282eb14b4a7;
static PyObject *const_str_digest_470b7b4df7b47adb1238b6037fa4cd0a;
extern PyObject *const_str_plain_docstring;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_from_values;
extern PyObject *const_int_0;
static PyObject *const_str_digest_63927bfa0c43c549c9c79521fa550712;
static PyObject *const_str_plain_datalim_high;
extern PyObject *const_tuple_str_plain_self_str_plain___class___tuple;
extern PyObject *const_tuple_int_0_int_pos_1_tuple;
static PyObject *const_str_plain__theta2;
extern PyObject *const_str_digest_2076eb8266023786dfd5dac58b1cb6de;
extern PyObject *const_str_plain_get_yaxis_transform;
extern PyObject *const_str_plain_get_rorigin;
static PyObject *const_str_digest_c85d08013ec811690755b19130a310a3;
extern PyObject *const_str_plain_shape;
extern PyObject *const_str_plain_get_window_extent;
extern PyObject *const_str_plain_Bbox;
extern PyObject *const_str_plain_identity;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain__patch_transform;
extern PyObject *const_str_plain_set_visible;
extern PyObject *const_dict_0563751b61a311cc65ebabc9436682b6;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_tuple_float_0_0_float_0_999_tuple;
extern PyObject *const_str_plain_bottom;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple;
extern PyObject *const_str_plain_dpi;
static PyObject *const_str_digest_5aefd65bb28639ccb724edefbb95e1a5;
extern PyObject *const_str_plain_post;
static PyObject *const_str_plain_how;
static PyObject *const_tuple_ab78564a503682fe20f7d635d6477ed0_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_union;
static PyObject *const_str_digest_7346bb7bbc2b801fa2a2b8257b91943c;
extern PyObject *const_str_plain_arc;
extern PyObject *const_str_plain_translate;
extern PyObject *const_str_plain_vertices;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_tuple_str_plain_self_str_plain_c_tuple;
extern PyObject *const_str_plain_format;
static PyObject *const_str_digest_3e39b8a66b612e3cde13c3b6246d1d12;
static PyObject *const_str_plain_tickstocheck;
extern PyObject *const_list_str_plain_left_str_plain_right_list;
extern PyObject *const_str_plain_position;
extern PyObject *const_str_plain_xaxis;
extern PyObject *const_str_plain_rad2deg;
static PyObject *const_str_digest_74aa8353e4cb4da0043cf2dbbba91ba7;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_left_str_plain_right_tuple;
extern PyObject *const_str_plain_set_edgecolor;
extern PyObject *const_str_plain_get_theta_offset;
static PyObject *const_str_plain_padout;
extern PyObject *const_str_plain_circular_spine;
extern PyObject *const_str_plain_set_position;
extern PyObject *const_str_plain_transScale;
static PyObject *const_str_plain_position_type;
static PyObject *const_tuple_float_0_999_float_1_0_tuple;
static PyObject *const_str_plain_base_transform;
static PyObject *const_str_plain_set_patch_line;
extern PyObject *const_str_digest_50e4933a9d0fc470d2deeb63d403662b;
static PyObject *const_str_plain__spine_transform;
static PyObject *const_str_digest_dd619daddaa7d6aac3f692e9458ec6a1;
extern PyObject *const_str_plain_Patch;
extern PyObject *const_str_plain_frozen;
static PyObject *const_str_digest_9c77d829d34c9c75bf5eee383da79b08;
extern PyObject *const_float_0_5;
extern PyObject *const_str_plain__bounds;
extern PyObject *const_str_digest_df695419470221061addd88ff9f521ff;
static PyObject *const_str_plain_is_frame_like;
extern PyObject *const_str_digest_7823f2c53877057ff15faa7c3608db53;
extern PyObject *const_str_plain_get_spine_transform;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_c0a88f9552b5ec7f51197257738e17a1;
extern PyObject *const_str_plain_transData;
extern PyObject *const_str_plain_set_capstyle;
static PyObject *const_str_digest_97dbc6b904ee2c21b17f694d05e99ac4;
extern PyObject *const_tuple_int_pos_2_int_pos_2_tuple;
static PyObject *const_str_digest_dbd58e05dc84889d5ad7cf580b405b20;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_y0;
static PyObject *const_str_digest_cbff2e8d0e23c4ff96229c43d46e8c71;
extern PyObject *const_str_plain_value;
extern PyObject *const_tuple_str_plain_self_str_plain_low_str_plain_high_tuple;
extern PyObject *const_str_plain_x0;
extern PyObject *const_str_plain__position;
extern PyObject *const_tuple_str_plain_none_tuple;
static PyObject *const_str_plain_tickvals;
extern PyObject *const_str_plain_intervalx;
static PyObject *const_tuple_str_plain_data_int_0_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_value_tuple;
extern PyObject *const_str_plain_top;
static PyObject *const_str_digest_57cacffc3b78985b437104b8d0e0907f;
extern PyObject *const_str_plain_set_figure;
extern PyObject *const_str_plain__center;
extern PyObject *const_str_plain_mpatches;
static PyObject *const_str_digest_f2cbfcf5028382197dc3153734a1c6cd;
static PyObject *const_str_digest_879be27065d0bc95273e5f3bff67279d;
static PyObject *const_str_digest_a40723ed11e4414a2998ff867a896a6d;
extern PyObject *const_float_2_5;
extern PyObject *const_str_plain_bboxes;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_get_yticks;
static PyObject *const_float_0_999;
extern PyObject *const_str_plain_theta1;
static PyObject *const_str_digest_0509ad98bae8d5faed4f9802c9149ef1;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain__ensure_position_is_set;
extern PyObject *const_str_plain_transAxes;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_tuple_51b7a56d41bf9c59329d657db55def47_tuple;
static PyObject *const_list_f8554cd76988f9d6d7371cacaa6c6a26_list;
static PyObject *const_str_plain_spine_type;
static PyObject *const_tuple_float_1_0_float_0_999_tuple;
extern PyObject *const_str_plain_ScaledTranslation;
static PyObject *const_tuple_str_plain_self_str_plain_center_str_plain_radius_tuple;
extern PyObject *const_str_plain_set_transform;
static PyObject *const_str_digest_98873532fb91af52ecaeed111a5dfadd;
extern PyObject *const_str_plain_transforms;
static PyObject *const_list_str_plain_bottom_str_plain_top_list;
static PyObject *const_str_digest_33812b40370aee00bde878bebcde1dfb;
static PyObject *const_str_digest_3ea6191f8e6badb08a6c198cb3f8b746;
extern PyObject *const_str_plain_ticks;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_left;
static PyObject *const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_set_bounds;
extern PyObject *const_str_plain_figure;
static PyObject *const_str_digest_f3047b8c15cc12f7c448de553e890bfd;
extern PyObject *const_str_plain_draw;
static PyObject *const_dict_75f1ed2738ae304e7e502ba9973de3b5;
extern PyObject *const_float_0_0;
static PyObject *const_list_str_plain_top_list;
extern PyObject *const_str_plain_dedent_interpd;
static PyObject *const_str_digest_4d5fff89f9803dc4e2d1dfb63dc84c28;
extern PyObject *const_str_plain_grid;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain_v1;
static PyObject *const_str_digest_3f6a3a283f4f6d95d2c969fadb020cd2;
extern PyObject *const_str_plain_Path;
static PyObject *const_tuple_str_digest_18dd205cb77f09d4e383f56ea1ba901d_tuple;
extern PyObject *const_str_plain_get_smart_bounds;
extern PyObject *const_str_plain_register_axis;
extern PyObject *const_tuple_str_plain_self_str_plain_axis_tuple;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_set_color;
extern PyObject *const_str_plain_which;
static PyObject *const_str_digest_b3cc18b655aab4953fc3abb35d5e21ed;
extern PyObject *const_str_plain_classmethod;
static PyObject *const_tuple_str_plain_center_str_plain_zero_tuple;
static PyObject *const_str_plain_viewlim_high;
extern PyObject *const_str_plain_get_visible;
static PyObject *const_str_digest_d12962331895cfa026fce7746c626985;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_int_neg_1_int_0_tuple;
extern PyObject *const_str_plain_data;
extern PyObject *const_list_c333dfadccd8c3bb49d860b0b29f8fad_list;
static PyObject *const_str_plain_padin;
static PyObject *const_tuple_float_0_999_float_0_0_tuple;
extern PyObject *const_str_plain_mpath;
extern PyObject *const_str_plain_direction;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_str_digest_e8221eb249e2a624dc2a35c5cdce05c5;
static PyObject *const_str_plain__theta1;
static PyObject *const_str_plain__calc_offset_transform;
extern PyObject *const_str_plain_height;
static PyObject *const_str_plain_datalim_low;
extern PyObject *const_str_plain__smart_bounds;
extern PyObject *const_str_plain_what;
extern PyObject *const_str_plain_low;
static PyObject *const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_out;
extern PyObject *const_str_plain_append;
static PyObject *const_tuple_str_digest_4cde890927cc2b88beac2083dde55dbb_tuple;
extern PyObject *const_str_plain_set_zorder;
static PyObject *const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple;
static PyObject *const_str_digest_07e50c74d2c6c2d4a635225d2f41a719;
static PyObject *const_list_6286e504ff565f6e3366f4d017b6e11f_list;
extern PyObject *const_str_plain_pre;
extern PyObject *const_str_plain_tick;
extern PyObject *const_str_plain_renderer;
static PyObject *const_list_cbfdd3127f278d92c67096f4b0d1b35b_list;
static PyObject *const_str_plain_viewlim_low;
extern PyObject *const_tuple_int_pos_1_int_pos_1_tuple;
static PyObject *const_str_digest_5b0eeed3299ebc0371e700bda377eebd;
extern PyObject *const_int_pos_72;
extern PyObject *const_str_plain_arc_spine;
extern PyObject *const_str_plain_get_path;
static PyObject *const_str_plain_outward;
static PyObject *const_str_digest_b985a329b7dc801420c60351e0f03563;
extern PyObject *const_str_plain_Spine;
extern PyObject *const_str_plain_c;
static PyObject *const_tuple_17408f49714903231999582c0a2bd1e0_tuple;
extern PyObject *const_str_plain__height;
static PyObject *const_str_digest_c92d25a9b1a6742b310e8f13e2584d26;
static PyObject *const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple;
extern PyObject *const_str_plain_deprecated;
extern PyObject *const_str_plain_circle;
static PyObject *const_str_plain__patch_type;
extern PyObject *const_str_plain_zero;
static PyObject *const_str_plain_scaled_diameter;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_plain__adjust_location;
static PyObject *const_str_digest_16681fffc77eb6521237e4dc77c7912f;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain___str__;
extern PyObject *const_str_plain_tick2line;
extern PyObject *const_str_plain_tickdir;
extern PyObject *const_tuple_str_plain_right_str_plain_top_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_position_tuple;
extern PyObject *const_str_plain_unit_circle;
extern PyObject *const_str_digest_cdc8c7af9e77520aff3a80ecb90be995;
static PyObject *const_str_plain_set_patch_circle;
static PyObject *const_tuple_str_plain_arc_str_plain_circle_tuple;
static PyObject *const_tuple_str_plain_top_str_plain_bottom_tuple;
extern PyObject *const_str_plain_center;
extern PyObject *const_str_plain_allow_rasterization;
static PyObject *const_str_digest_8f8713570e1427948e1ca90180abc3e6;
extern PyObject *const_str_plain_axes;
extern PyObject *const_tuple_str_digest_50e4933a9d0fc470d2deeb63d403662b_tuple;
extern PyObject *const_str_plain_np;
static PyObject *const_str_digest_4ed0dc019602d7c679634d1b0f749d6d;
extern PyObject *const_str_plain_sort;
static PyObject *const_str_digest_ba50f011523bfdce32fc7d5bde850c78;
static PyObject *const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple;
static PyObject *const_str_digest_5341bccd4e191a713da7092029c69a6d;
static PyObject *const_str_digest_968c73a7ad9fd01fb4da32abe8c758cc;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_list_4ed07426722cc6107a2a6c9fea42842d_list;
extern PyObject *const_tuple_e32defe33cfa7402dfc80dee03c6f087_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_offset;
extern PyObject *const_str_plain_x1;
extern PyObject *const_str_plain_ret;
extern PyObject *const_tuple_int_0_int_neg_1_tuple;
static PyObject *const_str_digest_ed10b850583ad5f670e105ca391d49d2;
extern PyObject *const_str_plain_dpi_scale_trans;
extern PyObject *const_str_plain_offset_y;
static PyObject *const_str_digest_2f9f2cda6eace1794aa0f038d9da3819;
extern PyObject *const_tuple_str_plain_cbook_str_plain_docstring_str_plain_rcParams_tuple;
extern PyObject *const_str_plain_cls;
extern PyObject *const_tuple_int_0_int_0_tuple;
extern PyObject *const_str_plain_get_position;
static PyObject *const_str_digest_6c00ad45f826db1edf1142ca3256970d;
static PyObject *const_tuple_str_plain_axes_float_0_5_tuple;
extern PyObject *const_tuple_int_pos_1_int_0_tuple;
static PyObject *const_tuple_str_plain_outward_float_0_0_tuple;
extern PyObject *const_str_plain_convert_yunits;
extern PyObject *const_str_plain_Affine2D;
static PyObject *const_list_str_plain_bottom_list;
extern PyObject *const_str_digest_d28745151750b44e549d12f294293c52;
static PyObject *const_str_digest_d85de36ab700a79bc2609ad98fe19b50;
extern PyObject *const_str_plain__path;
extern PyObject *const_float_72_0;
static PyObject *const_str_digest_cb20ecd0d46462f137bfb2aae1246d58;
extern PyObject *const_str_plain_in;
extern PyObject *const_str_plain_line;
extern PyObject *const_str_plain_set_smart_bounds;
extern PyObject *const_str_plain_offset_x;
extern PyObject *const_str_plain_get_patch_transform;
extern PyObject *const_str_plain_mtransforms;
extern PyObject *const_str_plain_cbook;
extern PyObject *const_str_plain_convert_xunits;
extern PyObject *const_str_plain_viewLim;
extern PyObject *const_str_plain_minorTicks;
extern PyObject *const_list_str_plain_top_str_plain_bottom_list;
extern PyObject *const_str_plain_theta2;
extern PyObject *const_str_plain__tickdir;
static PyObject *const_str_digest_eedbb17a97575af3601940e525f2682d;
extern PyObject *const_str_digest_b4293af20113ad4e8388ca98e394ca7d;
static PyObject *const_str_digest_caf6751bbb2c810036d850768fe28a21;
extern PyObject *const_str_plain_set_facecolor;
extern PyObject *const_tuple_str_plain_bottom_str_plain_top_tuple;
extern PyObject *const_str_plain__width;
extern PyObject *const_str_plain_right;
extern PyObject *const_str_plain_radius;
extern PyObject *const_str_plain_bb;
static PyObject *const_str_digest_4b37bc7bee6b785e69831b81725236ec;
static PyObject *const_list_str_plain_outward_str_plain_axes_str_plain_data_list;
static PyObject *const_str_digest_1b1cfa91783d27298c896d931bf1a5cb;
static PyObject *const_str_plain_set_patch_arc;
static PyObject *const_str_plain_get_bounds;
extern PyObject *const_str_plain_get_xticks;
static PyObject *const_tuple_str_plain_axes_str_plain_outward_str_plain_data_tuple;
extern PyObject *const_str_plain_cla;
static PyObject *const_tuple_str_plain_projecting_tuple;
static PyObject *const_str_digest_4dbf58747abca490214bfaa2e0ede95c;
extern PyObject *const_str_plain__warn_external;
extern PyObject *const_str_plain_dataLim;
extern PyObject *const_str_plain_majorTicks;
static PyObject *const_tuple_616bdd7a14b48c15de56738ca685c90d_tuple;
extern PyObject *const_str_plain_axis;
extern PyObject *const_str_plain_width;
extern PyObject *const_str_plain_rmax;
static PyObject *const_tuple_float_2_5_tuple;
extern PyObject *const_str_plain_self;
static PyObject *const_list_str_plain_right_list;
static PyObject *const_str_digest_282263d2fa2adfc14d01396958fadf12;
extern PyObject *const_str_plain_IdentityTransform;
static PyObject *const_str_plain_bb0;
static PyObject *const_str_digest_18dd205cb77f09d4e383f56ea1ba901d;
extern PyObject *const_str_plain_amount;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_get_xaxis_transform;
extern PyObject *const_str_plain_linear_spine;
extern PyObject *const_str_plain__recompute_transform;
extern PyObject *const_str_plain_get_theta_direction;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_d91088e024bb306c7c9816e9c6e3ea41 = UNSTREAM_STRING_ASCII( &constant_bin[ 2468932 ], 29, 0 );
    const_str_digest_4eb5c8b8ae4c516a96c86250167f0763 = UNSTREAM_STRING_ASCII( &constant_bin[ 2468961 ], 23, 0 );
    const_str_digest_43a0d20a36c74a4f0cc7701ef5b07729 = UNSTREAM_STRING_ASCII( &constant_bin[ 2468984 ], 18, 0 );
    const_list_str_plain_left_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_left_list, 0, const_str_plain_left ); Py_INCREF( const_str_plain_left );
    const_tuple_str_plain_allow_rasterization_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_allow_rasterization_tuple, 0, const_str_plain_allow_rasterization ); Py_INCREF( const_str_plain_allow_rasterization );
    const_tuple_87ed802b88cc6ec2549178254512f498_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_87ed802b88cc6ec2549178254512f498_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_87ed802b88cc6ec2549178254512f498_tuple, 1, const_str_plain_center ); Py_INCREF( const_str_plain_center );
    PyTuple_SET_ITEM( const_tuple_87ed802b88cc6ec2549178254512f498_tuple, 2, const_str_plain_radius ); Py_INCREF( const_str_plain_radius );
    PyTuple_SET_ITEM( const_tuple_87ed802b88cc6ec2549178254512f498_tuple, 3, const_str_plain_theta1 ); Py_INCREF( const_str_plain_theta1 );
    PyTuple_SET_ITEM( const_tuple_87ed802b88cc6ec2549178254512f498_tuple, 4, const_str_plain_theta2 ); Py_INCREF( const_str_plain_theta2 );
    const_str_plain_tickl = UNSTREAM_STRING_ASCII( &constant_bin[ 1385094 ], 5, 1 );
    const_str_digest_d1d094a2ccd8ffcb8a48b5737424aa70 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469002 ], 25, 0 );
    const_str_digest_4cde890927cc2b88beac2083dde55dbb = UNSTREAM_STRING_ASCII( &constant_bin[ 2469027 ], 26, 0 );
    const_str_digest_d7df2bc1ee2d5e920ce5e74ee0a59514 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469053 ], 42, 0 );
    const_str_plain_offset_vec = UNSTREAM_STRING_ASCII( &constant_bin[ 2469095 ], 10, 1 );
    const_str_digest_05884c283e9966b055226c4ef8509afc = UNSTREAM_STRING_ASCII( &constant_bin[ 2469105 ], 22, 0 );
    const_str_digest_60b6c7d6206290ea6562a9ab363af740 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469127 ], 26, 0 );
    const_str_digest_ec2beb12f88ffcdb17573ee7e2632e33 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469153 ], 52, 0 );
    const_str_digest_bdb87b76fac327390dcf9d24b80e6456 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469205 ], 259, 0 );
    const_str_digest_ab1bc000ced809648b71ee9f8efb43ad = UNSTREAM_STRING_ASCII( &constant_bin[ 2469464 ], 65, 0 );
    const_str_digest_acebd7f4ec0b9d1cad545899e69bd71b = UNSTREAM_STRING_ASCII( &constant_bin[ 2469529 ], 10, 0 );
    const_str_digest_4e850d568760852e97a1610f6e76f79c = UNSTREAM_STRING_ASCII( &constant_bin[ 2469539 ], 58, 0 );
    const_str_plain_data_xform = UNSTREAM_STRING_ASCII( &constant_bin[ 2469597 ], 10, 1 );
    const_str_digest_e625b20cc5b20388c7b371959b1ae678 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469607 ], 54, 0 );
    const_str_digest_470b7b4df7b47adb1238b6037fa4cd0a = UNSTREAM_STRING_ASCII( &constant_bin[ 2469661 ], 24, 0 );
    const_str_digest_63927bfa0c43c549c9c79521fa550712 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469685 ], 28, 0 );
    const_str_plain_datalim_high = UNSTREAM_STRING_ASCII( &constant_bin[ 2469713 ], 12, 1 );
    const_str_plain__theta2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2277940 ], 7, 1 );
    const_str_digest_c85d08013ec811690755b19130a310a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469725 ], 197, 0 );
    const_tuple_float_0_0_float_0_999_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_999_tuple, 0, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    const_float_0_999 = UNSTREAM_FLOAT( &constant_bin[ 2469922 ] );
    PyTuple_SET_ITEM( const_tuple_float_0_0_float_0_999_tuple, 1, const_float_0_999 ); Py_INCREF( const_float_0_999 );
    const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 1, const_str_plain_position ); Py_INCREF( const_str_plain_position );
    const_str_plain_position_type = UNSTREAM_STRING_ASCII( &constant_bin[ 2469930 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 2, const_str_plain_position_type ); Py_INCREF( const_str_plain_position_type );
    PyTuple_SET_ITEM( const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 3, const_str_plain_amount ); Py_INCREF( const_str_plain_amount );
    PyTuple_SET_ITEM( const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 4, const_str_plain_offset_vec ); Py_INCREF( const_str_plain_offset_vec );
    PyTuple_SET_ITEM( const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 5, const_str_plain_offset_x ); Py_INCREF( const_str_plain_offset_x );
    PyTuple_SET_ITEM( const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 6, const_str_plain_offset_y ); Py_INCREF( const_str_plain_offset_y );
    const_str_digest_5aefd65bb28639ccb724edefbb95e1a5 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469943 ], 15, 0 );
    const_str_plain_how = UNSTREAM_STRING_ASCII( &constant_bin[ 18048 ], 3, 1 );
    const_tuple_ab78564a503682fe20f7d635d6477ed0_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_ab78564a503682fe20f7d635d6477ed0_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_ab78564a503682fe20f7d635d6477ed0_tuple, 1, const_str_plain_renderer ); Py_INCREF( const_str_plain_renderer );
    PyTuple_SET_ITEM( const_tuple_ab78564a503682fe20f7d635d6477ed0_tuple, 2, const_str_plain_ret ); Py_INCREF( const_str_plain_ret );
    PyTuple_SET_ITEM( const_tuple_ab78564a503682fe20f7d635d6477ed0_tuple, 3, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_7346bb7bbc2b801fa2a2b8257b91943c = UNSTREAM_STRING_ASCII( &constant_bin[ 2469958 ], 18, 0 );
    const_str_digest_3e39b8a66b612e3cde13c3b6246d1d12 = UNSTREAM_STRING_ASCII( &constant_bin[ 2469976 ], 28, 0 );
    const_str_plain_tickstocheck = UNSTREAM_STRING_ASCII( &constant_bin[ 2470004 ], 12, 1 );
    const_str_digest_74aa8353e4cb4da0043cf2dbbba91ba7 = UNSTREAM_STRING_ASCII( &constant_bin[ 2470016 ], 14, 0 );
    const_str_plain_padout = UNSTREAM_STRING_ASCII( &constant_bin[ 2470030 ], 6, 1 );
    const_tuple_float_0_999_float_1_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_float_0_999_float_1_0_tuple, 0, const_float_0_999 ); Py_INCREF( const_float_0_999 );
    PyTuple_SET_ITEM( const_tuple_float_0_999_float_1_0_tuple, 1, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    const_str_plain_base_transform = UNSTREAM_STRING_ASCII( &constant_bin[ 2470036 ], 14, 1 );
    const_str_plain_set_patch_line = UNSTREAM_STRING_ASCII( &constant_bin[ 2470050 ], 14, 1 );
    const_str_plain__spine_transform = UNSTREAM_STRING_ASCII( &constant_bin[ 2470064 ], 16, 1 );
    const_str_digest_dd619daddaa7d6aac3f692e9458ec6a1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2470080 ], 29, 0 );
    const_str_digest_9c77d829d34c9c75bf5eee383da79b08 = UNSTREAM_STRING_ASCII( &constant_bin[ 2470109 ], 330, 0 );
    const_str_plain_is_frame_like = UNSTREAM_STRING_ASCII( &constant_bin[ 2470439 ], 13, 1 );
    const_str_digest_c0a88f9552b5ec7f51197257738e17a1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2470452 ], 18, 0 );
    const_str_digest_97dbc6b904ee2c21b17f694d05e99ac4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2470470 ], 29, 0 );
    const_str_digest_dbd58e05dc84889d5ad7cf580b405b20 = UNSTREAM_STRING_ASCII( &constant_bin[ 2470499 ], 19, 0 );
    const_str_digest_cbff2e8d0e23c4ff96229c43d46e8c71 = UNSTREAM_STRING_ASCII( &constant_bin[ 2470518 ], 183, 0 );
    const_str_plain_tickvals = UNSTREAM_STRING_ASCII( &constant_bin[ 2470701 ], 8, 1 );
    const_tuple_str_plain_data_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_data_int_0_tuple, 0, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_str_plain_data_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_digest_57cacffc3b78985b437104b8d0e0907f = UNSTREAM_STRING_ASCII( &constant_bin[ 2470709 ], 35, 0 );
    const_str_digest_f2cbfcf5028382197dc3153734a1c6cd = UNSTREAM_STRING_ASCII( &constant_bin[ 2470744 ], 25, 0 );
    const_str_digest_879be27065d0bc95273e5f3bff67279d = UNSTREAM_STRING_ASCII( &constant_bin[ 2470769 ], 32, 0 );
    const_str_digest_a40723ed11e4414a2998ff867a896a6d = UNSTREAM_STRING_ASCII( &constant_bin[ 2470801 ], 723, 0 );
    const_str_digest_0509ad98bae8d5faed4f9802c9149ef1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471334 ], 20, 0 );
    const_str_plain__ensure_position_is_set = UNSTREAM_STRING_ASCII( &constant_bin[ 2468938 ], 23, 1 );
    const_tuple_51b7a56d41bf9c59329d657db55def47_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_51b7a56d41bf9c59329d657db55def47_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_51b7a56d41bf9c59329d657db55def47_tuple, 1, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    const_str_plain_spine_type = UNSTREAM_STRING_ASCII( &constant_bin[ 2471524 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_51b7a56d41bf9c59329d657db55def47_tuple, 2, const_str_plain_spine_type ); Py_INCREF( const_str_plain_spine_type );
    PyTuple_SET_ITEM( const_tuple_51b7a56d41bf9c59329d657db55def47_tuple, 3, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_51b7a56d41bf9c59329d657db55def47_tuple, 4, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_51b7a56d41bf9c59329d657db55def47_tuple, 5, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_list_f8554cd76988f9d6d7371cacaa6c6a26_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_f8554cd76988f9d6d7371cacaa6c6a26_list, 0, const_tuple_float_0_0_float_0_999_tuple ); Py_INCREF( const_tuple_float_0_0_float_0_999_tuple );
    PyList_SET_ITEM( const_list_f8554cd76988f9d6d7371cacaa6c6a26_list, 1, const_tuple_float_0_0_float_0_999_tuple ); Py_INCREF( const_tuple_float_0_0_float_0_999_tuple );
    const_tuple_float_1_0_float_0_999_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_999_tuple, 0, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_float_0_999_tuple, 1, const_float_0_999 ); Py_INCREF( const_float_0_999 );
    const_tuple_str_plain_self_str_plain_center_str_plain_radius_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_center_str_plain_radius_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_center_str_plain_radius_tuple, 1, const_str_plain_center ); Py_INCREF( const_str_plain_center );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_center_str_plain_radius_tuple, 2, const_str_plain_radius ); Py_INCREF( const_str_plain_radius );
    const_str_digest_98873532fb91af52ecaeed111a5dfadd = UNSTREAM_STRING_ASCII( &constant_bin[ 2471534 ], 34, 0 );
    const_list_str_plain_bottom_str_plain_top_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_bottom_str_plain_top_list, 0, const_str_plain_bottom ); Py_INCREF( const_str_plain_bottom );
    PyList_SET_ITEM( const_list_str_plain_bottom_str_plain_top_list, 1, const_str_plain_top ); Py_INCREF( const_str_plain_top );
    const_str_digest_33812b40370aee00bde878bebcde1dfb = UNSTREAM_STRING_ASCII( &constant_bin[ 2471568 ], 19, 0 );
    const_str_digest_3ea6191f8e6badb08a6c198cb3f8b746 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471587 ], 28, 0 );
    const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 1, const_str_plain_renderer ); Py_INCREF( const_str_plain_renderer );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 2, const_str_plain_bb ); Py_INCREF( const_str_plain_bb );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 3, const_str_plain_bboxes ); Py_INCREF( const_str_plain_bboxes );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 4, const_str_plain_tickstocheck ); Py_INCREF( const_str_plain_tickstocheck );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 5, const_str_plain_tick ); Py_INCREF( const_str_plain_tick );
    const_str_plain_bb0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1368610 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 6, const_str_plain_bb0 ); Py_INCREF( const_str_plain_bb0 );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 7, const_str_plain_tickl ); Py_INCREF( const_str_plain_tickl );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 8, const_str_plain_tickdir ); Py_INCREF( const_str_plain_tickdir );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 9, const_str_plain_padout ); Py_INCREF( const_str_plain_padout );
    const_str_plain_padin = UNSTREAM_STRING_ASCII( &constant_bin[ 2460102 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 10, const_str_plain_padin ); Py_INCREF( const_str_plain_padin );
    PyTuple_SET_ITEM( const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 11, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_f3047b8c15cc12f7c448de553e890bfd = UNSTREAM_STRING_ASCII( &constant_bin[ 2471615 ], 55, 0 );
    const_dict_75f1ed2738ae304e7e502ba9973de3b5 = _PyDict_NewPresized( 4 );
    const_tuple_int_neg_1_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_neg_1_int_0_tuple, 0, const_int_neg_1 ); Py_INCREF( const_int_neg_1 );
    PyTuple_SET_ITEM( const_tuple_int_neg_1_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    PyDict_SetItem( const_dict_75f1ed2738ae304e7e502ba9973de3b5, const_str_plain_left, const_tuple_int_neg_1_int_0_tuple );
    PyDict_SetItem( const_dict_75f1ed2738ae304e7e502ba9973de3b5, const_str_plain_right, const_tuple_int_pos_1_int_0_tuple );
    PyDict_SetItem( const_dict_75f1ed2738ae304e7e502ba9973de3b5, const_str_plain_bottom, const_tuple_int_0_int_neg_1_tuple );
    PyDict_SetItem( const_dict_75f1ed2738ae304e7e502ba9973de3b5, const_str_plain_top, const_tuple_int_0_int_pos_1_tuple );
    assert( PyDict_Size( const_dict_75f1ed2738ae304e7e502ba9973de3b5 ) == 4 );
    const_list_str_plain_top_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_top_list, 0, const_str_plain_top ); Py_INCREF( const_str_plain_top );
    const_str_digest_4d5fff89f9803dc4e2d1dfb63dc84c28 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471670 ], 26, 0 );
    const_str_digest_3f6a3a283f4f6d95d2c969fadb020cd2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471696 ], 9, 0 );
    const_tuple_str_digest_18dd205cb77f09d4e383f56ea1ba901d_tuple = PyTuple_New( 1 );
    const_str_digest_18dd205cb77f09d4e383f56ea1ba901d = UNSTREAM_STRING_ASCII( &constant_bin[ 2471705 ], 25, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_18dd205cb77f09d4e383f56ea1ba901d_tuple, 0, const_str_digest_18dd205cb77f09d4e383f56ea1ba901d ); Py_INCREF( const_str_digest_18dd205cb77f09d4e383f56ea1ba901d );
    const_str_digest_b3cc18b655aab4953fc3abb35d5e21ed = UNSTREAM_STRING_ASCII( &constant_bin[ 2471730 ], 16, 0 );
    const_tuple_str_plain_center_str_plain_zero_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_center_str_plain_zero_tuple, 0, const_str_plain_center ); Py_INCREF( const_str_plain_center );
    PyTuple_SET_ITEM( const_tuple_str_plain_center_str_plain_zero_tuple, 1, const_str_plain_zero ); Py_INCREF( const_str_plain_zero );
    const_str_plain_viewlim_high = UNSTREAM_STRING_ASCII( &constant_bin[ 2471746 ], 12, 1 );
    const_str_digest_d12962331895cfa026fce7746c626985 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471758 ], 15, 0 );
    const_tuple_float_0_999_float_0_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_float_0_999_float_0_0_tuple, 0, const_float_0_999 ); Py_INCREF( const_float_0_999 );
    PyTuple_SET_ITEM( const_tuple_float_0_999_float_0_0_tuple, 1, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    const_str_digest_e8221eb249e2a624dc2a35c5cdce05c5 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471773 ], 67, 0 );
    const_str_plain__theta1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2289189 ], 7, 1 );
    const_str_plain__calc_offset_transform = UNSTREAM_STRING_ASCII( &constant_bin[ 2469691 ], 22, 1 );
    const_str_plain_datalim_low = UNSTREAM_STRING_ASCII( &constant_bin[ 2471840 ], 11, 1 );
    const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple, 1, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    PyTuple_SET_ITEM( const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple, 2, const_str_plain_spine_type ); Py_INCREF( const_str_plain_spine_type );
    PyTuple_SET_ITEM( const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple, 4, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple, 5, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    const_tuple_str_digest_4cde890927cc2b88beac2083dde55dbb_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4cde890927cc2b88beac2083dde55dbb_tuple, 0, const_str_digest_4cde890927cc2b88beac2083dde55dbb ); Py_INCREF( const_str_digest_4cde890927cc2b88beac2083dde55dbb );
    const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 1, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 2, const_str_plain_spine_type ); Py_INCREF( const_str_plain_spine_type );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 3, const_str_plain_center ); Py_INCREF( const_str_plain_center );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 4, const_str_plain_radius ); Py_INCREF( const_str_plain_radius );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 5, const_str_plain_theta1 ); Py_INCREF( const_str_plain_theta1 );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 6, const_str_plain_theta2 ); Py_INCREF( const_str_plain_theta2 );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 7, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 8, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 9, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    const_str_digest_07e50c74d2c6c2d4a635225d2f41a719 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471851 ], 22, 0 );
    const_list_6286e504ff565f6e3366f4d017b6e11f_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_6286e504ff565f6e3366f4d017b6e11f_list, 0, const_tuple_float_0_999_float_1_0_tuple ); Py_INCREF( const_tuple_float_0_999_float_1_0_tuple );
    PyList_SET_ITEM( const_list_6286e504ff565f6e3366f4d017b6e11f_list, 1, const_tuple_float_0_999_float_1_0_tuple ); Py_INCREF( const_tuple_float_0_999_float_1_0_tuple );
    const_list_cbfdd3127f278d92c67096f4b0d1b35b_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_cbfdd3127f278d92c67096f4b0d1b35b_list, 0, const_tuple_float_1_0_float_0_999_tuple ); Py_INCREF( const_tuple_float_1_0_float_0_999_tuple );
    PyList_SET_ITEM( const_list_cbfdd3127f278d92c67096f4b0d1b35b_list, 1, const_tuple_float_1_0_float_0_999_tuple ); Py_INCREF( const_tuple_float_1_0_float_0_999_tuple );
    const_str_plain_viewlim_low = UNSTREAM_STRING_ASCII( &constant_bin[ 2471873 ], 11, 1 );
    const_str_digest_5b0eeed3299ebc0371e700bda377eebd = UNSTREAM_STRING_ASCII( &constant_bin[ 2471884 ], 16, 0 );
    const_str_plain_outward = UNSTREAM_STRING_ASCII( &constant_bin[ 2469569 ], 7, 1 );
    const_str_digest_b985a329b7dc801420c60351e0f03563 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471900 ], 20, 0 );
    const_tuple_17408f49714903231999582c0a2bd1e0_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_17408f49714903231999582c0a2bd1e0_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_17408f49714903231999582c0a2bd1e0_tuple, 1, const_str_plain_what ); Py_INCREF( const_str_plain_what );
    PyTuple_SET_ITEM( const_tuple_17408f49714903231999582c0a2bd1e0_tuple, 2, const_str_plain_how ); Py_INCREF( const_str_plain_how );
    PyTuple_SET_ITEM( const_tuple_17408f49714903231999582c0a2bd1e0_tuple, 3, const_str_plain_data_xform ); Py_INCREF( const_str_plain_data_xform );
    PyTuple_SET_ITEM( const_tuple_17408f49714903231999582c0a2bd1e0_tuple, 4, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_17408f49714903231999582c0a2bd1e0_tuple, 5, const_str_plain_base_transform ); Py_INCREF( const_str_plain_base_transform );
    const_str_digest_c92d25a9b1a6742b310e8f13e2584d26 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471920 ], 625, 0 );
    const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple = PyTuple_New( 17 );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 1, const_str_plain_low ); Py_INCREF( const_str_plain_low );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 2, const_str_plain_high ); Py_INCREF( const_str_plain_high );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 3, const_str_plain_viewlim_low ); Py_INCREF( const_str_plain_viewlim_low );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 4, const_str_plain_viewlim_high ); Py_INCREF( const_str_plain_viewlim_high );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 5, const_str_plain_datalim_low ); Py_INCREF( const_str_plain_datalim_low );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 6, const_str_plain_datalim_high ); Py_INCREF( const_str_plain_datalim_high );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 7, const_str_plain_ticks ); Py_INCREF( const_str_plain_ticks );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 8, const_str_plain_cond ); Py_INCREF( const_str_plain_cond );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 9, const_str_plain_tickvals ); Py_INCREF( const_str_plain_tickvals );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 10, const_str_plain_direction ); Py_INCREF( const_str_plain_direction );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 11, const_str_plain_offset ); Py_INCREF( const_str_plain_offset );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 12, const_str_plain_rmin ); Py_INCREF( const_str_plain_rmin );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 13, const_str_plain_rmax ); Py_INCREF( const_str_plain_rmax );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 14, const_str_plain_rorigin ); Py_INCREF( const_str_plain_rorigin );
    const_str_plain_scaled_diameter = UNSTREAM_STRING_ASCII( &constant_bin[ 2472545 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 15, const_str_plain_scaled_diameter ); Py_INCREF( const_str_plain_scaled_diameter );
    PyTuple_SET_ITEM( const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 16, const_str_plain_v1 ); Py_INCREF( const_str_plain_v1 );
    const_str_plain__patch_type = UNSTREAM_STRING_ASCII( &constant_bin[ 2472560 ], 11, 1 );
    const_str_plain__adjust_location = UNSTREAM_STRING_ASCII( &constant_bin[ 2472571 ], 16, 1 );
    const_str_digest_16681fffc77eb6521237e4dc77c7912f = UNSTREAM_STRING_ASCII( &constant_bin[ 2472587 ], 26, 0 );
    const_str_plain_set_patch_circle = UNSTREAM_STRING_ASCII( &constant_bin[ 2469111 ], 16, 1 );
    const_tuple_str_plain_arc_str_plain_circle_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_arc_str_plain_circle_tuple, 0, const_str_plain_arc ); Py_INCREF( const_str_plain_arc );
    PyTuple_SET_ITEM( const_tuple_str_plain_arc_str_plain_circle_tuple, 1, const_str_plain_circle ); Py_INCREF( const_str_plain_circle );
    const_tuple_str_plain_top_str_plain_bottom_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_top_str_plain_bottom_tuple, 0, const_str_plain_top ); Py_INCREF( const_str_plain_top );
    PyTuple_SET_ITEM( const_tuple_str_plain_top_str_plain_bottom_tuple, 1, const_str_plain_bottom ); Py_INCREF( const_str_plain_bottom );
    const_str_digest_8f8713570e1427948e1ca90180abc3e6 = UNSTREAM_STRING_ASCII( &constant_bin[ 2472613 ], 62, 0 );
    const_str_digest_4ed0dc019602d7c679634d1b0f749d6d = UNSTREAM_STRING_ASCII( &constant_bin[ 2472675 ], 38, 0 );
    const_str_digest_ba50f011523bfdce32fc7d5bde850c78 = UNSTREAM_STRING_ASCII( &constant_bin[ 2472713 ], 53, 0 );
    const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 1, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 2, const_str_plain_center ); Py_INCREF( const_str_plain_center );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 3, const_str_plain_radius ); Py_INCREF( const_str_plain_radius );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 4, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 5, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 6, const_str_plain_spine_type ); Py_INCREF( const_str_plain_spine_type );
    PyTuple_SET_ITEM( const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 7, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    const_str_digest_5341bccd4e191a713da7092029c69a6d = UNSTREAM_STRING_ASCII( &constant_bin[ 2472766 ], 14, 0 );
    const_str_digest_968c73a7ad9fd01fb4da32abe8c758cc = UNSTREAM_STRING_ASCII( &constant_bin[ 2472780 ], 27, 0 );
    const_list_4ed07426722cc6107a2a6c9fea42842d_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_4ed07426722cc6107a2a6c9fea42842d_list, 0, const_tuple_float_0_999_float_0_0_tuple ); Py_INCREF( const_tuple_float_0_999_float_0_0_tuple );
    PyList_SET_ITEM( const_list_4ed07426722cc6107a2a6c9fea42842d_list, 1, const_tuple_float_0_999_float_0_0_tuple ); Py_INCREF( const_tuple_float_0_999_float_0_0_tuple );
    const_str_digest_ed10b850583ad5f670e105ca391d49d2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2471451 ], 19, 0 );
    const_str_digest_2f9f2cda6eace1794aa0f038d9da3819 = UNSTREAM_STRING_ASCII( &constant_bin[ 2472807 ], 22, 0 );
    const_str_digest_6c00ad45f826db1edf1142ca3256970d = UNSTREAM_STRING_ASCII( &constant_bin[ 2472829 ], 52, 0 );
    const_tuple_str_plain_axes_float_0_5_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_float_0_5_tuple, 0, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_float_0_5_tuple, 1, const_float_0_5 ); Py_INCREF( const_float_0_5 );
    const_tuple_str_plain_outward_float_0_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_outward_float_0_0_tuple, 0, const_str_plain_outward ); Py_INCREF( const_str_plain_outward );
    PyTuple_SET_ITEM( const_tuple_str_plain_outward_float_0_0_tuple, 1, const_float_0_0 ); Py_INCREF( const_float_0_0 );
    const_list_str_plain_bottom_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_bottom_list, 0, const_str_plain_bottom ); Py_INCREF( const_str_plain_bottom );
    const_str_digest_d85de36ab700a79bc2609ad98fe19b50 = UNSTREAM_STRING_ASCII( &constant_bin[ 2472881 ], 22, 0 );
    const_str_digest_cb20ecd0d46462f137bfb2aae1246d58 = UNSTREAM_STRING_ASCII( &constant_bin[ 2472903 ], 13, 0 );
    const_str_digest_eedbb17a97575af3601940e525f2682d = UNSTREAM_STRING_ASCII( &constant_bin[ 2472916 ], 20, 0 );
    const_str_digest_caf6751bbb2c810036d850768fe28a21 = UNSTREAM_STRING_ASCII( &constant_bin[ 2472936 ], 27, 0 );
    const_str_digest_4b37bc7bee6b785e69831b81725236ec = UNSTREAM_STRING_ASCII( &constant_bin[ 2472963 ], 232, 0 );
    const_list_str_plain_outward_str_plain_axes_str_plain_data_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_outward_str_plain_axes_str_plain_data_list, 0, const_str_plain_outward ); Py_INCREF( const_str_plain_outward );
    PyList_SET_ITEM( const_list_str_plain_outward_str_plain_axes_str_plain_data_list, 1, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    PyList_SET_ITEM( const_list_str_plain_outward_str_plain_axes_str_plain_data_list, 2, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    const_str_digest_1b1cfa91783d27298c896d931bf1a5cb = UNSTREAM_STRING_ASCII( &constant_bin[ 2473195 ], 50, 0 );
    const_str_plain_set_patch_arc = UNSTREAM_STRING_ASCII( &constant_bin[ 2471457 ], 13, 1 );
    const_str_plain_get_bounds = UNSTREAM_STRING_ASCII( &constant_bin[ 2471890 ], 10, 1 );
    const_tuple_str_plain_axes_str_plain_outward_str_plain_data_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_str_plain_outward_str_plain_data_tuple, 0, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_str_plain_outward_str_plain_data_tuple, 1, const_str_plain_outward ); Py_INCREF( const_str_plain_outward );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_str_plain_outward_str_plain_data_tuple, 2, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    const_tuple_str_plain_projecting_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_projecting_tuple, 0, const_str_plain_projecting ); Py_INCREF( const_str_plain_projecting );
    const_str_digest_4dbf58747abca490214bfaa2e0ede95c = UNSTREAM_STRING_ASCII( &constant_bin[ 2473245 ], 15, 0 );
    const_tuple_616bdd7a14b48c15de56738ca685c90d_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_616bdd7a14b48c15de56738ca685c90d_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_616bdd7a14b48c15de56738ca685c90d_tuple, 1, const_str_plain_position ); Py_INCREF( const_str_plain_position );
    PyTuple_SET_ITEM( const_tuple_616bdd7a14b48c15de56738ca685c90d_tuple, 2, const_str_plain_position_type ); Py_INCREF( const_str_plain_position_type );
    PyTuple_SET_ITEM( const_tuple_616bdd7a14b48c15de56738ca685c90d_tuple, 3, const_str_plain_amount ); Py_INCREF( const_str_plain_amount );
    const_tuple_float_2_5_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_float_2_5_tuple, 0, const_float_2_5 ); Py_INCREF( const_float_2_5 );
    const_list_str_plain_right_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_right_list, 0, const_str_plain_right ); Py_INCREF( const_str_plain_right );
    const_str_digest_282263d2fa2adfc14d01396958fadf12 = UNSTREAM_STRING_ASCII( &constant_bin[ 2473260 ], 28, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$spines( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_4b24228e9d2116c3af2043f8b60ce364;
static PyCodeObject *codeobj_116eaf6e7626099dd562e6a26b48f471;
static PyCodeObject *codeobj_653fe4632b0f675c9ad3a6b8c195a957;
static PyCodeObject *codeobj_7f1429bc54e5af598aab3ee86e64677f;
static PyCodeObject *codeobj_1732b7288de3ff07b5d0689f845f9ced;
static PyCodeObject *codeobj_dbd9f46b071cbd449b8d1631eb33a7a8;
static PyCodeObject *codeobj_c5bd117846bf6461fd37167d3813bbb2;
static PyCodeObject *codeobj_aab338e1afcbcb75c3d4c51af7442c54;
static PyCodeObject *codeobj_0676f38a2d415a197c296686599259ee;
static PyCodeObject *codeobj_b2f602459ae7ea8ad94c075daf334294;
static PyCodeObject *codeobj_7bd8e10ceae9ecfcf087123d499c9345;
static PyCodeObject *codeobj_ae20ba00c2eef183aa7a8fb216a670a8;
static PyCodeObject *codeobj_28f24e50057ecf6d6e80b1094bc189f0;
static PyCodeObject *codeobj_d27c36f537f61b2b493b1861184ecc05;
static PyCodeObject *codeobj_7b36e0ef0cedb5b112ddf64accbe3ba5;
static PyCodeObject *codeobj_780f5a3a09e3de88922cbb1fe3569671;
static PyCodeObject *codeobj_06a62e70be890a801744ddcdb68e42fa;
static PyCodeObject *codeobj_48a91bc7af0ddec1974fbdd87958d538;
static PyCodeObject *codeobj_6523a8fda21c49e09c95926afd905246;
static PyCodeObject *codeobj_bc7672b1c966a11cfbfcd3b29487a2e2;
static PyCodeObject *codeobj_8f718093ca383841ea25feb47f13427b;
static PyCodeObject *codeobj_4e9246b0c43ceae3e3f16f0c7488584e;
static PyCodeObject *codeobj_4f6e7d92b0ec0a548368184a30d4bb66;
static PyCodeObject *codeobj_d7af239eeca0da49d6a73d3836f4902b;
static PyCodeObject *codeobj_6c4f3735db0bf97914eedcbb2abda000;
static PyCodeObject *codeobj_d26c04bf29cf1499b04ca9ff5aa2efe5;
static PyCodeObject *codeobj_3e650edf4f100d9bb9124531d6ae97a4;
static PyCodeObject *codeobj_b45baa0ce76614000fd72883197e7273;
static PyCodeObject *codeobj_f7cd8877f5a6d65e18c1760d0e79da16;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_b985a329b7dc801420c60351e0f03563 );
    codeobj_4b24228e9d2116c3af2043f8b60ce364 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_16681fffc77eb6521237e4dc77c7912f, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_116eaf6e7626099dd562e6a26b48f471 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Spine, 11, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_653fe4632b0f675c9ad3a6b8c195a957 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 34, const_tuple_51b7a56d41bf9c59329d657db55def47_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_7f1429bc54e5af598aab3ee86e64677f = MAKE_CODEOBJ( module_filename_obj, const_str_plain___str__, 31, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1732b7288de3ff07b5d0689f845f9ced = MAKE_CODEOBJ( module_filename_obj, const_str_plain__adjust_location, 254, const_tuple_e02e6775a371a0a532df6fb5da45683b_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dbd9f46b071cbd449b8d1631eb33a7a8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__calc_offset_transform, 369, const_tuple_6ad90fc2afca74ec5b925378bcc28d5c_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c5bd117846bf6461fd37167d3813bbb2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__ensure_position_is_set, 208, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_aab338e1afcbcb75c3d4c51af7442c54 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__recompute_transform, 125, const_tuple_e32defe33cfa7402dfc80dee03c6f087_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0676f38a2d415a197c296686599259ee = MAKE_CODEOBJ( module_filename_obj, const_str_plain_arc_spine, 557, const_tuple_37e40ca625ddaf4682bad5055e26a8c4_tuple, 7, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_b2f602459ae7ea8ad94c075daf334294 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_circular_spine, 568, const_tuple_3fa20a0aabaf7ab4e4e11f81d8999cba_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_7bd8e10ceae9ecfcf087123d499c9345 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cla, 226, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ae20ba00c2eef183aa7a8fb216a670a8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_draw, 362, const_tuple_ab78564a503682fe20f7d635d6477ed0_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_28f24e50057ecf6d6e80b1094bc189f0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_bounds, 532, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d27c36f537f61b2b493b1861184ecc05 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_patch_transform, 140, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_7b36e0ef0cedb5b112ddf64accbe3ba5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_path, 205, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_780f5a3a09e3de88922cbb1fe3569671 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_position, 482, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_06a62e70be890a801744ddcdb68e42fa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_smart_bounds, 92, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_48a91bc7af0ddec1974fbdd87958d538 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_spine_transform, 487, const_tuple_17408f49714903231999582c0a2bd1e0_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6523a8fda21c49e09c95926afd905246 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_window_extent, 147, const_tuple_f8f23fb5d380a611f64e07bdcffc8372_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_bc7672b1c966a11cfbfcd3b29487a2e2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_frame_like, 232, const_tuple_616bdd7a14b48c15de56738ca685c90d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8f718093ca383841ea25feb47f13427b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_linear_spine, 536, const_tuple_19adc52a83d59b2d7b0c2177530a308d_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_4e9246b0c43ceae3e3f16f0c7488584e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_register_axis, 214, const_tuple_str_plain_self_str_plain_axis_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4f6e7d92b0ec0a548368184a30d4bb66 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_bounds, 524, const_tuple_str_plain_self_str_plain_low_str_plain_high_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d7af239eeca0da49d6a73d3836f4902b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_color, 579, const_tuple_str_plain_self_str_plain_c_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6c4f3735db0bf97914eedcbb2abda000 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_patch_arc, 96, const_tuple_87ed802b88cc6ec2549178254512f498_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d26c04bf29cf1499b04ca9ff5aa2efe5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_patch_circle, 109, const_tuple_str_plain_self_str_plain_center_str_plain_radius_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3e650edf4f100d9bb9124531d6ae97a4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_patch_line, 119, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b45baa0ce76614000fd72883197e7273 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_position, 443, const_tuple_str_plain_self_str_plain_position_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f7cd8877f5a6d65e18c1760d0e79da16 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_smart_bounds, 81, const_tuple_str_plain_self_str_plain_value_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_2_complex_call_helper_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_10_get_window_extent( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_11_get_path(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_12__ensure_position_is_set(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_13_register_axis(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_14_cla(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_15_is_frame_like(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_16__adjust_location(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_17_draw(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_18__calc_offset_transform(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_19_set_position(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_1___str__(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_20_get_position(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_21_get_spine_transform(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_22_set_bounds(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_23_get_bounds(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_24_linear_spine(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_25_arc_spine(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_26_circular_spine(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_27_set_color(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_2___init__(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_3_set_smart_bounds(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_4_get_smart_bounds(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_5_set_patch_arc(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_6_set_patch_circle(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_7_set_patch_line(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_8__recompute_transform(  );


static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_9_get_patch_transform(  );


// The module function definitions.
static PyObject *impl_matplotlib$spines$$$function_1___str__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_str_plain_Spine;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_1___str__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_1___str__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_2___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_axes = python_pars[ 1 ];
    PyObject *par_spine_type = python_pars[ 2 ];
    PyObject *par_path = python_pars[ 3 ];
    PyObject *par_kwargs = python_pars[ 4 ];
    struct Nuitka_FrameObject *frame_653fe4632b0f675c9ad3a6b8c195a957;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_653fe4632b0f675c9ad3a6b8c195a957 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_653fe4632b0f675c9ad3a6b8c195a957, codeobj_653fe4632b0f675c9ad3a6b8c195a957, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_653fe4632b0f675c9ad3a6b8c195a957 = cache_frame_653fe4632b0f675c9ad3a6b8c195a957;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_653fe4632b0f675c9ad3a6b8c195a957 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_653fe4632b0f675c9ad3a6b8c195a957 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg2_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_call_result_1 = impl___internal__$$$function_2_complex_call_helper_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_axes );
        tmp_assattr_name_1 = par_axes;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_axes, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_set_figure );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_axes );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 46;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_figure );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 46;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_spine_type );
        tmp_assattr_name_2 = par_spine_type;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_spine_type, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_3;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 48;
        tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_set_facecolor, &PyTuple_GET_ITEM( const_tuple_str_plain_none_tuple, 0 ) );

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_set_edgecolor );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 49;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        tmp_subscript_name_1 = const_str_digest_d68f16e49ad8add4cd6ce282eb14b4a7;
        tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 49;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 49;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_set_linewidth );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_2 = tmp_mvar_value_2;
        tmp_subscript_name_2 = const_str_digest_2076eb8266023786dfd5dac58b1cb6de;
        tmp_args_element_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 50;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 50;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_6;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 51;
        tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_capstyle, &PyTuple_GET_ITEM( const_tuple_str_plain_projecting_tuple, 0 ) );

        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        tmp_assattr_name_3 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_axis, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_7;
        CHECK_OBJECT( par_self );
        tmp_called_instance_3 = par_self;
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 54;
        tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_set_zorder, &PyTuple_GET_ITEM( const_tuple_float_2_5_tuple, 0 ) );

        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_8;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_set_transform );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_axes );
        if ( tmp_source_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 55;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_transData );
        Py_DECREF( tmp_source_name_8 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 55;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        tmp_assattr_name_4 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__bounds, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        tmp_assattr_name_5 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__smart_bounds, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        tmp_assattr_name_6 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__position, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_10;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( par_path );
        tmp_isinstance_inst_1 = par_path;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_matplotlib );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_matplotlib );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "matplotlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_3;
        tmp_source_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_path );
        if ( tmp_source_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_Path );
        Py_DECREF( tmp_source_name_10 );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_6c00ad45f826db1edf1142ca3256970d;
            frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 65;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 65;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_assattr_target_7;
        CHECK_OBJECT( par_path );
        tmp_assattr_name_7 = par_path;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain__path, tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_assattr_target_8;
        tmp_assattr_name_8 = const_str_plain_line;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain__patch_type, tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_9;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_assattr_target_9;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_4;
        frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame.f_lineno = 79;
        tmp_assattr_name_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_IdentityTransform );
        if ( tmp_assattr_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_9 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain__patch_transform, tmp_assattr_name_9 );
        Py_DECREF( tmp_assattr_name_9 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "oooooc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_653fe4632b0f675c9ad3a6b8c195a957 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_653fe4632b0f675c9ad3a6b8c195a957 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_653fe4632b0f675c9ad3a6b8c195a957, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_653fe4632b0f675c9ad3a6b8c195a957->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_653fe4632b0f675c9ad3a6b8c195a957, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_653fe4632b0f675c9ad3a6b8c195a957,
        type_description_1,
        par_self,
        par_axes,
        par_spine_type,
        par_path,
        par_kwargs,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_653fe4632b0f675c9ad3a6b8c195a957 == cache_frame_653fe4632b0f675c9ad3a6b8c195a957 )
    {
        Py_DECREF( frame_653fe4632b0f675c9ad3a6b8c195a957 );
    }
    cache_frame_653fe4632b0f675c9ad3a6b8c195a957 = NULL;

    assertFrameObject( frame_653fe4632b0f675c9ad3a6b8c195a957 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_2___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_spine_type );
    Py_DECREF( par_spine_type );
    par_spine_type = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_spine_type );
    Py_DECREF( par_spine_type );
    par_spine_type = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_2___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_3_set_smart_bounds( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_f7cd8877f5a6d65e18c1760d0e79da16;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_f7cd8877f5a6d65e18c1760d0e79da16 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f7cd8877f5a6d65e18c1760d0e79da16, codeobj_f7cd8877f5a6d65e18c1760d0e79da16, module_matplotlib$spines, sizeof(void *)+sizeof(void *) );
    frame_f7cd8877f5a6d65e18c1760d0e79da16 = cache_frame_f7cd8877f5a6d65e18c1760d0e79da16;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f7cd8877f5a6d65e18c1760d0e79da16 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f7cd8877f5a6d65e18c1760d0e79da16 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_value );
        tmp_assattr_name_1 = par_value;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__smart_bounds, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_spine_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_tuple_str_plain_left_str_plain_right_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_axes );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 87;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_yaxis );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 87;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_value );
            tmp_args_element_name_1 = par_value;
            frame_f7cd8877f5a6d65e18c1760d0e79da16->m_frame.f_lineno = 87;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_set_smart_bounds, call_args );
            }

            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 87;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_spine_type );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_tuple_str_plain_top_str_plain_bottom_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_2;
                PyObject *tmp_source_name_5;
                PyObject *tmp_source_name_6;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_2;
                CHECK_OBJECT( par_self );
                tmp_source_name_6 = par_self;
                tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_axes );
                if ( tmp_source_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_xaxis );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_called_instance_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_value );
                tmp_args_element_name_2 = par_value;
                frame_f7cd8877f5a6d65e18c1760d0e79da16->m_frame.f_lineno = 89;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_smart_bounds, call_args );
                }

                Py_DECREF( tmp_called_instance_2 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_stale, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7cd8877f5a6d65e18c1760d0e79da16 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f7cd8877f5a6d65e18c1760d0e79da16 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f7cd8877f5a6d65e18c1760d0e79da16, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f7cd8877f5a6d65e18c1760d0e79da16->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f7cd8877f5a6d65e18c1760d0e79da16, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f7cd8877f5a6d65e18c1760d0e79da16,
        type_description_1,
        par_self,
        par_value
    );


    // Release cached frame.
    if ( frame_f7cd8877f5a6d65e18c1760d0e79da16 == cache_frame_f7cd8877f5a6d65e18c1760d0e79da16 )
    {
        Py_DECREF( frame_f7cd8877f5a6d65e18c1760d0e79da16 );
    }
    cache_frame_f7cd8877f5a6d65e18c1760d0e79da16 = NULL;

    assertFrameObject( frame_f7cd8877f5a6d65e18c1760d0e79da16 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_3_set_smart_bounds );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_3_set_smart_bounds );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_4_get_smart_bounds( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_06a62e70be890a801744ddcdb68e42fa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_06a62e70be890a801744ddcdb68e42fa = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_06a62e70be890a801744ddcdb68e42fa, codeobj_06a62e70be890a801744ddcdb68e42fa, module_matplotlib$spines, sizeof(void *) );
    frame_06a62e70be890a801744ddcdb68e42fa = cache_frame_06a62e70be890a801744ddcdb68e42fa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_06a62e70be890a801744ddcdb68e42fa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_06a62e70be890a801744ddcdb68e42fa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__smart_bounds );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_06a62e70be890a801744ddcdb68e42fa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_06a62e70be890a801744ddcdb68e42fa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_06a62e70be890a801744ddcdb68e42fa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_06a62e70be890a801744ddcdb68e42fa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_06a62e70be890a801744ddcdb68e42fa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_06a62e70be890a801744ddcdb68e42fa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_06a62e70be890a801744ddcdb68e42fa,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_06a62e70be890a801744ddcdb68e42fa == cache_frame_06a62e70be890a801744ddcdb68e42fa )
    {
        Py_DECREF( frame_06a62e70be890a801744ddcdb68e42fa );
    }
    cache_frame_06a62e70be890a801744ddcdb68e42fa = NULL;

    assertFrameObject( frame_06a62e70be890a801744ddcdb68e42fa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_4_get_smart_bounds );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_4_get_smart_bounds );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_5_set_patch_arc( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_center = python_pars[ 1 ];
    PyObject *par_radius = python_pars[ 2 ];
    PyObject *par_theta1 = python_pars[ 3 ];
    PyObject *par_theta2 = python_pars[ 4 ];
    struct Nuitka_FrameObject *frame_6c4f3735db0bf97914eedcbb2abda000;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6c4f3735db0bf97914eedcbb2abda000 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6c4f3735db0bf97914eedcbb2abda000, codeobj_6c4f3735db0bf97914eedcbb2abda000, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6c4f3735db0bf97914eedcbb2abda000 = cache_frame_6c4f3735db0bf97914eedcbb2abda000;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6c4f3735db0bf97914eedcbb2abda000 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6c4f3735db0bf97914eedcbb2abda000 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = const_str_plain_arc;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__patch_type, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_center );
        tmp_assattr_name_2 = par_center;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__center, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_radius );
        tmp_left_name_1 = par_radius;
        tmp_right_name_1 = const_int_pos_2;
        tmp_assattr_name_3 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__width, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_radius );
        tmp_left_name_2 = par_radius;
        tmp_right_name_2 = const_int_pos_2;
        tmp_assattr_name_4 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__height, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        CHECK_OBJECT( par_theta1 );
        tmp_assattr_name_5 = par_theta1;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__theta1, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        CHECK_OBJECT( par_theta2 );
        tmp_assattr_name_6 = par_theta2;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__theta2, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_assattr_target_7;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Path );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_theta1 );
        tmp_args_element_name_1 = par_theta1;
        CHECK_OBJECT( par_theta2 );
        tmp_args_element_name_2 = par_theta2;
        frame_6c4f3735db0bf97914eedcbb2abda000->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assattr_name_7 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_arc, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assattr_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain__path, tmp_assattr_name_7 );
        Py_DECREF( tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_set_transform );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_axes );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 106;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_transAxes );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 106;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_6c4f3735db0bf97914eedcbb2abda000->m_frame.f_lineno = 106;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_assattr_target_8;
        tmp_assattr_name_8 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain_stale, tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6c4f3735db0bf97914eedcbb2abda000 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6c4f3735db0bf97914eedcbb2abda000 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6c4f3735db0bf97914eedcbb2abda000, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6c4f3735db0bf97914eedcbb2abda000->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6c4f3735db0bf97914eedcbb2abda000, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6c4f3735db0bf97914eedcbb2abda000,
        type_description_1,
        par_self,
        par_center,
        par_radius,
        par_theta1,
        par_theta2
    );


    // Release cached frame.
    if ( frame_6c4f3735db0bf97914eedcbb2abda000 == cache_frame_6c4f3735db0bf97914eedcbb2abda000 )
    {
        Py_DECREF( frame_6c4f3735db0bf97914eedcbb2abda000 );
    }
    cache_frame_6c4f3735db0bf97914eedcbb2abda000 = NULL;

    assertFrameObject( frame_6c4f3735db0bf97914eedcbb2abda000 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_5_set_patch_arc );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    CHECK_OBJECT( (PyObject *)par_theta1 );
    Py_DECREF( par_theta1 );
    par_theta1 = NULL;

    CHECK_OBJECT( (PyObject *)par_theta2 );
    Py_DECREF( par_theta2 );
    par_theta2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    CHECK_OBJECT( (PyObject *)par_theta1 );
    Py_DECREF( par_theta1 );
    par_theta1 = NULL;

    CHECK_OBJECT( (PyObject *)par_theta2 );
    Py_DECREF( par_theta2 );
    par_theta2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_5_set_patch_arc );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_6_set_patch_circle( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_center = python_pars[ 1 ];
    PyObject *par_radius = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_d26c04bf29cf1499b04ca9ff5aa2efe5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d26c04bf29cf1499b04ca9ff5aa2efe5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d26c04bf29cf1499b04ca9ff5aa2efe5, codeobj_d26c04bf29cf1499b04ca9ff5aa2efe5, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d26c04bf29cf1499b04ca9ff5aa2efe5 = cache_frame_d26c04bf29cf1499b04ca9ff5aa2efe5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d26c04bf29cf1499b04ca9ff5aa2efe5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d26c04bf29cf1499b04ca9ff5aa2efe5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = const_str_plain_circle;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__patch_type, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_center );
        tmp_assattr_name_2 = par_center;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__center, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_radius );
        tmp_left_name_1 = par_radius;
        tmp_right_name_1 = const_int_pos_2;
        tmp_assattr_name_3 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__width, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_radius );
        tmp_left_name_2 = par_radius;
        tmp_right_name_2 = const_int_pos_2;
        tmp_assattr_name_4 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__height, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_set_transform );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_axes );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_transAxes );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_d26c04bf29cf1499b04ca9ff5aa2efe5->m_frame.f_lineno = 116;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        tmp_assattr_name_5 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_stale, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d26c04bf29cf1499b04ca9ff5aa2efe5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d26c04bf29cf1499b04ca9ff5aa2efe5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d26c04bf29cf1499b04ca9ff5aa2efe5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d26c04bf29cf1499b04ca9ff5aa2efe5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d26c04bf29cf1499b04ca9ff5aa2efe5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d26c04bf29cf1499b04ca9ff5aa2efe5,
        type_description_1,
        par_self,
        par_center,
        par_radius
    );


    // Release cached frame.
    if ( frame_d26c04bf29cf1499b04ca9ff5aa2efe5 == cache_frame_d26c04bf29cf1499b04ca9ff5aa2efe5 )
    {
        Py_DECREF( frame_d26c04bf29cf1499b04ca9ff5aa2efe5 );
    }
    cache_frame_d26c04bf29cf1499b04ca9ff5aa2efe5 = NULL;

    assertFrameObject( frame_d26c04bf29cf1499b04ca9ff5aa2efe5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_6_set_patch_circle );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_6_set_patch_circle );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_7_set_patch_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3e650edf4f100d9bb9124531d6ae97a4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3e650edf4f100d9bb9124531d6ae97a4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3e650edf4f100d9bb9124531d6ae97a4, codeobj_3e650edf4f100d9bb9124531d6ae97a4, module_matplotlib$spines, sizeof(void *) );
    frame_3e650edf4f100d9bb9124531d6ae97a4 = cache_frame_3e650edf4f100d9bb9124531d6ae97a4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3e650edf4f100d9bb9124531d6ae97a4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3e650edf4f100d9bb9124531d6ae97a4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = const_str_plain_line;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__patch_type, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_stale, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3e650edf4f100d9bb9124531d6ae97a4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3e650edf4f100d9bb9124531d6ae97a4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3e650edf4f100d9bb9124531d6ae97a4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3e650edf4f100d9bb9124531d6ae97a4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3e650edf4f100d9bb9124531d6ae97a4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3e650edf4f100d9bb9124531d6ae97a4,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_3e650edf4f100d9bb9124531d6ae97a4 == cache_frame_3e650edf4f100d9bb9124531d6ae97a4 )
    {
        Py_DECREF( frame_3e650edf4f100d9bb9124531d6ae97a4 );
    }
    cache_frame_3e650edf4f100d9bb9124531d6ae97a4 = NULL;

    assertFrameObject( frame_3e650edf4f100d9bb9124531d6ae97a4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_7_set_patch_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_7_set_patch_line );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_8__recompute_transform( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_center = NULL;
    PyObject *var_width = NULL;
    PyObject *var_height = NULL;
    struct Nuitka_FrameObject *frame_aab338e1afcbcb75c3d4c51af7442c54;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_aab338e1afcbcb75c3d4c51af7442c54 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aab338e1afcbcb75c3d4c51af7442c54, codeobj_aab338e1afcbcb75c3d4c51af7442c54, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_aab338e1afcbcb75c3d4c51af7442c54 = cache_frame_aab338e1afcbcb75c3d4c51af7442c54;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aab338e1afcbcb75c3d4c51af7442c54 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aab338e1afcbcb75c3d4c51af7442c54 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__patch_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_tuple_str_plain_arc_str_plain_circle_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 131;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_convert_xunits );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__center );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 132;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 132;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_aab338e1afcbcb75c3d4c51af7442c54->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_assign_source_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_convert_yunits );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_1 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__center );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_int_pos_1;
        tmp_args_element_name_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_aab338e1afcbcb75c3d4c51af7442c54->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_1 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_assign_source_1, 1, tmp_tuple_element_1 );
        assert( var_center == NULL );
        var_center = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_convert_xunits );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__width );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 134;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_aab338e1afcbcb75c3d4c51af7442c54->m_frame.f_lineno = 134;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_width == NULL );
        var_width = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_convert_yunits );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__height );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 135;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_aab338e1afcbcb75c3d4c51af7442c54->m_frame.f_lineno = 135;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_height == NULL );
        var_height = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_10;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_11;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_aab338e1afcbcb75c3d4c51af7442c54->m_frame.f_lineno = 136;
        tmp_source_name_11 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_Affine2D );
        if ( tmp_source_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_scale );
        Py_DECREF( tmp_source_name_11 );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_width );
        tmp_left_name_1 = var_width;
        tmp_right_name_1 = const_float_0_5;
        tmp_args_element_name_5 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 137;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_height );
        tmp_left_name_2 = var_height;
        tmp_right_name_2 = const_float_0_5;
        tmp_args_element_name_6 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_5 );

            exception_lineno = 137;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_aab338e1afcbcb75c3d4c51af7442c54->m_frame.f_lineno = 136;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_source_name_10 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_5 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_source_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_translate );
        Py_DECREF( tmp_source_name_10 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_center );
        tmp_dircall_arg2_1 = var_center;
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_assattr_name_1 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
        }
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__patch_transform, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aab338e1afcbcb75c3d4c51af7442c54 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aab338e1afcbcb75c3d4c51af7442c54 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aab338e1afcbcb75c3d4c51af7442c54, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aab338e1afcbcb75c3d4c51af7442c54->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aab338e1afcbcb75c3d4c51af7442c54, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aab338e1afcbcb75c3d4c51af7442c54,
        type_description_1,
        par_self,
        var_center,
        var_width,
        var_height
    );


    // Release cached frame.
    if ( frame_aab338e1afcbcb75c3d4c51af7442c54 == cache_frame_aab338e1afcbcb75c3d4c51af7442c54 )
    {
        Py_DECREF( frame_aab338e1afcbcb75c3d4c51af7442c54 );
    }
    cache_frame_aab338e1afcbcb75c3d4c51af7442c54 = NULL;

    assertFrameObject( frame_aab338e1afcbcb75c3d4c51af7442c54 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_8__recompute_transform );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_center );
    Py_DECREF( var_center );
    var_center = NULL;

    CHECK_OBJECT( (PyObject *)var_width );
    Py_DECREF( var_width );
    var_width = NULL;

    CHECK_OBJECT( (PyObject *)var_height );
    Py_DECREF( var_height );
    var_height = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_center );
    var_center = NULL;

    Py_XDECREF( var_width );
    var_width = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_8__recompute_transform );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_9_get_patch_transform( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d27c36f537f61b2b493b1861184ecc05;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d27c36f537f61b2b493b1861184ecc05 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d27c36f537f61b2b493b1861184ecc05, codeobj_d27c36f537f61b2b493b1861184ecc05, module_matplotlib$spines, sizeof(void *)+sizeof(void *) );
    frame_d27c36f537f61b2b493b1861184ecc05 = cache_frame_d27c36f537f61b2b493b1861184ecc05;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d27c36f537f61b2b493b1861184ecc05 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d27c36f537f61b2b493b1861184ecc05 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__patch_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_tuple_str_plain_arc_str_plain_circle_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            frame_d27c36f537f61b2b493b1861184ecc05->m_frame.f_lineno = 142;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__recompute_transform );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__patch_transform );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_type_name_1;
            PyObject *tmp_object_name_1;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 145;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }

            tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( par_self );
            tmp_object_name_1 = par_self;
            tmp_called_instance_2 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 145;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }
            frame_d27c36f537f61b2b493b1861184ecc05->m_frame.f_lineno = 145;
            tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_patch_transform );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 145;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d27c36f537f61b2b493b1861184ecc05 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d27c36f537f61b2b493b1861184ecc05 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d27c36f537f61b2b493b1861184ecc05 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d27c36f537f61b2b493b1861184ecc05, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d27c36f537f61b2b493b1861184ecc05->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d27c36f537f61b2b493b1861184ecc05, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d27c36f537f61b2b493b1861184ecc05,
        type_description_1,
        par_self,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_d27c36f537f61b2b493b1861184ecc05 == cache_frame_d27c36f537f61b2b493b1861184ecc05 )
    {
        Py_DECREF( frame_d27c36f537f61b2b493b1861184ecc05 );
    }
    cache_frame_d27c36f537f61b2b493b1861184ecc05 = NULL;

    assertFrameObject( frame_d27c36f537f61b2b493b1861184ecc05 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_9_get_patch_transform );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_9_get_patch_transform );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_10_get_window_extent( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_renderer = python_pars[ 1 ];
    PyObject *var_bb = NULL;
    PyObject *var_bboxes = NULL;
    PyObject *var_tickstocheck = NULL;
    PyObject *var_tick = NULL;
    PyObject *var_bb0 = NULL;
    PyObject *var_tickl = NULL;
    PyObject *var_tickdir = NULL;
    PyObject *var_padout = NULL;
    PyObject *var_padin = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_6523a8fda21c49e09c95926afd905246;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_6523a8fda21c49e09c95926afd905246 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6523a8fda21c49e09c95926afd905246, codeobj_6523a8fda21c49e09c95926afd905246, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6523a8fda21c49e09c95926afd905246 = cache_frame_6523a8fda21c49e09c95926afd905246;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6523a8fda21c49e09c95926afd905246 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6523a8fda21c49e09c95926afd905246 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 160;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__adjust_location );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_window_extent );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_renderer;
        CHECK_OBJECT( par_renderer );
        tmp_dict_value_1 = par_renderer;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 161;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        assert( var_bb == NULL );
        var_bb = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_axis );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( var_bb );
        tmp_return_value = var_bb;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_list_element_1;
        CHECK_OBJECT( var_bb );
        tmp_list_element_1 = var_bb;
        tmp_assign_source_2 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_assign_source_2, 0, tmp_list_element_1 );
        assert( var_bboxes == NULL );
        var_bboxes = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_element_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_axis );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_majorTicks );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_list_element_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_assign_source_3, 0, tmp_list_element_2 );
        assert( var_tickstocheck == NULL );
        var_tickstocheck = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_axis );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_minorTicks );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_7;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_source_name_8;
            PyObject *tmp_source_name_9;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( var_tickstocheck );
            tmp_source_name_7 = var_tickstocheck;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_append );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 169;
                type_description_1 = "oooooooooooc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_9 = par_self;
            tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_axis );
            if ( tmp_source_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 169;
                type_description_1 = "oooooooooooc";
                goto frame_exception_exit_1;
            }
            tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_minorTicks );
            Py_DECREF( tmp_source_name_8 );
            if ( tmp_subscribed_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 169;
                type_description_1 = "oooooooooooc";
                goto frame_exception_exit_1;
            }
            tmp_subscript_name_2 = const_int_pos_1;
            tmp_args_element_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
            Py_DECREF( tmp_subscribed_name_2 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 169;
                type_description_1 = "oooooooooooc";
                goto frame_exception_exit_1;
            }
            frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 169;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 169;
                type_description_1 = "oooooooooooc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_tickstocheck );
        tmp_iter_arg_1 = var_tickstocheck;
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooc";
                exception_lineno = 170;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_6 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_tick;
            var_tick = tmp_assign_source_6;
            Py_INCREF( var_tick );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_bb );
        tmp_called_instance_2 = var_bb;
        frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 171;
        tmp_assign_source_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_frozen );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_bb0;
            var_bb0 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( var_tick );
        tmp_source_name_10 = var_tick;
        tmp_assign_source_8 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain__size );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_tickl;
            var_tickl = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_source_name_11;
        CHECK_OBJECT( var_tick );
        tmp_source_name_11 = var_tick;
        tmp_assign_source_9 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain__tickdir );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_tickdir;
            var_tickdir = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( var_tickdir );
        tmp_compexpr_left_3 = var_tickdir;
        tmp_compexpr_right_3 = const_str_plain_out;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = const_int_pos_1;
            {
                PyObject *old = var_padout;
                var_padout = tmp_assign_source_10;
                Py_INCREF( var_padout );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_11;
            tmp_assign_source_11 = const_int_0;
            {
                PyObject *old = var_padin;
                var_padin = tmp_assign_source_11;
                Py_INCREF( var_padin );
                Py_XDECREF( old );
            }

        }
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( var_tickdir );
            tmp_compexpr_left_4 = var_tickdir;
            tmp_compexpr_right_4 = const_str_plain_in;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 177;
                type_description_1 = "oooooooooooc";
                goto try_except_handler_2;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_12;
                tmp_assign_source_12 = const_int_0;
                {
                    PyObject *old = var_padout;
                    var_padout = tmp_assign_source_12;
                    Py_INCREF( var_padout );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_13;
                tmp_assign_source_13 = const_int_pos_1;
                {
                    PyObject *old = var_padin;
                    var_padin = tmp_assign_source_13;
                    Py_INCREF( var_padin );
                    Py_XDECREF( old );
                }

            }
            goto branch_end_4;
            branch_no_4:;
            {
                PyObject *tmp_assign_source_14;
                tmp_assign_source_14 = const_float_0_5;
                {
                    PyObject *old = var_padout;
                    var_padout = tmp_assign_source_14;
                    Py_INCREF( var_padout );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_15;
                tmp_assign_source_15 = const_float_0_5;
                {
                    PyObject *old = var_padin;
                    var_padin = tmp_assign_source_15;
                    Py_INCREF( var_padin );
                    Py_XDECREF( old );
                }

            }
            branch_end_4:;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_source_name_12;
        PyObject *tmp_source_name_13;
        if ( var_padout == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "padout" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }

        tmp_left_name_3 = var_padout;
        CHECK_OBJECT( var_tickl );
        tmp_right_name_1 = var_tickl;
        tmp_left_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_right_name_2 = const_int_pos_72;
        tmp_left_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_13 = par_self;
        tmp_source_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_figure );
        if ( tmp_source_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 183;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_dpi );
        Py_DECREF( tmp_source_name_12 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 183;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_assign_source_16 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_padout;
            var_padout = tmp_assign_source_16;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_left_name_6;
        PyObject *tmp_right_name_4;
        PyObject *tmp_right_name_5;
        PyObject *tmp_right_name_6;
        PyObject *tmp_source_name_14;
        PyObject *tmp_source_name_15;
        if ( var_padin == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "padin" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }

        tmp_left_name_6 = var_padin;
        CHECK_OBJECT( var_tickl );
        tmp_right_name_4 = var_tickl;
        tmp_left_name_5 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_4 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_right_name_5 = const_int_pos_72;
        tmp_left_name_4 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_5, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_5 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_15 = par_self;
        tmp_source_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_figure );
        if ( tmp_source_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_4 );

            exception_lineno = 184;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_right_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_dpi );
        Py_DECREF( tmp_source_name_14 );
        if ( tmp_right_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_4 );

            exception_lineno = 184;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_assign_source_17 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_6 );
        Py_DECREF( tmp_left_name_4 );
        Py_DECREF( tmp_right_name_6 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_padin;
            var_padin = tmp_assign_source_17;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_16;
        PyObject *tmp_call_result_3;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_tick );
        tmp_source_name_16 = var_tick;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_tick1line );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 186;
        tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_visible );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_3 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_3 );

            exception_lineno = 186;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_condition_result_5 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_3 );
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_source_name_17;
            CHECK_OBJECT( par_self );
            tmp_source_name_17 = par_self;
            tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_spine_type );
            if ( tmp_compexpr_left_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;
                type_description_1 = "oooooooooooc";
                goto try_except_handler_2;
            }
            tmp_compexpr_right_5 = LIST_COPY( const_list_str_plain_left_list );
            tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
            Py_DECREF( tmp_compexpr_left_5 );
            Py_DECREF( tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 187;
                type_description_1 = "oooooooooooc";
                goto try_except_handler_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assattr_name_1;
                PyObject *tmp_left_name_7;
                PyObject *tmp_source_name_18;
                PyObject *tmp_right_name_7;
                PyObject *tmp_assattr_target_1;
                CHECK_OBJECT( var_bb0 );
                tmp_source_name_18 = var_bb0;
                tmp_left_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_x0 );
                if ( tmp_left_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 188;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_padout );
                tmp_right_name_7 = var_padout;
                tmp_assattr_name_1 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_7 );
                Py_DECREF( tmp_left_name_7 );
                if ( tmp_assattr_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 188;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_bb0 );
                tmp_assattr_target_1 = var_bb0;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_x0, tmp_assattr_name_1 );
                Py_DECREF( tmp_assattr_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 188;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
            }
            {
                PyObject *tmp_assattr_name_2;
                PyObject *tmp_left_name_8;
                PyObject *tmp_source_name_19;
                PyObject *tmp_right_name_8;
                PyObject *tmp_assattr_target_2;
                CHECK_OBJECT( var_bb0 );
                tmp_source_name_19 = var_bb0;
                tmp_left_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_x1 );
                if ( tmp_left_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_padin );
                tmp_right_name_8 = var_padin;
                tmp_assattr_name_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_8, tmp_right_name_8 );
                Py_DECREF( tmp_left_name_8 );
                if ( tmp_assattr_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_bb0 );
                tmp_assattr_target_2 = var_bb0;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_x1, tmp_assattr_name_2 );
                Py_DECREF( tmp_assattr_name_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
            }
            goto branch_end_6;
            branch_no_6:;
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_compexpr_left_6;
                PyObject *tmp_compexpr_right_6;
                PyObject *tmp_source_name_20;
                CHECK_OBJECT( par_self );
                tmp_source_name_20 = par_self;
                tmp_compexpr_left_6 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_spine_type );
                if ( tmp_compexpr_left_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 190;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                tmp_compexpr_right_6 = LIST_COPY( const_list_str_plain_bottom_list );
                tmp_res = PySequence_Contains( tmp_compexpr_right_6, tmp_compexpr_left_6 );
                Py_DECREF( tmp_compexpr_left_6 );
                Py_DECREF( tmp_compexpr_right_6 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 190;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_assattr_name_3;
                    PyObject *tmp_left_name_9;
                    PyObject *tmp_source_name_21;
                    PyObject *tmp_right_name_9;
                    PyObject *tmp_assattr_target_3;
                    CHECK_OBJECT( var_bb0 );
                    tmp_source_name_21 = var_bb0;
                    tmp_left_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_y0 );
                    if ( tmp_left_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 191;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_padout );
                    tmp_right_name_9 = var_padout;
                    tmp_assattr_name_3 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_9, tmp_right_name_9 );
                    Py_DECREF( tmp_left_name_9 );
                    if ( tmp_assattr_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 191;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_bb0 );
                    tmp_assattr_target_3 = var_bb0;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_y0, tmp_assattr_name_3 );
                    Py_DECREF( tmp_assattr_name_3 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 191;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                }
                {
                    PyObject *tmp_assattr_name_4;
                    PyObject *tmp_left_name_10;
                    PyObject *tmp_source_name_22;
                    PyObject *tmp_right_name_10;
                    PyObject *tmp_assattr_target_4;
                    CHECK_OBJECT( var_bb0 );
                    tmp_source_name_22 = var_bb0;
                    tmp_left_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_y1 );
                    if ( tmp_left_name_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_padin );
                    tmp_right_name_10 = var_padin;
                    tmp_assattr_name_4 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_10, tmp_right_name_10 );
                    Py_DECREF( tmp_left_name_10 );
                    if ( tmp_assattr_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_bb0 );
                    tmp_assattr_target_4 = var_bb0;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_y1, tmp_assattr_name_4 );
                    Py_DECREF( tmp_assattr_name_4 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                }
                branch_no_7:;
            }
            branch_end_6:;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_source_name_23;
        PyObject *tmp_call_result_4;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_tick );
        tmp_source_name_23 = var_tick;
        tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_tick2line );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 194;
        tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_get_visible );
        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_4 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_4 );

            exception_lineno = 194;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_4 );
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            PyObject *tmp_source_name_24;
            CHECK_OBJECT( par_self );
            tmp_source_name_24 = par_self;
            tmp_compexpr_left_7 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_spine_type );
            if ( tmp_compexpr_left_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_1 = "oooooooooooc";
                goto try_except_handler_2;
            }
            tmp_compexpr_right_7 = LIST_COPY( const_list_str_plain_right_list );
            tmp_res = PySequence_Contains( tmp_compexpr_right_7, tmp_compexpr_left_7 );
            Py_DECREF( tmp_compexpr_left_7 );
            Py_DECREF( tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_1 = "oooooooooooc";
                goto try_except_handler_2;
            }
            tmp_condition_result_9 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                PyObject *tmp_assattr_name_5;
                PyObject *tmp_left_name_11;
                PyObject *tmp_source_name_25;
                PyObject *tmp_right_name_11;
                PyObject *tmp_assattr_target_5;
                CHECK_OBJECT( var_bb0 );
                tmp_source_name_25 = var_bb0;
                tmp_left_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_x1 );
                if ( tmp_left_name_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 196;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_padout );
                tmp_right_name_11 = var_padout;
                tmp_assattr_name_5 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_11, tmp_right_name_11 );
                Py_DECREF( tmp_left_name_11 );
                if ( tmp_assattr_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 196;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_bb0 );
                tmp_assattr_target_5 = var_bb0;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_x1, tmp_assattr_name_5 );
                Py_DECREF( tmp_assattr_name_5 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 196;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
            }
            {
                PyObject *tmp_assattr_name_6;
                PyObject *tmp_left_name_12;
                PyObject *tmp_source_name_26;
                PyObject *tmp_right_name_12;
                PyObject *tmp_assattr_target_6;
                CHECK_OBJECT( var_bb0 );
                tmp_source_name_26 = var_bb0;
                tmp_left_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_x0 );
                if ( tmp_left_name_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_padin );
                tmp_right_name_12 = var_padin;
                tmp_assattr_name_6 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_12, tmp_right_name_12 );
                Py_DECREF( tmp_left_name_12 );
                if ( tmp_assattr_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_bb0 );
                tmp_assattr_target_6 = var_bb0;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_x0, tmp_assattr_name_6 );
                Py_DECREF( tmp_assattr_name_6 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
            }
            goto branch_end_9;
            branch_no_9:;
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                PyObject *tmp_source_name_27;
                CHECK_OBJECT( par_self );
                tmp_source_name_27 = par_self;
                tmp_compexpr_left_8 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_spine_type );
                if ( tmp_compexpr_left_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 198;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                tmp_compexpr_right_8 = LIST_COPY( const_list_str_plain_top_list );
                tmp_res = PySequence_Contains( tmp_compexpr_right_8, tmp_compexpr_left_8 );
                Py_DECREF( tmp_compexpr_left_8 );
                Py_DECREF( tmp_compexpr_right_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 198;
                    type_description_1 = "oooooooooooc";
                    goto try_except_handler_2;
                }
                tmp_condition_result_10 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    PyObject *tmp_assattr_name_7;
                    PyObject *tmp_left_name_13;
                    PyObject *tmp_source_name_28;
                    PyObject *tmp_right_name_13;
                    PyObject *tmp_assattr_target_7;
                    CHECK_OBJECT( var_bb0 );
                    tmp_source_name_28 = var_bb0;
                    tmp_left_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_y1 );
                    if ( tmp_left_name_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 199;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_padout );
                    tmp_right_name_13 = var_padout;
                    tmp_assattr_name_7 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_13, tmp_right_name_13 );
                    Py_DECREF( tmp_left_name_13 );
                    if ( tmp_assattr_name_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 199;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_bb0 );
                    tmp_assattr_target_7 = var_bb0;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_y1, tmp_assattr_name_7 );
                    Py_DECREF( tmp_assattr_name_7 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 199;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                }
                {
                    PyObject *tmp_assattr_name_8;
                    PyObject *tmp_left_name_14;
                    PyObject *tmp_source_name_29;
                    PyObject *tmp_right_name_14;
                    PyObject *tmp_assattr_target_8;
                    CHECK_OBJECT( var_bb0 );
                    tmp_source_name_29 = var_bb0;
                    tmp_left_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_y0 );
                    if ( tmp_left_name_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 200;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_padout );
                    tmp_right_name_14 = var_padout;
                    tmp_assattr_name_8 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_14, tmp_right_name_14 );
                    Py_DECREF( tmp_left_name_14 );
                    if ( tmp_assattr_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 200;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_bb0 );
                    tmp_assattr_target_8 = var_bb0;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain_y0, tmp_assattr_name_8 );
                    Py_DECREF( tmp_assattr_name_8 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 200;
                        type_description_1 = "oooooooooooc";
                        goto try_except_handler_2;
                    }
                }
                branch_no_10:;
            }
            branch_end_9:;
        }
        branch_no_8:;
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_bboxes );
        tmp_called_instance_5 = var_bboxes;
        CHECK_OBJECT( var_bb0 );
        tmp_args_element_name_2 = var_bb0;
        frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 201;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_1 = "oooooooooooc";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 170;
        type_description_1 = "oooooooooooc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_6;
        PyObject *tmp_source_name_30;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 203;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_30 = tmp_mvar_value_1;
        tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_Bbox );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bboxes );
        tmp_args_element_name_3 = var_bboxes;
        frame_6523a8fda21c49e09c95926afd905246->m_frame.f_lineno = 203;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_union, call_args );
        }

        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;
            type_description_1 = "oooooooooooc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6523a8fda21c49e09c95926afd905246 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6523a8fda21c49e09c95926afd905246 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6523a8fda21c49e09c95926afd905246 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6523a8fda21c49e09c95926afd905246, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6523a8fda21c49e09c95926afd905246->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6523a8fda21c49e09c95926afd905246, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6523a8fda21c49e09c95926afd905246,
        type_description_1,
        par_self,
        par_renderer,
        var_bb,
        var_bboxes,
        var_tickstocheck,
        var_tick,
        var_bb0,
        var_tickl,
        var_tickdir,
        var_padout,
        var_padin,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_6523a8fda21c49e09c95926afd905246 == cache_frame_6523a8fda21c49e09c95926afd905246 )
    {
        Py_DECREF( frame_6523a8fda21c49e09c95926afd905246 );
    }
    cache_frame_6523a8fda21c49e09c95926afd905246 = NULL;

    assertFrameObject( frame_6523a8fda21c49e09c95926afd905246 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_10_get_window_extent );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)var_bb );
    Py_DECREF( var_bb );
    var_bb = NULL;

    Py_XDECREF( var_bboxes );
    var_bboxes = NULL;

    Py_XDECREF( var_tickstocheck );
    var_tickstocheck = NULL;

    Py_XDECREF( var_tick );
    var_tick = NULL;

    Py_XDECREF( var_bb0 );
    var_bb0 = NULL;

    Py_XDECREF( var_tickl );
    var_tickl = NULL;

    Py_XDECREF( var_tickdir );
    var_tickdir = NULL;

    Py_XDECREF( var_padout );
    var_padout = NULL;

    Py_XDECREF( var_padin );
    var_padin = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    Py_XDECREF( var_bb );
    var_bb = NULL;

    Py_XDECREF( var_bboxes );
    var_bboxes = NULL;

    Py_XDECREF( var_tickstocheck );
    var_tickstocheck = NULL;

    Py_XDECREF( var_tick );
    var_tick = NULL;

    Py_XDECREF( var_bb0 );
    var_bb0 = NULL;

    Py_XDECREF( var_tickl );
    var_tickl = NULL;

    Py_XDECREF( var_tickdir );
    var_tickdir = NULL;

    Py_XDECREF( var_padout );
    var_padout = NULL;

    Py_XDECREF( var_padin );
    var_padin = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_10_get_window_extent );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_11_get_path( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7b36e0ef0cedb5b112ddf64accbe3ba5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7b36e0ef0cedb5b112ddf64accbe3ba5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7b36e0ef0cedb5b112ddf64accbe3ba5, codeobj_7b36e0ef0cedb5b112ddf64accbe3ba5, module_matplotlib$spines, sizeof(void *) );
    frame_7b36e0ef0cedb5b112ddf64accbe3ba5 = cache_frame_7b36e0ef0cedb5b112ddf64accbe3ba5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__path );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7b36e0ef0cedb5b112ddf64accbe3ba5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7b36e0ef0cedb5b112ddf64accbe3ba5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7b36e0ef0cedb5b112ddf64accbe3ba5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7b36e0ef0cedb5b112ddf64accbe3ba5,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 == cache_frame_7b36e0ef0cedb5b112ddf64accbe3ba5 )
    {
        Py_DECREF( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 );
    }
    cache_frame_7b36e0ef0cedb5b112ddf64accbe3ba5 = NULL;

    assertFrameObject( frame_7b36e0ef0cedb5b112ddf64accbe3ba5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_11_get_path );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_11_get_path );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_12__ensure_position_is_set( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c5bd117846bf6461fd37167d3813bbb2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_c5bd117846bf6461fd37167d3813bbb2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c5bd117846bf6461fd37167d3813bbb2, codeobj_c5bd117846bf6461fd37167d3813bbb2, module_matplotlib$spines, sizeof(void *) );
    frame_c5bd117846bf6461fd37167d3813bbb2 = cache_frame_c5bd117846bf6461fd37167d3813bbb2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c5bd117846bf6461fd37167d3813bbb2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c5bd117846bf6461fd37167d3813bbb2 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__position );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 209;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            tmp_assattr_name_1 = const_tuple_str_plain_outward_float_0_0_tuple;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__position, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_set_position );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__position );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 212;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_c5bd117846bf6461fd37167d3813bbb2->m_frame.f_lineno = 212;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c5bd117846bf6461fd37167d3813bbb2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c5bd117846bf6461fd37167d3813bbb2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c5bd117846bf6461fd37167d3813bbb2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c5bd117846bf6461fd37167d3813bbb2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c5bd117846bf6461fd37167d3813bbb2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c5bd117846bf6461fd37167d3813bbb2,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_c5bd117846bf6461fd37167d3813bbb2 == cache_frame_c5bd117846bf6461fd37167d3813bbb2 )
    {
        Py_DECREF( frame_c5bd117846bf6461fd37167d3813bbb2 );
    }
    cache_frame_c5bd117846bf6461fd37167d3813bbb2 = NULL;

    assertFrameObject( frame_c5bd117846bf6461fd37167d3813bbb2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_12__ensure_position_is_set );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_12__ensure_position_is_set );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_13_register_axis( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_axis = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_4e9246b0c43ceae3e3f16f0c7488584e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4e9246b0c43ceae3e3f16f0c7488584e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4e9246b0c43ceae3e3f16f0c7488584e, codeobj_4e9246b0c43ceae3e3f16f0c7488584e, module_matplotlib$spines, sizeof(void *)+sizeof(void *) );
    frame_4e9246b0c43ceae3e3f16f0c7488584e = cache_frame_4e9246b0c43ceae3e3f16f0c7488584e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4e9246b0c43ceae3e3f16f0c7488584e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4e9246b0c43ceae3e3f16f0c7488584e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_axis );
        tmp_assattr_name_1 = par_axis;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_axis, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 221;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_axis );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_axis );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_4e9246b0c43ceae3e3f16f0c7488584e->m_frame.f_lineno = 223;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_cla );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_stale, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4e9246b0c43ceae3e3f16f0c7488584e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4e9246b0c43ceae3e3f16f0c7488584e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4e9246b0c43ceae3e3f16f0c7488584e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4e9246b0c43ceae3e3f16f0c7488584e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4e9246b0c43ceae3e3f16f0c7488584e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4e9246b0c43ceae3e3f16f0c7488584e,
        type_description_1,
        par_self,
        par_axis
    );


    // Release cached frame.
    if ( frame_4e9246b0c43ceae3e3f16f0c7488584e == cache_frame_4e9246b0c43ceae3e3f16f0c7488584e )
    {
        Py_DECREF( frame_4e9246b0c43ceae3e3f16f0c7488584e );
    }
    cache_frame_4e9246b0c43ceae3e3f16f0c7488584e = NULL;

    assertFrameObject( frame_4e9246b0c43ceae3e3f16f0c7488584e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_13_register_axis );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_axis );
    Py_DECREF( par_axis );
    par_axis = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_axis );
    Py_DECREF( par_axis );
    par_axis = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_13_register_axis );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_14_cla( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7bd8e10ceae9ecfcf087123d499c9345;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7bd8e10ceae9ecfcf087123d499c9345 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7bd8e10ceae9ecfcf087123d499c9345, codeobj_7bd8e10ceae9ecfcf087123d499c9345, module_matplotlib$spines, sizeof(void *) );
    frame_7bd8e10ceae9ecfcf087123d499c9345 = cache_frame_7bd8e10ceae9ecfcf087123d499c9345;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7bd8e10ceae9ecfcf087123d499c9345 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7bd8e10ceae9ecfcf087123d499c9345 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_axis );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 229;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_axis );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_7bd8e10ceae9ecfcf087123d499c9345->m_frame.f_lineno = 230;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_cla );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7bd8e10ceae9ecfcf087123d499c9345 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7bd8e10ceae9ecfcf087123d499c9345 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7bd8e10ceae9ecfcf087123d499c9345, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7bd8e10ceae9ecfcf087123d499c9345->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7bd8e10ceae9ecfcf087123d499c9345, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7bd8e10ceae9ecfcf087123d499c9345,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_7bd8e10ceae9ecfcf087123d499c9345 == cache_frame_7bd8e10ceae9ecfcf087123d499c9345 )
    {
        Py_DECREF( frame_7bd8e10ceae9ecfcf087123d499c9345 );
    }
    cache_frame_7bd8e10ceae9ecfcf087123d499c9345 = NULL;

    assertFrameObject( frame_7bd8e10ceae9ecfcf087123d499c9345 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_14_cla );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_14_cla );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_15_is_frame_like( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_position = NULL;
    PyObject *var_position_type = NULL;
    PyObject *var_amount = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_bc7672b1c966a11cfbfcd3b29487a2e2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_bc7672b1c966a11cfbfcd3b29487a2e2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bc7672b1c966a11cfbfcd3b29487a2e2, codeobj_bc7672b1c966a11cfbfcd3b29487a2e2, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bc7672b1c966a11cfbfcd3b29487a2e2 = cache_frame_bc7672b1c966a11cfbfcd3b29487a2e2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bc7672b1c966a11cfbfcd3b29487a2e2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bc7672b1c966a11cfbfcd3b29487a2e2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_bc7672b1c966a11cfbfcd3b29487a2e2->m_frame.f_lineno = 239;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__ensure_position_is_set );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__position );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_position == NULL );
        var_position = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( var_position );
        tmp_isinstance_inst_1 = var_position;
        tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 241;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( var_position );
            tmp_compexpr_left_1 = var_position;
            tmp_compexpr_right_1 = const_str_plain_center;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 242;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                tmp_assign_source_2 = const_tuple_str_plain_axes_float_0_5_tuple;
                {
                    PyObject *old = var_position;
                    assert( old != NULL );
                    var_position = tmp_assign_source_2;
                    Py_INCREF( var_position );
                    Py_DECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                CHECK_OBJECT( var_position );
                tmp_compexpr_left_2 = var_position;
                tmp_compexpr_right_2 = const_str_plain_zero;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 244;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_3;
                    tmp_assign_source_3 = const_tuple_str_plain_data_int_0_tuple;
                    {
                        PyObject *old = var_position;
                        assert( old != NULL );
                        var_position = tmp_assign_source_3;
                        Py_INCREF( var_position );
                        Py_DECREF( old );
                    }

                }
                branch_no_3:;
            }
            branch_end_2:;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_len_arg_1;
        if ( var_position == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "position" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 246;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_len_arg_1 = var_position;
        tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = const_int_pos_2;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_4cde890927cc2b88beac2083dde55dbb;
            frame_bc7672b1c966a11cfbfcd3b29487a2e2->m_frame.f_lineno = 247;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 247;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_4:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        if ( var_position == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "position" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 248;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_iter_arg_1 = var_position;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 248;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 248;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooo";
                    exception_lineno = 248;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooo";
            exception_lineno = 248;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        assert( var_position_type == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_position_type = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        assert( var_amount == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_amount = tmp_assign_source_8;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_5;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( var_position_type );
        tmp_compexpr_left_4 = var_position_type;
        tmp_compexpr_right_4 = const_str_plain_outward;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_amount );
        tmp_compexpr_left_5 = var_amount;
        tmp_compexpr_right_5 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_5 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_5 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        tmp_return_value = Py_True;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_5;
        branch_no_5:;
        tmp_return_value = Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_end_5:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc7672b1c966a11cfbfcd3b29487a2e2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc7672b1c966a11cfbfcd3b29487a2e2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc7672b1c966a11cfbfcd3b29487a2e2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bc7672b1c966a11cfbfcd3b29487a2e2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bc7672b1c966a11cfbfcd3b29487a2e2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bc7672b1c966a11cfbfcd3b29487a2e2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bc7672b1c966a11cfbfcd3b29487a2e2,
        type_description_1,
        par_self,
        var_position,
        var_position_type,
        var_amount
    );


    // Release cached frame.
    if ( frame_bc7672b1c966a11cfbfcd3b29487a2e2 == cache_frame_bc7672b1c966a11cfbfcd3b29487a2e2 )
    {
        Py_DECREF( frame_bc7672b1c966a11cfbfcd3b29487a2e2 );
    }
    cache_frame_bc7672b1c966a11cfbfcd3b29487a2e2 = NULL;

    assertFrameObject( frame_bc7672b1c966a11cfbfcd3b29487a2e2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_15_is_frame_like );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_position );
    var_position = NULL;

    CHECK_OBJECT( (PyObject *)var_position_type );
    Py_DECREF( var_position_type );
    var_position_type = NULL;

    CHECK_OBJECT( (PyObject *)var_amount );
    Py_DECREF( var_amount );
    var_amount = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_position );
    var_position = NULL;

    Py_XDECREF( var_position_type );
    var_position_type = NULL;

    Py_XDECREF( var_amount );
    var_amount = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_15_is_frame_like );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_16__adjust_location( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_low = NULL;
    PyObject *var_high = NULL;
    PyObject *var_viewlim_low = NULL;
    PyObject *var_viewlim_high = NULL;
    PyObject *var_datalim_low = NULL;
    PyObject *var_datalim_high = NULL;
    PyObject *var_ticks = NULL;
    PyObject *var_cond = NULL;
    PyObject *var_tickvals = NULL;
    PyObject *var_direction = NULL;
    PyObject *var_offset = NULL;
    PyObject *var_rmin = NULL;
    PyObject *var_rmax = NULL;
    PyObject *var_rorigin = NULL;
    PyObject *var_scaled_diameter = NULL;
    PyObject *var_v1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_4__element_1 = NULL;
    PyObject *tmp_tuple_unpack_4__element_2 = NULL;
    PyObject *tmp_tuple_unpack_4__source_iter = NULL;
    PyObject *tmp_tuple_unpack_5__element_1 = NULL;
    PyObject *tmp_tuple_unpack_5__element_2 = NULL;
    PyObject *tmp_tuple_unpack_5__source_iter = NULL;
    PyObject *tmp_tuple_unpack_6__element_1 = NULL;
    PyObject *tmp_tuple_unpack_6__element_2 = NULL;
    PyObject *tmp_tuple_unpack_6__source_iter = NULL;
    PyObject *tmp_tuple_unpack_7__element_1 = NULL;
    PyObject *tmp_tuple_unpack_7__element_2 = NULL;
    PyObject *tmp_tuple_unpack_7__source_iter = NULL;
    PyObject *tmp_tuple_unpack_8__element_1 = NULL;
    PyObject *tmp_tuple_unpack_8__element_2 = NULL;
    PyObject *tmp_tuple_unpack_8__source_iter = NULL;
    PyObject *tmp_tuple_unpack_9__element_1 = NULL;
    PyObject *tmp_tuple_unpack_9__element_2 = NULL;
    PyObject *tmp_tuple_unpack_9__source_iter = NULL;
    struct Nuitka_FrameObject *frame_1732b7288de3ff07b5d0689f845f9ced;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;
    static struct Nuitka_FrameObject *cache_frame_1732b7288de3ff07b5d0689f845f9ced = NULL;
    PyObject *exception_keeper_type_25;
    PyObject *exception_keeper_value_25;
    PyTracebackObject *exception_keeper_tb_25;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_25;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1732b7288de3ff07b5d0689f845f9ced, codeobj_1732b7288de3ff07b5d0689f845f9ced, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1732b7288de3ff07b5d0689f845f9ced = cache_frame_1732b7288de3ff07b5d0689f845f9ced;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1732b7288de3ff07b5d0689f845f9ced );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1732b7288de3ff07b5d0689f845f9ced ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_spine_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_circle;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__bounds );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_spine_type );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 261;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_3 = const_tuple_str_plain_left_str_plain_right_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 261;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            // Tried code:
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_source_name_5;
                PyObject *tmp_source_name_6;
                CHECK_OBJECT( par_self );
                tmp_source_name_6 = par_self;
                tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_axes );
                if ( tmp_source_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 262;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_viewLim );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_source_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 262;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_intervaly );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_iter_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 262;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 262;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_2;
                }
                assert( tmp_tuple_unpack_1__source_iter == NULL );
                tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_unpack_1;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
                tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
                if ( tmp_assign_source_2 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 262;
                    goto try_except_handler_3;
                }
                assert( tmp_tuple_unpack_1__element_1 == NULL );
                tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
            }
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_unpack_2;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
                tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
                if ( tmp_assign_source_3 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 262;
                    goto try_except_handler_3;
                }
                assert( tmp_tuple_unpack_1__element_2 == NULL );
                tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
            }
            {
                PyObject *tmp_iterator_name_1;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooooooooooooooo";
                            exception_lineno = 262;
                            goto try_except_handler_3;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 262;
                    goto try_except_handler_3;
                }
            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
            Py_DECREF( tmp_tuple_unpack_1__source_iter );
            tmp_tuple_unpack_1__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto try_except_handler_2;
            // End of try:
            try_end_1:;
            goto try_end_2;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
            tmp_tuple_unpack_1__element_1 = NULL;

            Py_XDECREF( tmp_tuple_unpack_1__element_2 );
            tmp_tuple_unpack_1__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto frame_exception_exit_1;
            // End of try:
            try_end_2:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
            Py_DECREF( tmp_tuple_unpack_1__source_iter );
            tmp_tuple_unpack_1__source_iter = NULL;

            {
                PyObject *tmp_assign_source_4;
                CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
                tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
                assert( var_low == NULL );
                Py_INCREF( tmp_assign_source_4 );
                var_low = tmp_assign_source_4;
            }
            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
            tmp_tuple_unpack_1__element_1 = NULL;

            {
                PyObject *tmp_assign_source_5;
                CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
                tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
                assert( var_high == NULL );
                Py_INCREF( tmp_assign_source_5 );
                var_high = tmp_assign_source_5;
            }
            Py_XDECREF( tmp_tuple_unpack_1__element_2 );
            tmp_tuple_unpack_1__element_2 = NULL;

            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                PyObject *tmp_source_name_7;
                CHECK_OBJECT( par_self );
                tmp_source_name_7 = par_self;
                tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_spine_type );
                if ( tmp_compexpr_left_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 263;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_4 = const_tuple_str_plain_top_str_plain_bottom_tuple;
                tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
                Py_DECREF( tmp_compexpr_left_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 263;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                // Tried code:
                {
                    PyObject *tmp_assign_source_6;
                    PyObject *tmp_iter_arg_2;
                    PyObject *tmp_source_name_8;
                    PyObject *tmp_source_name_9;
                    PyObject *tmp_source_name_10;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_10 = par_self;
                    tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_axes );
                    if ( tmp_source_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 264;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_4;
                    }
                    tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_viewLim );
                    Py_DECREF( tmp_source_name_9 );
                    if ( tmp_source_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 264;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_4;
                    }
                    tmp_iter_arg_2 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_intervalx );
                    Py_DECREF( tmp_source_name_8 );
                    if ( tmp_iter_arg_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 264;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_4;
                    }
                    tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
                    Py_DECREF( tmp_iter_arg_2 );
                    if ( tmp_assign_source_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 264;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_4;
                    }
                    assert( tmp_tuple_unpack_2__source_iter == NULL );
                    tmp_tuple_unpack_2__source_iter = tmp_assign_source_6;
                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_7;
                    PyObject *tmp_unpack_3;
                    CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                    tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
                    tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
                    if ( tmp_assign_source_7 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 264;
                        goto try_except_handler_5;
                    }
                    assert( tmp_tuple_unpack_2__element_1 == NULL );
                    tmp_tuple_unpack_2__element_1 = tmp_assign_source_7;
                }
                {
                    PyObject *tmp_assign_source_8;
                    PyObject *tmp_unpack_4;
                    CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                    tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
                    tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
                    if ( tmp_assign_source_8 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 264;
                        goto try_except_handler_5;
                    }
                    assert( tmp_tuple_unpack_2__element_2 == NULL );
                    tmp_tuple_unpack_2__element_2 = tmp_assign_source_8;
                }
                {
                    PyObject *tmp_iterator_name_2;
                    CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                    tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
                    // Check if iterator has left-over elements.
                    CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

                    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

                    if (likely( tmp_iterator_attempt == NULL ))
                    {
                        PyObject *error = GET_ERROR_OCCURRED();

                        if ( error != NULL )
                        {
                            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                            {
                                CLEAR_ERROR_OCCURRED();
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                type_description_1 = "ooooooooooooooooo";
                                exception_lineno = 264;
                                goto try_except_handler_5;
                            }
                        }
                    }
                    else
                    {
                        Py_DECREF( tmp_iterator_attempt );

                        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 264;
                        goto try_except_handler_5;
                    }
                }
                goto try_end_3;
                // Exception handler code:
                try_except_handler_5:;
                exception_keeper_type_3 = exception_type;
                exception_keeper_value_3 = exception_value;
                exception_keeper_tb_3 = exception_tb;
                exception_keeper_lineno_3 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
                Py_DECREF( tmp_tuple_unpack_2__source_iter );
                tmp_tuple_unpack_2__source_iter = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_3;
                exception_value = exception_keeper_value_3;
                exception_tb = exception_keeper_tb_3;
                exception_lineno = exception_keeper_lineno_3;

                goto try_except_handler_4;
                // End of try:
                try_end_3:;
                goto try_end_4;
                // Exception handler code:
                try_except_handler_4:;
                exception_keeper_type_4 = exception_type;
                exception_keeper_value_4 = exception_value;
                exception_keeper_tb_4 = exception_tb;
                exception_keeper_lineno_4 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_tuple_unpack_2__element_1 );
                tmp_tuple_unpack_2__element_1 = NULL;

                Py_XDECREF( tmp_tuple_unpack_2__element_2 );
                tmp_tuple_unpack_2__element_2 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_4;
                exception_value = exception_keeper_value_4;
                exception_tb = exception_keeper_tb_4;
                exception_lineno = exception_keeper_lineno_4;

                goto frame_exception_exit_1;
                // End of try:
                try_end_4:;
                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
                Py_DECREF( tmp_tuple_unpack_2__source_iter );
                tmp_tuple_unpack_2__source_iter = NULL;

                {
                    PyObject *tmp_assign_source_9;
                    CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
                    tmp_assign_source_9 = tmp_tuple_unpack_2__element_1;
                    assert( var_low == NULL );
                    Py_INCREF( tmp_assign_source_9 );
                    var_low = tmp_assign_source_9;
                }
                Py_XDECREF( tmp_tuple_unpack_2__element_1 );
                tmp_tuple_unpack_2__element_1 = NULL;

                {
                    PyObject *tmp_assign_source_10;
                    CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
                    tmp_assign_source_10 = tmp_tuple_unpack_2__element_2;
                    assert( var_high == NULL );
                    Py_INCREF( tmp_assign_source_10 );
                    var_high = tmp_assign_source_10;
                }
                Py_XDECREF( tmp_tuple_unpack_2__element_2 );
                tmp_tuple_unpack_2__element_2 = NULL;

                goto branch_end_4;
                branch_no_4:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_make_exception_arg_1;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_source_name_11;
                    tmp_left_name_1 = const_str_digest_282263d2fa2adfc14d01396958fadf12;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_11 = par_self;
                    tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_spine_type );
                    if ( tmp_right_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 267;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_make_exception_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 266;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 266;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_1 };
                        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                    }

                    Py_DECREF( tmp_make_exception_arg_1 );
                    assert( !(tmp_raise_type_1 == NULL) );
                    exception_type = tmp_raise_type_1;
                    exception_lineno = 266;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_source_name_12;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_12 = par_self;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain__smart_bounds );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 269;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 269;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            // Tried code:
            {
                PyObject *tmp_assign_source_11;
                PyObject *tmp_iter_arg_3;
                PyObject *tmp_called_name_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_list_element_1;
                tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_sorted );
                assert( tmp_called_name_1 != NULL );
                if ( var_low == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "low" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 273;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_6;
                }

                tmp_list_element_1 = var_low;
                tmp_args_element_name_1 = PyList_New( 2 );
                Py_INCREF( tmp_list_element_1 );
                PyList_SET_ITEM( tmp_args_element_name_1, 0, tmp_list_element_1 );
                if ( var_high == NULL )
                {
                    Py_DECREF( tmp_args_element_name_1 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "high" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 273;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_6;
                }

                tmp_list_element_1 = var_high;
                Py_INCREF( tmp_list_element_1 );
                PyList_SET_ITEM( tmp_args_element_name_1, 1, tmp_list_element_1 );
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 273;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_args_element_name_1 );
                if ( tmp_iter_arg_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 273;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_6;
                }
                tmp_assign_source_11 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
                Py_DECREF( tmp_iter_arg_3 );
                if ( tmp_assign_source_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 273;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_6;
                }
                assert( tmp_tuple_unpack_3__source_iter == NULL );
                tmp_tuple_unpack_3__source_iter = tmp_assign_source_11;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_12;
                PyObject *tmp_unpack_5;
                CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
                tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
                if ( tmp_assign_source_12 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 273;
                    goto try_except_handler_7;
                }
                assert( tmp_tuple_unpack_3__element_1 == NULL );
                tmp_tuple_unpack_3__element_1 = tmp_assign_source_12;
            }
            {
                PyObject *tmp_assign_source_13;
                PyObject *tmp_unpack_6;
                CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
                tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
                if ( tmp_assign_source_13 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 273;
                    goto try_except_handler_7;
                }
                assert( tmp_tuple_unpack_3__element_2 == NULL );
                tmp_tuple_unpack_3__element_2 = tmp_assign_source_13;
            }
            {
                PyObject *tmp_iterator_name_3;
                CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooooooooooooooo";
                            exception_lineno = 273;
                            goto try_except_handler_7;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 273;
                    goto try_except_handler_7;
                }
            }
            goto try_end_5;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
            Py_DECREF( tmp_tuple_unpack_3__source_iter );
            tmp_tuple_unpack_3__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto try_except_handler_6;
            // End of try:
            try_end_5:;
            goto try_end_6;
            // Exception handler code:
            try_except_handler_6:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_3__element_1 );
            tmp_tuple_unpack_3__element_1 = NULL;

            Py_XDECREF( tmp_tuple_unpack_3__element_2 );
            tmp_tuple_unpack_3__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto frame_exception_exit_1;
            // End of try:
            try_end_6:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
            Py_DECREF( tmp_tuple_unpack_3__source_iter );
            tmp_tuple_unpack_3__source_iter = NULL;

            {
                PyObject *tmp_assign_source_14;
                CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
                tmp_assign_source_14 = tmp_tuple_unpack_3__element_1;
                assert( var_viewlim_low == NULL );
                Py_INCREF( tmp_assign_source_14 );
                var_viewlim_low = tmp_assign_source_14;
            }
            Py_XDECREF( tmp_tuple_unpack_3__element_1 );
            tmp_tuple_unpack_3__element_1 = NULL;

            {
                PyObject *tmp_assign_source_15;
                CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
                tmp_assign_source_15 = tmp_tuple_unpack_3__element_2;
                assert( var_viewlim_high == NULL );
                Py_INCREF( tmp_assign_source_15 );
                var_viewlim_high = tmp_assign_source_15;
            }
            Py_XDECREF( tmp_tuple_unpack_3__element_2 );
            tmp_tuple_unpack_3__element_2 = NULL;

            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_compexpr_left_5;
                PyObject *tmp_compexpr_right_5;
                PyObject *tmp_source_name_13;
                CHECK_OBJECT( par_self );
                tmp_source_name_13 = par_self;
                tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_spine_type );
                if ( tmp_compexpr_left_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 275;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_5 = const_tuple_str_plain_left_str_plain_right_tuple;
                tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
                Py_DECREF( tmp_compexpr_left_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 275;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                // Tried code:
                {
                    PyObject *tmp_assign_source_16;
                    PyObject *tmp_iter_arg_4;
                    PyObject *tmp_source_name_14;
                    PyObject *tmp_source_name_15;
                    PyObject *tmp_source_name_16;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_16 = par_self;
                    tmp_source_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_axes );
                    if ( tmp_source_name_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 276;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_8;
                    }
                    tmp_source_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_dataLim );
                    Py_DECREF( tmp_source_name_15 );
                    if ( tmp_source_name_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 276;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_8;
                    }
                    tmp_iter_arg_4 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_intervaly );
                    Py_DECREF( tmp_source_name_14 );
                    if ( tmp_iter_arg_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 276;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_8;
                    }
                    tmp_assign_source_16 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
                    Py_DECREF( tmp_iter_arg_4 );
                    if ( tmp_assign_source_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 276;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_8;
                    }
                    assert( tmp_tuple_unpack_4__source_iter == NULL );
                    tmp_tuple_unpack_4__source_iter = tmp_assign_source_16;
                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_17;
                    PyObject *tmp_unpack_7;
                    CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
                    tmp_unpack_7 = tmp_tuple_unpack_4__source_iter;
                    tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_7, 0, 2 );
                    if ( tmp_assign_source_17 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 276;
                        goto try_except_handler_9;
                    }
                    assert( tmp_tuple_unpack_4__element_1 == NULL );
                    tmp_tuple_unpack_4__element_1 = tmp_assign_source_17;
                }
                {
                    PyObject *tmp_assign_source_18;
                    PyObject *tmp_unpack_8;
                    CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
                    tmp_unpack_8 = tmp_tuple_unpack_4__source_iter;
                    tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_8, 1, 2 );
                    if ( tmp_assign_source_18 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 276;
                        goto try_except_handler_9;
                    }
                    assert( tmp_tuple_unpack_4__element_2 == NULL );
                    tmp_tuple_unpack_4__element_2 = tmp_assign_source_18;
                }
                {
                    PyObject *tmp_iterator_name_4;
                    CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
                    tmp_iterator_name_4 = tmp_tuple_unpack_4__source_iter;
                    // Check if iterator has left-over elements.
                    CHECK_OBJECT( tmp_iterator_name_4 ); assert( HAS_ITERNEXT( tmp_iterator_name_4 ) );

                    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_4 )->tp_iternext)( tmp_iterator_name_4 );

                    if (likely( tmp_iterator_attempt == NULL ))
                    {
                        PyObject *error = GET_ERROR_OCCURRED();

                        if ( error != NULL )
                        {
                            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                            {
                                CLEAR_ERROR_OCCURRED();
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                type_description_1 = "ooooooooooooooooo";
                                exception_lineno = 276;
                                goto try_except_handler_9;
                            }
                        }
                    }
                    else
                    {
                        Py_DECREF( tmp_iterator_attempt );

                        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 276;
                        goto try_except_handler_9;
                    }
                }
                goto try_end_7;
                // Exception handler code:
                try_except_handler_9:;
                exception_keeper_type_7 = exception_type;
                exception_keeper_value_7 = exception_value;
                exception_keeper_tb_7 = exception_tb;
                exception_keeper_lineno_7 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
                Py_DECREF( tmp_tuple_unpack_4__source_iter );
                tmp_tuple_unpack_4__source_iter = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_7;
                exception_value = exception_keeper_value_7;
                exception_tb = exception_keeper_tb_7;
                exception_lineno = exception_keeper_lineno_7;

                goto try_except_handler_8;
                // End of try:
                try_end_7:;
                goto try_end_8;
                // Exception handler code:
                try_except_handler_8:;
                exception_keeper_type_8 = exception_type;
                exception_keeper_value_8 = exception_value;
                exception_keeper_tb_8 = exception_tb;
                exception_keeper_lineno_8 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_tuple_unpack_4__element_1 );
                tmp_tuple_unpack_4__element_1 = NULL;

                Py_XDECREF( tmp_tuple_unpack_4__element_2 );
                tmp_tuple_unpack_4__element_2 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_8;
                exception_value = exception_keeper_value_8;
                exception_tb = exception_keeper_tb_8;
                exception_lineno = exception_keeper_lineno_8;

                goto frame_exception_exit_1;
                // End of try:
                try_end_8:;
                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
                Py_DECREF( tmp_tuple_unpack_4__source_iter );
                tmp_tuple_unpack_4__source_iter = NULL;

                {
                    PyObject *tmp_assign_source_19;
                    CHECK_OBJECT( tmp_tuple_unpack_4__element_1 );
                    tmp_assign_source_19 = tmp_tuple_unpack_4__element_1;
                    assert( var_datalim_low == NULL );
                    Py_INCREF( tmp_assign_source_19 );
                    var_datalim_low = tmp_assign_source_19;
                }
                Py_XDECREF( tmp_tuple_unpack_4__element_1 );
                tmp_tuple_unpack_4__element_1 = NULL;

                {
                    PyObject *tmp_assign_source_20;
                    CHECK_OBJECT( tmp_tuple_unpack_4__element_2 );
                    tmp_assign_source_20 = tmp_tuple_unpack_4__element_2;
                    assert( var_datalim_high == NULL );
                    Py_INCREF( tmp_assign_source_20 );
                    var_datalim_high = tmp_assign_source_20;
                }
                Py_XDECREF( tmp_tuple_unpack_4__element_2 );
                tmp_tuple_unpack_4__element_2 = NULL;

                {
                    PyObject *tmp_assign_source_21;
                    PyObject *tmp_called_instance_1;
                    PyObject *tmp_source_name_17;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_17 = par_self;
                    tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_axes );
                    if ( tmp_called_instance_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 277;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 277;
                    tmp_assign_source_21 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_yticks );
                    Py_DECREF( tmp_called_instance_1 );
                    if ( tmp_assign_source_21 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 277;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_ticks == NULL );
                    var_ticks = tmp_assign_source_21;
                }
                goto branch_end_6;
                branch_no_6:;
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_compexpr_left_6;
                    PyObject *tmp_compexpr_right_6;
                    PyObject *tmp_source_name_18;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_18 = par_self;
                    tmp_compexpr_left_6 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_spine_type );
                    if ( tmp_compexpr_left_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 278;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_6 = const_tuple_str_plain_top_str_plain_bottom_tuple;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_6, tmp_compexpr_left_6 );
                    Py_DECREF( tmp_compexpr_left_6 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 278;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_7;
                    }
                    else
                    {
                        goto branch_no_7;
                    }
                    branch_yes_7:;
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_22;
                        PyObject *tmp_iter_arg_5;
                        PyObject *tmp_source_name_19;
                        PyObject *tmp_source_name_20;
                        PyObject *tmp_source_name_21;
                        CHECK_OBJECT( par_self );
                        tmp_source_name_21 = par_self;
                        tmp_source_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_axes );
                        if ( tmp_source_name_20 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 279;
                            type_description_1 = "ooooooooooooooooo";
                            goto try_except_handler_10;
                        }
                        tmp_source_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_dataLim );
                        Py_DECREF( tmp_source_name_20 );
                        if ( tmp_source_name_19 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 279;
                            type_description_1 = "ooooooooooooooooo";
                            goto try_except_handler_10;
                        }
                        tmp_iter_arg_5 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_intervalx );
                        Py_DECREF( tmp_source_name_19 );
                        if ( tmp_iter_arg_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 279;
                            type_description_1 = "ooooooooooooooooo";
                            goto try_except_handler_10;
                        }
                        tmp_assign_source_22 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_5 );
                        Py_DECREF( tmp_iter_arg_5 );
                        if ( tmp_assign_source_22 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 279;
                            type_description_1 = "ooooooooooooooooo";
                            goto try_except_handler_10;
                        }
                        assert( tmp_tuple_unpack_5__source_iter == NULL );
                        tmp_tuple_unpack_5__source_iter = tmp_assign_source_22;
                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_23;
                        PyObject *tmp_unpack_9;
                        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                        tmp_unpack_9 = tmp_tuple_unpack_5__source_iter;
                        tmp_assign_source_23 = UNPACK_NEXT( tmp_unpack_9, 0, 2 );
                        if ( tmp_assign_source_23 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooooooooooooooo";
                            exception_lineno = 279;
                            goto try_except_handler_11;
                        }
                        assert( tmp_tuple_unpack_5__element_1 == NULL );
                        tmp_tuple_unpack_5__element_1 = tmp_assign_source_23;
                    }
                    {
                        PyObject *tmp_assign_source_24;
                        PyObject *tmp_unpack_10;
                        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                        tmp_unpack_10 = tmp_tuple_unpack_5__source_iter;
                        tmp_assign_source_24 = UNPACK_NEXT( tmp_unpack_10, 1, 2 );
                        if ( tmp_assign_source_24 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooooooooooooooo";
                            exception_lineno = 279;
                            goto try_except_handler_11;
                        }
                        assert( tmp_tuple_unpack_5__element_2 == NULL );
                        tmp_tuple_unpack_5__element_2 = tmp_assign_source_24;
                    }
                    {
                        PyObject *tmp_iterator_name_5;
                        CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                        tmp_iterator_name_5 = tmp_tuple_unpack_5__source_iter;
                        // Check if iterator has left-over elements.
                        CHECK_OBJECT( tmp_iterator_name_5 ); assert( HAS_ITERNEXT( tmp_iterator_name_5 ) );

                        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_5 )->tp_iternext)( tmp_iterator_name_5 );

                        if (likely( tmp_iterator_attempt == NULL ))
                        {
                            PyObject *error = GET_ERROR_OCCURRED();

                            if ( error != NULL )
                            {
                                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                                {
                                    CLEAR_ERROR_OCCURRED();
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                    type_description_1 = "ooooooooooooooooo";
                                    exception_lineno = 279;
                                    goto try_except_handler_11;
                                }
                            }
                        }
                        else
                        {
                            Py_DECREF( tmp_iterator_attempt );

                            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooooooooooooooo";
                            exception_lineno = 279;
                            goto try_except_handler_11;
                        }
                    }
                    goto try_end_9;
                    // Exception handler code:
                    try_except_handler_11:;
                    exception_keeper_type_9 = exception_type;
                    exception_keeper_value_9 = exception_value;
                    exception_keeper_tb_9 = exception_tb;
                    exception_keeper_lineno_9 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
                    Py_DECREF( tmp_tuple_unpack_5__source_iter );
                    tmp_tuple_unpack_5__source_iter = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_9;
                    exception_value = exception_keeper_value_9;
                    exception_tb = exception_keeper_tb_9;
                    exception_lineno = exception_keeper_lineno_9;

                    goto try_except_handler_10;
                    // End of try:
                    try_end_9:;
                    goto try_end_10;
                    // Exception handler code:
                    try_except_handler_10:;
                    exception_keeper_type_10 = exception_type;
                    exception_keeper_value_10 = exception_value;
                    exception_keeper_tb_10 = exception_tb;
                    exception_keeper_lineno_10 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
                    tmp_tuple_unpack_5__element_1 = NULL;

                    Py_XDECREF( tmp_tuple_unpack_5__element_2 );
                    tmp_tuple_unpack_5__element_2 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_10;
                    exception_value = exception_keeper_value_10;
                    exception_tb = exception_keeper_tb_10;
                    exception_lineno = exception_keeper_lineno_10;

                    goto frame_exception_exit_1;
                    // End of try:
                    try_end_10:;
                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
                    Py_DECREF( tmp_tuple_unpack_5__source_iter );
                    tmp_tuple_unpack_5__source_iter = NULL;

                    {
                        PyObject *tmp_assign_source_25;
                        CHECK_OBJECT( tmp_tuple_unpack_5__element_1 );
                        tmp_assign_source_25 = tmp_tuple_unpack_5__element_1;
                        assert( var_datalim_low == NULL );
                        Py_INCREF( tmp_assign_source_25 );
                        var_datalim_low = tmp_assign_source_25;
                    }
                    Py_XDECREF( tmp_tuple_unpack_5__element_1 );
                    tmp_tuple_unpack_5__element_1 = NULL;

                    {
                        PyObject *tmp_assign_source_26;
                        CHECK_OBJECT( tmp_tuple_unpack_5__element_2 );
                        tmp_assign_source_26 = tmp_tuple_unpack_5__element_2;
                        assert( var_datalim_high == NULL );
                        Py_INCREF( tmp_assign_source_26 );
                        var_datalim_high = tmp_assign_source_26;
                    }
                    Py_XDECREF( tmp_tuple_unpack_5__element_2 );
                    tmp_tuple_unpack_5__element_2 = NULL;

                    {
                        PyObject *tmp_assign_source_27;
                        PyObject *tmp_called_instance_2;
                        PyObject *tmp_source_name_22;
                        CHECK_OBJECT( par_self );
                        tmp_source_name_22 = par_self;
                        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_axes );
                        if ( tmp_called_instance_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 280;
                            type_description_1 = "ooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 280;
                        tmp_assign_source_27 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_xticks );
                        Py_DECREF( tmp_called_instance_2 );
                        if ( tmp_assign_source_27 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 280;
                            type_description_1 = "ooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        assert( var_ticks == NULL );
                        var_ticks = tmp_assign_source_27;
                    }
                    branch_no_7:;
                }
                branch_end_6:;
            }
            {
                PyObject *tmp_assign_source_28;
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_23;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_2;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_np );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 282;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_23 = tmp_mvar_value_1;
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_sort );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 282;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                if ( var_ticks == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ticks" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 282;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_args_element_name_2 = var_ticks;
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 282;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_called_name_2 );
                if ( tmp_assign_source_28 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 282;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_ticks;
                    var_ticks = tmp_assign_source_28;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_29;
                PyObject *tmp_iter_arg_6;
                PyObject *tmp_called_name_3;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_list_element_2;
                tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_sorted );
                assert( tmp_called_name_3 != NULL );
                if ( var_datalim_low == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "datalim_low" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 283;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_12;
                }

                tmp_list_element_2 = var_datalim_low;
                tmp_args_element_name_3 = PyList_New( 2 );
                Py_INCREF( tmp_list_element_2 );
                PyList_SET_ITEM( tmp_args_element_name_3, 0, tmp_list_element_2 );
                if ( var_datalim_high == NULL )
                {
                    Py_DECREF( tmp_args_element_name_3 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "datalim_high" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 283;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_12;
                }

                tmp_list_element_2 = var_datalim_high;
                Py_INCREF( tmp_list_element_2 );
                PyList_SET_ITEM( tmp_args_element_name_3, 1, tmp_list_element_2 );
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 283;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_iter_arg_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                }

                Py_DECREF( tmp_args_element_name_3 );
                if ( tmp_iter_arg_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 283;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_12;
                }
                tmp_assign_source_29 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
                Py_DECREF( tmp_iter_arg_6 );
                if ( tmp_assign_source_29 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 283;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_12;
                }
                assert( tmp_tuple_unpack_6__source_iter == NULL );
                tmp_tuple_unpack_6__source_iter = tmp_assign_source_29;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_30;
                PyObject *tmp_unpack_11;
                CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
                tmp_unpack_11 = tmp_tuple_unpack_6__source_iter;
                tmp_assign_source_30 = UNPACK_NEXT( tmp_unpack_11, 0, 2 );
                if ( tmp_assign_source_30 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 283;
                    goto try_except_handler_13;
                }
                assert( tmp_tuple_unpack_6__element_1 == NULL );
                tmp_tuple_unpack_6__element_1 = tmp_assign_source_30;
            }
            {
                PyObject *tmp_assign_source_31;
                PyObject *tmp_unpack_12;
                CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
                tmp_unpack_12 = tmp_tuple_unpack_6__source_iter;
                tmp_assign_source_31 = UNPACK_NEXT( tmp_unpack_12, 1, 2 );
                if ( tmp_assign_source_31 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 283;
                    goto try_except_handler_13;
                }
                assert( tmp_tuple_unpack_6__element_2 == NULL );
                tmp_tuple_unpack_6__element_2 = tmp_assign_source_31;
            }
            {
                PyObject *tmp_iterator_name_6;
                CHECK_OBJECT( tmp_tuple_unpack_6__source_iter );
                tmp_iterator_name_6 = tmp_tuple_unpack_6__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_6 ); assert( HAS_ITERNEXT( tmp_iterator_name_6 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_6 )->tp_iternext)( tmp_iterator_name_6 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooooooooooooooo";
                            exception_lineno = 283;
                            goto try_except_handler_13;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooooooo";
                    exception_lineno = 283;
                    goto try_except_handler_13;
                }
            }
            goto try_end_11;
            // Exception handler code:
            try_except_handler_13:;
            exception_keeper_type_11 = exception_type;
            exception_keeper_value_11 = exception_value;
            exception_keeper_tb_11 = exception_tb;
            exception_keeper_lineno_11 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
            Py_DECREF( tmp_tuple_unpack_6__source_iter );
            tmp_tuple_unpack_6__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_11;
            exception_value = exception_keeper_value_11;
            exception_tb = exception_keeper_tb_11;
            exception_lineno = exception_keeper_lineno_11;

            goto try_except_handler_12;
            // End of try:
            try_end_11:;
            goto try_end_12;
            // Exception handler code:
            try_except_handler_12:;
            exception_keeper_type_12 = exception_type;
            exception_keeper_value_12 = exception_value;
            exception_keeper_tb_12 = exception_tb;
            exception_keeper_lineno_12 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_6__element_1 );
            tmp_tuple_unpack_6__element_1 = NULL;

            Py_XDECREF( tmp_tuple_unpack_6__element_2 );
            tmp_tuple_unpack_6__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_12;
            exception_value = exception_keeper_value_12;
            exception_tb = exception_keeper_tb_12;
            exception_lineno = exception_keeper_lineno_12;

            goto frame_exception_exit_1;
            // End of try:
            try_end_12:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_6__source_iter );
            Py_DECREF( tmp_tuple_unpack_6__source_iter );
            tmp_tuple_unpack_6__source_iter = NULL;

            {
                PyObject *tmp_assign_source_32;
                CHECK_OBJECT( tmp_tuple_unpack_6__element_1 );
                tmp_assign_source_32 = tmp_tuple_unpack_6__element_1;
                {
                    PyObject *old = var_datalim_low;
                    var_datalim_low = tmp_assign_source_32;
                    Py_INCREF( var_datalim_low );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_6__element_1 );
            tmp_tuple_unpack_6__element_1 = NULL;

            {
                PyObject *tmp_assign_source_33;
                CHECK_OBJECT( tmp_tuple_unpack_6__element_2 );
                tmp_assign_source_33 = tmp_tuple_unpack_6__element_2;
                {
                    PyObject *old = var_datalim_high;
                    var_datalim_high = tmp_assign_source_33;
                    Py_INCREF( var_datalim_high );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_6__element_2 );
            tmp_tuple_unpack_6__element_2 = NULL;

            {
                nuitka_bool tmp_condition_result_8;
                PyObject *tmp_compexpr_left_7;
                PyObject *tmp_compexpr_right_7;
                CHECK_OBJECT( var_datalim_low );
                tmp_compexpr_left_7 = var_datalim_low;
                CHECK_OBJECT( var_viewlim_low );
                tmp_compexpr_right_7 = var_viewlim_low;
                tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 285;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_assign_source_34;
                    CHECK_OBJECT( var_viewlim_low );
                    tmp_assign_source_34 = var_viewlim_low;
                    {
                        PyObject *old = var_low;
                        var_low = tmp_assign_source_34;
                        Py_INCREF( var_low );
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_8;
                branch_no_8:;
                {
                    PyObject *tmp_assign_source_35;
                    PyObject *tmp_left_name_2;
                    PyObject *tmp_compexpr_left_8;
                    PyObject *tmp_compexpr_right_8;
                    PyObject *tmp_right_name_2;
                    PyObject *tmp_compexpr_left_9;
                    PyObject *tmp_compexpr_right_9;
                    CHECK_OBJECT( var_ticks );
                    tmp_compexpr_left_8 = var_ticks;
                    CHECK_OBJECT( var_datalim_low );
                    tmp_compexpr_right_8 = var_datalim_low;
                    tmp_left_name_2 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                    if ( tmp_left_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 290;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_ticks );
                    tmp_compexpr_left_9 = var_ticks;
                    CHECK_OBJECT( var_viewlim_low );
                    tmp_compexpr_right_9 = var_viewlim_low;
                    tmp_right_name_2 = RICH_COMPARE_GTE_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                    if ( tmp_right_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_2 );

                        exception_lineno = 290;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_35 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_2 );
                    Py_DECREF( tmp_left_name_2 );
                    Py_DECREF( tmp_right_name_2 );
                    if ( tmp_assign_source_35 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 290;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_cond == NULL );
                    var_cond = tmp_assign_source_35;
                }
                {
                    PyObject *tmp_assign_source_36;
                    PyObject *tmp_subscribed_name_1;
                    PyObject *tmp_subscript_name_1;
                    CHECK_OBJECT( var_ticks );
                    tmp_subscribed_name_1 = var_ticks;
                    CHECK_OBJECT( var_cond );
                    tmp_subscript_name_1 = var_cond;
                    tmp_assign_source_36 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                    if ( tmp_assign_source_36 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 291;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_tickvals == NULL );
                    var_tickvals = tmp_assign_source_36;
                }
                {
                    nuitka_bool tmp_condition_result_9;
                    PyObject *tmp_len_arg_1;
                    PyObject *tmp_capi_result_1;
                    int tmp_truth_name_2;
                    CHECK_OBJECT( var_tickvals );
                    tmp_len_arg_1 = var_tickvals;
                    tmp_capi_result_1 = BUILTIN_LEN( tmp_len_arg_1 );
                    if ( tmp_capi_result_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 292;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_truth_name_2 = CHECK_IF_TRUE( tmp_capi_result_1 );
                    if ( tmp_truth_name_2 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_capi_result_1 );

                        exception_lineno = 292;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_9 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    Py_DECREF( tmp_capi_result_1 );
                    if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_9;
                    }
                    else
                    {
                        goto branch_no_9;
                    }
                    branch_yes_9:;
                    {
                        PyObject *tmp_assign_source_37;
                        PyObject *tmp_subscribed_name_2;
                        PyObject *tmp_subscript_name_2;
                        CHECK_OBJECT( var_tickvals );
                        tmp_subscribed_name_2 = var_tickvals;
                        tmp_subscript_name_2 = const_int_neg_1;
                        tmp_assign_source_37 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, -1 );
                        if ( tmp_assign_source_37 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 294;
                            type_description_1 = "ooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        {
                            PyObject *old = var_low;
                            var_low = tmp_assign_source_37;
                            Py_XDECREF( old );
                        }

                    }
                    goto branch_end_9;
                    branch_no_9:;
                    {
                        PyObject *tmp_assign_source_38;
                        CHECK_OBJECT( var_datalim_low );
                        tmp_assign_source_38 = var_datalim_low;
                        {
                            PyObject *old = var_low;
                            var_low = tmp_assign_source_38;
                            Py_INCREF( var_low );
                            Py_XDECREF( old );
                        }

                    }
                    branch_end_9:;
                }
                {
                    PyObject *tmp_assign_source_39;
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_args_element_name_4;
                    PyObject *tmp_args_element_name_5;
                    tmp_called_name_4 = LOOKUP_BUILTIN( const_str_plain_max );
                    assert( tmp_called_name_4 != NULL );
                    CHECK_OBJECT( var_low );
                    tmp_args_element_name_4 = var_low;
                    CHECK_OBJECT( var_viewlim_low );
                    tmp_args_element_name_5 = var_viewlim_low;
                    frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 298;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                        tmp_assign_source_39 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
                    }

                    if ( tmp_assign_source_39 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 298;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_low;
                        assert( old != NULL );
                        var_low = tmp_assign_source_39;
                        Py_DECREF( old );
                    }

                }
                branch_end_8:;
            }
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_10;
                PyObject *tmp_compexpr_right_10;
                CHECK_OBJECT( var_datalim_high );
                tmp_compexpr_left_10 = var_datalim_high;
                CHECK_OBJECT( var_viewlim_high );
                tmp_compexpr_right_10 = var_viewlim_high;
                tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 300;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    PyObject *tmp_assign_source_40;
                    CHECK_OBJECT( var_viewlim_high );
                    tmp_assign_source_40 = var_viewlim_high;
                    {
                        PyObject *old = var_high;
                        var_high = tmp_assign_source_40;
                        Py_INCREF( var_high );
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_10;
                branch_no_10:;
                {
                    PyObject *tmp_assign_source_41;
                    PyObject *tmp_left_name_3;
                    PyObject *tmp_compexpr_left_11;
                    PyObject *tmp_compexpr_right_11;
                    PyObject *tmp_right_name_3;
                    PyObject *tmp_compexpr_left_12;
                    PyObject *tmp_compexpr_right_12;
                    CHECK_OBJECT( var_ticks );
                    tmp_compexpr_left_11 = var_ticks;
                    CHECK_OBJECT( var_datalim_high );
                    tmp_compexpr_right_11 = var_datalim_high;
                    tmp_left_name_3 = RICH_COMPARE_GTE_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                    if ( tmp_left_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 305;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_ticks );
                    tmp_compexpr_left_12 = var_ticks;
                    CHECK_OBJECT( var_viewlim_high );
                    tmp_compexpr_right_12 = var_viewlim_high;
                    tmp_right_name_3 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
                    if ( tmp_right_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_3 );

                        exception_lineno = 305;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_41 = BINARY_OPERATION( PyNumber_And, tmp_left_name_3, tmp_right_name_3 );
                    Py_DECREF( tmp_left_name_3 );
                    Py_DECREF( tmp_right_name_3 );
                    if ( tmp_assign_source_41 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 305;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_cond;
                        var_cond = tmp_assign_source_41;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_42;
                    PyObject *tmp_subscribed_name_3;
                    PyObject *tmp_subscript_name_3;
                    CHECK_OBJECT( var_ticks );
                    tmp_subscribed_name_3 = var_ticks;
                    CHECK_OBJECT( var_cond );
                    tmp_subscript_name_3 = var_cond;
                    tmp_assign_source_42 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                    if ( tmp_assign_source_42 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 306;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_tickvals;
                        var_tickvals = tmp_assign_source_42;
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_11;
                    PyObject *tmp_len_arg_2;
                    PyObject *tmp_capi_result_2;
                    int tmp_truth_name_3;
                    CHECK_OBJECT( var_tickvals );
                    tmp_len_arg_2 = var_tickvals;
                    tmp_capi_result_2 = BUILTIN_LEN( tmp_len_arg_2 );
                    if ( tmp_capi_result_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 307;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_truth_name_3 = CHECK_IF_TRUE( tmp_capi_result_2 );
                    if ( tmp_truth_name_3 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_capi_result_2 );

                        exception_lineno = 307;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_11 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    Py_DECREF( tmp_capi_result_2 );
                    if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_11;
                    }
                    else
                    {
                        goto branch_no_11;
                    }
                    branch_yes_11:;
                    {
                        PyObject *tmp_assign_source_43;
                        PyObject *tmp_subscribed_name_4;
                        PyObject *tmp_subscript_name_4;
                        CHECK_OBJECT( var_tickvals );
                        tmp_subscribed_name_4 = var_tickvals;
                        tmp_subscript_name_4 = const_int_0;
                        tmp_assign_source_43 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
                        if ( tmp_assign_source_43 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 310;
                            type_description_1 = "ooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        {
                            PyObject *old = var_high;
                            var_high = tmp_assign_source_43;
                            Py_XDECREF( old );
                        }

                    }
                    goto branch_end_11;
                    branch_no_11:;
                    {
                        PyObject *tmp_assign_source_44;
                        CHECK_OBJECT( var_datalim_high );
                        tmp_assign_source_44 = var_datalim_high;
                        {
                            PyObject *old = var_high;
                            var_high = tmp_assign_source_44;
                            Py_INCREF( var_high );
                            Py_XDECREF( old );
                        }

                    }
                    branch_end_11:;
                }
                {
                    PyObject *tmp_assign_source_45;
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_args_element_name_7;
                    tmp_called_name_5 = LOOKUP_BUILTIN( const_str_plain_min );
                    assert( tmp_called_name_5 != NULL );
                    CHECK_OBJECT( var_high );
                    tmp_args_element_name_6 = var_high;
                    CHECK_OBJECT( var_viewlim_high );
                    tmp_args_element_name_7 = var_viewlim_high;
                    frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 314;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                        tmp_assign_source_45 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
                    }

                    if ( tmp_assign_source_45 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 314;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_high;
                        assert( old != NULL );
                        var_high = tmp_assign_source_45;
                        Py_DECREF( old );
                    }

                }
                branch_end_10:;
            }
            branch_no_5:;
        }
        goto branch_end_2;
        branch_no_2:;
        // Tried code:
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_iter_arg_7;
            PyObject *tmp_source_name_24;
            CHECK_OBJECT( par_self );
            tmp_source_name_24 = par_self;
            tmp_iter_arg_7 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain__bounds );
            if ( tmp_iter_arg_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_14;
            }
            tmp_assign_source_46 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_7 );
            Py_DECREF( tmp_iter_arg_7 );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_14;
            }
            assert( tmp_tuple_unpack_7__source_iter == NULL );
            tmp_tuple_unpack_7__source_iter = tmp_assign_source_46;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_47;
            PyObject *tmp_unpack_13;
            CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
            tmp_unpack_13 = tmp_tuple_unpack_7__source_iter;
            tmp_assign_source_47 = UNPACK_NEXT( tmp_unpack_13, 0, 2 );
            if ( tmp_assign_source_47 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooooo";
                exception_lineno = 317;
                goto try_except_handler_15;
            }
            assert( tmp_tuple_unpack_7__element_1 == NULL );
            tmp_tuple_unpack_7__element_1 = tmp_assign_source_47;
        }
        {
            PyObject *tmp_assign_source_48;
            PyObject *tmp_unpack_14;
            CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
            tmp_unpack_14 = tmp_tuple_unpack_7__source_iter;
            tmp_assign_source_48 = UNPACK_NEXT( tmp_unpack_14, 1, 2 );
            if ( tmp_assign_source_48 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooooo";
                exception_lineno = 317;
                goto try_except_handler_15;
            }
            assert( tmp_tuple_unpack_7__element_2 == NULL );
            tmp_tuple_unpack_7__element_2 = tmp_assign_source_48;
        }
        {
            PyObject *tmp_iterator_name_7;
            CHECK_OBJECT( tmp_tuple_unpack_7__source_iter );
            tmp_iterator_name_7 = tmp_tuple_unpack_7__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_7 ); assert( HAS_ITERNEXT( tmp_iterator_name_7 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_7 )->tp_iternext)( tmp_iterator_name_7 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 317;
                        goto try_except_handler_15;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooooooo";
                exception_lineno = 317;
                goto try_except_handler_15;
            }
        }
        goto try_end_13;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_7__source_iter );
        Py_DECREF( tmp_tuple_unpack_7__source_iter );
        tmp_tuple_unpack_7__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto try_except_handler_14;
        // End of try:
        try_end_13:;
        goto try_end_14;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_7__element_1 );
        tmp_tuple_unpack_7__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_7__element_2 );
        tmp_tuple_unpack_7__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto frame_exception_exit_1;
        // End of try:
        try_end_14:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_7__source_iter );
        Py_DECREF( tmp_tuple_unpack_7__source_iter );
        tmp_tuple_unpack_7__source_iter = NULL;

        {
            PyObject *tmp_assign_source_49;
            CHECK_OBJECT( tmp_tuple_unpack_7__element_1 );
            tmp_assign_source_49 = tmp_tuple_unpack_7__element_1;
            assert( var_low == NULL );
            Py_INCREF( tmp_assign_source_49 );
            var_low = tmp_assign_source_49;
        }
        Py_XDECREF( tmp_tuple_unpack_7__element_1 );
        tmp_tuple_unpack_7__element_1 = NULL;

        {
            PyObject *tmp_assign_source_50;
            CHECK_OBJECT( tmp_tuple_unpack_7__element_2 );
            tmp_assign_source_50 = tmp_tuple_unpack_7__element_2;
            assert( var_high == NULL );
            Py_INCREF( tmp_assign_source_50 );
            var_high = tmp_assign_source_50;
        }
        Py_XDECREF( tmp_tuple_unpack_7__element_2 );
        tmp_tuple_unpack_7__element_2 = NULL;

        branch_end_2:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_compexpr_left_13;
        PyObject *tmp_compexpr_right_13;
        PyObject *tmp_source_name_25;
        CHECK_OBJECT( par_self );
        tmp_source_name_25 = par_self;
        tmp_compexpr_left_13 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain__patch_type );
        if ( tmp_compexpr_left_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_13 = const_str_plain_arc;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
        Py_DECREF( tmp_compexpr_left_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "ooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_14;
            PyObject *tmp_compexpr_right_14;
            PyObject *tmp_source_name_26;
            CHECK_OBJECT( par_self );
            tmp_source_name_26 = par_self;
            tmp_compexpr_left_14 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_spine_type );
            if ( tmp_compexpr_left_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_14 = const_tuple_str_plain_bottom_str_plain_top_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_14, tmp_compexpr_left_14 );
            Py_DECREF( tmp_compexpr_left_14 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_13 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            // Tried code:
            {
                PyObject *tmp_assign_source_51;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_source_name_27;
                CHECK_OBJECT( par_self );
                tmp_source_name_27 = par_self;
                tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_axes );
                if ( tmp_called_instance_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 322;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_16;
                }
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 322;
                tmp_assign_source_51 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_theta_direction );
                Py_DECREF( tmp_called_instance_3 );
                if ( tmp_assign_source_51 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 322;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_16;
                }
                assert( var_direction == NULL );
                var_direction = tmp_assign_source_51;
            }
            goto try_end_15;
            // Exception handler code:
            try_except_handler_16:;
            exception_keeper_type_15 = exception_type;
            exception_keeper_value_15 = exception_value;
            exception_keeper_tb_15 = exception_tb;
            exception_keeper_lineno_15 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_1 );
            exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_1 );
            exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_1 );

            if ( exception_keeper_tb_15 == NULL )
            {
                exception_keeper_tb_15 = MAKE_TRACEBACK( frame_1732b7288de3ff07b5d0689f845f9ced, exception_keeper_lineno_15 );
            }
            else if ( exception_keeper_lineno_15 != 0 )
            {
                exception_keeper_tb_15 = ADD_TRACEBACK( exception_keeper_tb_15, frame_1732b7288de3ff07b5d0689f845f9ced, exception_keeper_lineno_15 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_15, &exception_keeper_value_15, &exception_keeper_tb_15 );
            PyException_SetTraceback( exception_keeper_value_15, (PyObject *)exception_keeper_tb_15 );
            PUBLISH_EXCEPTION( &exception_keeper_type_15, &exception_keeper_value_15, &exception_keeper_tb_15 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_14;
                PyObject *tmp_compexpr_left_15;
                PyObject *tmp_compexpr_right_15;
                tmp_compexpr_left_15 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_15 = PyExc_AttributeError;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_15, tmp_compexpr_right_15 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 323;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_17;
                }
                tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_14;
                }
                else
                {
                    goto branch_no_14;
                }
                branch_yes_14:;
                {
                    PyObject *tmp_assign_source_52;
                    tmp_assign_source_52 = const_int_pos_1;
                    assert( var_direction == NULL );
                    Py_INCREF( tmp_assign_source_52 );
                    var_direction = tmp_assign_source_52;
                }
                goto branch_end_14;
                branch_no_14:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 321;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_1732b7288de3ff07b5d0689f845f9ced->m_frame) frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_17;
                branch_end_14:;
            }
            goto try_end_16;
            // Exception handler code:
            try_except_handler_17:;
            exception_keeper_type_16 = exception_type;
            exception_keeper_value_16 = exception_value;
            exception_keeper_tb_16 = exception_tb;
            exception_keeper_lineno_16 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            // Re-raise.
            exception_type = exception_keeper_type_16;
            exception_value = exception_keeper_value_16;
            exception_tb = exception_keeper_tb_16;
            exception_lineno = exception_keeper_lineno_16;

            goto frame_exception_exit_1;
            // End of try:
            try_end_16:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto try_end_15;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_16__adjust_location );
            return NULL;
            // End of try:
            try_end_15:;
            // Tried code:
            {
                PyObject *tmp_assign_source_53;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_source_name_28;
                CHECK_OBJECT( par_self );
                tmp_source_name_28 = par_self;
                tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_axes );
                if ( tmp_called_instance_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 326;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_18;
                }
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 326;
                tmp_assign_source_53 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_get_theta_offset );
                Py_DECREF( tmp_called_instance_4 );
                if ( tmp_assign_source_53 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 326;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_18;
                }
                assert( var_offset == NULL );
                var_offset = tmp_assign_source_53;
            }
            goto try_end_17;
            // Exception handler code:
            try_except_handler_18:;
            exception_keeper_type_17 = exception_type;
            exception_keeper_value_17 = exception_value;
            exception_keeper_tb_17 = exception_tb;
            exception_keeper_lineno_17 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_2 );
            exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_2 );
            exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_2 );

            if ( exception_keeper_tb_17 == NULL )
            {
                exception_keeper_tb_17 = MAKE_TRACEBACK( frame_1732b7288de3ff07b5d0689f845f9ced, exception_keeper_lineno_17 );
            }
            else if ( exception_keeper_lineno_17 != 0 )
            {
                exception_keeper_tb_17 = ADD_TRACEBACK( exception_keeper_tb_17, frame_1732b7288de3ff07b5d0689f845f9ced, exception_keeper_lineno_17 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
            PyException_SetTraceback( exception_keeper_value_17, (PyObject *)exception_keeper_tb_17 );
            PUBLISH_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_15;
                PyObject *tmp_compexpr_left_16;
                PyObject *tmp_compexpr_right_16;
                tmp_compexpr_left_16 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_16 = PyExc_AttributeError;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_16, tmp_compexpr_right_16 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 327;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_19;
                }
                tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_15;
                }
                else
                {
                    goto branch_no_15;
                }
                branch_yes_15:;
                {
                    PyObject *tmp_assign_source_54;
                    tmp_assign_source_54 = const_int_0;
                    assert( var_offset == NULL );
                    Py_INCREF( tmp_assign_source_54 );
                    var_offset = tmp_assign_source_54;
                }
                goto branch_end_15;
                branch_no_15:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 325;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_1732b7288de3ff07b5d0689f845f9ced->m_frame) frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooooooooooooo";
                goto try_except_handler_19;
                branch_end_15:;
            }
            goto try_end_18;
            // Exception handler code:
            try_except_handler_19:;
            exception_keeper_type_18 = exception_type;
            exception_keeper_value_18 = exception_value;
            exception_keeper_tb_18 = exception_tb;
            exception_keeper_lineno_18 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
            // Re-raise.
            exception_type = exception_keeper_type_18;
            exception_value = exception_keeper_value_18;
            exception_tb = exception_keeper_tb_18;
            exception_lineno = exception_keeper_lineno_18;

            goto frame_exception_exit_1;
            // End of try:
            try_end_18:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
            goto try_end_17;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_16__adjust_location );
            return NULL;
            // End of try:
            try_end_17:;
            {
                PyObject *tmp_assign_source_55;
                PyObject *tmp_left_name_4;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_4;
                PyObject *tmp_right_name_5;
                if ( var_low == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "low" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 329;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_5 = var_low;
                if ( var_direction == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "direction" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 329;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_right_name_4 = var_direction;
                tmp_left_name_4 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_4 );
                if ( tmp_left_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 329;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                if ( var_offset == NULL )
                {
                    Py_DECREF( tmp_left_name_4 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "offset" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 329;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_right_name_5 = var_offset;
                tmp_assign_source_55 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_5 );
                Py_DECREF( tmp_left_name_4 );
                if ( tmp_assign_source_55 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 329;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_low;
                    var_low = tmp_assign_source_55;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_56;
                PyObject *tmp_left_name_6;
                PyObject *tmp_left_name_7;
                PyObject *tmp_right_name_6;
                PyObject *tmp_right_name_7;
                if ( var_high == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "high" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 330;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_7 = var_high;
                if ( var_direction == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "direction" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 330;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_right_name_6 = var_direction;
                tmp_left_name_6 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_6 );
                if ( tmp_left_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 330;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                if ( var_offset == NULL )
                {
                    Py_DECREF( tmp_left_name_6 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "offset" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 330;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_right_name_7 = var_offset;
                tmp_assign_source_56 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_7 );
                Py_DECREF( tmp_left_name_6 );
                if ( tmp_assign_source_56 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 330;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_high;
                    var_high = tmp_assign_source_56;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_16;
                PyObject *tmp_compexpr_left_17;
                PyObject *tmp_compexpr_right_17;
                CHECK_OBJECT( var_low );
                tmp_compexpr_left_17 = var_low;
                CHECK_OBJECT( var_high );
                tmp_compexpr_right_17 = var_high;
                tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_17, tmp_compexpr_right_17 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 331;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_16;
                }
                else
                {
                    goto branch_no_16;
                }
                branch_yes_16:;
                {
                    PyObject *tmp_assign_source_57;
                    PyObject *tmp_iter_arg_8;
                    PyObject *tmp_tuple_element_1;
                    CHECK_OBJECT( var_high );
                    tmp_tuple_element_1 = var_high;
                    tmp_iter_arg_8 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_iter_arg_8, 0, tmp_tuple_element_1 );
                    CHECK_OBJECT( var_low );
                    tmp_tuple_element_1 = var_low;
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_iter_arg_8, 1, tmp_tuple_element_1 );
                    tmp_assign_source_57 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_8 );
                    Py_DECREF( tmp_iter_arg_8 );
                    assert( !(tmp_assign_source_57 == NULL) );
                    assert( tmp_tuple_unpack_8__source_iter == NULL );
                    tmp_tuple_unpack_8__source_iter = tmp_assign_source_57;
                }
                // Tried code:
                // Tried code:
                {
                    PyObject *tmp_assign_source_58;
                    PyObject *tmp_unpack_15;
                    CHECK_OBJECT( tmp_tuple_unpack_8__source_iter );
                    tmp_unpack_15 = tmp_tuple_unpack_8__source_iter;
                    tmp_assign_source_58 = UNPACK_NEXT( tmp_unpack_15, 0, 2 );
                    if ( tmp_assign_source_58 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 332;
                        goto try_except_handler_21;
                    }
                    assert( tmp_tuple_unpack_8__element_1 == NULL );
                    tmp_tuple_unpack_8__element_1 = tmp_assign_source_58;
                }
                {
                    PyObject *tmp_assign_source_59;
                    PyObject *tmp_unpack_16;
                    CHECK_OBJECT( tmp_tuple_unpack_8__source_iter );
                    tmp_unpack_16 = tmp_tuple_unpack_8__source_iter;
                    tmp_assign_source_59 = UNPACK_NEXT( tmp_unpack_16, 1, 2 );
                    if ( tmp_assign_source_59 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 332;
                        goto try_except_handler_21;
                    }
                    assert( tmp_tuple_unpack_8__element_2 == NULL );
                    tmp_tuple_unpack_8__element_2 = tmp_assign_source_59;
                }
                goto try_end_19;
                // Exception handler code:
                try_except_handler_21:;
                exception_keeper_type_19 = exception_type;
                exception_keeper_value_19 = exception_value;
                exception_keeper_tb_19 = exception_tb;
                exception_keeper_lineno_19 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_8__source_iter );
                Py_DECREF( tmp_tuple_unpack_8__source_iter );
                tmp_tuple_unpack_8__source_iter = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_19;
                exception_value = exception_keeper_value_19;
                exception_tb = exception_keeper_tb_19;
                exception_lineno = exception_keeper_lineno_19;

                goto try_except_handler_20;
                // End of try:
                try_end_19:;
                goto try_end_20;
                // Exception handler code:
                try_except_handler_20:;
                exception_keeper_type_20 = exception_type;
                exception_keeper_value_20 = exception_value;
                exception_keeper_tb_20 = exception_tb;
                exception_keeper_lineno_20 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_tuple_unpack_8__element_1 );
                tmp_tuple_unpack_8__element_1 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_20;
                exception_value = exception_keeper_value_20;
                exception_tb = exception_keeper_tb_20;
                exception_lineno = exception_keeper_lineno_20;

                goto frame_exception_exit_1;
                // End of try:
                try_end_20:;
                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_8__source_iter );
                Py_DECREF( tmp_tuple_unpack_8__source_iter );
                tmp_tuple_unpack_8__source_iter = NULL;

                {
                    PyObject *tmp_assign_source_60;
                    CHECK_OBJECT( tmp_tuple_unpack_8__element_1 );
                    tmp_assign_source_60 = tmp_tuple_unpack_8__element_1;
                    {
                        PyObject *old = var_low;
                        assert( old != NULL );
                        var_low = tmp_assign_source_60;
                        Py_INCREF( var_low );
                        Py_DECREF( old );
                    }

                }
                Py_XDECREF( tmp_tuple_unpack_8__element_1 );
                tmp_tuple_unpack_8__element_1 = NULL;

                {
                    PyObject *tmp_assign_source_61;
                    CHECK_OBJECT( tmp_tuple_unpack_8__element_2 );
                    tmp_assign_source_61 = tmp_tuple_unpack_8__element_2;
                    {
                        PyObject *old = var_high;
                        assert( old != NULL );
                        var_high = tmp_assign_source_61;
                        Py_INCREF( var_high );
                        Py_DECREF( old );
                    }

                }
                Py_XDECREF( tmp_tuple_unpack_8__element_2 );
                tmp_tuple_unpack_8__element_2 = NULL;

                branch_no_16:;
            }
            {
                PyObject *tmp_assattr_name_1;
                PyObject *tmp_called_name_6;
                PyObject *tmp_source_name_29;
                PyObject *tmp_source_name_30;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_args_element_name_8;
                PyObject *tmp_called_instance_5;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_args_element_name_10;
                PyObject *tmp_called_instance_6;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_11;
                PyObject *tmp_assattr_target_1;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_30 = tmp_mvar_value_2;
                tmp_source_name_29 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_Path );
                if ( tmp_source_name_29 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_arc );
                Py_DECREF( tmp_source_name_29 );
                if ( tmp_called_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_np );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
                }

                if ( tmp_mvar_value_3 == NULL )
                {
                    Py_DECREF( tmp_called_name_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_5 = tmp_mvar_value_3;
                CHECK_OBJECT( var_low );
                tmp_args_element_name_9 = var_low;
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 334;
                {
                    PyObject *call_args[] = { tmp_args_element_name_9 };
                    tmp_args_element_name_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_rad2deg, call_args );
                }

                if ( tmp_args_element_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_6 );

                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_np );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
                }

                if ( tmp_mvar_value_4 == NULL )
                {
                    Py_DECREF( tmp_called_name_6 );
                    Py_DECREF( tmp_args_element_name_8 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_6 = tmp_mvar_value_4;
                CHECK_OBJECT( var_high );
                tmp_args_element_name_11 = var_high;
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 334;
                {
                    PyObject *call_args[] = { tmp_args_element_name_11 };
                    tmp_args_element_name_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_rad2deg, call_args );
                }

                if ( tmp_args_element_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_6 );
                    Py_DECREF( tmp_args_element_name_8 );

                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 334;
                {
                    PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_10 };
                    tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
                }

                Py_DECREF( tmp_called_name_6 );
                Py_DECREF( tmp_args_element_name_8 );
                Py_DECREF( tmp_args_element_name_10 );
                if ( tmp_assattr_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_self );
                tmp_assattr_target_1 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__path, tmp_assattr_name_1 );
                Py_DECREF( tmp_assattr_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 334;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            {
                nuitka_bool tmp_condition_result_17;
                PyObject *tmp_compexpr_left_18;
                PyObject *tmp_compexpr_right_18;
                PyObject *tmp_source_name_31;
                CHECK_OBJECT( par_self );
                tmp_source_name_31 = par_self;
                tmp_compexpr_left_18 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_spine_type );
                if ( tmp_compexpr_left_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 336;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_18 = const_str_plain_bottom;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_18, tmp_compexpr_right_18 );
                Py_DECREF( tmp_compexpr_left_18 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 336;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_17;
                }
                else
                {
                    goto branch_no_17;
                }
                branch_yes_17:;
                // Tried code:
                {
                    PyObject *tmp_assign_source_62;
                    PyObject *tmp_iter_arg_9;
                    PyObject *tmp_source_name_32;
                    PyObject *tmp_source_name_33;
                    PyObject *tmp_source_name_34;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_34 = par_self;
                    tmp_source_name_33 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_axes );
                    if ( tmp_source_name_33 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 337;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_22;
                    }
                    tmp_source_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_viewLim );
                    Py_DECREF( tmp_source_name_33 );
                    if ( tmp_source_name_32 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 337;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_22;
                    }
                    tmp_iter_arg_9 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_intervaly );
                    Py_DECREF( tmp_source_name_32 );
                    if ( tmp_iter_arg_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 337;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_22;
                    }
                    tmp_assign_source_62 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_9 );
                    Py_DECREF( tmp_iter_arg_9 );
                    if ( tmp_assign_source_62 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 337;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_22;
                    }
                    assert( tmp_tuple_unpack_9__source_iter == NULL );
                    tmp_tuple_unpack_9__source_iter = tmp_assign_source_62;
                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_63;
                    PyObject *tmp_unpack_17;
                    CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
                    tmp_unpack_17 = tmp_tuple_unpack_9__source_iter;
                    tmp_assign_source_63 = UNPACK_NEXT( tmp_unpack_17, 0, 2 );
                    if ( tmp_assign_source_63 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 337;
                        goto try_except_handler_23;
                    }
                    assert( tmp_tuple_unpack_9__element_1 == NULL );
                    tmp_tuple_unpack_9__element_1 = tmp_assign_source_63;
                }
                {
                    PyObject *tmp_assign_source_64;
                    PyObject *tmp_unpack_18;
                    CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
                    tmp_unpack_18 = tmp_tuple_unpack_9__source_iter;
                    tmp_assign_source_64 = UNPACK_NEXT( tmp_unpack_18, 1, 2 );
                    if ( tmp_assign_source_64 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 337;
                        goto try_except_handler_23;
                    }
                    assert( tmp_tuple_unpack_9__element_2 == NULL );
                    tmp_tuple_unpack_9__element_2 = tmp_assign_source_64;
                }
                {
                    PyObject *tmp_iterator_name_8;
                    CHECK_OBJECT( tmp_tuple_unpack_9__source_iter );
                    tmp_iterator_name_8 = tmp_tuple_unpack_9__source_iter;
                    // Check if iterator has left-over elements.
                    CHECK_OBJECT( tmp_iterator_name_8 ); assert( HAS_ITERNEXT( tmp_iterator_name_8 ) );

                    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_8 )->tp_iternext)( tmp_iterator_name_8 );

                    if (likely( tmp_iterator_attempt == NULL ))
                    {
                        PyObject *error = GET_ERROR_OCCURRED();

                        if ( error != NULL )
                        {
                            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                            {
                                CLEAR_ERROR_OCCURRED();
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                type_description_1 = "ooooooooooooooooo";
                                exception_lineno = 337;
                                goto try_except_handler_23;
                            }
                        }
                    }
                    else
                    {
                        Py_DECREF( tmp_iterator_attempt );

                        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooooo";
                        exception_lineno = 337;
                        goto try_except_handler_23;
                    }
                }
                goto try_end_21;
                // Exception handler code:
                try_except_handler_23:;
                exception_keeper_type_21 = exception_type;
                exception_keeper_value_21 = exception_value;
                exception_keeper_tb_21 = exception_tb;
                exception_keeper_lineno_21 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_9__source_iter );
                Py_DECREF( tmp_tuple_unpack_9__source_iter );
                tmp_tuple_unpack_9__source_iter = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_21;
                exception_value = exception_keeper_value_21;
                exception_tb = exception_keeper_tb_21;
                exception_lineno = exception_keeper_lineno_21;

                goto try_except_handler_22;
                // End of try:
                try_end_21:;
                goto try_end_22;
                // Exception handler code:
                try_except_handler_22:;
                exception_keeper_type_22 = exception_type;
                exception_keeper_value_22 = exception_value;
                exception_keeper_tb_22 = exception_tb;
                exception_keeper_lineno_22 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_tuple_unpack_9__element_1 );
                tmp_tuple_unpack_9__element_1 = NULL;

                Py_XDECREF( tmp_tuple_unpack_9__element_2 );
                tmp_tuple_unpack_9__element_2 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_22;
                exception_value = exception_keeper_value_22;
                exception_tb = exception_keeper_tb_22;
                exception_lineno = exception_keeper_lineno_22;

                goto frame_exception_exit_1;
                // End of try:
                try_end_22:;
                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_9__source_iter );
                Py_DECREF( tmp_tuple_unpack_9__source_iter );
                tmp_tuple_unpack_9__source_iter = NULL;

                {
                    PyObject *tmp_assign_source_65;
                    CHECK_OBJECT( tmp_tuple_unpack_9__element_1 );
                    tmp_assign_source_65 = tmp_tuple_unpack_9__element_1;
                    assert( var_rmin == NULL );
                    Py_INCREF( tmp_assign_source_65 );
                    var_rmin = tmp_assign_source_65;
                }
                Py_XDECREF( tmp_tuple_unpack_9__element_1 );
                tmp_tuple_unpack_9__element_1 = NULL;

                {
                    PyObject *tmp_assign_source_66;
                    CHECK_OBJECT( tmp_tuple_unpack_9__element_2 );
                    tmp_assign_source_66 = tmp_tuple_unpack_9__element_2;
                    assert( var_rmax == NULL );
                    Py_INCREF( tmp_assign_source_66 );
                    var_rmax = tmp_assign_source_66;
                }
                Py_XDECREF( tmp_tuple_unpack_9__element_2 );
                tmp_tuple_unpack_9__element_2 = NULL;

                // Tried code:
                {
                    PyObject *tmp_assign_source_67;
                    PyObject *tmp_called_instance_7;
                    PyObject *tmp_source_name_35;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_35 = par_self;
                    tmp_called_instance_7 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_axes );
                    if ( tmp_called_instance_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 339;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_24;
                    }
                    frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 339;
                    tmp_assign_source_67 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_get_rorigin );
                    Py_DECREF( tmp_called_instance_7 );
                    if ( tmp_assign_source_67 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 339;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_24;
                    }
                    assert( var_rorigin == NULL );
                    var_rorigin = tmp_assign_source_67;
                }
                goto try_end_23;
                // Exception handler code:
                try_except_handler_24:;
                exception_keeper_type_23 = exception_type;
                exception_keeper_value_23 = exception_value;
                exception_keeper_tb_23 = exception_tb;
                exception_keeper_lineno_23 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Preserve existing published exception.
                exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
                Py_XINCREF( exception_preserved_type_3 );
                exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
                Py_XINCREF( exception_preserved_value_3 );
                exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
                Py_XINCREF( exception_preserved_tb_3 );

                if ( exception_keeper_tb_23 == NULL )
                {
                    exception_keeper_tb_23 = MAKE_TRACEBACK( frame_1732b7288de3ff07b5d0689f845f9ced, exception_keeper_lineno_23 );
                }
                else if ( exception_keeper_lineno_23 != 0 )
                {
                    exception_keeper_tb_23 = ADD_TRACEBACK( exception_keeper_tb_23, frame_1732b7288de3ff07b5d0689f845f9ced, exception_keeper_lineno_23 );
                }

                NORMALIZE_EXCEPTION( &exception_keeper_type_23, &exception_keeper_value_23, &exception_keeper_tb_23 );
                PyException_SetTraceback( exception_keeper_value_23, (PyObject *)exception_keeper_tb_23 );
                PUBLISH_EXCEPTION( &exception_keeper_type_23, &exception_keeper_value_23, &exception_keeper_tb_23 );
                // Tried code:
                {
                    nuitka_bool tmp_condition_result_18;
                    PyObject *tmp_compexpr_left_19;
                    PyObject *tmp_compexpr_right_19;
                    tmp_compexpr_left_19 = EXC_TYPE(PyThreadState_GET());
                    tmp_compexpr_right_19 = PyExc_AttributeError;
                    tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_19, tmp_compexpr_right_19 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 340;
                        type_description_1 = "ooooooooooooooooo";
                        goto try_except_handler_25;
                    }
                    tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_18;
                    }
                    else
                    {
                        goto branch_no_18;
                    }
                    branch_yes_18:;
                    {
                        PyObject *tmp_assign_source_68;
                        CHECK_OBJECT( var_rmin );
                        tmp_assign_source_68 = var_rmin;
                        assert( var_rorigin == NULL );
                        Py_INCREF( tmp_assign_source_68 );
                        var_rorigin = tmp_assign_source_68;
                    }
                    goto branch_end_18;
                    branch_no_18:;
                    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    if (unlikely( tmp_result == false ))
                    {
                        exception_lineno = 338;
                    }

                    if (exception_tb && exception_tb->tb_frame == &frame_1732b7288de3ff07b5d0689f845f9ced->m_frame) frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = exception_tb->tb_lineno;
                    type_description_1 = "ooooooooooooooooo";
                    goto try_except_handler_25;
                    branch_end_18:;
                }
                goto try_end_24;
                // Exception handler code:
                try_except_handler_25:;
                exception_keeper_type_24 = exception_type;
                exception_keeper_value_24 = exception_value;
                exception_keeper_tb_24 = exception_tb;
                exception_keeper_lineno_24 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Restore previous exception.
                SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
                // Re-raise.
                exception_type = exception_keeper_type_24;
                exception_value = exception_keeper_value_24;
                exception_tb = exception_keeper_tb_24;
                exception_lineno = exception_keeper_lineno_24;

                goto frame_exception_exit_1;
                // End of try:
                try_end_24:;
                // Restore previous exception.
                SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
                goto try_end_23;
                // exception handler codes exits in all cases
                NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_16__adjust_location );
                return NULL;
                // End of try:
                try_end_23:;
                {
                    PyObject *tmp_assign_source_69;
                    PyObject *tmp_left_name_8;
                    PyObject *tmp_left_name_9;
                    PyObject *tmp_right_name_8;
                    PyObject *tmp_right_name_9;
                    PyObject *tmp_left_name_10;
                    PyObject *tmp_right_name_10;
                    CHECK_OBJECT( var_rmin );
                    tmp_left_name_9 = var_rmin;
                    if ( var_rorigin == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rorigin" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 342;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_right_name_8 = var_rorigin;
                    tmp_left_name_8 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_9, tmp_right_name_8 );
                    if ( tmp_left_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 342;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_rmax );
                    tmp_left_name_10 = var_rmax;
                    if ( var_rorigin == NULL )
                    {
                        Py_DECREF( tmp_left_name_8 );
                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rorigin" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 342;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_right_name_10 = var_rorigin;
                    tmp_right_name_9 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_10, tmp_right_name_10 );
                    if ( tmp_right_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_8 );

                        exception_lineno = 342;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_69 = BINARY_OPERATION_TRUEDIV_OBJECT_OBJECT( tmp_left_name_8, tmp_right_name_9 );
                    Py_DECREF( tmp_left_name_8 );
                    Py_DECREF( tmp_right_name_9 );
                    if ( tmp_assign_source_69 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 342;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_scaled_diameter == NULL );
                    var_scaled_diameter = tmp_assign_source_69;
                }
                {
                    PyObject *tmp_assattr_name_2;
                    PyObject *tmp_assattr_target_2;
                    CHECK_OBJECT( var_scaled_diameter );
                    tmp_assattr_name_2 = var_scaled_diameter;
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_2 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__height, tmp_assattr_name_2 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 343;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                {
                    PyObject *tmp_assattr_name_3;
                    PyObject *tmp_assattr_target_3;
                    CHECK_OBJECT( var_scaled_diameter );
                    tmp_assattr_name_3 = var_scaled_diameter;
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_3 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__width, tmp_assattr_name_3 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 344;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                branch_no_17:;
            }
            goto branch_end_13;
            branch_no_13:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                PyObject *tmp_left_name_11;
                PyObject *tmp_right_name_11;
                PyObject *tmp_source_name_36;
                tmp_left_name_11 = const_str_digest_57cacffc3b78985b437104b8d0e0907f;
                CHECK_OBJECT( par_self );
                tmp_source_name_36 = par_self;
                tmp_right_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_spine_type );
                if ( tmp_right_name_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 348;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_make_exception_arg_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_11, tmp_right_name_11 );
                Py_DECREF( tmp_right_name_11 );
                if ( tmp_make_exception_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 347;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 347;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_2 );
                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 347;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            branch_end_13:;
        }
        goto branch_end_12;
        branch_no_12:;
        {
            PyObject *tmp_assign_source_70;
            PyObject *tmp_source_name_37;
            PyObject *tmp_source_name_38;
            CHECK_OBJECT( par_self );
            tmp_source_name_38 = par_self;
            tmp_source_name_37 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain__path );
            if ( tmp_source_name_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 350;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_70 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_vertices );
            Py_DECREF( tmp_source_name_37 );
            if ( tmp_assign_source_70 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 350;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_v1 == NULL );
            var_v1 = tmp_assign_source_70;
        }
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_compexpr_left_20;
            PyObject *tmp_compexpr_right_20;
            PyObject *tmp_source_name_39;
            CHECK_OBJECT( var_v1 );
            tmp_source_name_39 = var_v1;
            tmp_compexpr_left_20 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_shape );
            if ( tmp_compexpr_left_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 351;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_20 = const_tuple_int_pos_2_int_pos_2_tuple;
            tmp_operand_name_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_20, tmp_compexpr_right_20 );
            Py_DECREF( tmp_compexpr_left_20 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 351;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 351;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_19 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_19;
            }
            else
            {
                goto branch_no_19;
            }
            branch_yes_19:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_1;
                tmp_raise_type_3 = PyExc_AssertionError;
                tmp_raise_value_1 = const_tuple_str_digest_18dd205cb77f09d4e383f56ea1ba901d_tuple;
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_1;
                Py_INCREF( tmp_raise_value_1 );
                exception_lineno = 351;
                RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            branch_no_19:;
        }
        {
            nuitka_bool tmp_condition_result_20;
            PyObject *tmp_compexpr_left_21;
            PyObject *tmp_compexpr_right_21;
            PyObject *tmp_source_name_40;
            CHECK_OBJECT( par_self );
            tmp_source_name_40 = par_self;
            tmp_compexpr_left_21 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain_spine_type );
            if ( tmp_compexpr_left_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_21 = LIST_COPY( const_list_str_plain_left_str_plain_right_list );
            tmp_res = PySequence_Contains( tmp_compexpr_right_21, tmp_compexpr_left_21 );
            Py_DECREF( tmp_compexpr_left_21 );
            Py_DECREF( tmp_compexpr_right_21 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 352;
                type_description_1 = "ooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_20 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            {
                PyObject *tmp_ass_subvalue_1;
                PyObject *tmp_ass_subscribed_1;
                PyObject *tmp_ass_subscript_1;
                if ( var_low == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "low" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 353;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_ass_subvalue_1 = var_low;
                CHECK_OBJECT( var_v1 );
                tmp_ass_subscribed_1 = var_v1;
                tmp_ass_subscript_1 = const_tuple_int_0_int_pos_1_tuple;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 353;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            {
                PyObject *tmp_ass_subvalue_2;
                PyObject *tmp_ass_subscribed_2;
                PyObject *tmp_ass_subscript_2;
                if ( var_high == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "high" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 354;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_ass_subvalue_2 = var_high;
                CHECK_OBJECT( var_v1 );
                tmp_ass_subscribed_2 = var_v1;
                tmp_ass_subscript_2 = const_tuple_int_pos_1_int_pos_1_tuple;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 354;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_20;
            branch_no_20:;
            {
                nuitka_bool tmp_condition_result_21;
                PyObject *tmp_compexpr_left_22;
                PyObject *tmp_compexpr_right_22;
                PyObject *tmp_source_name_41;
                CHECK_OBJECT( par_self );
                tmp_source_name_41 = par_self;
                tmp_compexpr_left_22 = LOOKUP_ATTRIBUTE( tmp_source_name_41, const_str_plain_spine_type );
                if ( tmp_compexpr_left_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 355;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_22 = LIST_COPY( const_list_str_plain_bottom_str_plain_top_list );
                tmp_res = PySequence_Contains( tmp_compexpr_right_22, tmp_compexpr_left_22 );
                Py_DECREF( tmp_compexpr_left_22 );
                Py_DECREF( tmp_compexpr_right_22 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 355;
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_21 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_21;
                }
                else
                {
                    goto branch_no_21;
                }
                branch_yes_21:;
                {
                    PyObject *tmp_ass_subvalue_3;
                    PyObject *tmp_ass_subscribed_3;
                    PyObject *tmp_ass_subscript_3;
                    if ( var_low == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "low" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 356;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_ass_subvalue_3 = var_low;
                    CHECK_OBJECT( var_v1 );
                    tmp_ass_subscribed_3 = var_v1;
                    tmp_ass_subscript_3 = const_tuple_int_0_int_0_tuple;
                    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 356;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                {
                    PyObject *tmp_ass_subvalue_4;
                    PyObject *tmp_ass_subscribed_4;
                    PyObject *tmp_ass_subscript_4;
                    if ( var_high == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "high" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 357;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_ass_subvalue_4 = var_high;
                    CHECK_OBJECT( var_v1 );
                    tmp_ass_subscribed_4 = var_v1;
                    tmp_ass_subscript_4 = const_tuple_int_pos_1_int_0_tuple;
                    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_4, tmp_ass_subscript_4, tmp_ass_subvalue_4 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 357;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                goto branch_end_21;
                branch_no_21:;
                {
                    PyObject *tmp_raise_type_4;
                    PyObject *tmp_make_exception_arg_3;
                    PyObject *tmp_left_name_12;
                    PyObject *tmp_right_name_12;
                    PyObject *tmp_source_name_42;
                    tmp_left_name_12 = const_str_digest_57cacffc3b78985b437104b8d0e0907f;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_42 = par_self;
                    tmp_right_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain_spine_type );
                    if ( tmp_right_name_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 360;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_make_exception_arg_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_12, tmp_right_name_12 );
                    Py_DECREF( tmp_right_name_12 );
                    if ( tmp_make_exception_arg_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 359;
                        type_description_1 = "ooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_1732b7288de3ff07b5d0689f845f9ced->m_frame.f_lineno = 359;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_3 };
                        tmp_raise_type_4 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                    }

                    Py_DECREF( tmp_make_exception_arg_3 );
                    assert( !(tmp_raise_type_4 == NULL) );
                    exception_type = tmp_raise_type_4;
                    exception_lineno = 359;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                branch_end_21:;
            }
            branch_end_20:;
        }
        branch_end_12:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1732b7288de3ff07b5d0689f845f9ced );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1732b7288de3ff07b5d0689f845f9ced );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1732b7288de3ff07b5d0689f845f9ced );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1732b7288de3ff07b5d0689f845f9ced, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1732b7288de3ff07b5d0689f845f9ced->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1732b7288de3ff07b5d0689f845f9ced, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1732b7288de3ff07b5d0689f845f9ced,
        type_description_1,
        par_self,
        var_low,
        var_high,
        var_viewlim_low,
        var_viewlim_high,
        var_datalim_low,
        var_datalim_high,
        var_ticks,
        var_cond,
        var_tickvals,
        var_direction,
        var_offset,
        var_rmin,
        var_rmax,
        var_rorigin,
        var_scaled_diameter,
        var_v1
    );


    // Release cached frame.
    if ( frame_1732b7288de3ff07b5d0689f845f9ced == cache_frame_1732b7288de3ff07b5d0689f845f9ced )
    {
        Py_DECREF( frame_1732b7288de3ff07b5d0689f845f9ced );
    }
    cache_frame_1732b7288de3ff07b5d0689f845f9ced = NULL;

    assertFrameObject( frame_1732b7288de3ff07b5d0689f845f9ced );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_16__adjust_location );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_low );
    var_low = NULL;

    Py_XDECREF( var_high );
    var_high = NULL;

    Py_XDECREF( var_viewlim_low );
    var_viewlim_low = NULL;

    Py_XDECREF( var_viewlim_high );
    var_viewlim_high = NULL;

    Py_XDECREF( var_datalim_low );
    var_datalim_low = NULL;

    Py_XDECREF( var_datalim_high );
    var_datalim_high = NULL;

    Py_XDECREF( var_ticks );
    var_ticks = NULL;

    Py_XDECREF( var_cond );
    var_cond = NULL;

    Py_XDECREF( var_tickvals );
    var_tickvals = NULL;

    Py_XDECREF( var_direction );
    var_direction = NULL;

    Py_XDECREF( var_offset );
    var_offset = NULL;

    Py_XDECREF( var_rmin );
    var_rmin = NULL;

    Py_XDECREF( var_rmax );
    var_rmax = NULL;

    Py_XDECREF( var_rorigin );
    var_rorigin = NULL;

    Py_XDECREF( var_scaled_diameter );
    var_scaled_diameter = NULL;

    Py_XDECREF( var_v1 );
    var_v1 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_25 = exception_type;
    exception_keeper_value_25 = exception_value;
    exception_keeper_tb_25 = exception_tb;
    exception_keeper_lineno_25 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_low );
    var_low = NULL;

    Py_XDECREF( var_high );
    var_high = NULL;

    Py_XDECREF( var_viewlim_low );
    var_viewlim_low = NULL;

    Py_XDECREF( var_viewlim_high );
    var_viewlim_high = NULL;

    Py_XDECREF( var_datalim_low );
    var_datalim_low = NULL;

    Py_XDECREF( var_datalim_high );
    var_datalim_high = NULL;

    Py_XDECREF( var_ticks );
    var_ticks = NULL;

    Py_XDECREF( var_cond );
    var_cond = NULL;

    Py_XDECREF( var_tickvals );
    var_tickvals = NULL;

    Py_XDECREF( var_direction );
    var_direction = NULL;

    Py_XDECREF( var_offset );
    var_offset = NULL;

    Py_XDECREF( var_rmin );
    var_rmin = NULL;

    Py_XDECREF( var_rmax );
    var_rmax = NULL;

    Py_XDECREF( var_rorigin );
    var_rorigin = NULL;

    Py_XDECREF( var_scaled_diameter );
    var_scaled_diameter = NULL;

    Py_XDECREF( var_v1 );
    var_v1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_25;
    exception_value = exception_keeper_value_25;
    exception_tb = exception_keeper_tb_25;
    exception_lineno = exception_keeper_lineno_25;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_16__adjust_location );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_17_draw( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_renderer = python_pars[ 1 ];
    PyObject *var_ret = NULL;
    struct Nuitka_FrameObject *frame_ae20ba00c2eef183aa7a8fb216a670a8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_ae20ba00c2eef183aa7a8fb216a670a8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ae20ba00c2eef183aa7a8fb216a670a8, codeobj_ae20ba00c2eef183aa7a8fb216a670a8, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ae20ba00c2eef183aa7a8fb216a670a8 = cache_frame_ae20ba00c2eef183aa7a8fb216a670a8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ae20ba00c2eef183aa7a8fb216a670a8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ae20ba00c2eef183aa7a8fb216a670a8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_ae20ba00c2eef183aa7a8fb216a670a8->m_frame.f_lineno = 364;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__adjust_location );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 365;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_2 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_renderer );
        tmp_args_element_name_1 = par_renderer;
        frame_ae20ba00c2eef183aa7a8fb216a670a8->m_frame.f_lineno = 365;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_draw, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
        assert( var_ret == NULL );
        var_ret = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_stale, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;
            type_description_1 = "oooc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ae20ba00c2eef183aa7a8fb216a670a8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ae20ba00c2eef183aa7a8fb216a670a8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ae20ba00c2eef183aa7a8fb216a670a8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ae20ba00c2eef183aa7a8fb216a670a8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ae20ba00c2eef183aa7a8fb216a670a8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ae20ba00c2eef183aa7a8fb216a670a8,
        type_description_1,
        par_self,
        par_renderer,
        var_ret,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_ae20ba00c2eef183aa7a8fb216a670a8 == cache_frame_ae20ba00c2eef183aa7a8fb216a670a8 )
    {
        Py_DECREF( frame_ae20ba00c2eef183aa7a8fb216a670a8 );
    }
    cache_frame_ae20ba00c2eef183aa7a8fb216a670a8 = NULL;

    assertFrameObject( frame_ae20ba00c2eef183aa7a8fb216a670a8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_ret );
    tmp_return_value = var_ret;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_17_draw );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    CHECK_OBJECT( (PyObject *)var_ret );
    Py_DECREF( var_ret );
    var_ret = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    Py_XDECREF( var_ret );
    var_ret = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_17_draw );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_18__calc_offset_transform( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_position = NULL;
    PyObject *var_position_type = NULL;
    PyObject *var_amount = NULL;
    PyObject *var_offset_vec = NULL;
    PyObject *var_offset_x = NULL;
    PyObject *var_offset_y = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_dbd9f46b071cbd449b8d1631eb33a7a8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_dbd9f46b071cbd449b8d1631eb33a7a8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dbd9f46b071cbd449b8d1631eb33a7a8, codeobj_dbd9f46b071cbd449b8d1631eb33a7a8, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_dbd9f46b071cbd449b8d1631eb33a7a8 = cache_frame_dbd9f46b071cbd449b8d1631eb33a7a8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dbd9f46b071cbd449b8d1631eb33a7a8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dbd9f46b071cbd449b8d1631eb33a7a8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 371;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__ensure_position_is_set );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__position );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 372;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_position == NULL );
        var_position = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( var_position );
        tmp_isinstance_inst_1 = var_position;
        tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 373;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( var_position );
            tmp_compexpr_left_1 = var_position;
            tmp_compexpr_right_1 = const_str_plain_center;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                tmp_assign_source_2 = const_tuple_str_plain_axes_float_0_5_tuple;
                {
                    PyObject *old = var_position;
                    assert( old != NULL );
                    var_position = tmp_assign_source_2;
                    Py_INCREF( var_position );
                    Py_DECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                CHECK_OBJECT( var_position );
                tmp_compexpr_left_2 = var_position;
                tmp_compexpr_right_2 = const_str_plain_zero;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 376;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_3;
                    tmp_assign_source_3 = const_tuple_str_plain_data_int_0_tuple;
                    {
                        PyObject *old = var_position;
                        assert( old != NULL );
                        var_position = tmp_assign_source_3;
                        Py_INCREF( var_position );
                        Py_DECREF( old );
                    }

                }
                branch_no_3:;
            }
            branch_end_2:;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_len_arg_1;
        if ( var_position == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "position" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 378;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_len_arg_1 = var_position;
        tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 378;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = const_int_pos_2;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_raise_value_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            tmp_raise_value_1 = const_tuple_str_digest_4cde890927cc2b88beac2083dde55dbb_tuple;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_value = tmp_raise_value_1;
            Py_INCREF( tmp_raise_value_1 );
            exception_lineno = 378;
            RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_4:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        if ( var_position == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "position" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 379;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }

        tmp_iter_arg_1 = var_position;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 379;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 379;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 379;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooo";
                    exception_lineno = 379;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooo";
            exception_lineno = 379;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        assert( var_position_type == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_position_type = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        assert( var_amount == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_amount = tmp_assign_source_8;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        CHECK_OBJECT( var_position_type );
        tmp_compexpr_left_4 = var_position_type;
        tmp_compexpr_right_4 = const_tuple_str_plain_axes_str_plain_outward_str_plain_data_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 380;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_raise_type_2;
            tmp_raise_type_2 = PyExc_AssertionError;
            exception_type = tmp_raise_type_2;
            Py_INCREF( tmp_raise_type_2 );
            exception_lineno = 380;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( var_position_type );
        tmp_compexpr_left_5 = var_position_type;
        tmp_compexpr_right_5 = const_str_plain_outward;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 381;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( var_amount );
            tmp_compexpr_left_6 = var_amount;
            tmp_compexpr_right_6 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 382;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_assattr_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_assattr_target_1;
                tmp_tuple_element_1 = const_str_plain_identity;
                tmp_assattr_name_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_assattr_name_1, 0, tmp_tuple_element_1 );
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                }

                if ( tmp_mvar_value_1 == NULL )
                {
                    Py_DECREF( tmp_assattr_name_1 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 385;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_2 = tmp_mvar_value_1;
                frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 385;
                tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_IdentityTransform );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_assattr_name_1 );

                    exception_lineno = 385;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                PyTuple_SET_ITEM( tmp_assattr_name_1, 1, tmp_tuple_element_1 );
                CHECK_OBJECT( par_self );
                tmp_assattr_target_1 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__spine_transform, tmp_assattr_name_1 );
                Py_DECREF( tmp_assattr_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_7;
            branch_no_7:;
            {
                nuitka_bool tmp_condition_result_8;
                PyObject *tmp_compexpr_left_7;
                PyObject *tmp_compexpr_right_7;
                PyObject *tmp_source_name_2;
                CHECK_OBJECT( par_self );
                tmp_source_name_2 = par_self;
                tmp_compexpr_left_7 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_spine_type );
                if ( tmp_compexpr_left_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 386;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_7 = LIST_COPY( const_list_c333dfadccd8c3bb49d860b0b29f8fad_list );
                tmp_res = PySequence_Contains( tmp_compexpr_right_7, tmp_compexpr_left_7 );
                Py_DECREF( tmp_compexpr_left_7 );
                Py_DECREF( tmp_compexpr_right_7 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 386;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_8 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_assign_source_9;
                    PyObject *tmp_subscribed_name_1;
                    PyObject *tmp_subscript_name_1;
                    PyObject *tmp_source_name_3;
                    tmp_subscribed_name_1 = PyDict_Copy( const_dict_75f1ed2738ae304e7e502ba9973de3b5 );
                    CHECK_OBJECT( par_self );
                    tmp_source_name_3 = par_self;
                    tmp_subscript_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_spine_type );
                    if ( tmp_subscript_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_subscribed_name_1 );

                        exception_lineno = 391;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                    Py_DECREF( tmp_subscribed_name_1 );
                    Py_DECREF( tmp_subscript_name_1 );
                    if ( tmp_assign_source_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 387;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_offset_vec == NULL );
                    var_offset_vec = tmp_assign_source_9;
                }
                {
                    PyObject *tmp_assign_source_10;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_left_name_2;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_subscribed_name_2;
                    PyObject *tmp_subscript_name_2;
                    PyObject *tmp_right_name_2;
                    CHECK_OBJECT( var_amount );
                    tmp_left_name_2 = var_amount;
                    CHECK_OBJECT( var_offset_vec );
                    tmp_subscribed_name_2 = var_offset_vec;
                    tmp_subscript_name_2 = const_int_0;
                    tmp_right_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
                    if ( tmp_right_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 393;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_left_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 393;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_2 = const_float_72_0;
                    tmp_assign_source_10 = BINARY_OPERATION_TRUEDIV_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_2 );
                    Py_DECREF( tmp_left_name_1 );
                    if ( tmp_assign_source_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 393;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_offset_x == NULL );
                    var_offset_x = tmp_assign_source_10;
                }
                {
                    PyObject *tmp_assign_source_11;
                    PyObject *tmp_left_name_3;
                    PyObject *tmp_left_name_4;
                    PyObject *tmp_right_name_3;
                    PyObject *tmp_subscribed_name_3;
                    PyObject *tmp_subscript_name_3;
                    PyObject *tmp_right_name_4;
                    CHECK_OBJECT( var_amount );
                    tmp_left_name_4 = var_amount;
                    CHECK_OBJECT( var_offset_vec );
                    tmp_subscribed_name_3 = var_offset_vec;
                    tmp_subscript_name_3 = const_int_pos_1;
                    tmp_right_name_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 1 );
                    if ( tmp_right_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 394;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_left_name_3 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_3 );
                    Py_DECREF( tmp_right_name_3 );
                    if ( tmp_left_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 394;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_4 = const_float_72_0;
                    tmp_assign_source_11 = BINARY_OPERATION_TRUEDIV_OBJECT_FLOAT( tmp_left_name_3, tmp_right_name_4 );
                    Py_DECREF( tmp_left_name_3 );
                    if ( tmp_assign_source_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 394;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_offset_y == NULL );
                    var_offset_y = tmp_assign_source_11;
                }
                {
                    PyObject *tmp_assattr_name_2;
                    PyObject *tmp_tuple_element_2;
                    PyObject *tmp_called_name_1;
                    PyObject *tmp_source_name_4;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_args_element_name_1;
                    PyObject *tmp_args_element_name_2;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_source_name_5;
                    PyObject *tmp_source_name_6;
                    PyObject *tmp_assattr_target_2;
                    tmp_tuple_element_2 = const_str_plain_post;
                    tmp_assattr_name_2 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_assattr_name_2, 0, tmp_tuple_element_2 );
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {
                        Py_DECREF( tmp_assattr_name_2 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 396;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_4 = tmp_mvar_value_2;
                    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_ScaledTranslation );
                    if ( tmp_called_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_assattr_name_2 );

                        exception_lineno = 396;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_offset_x );
                    tmp_args_element_name_1 = var_offset_x;
                    CHECK_OBJECT( var_offset_y );
                    tmp_args_element_name_2 = var_offset_y;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_6 = par_self;
                    tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_figure );
                    if ( tmp_source_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_assattr_name_2 );
                        Py_DECREF( tmp_called_name_1 );

                        exception_lineno = 399;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_dpi_scale_trans );
                    Py_DECREF( tmp_source_name_5 );
                    if ( tmp_args_element_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_assattr_name_2 );
                        Py_DECREF( tmp_called_name_1 );

                        exception_lineno = 399;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 396;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                        tmp_tuple_element_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
                    }

                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_element_name_3 );
                    if ( tmp_tuple_element_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_assattr_name_2 );

                        exception_lineno = 396;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_assattr_name_2, 1, tmp_tuple_element_2 );
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_2 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__spine_transform, tmp_assattr_name_2 );
                    Py_DECREF( tmp_assattr_name_2 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 395;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                goto branch_end_8;
                branch_no_8:;
                {
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_7;
                    PyObject *tmp_mvar_value_3;
                    PyObject *tmp_call_result_2;
                    PyObject *tmp_args_element_name_4;
                    PyObject *tmp_left_name_5;
                    PyObject *tmp_right_name_5;
                    PyObject *tmp_source_name_8;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_cbook );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 401;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_7 = tmp_mvar_value_3;
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__warn_external );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 401;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_left_name_5 = const_str_digest_1b1cfa91783d27298c896d931bf1a5cb;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_8 = par_self;
                    tmp_right_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_spine_type );
                    if ( tmp_right_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 402;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_args_element_name_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                    Py_DECREF( tmp_right_name_5 );
                    if ( tmp_args_element_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 401;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 401;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_4 };
                        tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                    }

                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_args_element_name_4 );
                    if ( tmp_call_result_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 401;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_2 );
                }
                {
                    PyObject *tmp_assattr_name_3;
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_called_instance_3;
                    PyObject *tmp_mvar_value_4;
                    PyObject *tmp_assattr_target_3;
                    tmp_tuple_element_3 = const_str_plain_identity;
                    tmp_assattr_name_3 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_assattr_name_3, 0, tmp_tuple_element_3 );
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                    if (unlikely( tmp_mvar_value_4 == NULL ))
                    {
                        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                    }

                    if ( tmp_mvar_value_4 == NULL )
                    {
                        Py_DECREF( tmp_assattr_name_3 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 404;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_instance_3 = tmp_mvar_value_4;
                    frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 404;
                    tmp_tuple_element_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_IdentityTransform );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_assattr_name_3 );

                        exception_lineno = 404;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_assattr_name_3, 1, tmp_tuple_element_3 );
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_3 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__spine_transform, tmp_assattr_name_3 );
                    Py_DECREF( tmp_assattr_name_3 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 403;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                branch_end_8:;
            }
            branch_end_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_8;
            PyObject *tmp_compexpr_right_8;
            CHECK_OBJECT( var_position_type );
            tmp_compexpr_left_8 = var_position_type;
            tmp_compexpr_right_8 = const_str_plain_axes;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 405;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_9;
                PyObject *tmp_compexpr_right_9;
                PyObject *tmp_source_name_9;
                CHECK_OBJECT( par_self );
                tmp_source_name_9 = par_self;
                tmp_compexpr_left_9 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_spine_type );
                if ( tmp_compexpr_left_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 406;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_9 = const_tuple_str_plain_left_str_plain_right_tuple;
                tmp_res = PySequence_Contains( tmp_compexpr_right_9, tmp_compexpr_left_9 );
                Py_DECREF( tmp_compexpr_left_9 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 406;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_10 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    PyObject *tmp_assattr_name_4;
                    PyObject *tmp_tuple_element_4;
                    PyObject *tmp_called_instance_4;
                    PyObject *tmp_source_name_10;
                    PyObject *tmp_mvar_value_5;
                    PyObject *tmp_args_element_name_5;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_args_element_name_9;
                    PyObject *tmp_args_element_name_10;
                    PyObject *tmp_assattr_target_4;
                    tmp_tuple_element_4 = const_str_plain_pre;
                    tmp_assattr_name_4 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_4 );
                    PyTuple_SET_ITEM( tmp_assattr_name_4, 0, tmp_tuple_element_4 );
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                    if (unlikely( tmp_mvar_value_5 == NULL ))
                    {
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                    }

                    if ( tmp_mvar_value_5 == NULL )
                    {
                        Py_DECREF( tmp_assattr_name_4 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 408;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_10 = tmp_mvar_value_5;
                    tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_Affine2D );
                    if ( tmp_called_instance_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_assattr_name_4 );

                        exception_lineno = 408;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_args_element_name_5 = const_int_0;
                    tmp_args_element_name_6 = const_int_0;
                    tmp_args_element_name_7 = const_int_0;
                    tmp_args_element_name_8 = const_int_pos_1;
                    CHECK_OBJECT( var_amount );
                    tmp_args_element_name_9 = var_amount;
                    tmp_args_element_name_10 = const_int_0;
                    frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 408;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10 };
                        tmp_tuple_element_4 = CALL_METHOD_WITH_ARGS6( tmp_called_instance_4, const_str_plain_from_values, call_args );
                    }

                    Py_DECREF( tmp_called_instance_4 );
                    if ( tmp_tuple_element_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_assattr_name_4 );

                        exception_lineno = 408;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_assattr_name_4, 1, tmp_tuple_element_4 );
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_4 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__spine_transform, tmp_assattr_name_4 );
                    Py_DECREF( tmp_assattr_name_4 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 407;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                goto branch_end_10;
                branch_no_10:;
                {
                    nuitka_bool tmp_condition_result_11;
                    PyObject *tmp_compexpr_left_10;
                    PyObject *tmp_compexpr_right_10;
                    PyObject *tmp_source_name_11;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_11 = par_self;
                    tmp_compexpr_left_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_spine_type );
                    if ( tmp_compexpr_left_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 412;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_10 = const_tuple_str_plain_bottom_str_plain_top_tuple;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_10, tmp_compexpr_left_10 );
                    Py_DECREF( tmp_compexpr_left_10 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 412;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_11 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_11;
                    }
                    else
                    {
                        goto branch_no_11;
                    }
                    branch_yes_11:;
                    {
                        PyObject *tmp_assattr_name_5;
                        PyObject *tmp_tuple_element_5;
                        PyObject *tmp_called_instance_5;
                        PyObject *tmp_source_name_12;
                        PyObject *tmp_mvar_value_6;
                        PyObject *tmp_args_element_name_11;
                        PyObject *tmp_args_element_name_12;
                        PyObject *tmp_args_element_name_13;
                        PyObject *tmp_args_element_name_14;
                        PyObject *tmp_args_element_name_15;
                        PyObject *tmp_args_element_name_16;
                        PyObject *tmp_assattr_target_5;
                        tmp_tuple_element_5 = const_str_plain_pre;
                        tmp_assattr_name_5 = PyTuple_New( 2 );
                        Py_INCREF( tmp_tuple_element_5 );
                        PyTuple_SET_ITEM( tmp_assattr_name_5, 0, tmp_tuple_element_5 );
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                        if (unlikely( tmp_mvar_value_6 == NULL ))
                        {
                            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                        }

                        if ( tmp_mvar_value_6 == NULL )
                        {
                            Py_DECREF( tmp_assattr_name_5 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 414;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_source_name_12 = tmp_mvar_value_6;
                        tmp_called_instance_5 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_Affine2D );
                        if ( tmp_called_instance_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_assattr_name_5 );

                            exception_lineno = 414;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_args_element_name_11 = const_int_pos_1;
                        tmp_args_element_name_12 = const_int_0;
                        tmp_args_element_name_13 = const_int_0;
                        tmp_args_element_name_14 = const_int_0;
                        tmp_args_element_name_15 = const_int_0;
                        CHECK_OBJECT( var_amount );
                        tmp_args_element_name_16 = var_amount;
                        frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 414;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15, tmp_args_element_name_16 };
                            tmp_tuple_element_5 = CALL_METHOD_WITH_ARGS6( tmp_called_instance_5, const_str_plain_from_values, call_args );
                        }

                        Py_DECREF( tmp_called_instance_5 );
                        if ( tmp_tuple_element_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_assattr_name_5 );

                            exception_lineno = 414;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        PyTuple_SET_ITEM( tmp_assattr_name_5, 1, tmp_tuple_element_5 );
                        CHECK_OBJECT( par_self );
                        tmp_assattr_target_5 = par_self;
                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__spine_transform, tmp_assattr_name_5 );
                        Py_DECREF( tmp_assattr_name_5 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 413;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                    }
                    goto branch_end_11;
                    branch_no_11:;
                    {
                        PyObject *tmp_called_name_3;
                        PyObject *tmp_source_name_13;
                        PyObject *tmp_mvar_value_7;
                        PyObject *tmp_call_result_3;
                        PyObject *tmp_args_element_name_17;
                        PyObject *tmp_left_name_6;
                        PyObject *tmp_right_name_6;
                        PyObject *tmp_source_name_14;
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_cbook );

                        if (unlikely( tmp_mvar_value_7 == NULL ))
                        {
                            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
                        }

                        if ( tmp_mvar_value_7 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 419;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_source_name_13 = tmp_mvar_value_7;
                        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain__warn_external );
                        if ( tmp_called_name_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 419;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_left_name_6 = const_str_digest_1b1cfa91783d27298c896d931bf1a5cb;
                        CHECK_OBJECT( par_self );
                        tmp_source_name_14 = par_self;
                        tmp_right_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_spine_type );
                        if ( tmp_right_name_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_3 );

                            exception_lineno = 420;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_args_element_name_17 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                        Py_DECREF( tmp_right_name_6 );
                        if ( tmp_args_element_name_17 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_3 );

                            exception_lineno = 419;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 419;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_17 };
                            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                        }

                        Py_DECREF( tmp_called_name_3 );
                        Py_DECREF( tmp_args_element_name_17 );
                        if ( tmp_call_result_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 419;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        Py_DECREF( tmp_call_result_3 );
                    }
                    {
                        PyObject *tmp_assattr_name_6;
                        PyObject *tmp_tuple_element_6;
                        PyObject *tmp_called_instance_6;
                        PyObject *tmp_mvar_value_8;
                        PyObject *tmp_assattr_target_6;
                        tmp_tuple_element_6 = const_str_plain_identity;
                        tmp_assattr_name_6 = PyTuple_New( 2 );
                        Py_INCREF( tmp_tuple_element_6 );
                        PyTuple_SET_ITEM( tmp_assattr_name_6, 0, tmp_tuple_element_6 );
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                        if (unlikely( tmp_mvar_value_8 == NULL ))
                        {
                            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                        }

                        if ( tmp_mvar_value_8 == NULL )
                        {
                            Py_DECREF( tmp_assattr_name_6 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 422;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_instance_6 = tmp_mvar_value_8;
                        frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 422;
                        tmp_tuple_element_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_IdentityTransform );
                        if ( tmp_tuple_element_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_assattr_name_6 );

                            exception_lineno = 422;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        PyTuple_SET_ITEM( tmp_assattr_name_6, 1, tmp_tuple_element_6 );
                        CHECK_OBJECT( par_self );
                        tmp_assattr_target_6 = par_self;
                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__spine_transform, tmp_assattr_name_6 );
                        Py_DECREF( tmp_assattr_name_6 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 421;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                    }
                    branch_end_11:;
                }
                branch_end_10:;
            }
            goto branch_end_9;
            branch_no_9:;
            {
                nuitka_bool tmp_condition_result_12;
                PyObject *tmp_compexpr_left_11;
                PyObject *tmp_compexpr_right_11;
                CHECK_OBJECT( var_position_type );
                tmp_compexpr_left_11 = var_position_type;
                tmp_compexpr_right_11 = const_str_plain_data;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 423;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_12;
                }
                else
                {
                    goto branch_no_12;
                }
                branch_yes_12:;
                {
                    nuitka_bool tmp_condition_result_13;
                    PyObject *tmp_compexpr_left_12;
                    PyObject *tmp_compexpr_right_12;
                    PyObject *tmp_source_name_15;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_15 = par_self;
                    tmp_compexpr_left_12 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_spine_type );
                    if ( tmp_compexpr_left_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 424;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_12 = const_tuple_str_plain_right_str_plain_top_tuple;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_12, tmp_compexpr_left_12 );
                    Py_DECREF( tmp_compexpr_left_12 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 424;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_13 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_13;
                    }
                    else
                    {
                        goto branch_no_13;
                    }
                    branch_yes_13:;
                    {
                        PyObject *tmp_assign_source_12;
                        PyObject *tmp_left_name_7;
                        PyObject *tmp_right_name_7;
                        CHECK_OBJECT( var_amount );
                        tmp_left_name_7 = var_amount;
                        tmp_right_name_7 = const_int_pos_1;
                        tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceSubtract, &tmp_left_name_7, tmp_right_name_7 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 428;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_assign_source_12 = tmp_left_name_7;
                        var_amount = tmp_assign_source_12;

                    }
                    branch_no_13:;
                }
                {
                    nuitka_bool tmp_condition_result_14;
                    PyObject *tmp_compexpr_left_13;
                    PyObject *tmp_compexpr_right_13;
                    PyObject *tmp_source_name_16;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_16 = par_self;
                    tmp_compexpr_left_13 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_spine_type );
                    if ( tmp_compexpr_left_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 429;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_13 = const_tuple_str_plain_left_str_plain_right_tuple;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_13, tmp_compexpr_left_13 );
                    Py_DECREF( tmp_compexpr_left_13 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 429;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_14 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_14;
                    }
                    else
                    {
                        goto branch_no_14;
                    }
                    branch_yes_14:;
                    {
                        PyObject *tmp_assattr_name_7;
                        PyObject *tmp_tuple_element_7;
                        PyObject *tmp_called_instance_7;
                        PyObject *tmp_called_instance_8;
                        PyObject *tmp_mvar_value_9;
                        PyObject *tmp_args_element_name_18;
                        PyObject *tmp_args_element_name_19;
                        PyObject *tmp_assattr_target_7;
                        tmp_tuple_element_7 = const_str_plain_data;
                        tmp_assattr_name_7 = PyTuple_New( 2 );
                        Py_INCREF( tmp_tuple_element_7 );
                        PyTuple_SET_ITEM( tmp_assattr_name_7, 0, tmp_tuple_element_7 );
                        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                        if (unlikely( tmp_mvar_value_9 == NULL ))
                        {
                            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                        }

                        if ( tmp_mvar_value_9 == NULL )
                        {
                            Py_DECREF( tmp_assattr_name_7 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 431;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_instance_8 = tmp_mvar_value_9;
                        frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 431;
                        tmp_called_instance_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_Affine2D );
                        if ( tmp_called_instance_7 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_assattr_name_7 );

                            exception_lineno = 431;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        CHECK_OBJECT( var_amount );
                        tmp_args_element_name_18 = var_amount;
                        tmp_args_element_name_19 = const_int_0;
                        frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 431;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19 };
                            tmp_tuple_element_7 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_7, const_str_plain_translate, call_args );
                        }

                        Py_DECREF( tmp_called_instance_7 );
                        if ( tmp_tuple_element_7 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_assattr_name_7 );

                            exception_lineno = 431;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        PyTuple_SET_ITEM( tmp_assattr_name_7, 1, tmp_tuple_element_7 );
                        CHECK_OBJECT( par_self );
                        tmp_assattr_target_7 = par_self;
                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain__spine_transform, tmp_assattr_name_7 );
                        Py_DECREF( tmp_assattr_name_7 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 430;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                    }
                    goto branch_end_14;
                    branch_no_14:;
                    {
                        nuitka_bool tmp_condition_result_15;
                        PyObject *tmp_compexpr_left_14;
                        PyObject *tmp_compexpr_right_14;
                        PyObject *tmp_source_name_17;
                        CHECK_OBJECT( par_self );
                        tmp_source_name_17 = par_self;
                        tmp_compexpr_left_14 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_spine_type );
                        if ( tmp_compexpr_left_14 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 433;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_compexpr_right_14 = const_tuple_str_plain_bottom_str_plain_top_tuple;
                        tmp_res = PySequence_Contains( tmp_compexpr_right_14, tmp_compexpr_left_14 );
                        Py_DECREF( tmp_compexpr_left_14 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 433;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_15 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_15;
                        }
                        else
                        {
                            goto branch_no_15;
                        }
                        branch_yes_15:;
                        {
                            PyObject *tmp_assattr_name_8;
                            PyObject *tmp_tuple_element_8;
                            PyObject *tmp_called_instance_9;
                            PyObject *tmp_called_instance_10;
                            PyObject *tmp_mvar_value_10;
                            PyObject *tmp_args_element_name_20;
                            PyObject *tmp_args_element_name_21;
                            PyObject *tmp_assattr_target_8;
                            tmp_tuple_element_8 = const_str_plain_data;
                            tmp_assattr_name_8 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_8 );
                            PyTuple_SET_ITEM( tmp_assattr_name_8, 0, tmp_tuple_element_8 );
                            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                            if (unlikely( tmp_mvar_value_10 == NULL ))
                            {
                                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                            }

                            if ( tmp_mvar_value_10 == NULL )
                            {
                                Py_DECREF( tmp_assattr_name_8 );
                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 435;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_called_instance_10 = tmp_mvar_value_10;
                            frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 435;
                            tmp_called_instance_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_10, const_str_plain_Affine2D );
                            if ( tmp_called_instance_9 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_assattr_name_8 );

                                exception_lineno = 435;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_args_element_name_20 = const_int_0;
                            CHECK_OBJECT( var_amount );
                            tmp_args_element_name_21 = var_amount;
                            frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 435;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_20, tmp_args_element_name_21 };
                                tmp_tuple_element_8 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_9, const_str_plain_translate, call_args );
                            }

                            Py_DECREF( tmp_called_instance_9 );
                            if ( tmp_tuple_element_8 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_assattr_name_8 );

                                exception_lineno = 435;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            PyTuple_SET_ITEM( tmp_assattr_name_8, 1, tmp_tuple_element_8 );
                            CHECK_OBJECT( par_self );
                            tmp_assattr_target_8 = par_self;
                            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain__spine_transform, tmp_assattr_name_8 );
                            Py_DECREF( tmp_assattr_name_8 );
                            if ( tmp_result == false )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 434;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                        }
                        goto branch_end_15;
                        branch_no_15:;
                        {
                            PyObject *tmp_called_name_4;
                            PyObject *tmp_source_name_18;
                            PyObject *tmp_mvar_value_11;
                            PyObject *tmp_call_result_4;
                            PyObject *tmp_args_element_name_22;
                            PyObject *tmp_left_name_8;
                            PyObject *tmp_right_name_8;
                            PyObject *tmp_source_name_19;
                            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_cbook );

                            if (unlikely( tmp_mvar_value_11 == NULL ))
                            {
                                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
                            }

                            if ( tmp_mvar_value_11 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 438;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_source_name_18 = tmp_mvar_value_11;
                            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain__warn_external );
                            if ( tmp_called_name_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 438;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_left_name_8 = const_str_digest_1b1cfa91783d27298c896d931bf1a5cb;
                            CHECK_OBJECT( par_self );
                            tmp_source_name_19 = par_self;
                            tmp_right_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_spine_type );
                            if ( tmp_right_name_8 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_called_name_4 );

                                exception_lineno = 439;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_args_element_name_22 = BINARY_OPERATION_REMAINDER( tmp_left_name_8, tmp_right_name_8 );
                            Py_DECREF( tmp_right_name_8 );
                            if ( tmp_args_element_name_22 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_called_name_4 );

                                exception_lineno = 438;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 438;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_22 };
                                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                            }

                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_element_name_22 );
                            if ( tmp_call_result_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 438;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            Py_DECREF( tmp_call_result_4 );
                        }
                        {
                            PyObject *tmp_assattr_name_9;
                            PyObject *tmp_tuple_element_9;
                            PyObject *tmp_called_instance_11;
                            PyObject *tmp_mvar_value_12;
                            PyObject *tmp_assattr_target_9;
                            tmp_tuple_element_9 = const_str_plain_identity;
                            tmp_assattr_name_9 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_9 );
                            PyTuple_SET_ITEM( tmp_assattr_name_9, 0, tmp_tuple_element_9 );
                            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                            if (unlikely( tmp_mvar_value_12 == NULL ))
                            {
                                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                            }

                            if ( tmp_mvar_value_12 == NULL )
                            {
                                Py_DECREF( tmp_assattr_name_9 );
                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 441;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_called_instance_11 = tmp_mvar_value_12;
                            frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame.f_lineno = 441;
                            tmp_tuple_element_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_IdentityTransform );
                            if ( tmp_tuple_element_9 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_assattr_name_9 );

                                exception_lineno = 441;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            PyTuple_SET_ITEM( tmp_assattr_name_9, 1, tmp_tuple_element_9 );
                            CHECK_OBJECT( par_self );
                            tmp_assattr_target_9 = par_self;
                            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain__spine_transform, tmp_assattr_name_9 );
                            Py_DECREF( tmp_assattr_name_9 );
                            if ( tmp_result == false )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 440;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                        }
                        branch_end_15:;
                    }
                    branch_end_14:;
                }
                branch_no_12:;
            }
            branch_end_9:;
        }
        branch_end_6:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dbd9f46b071cbd449b8d1631eb33a7a8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dbd9f46b071cbd449b8d1631eb33a7a8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dbd9f46b071cbd449b8d1631eb33a7a8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dbd9f46b071cbd449b8d1631eb33a7a8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dbd9f46b071cbd449b8d1631eb33a7a8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dbd9f46b071cbd449b8d1631eb33a7a8,
        type_description_1,
        par_self,
        var_position,
        var_position_type,
        var_amount,
        var_offset_vec,
        var_offset_x,
        var_offset_y
    );


    // Release cached frame.
    if ( frame_dbd9f46b071cbd449b8d1631eb33a7a8 == cache_frame_dbd9f46b071cbd449b8d1631eb33a7a8 )
    {
        Py_DECREF( frame_dbd9f46b071cbd449b8d1631eb33a7a8 );
    }
    cache_frame_dbd9f46b071cbd449b8d1631eb33a7a8 = NULL;

    assertFrameObject( frame_dbd9f46b071cbd449b8d1631eb33a7a8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_18__calc_offset_transform );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_position );
    var_position = NULL;

    CHECK_OBJECT( (PyObject *)var_position_type );
    Py_DECREF( var_position_type );
    var_position_type = NULL;

    Py_XDECREF( var_amount );
    var_amount = NULL;

    Py_XDECREF( var_offset_vec );
    var_offset_vec = NULL;

    Py_XDECREF( var_offset_x );
    var_offset_x = NULL;

    Py_XDECREF( var_offset_y );
    var_offset_y = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_position );
    var_position = NULL;

    Py_XDECREF( var_position_type );
    var_position_type = NULL;

    Py_XDECREF( var_amount );
    var_amount = NULL;

    Py_XDECREF( var_offset_vec );
    var_offset_vec = NULL;

    Py_XDECREF( var_offset_x );
    var_offset_x = NULL;

    Py_XDECREF( var_offset_y );
    var_offset_y = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_18__calc_offset_transform );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_19_set_position( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_position = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_b45baa0ce76614000fd72883197e7273;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_b45baa0ce76614000fd72883197e7273 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b45baa0ce76614000fd72883197e7273, codeobj_b45baa0ce76614000fd72883197e7273, module_matplotlib$spines, sizeof(void *)+sizeof(void *) );
    frame_b45baa0ce76614000fd72883197e7273 = cache_frame_b45baa0ce76614000fd72883197e7273;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b45baa0ce76614000fd72883197e7273 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b45baa0ce76614000fd72883197e7273 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_position );
        tmp_compexpr_left_1 = par_position;
        tmp_compexpr_right_1 = const_tuple_str_plain_center_str_plain_zero_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 464;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_len_arg_1;
            CHECK_OBJECT( par_position );
            tmp_len_arg_1 = par_position;
            tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 468;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_int_pos_2;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_make_exception_arg_1;
                tmp_make_exception_arg_1 = const_str_digest_4ed0dc019602d7c679634d1b0f749d6d;
                frame_b45baa0ce76614000fd72883197e7273->m_frame.f_lineno = 469;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                }

                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 469;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_position );
            tmp_subscribed_name_1 = par_position;
            tmp_subscript_name_1 = const_int_0;
            tmp_compexpr_left_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 470;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_3 = LIST_COPY( const_list_str_plain_outward_str_plain_axes_str_plain_data_list );
            tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            Py_DECREF( tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 470;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                tmp_make_exception_arg_2 = const_str_digest_4e850d568760852e97a1610f6e76f79c;
                frame_b45baa0ce76614000fd72883197e7273->m_frame.f_lineno = 471;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                }

                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 471;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            branch_no_3:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_position );
        tmp_assattr_name_1 = par_position;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 473;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_b45baa0ce76614000fd72883197e7273->m_frame.f_lineno = 474;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__calc_offset_transform );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 474;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_set_transform );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        frame_b45baa0ce76614000fd72883197e7273->m_frame.f_lineno = 476;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_spine_transform );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 476;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_b45baa0ce76614000fd72883197e7273->m_frame.f_lineno = 476;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 476;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_axis );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 478;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = Py_None;
        tmp_condition_result_4 = ( tmp_compexpr_left_4 != tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_axis );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 479;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_b45baa0ce76614000fd72883197e7273->m_frame.f_lineno = 479;
            tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_reset_ticks );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 479;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_4:;
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_stale, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 480;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b45baa0ce76614000fd72883197e7273 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b45baa0ce76614000fd72883197e7273 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b45baa0ce76614000fd72883197e7273, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b45baa0ce76614000fd72883197e7273->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b45baa0ce76614000fd72883197e7273, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b45baa0ce76614000fd72883197e7273,
        type_description_1,
        par_self,
        par_position
    );


    // Release cached frame.
    if ( frame_b45baa0ce76614000fd72883197e7273 == cache_frame_b45baa0ce76614000fd72883197e7273 )
    {
        Py_DECREF( frame_b45baa0ce76614000fd72883197e7273 );
    }
    cache_frame_b45baa0ce76614000fd72883197e7273 = NULL;

    assertFrameObject( frame_b45baa0ce76614000fd72883197e7273 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_19_set_position );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_position );
    Py_DECREF( par_position );
    par_position = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_position );
    Py_DECREF( par_position );
    par_position = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_19_set_position );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_20_get_position( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_780f5a3a09e3de88922cbb1fe3569671;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_780f5a3a09e3de88922cbb1fe3569671 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_780f5a3a09e3de88922cbb1fe3569671, codeobj_780f5a3a09e3de88922cbb1fe3569671, module_matplotlib$spines, sizeof(void *) );
    frame_780f5a3a09e3de88922cbb1fe3569671 = cache_frame_780f5a3a09e3de88922cbb1fe3569671;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_780f5a3a09e3de88922cbb1fe3569671 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_780f5a3a09e3de88922cbb1fe3569671 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_780f5a3a09e3de88922cbb1fe3569671->m_frame.f_lineno = 484;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__ensure_position_is_set );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 484;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__position );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 485;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_780f5a3a09e3de88922cbb1fe3569671 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_780f5a3a09e3de88922cbb1fe3569671 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_780f5a3a09e3de88922cbb1fe3569671 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_780f5a3a09e3de88922cbb1fe3569671, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_780f5a3a09e3de88922cbb1fe3569671->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_780f5a3a09e3de88922cbb1fe3569671, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_780f5a3a09e3de88922cbb1fe3569671,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_780f5a3a09e3de88922cbb1fe3569671 == cache_frame_780f5a3a09e3de88922cbb1fe3569671 )
    {
        Py_DECREF( frame_780f5a3a09e3de88922cbb1fe3569671 );
    }
    cache_frame_780f5a3a09e3de88922cbb1fe3569671 = NULL;

    assertFrameObject( frame_780f5a3a09e3de88922cbb1fe3569671 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_20_get_position );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_20_get_position );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_21_get_spine_transform( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_what = NULL;
    PyObject *var_how = NULL;
    PyObject *var_data_xform = NULL;
    PyObject *var_result = NULL;
    PyObject *var_base_transform = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_48a91bc7af0ddec1974fbdd87958d538;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_48a91bc7af0ddec1974fbdd87958d538 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_48a91bc7af0ddec1974fbdd87958d538, codeobj_48a91bc7af0ddec1974fbdd87958d538, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_48a91bc7af0ddec1974fbdd87958d538 = cache_frame_48a91bc7af0ddec1974fbdd87958d538;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_48a91bc7af0ddec1974fbdd87958d538 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_48a91bc7af0ddec1974fbdd87958d538 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 489;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__ensure_position_is_set );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 489;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__spine_transform );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 490;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 490;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 490;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 490;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 490;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 490;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_what == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_what = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_how == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_how = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_what );
        tmp_compexpr_left_1 = var_what;
        tmp_compexpr_right_1 = const_str_plain_data;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 492;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_left_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_right_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_source_name_5;
            PyObject *tmp_right_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_axes );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 494;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_transScale );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 494;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_how );
            tmp_left_name_3 = var_how;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_axes );
            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 495;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_transLimits );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 495;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 495;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_axes );
            if ( tmp_source_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 495;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_transAxes );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 495;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_3 );
            Py_DECREF( tmp_left_name_2 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 495;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 494;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( var_data_xform == NULL );
            var_data_xform = tmp_assign_source_6;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( par_self );
            tmp_source_name_8 = par_self;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_spine_type );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 496;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = LIST_COPY( const_list_str_plain_left_str_plain_right_list );
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 496;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_9;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_source_name_10;
                PyObject *tmp_source_name_11;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 497;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_9 = tmp_mvar_value_1;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_blended_transform_factory );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 497;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_data_xform );
                tmp_args_element_name_1 = var_data_xform;
                CHECK_OBJECT( par_self );
                tmp_source_name_11 = par_self;
                tmp_source_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_axes );
                if ( tmp_source_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 498;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_transData );
                Py_DECREF( tmp_source_name_10 );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 498;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 497;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                    tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_assign_source_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 497;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_result == NULL );
                var_result = tmp_assign_source_7;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_source_name_12;
                CHECK_OBJECT( par_self );
                tmp_source_name_12 = par_self;
                tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_spine_type );
                if ( tmp_compexpr_left_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 499;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_3 = LIST_COPY( const_list_str_plain_top_str_plain_bottom_list );
                tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
                Py_DECREF( tmp_compexpr_left_3 );
                Py_DECREF( tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 499;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_8;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_13;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_source_name_14;
                    PyObject *tmp_source_name_15;
                    PyObject *tmp_args_element_name_4;
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mtransforms );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mtransforms" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 500;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_13 = tmp_mvar_value_2;
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_blended_transform_factory );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 500;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_self );
                    tmp_source_name_15 = par_self;
                    tmp_source_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_axes );
                    if ( tmp_source_name_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 501;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_transData );
                    Py_DECREF( tmp_source_name_14 );
                    if ( tmp_args_element_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 501;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_data_xform );
                    tmp_args_element_name_4 = var_data_xform;
                    frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 500;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                        tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
                    }

                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_args_element_name_3 );
                    if ( tmp_assign_source_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 500;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_result == NULL );
                    var_result = tmp_assign_source_8;
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_make_exception_arg_1;
                    PyObject *tmp_left_name_4;
                    PyObject *tmp_right_name_4;
                    PyObject *tmp_source_name_16;
                    tmp_left_name_4 = const_str_digest_282263d2fa2adfc14d01396958fadf12;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_16 = par_self;
                    tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_spine_type );
                    if ( tmp_right_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 504;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                    Py_DECREF( tmp_right_name_4 );
                    if ( tmp_make_exception_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 503;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 503;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_1 };
                        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                    }

                    Py_DECREF( tmp_make_exception_arg_1 );
                    assert( !(tmp_raise_type_1 == NULL) );
                    exception_type = tmp_raise_type_1;
                    exception_lineno = 503;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        if ( var_result == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 505;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = var_result;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_17;
        CHECK_OBJECT( par_self );
        tmp_source_name_17 = par_self;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_spine_type );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 507;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = LIST_COPY( const_list_str_plain_left_str_plain_right_list );
        tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        Py_DECREF( tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 507;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_18;
            PyObject *tmp_source_name_19;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_19 = par_self;
            tmp_source_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_axes );
            if ( tmp_source_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 508;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_get_yaxis_transform );
            Py_DECREF( tmp_source_name_18 );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 508;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_0563751b61a311cc65ebabc9436682b6 );
            frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 508;
            tmp_assign_source_9 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 508;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( var_base_transform == NULL );
            var_base_transform = tmp_assign_source_9;
        }
        goto branch_end_4;
        branch_no_4:;
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_source_name_20;
            CHECK_OBJECT( par_self );
            tmp_source_name_20 = par_self;
            tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_spine_type );
            if ( tmp_compexpr_left_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 509;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_5 = LIST_COPY( const_list_str_plain_top_str_plain_bottom_list );
            tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
            Py_DECREF( tmp_compexpr_left_5 );
            Py_DECREF( tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 509;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_21;
                PyObject *tmp_source_name_22;
                PyObject *tmp_kw_name_2;
                CHECK_OBJECT( par_self );
                tmp_source_name_22 = par_self;
                tmp_source_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_axes );
                if ( tmp_source_name_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 510;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_get_xaxis_transform );
                Py_DECREF( tmp_source_name_21 );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 510;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_kw_name_2 = PyDict_Copy( const_dict_0563751b61a311cc65ebabc9436682b6 );
                frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 510;
                tmp_assign_source_10 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_2 );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_kw_name_2 );
                if ( tmp_assign_source_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 510;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_base_transform == NULL );
                var_base_transform = tmp_assign_source_10;
            }
            goto branch_end_5;
            branch_no_5:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                PyObject *tmp_source_name_23;
                tmp_left_name_5 = const_str_digest_282263d2fa2adfc14d01396958fadf12;
                CHECK_OBJECT( par_self );
                tmp_source_name_23 = par_self;
                tmp_right_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_spine_type );
                if ( tmp_right_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 513;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_make_exception_arg_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_make_exception_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 512;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 512;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_2 );
                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 512;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            branch_end_5:;
        }
        branch_end_4:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        CHECK_OBJECT( var_what );
        tmp_compexpr_left_6 = var_what;
        tmp_compexpr_right_6 = const_str_plain_identity;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 515;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        if ( var_base_transform == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "base_transform" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 516;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = var_base_transform;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            CHECK_OBJECT( var_what );
            tmp_compexpr_left_7 = var_what;
            tmp_compexpr_right_7 = const_str_plain_post;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 517;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_left_name_6;
                PyObject *tmp_right_name_6;
                if ( var_base_transform == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "base_transform" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 518;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_6 = var_base_transform;
                CHECK_OBJECT( var_how );
                tmp_right_name_6 = var_how;
                tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_6 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 518;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_7;
            branch_no_7:;
            {
                nuitka_bool tmp_condition_result_8;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                CHECK_OBJECT( var_what );
                tmp_compexpr_left_8 = var_what;
                tmp_compexpr_right_8 = const_str_plain_pre;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 519;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_left_name_7;
                    PyObject *tmp_right_name_7;
                    CHECK_OBJECT( var_how );
                    tmp_left_name_7 = var_how;
                    if ( var_base_transform == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "base_transform" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 520;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_right_name_7 = var_base_transform;
                    tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_7 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 520;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                goto branch_end_8;
                branch_no_8:;
                {
                    PyObject *tmp_raise_type_3;
                    PyObject *tmp_make_exception_arg_3;
                    PyObject *tmp_left_name_8;
                    PyObject *tmp_right_name_8;
                    tmp_left_name_8 = const_str_digest_879be27065d0bc95273e5f3bff67279d;
                    CHECK_OBJECT( var_what );
                    tmp_right_name_8 = var_what;
                    tmp_make_exception_arg_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_8, tmp_right_name_8 );
                    if ( tmp_make_exception_arg_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 522;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_48a91bc7af0ddec1974fbdd87958d538->m_frame.f_lineno = 522;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_3 };
                        tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                    }

                    Py_DECREF( tmp_make_exception_arg_3 );
                    assert( !(tmp_raise_type_3 == NULL) );
                    exception_type = tmp_raise_type_3;
                    exception_lineno = 522;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                branch_end_8:;
            }
            branch_end_7:;
        }
        branch_end_6:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_48a91bc7af0ddec1974fbdd87958d538 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_48a91bc7af0ddec1974fbdd87958d538 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_48a91bc7af0ddec1974fbdd87958d538 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_48a91bc7af0ddec1974fbdd87958d538, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_48a91bc7af0ddec1974fbdd87958d538->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_48a91bc7af0ddec1974fbdd87958d538, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_48a91bc7af0ddec1974fbdd87958d538,
        type_description_1,
        par_self,
        var_what,
        var_how,
        var_data_xform,
        var_result,
        var_base_transform
    );


    // Release cached frame.
    if ( frame_48a91bc7af0ddec1974fbdd87958d538 == cache_frame_48a91bc7af0ddec1974fbdd87958d538 )
    {
        Py_DECREF( frame_48a91bc7af0ddec1974fbdd87958d538 );
    }
    cache_frame_48a91bc7af0ddec1974fbdd87958d538 = NULL;

    assertFrameObject( frame_48a91bc7af0ddec1974fbdd87958d538 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_21_get_spine_transform );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_what );
    Py_DECREF( var_what );
    var_what = NULL;

    CHECK_OBJECT( (PyObject *)var_how );
    Py_DECREF( var_how );
    var_how = NULL;

    Py_XDECREF( var_data_xform );
    var_data_xform = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_base_transform );
    var_base_transform = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_what );
    var_what = NULL;

    Py_XDECREF( var_how );
    var_how = NULL;

    Py_XDECREF( var_data_xform );
    var_data_xform = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_base_transform );
    var_base_transform = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_21_get_spine_transform );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_22_set_bounds( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_low = python_pars[ 1 ];
    PyObject *par_high = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_4f6e7d92b0ec0a548368184a30d4bb66;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_4f6e7d92b0ec0a548368184a30d4bb66 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4f6e7d92b0ec0a548368184a30d4bb66, codeobj_4f6e7d92b0ec0a548368184a30d4bb66, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4f6e7d92b0ec0a548368184a30d4bb66 = cache_frame_4f6e7d92b0ec0a548368184a30d4bb66;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4f6e7d92b0ec0a548368184a30d4bb66 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4f6e7d92b0ec0a548368184a30d4bb66 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_spine_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 526;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_circle;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 526;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_ba50f011523bfdce32fc7d5bde850c78;
            frame_4f6e7d92b0ec0a548368184a30d4bb66->m_frame.f_lineno = 527;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 527;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_low );
        tmp_tuple_element_1 = par_low;
        tmp_assattr_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assattr_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_high );
        tmp_tuple_element_1 = par_high;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assattr_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__bounds, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 529;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        tmp_assattr_name_2 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_stale, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 530;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4f6e7d92b0ec0a548368184a30d4bb66 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4f6e7d92b0ec0a548368184a30d4bb66 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4f6e7d92b0ec0a548368184a30d4bb66, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4f6e7d92b0ec0a548368184a30d4bb66->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4f6e7d92b0ec0a548368184a30d4bb66, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4f6e7d92b0ec0a548368184a30d4bb66,
        type_description_1,
        par_self,
        par_low,
        par_high
    );


    // Release cached frame.
    if ( frame_4f6e7d92b0ec0a548368184a30d4bb66 == cache_frame_4f6e7d92b0ec0a548368184a30d4bb66 )
    {
        Py_DECREF( frame_4f6e7d92b0ec0a548368184a30d4bb66 );
    }
    cache_frame_4f6e7d92b0ec0a548368184a30d4bb66 = NULL;

    assertFrameObject( frame_4f6e7d92b0ec0a548368184a30d4bb66 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_22_set_bounds );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_low );
    Py_DECREF( par_low );
    par_low = NULL;

    CHECK_OBJECT( (PyObject *)par_high );
    Py_DECREF( par_high );
    par_high = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_low );
    Py_DECREF( par_low );
    par_low = NULL;

    CHECK_OBJECT( (PyObject *)par_high );
    Py_DECREF( par_high );
    par_high = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_22_set_bounds );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_23_get_bounds( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_28f24e50057ecf6d6e80b1094bc189f0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_28f24e50057ecf6d6e80b1094bc189f0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_28f24e50057ecf6d6e80b1094bc189f0, codeobj_28f24e50057ecf6d6e80b1094bc189f0, module_matplotlib$spines, sizeof(void *) );
    frame_28f24e50057ecf6d6e80b1094bc189f0 = cache_frame_28f24e50057ecf6d6e80b1094bc189f0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_28f24e50057ecf6d6e80b1094bc189f0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_28f24e50057ecf6d6e80b1094bc189f0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__bounds );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 534;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_28f24e50057ecf6d6e80b1094bc189f0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_28f24e50057ecf6d6e80b1094bc189f0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_28f24e50057ecf6d6e80b1094bc189f0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_28f24e50057ecf6d6e80b1094bc189f0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_28f24e50057ecf6d6e80b1094bc189f0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_28f24e50057ecf6d6e80b1094bc189f0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_28f24e50057ecf6d6e80b1094bc189f0,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_28f24e50057ecf6d6e80b1094bc189f0 == cache_frame_28f24e50057ecf6d6e80b1094bc189f0 )
    {
        Py_DECREF( frame_28f24e50057ecf6d6e80b1094bc189f0 );
    }
    cache_frame_28f24e50057ecf6d6e80b1094bc189f0 = NULL;

    assertFrameObject( frame_28f24e50057ecf6d6e80b1094bc189f0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_23_get_bounds );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_23_get_bounds );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_24_linear_spine( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_axes = python_pars[ 1 ];
    PyObject *par_spine_type = python_pars[ 2 ];
    PyObject *par_kwargs = python_pars[ 3 ];
    PyObject *var_path = NULL;
    PyObject *var_result = NULL;
    struct Nuitka_FrameObject *frame_8f718093ca383841ea25feb47f13427b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8f718093ca383841ea25feb47f13427b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8f718093ca383841ea25feb47f13427b, codeobj_8f718093ca383841ea25feb47f13427b, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8f718093ca383841ea25feb47f13427b = cache_frame_8f718093ca383841ea25feb47f13427b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8f718093ca383841ea25feb47f13427b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8f718093ca383841ea25feb47f13427b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_spine_type );
        tmp_compexpr_left_1 = par_spine_type;
        tmp_compexpr_right_1 = const_str_plain_left;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 542;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_arg_element_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 543;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            tmp_call_arg_element_1 = LIST_COPY( const_list_f8554cd76988f9d6d7371cacaa6c6a26_list );
            frame_8f718093ca383841ea25feb47f13427b->m_frame.f_lineno = 543;
            {
                PyObject *call_args[] = { tmp_call_arg_element_1 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_Path, call_args );
            }

            Py_DECREF( tmp_call_arg_element_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 543;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( var_path == NULL );
            var_path = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_spine_type );
            tmp_compexpr_left_2 = par_spine_type;
            tmp_compexpr_right_2 = const_str_plain_right;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 544;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_call_arg_element_2;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 545;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_2 = tmp_mvar_value_2;
                tmp_call_arg_element_2 = LIST_COPY( const_list_cbfdd3127f278d92c67096f4b0d1b35b_list );
                frame_8f718093ca383841ea25feb47f13427b->m_frame.f_lineno = 545;
                {
                    PyObject *call_args[] = { tmp_call_arg_element_2 };
                    tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_Path, call_args );
                }

                Py_DECREF( tmp_call_arg_element_2 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 545;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_path == NULL );
                var_path = tmp_assign_source_2;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( par_spine_type );
                tmp_compexpr_left_3 = par_spine_type;
                tmp_compexpr_right_3 = const_str_plain_bottom;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 546;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_3;
                    PyObject *tmp_called_instance_3;
                    PyObject *tmp_mvar_value_3;
                    PyObject *tmp_call_arg_element_3;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 547;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_instance_3 = tmp_mvar_value_3;
                    tmp_call_arg_element_3 = LIST_COPY( const_list_4ed07426722cc6107a2a6c9fea42842d_list );
                    frame_8f718093ca383841ea25feb47f13427b->m_frame.f_lineno = 547;
                    {
                        PyObject *call_args[] = { tmp_call_arg_element_3 };
                        tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_Path, call_args );
                    }

                    Py_DECREF( tmp_call_arg_element_3 );
                    if ( tmp_assign_source_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 547;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_path == NULL );
                    var_path = tmp_assign_source_3;
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    CHECK_OBJECT( par_spine_type );
                    tmp_compexpr_left_4 = par_spine_type;
                    tmp_compexpr_right_4 = const_str_plain_top;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 548;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_assign_source_4;
                        PyObject *tmp_called_instance_4;
                        PyObject *tmp_mvar_value_4;
                        PyObject *tmp_call_arg_element_4;
                        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

                        if (unlikely( tmp_mvar_value_4 == NULL ))
                        {
                            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
                        }

                        if ( tmp_mvar_value_4 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 549;
                            type_description_1 = "oooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_instance_4 = tmp_mvar_value_4;
                        tmp_call_arg_element_4 = LIST_COPY( const_list_6286e504ff565f6e3366f4d017b6e11f_list );
                        frame_8f718093ca383841ea25feb47f13427b->m_frame.f_lineno = 549;
                        {
                            PyObject *call_args[] = { tmp_call_arg_element_4 };
                            tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_Path, call_args );
                        }

                        Py_DECREF( tmp_call_arg_element_4 );
                        if ( tmp_assign_source_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 549;
                            type_description_1 = "oooooo";
                            goto frame_exception_exit_1;
                        }
                        assert( var_path == NULL );
                        var_path = tmp_assign_source_4;
                    }
                    goto branch_end_4;
                    branch_no_4:;
                    {
                        PyObject *tmp_raise_type_1;
                        PyObject *tmp_make_exception_arg_1;
                        PyObject *tmp_left_name_1;
                        PyObject *tmp_right_name_1;
                        tmp_left_name_1 = const_str_digest_98873532fb91af52ecaeed111a5dfadd;
                        CHECK_OBJECT( par_spine_type );
                        tmp_right_name_1 = par_spine_type;
                        tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                        if ( tmp_make_exception_arg_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 551;
                            type_description_1 = "oooooo";
                            goto frame_exception_exit_1;
                        }
                        frame_8f718093ca383841ea25feb47f13427b->m_frame.f_lineno = 551;
                        {
                            PyObject *call_args[] = { tmp_make_exception_arg_1 };
                            tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                        }

                        Py_DECREF( tmp_make_exception_arg_1 );
                        assert( !(tmp_raise_type_1 == NULL) );
                        exception_type = tmp_raise_type_1;
                        exception_lineno = 551;
                        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    branch_end_4:;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_cls );
        tmp_dircall_arg1_1 = par_cls;
        CHECK_OBJECT( par_axes );
        tmp_tuple_element_1 = par_axes;
        tmp_dircall_arg2_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_spine_type );
        tmp_tuple_element_1 = par_spine_type;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        if ( var_path == NULL )
        {
            Py_DECREF( tmp_dircall_arg2_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "path" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 552;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = var_path;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_5 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 552;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_result == NULL );
        var_result = tmp_assign_source_5;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_result );
        tmp_source_name_1 = var_result;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_set_visible );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 553;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 553;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_5;
        tmp_called_instance_5 = const_str_digest_d12962331895cfa026fce7746c626985;
        CHECK_OBJECT( par_spine_type );
        tmp_args_element_name_2 = par_spine_type;
        frame_8f718093ca383841ea25feb47f13427b->m_frame.f_lineno = 553;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_subscript_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_format, call_args );
        }

        if ( tmp_subscript_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 553;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 553;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_8f718093ca383841ea25feb47f13427b->m_frame.f_lineno = 553;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 553;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f718093ca383841ea25feb47f13427b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f718093ca383841ea25feb47f13427b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8f718093ca383841ea25feb47f13427b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8f718093ca383841ea25feb47f13427b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8f718093ca383841ea25feb47f13427b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8f718093ca383841ea25feb47f13427b,
        type_description_1,
        par_cls,
        par_axes,
        par_spine_type,
        par_kwargs,
        var_path,
        var_result
    );


    // Release cached frame.
    if ( frame_8f718093ca383841ea25feb47f13427b == cache_frame_8f718093ca383841ea25feb47f13427b )
    {
        Py_DECREF( frame_8f718093ca383841ea25feb47f13427b );
    }
    cache_frame_8f718093ca383841ea25feb47f13427b = NULL;

    assertFrameObject( frame_8f718093ca383841ea25feb47f13427b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_24_linear_spine );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_spine_type );
    Py_DECREF( par_spine_type );
    par_spine_type = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_spine_type );
    Py_DECREF( par_spine_type );
    par_spine_type = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_24_linear_spine );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_25_arc_spine( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_axes = python_pars[ 1 ];
    PyObject *par_spine_type = python_pars[ 2 ];
    PyObject *par_center = python_pars[ 3 ];
    PyObject *par_radius = python_pars[ 4 ];
    PyObject *par_theta1 = python_pars[ 5 ];
    PyObject *par_theta2 = python_pars[ 6 ];
    PyObject *par_kwargs = python_pars[ 7 ];
    PyObject *var_path = NULL;
    PyObject *var_result = NULL;
    struct Nuitka_FrameObject *frame_0676f38a2d415a197c296686599259ee;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0676f38a2d415a197c296686599259ee = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0676f38a2d415a197c296686599259ee, codeobj_0676f38a2d415a197c296686599259ee, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0676f38a2d415a197c296686599259ee = cache_frame_0676f38a2d415a197c296686599259ee;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0676f38a2d415a197c296686599259ee );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0676f38a2d415a197c296686599259ee ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 563;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Path );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 563;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_theta1 );
        tmp_args_element_name_1 = par_theta1;
        CHECK_OBJECT( par_theta2 );
        tmp_args_element_name_2 = par_theta2;
        frame_0676f38a2d415a197c296686599259ee->m_frame.f_lineno = 563;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_arc, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 563;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_path == NULL );
        var_path = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_cls );
        tmp_dircall_arg1_1 = par_cls;
        CHECK_OBJECT( par_axes );
        tmp_tuple_element_1 = par_axes;
        tmp_dircall_arg2_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_spine_type );
        tmp_tuple_element_1 = par_spine_type;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_path );
        tmp_tuple_element_1 = var_path;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_2 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 564;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_result == NULL );
        var_result = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        CHECK_OBJECT( var_result );
        tmp_called_instance_2 = var_result;
        CHECK_OBJECT( par_center );
        tmp_args_element_name_3 = par_center;
        CHECK_OBJECT( par_radius );
        tmp_args_element_name_4 = par_radius;
        CHECK_OBJECT( par_theta1 );
        tmp_args_element_name_5 = par_theta1;
        CHECK_OBJECT( par_theta2 );
        tmp_args_element_name_6 = par_theta2;
        frame_0676f38a2d415a197c296686599259ee->m_frame.f_lineno = 565;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS4( tmp_called_instance_2, const_str_plain_set_patch_arc, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 565;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0676f38a2d415a197c296686599259ee );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0676f38a2d415a197c296686599259ee );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0676f38a2d415a197c296686599259ee, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0676f38a2d415a197c296686599259ee->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0676f38a2d415a197c296686599259ee, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0676f38a2d415a197c296686599259ee,
        type_description_1,
        par_cls,
        par_axes,
        par_spine_type,
        par_center,
        par_radius,
        par_theta1,
        par_theta2,
        par_kwargs,
        var_path,
        var_result
    );


    // Release cached frame.
    if ( frame_0676f38a2d415a197c296686599259ee == cache_frame_0676f38a2d415a197c296686599259ee )
    {
        Py_DECREF( frame_0676f38a2d415a197c296686599259ee );
    }
    cache_frame_0676f38a2d415a197c296686599259ee = NULL;

    assertFrameObject( frame_0676f38a2d415a197c296686599259ee );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_25_arc_spine );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_spine_type );
    Py_DECREF( par_spine_type );
    par_spine_type = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    CHECK_OBJECT( (PyObject *)par_theta1 );
    Py_DECREF( par_theta1 );
    par_theta1 = NULL;

    CHECK_OBJECT( (PyObject *)par_theta2 );
    Py_DECREF( par_theta2 );
    par_theta2 = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_path );
    Py_DECREF( var_path );
    var_path = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_spine_type );
    Py_DECREF( par_spine_type );
    par_spine_type = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    CHECK_OBJECT( (PyObject *)par_theta1 );
    Py_DECREF( par_theta1 );
    par_theta1 = NULL;

    CHECK_OBJECT( (PyObject *)par_theta2 );
    Py_DECREF( par_theta2 );
    par_theta2 = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_25_arc_spine );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_26_circular_spine( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_axes = python_pars[ 1 ];
    PyObject *par_center = python_pars[ 2 ];
    PyObject *par_radius = python_pars[ 3 ];
    PyObject *par_kwargs = python_pars[ 4 ];
    PyObject *var_path = NULL;
    PyObject *var_result = NULL;
    struct Nuitka_FrameObject *frame_b2f602459ae7ea8ad94c075daf334294;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b2f602459ae7ea8ad94c075daf334294 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b2f602459ae7ea8ad94c075daf334294, codeobj_b2f602459ae7ea8ad94c075daf334294, module_matplotlib$spines, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b2f602459ae7ea8ad94c075daf334294 = cache_frame_b2f602459ae7ea8ad94c075daf334294;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b2f602459ae7ea8ad94c075daf334294 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b2f602459ae7ea8ad94c075daf334294 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 573;
            type_description_1 = "ooooooNo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Path );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 573;
            type_description_1 = "ooooooNo";
            goto frame_exception_exit_1;
        }
        frame_b2f602459ae7ea8ad94c075daf334294->m_frame.f_lineno = 573;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_unit_circle );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 573;
            type_description_1 = "ooooooNo";
            goto frame_exception_exit_1;
        }
        assert( var_path == NULL );
        var_path = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_cls );
        tmp_dircall_arg1_1 = par_cls;
        CHECK_OBJECT( par_axes );
        tmp_tuple_element_1 = par_axes;
        tmp_dircall_arg2_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_circle;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_path );
        tmp_tuple_element_1 = var_path;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_2 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 575;
            type_description_1 = "ooooooNo";
            goto frame_exception_exit_1;
        }
        assert( var_result == NULL );
        var_result = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_result );
        tmp_called_instance_2 = var_result;
        CHECK_OBJECT( par_center );
        tmp_args_element_name_1 = par_center;
        CHECK_OBJECT( par_radius );
        tmp_args_element_name_2 = par_radius;
        frame_b2f602459ae7ea8ad94c075daf334294->m_frame.f_lineno = 576;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_set_patch_circle, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 576;
            type_description_1 = "ooooooNo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2f602459ae7ea8ad94c075daf334294 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2f602459ae7ea8ad94c075daf334294 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b2f602459ae7ea8ad94c075daf334294, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b2f602459ae7ea8ad94c075daf334294->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b2f602459ae7ea8ad94c075daf334294, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b2f602459ae7ea8ad94c075daf334294,
        type_description_1,
        par_cls,
        par_axes,
        par_center,
        par_radius,
        par_kwargs,
        var_path,
        NULL,
        var_result
    );


    // Release cached frame.
    if ( frame_b2f602459ae7ea8ad94c075daf334294 == cache_frame_b2f602459ae7ea8ad94c075daf334294 )
    {
        Py_DECREF( frame_b2f602459ae7ea8ad94c075daf334294 );
    }
    cache_frame_b2f602459ae7ea8ad94c075daf334294 = NULL;

    assertFrameObject( frame_b2f602459ae7ea8ad94c075daf334294 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_26_circular_spine );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_path );
    Py_DECREF( var_path );
    var_path = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_axes );
    Py_DECREF( par_axes );
    par_axes = NULL;

    CHECK_OBJECT( (PyObject *)par_center );
    Py_DECREF( par_center );
    par_center = NULL;

    CHECK_OBJECT( (PyObject *)par_radius );
    Py_DECREF( par_radius );
    par_radius = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_26_circular_spine );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$spines$$$function_27_set_color( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_c = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_d7af239eeca0da49d6a73d3836f4902b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d7af239eeca0da49d6a73d3836f4902b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d7af239eeca0da49d6a73d3836f4902b, codeobj_d7af239eeca0da49d6a73d3836f4902b, module_matplotlib$spines, sizeof(void *)+sizeof(void *) );
    frame_d7af239eeca0da49d6a73d3836f4902b = cache_frame_d7af239eeca0da49d6a73d3836f4902b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d7af239eeca0da49d6a73d3836f4902b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d7af239eeca0da49d6a73d3836f4902b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_c );
        tmp_args_element_name_1 = par_c;
        frame_d7af239eeca0da49d6a73d3836f4902b->m_frame.f_lineno = 593;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_set_edgecolor, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 593;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_stale, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 594;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d7af239eeca0da49d6a73d3836f4902b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d7af239eeca0da49d6a73d3836f4902b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d7af239eeca0da49d6a73d3836f4902b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d7af239eeca0da49d6a73d3836f4902b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d7af239eeca0da49d6a73d3836f4902b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d7af239eeca0da49d6a73d3836f4902b,
        type_description_1,
        par_self,
        par_c
    );


    // Release cached frame.
    if ( frame_d7af239eeca0da49d6a73d3836f4902b == cache_frame_d7af239eeca0da49d6a73d3836f4902b )
    {
        Py_DECREF( frame_d7af239eeca0da49d6a73d3836f4902b );
    }
    cache_frame_d7af239eeca0da49d6a73d3836f4902b = NULL;

    assertFrameObject( frame_d7af239eeca0da49d6a73d3836f4902b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_27_set_color );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_c );
    Py_DECREF( par_c );
    par_c = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_c );
    Py_DECREF( par_c );
    par_c = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$spines$$$function_27_set_color );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_10_get_window_extent( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_10_get_window_extent,
        const_str_plain_get_window_extent,
#if PYTHON_VERSION >= 300
        const_str_digest_4eb5c8b8ae4c516a96c86250167f0763,
#endif
        codeobj_6523a8fda21c49e09c95926afd905246,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_bdb87b76fac327390dcf9d24b80e6456,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_11_get_path(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_11_get_path,
        const_str_plain_get_path,
#if PYTHON_VERSION >= 300
        const_str_digest_74aa8353e4cb4da0043cf2dbbba91ba7,
#endif
        codeobj_7b36e0ef0cedb5b112ddf64accbe3ba5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_12__ensure_position_is_set(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_12__ensure_position_is_set,
        const_str_plain__ensure_position_is_set,
#if PYTHON_VERSION >= 300
        const_str_digest_d91088e024bb306c7c9816e9c6e3ea41,
#endif
        codeobj_c5bd117846bf6461fd37167d3813bbb2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_13_register_axis(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_13_register_axis,
        const_str_plain_register_axis,
#if PYTHON_VERSION >= 300
        const_str_digest_dbd58e05dc84889d5ad7cf580b405b20,
#endif
        codeobj_4e9246b0c43ceae3e3f16f0c7488584e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_c85d08013ec811690755b19130a310a3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_14_cla(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_14_cla,
        const_str_plain_cla,
#if PYTHON_VERSION >= 300
        const_str_digest_3f6a3a283f4f6d95d2c969fadb020cd2,
#endif
        codeobj_7bd8e10ceae9ecfcf087123d499c9345,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_470b7b4df7b47adb1238b6037fa4cd0a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_15_is_frame_like(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_15_is_frame_like,
        const_str_plain_is_frame_like,
#if PYTHON_VERSION >= 300
        const_str_digest_33812b40370aee00bde878bebcde1dfb,
#endif
        codeobj_bc7672b1c966a11cfbfcd3b29487a2e2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_cbff2e8d0e23c4ff96229c43d46e8c71,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_16__adjust_location(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_16__adjust_location,
        const_str_plain__adjust_location,
#if PYTHON_VERSION >= 300
        const_str_digest_d85de36ab700a79bc2609ad98fe19b50,
#endif
        codeobj_1732b7288de3ff07b5d0689f845f9ced,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_ec2beb12f88ffcdb17573ee7e2632e33,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_17_draw(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_17_draw,
        const_str_plain_draw,
#if PYTHON_VERSION >= 300
        const_str_digest_acebd7f4ec0b9d1cad545899e69bd71b,
#endif
        codeobj_ae20ba00c2eef183aa7a8fb216a670a8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_18__calc_offset_transform(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_18__calc_offset_transform,
        const_str_plain__calc_offset_transform,
#if PYTHON_VERSION >= 300
        const_str_digest_63927bfa0c43c549c9c79521fa550712,
#endif
        codeobj_dbd9f46b071cbd449b8d1631eb33a7a8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_e625b20cc5b20388c7b371959b1ae678,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_19_set_position(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_19_set_position,
        const_str_plain_set_position,
#if PYTHON_VERSION >= 300
        const_str_digest_43a0d20a36c74a4f0cc7701ef5b07729,
#endif
        codeobj_b45baa0ce76614000fd72883197e7273,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_c92d25a9b1a6742b310e8f13e2584d26,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_1___str__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_1___str__,
        const_str_plain___str__,
#if PYTHON_VERSION >= 300
        const_str_digest_cb20ecd0d46462f137bfb2aae1246d58,
#endif
        codeobj_7f1429bc54e5af598aab3ee86e64677f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_20_get_position(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_20_get_position,
        const_str_plain_get_position,
#if PYTHON_VERSION >= 300
        const_str_digest_7346bb7bbc2b801fa2a2b8257b91943c,
#endif
        codeobj_780f5a3a09e3de88922cbb1fe3569671,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_4d5fff89f9803dc4e2d1dfb63dc84c28,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_21_get_spine_transform(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_21_get_spine_transform,
        const_str_plain_get_spine_transform,
#if PYTHON_VERSION >= 300
        const_str_digest_f2cbfcf5028382197dc3153734a1c6cd,
#endif
        codeobj_48a91bc7af0ddec1974fbdd87958d538,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_caf6751bbb2c810036d850768fe28a21,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_22_set_bounds(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_22_set_bounds,
        const_str_plain_set_bounds,
#if PYTHON_VERSION >= 300
        const_str_digest_b3cc18b655aab4953fc3abb35d5e21ed,
#endif
        codeobj_4f6e7d92b0ec0a548368184a30d4bb66,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_3ea6191f8e6badb08a6c198cb3f8b746,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_23_get_bounds(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_23_get_bounds,
        const_str_plain_get_bounds,
#if PYTHON_VERSION >= 300
        const_str_digest_5b0eeed3299ebc0371e700bda377eebd,
#endif
        codeobj_28f24e50057ecf6d6e80b1094bc189f0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_3e39b8a66b612e3cde13c3b6246d1d12,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_24_linear_spine(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_24_linear_spine,
        const_str_plain_linear_spine,
#if PYTHON_VERSION >= 300
        const_str_digest_c0a88f9552b5ec7f51197257738e17a1,
#endif
        codeobj_8f718093ca383841ea25feb47f13427b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_ab1bc000ced809648b71ee9f8efb43ad,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_25_arc_spine(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_25_arc_spine,
        const_str_plain_arc_spine,
#if PYTHON_VERSION >= 300
        const_str_digest_5aefd65bb28639ccb724edefbb95e1a5,
#endif
        codeobj_0676f38a2d415a197c296686599259ee,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_8f8713570e1427948e1ca90180abc3e6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_26_circular_spine(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_26_circular_spine,
        const_str_plain_circular_spine,
#if PYTHON_VERSION >= 300
        const_str_digest_eedbb17a97575af3601940e525f2682d,
#endif
        codeobj_b2f602459ae7ea8ad94c075daf334294,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_e8221eb249e2a624dc2a35c5cdce05c5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_27_set_color(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_27_set_color,
        const_str_plain_set_color,
#if PYTHON_VERSION >= 300
        const_str_digest_4dbf58747abca490214bfaa2e0ede95c,
#endif
        codeobj_d7af239eeca0da49d6a73d3836f4902b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_9c77d829d34c9c75bf5eee383da79b08,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_2___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_2___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_5341bccd4e191a713da7092029c69a6d,
#endif
        codeobj_653fe4632b0f675c9ad3a6b8c195a957,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_4b37bc7bee6b785e69831b81725236ec,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_3_set_smart_bounds(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_3_set_smart_bounds,
        const_str_plain_set_smart_bounds,
#if PYTHON_VERSION >= 300
        const_str_digest_2f9f2cda6eace1794aa0f038d9da3819,
#endif
        codeobj_f7cd8877f5a6d65e18c1760d0e79da16,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_f3047b8c15cc12f7c448de553e890bfd,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_4_get_smart_bounds(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_4_get_smart_bounds,
        const_str_plain_get_smart_bounds,
#if PYTHON_VERSION >= 300
        const_str_digest_07e50c74d2c6c2d4a635225d2f41a719,
#endif
        codeobj_06a62e70be890a801744ddcdb68e42fa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_d7df2bc1ee2d5e920ce5e74ee0a59514,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_5_set_patch_arc(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_5_set_patch_arc,
        const_str_plain_set_patch_arc,
#if PYTHON_VERSION >= 300
        const_str_digest_ed10b850583ad5f670e105ca391d49d2,
#endif
        codeobj_6c4f3735db0bf97914eedcbb2abda000,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_dd619daddaa7d6aac3f692e9458ec6a1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_6_set_patch_circle(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_6_set_patch_circle,
        const_str_plain_set_patch_circle,
#if PYTHON_VERSION >= 300
        const_str_digest_05884c283e9966b055226c4ef8509afc,
#endif
        codeobj_d26c04bf29cf1499b04ca9ff5aa2efe5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_97dbc6b904ee2c21b17f694d05e99ac4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_7_set_patch_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_7_set_patch_line,
        const_str_plain_set_patch_line,
#if PYTHON_VERSION >= 300
        const_str_digest_0509ad98bae8d5faed4f9802c9149ef1,
#endif
        codeobj_3e650edf4f100d9bb9124531d6ae97a4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_968c73a7ad9fd01fb4da32abe8c758cc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_8__recompute_transform(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_8__recompute_transform,
        const_str_plain__recompute_transform,
#if PYTHON_VERSION >= 300
        const_str_digest_60b6c7d6206290ea6562a9ab363af740,
#endif
        codeobj_aab338e1afcbcb75c3d4c51af7442c54,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        const_str_digest_504ff24db154606f1bfc1985dd5cc9e4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$spines$$$function_9_get_patch_transform(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$spines$$$function_9_get_patch_transform,
        const_str_plain_get_patch_transform,
#if PYTHON_VERSION >= 300
        const_str_digest_d1d094a2ccd8ffcb8a48b5737424aa70,
#endif
        codeobj_d27c36f537f61b2b493b1861184ecc05,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$spines,
        NULL,
        1
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$spines =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib.spines",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$spines)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$spines)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$spines );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib.spines: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.spines: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.spines: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$spines" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$spines = Py_InitModule4(
        "matplotlib.spines",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$spines = PyModule_Create( &mdef_matplotlib$spines );
#endif

    moduledict_matplotlib$spines = MODULE_DICT( module_matplotlib$spines );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$spines,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$spines,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$spines,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$spines,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$spines );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_cdc8c7af9e77520aff3a80ecb90be995, module_matplotlib$spines );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_CellObject *outline_0_var___class__ = PyCell_EMPTY();
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_4b24228e9d2116c3af2043f8b60ce364;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_matplotlib$spines_11 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_116eaf6e7626099dd562e6a26b48f471_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_116eaf6e7626099dd562e6a26b48f471_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_4b24228e9d2116c3af2043f8b60ce364 = MAKE_MODULE_FRAME( codeobj_4b24228e9d2116c3af2043f8b60ce364, module_matplotlib$spines );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_4b24228e9d2116c3af2043f8b60ce364 );
    assert( Py_REFCNT( frame_4b24228e9d2116c3af2043f8b60ce364 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_numpy;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$spines;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 1;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_np, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_matplotlib;
        tmp_globals_name_2 = (PyObject *)moduledict_matplotlib$spines;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 3;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_matplotlib, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_matplotlib;
        tmp_globals_name_3 = (PyObject *)moduledict_matplotlib$spines;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_cbook_str_plain_docstring_str_plain_rcParams_tuple;
        tmp_level_name_3 = const_int_0;
        frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 4;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_cbook );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_cbook, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_docstring );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_docstring, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_rcParams );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_rcParams, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_df695419470221061addd88ff9f521ff;
        tmp_globals_name_4 = (PyObject *)moduledict_matplotlib$spines;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_allow_rasterization_tuple;
        tmp_level_name_4 = const_int_0;
        frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 5;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_allow_rasterization );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_allow_rasterization, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_d28745151750b44e549d12f294293c52;
        tmp_globals_name_5 = (PyObject *)moduledict_matplotlib$spines;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 6;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_transforms );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mtransforms, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_7823f2c53877057ff15faa7c3608db53;
        tmp_globals_name_6 = (PyObject *)moduledict_matplotlib$spines;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 7;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_patches );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpatches, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_b4293af20113ad4e8388ca98e394ca7d;
        tmp_globals_name_7 = (PyObject *)moduledict_matplotlib$spines;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = Py_None;
        tmp_level_name_7 = const_int_0;
        frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 8;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_path );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpath, tmp_assign_source_13 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_mpatches );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpatches );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpatches" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 11;

            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Patch );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        tmp_assign_source_14 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_14, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_14;
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_15 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_16;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_17 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_17;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_2 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_2, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;

                goto try_except_handler_2;
            }
            tmp_tuple_element_2 = const_str_plain_Spine;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 11;
            tmp_assign_source_18 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_18;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_4 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;

                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_5 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_5 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 11;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 11;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_19;
            tmp_assign_source_19 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_19;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_20;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_matplotlib$spines_11 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_cdc8c7af9e77520aff3a80ecb90be995;
        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_a40723ed11e4414a2998ff867a896a6d;
        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_Spine;
        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_116eaf6e7626099dd562e6a26b48f471_2, codeobj_116eaf6e7626099dd562e6a26b48f471, module_matplotlib$spines, sizeof(void *) );
        frame_116eaf6e7626099dd562e6a26b48f471_2 = cache_frame_116eaf6e7626099dd562e6a26b48f471_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_116eaf6e7626099dd562e6a26b48f471_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_116eaf6e7626099dd562e6a26b48f471_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_1___str__(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain___str__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_1;
            tmp_called_instance_1 = PyObject_GetItem( locals_matplotlib$spines_11, const_str_plain_docstring );

            if ( tmp_called_instance_1 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_docstring );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_docstring );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "docstring" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 34;
                    type_description_2 = "c";
                    goto frame_exception_exit_2;
                }

                tmp_called_instance_1 = tmp_mvar_value_4;
                Py_INCREF( tmp_called_instance_1 );
                }
            }

            tmp_args_element_name_1 = MAKE_FUNCTION_matplotlib$spines$$$function_2___init__(  );

            ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] = outline_0_var___class__;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] );


            frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame.f_lineno = 34;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_dedent_interpd, call_args );
            }

            Py_DECREF( tmp_called_instance_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_3_set_smart_bounds(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_set_smart_bounds, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_4_get_smart_bounds(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_get_smart_bounds, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_5_set_patch_arc(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_set_patch_arc, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_6_set_patch_circle(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_set_patch_circle, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_7_set_patch_line(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_set_patch_line, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_8__recompute_transform(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain__recompute_transform, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_9_get_patch_transform(  );

        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_0_var___class__;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );


        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_get_patch_transform, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_10_get_window_extent( tmp_defaults_1 );

            ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_0_var___class__;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );


            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_get_window_extent, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_11_get_path(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_get_path, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_12__ensure_position_is_set(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain__ensure_position_is_set, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_13_register_axis(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_register_axis, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 214;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_14_cla(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_cla, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_2;
            tmp_called_instance_2 = PyObject_GetItem( locals_matplotlib$spines_11, const_str_plain_cbook );

            if ( tmp_called_instance_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_cbook );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 232;
                    type_description_2 = "c";
                    goto frame_exception_exit_2;
                }

                tmp_called_instance_2 = tmp_mvar_value_5;
                Py_INCREF( tmp_called_instance_2 );
                }
            }

            frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame.f_lineno = 232;
            tmp_called_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_deprecated, &PyTuple_GET_ITEM( const_tuple_str_digest_50e4933a9d0fc470d2deeb63d403662b_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 232;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_2 = MAKE_FUNCTION_matplotlib$spines$$$function_15_is_frame_like(  );



            frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame.f_lineno = 232;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 232;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_is_frame_like, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 232;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_16__adjust_location(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain__adjust_location, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 254;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_3;
            tmp_called_name_3 = PyObject_GetItem( locals_matplotlib$spines_11, const_str_plain_allow_rasterization );

            if ( tmp_called_name_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_allow_rasterization );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_allow_rasterization );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "allow_rasterization" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 362;
                    type_description_2 = "c";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_3 = tmp_mvar_value_6;
                Py_INCREF( tmp_called_name_3 );
                }
            }

            tmp_args_element_name_3 = MAKE_FUNCTION_matplotlib$spines$$$function_17_draw(  );

            ((struct Nuitka_FunctionObject *)tmp_args_element_name_3)->m_closure[0] = outline_0_var___class__;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_3)->m_closure[0] );


            frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame.f_lineno = 362;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 362;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_draw, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 362;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_18__calc_offset_transform(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain__calc_offset_transform, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_19_set_position(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_set_position, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 443;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_20_get_position(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_get_position, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 482;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_21_get_spine_transform(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_get_spine_transform, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 487;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_22_set_bounds(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_set_bounds, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 524;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_23_get_bounds(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_get_bounds, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 532;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_classmethod_arg_1;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$spines_11, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 536;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_4 = PyObject_GetItem( locals_matplotlib$spines_11, const_str_plain_classmethod );

            if ( tmp_called_name_4 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 536;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 536;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_4 = MAKE_FUNCTION_matplotlib$spines$$$function_24_linear_spine(  );



            frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame.f_lineno = 536;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 536;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_classmethod_arg_1 = MAKE_FUNCTION_matplotlib$spines$$$function_24_linear_spine(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
            Py_DECREF( tmp_classmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 536;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_linear_spine, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 536;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_classmethod_arg_2;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$spines_11, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 557;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            tmp_called_name_5 = PyObject_GetItem( locals_matplotlib$spines_11, const_str_plain_classmethod );

            if ( tmp_called_name_5 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 557;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 557;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_5 = MAKE_FUNCTION_matplotlib$spines$$$function_25_arc_spine(  );



            frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame.f_lineno = 557;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 557;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_4;
            condexpr_false_4:;
            tmp_classmethod_arg_2 = MAKE_FUNCTION_matplotlib$spines$$$function_25_arc_spine(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_2 );
            Py_DECREF( tmp_classmethod_arg_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 557;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            condexpr_end_4:;
            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_arc_spine, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 557;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
        }
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_classmethod_arg_3;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$spines_11, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_8 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_5;
            }
            else
            {
                goto condexpr_false_5;
            }
            condexpr_true_5:;
            tmp_called_name_6 = PyObject_GetItem( locals_matplotlib$spines_11, const_str_plain_classmethod );

            if ( tmp_called_name_6 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 568;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_6 = MAKE_FUNCTION_matplotlib$spines$$$function_26_circular_spine(  );



            frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame.f_lineno = 568;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_5;
            condexpr_false_5:;
            tmp_classmethod_arg_3 = MAKE_FUNCTION_matplotlib$spines$$$function_26_circular_spine(  );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_3 );
            Py_DECREF( tmp_classmethod_arg_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
            condexpr_end_5:;
            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_circular_spine, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;
                type_description_2 = "c";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$spines$$$function_27_set_color(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain_set_color, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 579;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_116eaf6e7626099dd562e6a26b48f471_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_116eaf6e7626099dd562e6a26b48f471_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_116eaf6e7626099dd562e6a26b48f471_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_116eaf6e7626099dd562e6a26b48f471_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_116eaf6e7626099dd562e6a26b48f471_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_116eaf6e7626099dd562e6a26b48f471_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_116eaf6e7626099dd562e6a26b48f471_2 == cache_frame_116eaf6e7626099dd562e6a26b48f471_2 )
        {
            Py_DECREF( frame_116eaf6e7626099dd562e6a26b48f471_2 );
        }
        cache_frame_116eaf6e7626099dd562e6a26b48f471_2 = NULL;

        assertFrameObject( frame_116eaf6e7626099dd562e6a26b48f471_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;

                goto try_except_handler_4;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$spines_11, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_7 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_Spine;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_matplotlib$spines_11;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_4b24228e9d2116c3af2043f8b60ce364->m_frame.f_lineno = 11;
            tmp_assign_source_21 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 11;

                goto try_except_handler_4;
            }
            {
                PyObject *old = PyCell_GET( outline_0_var___class__ );
                PyCell_SET( outline_0_var___class__, tmp_assign_source_21 );
                Py_XDECREF( old );
            }

        }
        CHECK_OBJECT( PyCell_GET( outline_0_var___class__ ) );
        tmp_assign_source_20 = PyCell_GET( outline_0_var___class__ );
        Py_INCREF( tmp_assign_source_20 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$spines );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_matplotlib$spines_11 );
        locals_matplotlib$spines_11 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$spines_11 );
        locals_matplotlib$spines_11 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$spines );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$spines );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 11;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$spines, (Nuitka_StringObject *)const_str_plain_Spine, tmp_assign_source_20 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4b24228e9d2116c3af2043f8b60ce364 );
#endif
    popFrameStack();

    assertFrameObject( frame_4b24228e9d2116c3af2043f8b60ce364 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4b24228e9d2116c3af2043f8b60ce364 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4b24228e9d2116c3af2043f8b60ce364, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4b24228e9d2116c3af2043f8b60ce364->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4b24228e9d2116c3af2043f8b60ce364, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;


    return MOD_RETURN_VALUE( module_matplotlib$spines );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
