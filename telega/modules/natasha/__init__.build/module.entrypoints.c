/* Generated code for Python module 'entrypoints'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_entrypoints" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_entrypoints;
PyDictObject *moduledict_entrypoints;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_warn;
extern PyObject *const_str_plain_isdir;
extern PyObject *const_str_plain_itertools;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain_Distribution;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_cp;
extern PyObject *const_str_plain___name__;
static PyObject *const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_group;
static PyObject *const_str_plain_entry_point_pattern;
static PyObject *const_str_plain_egg_name;
extern PyObject *const_str_digest_e399ba4554180f37de594a6743234f17;
extern PyObject *const_str_plain___exit__;
static PyObject *const_str_plain_BadEntryPoint;
extern PyObject *const_str_plain_folder;
static PyObject *const_str_digest_ef8b11e24a7314c2dcc37d918c6c842a;
static PyObject *const_str_digest_bb1038cd44aa2b09e793965c82bc4a7d;
extern PyObject *const_str_plain_items;
static PyObject *const_str_digest_948b051c8fa1a2aa3c83e67ba6066340;
extern PyObject *const_str_plain_m;
static PyObject *const_tuple_str_plain_dist_version_tuple;
static PyObject *const_str_plain_distro_name_version;
extern PyObject *const_str_plain_from_string;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_7eff1490f60c741164bcb7f49a3a07cf;
extern PyObject *const_str_plain_staticmethod;
extern PyObject *const_str_chr_61;
static PyObject *const_str_plain_file_in_zip_pattern;
extern PyObject *const_str_plain_get_group_named;
static PyObject *const_dict_76e556d6c52e2dbcc094dfc5f551a7fe;
extern PyObject *const_str_plain_classmethod;
extern PyObject *const_str_plain___enter__;
extern PyObject *const_str_plain_isfile;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_glob;
static PyObject *const_tuple_str_plain_self_str_plain_group_str_plain_name_tuple;
extern PyObject *const_str_plain_zf;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_read;
static PyObject *const_str_plain_getinfo;
extern PyObject *const_str_plain_fu;
extern PyObject *const_tuple_none_none_none_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_epstr_tuple;
static PyObject *const_str_plain_distro_names_seen;
extern PyObject *const_slice_none_int_pos_2_none;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_contextlib;
extern PyObject *const_str_dot;
static PyObject *const_str_digest_13ed378b7410e21fad7ecf05fa88eca2;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_import_module;
extern PyObject *const_str_plain_extras;
extern PyObject *const_str_plain_add;
extern PyObject *const_str_plain_ZipFile;
extern PyObject *const_str_plain_warnings;
static PyObject *const_str_digest_c00ac55f660fa9d6bd94ce2aa4784c8c;
extern PyObject *const_str_plain_str;
static PyObject *const_str_digest_16e72aad35527e8bf9522820da57d4bc;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_delimiters;
static PyObject *const_str_digest_5a3dc9a5710d387f205bb6ab501c758b;
static PyObject *const_str_digest_1dd091117a5c21432bcdfa2e19ac0ff5;
static PyObject *const_tuple_none_str_plain_first_tuple;
extern PyObject *const_str_plain_read_file;
extern PyObject *const_str_plain_importlib;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_chr_45;
extern PyObject *const_str_digest_13128b1b0d1871349d3e6b743bd02a70;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple;
static PyObject *const_tuple_str_digest_eb8abca4a4f50447da0716f33bc5f4fd_tuple;
extern PyObject *const_str_plain_e;
static PyObject *const_str_plain_EntryPoint;
extern PyObject *const_str_plain_load;
static PyObject *const_tuple_str_plain_modulename_str_plain_objectname_str_plain_extras_tuple;
static PyObject *const_str_digest_13bb2dd9b06ecd57b66d7c5f80a7b6ba;
extern PyObject *const_str_plain_zipfile;
extern PyObject *const_str_plain_ConfigParser;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_tuple_str_chr_45_tuple;
static PyObject *const_str_digest_5d9095b13ac72d2f3542fe60a884dc5d;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_6897f207cd5c06470c8dae5c0fcbd936;
static PyObject *const_str_digest_141adb9d2f35e3281e60fef8ddb3351b;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_endswith;
extern PyObject *const_str_plain_configparser;
extern PyObject *const_str_plain_attr;
extern PyObject *const_tuple_str_plain_contextmanager_tuple;
extern PyObject *const_str_plain_NoSuchEntryPoint;
extern PyObject *const_tuple_str_chr_45_int_pos_1_tuple;
extern PyObject *const_str_plain_get_single;
extern PyObject *const_str_plain_compile;
static PyObject *const_str_plain_distro;
extern PyObject *const_str_plain_split;
extern PyObject *const_tuple_str_plain_e_tuple;
extern PyObject *const_str_plain_match;
extern PyObject *const_str_plain_config;
static PyObject *const_tuple_str_plain_configparser_tuple;
extern PyObject *const_str_plain_contextmanager;
extern PyObject *const_str_digest_30b3f90bc92c6e628ed5de0ed56164d2;
static PyObject *const_str_plain_object_name;
static PyObject *const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple;
extern PyObject *const_str_plain_splitext;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_digest_1cb0ef4017f52ef7e9ba307f4c246aaf;
extern PyObject *const_str_plain_optionxform;
extern PyObject *const_str_plain_f;
static PyObject *const_str_plain_CaseSensitiveConfigParser;
extern PyObject *const_str_plain_first;
extern PyObject *const_str_plain___version__;
static PyObject *const_str_digest_f962ccd29b65c27c6e2c4ebf97c53b05;
static PyObject *const_str_plain_objectname;
static PyObject *const_str_plain_err_to_warnings;
extern PyObject *const_int_0;
static PyObject *const_str_digest_964c9d3660d573692b72c24d5628626e;
static PyObject *const_tuple_str_plain_group_str_plain_path_str_plain_result_str_plain_ep_tuple;
static PyObject *const_str_digest_4cfecb27bec97b639142aaa4ac1b0331;
static PyObject *const_str_digest_a92be47747e8f6731d971726acd271be;
static PyObject *const_str_plain_ep_path;
static PyObject *const_str_digest_c7dd7cd78323cc9d4b8499907c7a923a;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_entrypoints;
extern PyObject *const_tuple_str_digest_34d7b029fde6190a4528caa876c94bcf_tuple;
static PyObject *const_str_plain_get_group_all;
static PyObject *const_str_plain_repeated_distro;
static PyObject *const_str_digest_40cf859baa3235ea630141f01acceeb8;
extern PyObject *const_tuple_str_chr_61_tuple;
static PyObject *const_str_digest_44d4a8d3065c4829e1a315cb8c6ac53d;
extern PyObject *const_str_digest_34d7b029fde6190a4528caa876c94bcf;
extern PyObject *const_str_plain_type;
static PyObject *const_str_plain_iter_files_distros;
static PyObject *const_str_plain_iglob;
extern PyObject *const_str_plain_dirname;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_tuple_type_Exception_tuple;
extern PyObject *const_str_plain_basename;
static PyObject *const_str_digest_d83b95a33b90ed35d20b5d2e756e19a5;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_source;
extern PyObject *const_str_plain___str__;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_io;
static PyObject *const_str_digest_55877b467cad3bdfff753865167c7968;
extern PyObject *const_str_plain_filename;
extern PyObject *const_str_plain_z;
static PyObject *const_str_digest_678f3080a7e13def35de5d6945bcf27c;
static PyObject *const_str_plain_infolist;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_module_name;
static PyObject *const_str_digest_e6aaf1b4360174938b1f474f888a17f2;
extern PyObject *const_str_plain_open;
extern PyObject *const_str_plain_is_zipfile;
extern PyObject *const_str_plain_modulename;
extern PyObject *const_str_plain_VERBOSE;
static PyObject *const_str_digest_58d5725a550c1decb228c839ea305688;
static PyObject *const_str_digest_7afeaed30035fa921fb1215f98240ee1;
extern PyObject *const_str_plain_chain;
extern PyObject *const_str_plain_ep;
static PyObject *const_tuple_str_digest_30b3f90bc92c6e628ed5de0ed56164d2_tuple;
static PyObject *const_tuple_1132e669f344342976692202871353d2_tuple;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
static PyObject *const_str_digest_08b412689f50806fa2820d12516efe5f;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_epstr;
extern PyObject *const_str_plain_rstrip;
static PyObject *const_str_digest_7ae94de8e546ee88855d387e29ed702b;
static PyObject *const_tuple_str_plain_self_str_plain_name_str_plain_version_tuple;
extern PyObject *const_str_plain_TextIOWrapper;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_str_plain___repr__;
static PyObject *const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple;
static PyObject *const_str_plain_dist_version;
extern PyObject *const_str_plain_mod;
extern PyObject *const_str_digest_91a36bc8f3f71591e35d77a1c3d31793;
static PyObject *const_str_digest_34dfa82f78216e205aea03d17bdcee2d;
extern PyObject *const_str_plain_version;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
static PyObject *const_tuple_str_plain_import_module_tuple;
extern PyObject *const_str_plain_obj;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_info;
static PyObject *const_str_digest_eb8abca4a4f50447da0716f33bc5f4fd;
static PyObject *const_str_plain_osp;
static PyObject *const_str_digest_c8c189412631363fa7dd7a818222a9c2;
extern PyObject *const_tuple_none_none_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_mod_str_plain_obj_str_plain_attr_tuple;
extern PyObject *const_str_plain_backports;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple, 0, const_str_plain_group ); Py_INCREF( const_str_plain_group );
    PyTuple_SET_ITEM( const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple, 2, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple, 3, const_str_plain_config ); Py_INCREF( const_str_plain_config );
    const_str_plain_distro = UNSTREAM_STRING_ASCII( &constant_bin[ 665073 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple, 4, const_str_plain_distro ); Py_INCREF( const_str_plain_distro );
    const_str_plain_epstr = UNSTREAM_STRING_ASCII( &constant_bin[ 665079 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple, 5, const_str_plain_epstr ); Py_INCREF( const_str_plain_epstr );
    const_str_plain_entry_point_pattern = UNSTREAM_STRING_ASCII( &constant_bin[ 665084 ], 19, 1 );
    const_str_plain_egg_name = UNSTREAM_STRING_ASCII( &constant_bin[ 665103 ], 8, 1 );
    const_str_plain_BadEntryPoint = UNSTREAM_STRING_ASCII( &constant_bin[ 665111 ], 13, 1 );
    const_str_digest_ef8b11e24a7314c2dcc37d918c6c842a = UNSTREAM_STRING_ASCII( &constant_bin[ 665124 ], 22, 0 );
    const_str_digest_bb1038cd44aa2b09e793965c82bc4a7d = UNSTREAM_STRING_ASCII( &constant_bin[ 665146 ], 67, 0 );
    const_str_digest_948b051c8fa1a2aa3c83e67ba6066340 = UNSTREAM_STRING_ASCII( &constant_bin[ 665213 ], 14, 0 );
    const_tuple_str_plain_dist_version_tuple = PyTuple_New( 1 );
    const_str_plain_dist_version = UNSTREAM_STRING_ASCII( &constant_bin[ 665151 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dist_version_tuple, 0, const_str_plain_dist_version ); Py_INCREF( const_str_plain_dist_version );
    const_str_plain_distro_name_version = UNSTREAM_STRING_ASCII( &constant_bin[ 665227 ], 19, 1 );
    const_str_digest_7eff1490f60c741164bcb7f49a3a07cf = UNSTREAM_STRING_ASCII( &constant_bin[ 665246 ], 55, 0 );
    const_str_plain_file_in_zip_pattern = UNSTREAM_STRING_ASCII( &constant_bin[ 665301 ], 19, 1 );
    const_dict_76e556d6c52e2dbcc094dfc5f551a7fe = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_76e556d6c52e2dbcc094dfc5f551a7fe, const_str_plain_delimiters, const_tuple_str_chr_61_tuple );
    assert( PyDict_Size( const_dict_76e556d6c52e2dbcc094dfc5f551a7fe ) == 1 );
    const_tuple_str_plain_self_str_plain_group_str_plain_name_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_group_str_plain_name_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_group_str_plain_name_tuple, 1, const_str_plain_group ); Py_INCREF( const_str_plain_group );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_group_str_plain_name_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain_getinfo = UNSTREAM_STRING_ASCII( &constant_bin[ 665320 ], 7, 1 );
    const_tuple_str_plain_self_str_plain_epstr_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_epstr_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_epstr_tuple, 1, const_str_plain_epstr ); Py_INCREF( const_str_plain_epstr );
    const_str_plain_distro_names_seen = UNSTREAM_STRING_ASCII( &constant_bin[ 665327 ], 17, 1 );
    const_str_digest_13ed378b7410e21fad7ecf05fa88eca2 = UNSTREAM_STRING_ASCII( &constant_bin[ 665344 ], 24, 0 );
    const_str_digest_c00ac55f660fa9d6bd94ce2aa4784c8c = UNSTREAM_STRING_ASCII( &constant_bin[ 665368 ], 19, 0 );
    const_str_digest_16e72aad35527e8bf9522820da57d4bc = UNSTREAM_STRING_ASCII( &constant_bin[ 665387 ], 89, 0 );
    const_str_digest_5a3dc9a5710d387f205bb6ab501c758b = UNSTREAM_STRING_ASCII( &constant_bin[ 665476 ], 11, 0 );
    const_str_digest_1dd091117a5c21432bcdfa2e19ac0ff5 = UNSTREAM_STRING_ASCII( &constant_bin[ 665487 ], 119, 0 );
    const_tuple_none_str_plain_first_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_none_str_plain_first_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_str_plain_first_tuple, 1, const_str_plain_first ); Py_INCREF( const_str_plain_first );
    const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 1, const_str_plain_epstr ); Py_INCREF( const_str_plain_epstr );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 3, const_str_plain_distro ); Py_INCREF( const_str_plain_distro );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 4, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 5, const_str_plain_mod ); Py_INCREF( const_str_plain_mod );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 6, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 7, const_str_plain_extras ); Py_INCREF( const_str_plain_extras );
    const_tuple_str_digest_eb8abca4a4f50447da0716f33bc5f4fd_tuple = PyTuple_New( 1 );
    const_str_digest_eb8abca4a4f50447da0716f33bc5f4fd = UNSTREAM_STRING_ASCII( &constant_bin[ 665606 ], 25, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_eb8abca4a4f50447da0716f33bc5f4fd_tuple, 0, const_str_digest_eb8abca4a4f50447da0716f33bc5f4fd ); Py_INCREF( const_str_digest_eb8abca4a4f50447da0716f33bc5f4fd );
    const_str_plain_EntryPoint = UNSTREAM_STRING_ASCII( &constant_bin[ 665114 ], 10, 1 );
    const_tuple_str_plain_modulename_str_plain_objectname_str_plain_extras_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_modulename_str_plain_objectname_str_plain_extras_tuple, 0, const_str_plain_modulename ); Py_INCREF( const_str_plain_modulename );
    const_str_plain_objectname = UNSTREAM_STRING_ASCII( &constant_bin[ 665422 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_modulename_str_plain_objectname_str_plain_extras_tuple, 1, const_str_plain_objectname ); Py_INCREF( const_str_plain_objectname );
    PyTuple_SET_ITEM( const_tuple_str_plain_modulename_str_plain_objectname_str_plain_extras_tuple, 2, const_str_plain_extras ); Py_INCREF( const_str_plain_extras );
    const_str_digest_13bb2dd9b06ecd57b66d7c5f80a7b6ba = UNSTREAM_STRING_ASCII( &constant_bin[ 665631 ], 20, 0 );
    const_str_digest_5d9095b13ac72d2f3542fe60a884dc5d = UNSTREAM_STRING_ASCII( &constant_bin[ 665651 ], 135, 0 );
    const_str_digest_6897f207cd5c06470c8dae5c0fcbd936 = UNSTREAM_STRING_ASCII( &constant_bin[ 665786 ], 39, 0 );
    const_str_digest_141adb9d2f35e3281e60fef8ddb3351b = UNSTREAM_STRING_ASCII( &constant_bin[ 665825 ], 21, 0 );
    const_tuple_str_plain_configparser_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_configparser_tuple, 0, const_str_plain_configparser ); Py_INCREF( const_str_plain_configparser );
    const_str_plain_object_name = UNSTREAM_STRING_ASCII( &constant_bin[ 665846 ], 11, 1 );
    const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 0, const_str_plain_group ); Py_INCREF( const_str_plain_group );
    PyTuple_SET_ITEM( const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 1, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 2, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 3, const_str_plain_config ); Py_INCREF( const_str_plain_config );
    PyTuple_SET_ITEM( const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 4, const_str_plain_distro ); Py_INCREF( const_str_plain_distro );
    PyTuple_SET_ITEM( const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 5, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 6, const_str_plain_epstr ); Py_INCREF( const_str_plain_epstr );
    const_str_digest_1cb0ef4017f52ef7e9ba307f4c246aaf = UNSTREAM_STRING_ASCII( &constant_bin[ 665857 ], 29, 0 );
    const_str_plain_CaseSensitiveConfigParser = UNSTREAM_STRING_ASCII( &constant_bin[ 665886 ], 25, 1 );
    const_str_digest_f962ccd29b65c27c6e2c4ebf97c53b05 = UNSTREAM_STRING_ASCII( &constant_bin[ 665911 ], 90, 0 );
    const_str_plain_err_to_warnings = UNSTREAM_STRING_ASCII( &constant_bin[ 665871 ], 15, 1 );
    const_str_digest_964c9d3660d573692b72c24d5628626e = UNSTREAM_STRING_ASCII( &constant_bin[ 666001 ], 67, 0 );
    const_tuple_str_plain_group_str_plain_path_str_plain_result_str_plain_ep_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_group_str_plain_path_str_plain_result_str_plain_ep_tuple, 0, const_str_plain_group ); Py_INCREF( const_str_plain_group );
    PyTuple_SET_ITEM( const_tuple_str_plain_group_str_plain_path_str_plain_result_str_plain_ep_tuple, 1, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_str_plain_group_str_plain_path_str_plain_result_str_plain_ep_tuple, 2, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_str_plain_group_str_plain_path_str_plain_result_str_plain_ep_tuple, 3, const_str_plain_ep ); Py_INCREF( const_str_plain_ep );
    const_str_digest_4cfecb27bec97b639142aaa4ac1b0331 = UNSTREAM_STRING_ASCII( &constant_bin[ 666068 ], 20, 0 );
    const_str_digest_a92be47747e8f6731d971726acd271be = UNSTREAM_STRING_ASCII( &constant_bin[ 666088 ], 26, 0 );
    const_str_plain_ep_path = UNSTREAM_STRING_ASCII( &constant_bin[ 666114 ], 7, 1 );
    const_str_digest_c7dd7cd78323cc9d4b8499907c7a923a = UNSTREAM_STRING_ASCII( &constant_bin[ 666121 ], 10, 0 );
    const_str_plain_get_group_all = UNSTREAM_STRING_ASCII( &constant_bin[ 666131 ], 13, 1 );
    const_str_plain_repeated_distro = UNSTREAM_STRING_ASCII( &constant_bin[ 666144 ], 15, 1 );
    const_str_digest_40cf859baa3235ea630141f01acceeb8 = UNSTREAM_STRING_ASCII( &constant_bin[ 666159 ], 48, 0 );
    const_str_digest_44d4a8d3065c4829e1a315cb8c6ac53d = UNSTREAM_STRING_ASCII( &constant_bin[ 666207 ], 25, 0 );
    const_str_plain_iter_files_distros = UNSTREAM_STRING_ASCII( &constant_bin[ 666232 ], 18, 1 );
    const_str_plain_iglob = UNSTREAM_STRING_ASCII( &constant_bin[ 666250 ], 5, 1 );
    const_str_digest_d83b95a33b90ed35d20b5d2e756e19a5 = UNSTREAM_STRING_ASCII( &constant_bin[ 666255 ], 15, 0 );
    const_str_digest_55877b467cad3bdfff753865167c7968 = UNSTREAM_STRING_ASCII( &constant_bin[ 666270 ], 19, 0 );
    const_str_digest_678f3080a7e13def35de5d6945bcf27c = UNSTREAM_STRING_ASCII( &constant_bin[ 665606 ], 8, 0 );
    const_str_plain_infolist = UNSTREAM_STRING_ASCII( &constant_bin[ 666289 ], 8, 1 );
    const_str_digest_e6aaf1b4360174938b1f474f888a17f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 666297 ], 21, 0 );
    const_str_digest_58d5725a550c1decb228c839ea305688 = UNSTREAM_STRING_ASCII( &constant_bin[ 666318 ], 35, 0 );
    const_str_digest_7afeaed30035fa921fb1215f98240ee1 = UNSTREAM_STRING_ASCII( &constant_bin[ 666353 ], 22, 0 );
    const_tuple_str_digest_30b3f90bc92c6e628ed5de0ed56164d2_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_30b3f90bc92c6e628ed5de0ed56164d2_tuple, 0, const_str_digest_30b3f90bc92c6e628ed5de0ed56164d2 ); Py_INCREF( const_str_digest_30b3f90bc92c6e628ed5de0ed56164d2 );
    const_tuple_1132e669f344342976692202871353d2_tuple = PyTuple_New( 15 );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 1, const_str_plain_repeated_distro ); Py_INCREF( const_str_plain_repeated_distro );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 2, const_str_plain_distro_names_seen ); Py_INCREF( const_str_plain_distro_names_seen );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 3, const_str_plain_folder ); Py_INCREF( const_str_plain_folder );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 4, const_str_plain_egg_name ); Py_INCREF( const_str_plain_egg_name );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 5, const_str_plain_distro ); Py_INCREF( const_str_plain_distro );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 6, const_str_plain_ep_path ); Py_INCREF( const_str_plain_ep_path );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 7, const_str_plain_cp ); Py_INCREF( const_str_plain_cp );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 8, const_str_plain_z ); Py_INCREF( const_str_plain_z );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 9, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 10, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 11, const_str_plain_fu ); Py_INCREF( const_str_plain_fu );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 12, const_str_plain_zf ); Py_INCREF( const_str_plain_zf );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 13, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_1132e669f344342976692202871353d2_tuple, 14, const_str_plain_distro_name_version ); Py_INCREF( const_str_plain_distro_name_version );
    const_str_digest_08b412689f50806fa2820d12516efe5f = UNSTREAM_STRING_ASCII( &constant_bin[ 666375 ], 21, 0 );
    const_str_digest_7ae94de8e546ee88855d387e29ed702b = UNSTREAM_STRING_ASCII( &constant_bin[ 665195 ], 16, 0 );
    const_tuple_str_plain_self_str_plain_name_str_plain_version_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_version_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_version_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_version_tuple, 2, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple, 2, const_str_plain_module_name ); Py_INCREF( const_str_plain_module_name );
    PyTuple_SET_ITEM( const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple, 3, const_str_plain_object_name ); Py_INCREF( const_str_plain_object_name );
    PyTuple_SET_ITEM( const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple, 4, const_str_plain_extras ); Py_INCREF( const_str_plain_extras );
    PyTuple_SET_ITEM( const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple, 5, const_str_plain_distro ); Py_INCREF( const_str_plain_distro );
    const_str_digest_34dfa82f78216e205aea03d17bdcee2d = UNSTREAM_STRING_ASCII( &constant_bin[ 666396 ], 58, 0 );
    const_tuple_str_plain_import_module_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_import_module_tuple, 0, const_str_plain_import_module ); Py_INCREF( const_str_plain_import_module );
    const_str_plain_osp = UNSTREAM_STRING_ASCII( &constant_bin[ 116213 ], 3, 1 );
    const_str_digest_c8c189412631363fa7dd7a818222a9c2 = UNSTREAM_STRING_ASCII( &constant_bin[ 666454 ], 386, 0 );
    const_tuple_str_plain_self_str_plain_mod_str_plain_obj_str_plain_attr_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_mod_str_plain_obj_str_plain_attr_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_mod_str_plain_obj_str_plain_attr_tuple, 1, const_str_plain_mod ); Py_INCREF( const_str_plain_mod );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_mod_str_plain_obj_str_plain_attr_tuple, 2, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_mod_str_plain_obj_str_plain_attr_tuple, 3, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_entrypoints( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_e7a21653f455668fe336842997db58a3;
static PyCodeObject *codeobj_3d06b94c70f17ac3e98edad0b759c663;
static PyCodeObject *codeobj_a63ac1107c8d65dae34066360d113e92;
static PyCodeObject *codeobj_f91d06a3ccd6785fe941a7059cd2aa88;
static PyCodeObject *codeobj_22cae30e30b61f966af73020fd42c7d3;
static PyCodeObject *codeobj_69a5ec53ca0712a2e1bccfd0310ed769;
static PyCodeObject *codeobj_1e964e02c56f2728b2d707b058571160;
static PyCodeObject *codeobj_1d0b1fe1ef1f55e9071542dd0f6143ab;
static PyCodeObject *codeobj_5af37954dc4d53f99cea1ad05cba7b68;
static PyCodeObject *codeobj_0a321b8033564d2fe34617ef9239c81d;
static PyCodeObject *codeobj_d6468e9d98fc5d43d5f2b2881d067ba8;
static PyCodeObject *codeobj_850dd1775b531284ea28a2a70765d5b8;
static PyCodeObject *codeobj_f794e5114fb011beb590d1db1297a5f7;
static PyCodeObject *codeobj_77991c516de63c615a30404b9fdabd67;
static PyCodeObject *codeobj_54cc5d1e7372367103d16d27ea439804;
static PyCodeObject *codeobj_2e933716041bab6c06a23de4e15ea1d6;
static PyCodeObject *codeobj_4a061c40103077024de98e7857e83c88;
static PyCodeObject *codeobj_2a04c1a76f6c9dbe155a394f7cad8d67;
static PyCodeObject *codeobj_af54eb11dcf9f66626745aa1a8c8d702;
static PyCodeObject *codeobj_a94a23ab97e50c47150f791064d420f0;
static PyCodeObject *codeobj_062bc9fd8a98ac2c90d2e9d78e82a878;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_948b051c8fa1a2aa3c83e67ba6066340 );
    codeobj_e7a21653f455668fe336842997db58a3 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_4cfecb27bec97b639142aaa4ac1b0331, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_3d06b94c70f17ac3e98edad0b759c663 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_BadEntryPoint, 36, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_a63ac1107c8d65dae34066360d113e92 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_CaseSensitiveConfigParser, 63, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_f91d06a3ccd6785fe941a7059cd2aa88 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Distribution, 108, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_22cae30e30b61f966af73020fd42c7d3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_EntryPoint, 67, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_69a5ec53ca0712a2e1bccfd0310ed769 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_NoSuchEntryPoint, 53, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_1e964e02c56f2728b2d707b058571160 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 39, const_tuple_str_plain_self_str_plain_epstr_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1d0b1fe1ef1f55e9071542dd0f6143ab = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 55, const_tuple_str_plain_self_str_plain_group_str_plain_name_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5af37954dc4d53f99cea1ad05cba7b68 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 68, const_tuple_37d29ac869de3b5e9d38c321951e4603_tuple, 6, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0a321b8033564d2fe34617ef9239c81d = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 109, const_tuple_str_plain_self_str_plain_name_str_plain_version_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d6468e9d98fc5d43d5f2b2881d067ba8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 75, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_850dd1775b531284ea28a2a70765d5b8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 113, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f794e5114fb011beb590d1db1297a5f7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___str__, 42, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_77991c516de63c615a30404b9fdabd67 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___str__, 59, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_54cc5d1e7372367103d16d27ea439804 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_err_to_warnings, 45, const_tuple_str_plain_e_tuple, 0, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2e933716041bab6c06a23de4e15ea1d6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_from_string, 89, const_tuple_6f4fc289205b80d96c8d23e7acaf82d6_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4a061c40103077024de98e7857e83c88 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_group_all, 230, const_tuple_ae0b5b491b83f7d27f410590b214c918_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2a04c1a76f6c9dbe155a394f7cad8d67 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_group_named, 219, const_tuple_str_plain_group_str_plain_path_str_plain_result_str_plain_ep_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_af54eb11dcf9f66626745aa1a8c8d702 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_single, 205, const_tuple_a4ef79b42a6f7c73c72fd494aaf899e5_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a94a23ab97e50c47150f791064d420f0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_iter_files_distros, 117, const_tuple_1132e669f344342976692202871353d2_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_062bc9fd8a98ac2c90d2e9d78e82a878 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load, 79, const_tuple_str_plain_self_str_plain_mod_str_plain_obj_str_plain_attr_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_maker( void );


static PyObject *entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_10___init__(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_11___repr__(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_12_iter_files_distros( PyObject *defaults );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_13_get_single( PyObject *defaults );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_14_get_group_named( PyObject *defaults );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_15_get_group_all( PyObject *defaults );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_1___init__(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_2___str__(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_3_err_to_warnings(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_4___init__(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_5___str__(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_6___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_7___repr__(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_8_load(  );


static PyObject *MAKE_FUNCTION_entrypoints$$$function_9_from_string( PyObject *defaults );


// The module function definitions.
static PyObject *impl_entrypoints$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_epstr = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_1e964e02c56f2728b2d707b058571160;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1e964e02c56f2728b2d707b058571160 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1e964e02c56f2728b2d707b058571160, codeobj_1e964e02c56f2728b2d707b058571160, module_entrypoints, sizeof(void *)+sizeof(void *) );
    frame_1e964e02c56f2728b2d707b058571160 = cache_frame_1e964e02c56f2728b2d707b058571160;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1e964e02c56f2728b2d707b058571160 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1e964e02c56f2728b2d707b058571160 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_epstr );
        tmp_assattr_name_1 = par_epstr;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_epstr, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e964e02c56f2728b2d707b058571160 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e964e02c56f2728b2d707b058571160 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1e964e02c56f2728b2d707b058571160, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1e964e02c56f2728b2d707b058571160->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1e964e02c56f2728b2d707b058571160, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1e964e02c56f2728b2d707b058571160,
        type_description_1,
        par_self,
        par_epstr
    );


    // Release cached frame.
    if ( frame_1e964e02c56f2728b2d707b058571160 == cache_frame_1e964e02c56f2728b2d707b058571160 )
    {
        Py_DECREF( frame_1e964e02c56f2728b2d707b058571160 );
    }
    cache_frame_1e964e02c56f2728b2d707b058571160 = NULL;

    assertFrameObject( frame_1e964e02c56f2728b2d707b058571160 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_epstr );
    Py_DECREF( par_epstr );
    par_epstr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_epstr );
    Py_DECREF( par_epstr );
    par_epstr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_2___str__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f794e5114fb011beb590d1db1297a5f7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f794e5114fb011beb590d1db1297a5f7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f794e5114fb011beb590d1db1297a5f7, codeobj_f794e5114fb011beb590d1db1297a5f7, module_entrypoints, sizeof(void *) );
    frame_f794e5114fb011beb590d1db1297a5f7 = cache_frame_f794e5114fb011beb590d1db1297a5f7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f794e5114fb011beb590d1db1297a5f7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f794e5114fb011beb590d1db1297a5f7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_1;
        tmp_left_name_1 = const_str_digest_58d5725a550c1decb228c839ea305688;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_epstr );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f794e5114fb011beb590d1db1297a5f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f794e5114fb011beb590d1db1297a5f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f794e5114fb011beb590d1db1297a5f7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f794e5114fb011beb590d1db1297a5f7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f794e5114fb011beb590d1db1297a5f7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f794e5114fb011beb590d1db1297a5f7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f794e5114fb011beb590d1db1297a5f7,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_f794e5114fb011beb590d1db1297a5f7 == cache_frame_f794e5114fb011beb590d1db1297a5f7 )
    {
        Py_DECREF( frame_f794e5114fb011beb590d1db1297a5f7 );
    }
    cache_frame_f794e5114fb011beb590d1db1297a5f7 = NULL;

    assertFrameObject( frame_f794e5114fb011beb590d1db1297a5f7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_2___str__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_2___str__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_3_err_to_warnings( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_maker();



    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_3_err_to_warnings );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_locals {
    PyObject *var_e;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_locals *generator_heap = (struct entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_e = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_54cc5d1e7372367103d16d27ea439804, module_entrypoints, sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_expression_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_expression_name_1 = Py_None;
        Py_INCREF( tmp_expression_name_1 );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 49;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Preserve existing published exception.
    generator_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_type_1 );
    generator_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_value_1 );
    generator_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_tb_1 );

    if ( generator_heap->exception_keeper_tb_1 == NULL )
    {
        generator_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }
    else if ( generator_heap->exception_keeper_lineno_1 != 0 )
    {
        generator_heap->exception_keeper_tb_1 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_1, generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( generator_heap->exception_keeper_value_1, (PyObject *)generator_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BadEntryPoint" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 50;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_3;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_1;
        generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 50;
            generator_heap->type_description_1 = "o";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = EXC_VALUE(PyThreadState_GET());
            assert( generator_heap->var_e == NULL );
            Py_INCREF( tmp_assign_source_1 );
            generator_heap->var_e = tmp_assign_source_1;
        }
        // Tried code:
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_unicode_arg_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_warnings );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 51;
                generator_heap->type_description_1 = "o";
                goto try_except_handler_4;
            }

            tmp_source_name_1 = tmp_mvar_value_2;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 51;
                generator_heap->type_description_1 = "o";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( generator_heap->var_e );
            tmp_unicode_arg_1 = generator_heap->var_e;
            tmp_args_element_name_1 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_called_name_1 );

                generator_heap->exception_lineno = 51;
                generator_heap->type_description_1 = "o";
                goto try_except_handler_4;
            }
            generator->m_frame->m_frame.f_lineno = 51;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 51;
                generator_heap->type_description_1 = "o";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_4:;
        generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        Py_XDECREF( generator_heap->var_e );
        generator_heap->var_e = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_2;
        generator_heap->exception_value = generator_heap->exception_keeper_value_2;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_2:;
        Py_XDECREF( generator_heap->var_e );
        generator_heap->var_e = NULL;

        goto branch_end_1;
        branch_no_1:;
        generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
        if (unlikely( generator_heap->tmp_result == false ))
        {
            generator_heap->exception_lineno = 48;
        }

        if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
        generator_heap->type_description_1 = "o";
        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings );
    return NULL;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator_heap->var_e
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_e );
    generator_heap->var_e = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->var_e );
    generator_heap->var_e = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_maker( void )
{
    return Nuitka_Generator_New(
        entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_context,
        module_entrypoints,
        const_str_plain_err_to_warnings,
#if PYTHON_VERSION >= 350
        const_str_digest_1cb0ef4017f52ef7e9ba307f4c246aaf,
#endif
        codeobj_54cc5d1e7372367103d16d27ea439804,
        0,
        sizeof(struct entrypoints$$$function_3_err_to_warnings$$$genobj_1_err_to_warnings_locals)
    );
}


static PyObject *impl_entrypoints$$$function_4___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_group = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_1d0b1fe1ef1f55e9071542dd0f6143ab;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1d0b1fe1ef1f55e9071542dd0f6143ab = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1d0b1fe1ef1f55e9071542dd0f6143ab, codeobj_1d0b1fe1ef1f55e9071542dd0f6143ab, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1d0b1fe1ef1f55e9071542dd0f6143ab = cache_frame_1d0b1fe1ef1f55e9071542dd0f6143ab;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1d0b1fe1ef1f55e9071542dd0f6143ab );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1d0b1fe1ef1f55e9071542dd0f6143ab ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_group );
        tmp_assattr_name_1 = par_group;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_group, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_name );
        tmp_assattr_name_2 = par_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_name, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d0b1fe1ef1f55e9071542dd0f6143ab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d0b1fe1ef1f55e9071542dd0f6143ab );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1d0b1fe1ef1f55e9071542dd0f6143ab, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1d0b1fe1ef1f55e9071542dd0f6143ab->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1d0b1fe1ef1f55e9071542dd0f6143ab, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1d0b1fe1ef1f55e9071542dd0f6143ab,
        type_description_1,
        par_self,
        par_group,
        par_name
    );


    // Release cached frame.
    if ( frame_1d0b1fe1ef1f55e9071542dd0f6143ab == cache_frame_1d0b1fe1ef1f55e9071542dd0f6143ab )
    {
        Py_DECREF( frame_1d0b1fe1ef1f55e9071542dd0f6143ab );
    }
    cache_frame_1d0b1fe1ef1f55e9071542dd0f6143ab = NULL;

    assertFrameObject( frame_1d0b1fe1ef1f55e9071542dd0f6143ab );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_4___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_4___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_5___str__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_77991c516de63c615a30404b9fdabd67;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_77991c516de63c615a30404b9fdabd67 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_77991c516de63c615a30404b9fdabd67, codeobj_77991c516de63c615a30404b9fdabd67, module_entrypoints, sizeof(void *) );
    frame_77991c516de63c615a30404b9fdabd67 = cache_frame_77991c516de63c615a30404b9fdabd67;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_77991c516de63c615a30404b9fdabd67 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_77991c516de63c615a30404b9fdabd67 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        tmp_source_name_1 = const_str_digest_6897f207cd5c06470c8dae5c0fcbd936;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        assert( !(tmp_called_name_1 == NULL) );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 60;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_group );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 60;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_77991c516de63c615a30404b9fdabd67->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_77991c516de63c615a30404b9fdabd67 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_77991c516de63c615a30404b9fdabd67 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_77991c516de63c615a30404b9fdabd67 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_77991c516de63c615a30404b9fdabd67, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_77991c516de63c615a30404b9fdabd67->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_77991c516de63c615a30404b9fdabd67, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_77991c516de63c615a30404b9fdabd67,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_77991c516de63c615a30404b9fdabd67 == cache_frame_77991c516de63c615a30404b9fdabd67 )
    {
        Py_DECREF( frame_77991c516de63c615a30404b9fdabd67 );
    }
    cache_frame_77991c516de63c615a30404b9fdabd67 = NULL;

    assertFrameObject( frame_77991c516de63c615a30404b9fdabd67 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_5___str__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_5___str__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_6___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_module_name = python_pars[ 2 ];
    PyObject *par_object_name = python_pars[ 3 ];
    PyObject *par_extras = python_pars[ 4 ];
    PyObject *par_distro = python_pars[ 5 ];
    struct Nuitka_FrameObject *frame_5af37954dc4d53f99cea1ad05cba7b68;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5af37954dc4d53f99cea1ad05cba7b68 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5af37954dc4d53f99cea1ad05cba7b68, codeobj_5af37954dc4d53f99cea1ad05cba7b68, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5af37954dc4d53f99cea1ad05cba7b68 = cache_frame_5af37954dc4d53f99cea1ad05cba7b68;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5af37954dc4d53f99cea1ad05cba7b68 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5af37954dc4d53f99cea1ad05cba7b68 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_name );
        tmp_assattr_name_1 = par_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_name, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_module_name );
        tmp_assattr_name_2 = par_module_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_module_name, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_object_name );
        tmp_assattr_name_3 = par_object_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_object_name, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_extras );
        tmp_assattr_name_4 = par_extras;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_extras, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        CHECK_OBJECT( par_distro );
        tmp_assattr_name_5 = par_distro;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_distro, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5af37954dc4d53f99cea1ad05cba7b68 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5af37954dc4d53f99cea1ad05cba7b68 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5af37954dc4d53f99cea1ad05cba7b68, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5af37954dc4d53f99cea1ad05cba7b68->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5af37954dc4d53f99cea1ad05cba7b68, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5af37954dc4d53f99cea1ad05cba7b68,
        type_description_1,
        par_self,
        par_name,
        par_module_name,
        par_object_name,
        par_extras,
        par_distro
    );


    // Release cached frame.
    if ( frame_5af37954dc4d53f99cea1ad05cba7b68 == cache_frame_5af37954dc4d53f99cea1ad05cba7b68 )
    {
        Py_DECREF( frame_5af37954dc4d53f99cea1ad05cba7b68 );
    }
    cache_frame_5af37954dc4d53f99cea1ad05cba7b68 = NULL;

    assertFrameObject( frame_5af37954dc4d53f99cea1ad05cba7b68 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_6___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_module_name );
    Py_DECREF( par_module_name );
    par_module_name = NULL;

    CHECK_OBJECT( (PyObject *)par_object_name );
    Py_DECREF( par_object_name );
    par_object_name = NULL;

    CHECK_OBJECT( (PyObject *)par_extras );
    Py_DECREF( par_extras );
    par_extras = NULL;

    CHECK_OBJECT( (PyObject *)par_distro );
    Py_DECREF( par_distro );
    par_distro = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_module_name );
    Py_DECREF( par_module_name );
    par_module_name = NULL;

    CHECK_OBJECT( (PyObject *)par_object_name );
    Py_DECREF( par_object_name );
    par_object_name = NULL;

    CHECK_OBJECT( (PyObject *)par_extras );
    Py_DECREF( par_extras );
    par_extras = NULL;

    CHECK_OBJECT( (PyObject *)par_distro );
    Py_DECREF( par_distro );
    par_distro = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_6___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_7___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d6468e9d98fc5d43d5f2b2881d067ba8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d6468e9d98fc5d43d5f2b2881d067ba8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d6468e9d98fc5d43d5f2b2881d067ba8, codeobj_d6468e9d98fc5d43d5f2b2881d067ba8, module_entrypoints, sizeof(void *) );
    frame_d6468e9d98fc5d43d5f2b2881d067ba8 = cache_frame_d6468e9d98fc5d43d5f2b2881d067ba8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d6468e9d98fc5d43d5f2b2881d067ba8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d6468e9d98fc5d43d5f2b2881d067ba8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        tmp_left_name_1 = const_str_digest_a92be47747e8f6731d971726acd271be;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 4 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_module_name );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 77;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_object_name );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 77;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_distro );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 77;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 3, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d6468e9d98fc5d43d5f2b2881d067ba8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d6468e9d98fc5d43d5f2b2881d067ba8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d6468e9d98fc5d43d5f2b2881d067ba8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d6468e9d98fc5d43d5f2b2881d067ba8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d6468e9d98fc5d43d5f2b2881d067ba8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d6468e9d98fc5d43d5f2b2881d067ba8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d6468e9d98fc5d43d5f2b2881d067ba8,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_d6468e9d98fc5d43d5f2b2881d067ba8 == cache_frame_d6468e9d98fc5d43d5f2b2881d067ba8 )
    {
        Py_DECREF( frame_d6468e9d98fc5d43d5f2b2881d067ba8 );
    }
    cache_frame_d6468e9d98fc5d43d5f2b2881d067ba8 = NULL;

    assertFrameObject( frame_d6468e9d98fc5d43d5f2b2881d067ba8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_7___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_7___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_8_load( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_mod = NULL;
    PyObject *var_obj = NULL;
    PyObject *var_attr = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_062bc9fd8a98ac2c90d2e9d78e82a878;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_062bc9fd8a98ac2c90d2e9d78e82a878 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_062bc9fd8a98ac2c90d2e9d78e82a878, codeobj_062bc9fd8a98ac2c90d2e9d78e82a878, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_062bc9fd8a98ac2c90d2e9d78e82a878 = cache_frame_062bc9fd8a98ac2c90d2e9d78e82a878;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_062bc9fd8a98ac2c90d2e9d78e82a878 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_062bc9fd8a98ac2c90d2e9d78e82a878 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_import_module );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_import_module );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "import_module" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 82;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_module_name );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_062bc9fd8a98ac2c90d2e9d78e82a878->m_frame.f_lineno = 82;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_mod == NULL );
        var_mod = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( var_mod );
        tmp_assign_source_2 = var_mod;
        assert( var_obj == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_obj = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_object_name );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 84;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_object_name );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_062bc9fd8a98ac2c90d2e9d78e82a878->m_frame.f_lineno = 85;
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_3;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooo";
                    exception_lineno = 85;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_5 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_attr;
                var_attr = tmp_assign_source_5;
                Py_INCREF( var_attr );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_getattr_target_1;
            PyObject *tmp_getattr_attr_1;
            CHECK_OBJECT( var_obj );
            tmp_getattr_target_1 = var_obj;
            CHECK_OBJECT( var_attr );
            tmp_getattr_attr_1 = var_attr;
            tmp_assign_source_6 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_obj;
                assert( old != NULL );
                var_obj = tmp_assign_source_6;
                Py_DECREF( old );
            }

        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        branch_no_1:;
    }
    if ( var_obj == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "obj" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 87;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_return_value = var_obj;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_062bc9fd8a98ac2c90d2e9d78e82a878 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_062bc9fd8a98ac2c90d2e9d78e82a878 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_062bc9fd8a98ac2c90d2e9d78e82a878 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_062bc9fd8a98ac2c90d2e9d78e82a878, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_062bc9fd8a98ac2c90d2e9d78e82a878->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_062bc9fd8a98ac2c90d2e9d78e82a878, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_062bc9fd8a98ac2c90d2e9d78e82a878,
        type_description_1,
        par_self,
        var_mod,
        var_obj,
        var_attr
    );


    // Release cached frame.
    if ( frame_062bc9fd8a98ac2c90d2e9d78e82a878 == cache_frame_062bc9fd8a98ac2c90d2e9d78e82a878 )
    {
        Py_DECREF( frame_062bc9fd8a98ac2c90d2e9d78e82a878 );
    }
    cache_frame_062bc9fd8a98ac2c90d2e9d78e82a878 = NULL;

    assertFrameObject( frame_062bc9fd8a98ac2c90d2e9d78e82a878 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_8_load );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_mod );
    Py_DECREF( var_mod );
    var_mod = NULL;

    Py_XDECREF( var_obj );
    var_obj = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_mod );
    var_mod = NULL;

    Py_XDECREF( var_obj );
    var_obj = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_8_load );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_9_from_string( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_epstr = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *par_distro = python_pars[ 3 ];
    PyObject *var_m = NULL;
    PyObject *var_mod = NULL;
    PyObject *var_obj = NULL;
    PyObject *var_extras = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_2e933716041bab6c06a23de4e15ea1d6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_2e933716041bab6c06a23de4e15ea1d6 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2e933716041bab6c06a23de4e15ea1d6, codeobj_2e933716041bab6c06a23de4e15ea1d6, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2e933716041bab6c06a23de4e15ea1d6 = cache_frame_2e933716041bab6c06a23de4e15ea1d6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2e933716041bab6c06a23de4e15ea1d6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2e933716041bab6c06a23de4e15ea1d6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_entry_point_pattern );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_entry_point_pattern );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "entry_point_pattern" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_epstr );
        tmp_args_element_name_1 = par_epstr;
        frame_2e933716041bab6c06a23de4e15ea1d6->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_match, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_m == NULL );
        var_m = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_m );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_m );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_m );
            tmp_called_instance_2 = var_m;
            frame_2e933716041bab6c06a23de4e15ea1d6->m_frame.f_lineno = 101;
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_str_plain_modulename_str_plain_objectname_str_plain_extras_tuple, 0 ) );

            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_tuple_unpack_1__source_iter == NULL );
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
            if ( tmp_assign_source_3 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooo";
                exception_lineno = 101;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_1 == NULL );
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooo";
                exception_lineno = 101;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_2 == NULL );
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
            if ( tmp_assign_source_5 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooo";
                exception_lineno = 101;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_3 == NULL );
            tmp_tuple_unpack_1__element_3 = tmp_assign_source_5;
        }
        {
            PyObject *tmp_iterator_name_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooooooo";
                        exception_lineno = 101;
                        goto try_except_handler_3;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooooooo";
                exception_lineno = 101;
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        Py_XDECREF( tmp_tuple_unpack_1__element_3 );
        tmp_tuple_unpack_1__element_3 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
            tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
            assert( var_mod == NULL );
            Py_INCREF( tmp_assign_source_6 );
            var_mod = tmp_assign_source_6;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
            tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
            assert( var_obj == NULL );
            Py_INCREF( tmp_assign_source_7 );
            var_obj = tmp_assign_source_7;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
            tmp_assign_source_8 = tmp_tuple_unpack_1__element_3;
            assert( var_extras == NULL );
            Py_INCREF( tmp_assign_source_8 );
            var_extras = tmp_assign_source_8;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_3 );
        tmp_tuple_unpack_1__element_3 = NULL;

        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( var_extras );
            tmp_compexpr_left_1 = var_extras;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 103;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_3 = tmp_mvar_value_2;
                tmp_args_element_name_2 = const_str_digest_91a36bc8f3f71591e35d77a1c3d31793;
                CHECK_OBJECT( var_extras );
                tmp_args_element_name_3 = var_extras;
                frame_2e933716041bab6c06a23de4e15ea1d6->m_frame.f_lineno = 103;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                    tmp_assign_source_9 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_3, const_str_plain_split, call_args );
                }

                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 103;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_extras;
                    assert( old != NULL );
                    var_extras = tmp_assign_source_9;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            CHECK_OBJECT( par_cls );
            tmp_called_name_1 = par_cls;
            CHECK_OBJECT( par_name );
            tmp_args_element_name_4 = par_name;
            CHECK_OBJECT( var_mod );
            tmp_args_element_name_5 = var_mod;
            CHECK_OBJECT( var_obj );
            tmp_args_element_name_6 = var_obj;
            CHECK_OBJECT( var_extras );
            tmp_args_element_name_7 = var_extras;
            CHECK_OBJECT( par_distro );
            tmp_args_element_name_8 = par_distro;
            frame_2e933716041bab6c06a23de4e15ea1d6->m_frame.f_lineno = 104;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_9;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BadEntryPoint" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 106;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_3;
            CHECK_OBJECT( par_epstr );
            tmp_args_element_name_9 = par_epstr;
            frame_2e933716041bab6c06a23de4e15ea1d6->m_frame.f_lineno = 106;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 106;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e933716041bab6c06a23de4e15ea1d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e933716041bab6c06a23de4e15ea1d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e933716041bab6c06a23de4e15ea1d6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2e933716041bab6c06a23de4e15ea1d6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2e933716041bab6c06a23de4e15ea1d6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2e933716041bab6c06a23de4e15ea1d6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2e933716041bab6c06a23de4e15ea1d6,
        type_description_1,
        par_cls,
        par_epstr,
        par_name,
        par_distro,
        var_m,
        var_mod,
        var_obj,
        var_extras
    );


    // Release cached frame.
    if ( frame_2e933716041bab6c06a23de4e15ea1d6 == cache_frame_2e933716041bab6c06a23de4e15ea1d6 )
    {
        Py_DECREF( frame_2e933716041bab6c06a23de4e15ea1d6 );
    }
    cache_frame_2e933716041bab6c06a23de4e15ea1d6 = NULL;

    assertFrameObject( frame_2e933716041bab6c06a23de4e15ea1d6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_9_from_string );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_epstr );
    Py_DECREF( par_epstr );
    par_epstr = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_distro );
    Py_DECREF( par_distro );
    par_distro = NULL;

    CHECK_OBJECT( (PyObject *)var_m );
    Py_DECREF( var_m );
    var_m = NULL;

    CHECK_OBJECT( (PyObject *)var_mod );
    Py_DECREF( var_mod );
    var_mod = NULL;

    CHECK_OBJECT( (PyObject *)var_obj );
    Py_DECREF( var_obj );
    var_obj = NULL;

    CHECK_OBJECT( (PyObject *)var_extras );
    Py_DECREF( var_extras );
    var_extras = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_epstr );
    Py_DECREF( par_epstr );
    par_epstr = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_distro );
    Py_DECREF( par_distro );
    par_distro = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_mod );
    var_mod = NULL;

    Py_XDECREF( var_obj );
    var_obj = NULL;

    Py_XDECREF( var_extras );
    var_extras = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_9_from_string );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_10___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_version = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_0a321b8033564d2fe34617ef9239c81d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0a321b8033564d2fe34617ef9239c81d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0a321b8033564d2fe34617ef9239c81d, codeobj_0a321b8033564d2fe34617ef9239c81d, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0a321b8033564d2fe34617ef9239c81d = cache_frame_0a321b8033564d2fe34617ef9239c81d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0a321b8033564d2fe34617ef9239c81d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0a321b8033564d2fe34617ef9239c81d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_name );
        tmp_assattr_name_1 = par_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_name, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_version );
        tmp_assattr_name_2 = par_version;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_version, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a321b8033564d2fe34617ef9239c81d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a321b8033564d2fe34617ef9239c81d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0a321b8033564d2fe34617ef9239c81d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0a321b8033564d2fe34617ef9239c81d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0a321b8033564d2fe34617ef9239c81d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0a321b8033564d2fe34617ef9239c81d,
        type_description_1,
        par_self,
        par_name,
        par_version
    );


    // Release cached frame.
    if ( frame_0a321b8033564d2fe34617ef9239c81d == cache_frame_0a321b8033564d2fe34617ef9239c81d )
    {
        Py_DECREF( frame_0a321b8033564d2fe34617ef9239c81d );
    }
    cache_frame_0a321b8033564d2fe34617ef9239c81d = NULL;

    assertFrameObject( frame_0a321b8033564d2fe34617ef9239c81d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_10___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_10___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_11___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_850dd1775b531284ea28a2a70765d5b8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_850dd1775b531284ea28a2a70765d5b8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_850dd1775b531284ea28a2a70765d5b8, codeobj_850dd1775b531284ea28a2a70765d5b8, module_entrypoints, sizeof(void *) );
    frame_850dd1775b531284ea28a2a70765d5b8 = cache_frame_850dd1775b531284ea28a2a70765d5b8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_850dd1775b531284ea28a2a70765d5b8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_850dd1775b531284ea28a2a70765d5b8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        tmp_left_name_1 = const_str_digest_13bb2dd9b06ecd57b66d7c5f80a7b6ba;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_version );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 114;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_850dd1775b531284ea28a2a70765d5b8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_850dd1775b531284ea28a2a70765d5b8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_850dd1775b531284ea28a2a70765d5b8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_850dd1775b531284ea28a2a70765d5b8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_850dd1775b531284ea28a2a70765d5b8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_850dd1775b531284ea28a2a70765d5b8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_850dd1775b531284ea28a2a70765d5b8,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_850dd1775b531284ea28a2a70765d5b8 == cache_frame_850dd1775b531284ea28a2a70765d5b8 )
    {
        Py_DECREF( frame_850dd1775b531284ea28a2a70765d5b8 );
    }
    cache_frame_850dd1775b531284ea28a2a70765d5b8 = NULL;

    assertFrameObject( frame_850dd1775b531284ea28a2a70765d5b8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_11___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_11___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_12_iter_files_distros( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_path = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_repeated_distro = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_path;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_repeated_distro;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_12_iter_files_distros );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_repeated_distro );
    Py_DECREF( par_repeated_distro );
    par_repeated_distro = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_repeated_distro );
    Py_DECREF( par_repeated_distro );
    par_repeated_distro = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_12_iter_files_distros );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_locals {
    PyObject *var_distro_names_seen;
    PyObject *var_folder;
    PyObject *var_egg_name;
    PyObject *var_distro;
    PyObject *var_ep_path;
    PyObject *var_cp;
    PyObject *var_z;
    PyObject *var_info;
    PyObject *var_f;
    PyObject *var_fu;
    PyObject *var_zf;
    PyObject *var_m;
    PyObject *var_distro_name_version;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_for_loop_2__for_iterator;
    PyObject *tmp_for_loop_2__iter_value;
    PyObject *tmp_for_loop_3__for_iterator;
    PyObject *tmp_for_loop_3__iter_value;
    PyObject *tmp_with_1__enter;
    PyObject *tmp_with_1__exit;
    PyObject *tmp_with_1__indicator;
    PyObject *tmp_with_1__source;
    PyObject *tmp_with_2__enter;
    PyObject *tmp_with_2__exit;
    PyObject *tmp_with_2__indicator;
    PyObject *tmp_with_2__source;
    PyObject *tmp_with_3__enter;
    PyObject *tmp_with_3__exit;
    PyObject *tmp_with_3__indicator;
    PyObject *tmp_with_3__source;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    int exception_keeper_lineno_7;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    int exception_keeper_lineno_12;
    PyObject *exception_preserved_type_4;
    PyObject *exception_preserved_value_4;
    PyTracebackObject *exception_preserved_tb_4;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    int exception_keeper_lineno_18;
};

static PyObject *entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_locals *generator_heap = (struct entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 4: goto yield_return_4;
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_distro_names_seen = NULL;
    generator_heap->var_folder = NULL;
    generator_heap->var_egg_name = NULL;
    generator_heap->var_distro = NULL;
    generator_heap->var_ep_path = NULL;
    generator_heap->var_cp = NULL;
    generator_heap->var_z = NULL;
    generator_heap->var_info = NULL;
    generator_heap->var_f = NULL;
    generator_heap->var_fu = NULL;
    generator_heap->var_zf = NULL;
    generator_heap->var_m = NULL;
    generator_heap->var_distro_name_version = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_for_loop_2__for_iterator = NULL;
    generator_heap->tmp_for_loop_2__iter_value = NULL;
    generator_heap->tmp_for_loop_3__for_iterator = NULL;
    generator_heap->tmp_for_loop_3__iter_value = NULL;
    generator_heap->tmp_with_1__enter = NULL;
    generator_heap->tmp_with_1__exit = NULL;
    generator_heap->tmp_with_1__indicator = NULL;
    generator_heap->tmp_with_1__source = NULL;
    generator_heap->tmp_with_2__enter = NULL;
    generator_heap->tmp_with_2__exit = NULL;
    generator_heap->tmp_with_2__indicator = NULL;
    generator_heap->tmp_with_2__source = NULL;
    generator_heap->tmp_with_3__enter = NULL;
    generator_heap->tmp_with_3__exit = NULL;
    generator_heap->tmp_with_3__indicator = NULL;
    generator_heap->tmp_with_3__source = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_a94a23ab97e50c47150f791064d420f0, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "path" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 118;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( generator->m_closure[0] );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 119;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 119;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = PyCell_GET( generator->m_closure[0] );
                PyCell_SET( generator->m_closure[0], tmp_assign_source_1 );
                Py_XDECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PySet_New( NULL );
        assert( generator_heap->var_distro_names_seen == NULL );
        generator_heap->var_distro_names_seen = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "path" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 127;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_iter_arg_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 127;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "ccooooooooooooo";
                generator_heap->exception_lineno = 127;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = generator_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = generator_heap->var_folder;
            generator_heap->var_folder = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_folder );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( generator_heap->var_folder );
        tmp_called_instance_2 = generator_heap->var_folder;
        generator->m_frame->m_frame.f_lineno = 128;
        tmp_called_instance_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_rstrip, &PyTuple_GET_ITEM( const_tuple_str_digest_34d7b029fde6190a4528caa876c94bcf_tuple, 0 ) );

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 128;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 128;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_digest_30b3f90bc92c6e628ed5de0ed56164d2_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 128;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_call_result_1 );

            generator_heap->exception_lineno = 128;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 130;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_called_instance_3 = tmp_mvar_value_2;
            CHECK_OBJECT( generator_heap->var_folder );
            tmp_args_element_name_1 = generator_heap->var_folder;
            generator->m_frame->m_frame.f_lineno = 130;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_basename, call_args );
            }

            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 130;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = generator_heap->var_egg_name;
                generator_heap->var_egg_name = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = const_str_chr_45;
            CHECK_OBJECT( generator_heap->var_egg_name );
            tmp_compexpr_right_2 = generator_heap->var_egg_name;
            generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 131;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_3 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_dircall_arg1_1;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_dircall_arg2_1;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_subscript_name_1;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_Distribution );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Distribution );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Distribution" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 132;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_dircall_arg1_1 = tmp_mvar_value_3;
                CHECK_OBJECT( generator_heap->var_egg_name );
                tmp_called_instance_4 = generator_heap->var_egg_name;
                generator->m_frame->m_frame.f_lineno = 132;
                tmp_subscribed_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_45_tuple, 0 ) );

                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 132;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_subscript_name_1 = const_slice_none_int_pos_2_none;
                tmp_dircall_arg2_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                Py_DECREF( tmp_subscribed_name_1 );
                if ( tmp_dircall_arg2_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 132;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                Py_INCREF( tmp_dircall_arg1_1 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
                    tmp_assign_source_7 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
                }
                if ( tmp_assign_source_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 132;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = generator_heap->var_distro;
                    generator_heap->var_distro = tmp_assign_source_7;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_4;
                int tmp_and_left_truth_1;
                nuitka_bool tmp_and_left_value_1;
                nuitka_bool tmp_and_right_value_1;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                PyObject *tmp_source_name_2;
                if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "repeated_distro" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 134;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_compexpr_left_3 = PyCell_GET( generator->m_closure[1] );
                tmp_compexpr_right_3 = const_str_plain_first;
                generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 134;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_and_left_value_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
                if ( tmp_and_left_truth_1 == 1 )
                {
                    goto and_right_1;
                }
                else
                {
                    goto and_left_1;
                }
                and_right_1:;
                CHECK_OBJECT( generator_heap->var_distro );
                tmp_source_name_2 = generator_heap->var_distro;
                tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
                if ( tmp_compexpr_left_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 135;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( generator_heap->var_distro_names_seen );
                tmp_compexpr_right_4 = generator_heap->var_distro_names_seen;
                generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
                Py_DECREF( tmp_compexpr_left_4 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 135;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_and_right_value_1 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_condition_result_4 = tmp_and_right_value_1;
                goto and_end_1;
                and_left_1:;
                tmp_condition_result_4 = tmp_and_left_value_1;
                and_end_1:;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                goto loop_start_1;
                branch_no_4:;
            }
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_3;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_source_name_4;
                CHECK_OBJECT( generator_heap->var_distro_names_seen );
                tmp_source_name_3 = generator_heap->var_distro_names_seen;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_add );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 137;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( generator_heap->var_distro );
                tmp_source_name_4 = generator_heap->var_distro;
                tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    generator_heap->exception_lineno = 137;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                generator->m_frame->m_frame.f_lineno = 137;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 137;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_assign_source_8;
                tmp_assign_source_8 = Py_None;
                {
                    PyObject *old = generator_heap->var_distro;
                    generator_heap->var_distro = tmp_assign_source_8;
                    Py_INCREF( generator_heap->var_distro );
                    Py_XDECREF( old );
                }

            }
            branch_end_3:;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_3;
            int tmp_truth_name_2;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 141;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_called_instance_5 = tmp_mvar_value_4;
            CHECK_OBJECT( generator_heap->var_folder );
            tmp_args_element_name_3 = generator_heap->var_folder;
            generator->m_frame->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_isdir, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 141;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_3 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_call_result_3 );

                generator_heap->exception_lineno = 141;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_5 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_3 );
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_called_instance_6;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_args_element_name_6;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 142;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_called_instance_6 = tmp_mvar_value_5;
                CHECK_OBJECT( generator_heap->var_folder );
                tmp_args_element_name_4 = generator_heap->var_folder;
                tmp_args_element_name_5 = const_str_digest_678f3080a7e13def35de5d6945bcf27c;
                tmp_args_element_name_6 = const_str_digest_7ae94de8e546ee88855d387e29ed702b;
                generator->m_frame->m_frame.f_lineno = 142;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                    tmp_assign_source_9 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_6, const_str_plain_join, call_args );
                }

                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 142;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = generator_heap->var_ep_path;
                    generator_heap->var_ep_path = tmp_assign_source_9;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_called_instance_7;
                PyObject *tmp_mvar_value_6;
                PyObject *tmp_call_result_4;
                PyObject *tmp_args_element_name_7;
                int tmp_truth_name_3;
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 143;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_called_instance_7 = tmp_mvar_value_6;
                CHECK_OBJECT( generator_heap->var_ep_path );
                tmp_args_element_name_7 = generator_heap->var_ep_path;
                generator->m_frame->m_frame.f_lineno = 143;
                {
                    PyObject *call_args[] = { tmp_args_element_name_7 };
                    tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_isfile, call_args );
                }

                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 143;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_4 );
                if ( tmp_truth_name_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_call_result_4 );

                    generator_heap->exception_lineno = 143;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_6 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_4 );
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_assign_source_10;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_mvar_value_7;
                    PyObject *tmp_kw_name_1;
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );

                    if (unlikely( tmp_mvar_value_7 == NULL ))
                    {
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );
                    }

                    if ( tmp_mvar_value_7 == NULL )
                    {

                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CaseSensitiveConfigParser" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 144;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_called_name_2 = tmp_mvar_value_7;
                    tmp_kw_name_1 = PyDict_Copy( const_dict_76e556d6c52e2dbcc094dfc5f551a7fe );
                    generator->m_frame->m_frame.f_lineno = 144;
                    tmp_assign_source_10 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
                    Py_DECREF( tmp_kw_name_1 );
                    if ( tmp_assign_source_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 144;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = generator_heap->var_cp;
                        generator_heap->var_cp = tmp_assign_source_10;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_called_instance_8;
                    PyObject *tmp_call_result_5;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_list_element_1;
                    CHECK_OBJECT( generator_heap->var_cp );
                    tmp_called_instance_8 = generator_heap->var_cp;
                    CHECK_OBJECT( generator_heap->var_ep_path );
                    tmp_list_element_1 = generator_heap->var_ep_path;
                    tmp_args_element_name_8 = PyList_New( 1 );
                    Py_INCREF( tmp_list_element_1 );
                    PyList_SET_ITEM( tmp_args_element_name_8, 0, tmp_list_element_1 );
                    generator->m_frame->m_frame.f_lineno = 145;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_8 };
                        tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_read, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_8 );
                    if ( tmp_call_result_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 145;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }
                    Py_DECREF( tmp_call_result_5 );
                }
                {
                    PyObject *tmp_expression_name_1;
                    PyObject *tmp_tuple_element_1;
                    NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
                    CHECK_OBJECT( generator_heap->var_cp );
                    tmp_tuple_element_1 = generator_heap->var_cp;
                    tmp_expression_name_1 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
                    CHECK_OBJECT( generator_heap->var_distro );
                    tmp_tuple_element_1 = generator_heap->var_distro;
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
                    Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_called_instance_5, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_call_result_3, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), &tmp_condition_result_6, sizeof(nuitka_bool), &tmp_called_instance_7, sizeof(PyObject *), &tmp_mvar_value_6, sizeof(PyObject *), &tmp_call_result_4, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_truth_name_3, sizeof(int), &tmp_tuple_element_1, sizeof(PyObject *), NULL );
                    generator->m_yield_return_index = 1;
                    return tmp_expression_name_1;
                    yield_return_1:
                    Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_called_instance_5, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_call_result_3, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), &tmp_condition_result_6, sizeof(nuitka_bool), &tmp_called_instance_7, sizeof(PyObject *), &tmp_mvar_value_6, sizeof(PyObject *), &tmp_call_result_4, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_truth_name_3, sizeof(int), &tmp_tuple_element_1, sizeof(PyObject *), NULL );
                    if ( yield_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 146;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_yield_result_1 = yield_return_value;
                }
                branch_no_6:;
            }
            goto branch_end_5;
            branch_no_5:;
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_called_instance_9;
                PyObject *tmp_mvar_value_8;
                PyObject *tmp_call_result_6;
                PyObject *tmp_args_element_name_9;
                int tmp_truth_name_4;
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_zipfile );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zipfile );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zipfile" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 148;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_called_instance_9 = tmp_mvar_value_8;
                CHECK_OBJECT( generator_heap->var_folder );
                tmp_args_element_name_9 = generator_heap->var_folder;
                generator->m_frame->m_frame.f_lineno = 148;
                {
                    PyObject *call_args[] = { tmp_args_element_name_9 };
                    tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_is_zipfile, call_args );
                }

                if ( tmp_call_result_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 148;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_truth_name_4 = CHECK_IF_TRUE( tmp_call_result_6 );
                if ( tmp_truth_name_4 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_call_result_6 );

                    generator_heap->exception_lineno = 148;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_7 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_6 );
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_assign_source_11;
                    PyObject *tmp_called_instance_10;
                    PyObject *tmp_mvar_value_9;
                    PyObject *tmp_args_element_name_10;
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_zipfile );

                    if (unlikely( tmp_mvar_value_9 == NULL ))
                    {
                        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zipfile );
                    }

                    if ( tmp_mvar_value_9 == NULL )
                    {

                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zipfile" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 149;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_called_instance_10 = tmp_mvar_value_9;
                    CHECK_OBJECT( generator_heap->var_folder );
                    tmp_args_element_name_10 = generator_heap->var_folder;
                    generator->m_frame->m_frame.f_lineno = 149;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_10 };
                        tmp_assign_source_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_ZipFile, call_args );
                    }

                    if ( tmp_assign_source_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 149;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = generator_heap->var_z;
                        generator_heap->var_z = tmp_assign_source_11;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_12;
                    PyObject *tmp_called_instance_11;
                    CHECK_OBJECT( generator_heap->var_z );
                    tmp_called_instance_11 = generator_heap->var_z;
                    generator->m_frame->m_frame.f_lineno = 151;
                    tmp_assign_source_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain_getinfo, &PyTuple_GET_ITEM( const_tuple_str_digest_eb8abca4a4f50447da0716f33bc5f4fd_tuple, 0 ) );

                    if ( tmp_assign_source_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 151;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_3;
                    }
                    {
                        PyObject *old = generator_heap->var_info;
                        generator_heap->var_info = tmp_assign_source_12;
                        Py_XDECREF( old );
                    }

                }
                goto try_end_1;
                // Exception handler code:
                try_except_handler_3:;
                generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
                generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
                generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
                generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
                generator_heap->exception_type = NULL;
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
                generator_heap->exception_lineno = 0;

                // Preserve existing published exception.
                generator_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
                Py_XINCREF( generator_heap->exception_preserved_type_1 );
                generator_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
                Py_XINCREF( generator_heap->exception_preserved_value_1 );
                generator_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
                Py_XINCREF( generator_heap->exception_preserved_tb_1 );

                if ( generator_heap->exception_keeper_tb_1 == NULL )
                {
                    generator_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_1 );
                }
                else if ( generator_heap->exception_keeper_lineno_1 != 0 )
                {
                    generator_heap->exception_keeper_tb_1 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_1, generator->m_frame, generator_heap->exception_keeper_lineno_1 );
                }

                NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
                PyException_SetTraceback( generator_heap->exception_keeper_value_1, (PyObject *)generator_heap->exception_keeper_tb_1 );
                PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
                // Tried code:
                {
                    nuitka_bool tmp_condition_result_8;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    tmp_compexpr_left_5 = EXC_TYPE(PyThreadState_GET());
                    tmp_compexpr_right_5 = PyExc_KeyError;
                    generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 152;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_4;
                    }
                    tmp_condition_result_8 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_8;
                    }
                    else
                    {
                        goto branch_no_8;
                    }
                    branch_yes_8:;
                    goto try_continue_handler_4;
                    goto branch_end_8;
                    branch_no_8:;
                    generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    if (unlikely( generator_heap->tmp_result == false ))
                    {
                        generator_heap->exception_lineno = 150;
                    }

                    if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_4;
                    branch_end_8:;
                }
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros );
                return NULL;
                // Exception handler code:
                try_except_handler_4:;
                generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
                generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
                generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
                generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
                generator_heap->exception_type = NULL;
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
                generator_heap->exception_lineno = 0;

                // Restore previous exception.
                SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
                // Re-raise.
                generator_heap->exception_type = generator_heap->exception_keeper_type_2;
                generator_heap->exception_value = generator_heap->exception_keeper_value_2;
                generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
                generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

                goto try_except_handler_2;
                // try continue handler code:
                try_continue_handler_4:;
                // Restore previous exception.
                SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
                goto loop_start_1;
                // End of try:
                // End of try:
                try_end_1:;
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_10;
                    PyObject *tmp_kw_name_2;
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );

                    if (unlikely( tmp_mvar_value_10 == NULL ))
                    {
                        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );
                    }

                    if ( tmp_mvar_value_10 == NULL )
                    {

                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CaseSensitiveConfigParser" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 154;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_called_name_3 = tmp_mvar_value_10;
                    tmp_kw_name_2 = PyDict_Copy( const_dict_76e556d6c52e2dbcc094dfc5f551a7fe );
                    generator->m_frame->m_frame.f_lineno = 154;
                    tmp_assign_source_13 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
                    Py_DECREF( tmp_kw_name_2 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 154;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = generator_heap->var_cp;
                        generator_heap->var_cp = tmp_assign_source_13;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_14;
                    PyObject *tmp_called_instance_12;
                    PyObject *tmp_args_element_name_11;
                    CHECK_OBJECT( generator_heap->var_z );
                    tmp_called_instance_12 = generator_heap->var_z;
                    CHECK_OBJECT( generator_heap->var_info );
                    tmp_args_element_name_11 = generator_heap->var_info;
                    generator->m_frame->m_frame.f_lineno = 155;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_11 };
                        tmp_assign_source_14 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_12, const_str_plain_open, call_args );
                    }

                    if ( tmp_assign_source_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 155;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = generator_heap->tmp_with_1__source;
                        generator_heap->tmp_with_1__source = tmp_assign_source_14;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_15;
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_source_name_5;
                    CHECK_OBJECT( generator_heap->tmp_with_1__source );
                    tmp_source_name_5 = generator_heap->tmp_with_1__source;
                    tmp_called_name_4 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___enter__ );
                    if ( tmp_called_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 155;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_5;
                    }
                    generator->m_frame->m_frame.f_lineno = 155;
                    tmp_assign_source_15 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
                    Py_DECREF( tmp_called_name_4 );
                    if ( tmp_assign_source_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 155;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = generator_heap->tmp_with_1__enter;
                        generator_heap->tmp_with_1__enter = tmp_assign_source_15;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_16;
                    PyObject *tmp_source_name_6;
                    CHECK_OBJECT( generator_heap->tmp_with_1__source );
                    tmp_source_name_6 = generator_heap->tmp_with_1__source;
                    tmp_assign_source_16 = LOOKUP_SPECIAL( tmp_source_name_6, const_str_plain___exit__ );
                    if ( tmp_assign_source_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 155;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = generator_heap->tmp_with_1__exit;
                        generator_heap->tmp_with_1__exit = tmp_assign_source_16;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_17;
                    tmp_assign_source_17 = Py_True;
                    {
                        PyObject *old = generator_heap->tmp_with_1__indicator;
                        generator_heap->tmp_with_1__indicator = tmp_assign_source_17;
                        Py_INCREF( generator_heap->tmp_with_1__indicator );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_18;
                    CHECK_OBJECT( generator_heap->tmp_with_1__enter );
                    tmp_assign_source_18 = generator_heap->tmp_with_1__enter;
                    {
                        PyObject *old = generator_heap->var_f;
                        generator_heap->var_f = tmp_assign_source_18;
                        Py_INCREF( generator_heap->var_f );
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                // Tried code:
                {
                    PyObject *tmp_assign_source_19;
                    PyObject *tmp_called_instance_13;
                    PyObject *tmp_mvar_value_11;
                    PyObject *tmp_args_element_name_12;
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_io );

                    if (unlikely( tmp_mvar_value_11 == NULL ))
                    {
                        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_io );
                    }

                    if ( tmp_mvar_value_11 == NULL )
                    {

                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "io" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 156;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_7;
                    }

                    tmp_called_instance_13 = tmp_mvar_value_11;
                    CHECK_OBJECT( generator_heap->var_f );
                    tmp_args_element_name_12 = generator_heap->var_f;
                    generator->m_frame->m_frame.f_lineno = 156;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_12 };
                        tmp_assign_source_19 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_13, const_str_plain_TextIOWrapper, call_args );
                    }

                    if ( tmp_assign_source_19 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 156;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_7;
                    }
                    {
                        PyObject *old = generator_heap->var_fu;
                        generator_heap->var_fu = tmp_assign_source_19;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_source_name_7;
                    PyObject *tmp_call_result_7;
                    PyObject *tmp_args_name_1;
                    PyObject *tmp_tuple_element_2;
                    PyObject *tmp_kw_name_3;
                    PyObject *tmp_dict_key_1;
                    PyObject *tmp_dict_value_1;
                    PyObject *tmp_called_instance_14;
                    PyObject *tmp_mvar_value_12;
                    PyObject *tmp_args_element_name_13;
                    PyObject *tmp_args_element_name_14;
                    PyObject *tmp_args_element_name_15;
                    CHECK_OBJECT( generator_heap->var_cp );
                    tmp_source_name_7 = generator_heap->var_cp;
                    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_read_file );
                    if ( tmp_called_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 157;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_7;
                    }
                    CHECK_OBJECT( generator_heap->var_fu );
                    tmp_tuple_element_2 = generator_heap->var_fu;
                    tmp_args_name_1 = PyTuple_New( 1 );
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
                    tmp_dict_key_1 = const_str_plain_source;
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

                    if (unlikely( tmp_mvar_value_12 == NULL ))
                    {
                        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
                    }

                    if ( tmp_mvar_value_12 == NULL )
                    {
                        Py_DECREF( tmp_called_name_5 );
                        Py_DECREF( tmp_args_name_1 );
                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 158;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_7;
                    }

                    tmp_called_instance_14 = tmp_mvar_value_12;
                    CHECK_OBJECT( generator_heap->var_folder );
                    tmp_args_element_name_13 = generator_heap->var_folder;
                    tmp_args_element_name_14 = const_str_digest_678f3080a7e13def35de5d6945bcf27c;
                    tmp_args_element_name_15 = const_str_digest_7ae94de8e546ee88855d387e29ed702b;
                    generator->m_frame->m_frame.f_lineno = 158;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15 };
                        tmp_dict_value_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_14, const_str_plain_join, call_args );
                    }

                    if ( tmp_dict_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        Py_DECREF( tmp_called_name_5 );
                        Py_DECREF( tmp_args_name_1 );

                        generator_heap->exception_lineno = 158;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_7;
                    }
                    tmp_kw_name_3 = _PyDict_NewPresized( 1 );
                    generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_1, tmp_dict_value_1 );
                    Py_DECREF( tmp_dict_value_1 );
                    assert( !(generator_heap->tmp_res != 0) );
                    generator->m_frame->m_frame.f_lineno = 157;
                    tmp_call_result_7 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_3 );
                    Py_DECREF( tmp_called_name_5 );
                    Py_DECREF( tmp_args_name_1 );
                    Py_DECREF( tmp_kw_name_3 );
                    if ( tmp_call_result_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 157;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_7;
                    }
                    Py_DECREF( tmp_call_result_7 );
                }
                goto try_end_2;
                // Exception handler code:
                try_except_handler_7:;
                generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
                generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
                generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
                generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
                generator_heap->exception_type = NULL;
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
                generator_heap->exception_lineno = 0;

                // Preserve existing published exception.
                generator_heap->exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
                Py_XINCREF( generator_heap->exception_preserved_type_2 );
                generator_heap->exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
                Py_XINCREF( generator_heap->exception_preserved_value_2 );
                generator_heap->exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
                Py_XINCREF( generator_heap->exception_preserved_tb_2 );

                if ( generator_heap->exception_keeper_tb_3 == NULL )
                {
                    generator_heap->exception_keeper_tb_3 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_3 );
                }
                else if ( generator_heap->exception_keeper_lineno_3 != 0 )
                {
                    generator_heap->exception_keeper_tb_3 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_3, generator->m_frame, generator_heap->exception_keeper_lineno_3 );
                }

                NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_3, &generator_heap->exception_keeper_value_3, &generator_heap->exception_keeper_tb_3 );
                PyException_SetTraceback( generator_heap->exception_keeper_value_3, (PyObject *)generator_heap->exception_keeper_tb_3 );
                PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_3, &generator_heap->exception_keeper_value_3, &generator_heap->exception_keeper_tb_3 );
                // Tried code:
                {
                    nuitka_bool tmp_condition_result_9;
                    PyObject *tmp_compexpr_left_6;
                    PyObject *tmp_compexpr_right_6;
                    tmp_compexpr_left_6 = EXC_TYPE(PyThreadState_GET());
                    tmp_compexpr_right_6 = PyExc_BaseException;
                    generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 155;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_8;
                    }
                    tmp_condition_result_9 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_9;
                    }
                    else
                    {
                        goto branch_no_9;
                    }
                    branch_yes_9:;
                    {
                        PyObject *tmp_assign_source_20;
                        tmp_assign_source_20 = Py_False;
                        {
                            PyObject *old = generator_heap->tmp_with_1__indicator;
                            assert( old != NULL );
                            generator_heap->tmp_with_1__indicator = tmp_assign_source_20;
                            Py_INCREF( generator_heap->tmp_with_1__indicator );
                            Py_DECREF( old );
                        }

                    }
                    {
                        nuitka_bool tmp_condition_result_10;
                        PyObject *tmp_operand_name_1;
                        PyObject *tmp_called_name_6;
                        PyObject *tmp_args_element_name_16;
                        PyObject *tmp_args_element_name_17;
                        PyObject *tmp_args_element_name_18;
                        CHECK_OBJECT( generator_heap->tmp_with_1__exit );
                        tmp_called_name_6 = generator_heap->tmp_with_1__exit;
                        tmp_args_element_name_16 = EXC_TYPE(PyThreadState_GET());
                        tmp_args_element_name_17 = EXC_VALUE(PyThreadState_GET());
                        tmp_args_element_name_18 = EXC_TRACEBACK(PyThreadState_GET());
                        generator->m_frame->m_frame.f_lineno = 158;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_16, tmp_args_element_name_17, tmp_args_element_name_18 };
                            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, call_args );
                        }

                        if ( tmp_operand_name_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 158;
                            generator_heap->type_description_1 = "ccooooooooooooo";
                            goto try_except_handler_8;
                        }
                        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                        Py_DECREF( tmp_operand_name_1 );
                        if ( generator_heap->tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 158;
                            generator_heap->type_description_1 = "ccooooooooooooo";
                            goto try_except_handler_8;
                        }
                        tmp_condition_result_10 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_10;
                        }
                        else
                        {
                            goto branch_no_10;
                        }
                        branch_yes_10:;
                        generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        if (unlikely( generator_heap->tmp_result == false ))
                        {
                            generator_heap->exception_lineno = 158;
                        }

                        if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_8;
                        branch_no_10:;
                    }
                    goto branch_end_9;
                    branch_no_9:;
                    generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    if (unlikely( generator_heap->tmp_result == false ))
                    {
                        generator_heap->exception_lineno = 155;
                    }

                    if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_8;
                    branch_end_9:;
                }
                goto try_end_3;
                // Exception handler code:
                try_except_handler_8:;
                generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
                generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
                generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
                generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
                generator_heap->exception_type = NULL;
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
                generator_heap->exception_lineno = 0;

                // Restore previous exception.
                SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_2, generator_heap->exception_preserved_value_2, generator_heap->exception_preserved_tb_2 );
                // Re-raise.
                generator_heap->exception_type = generator_heap->exception_keeper_type_4;
                generator_heap->exception_value = generator_heap->exception_keeper_value_4;
                generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
                generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

                goto try_except_handler_6;
                // End of try:
                try_end_3:;
                // Restore previous exception.
                SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_2, generator_heap->exception_preserved_value_2, generator_heap->exception_preserved_tb_2 );
                goto try_end_2;
                // exception handler codes exits in all cases
                NUITKA_CANNOT_GET_HERE( entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros );
                return NULL;
                // End of try:
                try_end_2:;
                goto try_end_4;
                // Exception handler code:
                try_except_handler_6:;
                generator_heap->exception_keeper_type_5 = generator_heap->exception_type;
                generator_heap->exception_keeper_value_5 = generator_heap->exception_value;
                generator_heap->exception_keeper_tb_5 = generator_heap->exception_tb;
                generator_heap->exception_keeper_lineno_5 = generator_heap->exception_lineno;
                generator_heap->exception_type = NULL;
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
                generator_heap->exception_lineno = 0;

                {
                    nuitka_bool tmp_condition_result_11;
                    nuitka_bool tmp_compexpr_left_7;
                    nuitka_bool tmp_compexpr_right_7;
                    int tmp_truth_name_5;
                    CHECK_OBJECT( generator_heap->tmp_with_1__indicator );
                    tmp_truth_name_5 = CHECK_IF_TRUE( generator_heap->tmp_with_1__indicator );
                    if ( tmp_truth_name_5 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                        Py_DECREF( generator_heap->exception_keeper_type_5 );
                        Py_XDECREF( generator_heap->exception_keeper_value_5 );
                        Py_XDECREF( generator_heap->exception_keeper_tb_5 );

                        generator_heap->exception_lineno = 155;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_5;
                    }
                    tmp_compexpr_left_7 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    tmp_compexpr_right_7 = NUITKA_BOOL_TRUE;
                    tmp_condition_result_11 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_11;
                    }
                    else
                    {
                        goto branch_no_11;
                    }
                    branch_yes_11:;
                    {
                        PyObject *tmp_called_name_7;
                        PyObject *tmp_call_result_8;
                        CHECK_OBJECT( generator_heap->tmp_with_1__exit );
                        tmp_called_name_7 = generator_heap->tmp_with_1__exit;
                        generator->m_frame->m_frame.f_lineno = 158;
                        tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                        if ( tmp_call_result_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                            Py_DECREF( generator_heap->exception_keeper_type_5 );
                            Py_XDECREF( generator_heap->exception_keeper_value_5 );
                            Py_XDECREF( generator_heap->exception_keeper_tb_5 );

                            generator_heap->exception_lineno = 158;
                            generator_heap->type_description_1 = "ccooooooooooooo";
                            goto try_except_handler_5;
                        }
                        Py_DECREF( tmp_call_result_8 );
                    }
                    branch_no_11:;
                }
                // Re-raise.
                generator_heap->exception_type = generator_heap->exception_keeper_type_5;
                generator_heap->exception_value = generator_heap->exception_keeper_value_5;
                generator_heap->exception_tb = generator_heap->exception_keeper_tb_5;
                generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_5;

                goto try_except_handler_5;
                // End of try:
                try_end_4:;
                {
                    nuitka_bool tmp_condition_result_12;
                    nuitka_bool tmp_compexpr_left_8;
                    nuitka_bool tmp_compexpr_right_8;
                    int tmp_truth_name_6;
                    CHECK_OBJECT( generator_heap->tmp_with_1__indicator );
                    tmp_truth_name_6 = CHECK_IF_TRUE( generator_heap->tmp_with_1__indicator );
                    if ( tmp_truth_name_6 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 155;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_5;
                    }
                    tmp_compexpr_left_8 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    tmp_compexpr_right_8 = NUITKA_BOOL_TRUE;
                    tmp_condition_result_12 = ( tmp_compexpr_left_8 == tmp_compexpr_right_8 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_12;
                    }
                    else
                    {
                        goto branch_no_12;
                    }
                    branch_yes_12:;
                    {
                        PyObject *tmp_called_name_8;
                        PyObject *tmp_call_result_9;
                        CHECK_OBJECT( generator_heap->tmp_with_1__exit );
                        tmp_called_name_8 = generator_heap->tmp_with_1__exit;
                        generator->m_frame->m_frame.f_lineno = 158;
                        tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                        if ( tmp_call_result_9 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 158;
                            generator_heap->type_description_1 = "ccooooooooooooo";
                            goto try_except_handler_5;
                        }
                        Py_DECREF( tmp_call_result_9 );
                    }
                    branch_no_12:;
                }
                goto try_end_5;
                // Exception handler code:
                try_except_handler_5:;
                generator_heap->exception_keeper_type_6 = generator_heap->exception_type;
                generator_heap->exception_keeper_value_6 = generator_heap->exception_value;
                generator_heap->exception_keeper_tb_6 = generator_heap->exception_tb;
                generator_heap->exception_keeper_lineno_6 = generator_heap->exception_lineno;
                generator_heap->exception_type = NULL;
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
                generator_heap->exception_lineno = 0;

                Py_XDECREF( generator_heap->tmp_with_1__source );
                generator_heap->tmp_with_1__source = NULL;

                Py_XDECREF( generator_heap->tmp_with_1__enter );
                generator_heap->tmp_with_1__enter = NULL;

                Py_XDECREF( generator_heap->tmp_with_1__exit );
                generator_heap->tmp_with_1__exit = NULL;

                Py_XDECREF( generator_heap->tmp_with_1__indicator );
                generator_heap->tmp_with_1__indicator = NULL;

                // Re-raise.
                generator_heap->exception_type = generator_heap->exception_keeper_type_6;
                generator_heap->exception_value = generator_heap->exception_keeper_value_6;
                generator_heap->exception_tb = generator_heap->exception_keeper_tb_6;
                generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_6;

                goto try_except_handler_2;
                // End of try:
                try_end_5:;
                CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_1__source );
                Py_DECREF( generator_heap->tmp_with_1__source );
                generator_heap->tmp_with_1__source = NULL;

                CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_1__enter );
                Py_DECREF( generator_heap->tmp_with_1__enter );
                generator_heap->tmp_with_1__enter = NULL;

                CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_1__exit );
                Py_DECREF( generator_heap->tmp_with_1__exit );
                generator_heap->tmp_with_1__exit = NULL;

                Py_XDECREF( generator_heap->tmp_with_1__indicator );
                generator_heap->tmp_with_1__indicator = NULL;

                {
                    PyObject *tmp_expression_name_2;
                    PyObject *tmp_tuple_element_3;
                    NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
                    CHECK_OBJECT( generator_heap->var_cp );
                    tmp_tuple_element_3 = generator_heap->var_cp;
                    tmp_expression_name_2 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_expression_name_2, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( generator_heap->var_distro );
                    tmp_tuple_element_3 = generator_heap->var_distro;
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_expression_name_2, 1, tmp_tuple_element_3 );
                    Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_called_instance_5, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_call_result_3, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), &tmp_condition_result_7, sizeof(nuitka_bool), &tmp_called_instance_9, sizeof(PyObject *), &tmp_mvar_value_8, sizeof(PyObject *), &tmp_call_result_6, sizeof(PyObject *), &tmp_args_element_name_9, sizeof(PyObject *), &tmp_truth_name_4, sizeof(int), &tmp_tuple_element_3, sizeof(PyObject *), NULL );
                    generator->m_yield_return_index = 2;
                    return tmp_expression_name_2;
                    yield_return_2:
                    Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_called_instance_5, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_call_result_3, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), &tmp_condition_result_7, sizeof(nuitka_bool), &tmp_called_instance_9, sizeof(PyObject *), &tmp_mvar_value_8, sizeof(PyObject *), &tmp_call_result_6, sizeof(PyObject *), &tmp_args_element_name_9, sizeof(PyObject *), &tmp_truth_name_4, sizeof(int), &tmp_tuple_element_3, sizeof(PyObject *), NULL );
                    if ( yield_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 159;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_yield_result_2 = yield_return_value;
                }
                branch_no_7:;
            }
            branch_end_5:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_called_instance_15;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_call_result_10;
            PyObject *tmp_args_element_name_19;
            int tmp_truth_name_7;
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_zipfile );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zipfile );
            }

            if ( tmp_mvar_value_13 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zipfile" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 162;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }

            tmp_called_instance_15 = tmp_mvar_value_13;
            CHECK_OBJECT( generator_heap->var_folder );
            tmp_args_element_name_19 = generator_heap->var_folder;
            generator->m_frame->m_frame.f_lineno = 162;
            {
                PyObject *call_args[] = { tmp_args_element_name_19 };
                tmp_call_result_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_15, const_str_plain_is_zipfile, call_args );
            }

            if ( tmp_call_result_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 162;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_truth_name_7 = CHECK_IF_TRUE( tmp_call_result_10 );
            if ( tmp_truth_name_7 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_call_result_10 );

                generator_heap->exception_lineno = 162;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_13 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_10 );
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            // Tried code:
            {
                PyObject *tmp_assign_source_21;
                PyObject *tmp_called_instance_16;
                PyObject *tmp_mvar_value_14;
                PyObject *tmp_args_element_name_20;
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_zipfile );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zipfile );
                }

                if ( tmp_mvar_value_14 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zipfile" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_9;
                }

                tmp_called_instance_16 = tmp_mvar_value_14;
                CHECK_OBJECT( generator_heap->var_folder );
                tmp_args_element_name_20 = generator_heap->var_folder;
                generator->m_frame->m_frame.f_lineno = 163;
                {
                    PyObject *call_args[] = { tmp_args_element_name_20 };
                    tmp_assign_source_21 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_16, const_str_plain_ZipFile, call_args );
                }

                if ( tmp_assign_source_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_9;
                }
                {
                    PyObject *old = generator_heap->tmp_with_3__source;
                    generator_heap->tmp_with_3__source = tmp_assign_source_21;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_22;
                PyObject *tmp_called_name_9;
                PyObject *tmp_source_name_8;
                CHECK_OBJECT( generator_heap->tmp_with_3__source );
                tmp_source_name_8 = generator_heap->tmp_with_3__source;
                tmp_called_name_9 = LOOKUP_SPECIAL( tmp_source_name_8, const_str_plain___enter__ );
                if ( tmp_called_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_9;
                }
                generator->m_frame->m_frame.f_lineno = 163;
                tmp_assign_source_22 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
                Py_DECREF( tmp_called_name_9 );
                if ( tmp_assign_source_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_9;
                }
                {
                    PyObject *old = generator_heap->tmp_with_3__enter;
                    generator_heap->tmp_with_3__enter = tmp_assign_source_22;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_23;
                PyObject *tmp_source_name_9;
                CHECK_OBJECT( generator_heap->tmp_with_3__source );
                tmp_source_name_9 = generator_heap->tmp_with_3__source;
                tmp_assign_source_23 = LOOKUP_SPECIAL( tmp_source_name_9, const_str_plain___exit__ );
                if ( tmp_assign_source_23 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_9;
                }
                {
                    PyObject *old = generator_heap->tmp_with_3__exit;
                    generator_heap->tmp_with_3__exit = tmp_assign_source_23;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_24;
                tmp_assign_source_24 = Py_True;
                {
                    PyObject *old = generator_heap->tmp_with_3__indicator;
                    generator_heap->tmp_with_3__indicator = tmp_assign_source_24;
                    Py_INCREF( generator_heap->tmp_with_3__indicator );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_25;
                CHECK_OBJECT( generator_heap->tmp_with_3__enter );
                tmp_assign_source_25 = generator_heap->tmp_with_3__enter;
                {
                    PyObject *old = generator_heap->var_zf;
                    generator_heap->var_zf = tmp_assign_source_25;
                    Py_INCREF( generator_heap->var_zf );
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_assign_source_26;
                PyObject *tmp_iter_arg_2;
                PyObject *tmp_called_instance_17;
                CHECK_OBJECT( generator_heap->var_zf );
                tmp_called_instance_17 = generator_heap->var_zf;
                generator->m_frame->m_frame.f_lineno = 164;
                tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_17, const_str_plain_infolist );
                if ( tmp_iter_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 164;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_11;
                }
                tmp_assign_source_26 = MAKE_ITERATOR( tmp_iter_arg_2 );
                Py_DECREF( tmp_iter_arg_2 );
                if ( tmp_assign_source_26 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 164;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_11;
                }
                {
                    PyObject *old = generator_heap->tmp_for_loop_2__for_iterator;
                    generator_heap->tmp_for_loop_2__for_iterator = tmp_assign_source_26;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_27;
                CHECK_OBJECT( generator_heap->tmp_for_loop_2__for_iterator );
                tmp_next_source_2 = generator_heap->tmp_for_loop_2__for_iterator;
                tmp_assign_source_27 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_27 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        generator_heap->exception_lineno = 164;
                        goto try_except_handler_12;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_for_loop_2__iter_value;
                    generator_heap->tmp_for_loop_2__iter_value = tmp_assign_source_27;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_28;
                CHECK_OBJECT( generator_heap->tmp_for_loop_2__iter_value );
                tmp_assign_source_28 = generator_heap->tmp_for_loop_2__iter_value;
                {
                    PyObject *old = generator_heap->var_info;
                    generator_heap->var_info = tmp_assign_source_28;
                    Py_INCREF( generator_heap->var_info );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_29;
                PyObject *tmp_called_name_10;
                PyObject *tmp_source_name_10;
                PyObject *tmp_mvar_value_15;
                PyObject *tmp_args_element_name_21;
                PyObject *tmp_source_name_11;
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_file_in_zip_pattern );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_file_in_zip_pattern );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "file_in_zip_pattern" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 165;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }

                tmp_source_name_10 = tmp_mvar_value_15;
                tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_match );
                if ( tmp_called_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 165;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                CHECK_OBJECT( generator_heap->var_info );
                tmp_source_name_11 = generator_heap->var_info;
                tmp_args_element_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_filename );
                if ( tmp_args_element_name_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_10 );

                    generator_heap->exception_lineno = 165;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                generator->m_frame->m_frame.f_lineno = 165;
                {
                    PyObject *call_args[] = { tmp_args_element_name_21 };
                    tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
                }

                Py_DECREF( tmp_called_name_10 );
                Py_DECREF( tmp_args_element_name_21 );
                if ( tmp_assign_source_29 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 165;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                {
                    PyObject *old = generator_heap->var_m;
                    generator_heap->var_m = tmp_assign_source_29;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_14;
                PyObject *tmp_operand_name_2;
                CHECK_OBJECT( generator_heap->var_m );
                tmp_operand_name_2 = generator_heap->var_m;
                generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 166;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                tmp_condition_result_14 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_14;
                }
                else
                {
                    goto branch_no_14;
                }
                branch_yes_14:;
                goto loop_start_2;
                branch_no_14:;
            }
            {
                PyObject *tmp_assign_source_30;
                PyObject *tmp_called_instance_18;
                CHECK_OBJECT( generator_heap->var_m );
                tmp_called_instance_18 = generator_heap->var_m;
                generator->m_frame->m_frame.f_lineno = 169;
                tmp_assign_source_30 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_18, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_str_plain_dist_version_tuple, 0 ) );

                if ( tmp_assign_source_30 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 169;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                {
                    PyObject *old = generator_heap->var_distro_name_version;
                    generator_heap->var_distro_name_version = tmp_assign_source_30;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_15;
                PyObject *tmp_compexpr_left_9;
                PyObject *tmp_compexpr_right_9;
                tmp_compexpr_left_9 = const_str_chr_45;
                CHECK_OBJECT( generator_heap->var_distro_name_version );
                tmp_compexpr_right_9 = generator_heap->var_distro_name_version;
                generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_9, tmp_compexpr_left_9 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 170;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                tmp_condition_result_15 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_15;
                }
                else
                {
                    goto branch_no_15;
                }
                branch_yes_15:;
                {
                    PyObject *tmp_assign_source_31;
                    PyObject *tmp_dircall_arg1_2;
                    PyObject *tmp_mvar_value_16;
                    PyObject *tmp_dircall_arg2_2;
                    PyObject *tmp_called_instance_19;
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_Distribution );

                    if (unlikely( tmp_mvar_value_16 == NULL ))
                    {
                        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Distribution );
                    }

                    if ( tmp_mvar_value_16 == NULL )
                    {

                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Distribution" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 171;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }

                    tmp_dircall_arg1_2 = tmp_mvar_value_16;
                    CHECK_OBJECT( generator_heap->var_distro_name_version );
                    tmp_called_instance_19 = generator_heap->var_distro_name_version;
                    generator->m_frame->m_frame.f_lineno = 171;
                    tmp_dircall_arg2_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_19, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_45_int_pos_1_tuple, 0 ) );

                    if ( tmp_dircall_arg2_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 171;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    Py_INCREF( tmp_dircall_arg1_2 );

                    {
                        PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2};
                        tmp_assign_source_31 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
                    }
                    if ( tmp_assign_source_31 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 171;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    {
                        PyObject *old = generator_heap->var_distro;
                        generator_heap->var_distro = tmp_assign_source_31;
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_16;
                    int tmp_and_left_truth_2;
                    nuitka_bool tmp_and_left_value_2;
                    nuitka_bool tmp_and_right_value_2;
                    PyObject *tmp_compexpr_left_10;
                    PyObject *tmp_compexpr_right_10;
                    PyObject *tmp_compexpr_left_11;
                    PyObject *tmp_compexpr_right_11;
                    PyObject *tmp_source_name_12;
                    if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                    {

                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "repeated_distro" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 173;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }

                    tmp_compexpr_left_10 = PyCell_GET( generator->m_closure[1] );
                    tmp_compexpr_right_10 = const_str_plain_first;
                    generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 173;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    tmp_and_left_value_2 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
                    if ( tmp_and_left_truth_2 == 1 )
                    {
                        goto and_right_2;
                    }
                    else
                    {
                        goto and_left_2;
                    }
                    and_right_2:;
                    CHECK_OBJECT( generator_heap->var_distro );
                    tmp_source_name_12 = generator_heap->var_distro;
                    tmp_compexpr_left_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_name );
                    if ( tmp_compexpr_left_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 174;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    CHECK_OBJECT( generator_heap->var_distro_names_seen );
                    tmp_compexpr_right_11 = generator_heap->var_distro_names_seen;
                    generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_11, tmp_compexpr_left_11 );
                    Py_DECREF( tmp_compexpr_left_11 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 174;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    tmp_and_right_value_2 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    tmp_condition_result_16 = tmp_and_right_value_2;
                    goto and_end_2;
                    and_left_2:;
                    tmp_condition_result_16 = tmp_and_left_value_2;
                    and_end_2:;
                    if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_16;
                    }
                    else
                    {
                        goto branch_no_16;
                    }
                    branch_yes_16:;
                    goto loop_start_2;
                    branch_no_16:;
                }
                {
                    PyObject *tmp_called_name_11;
                    PyObject *tmp_source_name_13;
                    PyObject *tmp_call_result_11;
                    PyObject *tmp_args_element_name_22;
                    PyObject *tmp_source_name_14;
                    CHECK_OBJECT( generator_heap->var_distro_names_seen );
                    tmp_source_name_13 = generator_heap->var_distro_names_seen;
                    tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_add );
                    if ( tmp_called_name_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 176;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    CHECK_OBJECT( generator_heap->var_distro );
                    tmp_source_name_14 = generator_heap->var_distro;
                    tmp_args_element_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_name );
                    if ( tmp_args_element_name_22 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        Py_DECREF( tmp_called_name_11 );

                        generator_heap->exception_lineno = 176;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    generator->m_frame->m_frame.f_lineno = 176;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_22 };
                        tmp_call_result_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
                    }

                    Py_DECREF( tmp_called_name_11 );
                    Py_DECREF( tmp_args_element_name_22 );
                    if ( tmp_call_result_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 176;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_12;
                    }
                    Py_DECREF( tmp_call_result_11 );
                }
                goto branch_end_15;
                branch_no_15:;
                {
                    PyObject *tmp_assign_source_32;
                    tmp_assign_source_32 = Py_None;
                    {
                        PyObject *old = generator_heap->var_distro;
                        generator_heap->var_distro = tmp_assign_source_32;
                        Py_INCREF( generator_heap->var_distro );
                        Py_XDECREF( old );
                    }

                }
                branch_end_15:;
            }
            {
                PyObject *tmp_assign_source_33;
                PyObject *tmp_called_name_12;
                PyObject *tmp_mvar_value_17;
                PyObject *tmp_kw_name_4;
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );
                }

                if ( tmp_mvar_value_17 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CaseSensitiveConfigParser" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 180;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }

                tmp_called_name_12 = tmp_mvar_value_17;
                tmp_kw_name_4 = PyDict_Copy( const_dict_76e556d6c52e2dbcc094dfc5f551a7fe );
                generator->m_frame->m_frame.f_lineno = 180;
                tmp_assign_source_33 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_12, tmp_kw_name_4 );
                Py_DECREF( tmp_kw_name_4 );
                if ( tmp_assign_source_33 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 180;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                {
                    PyObject *old = generator_heap->var_cp;
                    generator_heap->var_cp = tmp_assign_source_33;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_34;
                PyObject *tmp_called_instance_20;
                PyObject *tmp_args_element_name_23;
                CHECK_OBJECT( generator_heap->var_zf );
                tmp_called_instance_20 = generator_heap->var_zf;
                CHECK_OBJECT( generator_heap->var_info );
                tmp_args_element_name_23 = generator_heap->var_info;
                generator->m_frame->m_frame.f_lineno = 181;
                {
                    PyObject *call_args[] = { tmp_args_element_name_23 };
                    tmp_assign_source_34 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_20, const_str_plain_open, call_args );
                }

                if ( tmp_assign_source_34 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 181;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_13;
                }
                {
                    PyObject *old = generator_heap->tmp_with_2__source;
                    generator_heap->tmp_with_2__source = tmp_assign_source_34;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_35;
                PyObject *tmp_called_name_13;
                PyObject *tmp_source_name_15;
                CHECK_OBJECT( generator_heap->tmp_with_2__source );
                tmp_source_name_15 = generator_heap->tmp_with_2__source;
                tmp_called_name_13 = LOOKUP_SPECIAL( tmp_source_name_15, const_str_plain___enter__ );
                if ( tmp_called_name_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 181;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_13;
                }
                generator->m_frame->m_frame.f_lineno = 181;
                tmp_assign_source_35 = CALL_FUNCTION_NO_ARGS( tmp_called_name_13 );
                Py_DECREF( tmp_called_name_13 );
                if ( tmp_assign_source_35 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 181;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_13;
                }
                {
                    PyObject *old = generator_heap->tmp_with_2__enter;
                    generator_heap->tmp_with_2__enter = tmp_assign_source_35;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_36;
                PyObject *tmp_source_name_16;
                CHECK_OBJECT( generator_heap->tmp_with_2__source );
                tmp_source_name_16 = generator_heap->tmp_with_2__source;
                tmp_assign_source_36 = LOOKUP_SPECIAL( tmp_source_name_16, const_str_plain___exit__ );
                if ( tmp_assign_source_36 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 181;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_13;
                }
                {
                    PyObject *old = generator_heap->tmp_with_2__exit;
                    generator_heap->tmp_with_2__exit = tmp_assign_source_36;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_37;
                tmp_assign_source_37 = Py_True;
                {
                    PyObject *old = generator_heap->tmp_with_2__indicator;
                    generator_heap->tmp_with_2__indicator = tmp_assign_source_37;
                    Py_INCREF( generator_heap->tmp_with_2__indicator );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_38;
                CHECK_OBJECT( generator_heap->tmp_with_2__enter );
                tmp_assign_source_38 = generator_heap->tmp_with_2__enter;
                {
                    PyObject *old = generator_heap->var_f;
                    generator_heap->var_f = tmp_assign_source_38;
                    Py_INCREF( generator_heap->var_f );
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_assign_source_39;
                PyObject *tmp_called_instance_21;
                PyObject *tmp_mvar_value_18;
                PyObject *tmp_args_element_name_24;
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_io );

                if (unlikely( tmp_mvar_value_18 == NULL ))
                {
                    tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_io );
                }

                if ( tmp_mvar_value_18 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "io" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 182;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }

                tmp_called_instance_21 = tmp_mvar_value_18;
                CHECK_OBJECT( generator_heap->var_f );
                tmp_args_element_name_24 = generator_heap->var_f;
                generator->m_frame->m_frame.f_lineno = 182;
                {
                    PyObject *call_args[] = { tmp_args_element_name_24 };
                    tmp_assign_source_39 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_21, const_str_plain_TextIOWrapper, call_args );
                }

                if ( tmp_assign_source_39 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 182;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }
                {
                    PyObject *old = generator_heap->var_fu;
                    generator_heap->var_fu = tmp_assign_source_39;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_14;
                PyObject *tmp_source_name_17;
                PyObject *tmp_call_result_12;
                PyObject *tmp_args_name_2;
                PyObject *tmp_tuple_element_4;
                PyObject *tmp_kw_name_5;
                PyObject *tmp_dict_key_2;
                PyObject *tmp_dict_value_2;
                PyObject *tmp_called_name_15;
                PyObject *tmp_source_name_18;
                PyObject *tmp_mvar_value_19;
                PyObject *tmp_args_element_name_25;
                PyObject *tmp_args_element_name_26;
                PyObject *tmp_source_name_19;
                CHECK_OBJECT( generator_heap->var_cp );
                tmp_source_name_17 = generator_heap->var_cp;
                tmp_called_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_read_file );
                if ( tmp_called_name_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 183;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }
                CHECK_OBJECT( generator_heap->var_fu );
                tmp_tuple_element_4 = generator_heap->var_fu;
                tmp_args_name_2 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
                tmp_dict_key_2 = const_str_plain_source;
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
                }

                if ( tmp_mvar_value_19 == NULL )
                {
                    Py_DECREF( tmp_called_name_14 );
                    Py_DECREF( tmp_args_name_2 );
                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 183;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }

                tmp_source_name_18 = tmp_mvar_value_19;
                tmp_called_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_join );
                if ( tmp_called_name_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_14 );
                    Py_DECREF( tmp_args_name_2 );

                    generator_heap->exception_lineno = 183;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }
                CHECK_OBJECT( generator_heap->var_folder );
                tmp_args_element_name_25 = generator_heap->var_folder;
                CHECK_OBJECT( generator_heap->var_info );
                tmp_source_name_19 = generator_heap->var_info;
                tmp_args_element_name_26 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_filename );
                if ( tmp_args_element_name_26 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_14 );
                    Py_DECREF( tmp_args_name_2 );
                    Py_DECREF( tmp_called_name_15 );

                    generator_heap->exception_lineno = 183;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }
                generator->m_frame->m_frame.f_lineno = 183;
                {
                    PyObject *call_args[] = { tmp_args_element_name_25, tmp_args_element_name_26 };
                    tmp_dict_value_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_15, call_args );
                }

                Py_DECREF( tmp_called_name_15 );
                Py_DECREF( tmp_args_element_name_26 );
                if ( tmp_dict_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_14 );
                    Py_DECREF( tmp_args_name_2 );

                    generator_heap->exception_lineno = 183;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }
                tmp_kw_name_5 = _PyDict_NewPresized( 1 );
                generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_2, tmp_dict_value_2 );
                Py_DECREF( tmp_dict_value_2 );
                assert( !(generator_heap->tmp_res != 0) );
                generator->m_frame->m_frame.f_lineno = 183;
                tmp_call_result_12 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_2, tmp_kw_name_5 );
                Py_DECREF( tmp_called_name_14 );
                Py_DECREF( tmp_args_name_2 );
                Py_DECREF( tmp_kw_name_5 );
                if ( tmp_call_result_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 183;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_15;
                }
                Py_DECREF( tmp_call_result_12 );
            }
            goto try_end_6;
            // Exception handler code:
            try_except_handler_15:;
            generator_heap->exception_keeper_type_7 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_7 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_7 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_7 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            // Preserve existing published exception.
            generator_heap->exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( generator_heap->exception_preserved_type_3 );
            generator_heap->exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( generator_heap->exception_preserved_value_3 );
            generator_heap->exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( generator_heap->exception_preserved_tb_3 );

            if ( generator_heap->exception_keeper_tb_7 == NULL )
            {
                generator_heap->exception_keeper_tb_7 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_7 );
            }
            else if ( generator_heap->exception_keeper_lineno_7 != 0 )
            {
                generator_heap->exception_keeper_tb_7 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_7, generator->m_frame, generator_heap->exception_keeper_lineno_7 );
            }

            NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_7, &generator_heap->exception_keeper_value_7, &generator_heap->exception_keeper_tb_7 );
            PyException_SetTraceback( generator_heap->exception_keeper_value_7, (PyObject *)generator_heap->exception_keeper_tb_7 );
            PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_7, &generator_heap->exception_keeper_value_7, &generator_heap->exception_keeper_tb_7 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_17;
                PyObject *tmp_compexpr_left_12;
                PyObject *tmp_compexpr_right_12;
                tmp_compexpr_left_12 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_12 = PyExc_BaseException;
                generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_12, tmp_compexpr_right_12 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 181;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_16;
                }
                tmp_condition_result_17 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_17;
                }
                else
                {
                    goto branch_no_17;
                }
                branch_yes_17:;
                {
                    PyObject *tmp_assign_source_40;
                    tmp_assign_source_40 = Py_False;
                    {
                        PyObject *old = generator_heap->tmp_with_2__indicator;
                        assert( old != NULL );
                        generator_heap->tmp_with_2__indicator = tmp_assign_source_40;
                        Py_INCREF( generator_heap->tmp_with_2__indicator );
                        Py_DECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_18;
                    PyObject *tmp_operand_name_3;
                    PyObject *tmp_called_name_16;
                    PyObject *tmp_args_element_name_27;
                    PyObject *tmp_args_element_name_28;
                    PyObject *tmp_args_element_name_29;
                    CHECK_OBJECT( generator_heap->tmp_with_2__exit );
                    tmp_called_name_16 = generator_heap->tmp_with_2__exit;
                    tmp_args_element_name_27 = EXC_TYPE(PyThreadState_GET());
                    tmp_args_element_name_28 = EXC_VALUE(PyThreadState_GET());
                    tmp_args_element_name_29 = EXC_TRACEBACK(PyThreadState_GET());
                    generator->m_frame->m_frame.f_lineno = 183;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_27, tmp_args_element_name_28, tmp_args_element_name_29 };
                        tmp_operand_name_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_16, call_args );
                    }

                    if ( tmp_operand_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 183;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_16;
                    }
                    generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                    Py_DECREF( tmp_operand_name_3 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 183;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_16;
                    }
                    tmp_condition_result_18 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_18;
                    }
                    else
                    {
                        goto branch_no_18;
                    }
                    branch_yes_18:;
                    generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    if (unlikely( generator_heap->tmp_result == false ))
                    {
                        generator_heap->exception_lineno = 183;
                    }

                    if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_16;
                    branch_no_18:;
                }
                goto branch_end_17;
                branch_no_17:;
                generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                if (unlikely( generator_heap->tmp_result == false ))
                {
                    generator_heap->exception_lineno = 181;
                }

                if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_16;
                branch_end_17:;
            }
            goto try_end_7;
            // Exception handler code:
            try_except_handler_16:;
            generator_heap->exception_keeper_type_8 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_8 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_8 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_8 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_3, generator_heap->exception_preserved_value_3, generator_heap->exception_preserved_tb_3 );
            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_8;
            generator_heap->exception_value = generator_heap->exception_keeper_value_8;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_8;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_8;

            goto try_except_handler_14;
            // End of try:
            try_end_7:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_3, generator_heap->exception_preserved_value_3, generator_heap->exception_preserved_tb_3 );
            goto try_end_6;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros );
            return NULL;
            // End of try:
            try_end_6:;
            goto try_end_8;
            // Exception handler code:
            try_except_handler_14:;
            generator_heap->exception_keeper_type_9 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_9 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_9 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_9 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            {
                nuitka_bool tmp_condition_result_19;
                nuitka_bool tmp_compexpr_left_13;
                nuitka_bool tmp_compexpr_right_13;
                int tmp_truth_name_8;
                CHECK_OBJECT( generator_heap->tmp_with_2__indicator );
                tmp_truth_name_8 = CHECK_IF_TRUE( generator_heap->tmp_with_2__indicator );
                if ( tmp_truth_name_8 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    Py_DECREF( generator_heap->exception_keeper_type_9 );
                    Py_XDECREF( generator_heap->exception_keeper_value_9 );
                    Py_XDECREF( generator_heap->exception_keeper_tb_9 );

                    generator_heap->exception_lineno = 181;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_13;
                }
                tmp_compexpr_left_13 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_compexpr_right_13 = NUITKA_BOOL_TRUE;
                tmp_condition_result_19 = ( tmp_compexpr_left_13 == tmp_compexpr_right_13 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_19;
                }
                else
                {
                    goto branch_no_19;
                }
                branch_yes_19:;
                {
                    PyObject *tmp_called_name_17;
                    PyObject *tmp_call_result_13;
                    CHECK_OBJECT( generator_heap->tmp_with_2__exit );
                    tmp_called_name_17 = generator_heap->tmp_with_2__exit;
                    generator->m_frame->m_frame.f_lineno = 183;
                    tmp_call_result_13 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                        Py_DECREF( generator_heap->exception_keeper_type_9 );
                        Py_XDECREF( generator_heap->exception_keeper_value_9 );
                        Py_XDECREF( generator_heap->exception_keeper_tb_9 );

                        generator_heap->exception_lineno = 183;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_13;
                    }
                    Py_DECREF( tmp_call_result_13 );
                }
                branch_no_19:;
            }
            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_9;
            generator_heap->exception_value = generator_heap->exception_keeper_value_9;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_9;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_9;

            goto try_except_handler_13;
            // End of try:
            try_end_8:;
            {
                nuitka_bool tmp_condition_result_20;
                nuitka_bool tmp_compexpr_left_14;
                nuitka_bool tmp_compexpr_right_14;
                int tmp_truth_name_9;
                CHECK_OBJECT( generator_heap->tmp_with_2__indicator );
                tmp_truth_name_9 = CHECK_IF_TRUE( generator_heap->tmp_with_2__indicator );
                if ( tmp_truth_name_9 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 181;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_13;
                }
                tmp_compexpr_left_14 = tmp_truth_name_9 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_compexpr_right_14 = NUITKA_BOOL_TRUE;
                tmp_condition_result_20 = ( tmp_compexpr_left_14 == tmp_compexpr_right_14 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_20;
                }
                else
                {
                    goto branch_no_20;
                }
                branch_yes_20:;
                {
                    PyObject *tmp_called_name_18;
                    PyObject *tmp_call_result_14;
                    CHECK_OBJECT( generator_heap->tmp_with_2__exit );
                    tmp_called_name_18 = generator_heap->tmp_with_2__exit;
                    generator->m_frame->m_frame.f_lineno = 183;
                    tmp_call_result_14 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 183;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_13;
                    }
                    Py_DECREF( tmp_call_result_14 );
                }
                branch_no_20:;
            }
            goto try_end_9;
            // Exception handler code:
            try_except_handler_13:;
            generator_heap->exception_keeper_type_10 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_10 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_10 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_10 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_with_2__source );
            generator_heap->tmp_with_2__source = NULL;

            Py_XDECREF( generator_heap->tmp_with_2__enter );
            generator_heap->tmp_with_2__enter = NULL;

            Py_XDECREF( generator_heap->tmp_with_2__exit );
            generator_heap->tmp_with_2__exit = NULL;

            Py_XDECREF( generator_heap->tmp_with_2__indicator );
            generator_heap->tmp_with_2__indicator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_10;
            generator_heap->exception_value = generator_heap->exception_keeper_value_10;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_10;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_10;

            goto try_except_handler_12;
            // End of try:
            try_end_9:;
            CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_2__source );
            Py_DECREF( generator_heap->tmp_with_2__source );
            generator_heap->tmp_with_2__source = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_2__enter );
            Py_DECREF( generator_heap->tmp_with_2__enter );
            generator_heap->tmp_with_2__enter = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_2__exit );
            Py_DECREF( generator_heap->tmp_with_2__exit );
            generator_heap->tmp_with_2__exit = NULL;

            Py_XDECREF( generator_heap->tmp_with_2__indicator );
            generator_heap->tmp_with_2__indicator = NULL;

            {
                PyObject *tmp_expression_name_3;
                PyObject *tmp_tuple_element_5;
                NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_3;
                CHECK_OBJECT( generator_heap->var_cp );
                tmp_tuple_element_5 = generator_heap->var_cp;
                tmp_expression_name_3 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_expression_name_3, 0, tmp_tuple_element_5 );
                CHECK_OBJECT( generator_heap->var_distro );
                tmp_tuple_element_5 = generator_heap->var_distro;
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_expression_name_3, 1, tmp_tuple_element_5 );
                Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_13, sizeof(nuitka_bool), &tmp_called_instance_15, sizeof(PyObject *), &tmp_mvar_value_13, sizeof(PyObject *), &tmp_call_result_10, sizeof(PyObject *), &tmp_args_element_name_19, sizeof(PyObject *), &tmp_truth_name_7, sizeof(int), &tmp_tuple_element_5, sizeof(PyObject *), NULL );
                generator->m_yield_return_index = 3;
                return tmp_expression_name_3;
                yield_return_3:
                Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_condition_result_13, sizeof(nuitka_bool), &tmp_called_instance_15, sizeof(PyObject *), &tmp_mvar_value_13, sizeof(PyObject *), &tmp_call_result_10, sizeof(PyObject *), &tmp_args_element_name_19, sizeof(PyObject *), &tmp_truth_name_7, sizeof(int), &tmp_tuple_element_5, sizeof(PyObject *), NULL );
                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 184;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_12;
                }
                tmp_yield_result_3 = yield_return_value;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 164;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_12;
            }
            goto loop_start_2;
            loop_end_2:;
            goto try_end_10;
            // Exception handler code:
            try_except_handler_12:;
            generator_heap->exception_keeper_type_11 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_11 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_11 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_11 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
            generator_heap->tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
            generator_heap->tmp_for_loop_2__for_iterator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_11;
            generator_heap->exception_value = generator_heap->exception_keeper_value_11;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_11;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_11;

            goto try_except_handler_11;
            // End of try:
            try_end_10:;
            Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
            generator_heap->tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
            generator_heap->tmp_for_loop_2__for_iterator = NULL;

            goto try_end_11;
            // Exception handler code:
            try_except_handler_11:;
            generator_heap->exception_keeper_type_12 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_12 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_12 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_12 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            // Preserve existing published exception.
            generator_heap->exception_preserved_type_4 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( generator_heap->exception_preserved_type_4 );
            generator_heap->exception_preserved_value_4 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( generator_heap->exception_preserved_value_4 );
            generator_heap->exception_preserved_tb_4 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( generator_heap->exception_preserved_tb_4 );

            if ( generator_heap->exception_keeper_tb_12 == NULL )
            {
                generator_heap->exception_keeper_tb_12 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_12 );
            }
            else if ( generator_heap->exception_keeper_lineno_12 != 0 )
            {
                generator_heap->exception_keeper_tb_12 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_12, generator->m_frame, generator_heap->exception_keeper_lineno_12 );
            }

            NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_12, &generator_heap->exception_keeper_value_12, &generator_heap->exception_keeper_tb_12 );
            PyException_SetTraceback( generator_heap->exception_keeper_value_12, (PyObject *)generator_heap->exception_keeper_tb_12 );
            PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_12, &generator_heap->exception_keeper_value_12, &generator_heap->exception_keeper_tb_12 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_21;
                PyObject *tmp_compexpr_left_15;
                PyObject *tmp_compexpr_right_15;
                tmp_compexpr_left_15 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_15 = PyExc_BaseException;
                generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_15, tmp_compexpr_right_15 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_17;
                }
                tmp_condition_result_21 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_21;
                }
                else
                {
                    goto branch_no_21;
                }
                branch_yes_21:;
                {
                    PyObject *tmp_assign_source_41;
                    tmp_assign_source_41 = Py_False;
                    {
                        PyObject *old = generator_heap->tmp_with_3__indicator;
                        assert( old != NULL );
                        generator_heap->tmp_with_3__indicator = tmp_assign_source_41;
                        Py_INCREF( generator_heap->tmp_with_3__indicator );
                        Py_DECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_22;
                    PyObject *tmp_operand_name_4;
                    PyObject *tmp_called_name_19;
                    PyObject *tmp_args_element_name_30;
                    PyObject *tmp_args_element_name_31;
                    PyObject *tmp_args_element_name_32;
                    CHECK_OBJECT( generator_heap->tmp_with_3__exit );
                    tmp_called_name_19 = generator_heap->tmp_with_3__exit;
                    tmp_args_element_name_30 = EXC_TYPE(PyThreadState_GET());
                    tmp_args_element_name_31 = EXC_VALUE(PyThreadState_GET());
                    tmp_args_element_name_32 = EXC_TRACEBACK(PyThreadState_GET());
                    generator->m_frame->m_frame.f_lineno = 164;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_30, tmp_args_element_name_31, tmp_args_element_name_32 };
                        tmp_operand_name_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_19, call_args );
                    }

                    if ( tmp_operand_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 164;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_17;
                    }
                    generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
                    Py_DECREF( tmp_operand_name_4 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 164;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_17;
                    }
                    tmp_condition_result_22 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_22;
                    }
                    else
                    {
                        goto branch_no_22;
                    }
                    branch_yes_22:;
                    generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    if (unlikely( generator_heap->tmp_result == false ))
                    {
                        generator_heap->exception_lineno = 164;
                    }

                    if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_17;
                    branch_no_22:;
                }
                goto branch_end_21;
                branch_no_21:;
                generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                if (unlikely( generator_heap->tmp_result == false ))
                {
                    generator_heap->exception_lineno = 163;
                }

                if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_17;
                branch_end_21:;
            }
            goto try_end_12;
            // Exception handler code:
            try_except_handler_17:;
            generator_heap->exception_keeper_type_13 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_13 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_13 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_13 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_4, generator_heap->exception_preserved_value_4, generator_heap->exception_preserved_tb_4 );
            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_13;
            generator_heap->exception_value = generator_heap->exception_keeper_value_13;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_13;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_13;

            goto try_except_handler_10;
            // End of try:
            try_end_12:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_4, generator_heap->exception_preserved_value_4, generator_heap->exception_preserved_tb_4 );
            goto try_end_11;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros );
            return NULL;
            // End of try:
            try_end_11:;
            goto try_end_13;
            // Exception handler code:
            try_except_handler_10:;
            generator_heap->exception_keeper_type_14 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_14 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_14 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_14 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            {
                nuitka_bool tmp_condition_result_23;
                nuitka_bool tmp_compexpr_left_16;
                nuitka_bool tmp_compexpr_right_16;
                int tmp_truth_name_10;
                CHECK_OBJECT( generator_heap->tmp_with_3__indicator );
                tmp_truth_name_10 = CHECK_IF_TRUE( generator_heap->tmp_with_3__indicator );
                if ( tmp_truth_name_10 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    Py_DECREF( generator_heap->exception_keeper_type_14 );
                    Py_XDECREF( generator_heap->exception_keeper_value_14 );
                    Py_XDECREF( generator_heap->exception_keeper_tb_14 );

                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_9;
                }
                tmp_compexpr_left_16 = tmp_truth_name_10 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_compexpr_right_16 = NUITKA_BOOL_TRUE;
                tmp_condition_result_23 = ( tmp_compexpr_left_16 == tmp_compexpr_right_16 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_23;
                }
                else
                {
                    goto branch_no_23;
                }
                branch_yes_23:;
                {
                    PyObject *tmp_called_name_20;
                    PyObject *tmp_call_result_15;
                    CHECK_OBJECT( generator_heap->tmp_with_3__exit );
                    tmp_called_name_20 = generator_heap->tmp_with_3__exit;
                    generator->m_frame->m_frame.f_lineno = 164;
                    tmp_call_result_15 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_20, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                        Py_DECREF( generator_heap->exception_keeper_type_14 );
                        Py_XDECREF( generator_heap->exception_keeper_value_14 );
                        Py_XDECREF( generator_heap->exception_keeper_tb_14 );

                        generator_heap->exception_lineno = 164;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_9;
                    }
                    Py_DECREF( tmp_call_result_15 );
                }
                branch_no_23:;
            }
            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_14;
            generator_heap->exception_value = generator_heap->exception_keeper_value_14;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_14;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_14;

            goto try_except_handler_9;
            // End of try:
            try_end_13:;
            {
                nuitka_bool tmp_condition_result_24;
                nuitka_bool tmp_compexpr_left_17;
                nuitka_bool tmp_compexpr_right_17;
                int tmp_truth_name_11;
                CHECK_OBJECT( generator_heap->tmp_with_3__indicator );
                tmp_truth_name_11 = CHECK_IF_TRUE( generator_heap->tmp_with_3__indicator );
                if ( tmp_truth_name_11 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 163;
                    generator_heap->type_description_1 = "ccooooooooooooo";
                    goto try_except_handler_9;
                }
                tmp_compexpr_left_17 = tmp_truth_name_11 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_compexpr_right_17 = NUITKA_BOOL_TRUE;
                tmp_condition_result_24 = ( tmp_compexpr_left_17 == tmp_compexpr_right_17 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_24;
                }
                else
                {
                    goto branch_no_24;
                }
                branch_yes_24:;
                {
                    PyObject *tmp_called_name_21;
                    PyObject *tmp_call_result_16;
                    CHECK_OBJECT( generator_heap->tmp_with_3__exit );
                    tmp_called_name_21 = generator_heap->tmp_with_3__exit;
                    generator->m_frame->m_frame.f_lineno = 164;
                    tmp_call_result_16 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 164;
                        generator_heap->type_description_1 = "ccooooooooooooo";
                        goto try_except_handler_9;
                    }
                    Py_DECREF( tmp_call_result_16 );
                }
                branch_no_24:;
            }
            goto try_end_14;
            // Exception handler code:
            try_except_handler_9:;
            generator_heap->exception_keeper_type_15 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_15 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_15 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_15 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_with_3__source );
            generator_heap->tmp_with_3__source = NULL;

            Py_XDECREF( generator_heap->tmp_with_3__enter );
            generator_heap->tmp_with_3__enter = NULL;

            Py_XDECREF( generator_heap->tmp_with_3__exit );
            generator_heap->tmp_with_3__exit = NULL;

            Py_XDECREF( generator_heap->tmp_with_3__indicator );
            generator_heap->tmp_with_3__indicator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_15;
            generator_heap->exception_value = generator_heap->exception_keeper_value_15;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_15;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_15;

            goto try_except_handler_2;
            // End of try:
            try_end_14:;
            CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_3__source );
            Py_DECREF( generator_heap->tmp_with_3__source );
            generator_heap->tmp_with_3__source = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_3__enter );
            Py_DECREF( generator_heap->tmp_with_3__enter );
            generator_heap->tmp_with_3__enter = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_3__exit );
            Py_DECREF( generator_heap->tmp_with_3__exit );
            generator_heap->tmp_with_3__exit = NULL;

            Py_XDECREF( generator_heap->tmp_with_3__indicator );
            generator_heap->tmp_with_3__indicator = NULL;

            branch_no_13:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_name_22;
        PyObject *tmp_source_name_20;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_33;
        PyObject *tmp_called_name_23;
        PyObject *tmp_source_name_21;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_34;
        PyObject *tmp_called_instance_22;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_args_element_name_35;
        PyObject *tmp_args_element_name_36;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_args_element_name_38;
        PyObject *tmp_called_name_24;
        PyObject *tmp_source_name_22;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_called_instance_23;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_args_element_name_42;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_itertools );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_itertools );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "itertools" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 187;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_20 = tmp_mvar_value_20;
        tmp_called_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_chain );
        if ( tmp_called_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 187;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_glob );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_glob );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_called_name_22 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "glob" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_21 = tmp_mvar_value_21;
        tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_iglob );
        if ( tmp_called_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_22 );

            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_23 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_22 = tmp_mvar_value_22;
        CHECK_OBJECT( generator_heap->var_folder );
        tmp_args_element_name_35 = generator_heap->var_folder;
        tmp_args_element_name_36 = const_str_digest_5a3dc9a5710d387f205bb6ab501c758b;
        tmp_args_element_name_37 = const_str_digest_7ae94de8e546ee88855d387e29ed702b;
        generator->m_frame->m_frame.f_lineno = 188;
        {
            PyObject *call_args[] = { tmp_args_element_name_35, tmp_args_element_name_36, tmp_args_element_name_37 };
            tmp_args_element_name_34 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_22, const_str_plain_join, call_args );
        }

        if ( tmp_args_element_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_23 );

            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 188;
        {
            PyObject *call_args[] = { tmp_args_element_name_34 };
            tmp_args_element_name_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_called_name_23 );
        Py_DECREF( tmp_args_element_name_34 );
        if ( tmp_args_element_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_22 );

            generator_heap->exception_lineno = 188;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_glob );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_glob );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_33 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "glob" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_22 = tmp_mvar_value_23;
        tmp_called_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_iglob );
        if ( tmp_called_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_33 );

            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_33 );
            Py_DECREF( tmp_called_name_24 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_23 = tmp_mvar_value_24;
        CHECK_OBJECT( generator_heap->var_folder );
        tmp_args_element_name_40 = generator_heap->var_folder;
        tmp_args_element_name_41 = const_str_digest_c7dd7cd78323cc9d4b8499907c7a923a;
        tmp_args_element_name_42 = const_str_digest_7ae94de8e546ee88855d387e29ed702b;
        generator->m_frame->m_frame.f_lineno = 189;
        {
            PyObject *call_args[] = { tmp_args_element_name_40, tmp_args_element_name_41, tmp_args_element_name_42 };
            tmp_args_element_name_39 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_23, const_str_plain_join, call_args );
        }

        if ( tmp_args_element_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_33 );
            Py_DECREF( tmp_called_name_24 );

            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 189;
        {
            PyObject *call_args[] = { tmp_args_element_name_39 };
            tmp_args_element_name_38 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_called_name_24 );
        Py_DECREF( tmp_args_element_name_39 );
        if ( tmp_args_element_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_33 );

            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 187;
        {
            PyObject *call_args[] = { tmp_args_element_name_33, tmp_args_element_name_38 };
            tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_called_name_22 );
        Py_DECREF( tmp_args_element_name_33 );
        Py_DECREF( tmp_args_element_name_38 );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 187;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_42 = MAKE_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 187;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = generator_heap->tmp_for_loop_3__for_iterator;
            generator_heap->tmp_for_loop_3__for_iterator = tmp_assign_source_42;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_3:;
    {
        PyObject *tmp_next_source_3;
        PyObject *tmp_assign_source_43;
        CHECK_OBJECT( generator_heap->tmp_for_loop_3__for_iterator );
        tmp_next_source_3 = generator_heap->tmp_for_loop_3__for_iterator;
        tmp_assign_source_43 = ITERATOR_NEXT( tmp_next_source_3 );
        if ( tmp_assign_source_43 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_3;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "ccooooooooooooo";
                generator_heap->exception_lineno = 187;
                goto try_except_handler_18;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_3__iter_value;
            generator_heap->tmp_for_loop_3__iter_value = tmp_assign_source_43;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_44;
        CHECK_OBJECT( generator_heap->tmp_for_loop_3__iter_value );
        tmp_assign_source_44 = generator_heap->tmp_for_loop_3__iter_value;
        {
            PyObject *old = PyCell_GET( generator->m_closure[0] );
            PyCell_SET( generator->m_closure[0], tmp_assign_source_44 );
            Py_INCREF( tmp_assign_source_44 );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_called_name_25;
        PyObject *tmp_source_name_23;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_called_name_26;
        PyObject *tmp_source_name_24;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_44;
        PyObject *tmp_called_instance_24;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_subscript_name_2;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }

        tmp_source_name_23 = tmp_mvar_value_25;
        tmp_called_name_25 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_splitext );
        if ( tmp_called_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_called_name_25 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }

        tmp_source_name_24 = tmp_mvar_value_26;
        tmp_called_name_26 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_basename );
        if ( tmp_called_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_25 );

            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_osp );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_called_name_25 );
            Py_DECREF( tmp_called_name_26 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "osp" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }

        tmp_called_instance_24 = tmp_mvar_value_27;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_args_element_name_45 = PyCell_GET( generator->m_closure[0] );
        generator->m_frame->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_45 };
            tmp_args_element_name_44 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_24, const_str_plain_dirname, call_args );
        }

        if ( tmp_args_element_name_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_25 );
            Py_DECREF( tmp_called_name_26 );

            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        generator->m_frame->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_44 };
            tmp_args_element_name_43 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
        }

        Py_DECREF( tmp_called_name_26 );
        Py_DECREF( tmp_args_element_name_44 );
        if ( tmp_args_element_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_25 );

            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        generator->m_frame->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_43 };
            tmp_subscribed_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_called_name_25 );
        Py_DECREF( tmp_args_element_name_43 );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_assign_source_45 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 191;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        {
            PyObject *old = generator_heap->var_distro_name_version;
            generator_heap->var_distro_name_version = tmp_assign_source_45;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_25;
        PyObject *tmp_compexpr_left_18;
        PyObject *tmp_compexpr_right_18;
        tmp_compexpr_left_18 = const_str_chr_45;
        CHECK_OBJECT( generator_heap->var_distro_name_version );
        tmp_compexpr_right_18 = generator_heap->var_distro_name_version;
        generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_18, tmp_compexpr_left_18 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 192;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        tmp_condition_result_25 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_25;
        }
        else
        {
            goto branch_no_25;
        }
        branch_yes_25:;
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_dircall_arg1_3;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_dircall_arg2_3;
            PyObject *tmp_called_instance_25;
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_Distribution );

            if (unlikely( tmp_mvar_value_28 == NULL ))
            {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Distribution );
            }

            if ( tmp_mvar_value_28 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Distribution" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 193;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }

            tmp_dircall_arg1_3 = tmp_mvar_value_28;
            CHECK_OBJECT( generator_heap->var_distro_name_version );
            tmp_called_instance_25 = generator_heap->var_distro_name_version;
            generator->m_frame->m_frame.f_lineno = 193;
            tmp_dircall_arg2_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_25, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_45_int_pos_1_tuple, 0 ) );

            if ( tmp_dircall_arg2_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 193;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            Py_INCREF( tmp_dircall_arg1_3 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_3, tmp_dircall_arg2_3};
                tmp_assign_source_46 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
            }
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 193;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            {
                PyObject *old = generator_heap->var_distro;
                generator_heap->var_distro = tmp_assign_source_46;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_26;
            int tmp_and_left_truth_3;
            nuitka_bool tmp_and_left_value_3;
            nuitka_bool tmp_and_right_value_3;
            PyObject *tmp_compexpr_left_19;
            PyObject *tmp_compexpr_right_19;
            PyObject *tmp_compexpr_left_20;
            PyObject *tmp_compexpr_right_20;
            PyObject *tmp_source_name_25;
            if ( PyCell_GET( generator->m_closure[1] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "repeated_distro" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 195;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }

            tmp_compexpr_left_19 = PyCell_GET( generator->m_closure[1] );
            tmp_compexpr_right_19 = const_str_plain_first;
            generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_19, tmp_compexpr_right_19 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 195;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            tmp_and_left_value_3 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_3 == 1 )
            {
                goto and_right_3;
            }
            else
            {
                goto and_left_3;
            }
            and_right_3:;
            CHECK_OBJECT( generator_heap->var_distro );
            tmp_source_name_25 = generator_heap->var_distro;
            tmp_compexpr_left_20 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_name );
            if ( tmp_compexpr_left_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 196;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            CHECK_OBJECT( generator_heap->var_distro_names_seen );
            tmp_compexpr_right_20 = generator_heap->var_distro_names_seen;
            generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_20, tmp_compexpr_left_20 );
            Py_DECREF( tmp_compexpr_left_20 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 196;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            tmp_and_right_value_3 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_26 = tmp_and_right_value_3;
            goto and_end_3;
            and_left_3:;
            tmp_condition_result_26 = tmp_and_left_value_3;
            and_end_3:;
            if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_26;
            }
            else
            {
                goto branch_no_26;
            }
            branch_yes_26:;
            goto loop_start_3;
            branch_no_26:;
        }
        {
            PyObject *tmp_called_name_27;
            PyObject *tmp_source_name_26;
            PyObject *tmp_call_result_17;
            PyObject *tmp_args_element_name_46;
            PyObject *tmp_source_name_27;
            CHECK_OBJECT( generator_heap->var_distro_names_seen );
            tmp_source_name_26 = generator_heap->var_distro_names_seen;
            tmp_called_name_27 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_add );
            if ( tmp_called_name_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 198;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            CHECK_OBJECT( generator_heap->var_distro );
            tmp_source_name_27 = generator_heap->var_distro;
            tmp_args_element_name_46 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_name );
            if ( tmp_args_element_name_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_called_name_27 );

                generator_heap->exception_lineno = 198;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            generator->m_frame->m_frame.f_lineno = 198;
            {
                PyObject *call_args[] = { tmp_args_element_name_46 };
                tmp_call_result_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
            }

            Py_DECREF( tmp_called_name_27 );
            Py_DECREF( tmp_args_element_name_46 );
            if ( tmp_call_result_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 198;
                generator_heap->type_description_1 = "ccooooooooooooo";
                goto try_except_handler_18;
            }
            Py_DECREF( tmp_call_result_17 );
        }
        goto branch_end_25;
        branch_no_25:;
        {
            PyObject *tmp_assign_source_47;
            tmp_assign_source_47 = Py_None;
            {
                PyObject *old = generator_heap->var_distro;
                generator_heap->var_distro = tmp_assign_source_47;
                Py_INCREF( generator_heap->var_distro );
                Py_XDECREF( old );
            }

        }
        branch_end_25:;
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_called_name_28;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_kw_name_6;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CaseSensitiveConfigParser" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 201;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }

        tmp_called_name_28 = tmp_mvar_value_29;
        tmp_kw_name_6 = PyDict_Copy( const_dict_76e556d6c52e2dbcc094dfc5f551a7fe );
        generator->m_frame->m_frame.f_lineno = 201;
        tmp_assign_source_48 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_28, tmp_kw_name_6 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 201;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        {
            PyObject *old = generator_heap->var_cp;
            generator_heap->var_cp = tmp_assign_source_48;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_26;
        PyObject *tmp_call_result_18;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_list_element_2;
        CHECK_OBJECT( generator_heap->var_cp );
        tmp_called_instance_26 = generator_heap->var_cp;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_list_element_2 = PyCell_GET( generator->m_closure[0] );
        tmp_args_element_name_47 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_args_element_name_47, 0, tmp_list_element_2 );
        generator->m_frame->m_frame.f_lineno = 202;
        {
            PyObject *call_args[] = { tmp_args_element_name_47 };
            tmp_call_result_18 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_26, const_str_plain_read, call_args );
        }

        Py_DECREF( tmp_args_element_name_47 );
        if ( tmp_call_result_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 202;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        Py_DECREF( tmp_call_result_18 );
    }
    {
        PyObject *tmp_expression_name_4;
        PyObject *tmp_tuple_element_6;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_4;
        CHECK_OBJECT( generator_heap->var_cp );
        tmp_tuple_element_6 = generator_heap->var_cp;
        tmp_expression_name_4 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_expression_name_4, 0, tmp_tuple_element_6 );
        CHECK_OBJECT( generator_heap->var_distro );
        tmp_tuple_element_6 = generator_heap->var_distro;
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_expression_name_4, 1, tmp_tuple_element_6 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_6, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 4;
        return tmp_expression_name_4;
        yield_return_4:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_6, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 203;
            generator_heap->type_description_1 = "ccooooooooooooo";
            goto try_except_handler_18;
        }
        tmp_yield_result_4 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 187;
        generator_heap->type_description_1 = "ccooooooooooooo";
        goto try_except_handler_18;
    }
    goto loop_start_3;
    loop_end_3:;
    goto try_end_15;
    // Exception handler code:
    try_except_handler_18:;
    generator_heap->exception_keeper_type_16 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_16 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_16 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_16 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_3__iter_value );
    generator_heap->tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_3__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_3__for_iterator );
    generator_heap->tmp_for_loop_3__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_16;
    generator_heap->exception_value = generator_heap->exception_keeper_value_16;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_16;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_16;

    goto try_except_handler_2;
    // End of try:
    try_end_15:;
    Py_XDECREF( generator_heap->tmp_for_loop_3__iter_value );
    generator_heap->tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_3__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_3__for_iterator );
    generator_heap->tmp_for_loop_3__for_iterator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 127;
        generator_heap->type_description_1 = "ccooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_16;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_17 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_17 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_17 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_17 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_17;
    generator_heap->exception_value = generator_heap->exception_keeper_value_17;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_17;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_17;

    goto frame_exception_exit_1;
    // End of try:
    try_end_16:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0],
            generator->m_closure[1],
            generator_heap->var_distro_names_seen,
            generator_heap->var_folder,
            generator_heap->var_egg_name,
            generator_heap->var_distro,
            generator_heap->var_ep_path,
            generator_heap->var_cp,
            generator_heap->var_z,
            generator_heap->var_info,
            generator_heap->var_f,
            generator_heap->var_fu,
            generator_heap->var_zf,
            generator_heap->var_m,
            generator_heap->var_distro_name_version
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_17;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_18 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_18 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_18 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_18 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_distro_names_seen );
    generator_heap->var_distro_names_seen = NULL;

    Py_XDECREF( generator_heap->var_folder );
    generator_heap->var_folder = NULL;

    Py_XDECREF( generator_heap->var_egg_name );
    generator_heap->var_egg_name = NULL;

    Py_XDECREF( generator_heap->var_distro );
    generator_heap->var_distro = NULL;

    Py_XDECREF( generator_heap->var_ep_path );
    generator_heap->var_ep_path = NULL;

    Py_XDECREF( generator_heap->var_cp );
    generator_heap->var_cp = NULL;

    Py_XDECREF( generator_heap->var_z );
    generator_heap->var_z = NULL;

    Py_XDECREF( generator_heap->var_info );
    generator_heap->var_info = NULL;

    Py_XDECREF( generator_heap->var_f );
    generator_heap->var_f = NULL;

    Py_XDECREF( generator_heap->var_fu );
    generator_heap->var_fu = NULL;

    Py_XDECREF( generator_heap->var_zf );
    generator_heap->var_zf = NULL;

    Py_XDECREF( generator_heap->var_m );
    generator_heap->var_m = NULL;

    Py_XDECREF( generator_heap->var_distro_name_version );
    generator_heap->var_distro_name_version = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_18;
    generator_heap->exception_value = generator_heap->exception_keeper_value_18;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_18;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_18;

    goto function_exception_exit;
    // End of try:
    try_end_17:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_distro_names_seen );
    Py_DECREF( generator_heap->var_distro_names_seen );
    generator_heap->var_distro_names_seen = NULL;

    Py_XDECREF( generator_heap->var_folder );
    generator_heap->var_folder = NULL;

    Py_XDECREF( generator_heap->var_egg_name );
    generator_heap->var_egg_name = NULL;

    Py_XDECREF( generator_heap->var_distro );
    generator_heap->var_distro = NULL;

    Py_XDECREF( generator_heap->var_ep_path );
    generator_heap->var_ep_path = NULL;

    Py_XDECREF( generator_heap->var_cp );
    generator_heap->var_cp = NULL;

    Py_XDECREF( generator_heap->var_z );
    generator_heap->var_z = NULL;

    Py_XDECREF( generator_heap->var_info );
    generator_heap->var_info = NULL;

    Py_XDECREF( generator_heap->var_f );
    generator_heap->var_f = NULL;

    Py_XDECREF( generator_heap->var_fu );
    generator_heap->var_fu = NULL;

    Py_XDECREF( generator_heap->var_zf );
    generator_heap->var_zf = NULL;

    Py_XDECREF( generator_heap->var_m );
    generator_heap->var_m = NULL;

    Py_XDECREF( generator_heap->var_distro_name_version );
    generator_heap->var_distro_name_version = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_maker( void )
{
    return Nuitka_Generator_New(
        entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_context,
        module_entrypoints,
        const_str_plain_iter_files_distros,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_a94a23ab97e50c47150f791064d420f0,
        2,
        sizeof(struct entrypoints$$$function_12_iter_files_distros$$$genobj_1_iter_files_distros_locals)
    );
}


static PyObject *impl_entrypoints$$$function_13_get_single( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_group = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_path = python_pars[ 2 ];
    PyObject *var_config = NULL;
    PyObject *var_distro = NULL;
    PyObject *var_epstr = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    PyObject *tmp_with_1__indicator = NULL;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_af54eb11dcf9f66626745aa1a8c8d702;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_af54eb11dcf9f66626745aa1a8c8d702 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_af54eb11dcf9f66626745aa1a8c8d702, codeobj_af54eb11dcf9f66626745aa1a8c8d702, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_af54eb11dcf9f66626745aa1a8c8d702 = cache_frame_af54eb11dcf9f66626745aa1a8c8d702;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_af54eb11dcf9f66626745aa1a8c8d702 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_af54eb11dcf9f66626745aa1a8c8d702 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_iter_files_distros );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iter_files_distros );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iter_files_distros" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 211;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_path;
        CHECK_OBJECT( par_path );
        tmp_dict_value_1 = par_path;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 211;
        tmp_iter_arg_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                exception_lineno = 211;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 211;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 211;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 211;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 211;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_config;
            var_config = tmp_assign_source_6;
            Py_INCREF( var_config );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_distro;
            var_distro = tmp_assign_source_7;
            Py_INCREF( var_distro );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_group );
        tmp_compexpr_left_1 = par_group;
        CHECK_OBJECT( var_config );
        tmp_compexpr_right_1 = var_config;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_and_left_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_name );
        tmp_compexpr_left_2 = par_name;
        CHECK_OBJECT( var_config );
        tmp_subscribed_name_1 = var_config;
        CHECK_OBJECT( par_group );
        tmp_subscript_name_1 = par_group;
        tmp_compexpr_right_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_and_right_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_subscript_name_3;
            CHECK_OBJECT( var_config );
            tmp_subscribed_name_3 = var_config;
            CHECK_OBJECT( par_group );
            tmp_subscript_name_2 = par_group;
            tmp_subscribed_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_2 );
            if ( tmp_subscribed_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 213;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( par_name );
            tmp_subscript_name_3 = par_name;
            tmp_assign_source_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_2 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 213;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_epstr;
                var_epstr = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BadEntryPoint" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 214;
            tmp_assign_source_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_err_to_warnings );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_with_1__source;
                tmp_with_1__source = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_1 = tmp_with_1__source;
            tmp_called_name_2 = LOOKUP_SPECIAL( tmp_source_name_1, const_str_plain___enter__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 214;
            tmp_assign_source_10 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_2 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_with_1__enter;
                tmp_with_1__enter = tmp_assign_source_10;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_2 = tmp_with_1__source;
            tmp_assign_source_11 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___exit__ );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_with_1__exit;
                tmp_with_1__exit = tmp_assign_source_11;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = Py_True;
            {
                PyObject *old = tmp_with_1__indicator;
                tmp_with_1__indicator = tmp_assign_source_12;
                Py_INCREF( tmp_with_1__indicator );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_EntryPoint );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EntryPoint );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EntryPoint" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 215;
                type_description_1 = "oooooo";
                goto try_except_handler_7;
            }

            tmp_called_instance_2 = tmp_mvar_value_3;
            CHECK_OBJECT( var_epstr );
            tmp_args_element_name_1 = var_epstr;
            CHECK_OBJECT( par_name );
            tmp_args_element_name_2 = par_name;
            CHECK_OBJECT( var_distro );
            tmp_args_element_name_3 = var_distro;
            frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 215;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_return_value = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain_from_string, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "oooooo";
                goto try_except_handler_7;
            }
            goto try_return_handler_6;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints$$$function_13_get_single );
        return NULL;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_3 == NULL )
        {
            exception_keeper_tb_3 = MAKE_TRACEBACK( frame_af54eb11dcf9f66626745aa1a8c8d702, exception_keeper_lineno_3 );
        }
        else if ( exception_keeper_lineno_3 != 0 )
        {
            exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_af54eb11dcf9f66626745aa1a8c8d702, exception_keeper_lineno_3 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
        PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_3 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_8;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_13;
                tmp_assign_source_13 = Py_False;
                {
                    PyObject *old = tmp_with_1__indicator;
                    assert( old != NULL );
                    tmp_with_1__indicator = tmp_assign_source_13;
                    Py_INCREF( tmp_with_1__indicator );
                    Py_DECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_called_name_3;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_args_element_name_6;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_3 = tmp_with_1__exit;
                tmp_args_element_name_4 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_5 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_6 = EXC_TRACEBACK(PyThreadState_GET());
                frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 215;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                    tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
                }

                if ( tmp_operand_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 215;
                    type_description_1 = "oooooo";
                    goto try_except_handler_8;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                Py_DECREF( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 215;
                    type_description_1 = "oooooo";
                    goto try_except_handler_8;
                }
                tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 215;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame) frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "oooooo";
                goto try_except_handler_8;
                branch_no_3:;
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 214;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame) frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooo";
            goto try_except_handler_8;
            branch_end_2:;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_6;
        // End of try:
        try_end_3:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_4;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints$$$function_13_get_single );
        return NULL;
        // End of try:
        try_end_4:;
        goto try_end_5;
        // Return handler code:
        try_return_handler_6:;
        {
            nuitka_bool tmp_condition_result_4;
            nuitka_bool tmp_compexpr_left_4;
            nuitka_bool tmp_compexpr_right_4;
            int tmp_truth_name_1;
            CHECK_OBJECT( tmp_with_1__indicator );
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_with_1__indicator );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_left_4 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
            tmp_condition_result_4 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_call_result_1;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_4 = tmp_with_1__exit;
                frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 215;
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 215;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_4:;
        }
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_5;
            nuitka_bool tmp_compexpr_left_5;
            nuitka_bool tmp_compexpr_right_5;
            int tmp_truth_name_2;
            CHECK_OBJECT( tmp_with_1__indicator );
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_with_1__indicator );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_5 );
                Py_XDECREF( exception_keeper_value_5 );
                Py_XDECREF( exception_keeper_tb_5 );

                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_left_5 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_compexpr_right_5 = NUITKA_BOOL_TRUE;
            tmp_condition_result_5 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_called_name_5;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_5 = tmp_with_1__exit;
                frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 215;
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_5 );
                    Py_XDECREF( exception_keeper_value_5 );
                    Py_XDECREF( exception_keeper_tb_5 );

                    exception_lineno = 215;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_5:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_5;
        // End of try:
        try_end_5:;
        {
            nuitka_bool tmp_condition_result_6;
            nuitka_bool tmp_compexpr_left_6;
            nuitka_bool tmp_compexpr_right_6;
            int tmp_truth_name_3;
            CHECK_OBJECT( tmp_with_1__indicator );
            tmp_truth_name_3 = CHECK_IF_TRUE( tmp_with_1__indicator );
            if ( tmp_truth_name_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_left_6 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_compexpr_right_6 = NUITKA_BOOL_TRUE;
            tmp_condition_result_6 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_called_name_6;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_6 = tmp_with_1__exit;
                frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 215;
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 215;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            branch_no_6:;
        }
        goto try_end_6;
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__indicator );
        Py_DECREF( tmp_with_1__indicator );
        tmp_with_1__indicator = NULL;

        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        Py_XDECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        Py_XDECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        Py_XDECREF( tmp_with_1__indicator );
        tmp_with_1__indicator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_2;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        Py_XDECREF( tmp_with_1__indicator );
        tmp_with_1__indicator = NULL;

        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 211;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_7;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_raise_type_1;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_NoSuchEntryPoint );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NoSuchEntryPoint );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NoSuchEntryPoint" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 217;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_4;
        CHECK_OBJECT( par_group );
        tmp_args_element_name_7 = par_group;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_8 = par_name;
        frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame.f_lineno = 217;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        if ( tmp_raise_type_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        exception_type = tmp_raise_type_1;
        exception_lineno = 217;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af54eb11dcf9f66626745aa1a8c8d702 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_af54eb11dcf9f66626745aa1a8c8d702 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af54eb11dcf9f66626745aa1a8c8d702 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_af54eb11dcf9f66626745aa1a8c8d702, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_af54eb11dcf9f66626745aa1a8c8d702->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_af54eb11dcf9f66626745aa1a8c8d702, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_af54eb11dcf9f66626745aa1a8c8d702,
        type_description_1,
        par_group,
        par_name,
        par_path,
        var_config,
        var_distro,
        var_epstr
    );


    // Release cached frame.
    if ( frame_af54eb11dcf9f66626745aa1a8c8d702 == cache_frame_af54eb11dcf9f66626745aa1a8c8d702 )
    {
        Py_DECREF( frame_af54eb11dcf9f66626745aa1a8c8d702 );
    }
    cache_frame_af54eb11dcf9f66626745aa1a8c8d702 = NULL;

    assertFrameObject( frame_af54eb11dcf9f66626745aa1a8c8d702 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_13_get_single );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)var_config );
    Py_DECREF( var_config );
    var_config = NULL;

    CHECK_OBJECT( (PyObject *)var_distro );
    Py_DECREF( var_distro );
    var_distro = NULL;

    CHECK_OBJECT( (PyObject *)var_epstr );
    Py_DECREF( var_epstr );
    var_epstr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( var_config );
    var_config = NULL;

    Py_XDECREF( var_distro );
    var_distro = NULL;

    Py_XDECREF( var_epstr );
    var_epstr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_13_get_single );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_14_get_group_named( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_group = python_pars[ 0 ];
    PyObject *par_path = python_pars[ 1 ];
    PyObject *var_result = NULL;
    PyObject *var_ep = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_2a04c1a76f6c9dbe155a394f7cad8d67;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_2a04c1a76f6c9dbe155a394f7cad8d67 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyDict_New();
        assert( var_result == NULL );
        var_result = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2a04c1a76f6c9dbe155a394f7cad8d67, codeobj_2a04c1a76f6c9dbe155a394f7cad8d67, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2a04c1a76f6c9dbe155a394f7cad8d67 = cache_frame_2a04c1a76f6c9dbe155a394f7cad8d67;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2a04c1a76f6c9dbe155a394f7cad8d67 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2a04c1a76f6c9dbe155a394f7cad8d67 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_get_group_all );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_group_all );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_group_all" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 225;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_group );
        tmp_tuple_element_1 = par_group;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_path;
        CHECK_OBJECT( par_path );
        tmp_dict_value_1 = par_path;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_2a04c1a76f6c9dbe155a394f7cad8d67->m_frame.f_lineno = 225;
        tmp_iter_arg_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 225;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_ep;
            var_ep = tmp_assign_source_4;
            Py_INCREF( var_ep );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dict_name_1;
        CHECK_OBJECT( var_ep );
        tmp_source_name_1 = var_ep;
        tmp_key_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
        if ( tmp_key_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_result );
        tmp_dict_name_1 = var_result;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        Py_DECREF( tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( var_ep );
            tmp_dictset_value = var_ep;
            CHECK_OBJECT( var_result );
            tmp_dictset_dict = var_result;
            CHECK_OBJECT( var_ep );
            tmp_source_name_2 = var_ep;
            tmp_dictset_key = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
            if ( tmp_dictset_key == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 227;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_key );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 227;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 225;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a04c1a76f6c9dbe155a394f7cad8d67 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a04c1a76f6c9dbe155a394f7cad8d67 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2a04c1a76f6c9dbe155a394f7cad8d67, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2a04c1a76f6c9dbe155a394f7cad8d67->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2a04c1a76f6c9dbe155a394f7cad8d67, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2a04c1a76f6c9dbe155a394f7cad8d67,
        type_description_1,
        par_group,
        par_path,
        var_result,
        var_ep
    );


    // Release cached frame.
    if ( frame_2a04c1a76f6c9dbe155a394f7cad8d67 == cache_frame_2a04c1a76f6c9dbe155a394f7cad8d67 )
    {
        Py_DECREF( frame_2a04c1a76f6c9dbe155a394f7cad8d67 );
    }
    cache_frame_2a04c1a76f6c9dbe155a394f7cad8d67 = NULL;

    assertFrameObject( frame_2a04c1a76f6c9dbe155a394f7cad8d67 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_14_get_group_named );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_ep );
    var_ep = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_ep );
    var_ep = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_14_get_group_named );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_entrypoints$$$function_15_get_group_all( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_group = python_pars[ 0 ];
    PyObject *par_path = python_pars[ 1 ];
    PyObject *var_result = NULL;
    PyObject *var_config = NULL;
    PyObject *var_distro = NULL;
    PyObject *var_name = NULL;
    PyObject *var_epstr = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    PyObject *tmp_with_1__indicator = NULL;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_4a061c40103077024de98e7857e83c88;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    static struct Nuitka_FrameObject *cache_frame_4a061c40103077024de98e7857e83c88 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_result == NULL );
        var_result = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4a061c40103077024de98e7857e83c88, codeobj_4a061c40103077024de98e7857e83c88, module_entrypoints, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4a061c40103077024de98e7857e83c88 = cache_frame_4a061c40103077024de98e7857e83c88;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4a061c40103077024de98e7857e83c88 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4a061c40103077024de98e7857e83c88 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_iter_files_distros );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iter_files_distros );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iter_files_distros" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 236;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_path;
        CHECK_OBJECT( par_path );
        tmp_dict_value_1 = par_path;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 236;
        tmp_iter_arg_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 236;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 236;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 236;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 236;
            type_description_1 = "ooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 236;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 236;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooo";
                    exception_lineno = 236;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooo";
            exception_lineno = 236;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_config;
            var_config = tmp_assign_source_7;
            Py_INCREF( var_config );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_distro;
            var_distro = tmp_assign_source_8;
            Py_INCREF( var_distro );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_group );
        tmp_compexpr_left_1 = par_group;
        CHECK_OBJECT( var_config );
        tmp_compexpr_right_1 = var_config;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 237;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_config );
            tmp_subscribed_name_1 = var_config;
            CHECK_OBJECT( par_group );
            tmp_subscript_name_1 = par_group;
            tmp_called_instance_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 238;
            tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = tmp_for_loop_2__for_iterator;
                tmp_for_loop_2__for_iterator = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( tmp_for_loop_2__for_iterator );
            tmp_next_source_2 = tmp_for_loop_2__for_iterator;
            tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooo";
                    exception_lineno = 238;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = tmp_for_loop_2__iter_value;
                tmp_for_loop_2__iter_value = tmp_assign_source_10;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_iter_arg_4;
            CHECK_OBJECT( tmp_for_loop_2__iter_value );
            tmp_iter_arg_4 = tmp_for_loop_2__iter_value;
            tmp_assign_source_11 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__source_iter;
                tmp_tuple_unpack_2__source_iter = tmp_assign_source_11;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
            if ( tmp_assign_source_12 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooo";
                exception_lineno = 238;
                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_1;
                tmp_tuple_unpack_2__element_1 = tmp_assign_source_12;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
            if ( tmp_assign_source_13 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooo";
                exception_lineno = 238;
                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_2;
                tmp_tuple_unpack_2__element_2 = tmp_assign_source_13;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooo";
                        exception_lineno = 238;
                        goto try_except_handler_7;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooo";
                exception_lineno = 238;
                goto try_except_handler_7;
            }
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_6;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_14;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_14 = tmp_tuple_unpack_2__element_1;
            {
                PyObject *old = var_name;
                var_name = tmp_assign_source_14;
                Py_INCREF( var_name );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_15;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_15 = tmp_tuple_unpack_2__element_2;
            {
                PyObject *old = var_epstr;
                var_epstr = tmp_assign_source_15;
                Py_INCREF( var_epstr );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        // Tried code:
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BadEntryPoint );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BadEntryPoint" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }

            tmp_called_instance_2 = tmp_mvar_value_2;
            frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 239;
            tmp_assign_source_16 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_err_to_warnings );
            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            {
                PyObject *old = tmp_with_1__source;
                tmp_with_1__source = tmp_assign_source_16;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_1 = tmp_with_1__source;
            tmp_called_name_2 = LOOKUP_SPECIAL( tmp_source_name_1, const_str_plain___enter__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 239;
            tmp_assign_source_17 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_2 );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            {
                PyObject *old = tmp_with_1__enter;
                tmp_with_1__enter = tmp_assign_source_17;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_2 = tmp_with_1__source;
            tmp_assign_source_18 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___exit__ );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            {
                PyObject *old = tmp_with_1__exit;
                tmp_with_1__exit = tmp_assign_source_18;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_19;
            tmp_assign_source_19 = Py_True;
            {
                PyObject *old = tmp_with_1__indicator;
                tmp_with_1__indicator = tmp_assign_source_19;
                Py_INCREF( tmp_with_1__indicator );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( var_result );
            tmp_source_name_3 = var_result;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_append );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 240;
                type_description_1 = "ooooooo";
                goto try_except_handler_10;
            }
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_EntryPoint );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EntryPoint );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EntryPoint" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 240;
                type_description_1 = "ooooooo";
                goto try_except_handler_10;
            }

            tmp_called_instance_3 = tmp_mvar_value_3;
            CHECK_OBJECT( var_epstr );
            tmp_args_element_name_2 = var_epstr;
            CHECK_OBJECT( var_name );
            tmp_args_element_name_3 = var_name;
            CHECK_OBJECT( var_distro );
            tmp_args_element_name_4 = var_distro;
            frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 240;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_3, const_str_plain_from_string, call_args );
            }

            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 240;
                type_description_1 = "ooooooo";
                goto try_except_handler_10;
            }
            frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 240;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 240;
                type_description_1 = "ooooooo";
                goto try_except_handler_10;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_5 == NULL )
        {
            exception_keeper_tb_5 = MAKE_TRACEBACK( frame_4a061c40103077024de98e7857e83c88, exception_keeper_lineno_5 );
        }
        else if ( exception_keeper_lineno_5 != 0 )
        {
            exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_4a061c40103077024de98e7857e83c88, exception_keeper_lineno_5 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
        PyException_SetTraceback( exception_keeper_value_5, (PyObject *)exception_keeper_tb_5 );
        PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_11;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_20;
                tmp_assign_source_20 = Py_False;
                {
                    PyObject *old = tmp_with_1__indicator;
                    assert( old != NULL );
                    tmp_with_1__indicator = tmp_assign_source_20;
                    Py_INCREF( tmp_with_1__indicator );
                    Py_DECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_called_name_4;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_args_element_name_6;
                PyObject *tmp_args_element_name_7;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_4 = tmp_with_1__exit;
                tmp_args_element_name_5 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_6 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_7 = EXC_TRACEBACK(PyThreadState_GET());
                frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 240;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
                    tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
                }

                if ( tmp_operand_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 240;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_11;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                Py_DECREF( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 240;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_11;
                }
                tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 240;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_4a061c40103077024de98e7857e83c88->m_frame) frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooo";
                goto try_except_handler_11;
                branch_no_3:;
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 239;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_4a061c40103077024de98e7857e83c88->m_frame) frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooo";
            goto try_except_handler_11;
            branch_end_2:;
        }
        goto try_end_6;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_9;
        // End of try:
        try_end_6:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_5;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints$$$function_15_get_group_all );
        return NULL;
        // End of try:
        try_end_5:;
        goto try_end_7;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_4;
            nuitka_bool tmp_compexpr_left_3;
            nuitka_bool tmp_compexpr_right_3;
            int tmp_truth_name_1;
            CHECK_OBJECT( tmp_with_1__indicator );
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_with_1__indicator );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_7 );
                Py_XDECREF( exception_keeper_value_7 );
                Py_XDECREF( exception_keeper_tb_7 );

                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            tmp_compexpr_left_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
            tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_name_5;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_5 = tmp_with_1__exit;
                frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 240;
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_7 );
                    Py_XDECREF( exception_keeper_value_7 );
                    Py_XDECREF( exception_keeper_tb_7 );

                    exception_lineno = 240;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_8;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_4:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        try_end_7:;
        {
            nuitka_bool tmp_condition_result_5;
            nuitka_bool tmp_compexpr_left_4;
            nuitka_bool tmp_compexpr_right_4;
            int tmp_truth_name_2;
            CHECK_OBJECT( tmp_with_1__indicator );
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_with_1__indicator );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 239;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            tmp_compexpr_left_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
            tmp_condition_result_5 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_called_name_6;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_6 = tmp_with_1__exit;
                frame_4a061c40103077024de98e7857e83c88->m_frame.f_lineno = 240;
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 240;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_8;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            branch_no_5:;
        }
        goto try_end_8;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        Py_XDECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        Py_XDECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        Py_XDECREF( tmp_with_1__indicator );
        tmp_with_1__indicator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto try_except_handler_5;
        // End of try:
        try_end_8:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        Py_XDECREF( tmp_with_1__indicator );
        tmp_with_1__indicator = NULL;

        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
        }
        goto loop_start_2;
        loop_end_2:;
        goto try_end_9;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_2__iter_value );
        tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
        Py_DECREF( tmp_for_loop_2__for_iterator );
        tmp_for_loop_2__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_2;
        // End of try:
        try_end_9:;
        Py_XDECREF( tmp_for_loop_2__iter_value );
        tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
        Py_DECREF( tmp_for_loop_2__for_iterator );
        tmp_for_loop_2__for_iterator = NULL;

        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_10;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4a061c40103077024de98e7857e83c88 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4a061c40103077024de98e7857e83c88 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4a061c40103077024de98e7857e83c88, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4a061c40103077024de98e7857e83c88->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4a061c40103077024de98e7857e83c88, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4a061c40103077024de98e7857e83c88,
        type_description_1,
        par_group,
        par_path,
        var_result,
        var_config,
        var_distro,
        var_name,
        var_epstr
    );


    // Release cached frame.
    if ( frame_4a061c40103077024de98e7857e83c88 == cache_frame_4a061c40103077024de98e7857e83c88 )
    {
        Py_DECREF( frame_4a061c40103077024de98e7857e83c88 );
    }
    cache_frame_4a061c40103077024de98e7857e83c88 = NULL;

    assertFrameObject( frame_4a061c40103077024de98e7857e83c88 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_15_get_group_all );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_config );
    var_config = NULL;

    Py_XDECREF( var_distro );
    var_distro = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_epstr );
    var_epstr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_group );
    Py_DECREF( par_group );
    par_group = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_config );
    var_config = NULL;

    Py_XDECREF( var_distro );
    var_distro = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_epstr );
    var_epstr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( entrypoints$$$function_15_get_group_all );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_10___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_10___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_141adb9d2f35e3281e60fef8ddb3351b,
#endif
        codeobj_0a321b8033564d2fe34617ef9239c81d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_11___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_11___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_e6aaf1b4360174938b1f474f888a17f2,
#endif
        codeobj_850dd1775b531284ea28a2a70765d5b8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_12_iter_files_distros( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_12_iter_files_distros,
        const_str_plain_iter_files_distros,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a94a23ab97e50c47150f791064d420f0,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_13_get_single( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_13_get_single,
        const_str_plain_get_single,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_af54eb11dcf9f66626745aa1a8c8d702,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        const_str_digest_5d9095b13ac72d2f3542fe60a884dc5d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_14_get_group_named( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_14_get_group_named,
        const_str_plain_get_group_named,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2a04c1a76f6c9dbe155a394f7cad8d67,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        const_str_digest_1dd091117a5c21432bcdfa2e19ac0ff5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_15_get_group_all( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_15_get_group_all,
        const_str_plain_get_group_all,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4a061c40103077024de98e7857e83c88,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        const_str_digest_f962ccd29b65c27c6e2c4ebf97c53b05,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_1___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_7afeaed30035fa921fb1215f98240ee1,
#endif
        codeobj_1e964e02c56f2728b2d707b058571160,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_2___str__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_2___str__,
        const_str_plain___str__,
#if PYTHON_VERSION >= 300
        const_str_digest_08b412689f50806fa2820d12516efe5f,
#endif
        codeobj_f794e5114fb011beb590d1db1297a5f7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_3_err_to_warnings(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_3_err_to_warnings,
        const_str_plain_err_to_warnings,
#if PYTHON_VERSION >= 300
        const_str_digest_1cb0ef4017f52ef7e9ba307f4c246aaf,
#endif
        codeobj_54cc5d1e7372367103d16d27ea439804,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_4___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_4___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_44d4a8d3065c4829e1a315cb8c6ac53d,
#endif
        codeobj_1d0b1fe1ef1f55e9071542dd0f6143ab,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_5___str__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_5___str__,
        const_str_plain___str__,
#if PYTHON_VERSION >= 300
        const_str_digest_13ed378b7410e21fad7ecf05fa88eca2,
#endif
        codeobj_77991c516de63c615a30404b9fdabd67,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_6___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_6___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_c00ac55f660fa9d6bd94ce2aa4784c8c,
#endif
        codeobj_5af37954dc4d53f99cea1ad05cba7b68,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_7___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_7___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_55877b467cad3bdfff753865167c7968,
#endif
        codeobj_d6468e9d98fc5d43d5f2b2881d067ba8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_8_load(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_8_load,
        const_str_plain_load,
#if PYTHON_VERSION >= 300
        const_str_digest_d83b95a33b90ed35d20b5d2e756e19a5,
#endif
        codeobj_062bc9fd8a98ac2c90d2e9d78e82a878,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        const_str_digest_34dfa82f78216e205aea03d17bdcee2d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_entrypoints$$$function_9_from_string( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_entrypoints$$$function_9_from_string,
        const_str_plain_from_string,
#if PYTHON_VERSION >= 300
        const_str_digest_ef8b11e24a7314c2dcc37d918c6c842a,
#endif
        codeobj_2e933716041bab6c06a23de4e15ea1d6,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_entrypoints,
        const_str_digest_c8c189412631363fa7dd7a818222a9c2,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_entrypoints =
{
    PyModuleDef_HEAD_INIT,
    "entrypoints",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(entrypoints)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(entrypoints)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_entrypoints );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("entrypoints: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("entrypoints: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("entrypoints: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initentrypoints" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_entrypoints = Py_InitModule4(
        "entrypoints",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_entrypoints = PyModule_Create( &mdef_entrypoints );
#endif

    moduledict_entrypoints = MODULE_DICT( module_entrypoints );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_entrypoints,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_entrypoints,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_entrypoints,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_entrypoints,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_entrypoints );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_plain_entrypoints, module_entrypoints );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *outline_4_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_class_creation_5__bases = NULL;
    PyObject *tmp_class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_class_creation_5__metaclass = NULL;
    PyObject *tmp_class_creation_5__prepared = NULL;
    struct Nuitka_FrameObject *frame_e7a21653f455668fe336842997db58a3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_entrypoints_36 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_3d06b94c70f17ac3e98edad0b759c663_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3d06b94c70f17ac3e98edad0b759c663_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *locals_entrypoints_53 = NULL;
    struct Nuitka_FrameObject *frame_69a5ec53ca0712a2e1bccfd0310ed769_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_69a5ec53ca0712a2e1bccfd0310ed769_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *locals_entrypoints_63 = NULL;
    struct Nuitka_FrameObject *frame_a63ac1107c8d65dae34066360d113e92_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_a63ac1107c8d65dae34066360d113e92_4 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *locals_entrypoints_67 = NULL;
    struct Nuitka_FrameObject *frame_22cae30e30b61f966af73020fd42c7d3_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_22cae30e30b61f966af73020fd42c7d3_5 = NULL;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *locals_entrypoints_108 = NULL;
    struct Nuitka_FrameObject *frame_f91d06a3ccd6785fe941a7059cd2aa88_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    static struct Nuitka_FrameObject *cache_frame_f91d06a3ccd6785fe941a7059cd2aa88_6 = NULL;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_7eff1490f60c741164bcb7f49a3a07cf;
        UPDATE_STRING_DICT0( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_e7a21653f455668fe336842997db58a3 = MAKE_MODULE_FRAME( codeobj_e7a21653f455668fe336842997db58a3, module_entrypoints );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_e7a21653f455668fe336842997db58a3 );
    assert( Py_REFCNT( frame_e7a21653f455668fe336842997db58a3 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_contextlib;
        tmp_globals_name_1 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_contextmanager_tuple;
        tmp_level_name_1 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 5;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_contextmanager );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_contextmanager, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_glob;
        tmp_globals_name_2 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 6;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_glob, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_importlib;
        tmp_globals_name_3 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_import_module_tuple;
        tmp_level_name_3 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 7;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_import_module );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_import_module, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_io;
        tmp_globals_name_4 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 8;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_io, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_itertools;
        tmp_globals_name_5 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 9;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        assert( !(tmp_assign_source_8 == NULL) );
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_itertools, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_e399ba4554180f37de594a6743234f17;
        tmp_globals_name_6 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 10;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_path );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_osp, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_re;
        tmp_globals_name_7 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = Py_None;
        tmp_level_name_7 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 11;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_sys;
        tmp_globals_name_8 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = Py_None;
        tmp_level_name_8 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 12;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        assert( !(tmp_assign_source_11 == NULL) );
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_warnings;
        tmp_globals_name_9 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = Py_None;
        tmp_level_name_9 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 13;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_warnings, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_plain_zipfile;
        tmp_globals_name_10 = (PyObject *)moduledict_entrypoints;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = Py_None;
        tmp_level_name_10 = const_int_0;
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 14;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_zipfile, tmp_assign_source_13 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version_info );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_3;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_name_name_11;
            PyObject *tmp_globals_name_11;
            PyObject *tmp_locals_name_11;
            PyObject *tmp_fromlist_name_11;
            PyObject *tmp_level_name_11;
            tmp_name_name_11 = const_str_plain_configparser;
            tmp_globals_name_11 = (PyObject *)moduledict_entrypoints;
            tmp_locals_name_11 = Py_None;
            tmp_fromlist_name_11 = Py_None;
            tmp_level_name_11 = const_int_0;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 17;
            tmp_assign_source_14 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
            if ( tmp_assign_source_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_configparser, tmp_assign_source_14 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_import_name_from_4;
            PyObject *tmp_name_name_12;
            PyObject *tmp_globals_name_12;
            PyObject *tmp_locals_name_12;
            PyObject *tmp_fromlist_name_12;
            PyObject *tmp_level_name_12;
            tmp_name_name_12 = const_str_plain_backports;
            tmp_globals_name_12 = (PyObject *)moduledict_entrypoints;
            tmp_locals_name_12 = Py_None;
            tmp_fromlist_name_12 = const_tuple_str_plain_configparser_tuple;
            tmp_level_name_12 = const_int_0;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 19;
            tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
            if ( tmp_import_name_from_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 19;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_configparser );
            Py_DECREF( tmp_import_name_from_4 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 19;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_configparser, tmp_assign_source_15 );
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 21;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_compile );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_digest_16e72aad35527e8bf9522820da57d4bc;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_5;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_VERBOSE );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 21;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_entry_point_pattern, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 29;

            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_6;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_compile );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = const_str_digest_bb1038cd44aa2b09e793965c82bc4a7d;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_7;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_VERBOSE );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 29;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_file_in_zip_pattern, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = const_str_digest_13128b1b0d1871349d3e6b743bd02a70;
        UPDATE_STRING_DICT0( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_18 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_Exception_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_19 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_19;
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_20;
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_2 = tmp_class_creation_1__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_21 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_21;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_1;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_6 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_6, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_7 = tmp_class_creation_1__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_1;
            }
            tmp_tuple_element_1 = const_str_plain_BadEntryPoint;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 36;
            tmp_assign_source_22 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_1;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_22;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_8 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_8, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_1;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_9;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 36;

                    goto try_except_handler_1;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_9 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_9 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_9 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 36;

                    goto try_except_handler_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 36;

                    goto try_except_handler_1;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 36;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_1;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_23;
            tmp_assign_source_23 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_23;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_24;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_entrypoints_36 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_entrypoints;
        tmp_res = PyObject_SetItem( locals_entrypoints_36, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_digest_40cf859baa3235ea630141f01acceeb8;
        tmp_res = PyObject_SetItem( locals_entrypoints_36, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_plain_BadEntryPoint;
        tmp_res = PyObject_SetItem( locals_entrypoints_36, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3d06b94c70f17ac3e98edad0b759c663_2, codeobj_3d06b94c70f17ac3e98edad0b759c663, module_entrypoints, sizeof(void *) );
        frame_3d06b94c70f17ac3e98edad0b759c663_2 = cache_frame_3d06b94c70f17ac3e98edad0b759c663_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3d06b94c70f17ac3e98edad0b759c663_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3d06b94c70f17ac3e98edad0b759c663_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_1___init__(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_36, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_2___str__(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_36, const_str_plain___str__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_staticmethod_arg_1;
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_args_element_name_7;
            tmp_res = MAPPING_HAS_ITEM( locals_entrypoints_36, const_str_plain_staticmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_4 = PyObject_GetItem( locals_entrypoints_36, const_str_plain_staticmethod );

            if ( tmp_called_name_4 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "staticmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_called_name_5 = PyObject_GetItem( locals_entrypoints_36, const_str_plain_contextmanager );

            if ( tmp_called_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_contextmanager );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_contextmanager );
                }

                if ( tmp_mvar_value_8 == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "contextmanager" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 46;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_5 = tmp_mvar_value_8;
                Py_INCREF( tmp_called_name_5 );
                }
            }

            tmp_args_element_name_6 = MAKE_FUNCTION_entrypoints$$$function_3_err_to_warnings(  );



            frame_3d06b94c70f17ac3e98edad0b759c663_2->m_frame.f_lineno = 46;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 46;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            frame_3d06b94c70f17ac3e98edad0b759c663_2->m_frame.f_lineno = 45;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_called_name_6 = PyObject_GetItem( locals_entrypoints_36, const_str_plain_contextmanager );

            if ( tmp_called_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_contextmanager );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_contextmanager );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "contextmanager" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 46;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_6 = tmp_mvar_value_9;
                Py_INCREF( tmp_called_name_6 );
                }
            }

            tmp_args_element_name_7 = MAKE_FUNCTION_entrypoints$$$function_3_err_to_warnings(  );



            frame_3d06b94c70f17ac3e98edad0b759c663_2->m_frame.f_lineno = 46;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_staticmethod_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_staticmethod_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dictset_value = BUILTIN_STATICMETHOD( tmp_staticmethod_arg_1 );
            Py_DECREF( tmp_staticmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_entrypoints_36, const_str_plain_err_to_warnings, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3d06b94c70f17ac3e98edad0b759c663_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3d06b94c70f17ac3e98edad0b759c663_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3d06b94c70f17ac3e98edad0b759c663_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3d06b94c70f17ac3e98edad0b759c663_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3d06b94c70f17ac3e98edad0b759c663_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3d06b94c70f17ac3e98edad0b759c663_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_3d06b94c70f17ac3e98edad0b759c663_2 == cache_frame_3d06b94c70f17ac3e98edad0b759c663_2 )
        {
            Py_DECREF( frame_3d06b94c70f17ac3e98edad0b759c663_2 );
        }
        cache_frame_3d06b94c70f17ac3e98edad0b759c663_2 = NULL;

        assertFrameObject( frame_3d06b94c70f17ac3e98edad0b759c663_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_3;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_2 = tmp_class_creation_1__bases;
            tmp_compexpr_right_2 = const_tuple_type_Exception_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_3;
            }
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_dictset_value = const_tuple_type_Exception_tuple;
            tmp_res = PyObject_SetItem( locals_entrypoints_36, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_3;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_7 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_BadEntryPoint;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_entrypoints_36;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 36;
            tmp_assign_source_25 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_3;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_25;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_24 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_24 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        Py_DECREF( locals_entrypoints_36 );
        locals_entrypoints_36 = NULL;
        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_entrypoints_36 );
        locals_entrypoints_36 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 36;
        goto try_except_handler_1;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_BadEntryPoint, tmp_assign_source_24 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_dircall_arg1_2;
        tmp_dircall_arg1_2 = const_tuple_type_Exception_tuple;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_26 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_26;
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_27;
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_10;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        tmp_condition_result_10 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_3 = tmp_class_creation_2__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_5:;
        condexpr_end_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_28 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_28;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_4;
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_10 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_10, const_str_plain___prepare__ );
        tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_called_name_8;
            PyObject *tmp_source_name_11;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_11 = tmp_class_creation_2__metaclass;
            tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain___prepare__ );
            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;

                goto try_except_handler_4;
            }
            tmp_tuple_element_4 = const_str_plain_NoSuchEntryPoint;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_4 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 53;
            tmp_assign_source_29 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_29;
        }
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_12;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_12 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_12, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;

                goto try_except_handler_4;
            }
            tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_5;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_13;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_5 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_4;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_5 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_13 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_13 == NULL) );
                tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_13 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 53;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_5 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 53;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 53;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_8:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_assign_source_30;
            tmp_assign_source_30 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_30;
        }
        branch_end_7:;
    }
    {
        PyObject *tmp_assign_source_31;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_entrypoints_53 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_entrypoints;
        tmp_res = PyObject_SetItem( locals_entrypoints_53, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_digest_964c9d3660d573692b72c24d5628626e;
        tmp_res = PyObject_SetItem( locals_entrypoints_53, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain_NoSuchEntryPoint;
        tmp_res = PyObject_SetItem( locals_entrypoints_53, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_69a5ec53ca0712a2e1bccfd0310ed769_3, codeobj_69a5ec53ca0712a2e1bccfd0310ed769, module_entrypoints, sizeof(void *) );
        frame_69a5ec53ca0712a2e1bccfd0310ed769_3 = cache_frame_69a5ec53ca0712a2e1bccfd0310ed769_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_69a5ec53ca0712a2e1bccfd0310ed769_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_69a5ec53ca0712a2e1bccfd0310ed769_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_4___init__(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_53, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_5___str__(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_53, const_str_plain___str__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_69a5ec53ca0712a2e1bccfd0310ed769_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_69a5ec53ca0712a2e1bccfd0310ed769_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_69a5ec53ca0712a2e1bccfd0310ed769_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_69a5ec53ca0712a2e1bccfd0310ed769_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_69a5ec53ca0712a2e1bccfd0310ed769_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_69a5ec53ca0712a2e1bccfd0310ed769_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_69a5ec53ca0712a2e1bccfd0310ed769_3 == cache_frame_69a5ec53ca0712a2e1bccfd0310ed769_3 )
        {
            Py_DECREF( frame_69a5ec53ca0712a2e1bccfd0310ed769_3 );
        }
        cache_frame_69a5ec53ca0712a2e1bccfd0310ed769_3 = NULL;

        assertFrameObject( frame_69a5ec53ca0712a2e1bccfd0310ed769_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_6;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_14;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_3 = tmp_class_creation_2__bases;
            tmp_compexpr_right_3 = const_tuple_type_Exception_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;

                goto try_except_handler_6;
            }
            tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            tmp_dictset_value = const_tuple_type_Exception_tuple;
            tmp_res = PyObject_SetItem( locals_entrypoints_53, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;

                goto try_except_handler_6;
            }
            branch_no_9:;
        }
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_called_name_9;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_9 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_6 = const_str_plain_NoSuchEntryPoint;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_6 );
            tmp_tuple_element_6 = locals_entrypoints_53;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 53;
            tmp_assign_source_32 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;

                goto try_except_handler_6;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_32;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_31 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_31 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_entrypoints_53 );
        locals_entrypoints_53 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_entrypoints_53 );
        locals_entrypoints_53 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 53;
        goto try_except_handler_4;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_NoSuchEntryPoint, tmp_assign_source_31 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_tuple_element_7;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_configparser );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_configparser );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "configparser" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto try_except_handler_7;
        }

        tmp_source_name_14 = tmp_mvar_value_10;
        tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_ConfigParser );
        if ( tmp_tuple_element_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        tmp_assign_source_33 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_33, 0, tmp_tuple_element_7 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_34 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        tmp_assign_source_35 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_35;
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_16;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        tmp_condition_result_16 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_4 = tmp_class_creation_3__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_7:;
        condexpr_end_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_36 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_36;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_7;
        }
        branch_no_10:;
    }
    {
        nuitka_bool tmp_condition_result_18;
        PyObject *tmp_source_name_15;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_15 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___prepare__ );
        tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_called_name_10;
            PyObject *tmp_source_name_16;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_16 = tmp_class_creation_3__metaclass;
            tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___prepare__ );
            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_7;
            }
            tmp_tuple_element_8 = const_str_plain_CaseSensitiveConfigParser;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_8 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 63;
            tmp_assign_source_37 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_7;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_37;
        }
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_17;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_17 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_17, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_7;
            }
            tmp_condition_result_19 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_9;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_18;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_9 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 63;

                    goto try_except_handler_7;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_9 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_18 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_18 == NULL) );
                tmp_tuple_element_9 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_18 );
                if ( tmp_tuple_element_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 63;

                    goto try_except_handler_7;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_9 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 63;

                    goto try_except_handler_7;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 63;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_7;
            }
            branch_no_12:;
        }
        goto branch_end_11;
        branch_no_11:;
        {
            PyObject *tmp_assign_source_38;
            tmp_assign_source_38 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_38;
        }
        branch_end_11:;
    }
    {
        PyObject *tmp_assign_source_39;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_entrypoints_63 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_entrypoints;
        tmp_res = PyObject_SetItem( locals_entrypoints_63, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_plain_CaseSensitiveConfigParser;
        tmp_res = PyObject_SetItem( locals_entrypoints_63, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto try_except_handler_9;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_a63ac1107c8d65dae34066360d113e92_4, codeobj_a63ac1107c8d65dae34066360d113e92, module_entrypoints, sizeof(void *) );
        frame_a63ac1107c8d65dae34066360d113e92_4 = cache_frame_a63ac1107c8d65dae34066360d113e92_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_a63ac1107c8d65dae34066360d113e92_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_a63ac1107c8d65dae34066360d113e92_4 ) == 2 ); // Frame stack

        // Framed code:
        {
            nuitka_bool tmp_condition_result_20;
            PyObject *tmp_called_name_11;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_staticmethod_arg_2;
            tmp_res = MAPPING_HAS_ITEM( locals_entrypoints_63, const_str_plain_staticmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_20 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_8;
            }
            else
            {
                goto condexpr_false_8;
            }
            condexpr_true_8:;
            tmp_called_name_11 = PyObject_GetItem( locals_entrypoints_63, const_str_plain_staticmethod );

            if ( tmp_called_name_11 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "staticmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_8 = PyObject_GetItem( locals_entrypoints_63, const_str_plain_str );

            if ( tmp_args_element_name_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_args_element_name_8 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_args_element_name_8 );
                }
            }

            frame_a63ac1107c8d65dae34066360d113e92_4->m_frame.f_lineno = 64;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_8;
            condexpr_false_8:;
            tmp_staticmethod_arg_2 = PyObject_GetItem( locals_entrypoints_63, const_str_plain_str );

            if ( tmp_staticmethod_arg_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_staticmethod_arg_2 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_staticmethod_arg_2 );
                }
            }

            tmp_dictset_value = BUILTIN_STATICMETHOD( tmp_staticmethod_arg_2 );
            Py_DECREF( tmp_staticmethod_arg_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_8:;
            tmp_res = PyObject_SetItem( locals_entrypoints_63, const_str_plain_optionxform, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a63ac1107c8d65dae34066360d113e92_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a63ac1107c8d65dae34066360d113e92_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_a63ac1107c8d65dae34066360d113e92_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_a63ac1107c8d65dae34066360d113e92_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_a63ac1107c8d65dae34066360d113e92_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_a63ac1107c8d65dae34066360d113e92_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_a63ac1107c8d65dae34066360d113e92_4 == cache_frame_a63ac1107c8d65dae34066360d113e92_4 )
        {
            Py_DECREF( frame_a63ac1107c8d65dae34066360d113e92_4 );
        }
        cache_frame_a63ac1107c8d65dae34066360d113e92_4 = NULL;

        assertFrameObject( frame_a63ac1107c8d65dae34066360d113e92_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_9;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_21;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_4 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_4 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_9;
            }
            tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_13;
            }
            else
            {
                goto branch_no_13;
            }
            branch_yes_13:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_entrypoints_63, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_9;
            }
            branch_no_13:;
        }
        {
            PyObject *tmp_assign_source_40;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_12 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_10 = const_str_plain_CaseSensitiveConfigParser;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_10 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_10 );
            tmp_tuple_element_10 = locals_entrypoints_63;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 63;
            tmp_assign_source_40 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_40 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;

                goto try_except_handler_9;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_40;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_39 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_39 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        Py_DECREF( locals_entrypoints_63 );
        locals_entrypoints_63 = NULL;
        goto try_return_handler_8;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_entrypoints_63 );
        locals_entrypoints_63 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 63;
        goto try_except_handler_7;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_CaseSensitiveConfigParser, tmp_assign_source_39 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_dircall_arg1_4;
        tmp_dircall_arg1_4 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
            tmp_assign_source_41 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_41;
    }
    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_42;
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_metaclass_name_4;
        nuitka_bool tmp_condition_result_22;
        PyObject *tmp_key_name_10;
        PyObject *tmp_dict_name_10;
        PyObject *tmp_dict_name_11;
        PyObject *tmp_key_name_11;
        nuitka_bool tmp_condition_result_23;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_7;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_bases_name_4;
        tmp_key_name_10 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_9;
        }
        else
        {
            goto condexpr_false_9;
        }
        condexpr_true_9:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_11 = const_str_plain_metaclass;
        tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        goto condexpr_end_9;
        condexpr_false_9:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        tmp_condition_result_23 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_10;
        }
        else
        {
            goto condexpr_false_10;
        }
        condexpr_true_10:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_5 = tmp_class_creation_4__bases;
        tmp_subscript_name_5 = const_int_0;
        tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
        if ( tmp_type_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
        Py_DECREF( tmp_type_arg_7 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        goto condexpr_end_10;
        condexpr_false_10:;
        tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_4 );
        condexpr_end_10:;
        condexpr_end_9:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_4 = tmp_class_creation_4__bases;
        tmp_assign_source_43 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
        Py_DECREF( tmp_metaclass_name_4 );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_43;
    }
    {
        nuitka_bool tmp_condition_result_24;
        PyObject *tmp_key_name_12;
        PyObject *tmp_dict_name_12;
        tmp_key_name_12 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_10;
        }
        branch_no_14:;
    }
    {
        nuitka_bool tmp_condition_result_25;
        PyObject *tmp_source_name_19;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_19 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_19, const_str_plain___prepare__ );
        tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_15;
        }
        else
        {
            goto branch_no_15;
        }
        branch_yes_15:;
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_13;
            PyObject *tmp_source_name_20;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_20 = tmp_class_creation_4__metaclass;
            tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain___prepare__ );
            if ( tmp_called_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;

                goto try_except_handler_10;
            }
            tmp_tuple_element_11 = const_str_plain_EntryPoint;
            tmp_args_name_7 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_11 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_4__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 67;
            tmp_assign_source_44 = CALL_FUNCTION( tmp_called_name_13, tmp_args_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;

                goto try_except_handler_10;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_44;
        }
        {
            nuitka_bool tmp_condition_result_26;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_21;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_21 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_21, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;

                goto try_except_handler_10;
            }
            tmp_condition_result_26 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_4;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_tuple_element_12;
                PyObject *tmp_getattr_target_4;
                PyObject *tmp_getattr_attr_4;
                PyObject *tmp_getattr_default_4;
                PyObject *tmp_source_name_22;
                PyObject *tmp_type_arg_8;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_4 = const_str_plain___name__;
                tmp_getattr_default_4 = const_str_angle_metaclass;
                tmp_tuple_element_12 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                if ( tmp_tuple_element_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 67;

                    goto try_except_handler_10;
                }
                tmp_right_name_4 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_12 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_8 = tmp_class_creation_4__prepared;
                tmp_source_name_22 = BUILTIN_TYPE1( tmp_type_arg_8 );
                assert( !(tmp_source_name_22 == NULL) );
                tmp_tuple_element_12 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_22 );
                if ( tmp_tuple_element_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_4 );

                    exception_lineno = 67;

                    goto try_except_handler_10;
                }
                PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_12 );
                tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_right_name_4 );
                if ( tmp_raise_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 67;

                    goto try_except_handler_10;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_4;
                exception_lineno = 67;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_10;
            }
            branch_no_16:;
        }
        goto branch_end_15;
        branch_no_15:;
        {
            PyObject *tmp_assign_source_45;
            tmp_assign_source_45 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_45;
        }
        branch_end_15:;
    }
    {
        PyObject *tmp_assign_source_46;
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_4 = tmp_class_creation_4__prepared;
            locals_entrypoints_67 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_entrypoints;
        tmp_res = PyObject_SetItem( locals_entrypoints_67, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_12;
        }
        tmp_dictset_value = const_str_plain_EntryPoint;
        tmp_res = PyObject_SetItem( locals_entrypoints_67, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto try_except_handler_12;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_22cae30e30b61f966af73020fd42c7d3_5, codeobj_22cae30e30b61f966af73020fd42c7d3, module_entrypoints, sizeof(void *) );
        frame_22cae30e30b61f966af73020fd42c7d3_5 = cache_frame_22cae30e30b61f966af73020fd42c7d3_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_22cae30e30b61f966af73020fd42c7d3_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_22cae30e30b61f966af73020fd42c7d3_5 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_6___init__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_entrypoints_67, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_7___repr__(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_67, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_8_load(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_67, const_str_plain_load, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        {
            nuitka_bool tmp_condition_result_27;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_defaults_2;
            PyObject *tmp_classmethod_arg_1;
            PyObject *tmp_defaults_3;
            tmp_res = MAPPING_HAS_ITEM( locals_entrypoints_67, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_condition_result_27 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_11;
            }
            else
            {
                goto condexpr_false_11;
            }
            condexpr_true_11:;
            tmp_called_name_14 = PyObject_GetItem( locals_entrypoints_67, const_str_plain_classmethod );

            if ( tmp_called_name_14 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }

            if ( tmp_called_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_defaults_2 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_args_element_name_9 = MAKE_FUNCTION_entrypoints$$$function_9_from_string( tmp_defaults_2 );



            frame_22cae30e30b61f966af73020fd42c7d3_5->m_frame.f_lineno = 89;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            goto condexpr_end_11;
            condexpr_false_11:;
            tmp_defaults_3 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_classmethod_arg_1 = MAKE_FUNCTION_entrypoints$$$function_9_from_string( tmp_defaults_3 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
            Py_DECREF( tmp_classmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            condexpr_end_11:;
            tmp_res = PyObject_SetItem( locals_entrypoints_67, const_str_plain_from_string, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_22cae30e30b61f966af73020fd42c7d3_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_22cae30e30b61f966af73020fd42c7d3_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_22cae30e30b61f966af73020fd42c7d3_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_22cae30e30b61f966af73020fd42c7d3_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_22cae30e30b61f966af73020fd42c7d3_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_22cae30e30b61f966af73020fd42c7d3_5,
            type_description_2,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_22cae30e30b61f966af73020fd42c7d3_5 == cache_frame_22cae30e30b61f966af73020fd42c7d3_5 )
        {
            Py_DECREF( frame_22cae30e30b61f966af73020fd42c7d3_5 );
        }
        cache_frame_22cae30e30b61f966af73020fd42c7d3_5 = NULL;

        assertFrameObject( frame_22cae30e30b61f966af73020fd42c7d3_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;

        goto try_except_handler_12;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_28;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_5 = tmp_class_creation_4__bases;
            tmp_compexpr_right_5 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;

                goto try_except_handler_12;
            }
            tmp_condition_result_28 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_17;
            }
            else
            {
                goto branch_no_17;
            }
            branch_yes_17:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_entrypoints_67, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;

                goto try_except_handler_12;
            }
            branch_no_17:;
        }
        {
            PyObject *tmp_assign_source_47;
            PyObject *tmp_called_name_15;
            PyObject *tmp_args_name_8;
            PyObject *tmp_tuple_element_13;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_15 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_13 = const_str_plain_EntryPoint;
            tmp_args_name_8 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_13 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_13 );
            tmp_tuple_element_13 = locals_entrypoints_67;
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 67;
            tmp_assign_source_47 = CALL_FUNCTION( tmp_called_name_15, tmp_args_name_8, tmp_kw_name_8 );
            Py_DECREF( tmp_args_name_8 );
            if ( tmp_assign_source_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;

                goto try_except_handler_12;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_47;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_assign_source_46 = outline_3_var___class__;
        Py_INCREF( tmp_assign_source_46 );
        goto try_return_handler_12;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_12:;
        Py_DECREF( locals_entrypoints_67 );
        locals_entrypoints_67 = NULL;
        goto try_return_handler_11;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_entrypoints_67 );
        locals_entrypoints_67 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto try_except_handler_11;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 67;
        goto try_except_handler_10;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_EntryPoint, tmp_assign_source_46 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_dircall_arg1_5;
        tmp_dircall_arg1_5 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_5 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
            tmp_assign_source_48 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        assert( tmp_class_creation_5__bases == NULL );
        tmp_class_creation_5__bases = tmp_assign_source_48;
    }
    {
        PyObject *tmp_assign_source_49;
        tmp_assign_source_49 = PyDict_New();
        assert( tmp_class_creation_5__class_decl_dict == NULL );
        tmp_class_creation_5__class_decl_dict = tmp_assign_source_49;
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_metaclass_name_5;
        nuitka_bool tmp_condition_result_29;
        PyObject *tmp_key_name_13;
        PyObject *tmp_dict_name_13;
        PyObject *tmp_dict_name_14;
        PyObject *tmp_key_name_14;
        nuitka_bool tmp_condition_result_30;
        int tmp_truth_name_5;
        PyObject *tmp_type_arg_9;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_bases_name_5;
        tmp_key_name_13 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_13 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        tmp_condition_result_29 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_12;
        }
        else
        {
            goto condexpr_false_12;
        }
        condexpr_true_12:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_14 = tmp_class_creation_5__class_decl_dict;
        tmp_key_name_14 = const_str_plain_metaclass;
        tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        goto condexpr_end_12;
        condexpr_false_12:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_class_creation_5__bases );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        tmp_condition_result_30 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_13;
        }
        else
        {
            goto condexpr_false_13;
        }
        condexpr_true_13:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_subscribed_name_6 = tmp_class_creation_5__bases;
        tmp_subscript_name_6 = const_int_0;
        tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
        if ( tmp_type_arg_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
        Py_DECREF( tmp_type_arg_9 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        goto condexpr_end_13;
        condexpr_false_13:;
        tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_5 );
        condexpr_end_13:;
        condexpr_end_12:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_bases_name_5 = tmp_class_creation_5__bases;
        tmp_assign_source_50 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
        Py_DECREF( tmp_metaclass_name_5 );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        assert( tmp_class_creation_5__metaclass == NULL );
        tmp_class_creation_5__metaclass = tmp_assign_source_50;
    }
    {
        nuitka_bool tmp_condition_result_31;
        PyObject *tmp_key_name_15;
        PyObject *tmp_dict_name_15;
        tmp_key_name_15 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_15 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        tmp_condition_result_31 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_18;
        }
        else
        {
            goto branch_no_18;
        }
        branch_yes_18:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_5__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_13;
        }
        branch_no_18:;
    }
    {
        nuitka_bool tmp_condition_result_32;
        PyObject *tmp_source_name_23;
        CHECK_OBJECT( tmp_class_creation_5__metaclass );
        tmp_source_name_23 = tmp_class_creation_5__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_23, const_str_plain___prepare__ );
        tmp_condition_result_32 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_19;
        }
        else
        {
            goto branch_no_19;
        }
        branch_yes_19:;
        {
            PyObject *tmp_assign_source_51;
            PyObject *tmp_called_name_16;
            PyObject *tmp_source_name_24;
            PyObject *tmp_args_name_9;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_kw_name_9;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_source_name_24 = tmp_class_creation_5__metaclass;
            tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain___prepare__ );
            if ( tmp_called_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;

                goto try_except_handler_13;
            }
            tmp_tuple_element_14 = const_str_plain_Distribution;
            tmp_args_name_9 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_14 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_9 = tmp_class_creation_5__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 108;
            tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_9, tmp_kw_name_9 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_args_name_9 );
            if ( tmp_assign_source_51 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;

                goto try_except_handler_13;
            }
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_51;
        }
        {
            nuitka_bool tmp_condition_result_33;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_source_name_25;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_source_name_25 = tmp_class_creation_5__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_25, const_str_plain___getitem__ );
            tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;

                goto try_except_handler_13;
            }
            tmp_condition_result_33 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            {
                PyObject *tmp_raise_type_5;
                PyObject *tmp_raise_value_5;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                PyObject *tmp_tuple_element_15;
                PyObject *tmp_getattr_target_5;
                PyObject *tmp_getattr_attr_5;
                PyObject *tmp_getattr_default_5;
                PyObject *tmp_source_name_26;
                PyObject *tmp_type_arg_10;
                tmp_raise_type_5 = PyExc_TypeError;
                tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_5__metaclass );
                tmp_getattr_target_5 = tmp_class_creation_5__metaclass;
                tmp_getattr_attr_5 = const_str_plain___name__;
                tmp_getattr_default_5 = const_str_angle_metaclass;
                tmp_tuple_element_15 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 108;

                    goto try_except_handler_13;
                }
                tmp_right_name_5 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_15 );
                CHECK_OBJECT( tmp_class_creation_5__prepared );
                tmp_type_arg_10 = tmp_class_creation_5__prepared;
                tmp_source_name_26 = BUILTIN_TYPE1( tmp_type_arg_10 );
                assert( !(tmp_source_name_26 == NULL) );
                tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_26 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_5 );

                    exception_lineno = 108;

                    goto try_except_handler_13;
                }
                PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_15 );
                tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_raise_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 108;

                    goto try_except_handler_13;
                }
                exception_type = tmp_raise_type_5;
                Py_INCREF( tmp_raise_type_5 );
                exception_value = tmp_raise_value_5;
                exception_lineno = 108;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_13;
            }
            branch_no_20:;
        }
        goto branch_end_19;
        branch_no_19:;
        {
            PyObject *tmp_assign_source_52;
            tmp_assign_source_52 = PyDict_New();
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_52;
        }
        branch_end_19:;
    }
    {
        PyObject *tmp_assign_source_53;
        {
            PyObject *tmp_set_locals_5;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_set_locals_5 = tmp_class_creation_5__prepared;
            locals_entrypoints_108 = tmp_set_locals_5;
            Py_INCREF( tmp_set_locals_5 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_entrypoints;
        tmp_res = PyObject_SetItem( locals_entrypoints_108, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_15;
        }
        tmp_dictset_value = const_str_plain_Distribution;
        tmp_res = PyObject_SetItem( locals_entrypoints_108, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;

            goto try_except_handler_15;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_f91d06a3ccd6785fe941a7059cd2aa88_6, codeobj_f91d06a3ccd6785fe941a7059cd2aa88, module_entrypoints, sizeof(void *) );
        frame_f91d06a3ccd6785fe941a7059cd2aa88_6 = cache_frame_f91d06a3ccd6785fe941a7059cd2aa88_6;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_f91d06a3ccd6785fe941a7059cd2aa88_6 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_f91d06a3ccd6785fe941a7059cd2aa88_6 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_10___init__(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_108, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }
        tmp_dictset_value = MAKE_FUNCTION_entrypoints$$$function_11___repr__(  );



        tmp_res = PyObject_SetItem( locals_entrypoints_108, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_f91d06a3ccd6785fe941a7059cd2aa88_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_5;

        frame_exception_exit_6:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_f91d06a3ccd6785fe941a7059cd2aa88_6 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_f91d06a3ccd6785fe941a7059cd2aa88_6, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_f91d06a3ccd6785fe941a7059cd2aa88_6->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_f91d06a3ccd6785fe941a7059cd2aa88_6, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_f91d06a3ccd6785fe941a7059cd2aa88_6,
            type_description_2,
            outline_4_var___class__
        );


        // Release cached frame.
        if ( frame_f91d06a3ccd6785fe941a7059cd2aa88_6 == cache_frame_f91d06a3ccd6785fe941a7059cd2aa88_6 )
        {
            Py_DECREF( frame_f91d06a3ccd6785fe941a7059cd2aa88_6 );
        }
        cache_frame_f91d06a3ccd6785fe941a7059cd2aa88_6 = NULL;

        assertFrameObject( frame_f91d06a3ccd6785fe941a7059cd2aa88_6 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_5;

        frame_no_exception_5:;
        goto skip_nested_handling_5;
        nested_frame_exit_5:;

        goto try_except_handler_15;
        skip_nested_handling_5:;
        {
            nuitka_bool tmp_condition_result_34;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_compexpr_left_6 = tmp_class_creation_5__bases;
            tmp_compexpr_right_6 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;

                goto try_except_handler_15;
            }
            tmp_condition_result_34 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_21;
            }
            else
            {
                goto branch_no_21;
            }
            branch_yes_21:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_entrypoints_108, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;

                goto try_except_handler_15;
            }
            branch_no_21:;
        }
        {
            PyObject *tmp_assign_source_54;
            PyObject *tmp_called_name_17;
            PyObject *tmp_args_name_10;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_kw_name_10;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_called_name_17 = tmp_class_creation_5__metaclass;
            tmp_tuple_element_16 = const_str_plain_Distribution;
            tmp_args_name_10 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_16 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_16 );
            tmp_tuple_element_16 = locals_entrypoints_108;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_10, 2, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_10 = tmp_class_creation_5__class_decl_dict;
            frame_e7a21653f455668fe336842997db58a3->m_frame.f_lineno = 108;
            tmp_assign_source_54 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_10, tmp_kw_name_10 );
            Py_DECREF( tmp_args_name_10 );
            if ( tmp_assign_source_54 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;

                goto try_except_handler_15;
            }
            assert( outline_4_var___class__ == NULL );
            outline_4_var___class__ = tmp_assign_source_54;
        }
        CHECK_OBJECT( outline_4_var___class__ );
        tmp_assign_source_53 = outline_4_var___class__;
        Py_INCREF( tmp_assign_source_53 );
        goto try_return_handler_15;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_15:;
        Py_DECREF( locals_entrypoints_108 );
        locals_entrypoints_108 = NULL;
        goto try_return_handler_14;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_entrypoints_108 );
        locals_entrypoints_108 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto try_except_handler_14;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( entrypoints );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_5:;
        exception_lineno = 108;
        goto try_except_handler_13;
        outline_result_5:;
        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_Distribution, tmp_assign_source_53 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    Py_XDECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7a21653f455668fe336842997db58a3 );
#endif
    popFrameStack();

    assertFrameObject( frame_e7a21653f455668fe336842997db58a3 );

    goto frame_no_exception_6;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7a21653f455668fe336842997db58a3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e7a21653f455668fe336842997db58a3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e7a21653f455668fe336842997db58a3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e7a21653f455668fe336842997db58a3, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases );
    Py_DECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__class_decl_dict );
    Py_DECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__metaclass );
    Py_DECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__prepared );
    Py_DECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_defaults_4;
        tmp_defaults_4 = const_tuple_none_str_plain_first_tuple;
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_55 = MAKE_FUNCTION_entrypoints$$$function_12_iter_files_distros( tmp_defaults_4 );



        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_iter_files_distros, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        PyObject *tmp_defaults_5;
        tmp_defaults_5 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_5 );
        tmp_assign_source_56 = MAKE_FUNCTION_entrypoints$$$function_13_get_single( tmp_defaults_5 );



        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_get_single, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_defaults_6;
        tmp_defaults_6 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_6 );
        tmp_assign_source_57 = MAKE_FUNCTION_entrypoints$$$function_14_get_group_named( tmp_defaults_6 );



        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_get_group_named, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        PyObject *tmp_defaults_7;
        tmp_defaults_7 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_7 );
        tmp_assign_source_58 = MAKE_FUNCTION_entrypoints$$$function_15_get_group_all( tmp_defaults_7 );



        UPDATE_STRING_DICT1( moduledict_entrypoints, (Nuitka_StringObject *)const_str_plain_get_group_all, tmp_assign_source_58 );
    }

    return MOD_RETURN_VALUE( module_entrypoints );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
