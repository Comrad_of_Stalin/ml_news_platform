/* Generated code for Python module 'testpath.asserts'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_testpath$asserts" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_testpath$asserts;
PyDictObject *moduledict_testpath$asserts;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_true_none_tuple;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_Path;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_c926dc77011764aa9b3bb30ed4447290;
static PyObject *const_str_plain_assert_islink;
static PyObject *const_str_plain_assert_not_ispipe;
static PyObject *const_str_digest_e404fd3ebeb2f7e85fde30500347e80f;
static PyObject *const_str_digest_443fa7a6de7f872cfe16146ec8e58033;
static PyObject *const_str_plain_assert_not_issocket;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_9014940713c6f8ed77065e6419645a00;
extern PyObject *const_str_plain_stat;
static PyObject *const_str_digest_fa793fa99a864abb5e2c1d809feff419;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_S_ISDIR;
static PyObject *const_str_digest_baa54e3476b37e3f6f9feb3b112d4f71;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_actual;
static PyObject *const_str_plain_assert_isdir;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_p;
static PyObject *const_str_digest_a7890743df90394f5cf16eb15c18a9e8;
static PyObject *const_str_plain__strpath;
static PyObject *const_str_digest_8bbf0418ccbfc46201e9ee069aa08634;
static PyObject *const_str_digest_cc382abe148c946dbd9d9fdbeb5b4fca;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_39e8c4c28bca45c942a7780371d1fabf_tuple;
static PyObject *const_str_digest_390fbaeacc478762984081940bd22e52;
static PyObject *const_list_6ea49453a896f73b83ca69d0042bbfcd_list;
extern PyObject *const_str_plain_st_mode;
extern PyObject *const_str_plain_expected;
static PyObject *const_str_digest_0568a8936968252e7d9c22b273abb185;
static PyObject *const_str_plain_assert_not_path_exists;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_digest_21d1847ad164e2393145e8409b162cca;
static PyObject *const_str_digest_e28dcdb876ab4c23e5e2969de17f1424;
extern PyObject *const_str_plain___all__;
extern PyObject *const_int_0;
static PyObject *const_tuple_str_plain_path_str_plain_msg_tuple;
extern PyObject *const_str_plain_exists;
extern PyObject *const_str_plain_msg;
static PyObject *const_str_plain_S_ISFIFO;
static PyObject *const_str_digest_25d38d7a3fa7af5db857655a9644ddb5;
extern PyObject *const_str_plain_lstat;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_pathlib2;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_18aafda7c630447c81c12d126fa0d8fc;
static PyObject *const_str_digest_147ac877161e80d5706bb7b024f94046;
static PyObject *const_str_plain_assert_not_isdir;
static PyObject *const_tuple_62f5a673026eba82e95d8057446765b4_tuple;
static PyObject *const_str_digest_248524f094372553000860833941be98;
static PyObject *const_str_plain_assert_isfile;
static PyObject *const_str_plain_assert_not_isfile;
extern PyObject *const_str_plain_type;
static PyObject *const_str_plain_assert_issocket;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_98d764b6aceefa953b27dd46d8fdd7f6;
extern PyObject *const_str_plain_st;
static PyObject *const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple;
static PyObject *const_str_digest_1614d9e6c24f9c31c0e6fc513aeb2664;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_to;
static PyObject *const_str_plain__stat_for_assert;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_digest_7454cc19e49b12b40d8fb24ce406f1d1;
extern PyObject *const_str_plain_S_ISREG;
static PyObject *const_tuple_str_plain_path_str_plain_msg_str_plain_st_tuple;
static PyObject *const_str_plain_assert_ispipe;
static PyObject *const_str_plain_assert_path_exists;
extern PyObject *const_str_plain_target;
static PyObject *const_str_plain_follow_symlinks;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_plain_assert_not_islink;
extern PyObject *const_str_plain_pathlib;
static PyObject *const_str_digest_d083eaa7b694dc4d817e84fa2464e182;
extern PyObject *const_tuple_str_plain_Path_tuple;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_S_ISLNK;
extern PyObject *const_str_plain_readlink;
static PyObject *const_str_digest_41c54cd2205f8bd04d958692f00d178a;
static PyObject *const_str_digest_c6db506f43c3e7c0c414b98b1da45fbe;
static PyObject *const_str_plain__link_target_msg;
extern PyObject *const_str_plain_format;
static PyObject *const_str_digest_0922fd358e7b8dd41f9640f85b2322a9;
static PyObject *const_str_digest_2639ee77a618efc467e27b9fb46d3224;
static PyObject *const_str_digest_a5517f4c1d5669fd49cf3dd044b005ae;
static PyObject *const_str_digest_13c8d8e17e56bde0aaaddfb17ba5711a;
static PyObject *const_str_digest_e6bb3e6719e1d50cf6fb128e59c8727c;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_tuple_str_plain_p_tuple;
static PyObject *const_str_digest_0ca0b56babb73191fa586456f970f494;
extern PyObject *const_str_plain_S_ISSOCK;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_c926dc77011764aa9b3bb30ed4447290 = UNSTREAM_STRING_ASCII( &constant_bin[ 5307385 ], 42, 0 );
    const_str_plain_assert_islink = UNSTREAM_STRING_ASCII( &constant_bin[ 5307427 ], 13, 1 );
    const_str_plain_assert_not_ispipe = UNSTREAM_STRING_ASCII( &constant_bin[ 5307440 ], 17, 1 );
    const_str_digest_e404fd3ebeb2f7e85fde30500347e80f = UNSTREAM_STRING_ASCII( &constant_bin[ 5307457 ], 218, 0 );
    const_str_digest_443fa7a6de7f872cfe16146ec8e58033 = UNSTREAM_STRING_ASCII( &constant_bin[ 5307675 ], 26, 0 );
    const_str_plain_assert_not_issocket = UNSTREAM_STRING_ASCII( &constant_bin[ 5307701 ], 19, 1 );
    const_str_digest_9014940713c6f8ed77065e6419645a00 = UNSTREAM_STRING_ASCII( &constant_bin[ 5307720 ], 36, 0 );
    const_str_digest_fa793fa99a864abb5e2c1d809feff419 = UNSTREAM_STRING_ASCII( &constant_bin[ 5307756 ], 124, 0 );
    const_str_digest_baa54e3476b37e3f6f9feb3b112d4f71 = UNSTREAM_STRING_ASCII( &constant_bin[ 5307880 ], 213, 0 );
    const_str_plain_assert_isdir = UNSTREAM_STRING_ASCII( &constant_bin[ 5308093 ], 12, 1 );
    const_str_digest_a7890743df90394f5cf16eb15c18a9e8 = UNSTREAM_STRING_ASCII( &constant_bin[ 5308105 ], 23, 0 );
    const_str_plain__strpath = UNSTREAM_STRING_ASCII( &constant_bin[ 5308128 ], 8, 1 );
    const_str_digest_8bbf0418ccbfc46201e9ee069aa08634 = UNSTREAM_STRING_ASCII( &constant_bin[ 5308136 ], 20, 0 );
    const_str_digest_cc382abe148c946dbd9d9fdbeb5b4fca = UNSTREAM_STRING_ASCII( &constant_bin[ 5308156 ], 40, 0 );
    const_tuple_39e8c4c28bca45c942a7780371d1fabf_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_39e8c4c28bca45c942a7780371d1fabf_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    const_str_plain_follow_symlinks = UNSTREAM_STRING_ASCII( &constant_bin[ 5307522 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_39e8c4c28bca45c942a7780371d1fabf_tuple, 1, const_str_plain_follow_symlinks ); Py_INCREF( const_str_plain_follow_symlinks );
    PyTuple_SET_ITEM( const_tuple_39e8c4c28bca45c942a7780371d1fabf_tuple, 2, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    PyTuple_SET_ITEM( const_tuple_39e8c4c28bca45c942a7780371d1fabf_tuple, 3, const_str_plain_stat ); Py_INCREF( const_str_plain_stat );
    const_str_digest_390fbaeacc478762984081940bd22e52 = UNSTREAM_STRING_ASCII( &constant_bin[ 5308196 ], 37, 0 );
    const_list_6ea49453a896f73b83ca69d0042bbfcd_list = PyList_New( 12 );
    const_str_plain_assert_path_exists = UNSTREAM_STRING_ASCII( &constant_bin[ 5308233 ], 18, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 0, const_str_plain_assert_path_exists ); Py_INCREF( const_str_plain_assert_path_exists );
    const_str_plain_assert_not_path_exists = UNSTREAM_STRING_ASCII( &constant_bin[ 5308251 ], 22, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 1, const_str_plain_assert_not_path_exists ); Py_INCREF( const_str_plain_assert_not_path_exists );
    const_str_plain_assert_isfile = UNSTREAM_STRING_ASCII( &constant_bin[ 5308273 ], 13, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 2, const_str_plain_assert_isfile ); Py_INCREF( const_str_plain_assert_isfile );
    const_str_plain_assert_not_isfile = UNSTREAM_STRING_ASCII( &constant_bin[ 5308286 ], 17, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 3, const_str_plain_assert_not_isfile ); Py_INCREF( const_str_plain_assert_not_isfile );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 4, const_str_plain_assert_isdir ); Py_INCREF( const_str_plain_assert_isdir );
    const_str_plain_assert_not_isdir = UNSTREAM_STRING_ASCII( &constant_bin[ 5308303 ], 16, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 5, const_str_plain_assert_not_isdir ); Py_INCREF( const_str_plain_assert_not_isdir );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 6, const_str_plain_assert_islink ); Py_INCREF( const_str_plain_assert_islink );
    const_str_plain_assert_not_islink = UNSTREAM_STRING_ASCII( &constant_bin[ 5308319 ], 17, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 7, const_str_plain_assert_not_islink ); Py_INCREF( const_str_plain_assert_not_islink );
    const_str_plain_assert_ispipe = UNSTREAM_STRING_ASCII( &constant_bin[ 5308336 ], 13, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 8, const_str_plain_assert_ispipe ); Py_INCREF( const_str_plain_assert_ispipe );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 9, const_str_plain_assert_not_ispipe ); Py_INCREF( const_str_plain_assert_not_ispipe );
    const_str_plain_assert_issocket = UNSTREAM_STRING_ASCII( &constant_bin[ 5308349 ], 15, 1 );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 10, const_str_plain_assert_issocket ); Py_INCREF( const_str_plain_assert_issocket );
    PyList_SET_ITEM( const_list_6ea49453a896f73b83ca69d0042bbfcd_list, 11, const_str_plain_assert_not_issocket ); Py_INCREF( const_str_plain_assert_not_issocket );
    const_str_digest_0568a8936968252e7d9c22b273abb185 = UNSTREAM_STRING_ASCII( &constant_bin[ 5308364 ], 52, 0 );
    const_str_digest_21d1847ad164e2393145e8409b162cca = UNSTREAM_STRING_ASCII( &constant_bin[ 5308416 ], 208, 0 );
    const_str_digest_e28dcdb876ab4c23e5e2969de17f1424 = UNSTREAM_STRING_ASCII( &constant_bin[ 5308624 ], 212, 0 );
    const_tuple_str_plain_path_str_plain_msg_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_msg_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_msg_tuple, 1, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    const_str_plain_S_ISFIFO = UNSTREAM_STRING_ASCII( &constant_bin[ 5308836 ], 8, 1 );
    const_str_digest_25d38d7a3fa7af5db857655a9644ddb5 = UNSTREAM_STRING_ASCII( &constant_bin[ 5308844 ], 217, 0 );
    const_str_digest_18aafda7c630447c81c12d126fa0d8fc = UNSTREAM_STRING_ASCII( &constant_bin[ 5309061 ], 39, 0 );
    const_str_digest_147ac877161e80d5706bb7b024f94046 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309100 ], 222, 0 );
    const_tuple_62f5a673026eba82e95d8057446765b4_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_62f5a673026eba82e95d8057446765b4_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_62f5a673026eba82e95d8057446765b4_tuple, 1, const_str_plain_to ); Py_INCREF( const_str_plain_to );
    PyTuple_SET_ITEM( const_tuple_62f5a673026eba82e95d8057446765b4_tuple, 2, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    PyTuple_SET_ITEM( const_tuple_62f5a673026eba82e95d8057446765b4_tuple, 3, const_str_plain_st ); Py_INCREF( const_str_plain_st );
    PyTuple_SET_ITEM( const_tuple_62f5a673026eba82e95d8057446765b4_tuple, 4, const_str_plain_target ); Py_INCREF( const_str_plain_target );
    const_str_digest_248524f094372553000860833941be98 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309322 ], 25, 0 );
    const_str_digest_98d764b6aceefa953b27dd46d8fdd7f6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309347 ], 19, 0 );
    const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 1, const_str_plain_follow_symlinks ); Py_INCREF( const_str_plain_follow_symlinks );
    PyTuple_SET_ITEM( const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 2, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    PyTuple_SET_ITEM( const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, const_str_plain_st ); Py_INCREF( const_str_plain_st );
    const_str_digest_1614d9e6c24f9c31c0e6fc513aeb2664 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309366 ], 50, 0 );
    const_str_plain__stat_for_assert = UNSTREAM_STRING_ASCII( &constant_bin[ 5309416 ], 16, 1 );
    const_str_digest_7454cc19e49b12b40d8fb24ce406f1d1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309432 ], 24, 0 );
    const_tuple_str_plain_path_str_plain_msg_str_plain_st_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_msg_str_plain_st_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_msg_str_plain_st_tuple, 1, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_str_plain_msg_str_plain_st_tuple, 2, const_str_plain_st ); Py_INCREF( const_str_plain_st );
    const_str_digest_d083eaa7b694dc4d817e84fa2464e182 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309456 ], 15, 0 );
    const_str_digest_41c54cd2205f8bd04d958692f00d178a = UNSTREAM_STRING_ASCII( &constant_bin[ 5309330 ], 16, 0 );
    const_str_digest_c6db506f43c3e7c0c414b98b1da45fbe = UNSTREAM_STRING_ASCII( &constant_bin[ 5309471 ], 50, 0 );
    const_str_plain__link_target_msg = UNSTREAM_STRING_ASCII( &constant_bin[ 5309521 ], 16, 1 );
    const_str_digest_0922fd358e7b8dd41f9640f85b2322a9 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309537 ], 44, 0 );
    const_str_digest_2639ee77a618efc467e27b9fb46d3224 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309581 ], 214, 0 );
    const_str_digest_a5517f4c1d5669fd49cf3dd044b005ae = UNSTREAM_STRING_ASCII( &constant_bin[ 5309795 ], 27, 0 );
    const_str_digest_13c8d8e17e56bde0aaaddfb17ba5711a = UNSTREAM_STRING_ASCII( &constant_bin[ 5309822 ], 21, 0 );
    const_str_digest_e6bb3e6719e1d50cf6fb128e59c8727c = UNSTREAM_STRING_ASCII( &constant_bin[ 5309843 ], 70, 0 );
    const_str_digest_0ca0b56babb73191fa586456f970f494 = UNSTREAM_STRING_ASCII( &constant_bin[ 5309913 ], 226, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_testpath$asserts( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_ffada3128b04d5a1636cdcadcc36e436;
static PyCodeObject *codeobj_a544ceba3982ab45aaa5a765cefb6b9f;
static PyCodeObject *codeobj_a70c416c16aa0ffb5c3860440f62082f;
static PyCodeObject *codeobj_f550c408b9dac82d64ffd0426bed0126;
static PyCodeObject *codeobj_0f40ff96ede9ff4e414efcc4305f8bbb;
static PyCodeObject *codeobj_7bda7506e2b0249e7dc3fb32110b2d84;
static PyCodeObject *codeobj_505bdddb98a15113fd2d00b30c8f47ea;
static PyCodeObject *codeobj_2ab55ca29878ee52515abe62d6e8e680;
static PyCodeObject *codeobj_8e223bf6284e9d3d0985a6c76eb4260e;
static PyCodeObject *codeobj_7bfa05d8f9b9666d0d96e7677f2e6282;
static PyCodeObject *codeobj_cda566e420fc8b0521e6fafe41c1366f;
static PyCodeObject *codeobj_c027159c3997c53ffa518527e26b0d09;
static PyCodeObject *codeobj_7d6fa0a2ec01dd0f57e8f17f47acbfc7;
static PyCodeObject *codeobj_9a03e41f739272dc75e8bc6529a89915;
static PyCodeObject *codeobj_8510dd304b3126d89503c48198a6bf2b;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_98d764b6aceefa953b27dd46d8fdd7f6 );
    codeobj_ffada3128b04d5a1636cdcadcc36e436 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_248524f094372553000860833941be98, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_a544ceba3982ab45aaa5a765cefb6b9f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__stat_for_assert, 29, const_tuple_39e8c4c28bca45c942a7780371d1fabf_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a70c416c16aa0ffb5c3860440f62082f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__strpath, 24, const_tuple_str_plain_p_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f550c408b9dac82d64ffd0426bed0126 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_isdir, 78, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0f40ff96ede9ff4e414efcc4305f8bbb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_isfile, 52, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7bda7506e2b0249e7dc3fb32110b2d84 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_islink, 112, const_tuple_62f5a673026eba82e95d8057446765b4_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_505bdddb98a15113fd2d00b30c8f47ea = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_ispipe, 143, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ab55ca29878ee52515abe62d6e8e680 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_issocket, 169, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8e223bf6284e9d3d0985a6c76eb4260e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_not_isdir, 91, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7bfa05d8f9b9666d0d96e7677f2e6282 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_not_isfile, 65, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cda566e420fc8b0521e6fafe41c1366f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_not_islink, 133, const_tuple_str_plain_path_str_plain_msg_str_plain_st_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c027159c3997c53ffa518527e26b0d09 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_not_ispipe, 156, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7d6fa0a2ec01dd0f57e8f17f47acbfc7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_not_issocket, 182, const_tuple_697435779840ab1c40fcd40dfc6d3d26_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9a03e41f739272dc75e8bc6529a89915 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_not_path_exists, 43, const_tuple_str_plain_path_str_plain_msg_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8510dd304b3126d89503c48198a6bf2b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assert_path_exists, 38, const_tuple_str_plain_path_str_plain_msg_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_10_assert_not_islink( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_11_assert_ispipe( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_12_assert_not_ispipe( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_13_assert_issocket( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_14_assert_not_issocket( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_1__strpath(  );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_2__stat_for_assert( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_3_assert_path_exists( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_4_assert_not_path_exists( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_5_assert_isfile( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_6_assert_not_isfile( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_7_assert_isdir( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_8_assert_not_isdir( PyObject *defaults );


static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_9_assert_islink( PyObject *defaults );


// The module function definitions.
static PyObject *impl_testpath$asserts$$$function_1__strpath( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_p = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a70c416c16aa0ffb5c3860440f62082f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a70c416c16aa0ffb5c3860440f62082f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a70c416c16aa0ffb5c3860440f62082f, codeobj_a70c416c16aa0ffb5c3860440f62082f, module_testpath$asserts, sizeof(void *) );
    frame_a70c416c16aa0ffb5c3860440f62082f = cache_frame_a70c416c16aa0ffb5c3860440f62082f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a70c416c16aa0ffb5c3860440f62082f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a70c416c16aa0ffb5c3860440f62082f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_p );
        tmp_isinstance_inst_1 = par_p;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_Path );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Path );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Path" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 25;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_unicode_arg_1;
            CHECK_OBJECT( par_p );
            tmp_unicode_arg_1 = par_p;
            tmp_return_value = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 26;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a70c416c16aa0ffb5c3860440f62082f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a70c416c16aa0ffb5c3860440f62082f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a70c416c16aa0ffb5c3860440f62082f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a70c416c16aa0ffb5c3860440f62082f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a70c416c16aa0ffb5c3860440f62082f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a70c416c16aa0ffb5c3860440f62082f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a70c416c16aa0ffb5c3860440f62082f,
        type_description_1,
        par_p
    );


    // Release cached frame.
    if ( frame_a70c416c16aa0ffb5c3860440f62082f == cache_frame_a70c416c16aa0ffb5c3860440f62082f )
    {
        Py_DECREF( frame_a70c416c16aa0ffb5c3860440f62082f );
    }
    cache_frame_a70c416c16aa0ffb5c3860440f62082f = NULL;

    assertFrameObject( frame_a70c416c16aa0ffb5c3860440f62082f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_p );
    tmp_return_value = par_p;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_1__strpath );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_p );
    Py_DECREF( par_p );
    par_p = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_p );
    Py_DECREF( par_p );
    par_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_1__strpath );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_2__stat_for_assert( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_stat = NULL;
    struct Nuitka_FrameObject *frame_a544ceba3982ab45aaa5a765cefb6b9f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_a544ceba3982ab45aaa5a765cefb6b9f = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a544ceba3982ab45aaa5a765cefb6b9f, codeobj_a544ceba3982ab45aaa5a765cefb6b9f, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a544ceba3982ab45aaa5a765cefb6b9f = cache_frame_a544ceba3982ab45aaa5a765cefb6b9f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a544ceba3982ab45aaa5a765cefb6b9f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a544ceba3982ab45aaa5a765cefb6b9f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_follow_symlinks );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_stat );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_lstat );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        condexpr_end_1:;
        assert( var_stat == NULL );
        var_stat = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_stat );
        tmp_called_name_1 = var_stat;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_a544ceba3982ab45aaa5a765cefb6b9f->m_frame.f_lineno = 32;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_2__stat_for_assert );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_a544ceba3982ab45aaa5a765cefb6b9f, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_a544ceba3982ab45aaa5a765cefb6b9f, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_OSError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_2 = par_msg;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_0922fd358e7b8dd41f9640f85b2322a9;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 35;
                    type_description_1 = "oooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_2;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_a544ceba3982ab45aaa5a765cefb6b9f->m_frame.f_lineno = 36;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 36;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 31;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_a544ceba3982ab45aaa5a765cefb6b9f->m_frame) frame_a544ceba3982ab45aaa5a765cefb6b9f->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_2__stat_for_assert );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a544ceba3982ab45aaa5a765cefb6b9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a544ceba3982ab45aaa5a765cefb6b9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a544ceba3982ab45aaa5a765cefb6b9f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a544ceba3982ab45aaa5a765cefb6b9f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a544ceba3982ab45aaa5a765cefb6b9f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a544ceba3982ab45aaa5a765cefb6b9f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a544ceba3982ab45aaa5a765cefb6b9f,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_stat
    );


    // Release cached frame.
    if ( frame_a544ceba3982ab45aaa5a765cefb6b9f == cache_frame_a544ceba3982ab45aaa5a765cefb6b9f )
    {
        Py_DECREF( frame_a544ceba3982ab45aaa5a765cefb6b9f );
    }
    cache_frame_a544ceba3982ab45aaa5a765cefb6b9f = NULL;

    assertFrameObject( frame_a544ceba3982ab45aaa5a765cefb6b9f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_2__stat_for_assert );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_stat );
    Py_DECREF( var_stat );
    var_stat = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_stat );
    var_stat = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_2__stat_for_assert );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_3_assert_path_exists( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_msg = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_8510dd304b3126d89503c48198a6bf2b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8510dd304b3126d89503c48198a6bf2b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8510dd304b3126d89503c48198a6bf2b, codeobj_8510dd304b3126d89503c48198a6bf2b, module_testpath$asserts, sizeof(void *)+sizeof(void *) );
    frame_8510dd304b3126d89503c48198a6bf2b = cache_frame_8510dd304b3126d89503c48198a6bf2b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8510dd304b3126d89503c48198a6bf2b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8510dd304b3126d89503c48198a6bf2b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        frame_8510dd304b3126d89503c48198a6bf2b->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = Py_True;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_8510dd304b3126d89503c48198a6bf2b->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8510dd304b3126d89503c48198a6bf2b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8510dd304b3126d89503c48198a6bf2b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8510dd304b3126d89503c48198a6bf2b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8510dd304b3126d89503c48198a6bf2b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8510dd304b3126d89503c48198a6bf2b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8510dd304b3126d89503c48198a6bf2b,
        type_description_1,
        par_path,
        par_msg
    );


    // Release cached frame.
    if ( frame_8510dd304b3126d89503c48198a6bf2b == cache_frame_8510dd304b3126d89503c48198a6bf2b )
    {
        Py_DECREF( frame_8510dd304b3126d89503c48198a6bf2b );
    }
    cache_frame_8510dd304b3126d89503c48198a6bf2b = NULL;

    assertFrameObject( frame_8510dd304b3126d89503c48198a6bf2b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_3_assert_path_exists );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_3_assert_path_exists );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_4_assert_not_path_exists( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_msg = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_9a03e41f739272dc75e8bc6529a89915;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9a03e41f739272dc75e8bc6529a89915 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9a03e41f739272dc75e8bc6529a89915, codeobj_9a03e41f739272dc75e8bc6529a89915, module_testpath$asserts, sizeof(void *)+sizeof(void *) );
    frame_9a03e41f739272dc75e8bc6529a89915 = cache_frame_9a03e41f739272dc75e8bc6529a89915;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9a03e41f739272dc75e8bc6529a89915 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9a03e41f739272dc75e8bc6529a89915 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_9a03e41f739272dc75e8bc6529a89915->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        int tmp_truth_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        frame_9a03e41f739272dc75e8bc6529a89915->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_exists, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 47;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_d083eaa7b694dc4d817e84fa2464e182;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_2;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_9a03e41f739272dc75e8bc6529a89915->m_frame.f_lineno = 50;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 50;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a03e41f739272dc75e8bc6529a89915 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a03e41f739272dc75e8bc6529a89915 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9a03e41f739272dc75e8bc6529a89915, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9a03e41f739272dc75e8bc6529a89915->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9a03e41f739272dc75e8bc6529a89915, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9a03e41f739272dc75e8bc6529a89915,
        type_description_1,
        par_path,
        par_msg
    );


    // Release cached frame.
    if ( frame_9a03e41f739272dc75e8bc6529a89915 == cache_frame_9a03e41f739272dc75e8bc6529a89915 )
    {
        Py_DECREF( frame_9a03e41f739272dc75e8bc6529a89915 );
    }
    cache_frame_9a03e41f739272dc75e8bc6529a89915 = NULL;

    assertFrameObject( frame_9a03e41f739272dc75e8bc6529a89915 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_4_assert_not_path_exists );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_4_assert_not_path_exists );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_5_assert_isfile( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_0f40ff96ede9ff4e414efcc4305f8bbb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_0f40ff96ede9ff4e414efcc4305f8bbb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0f40ff96ede9ff4e414efcc4305f8bbb, codeobj_0f40ff96ede9ff4e414efcc4305f8bbb, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0f40ff96ede9ff4e414efcc4305f8bbb = cache_frame_0f40ff96ede9ff4e414efcc4305f8bbb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0f40ff96ede9ff4e414efcc4305f8bbb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0f40ff96ede9ff4e414efcc4305f8bbb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_0f40ff96ede9ff4e414efcc4305f8bbb->m_frame.f_lineno = 58;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_0f40ff96ede9ff4e414efcc4305f8bbb->m_frame.f_lineno = 59;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISREG );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 60;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_0f40ff96ede9ff4e414efcc4305f8bbb->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_c926dc77011764aa9b3bb30ed4447290;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 62;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_0f40ff96ede9ff4e414efcc4305f8bbb->m_frame.f_lineno = 63;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 63;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f40ff96ede9ff4e414efcc4305f8bbb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0f40ff96ede9ff4e414efcc4305f8bbb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0f40ff96ede9ff4e414efcc4305f8bbb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0f40ff96ede9ff4e414efcc4305f8bbb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0f40ff96ede9ff4e414efcc4305f8bbb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0f40ff96ede9ff4e414efcc4305f8bbb,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_0f40ff96ede9ff4e414efcc4305f8bbb == cache_frame_0f40ff96ede9ff4e414efcc4305f8bbb )
    {
        Py_DECREF( frame_0f40ff96ede9ff4e414efcc4305f8bbb );
    }
    cache_frame_0f40ff96ede9ff4e414efcc4305f8bbb = NULL;

    assertFrameObject( frame_0f40ff96ede9ff4e414efcc4305f8bbb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_5_assert_isfile );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_5_assert_isfile );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_6_assert_not_isfile( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_7bfa05d8f9b9666d0d96e7677f2e6282;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7bfa05d8f9b9666d0d96e7677f2e6282 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7bfa05d8f9b9666d0d96e7677f2e6282, codeobj_7bfa05d8f9b9666d0d96e7677f2e6282, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7bfa05d8f9b9666d0d96e7677f2e6282 = cache_frame_7bfa05d8f9b9666d0d96e7677f2e6282;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7bfa05d8f9b9666d0d96e7677f2e6282 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7bfa05d8f9b9666d0d96e7677f2e6282 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_7bfa05d8f9b9666d0d96e7677f2e6282->m_frame.f_lineno = 71;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_7bfa05d8f9b9666d0d96e7677f2e6282->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        int tmp_truth_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISREG );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 73;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_7bfa05d8f9b9666d0d96e7677f2e6282->m_frame.f_lineno = 73;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 73;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_443fa7a6de7f872cfe16146ec8e58033;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 75;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_7bfa05d8f9b9666d0d96e7677f2e6282->m_frame.f_lineno = 76;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 76;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7bfa05d8f9b9666d0d96e7677f2e6282 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7bfa05d8f9b9666d0d96e7677f2e6282 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7bfa05d8f9b9666d0d96e7677f2e6282, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7bfa05d8f9b9666d0d96e7677f2e6282->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7bfa05d8f9b9666d0d96e7677f2e6282, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7bfa05d8f9b9666d0d96e7677f2e6282,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_7bfa05d8f9b9666d0d96e7677f2e6282 == cache_frame_7bfa05d8f9b9666d0d96e7677f2e6282 )
    {
        Py_DECREF( frame_7bfa05d8f9b9666d0d96e7677f2e6282 );
    }
    cache_frame_7bfa05d8f9b9666d0d96e7677f2e6282 = NULL;

    assertFrameObject( frame_7bfa05d8f9b9666d0d96e7677f2e6282 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_6_assert_not_isfile );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_6_assert_not_isfile );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_7_assert_isdir( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_f550c408b9dac82d64ffd0426bed0126;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_f550c408b9dac82d64ffd0426bed0126 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f550c408b9dac82d64ffd0426bed0126, codeobj_f550c408b9dac82d64ffd0426bed0126, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f550c408b9dac82d64ffd0426bed0126 = cache_frame_f550c408b9dac82d64ffd0426bed0126;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f550c408b9dac82d64ffd0426bed0126 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f550c408b9dac82d64ffd0426bed0126 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_f550c408b9dac82d64ffd0426bed0126->m_frame.f_lineno = 84;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_f550c408b9dac82d64ffd0426bed0126->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISDIR );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_f550c408b9dac82d64ffd0426bed0126->m_frame.f_lineno = 86;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_18aafda7c630447c81c12d126fa0d8fc;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 88;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_f550c408b9dac82d64ffd0426bed0126->m_frame.f_lineno = 89;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 89;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f550c408b9dac82d64ffd0426bed0126 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f550c408b9dac82d64ffd0426bed0126 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f550c408b9dac82d64ffd0426bed0126, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f550c408b9dac82d64ffd0426bed0126->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f550c408b9dac82d64ffd0426bed0126, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f550c408b9dac82d64ffd0426bed0126,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_f550c408b9dac82d64ffd0426bed0126 == cache_frame_f550c408b9dac82d64ffd0426bed0126 )
    {
        Py_DECREF( frame_f550c408b9dac82d64ffd0426bed0126 );
    }
    cache_frame_f550c408b9dac82d64ffd0426bed0126 = NULL;

    assertFrameObject( frame_f550c408b9dac82d64ffd0426bed0126 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_7_assert_isdir );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_7_assert_isdir );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_8_assert_not_isdir( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_8e223bf6284e9d3d0985a6c76eb4260e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8e223bf6284e9d3d0985a6c76eb4260e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8e223bf6284e9d3d0985a6c76eb4260e, codeobj_8e223bf6284e9d3d0985a6c76eb4260e, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8e223bf6284e9d3d0985a6c76eb4260e = cache_frame_8e223bf6284e9d3d0985a6c76eb4260e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8e223bf6284e9d3d0985a6c76eb4260e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8e223bf6284e9d3d0985a6c76eb4260e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_8e223bf6284e9d3d0985a6c76eb4260e->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_8e223bf6284e9d3d0985a6c76eb4260e->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        int tmp_truth_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISDIR );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_8e223bf6284e9d3d0985a6c76eb4260e->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_a7890743df90394f5cf16eb15c18a9e8;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 101;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_8e223bf6284e9d3d0985a6c76eb4260e->m_frame.f_lineno = 102;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 102;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e223bf6284e9d3d0985a6c76eb4260e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e223bf6284e9d3d0985a6c76eb4260e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8e223bf6284e9d3d0985a6c76eb4260e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8e223bf6284e9d3d0985a6c76eb4260e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8e223bf6284e9d3d0985a6c76eb4260e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8e223bf6284e9d3d0985a6c76eb4260e,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_8e223bf6284e9d3d0985a6c76eb4260e == cache_frame_8e223bf6284e9d3d0985a6c76eb4260e )
    {
        Py_DECREF( frame_8e223bf6284e9d3d0985a6c76eb4260e );
    }
    cache_frame_8e223bf6284e9d3d0985a6c76eb4260e = NULL;

    assertFrameObject( frame_8e223bf6284e9d3d0985a6c76eb4260e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_8_assert_not_isdir );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_8_assert_not_isdir );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_9_assert_islink( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_to = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    PyObject *var_target = NULL;
    struct Nuitka_FrameObject *frame_7bda7506e2b0249e7dc3fb32110b2d84;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_7bda7506e2b0249e7dc3fb32110b2d84 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7bda7506e2b0249e7dc3fb32110b2d84, codeobj_7bda7506e2b0249e7dc3fb32110b2d84, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7bda7506e2b0249e7dc3fb32110b2d84 = cache_frame_7bda7506e2b0249e7dc3fb32110b2d84;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7bda7506e2b0249e7dc3fb32110b2d84 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7bda7506e2b0249e7dc3fb32110b2d84 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 117;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        tmp_args_element_name_3 = Py_False;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISLNK );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 119;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_390fbaeacc478762984081940bd22e52;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 122;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 122;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_to );
        tmp_compexpr_left_2 = par_to;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_6;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 125;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_4 = tmp_mvar_value_4;
            CHECK_OBJECT( par_to );
            tmp_args_element_name_6 = par_to;
            frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 125;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_to;
                assert( old != NULL );
                par_to = tmp_assign_source_4;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_7;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_os );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 126;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_5;
            CHECK_OBJECT( par_path );
            tmp_args_element_name_7 = par_path;
            frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 126;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_readlink, call_args );
            }

            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 126;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_target == NULL );
            var_target = tmp_assign_source_5;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( var_target );
            tmp_compexpr_left_3 = var_target;
            CHECK_OBJECT( par_to );
            tmp_compexpr_right_3 = par_to;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( par_msg );
                tmp_compexpr_left_4 = par_msg;
                tmp_compexpr_right_4 = Py_None;
                tmp_condition_result_5 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_6;
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_source_name_3;
                    PyObject *tmp_mvar_value_6;
                    PyObject *tmp_kw_name_1;
                    PyObject *tmp_dict_key_1;
                    PyObject *tmp_dict_value_1;
                    PyObject *tmp_dict_key_2;
                    PyObject *tmp_dict_value_2;
                    PyObject *tmp_dict_key_3;
                    PyObject *tmp_dict_value_3;
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__link_target_msg );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__link_target_msg );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_link_target_msg" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 130;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_3 = tmp_mvar_value_6;
                    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_format );
                    if ( tmp_called_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 130;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_dict_key_1 = const_str_plain_path;
                    CHECK_OBJECT( par_path );
                    tmp_dict_value_1 = par_path;
                    tmp_kw_name_1 = _PyDict_NewPresized( 3 );
                    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                    assert( !(tmp_res != 0) );
                    tmp_dict_key_2 = const_str_plain_expected;
                    CHECK_OBJECT( par_to );
                    tmp_dict_value_2 = par_to;
                    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
                    assert( !(tmp_res != 0) );
                    tmp_dict_key_3 = const_str_plain_actual;
                    CHECK_OBJECT( var_target );
                    tmp_dict_value_3 = var_target;
                    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
                    assert( !(tmp_res != 0) );
                    frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 130;
                    tmp_assign_source_6 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_1 );
                    Py_DECREF( tmp_called_name_5 );
                    Py_DECREF( tmp_kw_name_1 );
                    if ( tmp_assign_source_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 130;
                        type_description_1 = "ooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = par_msg;
                        assert( old != NULL );
                        par_msg = tmp_assign_source_6;
                        Py_DECREF( old );
                    }

                }
                branch_no_5:;
            }
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                CHECK_OBJECT( par_msg );
                tmp_make_exception_arg_2 = par_msg;
                frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame.f_lineno = 131;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
                }

                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 131;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            branch_no_4:;
        }
        branch_no_3:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7bda7506e2b0249e7dc3fb32110b2d84 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7bda7506e2b0249e7dc3fb32110b2d84 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7bda7506e2b0249e7dc3fb32110b2d84, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7bda7506e2b0249e7dc3fb32110b2d84->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7bda7506e2b0249e7dc3fb32110b2d84, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7bda7506e2b0249e7dc3fb32110b2d84,
        type_description_1,
        par_path,
        par_to,
        par_msg,
        var_st,
        var_target
    );


    // Release cached frame.
    if ( frame_7bda7506e2b0249e7dc3fb32110b2d84 == cache_frame_7bda7506e2b0249e7dc3fb32110b2d84 )
    {
        Py_DECREF( frame_7bda7506e2b0249e7dc3fb32110b2d84 );
    }
    cache_frame_7bda7506e2b0249e7dc3fb32110b2d84 = NULL;

    assertFrameObject( frame_7bda7506e2b0249e7dc3fb32110b2d84 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_9_assert_islink );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_to );
    Py_DECREF( par_to );
    par_to = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    Py_XDECREF( var_target );
    var_target = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_to );
    Py_DECREF( par_to );
    par_to = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    Py_XDECREF( var_target );
    var_target = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_9_assert_islink );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_10_assert_not_islink( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_msg = python_pars[ 1 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_cda566e420fc8b0521e6fafe41c1366f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_cda566e420fc8b0521e6fafe41c1366f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cda566e420fc8b0521e6fafe41c1366f, codeobj_cda566e420fc8b0521e6fafe41c1366f, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cda566e420fc8b0521e6fafe41c1366f = cache_frame_cda566e420fc8b0521e6fafe41c1366f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cda566e420fc8b0521e6fafe41c1366f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cda566e420fc8b0521e6fafe41c1366f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_cda566e420fc8b0521e6fafe41c1366f->m_frame.f_lineno = 136;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 137;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        tmp_args_element_name_3 = Py_False;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_cda566e420fc8b0521e6fafe41c1366f->m_frame.f_lineno = 137;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        int tmp_truth_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISLNK );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 138;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_cda566e420fc8b0521e6fafe41c1366f->m_frame.f_lineno = 138;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 138;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_13c8d8e17e56bde0aaaddfb17ba5711a;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_cda566e420fc8b0521e6fafe41c1366f->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 141;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cda566e420fc8b0521e6fafe41c1366f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cda566e420fc8b0521e6fafe41c1366f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cda566e420fc8b0521e6fafe41c1366f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cda566e420fc8b0521e6fafe41c1366f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cda566e420fc8b0521e6fafe41c1366f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cda566e420fc8b0521e6fafe41c1366f,
        type_description_1,
        par_path,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_cda566e420fc8b0521e6fafe41c1366f == cache_frame_cda566e420fc8b0521e6fafe41c1366f )
    {
        Py_DECREF( frame_cda566e420fc8b0521e6fafe41c1366f );
    }
    cache_frame_cda566e420fc8b0521e6fafe41c1366f = NULL;

    assertFrameObject( frame_cda566e420fc8b0521e6fafe41c1366f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_10_assert_not_islink );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_10_assert_not_islink );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_11_assert_ispipe( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_505bdddb98a15113fd2d00b30c8f47ea;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_505bdddb98a15113fd2d00b30c8f47ea = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_505bdddb98a15113fd2d00b30c8f47ea, codeobj_505bdddb98a15113fd2d00b30c8f47ea, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_505bdddb98a15113fd2d00b30c8f47ea = cache_frame_505bdddb98a15113fd2d00b30c8f47ea;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_505bdddb98a15113fd2d00b30c8f47ea );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_505bdddb98a15113fd2d00b30c8f47ea ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_505bdddb98a15113fd2d00b30c8f47ea->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_505bdddb98a15113fd2d00b30c8f47ea->m_frame.f_lineno = 150;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 151;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISFIFO );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 151;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_505bdddb98a15113fd2d00b30c8f47ea->m_frame.f_lineno = 151;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_cc382abe148c946dbd9d9fdbeb5b4fca;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_505bdddb98a15113fd2d00b30c8f47ea->m_frame.f_lineno = 154;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 154;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_505bdddb98a15113fd2d00b30c8f47ea );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_505bdddb98a15113fd2d00b30c8f47ea );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_505bdddb98a15113fd2d00b30c8f47ea, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_505bdddb98a15113fd2d00b30c8f47ea->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_505bdddb98a15113fd2d00b30c8f47ea, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_505bdddb98a15113fd2d00b30c8f47ea,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_505bdddb98a15113fd2d00b30c8f47ea == cache_frame_505bdddb98a15113fd2d00b30c8f47ea )
    {
        Py_DECREF( frame_505bdddb98a15113fd2d00b30c8f47ea );
    }
    cache_frame_505bdddb98a15113fd2d00b30c8f47ea = NULL;

    assertFrameObject( frame_505bdddb98a15113fd2d00b30c8f47ea );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_11_assert_ispipe );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_11_assert_ispipe );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_12_assert_not_ispipe( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_c027159c3997c53ffa518527e26b0d09;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c027159c3997c53ffa518527e26b0d09 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c027159c3997c53ffa518527e26b0d09, codeobj_c027159c3997c53ffa518527e26b0d09, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c027159c3997c53ffa518527e26b0d09 = cache_frame_c027159c3997c53ffa518527e26b0d09;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c027159c3997c53ffa518527e26b0d09 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c027159c3997c53ffa518527e26b0d09 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_c027159c3997c53ffa518527e26b0d09->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_c027159c3997c53ffa518527e26b0d09->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        int tmp_truth_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISFIFO );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 164;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_c027159c3997c53ffa518527e26b0d09->m_frame.f_lineno = 164;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 164;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_7454cc19e49b12b40d8fb24ce406f1d1;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 166;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_c027159c3997c53ffa518527e26b0d09->m_frame.f_lineno = 167;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 167;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c027159c3997c53ffa518527e26b0d09 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c027159c3997c53ffa518527e26b0d09 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c027159c3997c53ffa518527e26b0d09, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c027159c3997c53ffa518527e26b0d09->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c027159c3997c53ffa518527e26b0d09, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c027159c3997c53ffa518527e26b0d09,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_c027159c3997c53ffa518527e26b0d09 == cache_frame_c027159c3997c53ffa518527e26b0d09 )
    {
        Py_DECREF( frame_c027159c3997c53ffa518527e26b0d09 );
    }
    cache_frame_c027159c3997c53ffa518527e26b0d09 = NULL;

    assertFrameObject( frame_c027159c3997c53ffa518527e26b0d09 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_12_assert_not_ispipe );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_12_assert_not_ispipe );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_13_assert_issocket( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_2ab55ca29878ee52515abe62d6e8e680;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_2ab55ca29878ee52515abe62d6e8e680 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ab55ca29878ee52515abe62d6e8e680, codeobj_2ab55ca29878ee52515abe62d6e8e680, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2ab55ca29878ee52515abe62d6e8e680 = cache_frame_2ab55ca29878ee52515abe62d6e8e680;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ab55ca29878ee52515abe62d6e8e680 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ab55ca29878ee52515abe62d6e8e680 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_2ab55ca29878ee52515abe62d6e8e680->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_2ab55ca29878ee52515abe62d6e8e680->m_frame.f_lineno = 176;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISSOCK );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 177;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_2ab55ca29878ee52515abe62d6e8e680->m_frame.f_lineno = 177;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_9014940713c6f8ed77065e6419645a00;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 179;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_2ab55ca29878ee52515abe62d6e8e680->m_frame.f_lineno = 180;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 180;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ab55ca29878ee52515abe62d6e8e680 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ab55ca29878ee52515abe62d6e8e680 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ab55ca29878ee52515abe62d6e8e680, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ab55ca29878ee52515abe62d6e8e680->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ab55ca29878ee52515abe62d6e8e680, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ab55ca29878ee52515abe62d6e8e680,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_2ab55ca29878ee52515abe62d6e8e680 == cache_frame_2ab55ca29878ee52515abe62d6e8e680 )
    {
        Py_DECREF( frame_2ab55ca29878ee52515abe62d6e8e680 );
    }
    cache_frame_2ab55ca29878ee52515abe62d6e8e680 = NULL;

    assertFrameObject( frame_2ab55ca29878ee52515abe62d6e8e680 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_13_assert_issocket );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_13_assert_issocket );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_testpath$asserts$$$function_14_assert_not_issocket( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_path = python_pars[ 0 ];
    PyObject *par_follow_symlinks = python_pars[ 1 ];
    PyObject *par_msg = python_pars[ 2 ];
    PyObject *var_st = NULL;
    struct Nuitka_FrameObject *frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7, codeobj_7d6fa0a2ec01dd0f57e8f17f47acbfc7, module_testpath$asserts, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 = cache_frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__strpath );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_strpath" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_1 = par_path;
        frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7->m_frame.f_lineno = 188;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_path;
            assert( old != NULL );
            par_path = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__stat_for_assert );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_stat_for_assert" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_path );
        tmp_args_element_name_2 = par_path;
        CHECK_OBJECT( par_follow_symlinks );
        tmp_args_element_name_3 = par_follow_symlinks;
        CHECK_OBJECT( par_msg );
        tmp_args_element_name_4 = par_msg;
        frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7->m_frame.f_lineno = 189;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_st == NULL );
        var_st = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_2;
        int tmp_truth_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_stat );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "stat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 190;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_S_ISSOCK );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_st );
        tmp_source_name_2 = var_st;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_st_mode );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 190;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7->m_frame.f_lineno = 190;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 190;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_msg );
            tmp_compexpr_left_1 = par_msg;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_8bbf0418ccbfc46201e9ee069aa08634;
                CHECK_OBJECT( par_path );
                tmp_right_name_1 = par_path;
                tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_msg;
                    assert( old != NULL );
                    par_msg = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            CHECK_OBJECT( par_msg );
            tmp_make_exception_arg_1 = par_msg;
            frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7->m_frame.f_lineno = 193;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_AssertionError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 193;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7,
        type_description_1,
        par_path,
        par_follow_symlinks,
        par_msg,
        var_st
    );


    // Release cached frame.
    if ( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 == cache_frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 )
    {
        Py_DECREF( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 );
    }
    cache_frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 = NULL;

    assertFrameObject( frame_7d6fa0a2ec01dd0f57e8f17f47acbfc7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_14_assert_not_issocket );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    CHECK_OBJECT( (PyObject *)par_msg );
    Py_DECREF( par_msg );
    par_msg = NULL;

    CHECK_OBJECT( (PyObject *)var_st );
    Py_DECREF( var_st );
    var_st = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_symlinks );
    Py_DECREF( par_follow_symlinks );
    par_follow_symlinks = NULL;

    Py_XDECREF( par_msg );
    par_msg = NULL;

    Py_XDECREF( var_st );
    var_st = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( testpath$asserts$$$function_14_assert_not_issocket );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_10_assert_not_islink( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_10_assert_not_islink,
        const_str_plain_assert_not_islink,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cda566e420fc8b0521e6fafe41c1366f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_c6db506f43c3e7c0c414b98b1da45fbe,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_11_assert_ispipe( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_11_assert_ispipe,
        const_str_plain_assert_ispipe,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_505bdddb98a15113fd2d00b30c8f47ea,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_baa54e3476b37e3f6f9feb3b112d4f71,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_12_assert_not_ispipe( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_12_assert_not_ispipe,
        const_str_plain_assert_not_ispipe,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c027159c3997c53ffa518527e26b0d09,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_25d38d7a3fa7af5db857655a9644ddb5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_13_assert_issocket( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_13_assert_issocket,
        const_str_plain_assert_issocket,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ab55ca29878ee52515abe62d6e8e680,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_147ac877161e80d5706bb7b024f94046,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_14_assert_not_issocket( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_14_assert_not_issocket,
        const_str_plain_assert_not_issocket,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7d6fa0a2ec01dd0f57e8f17f47acbfc7,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_0ca0b56babb73191fa586456f970f494,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_1__strpath(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_1__strpath,
        const_str_plain__strpath,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a70c416c16aa0ffb5c3860440f62082f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_2__stat_for_assert( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_2__stat_for_assert,
        const_str_plain__stat_for_assert,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a544ceba3982ab45aaa5a765cefb6b9f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_3_assert_path_exists( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_3_assert_path_exists,
        const_str_plain_assert_path_exists,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8510dd304b3126d89503c48198a6bf2b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_0568a8936968252e7d9c22b273abb185,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_4_assert_not_path_exists( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_4_assert_not_path_exists,
        const_str_plain_assert_not_path_exists,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9a03e41f739272dc75e8bc6529a89915,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_1614d9e6c24f9c31c0e6fc513aeb2664,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_5_assert_isfile( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_5_assert_isfile,
        const_str_plain_assert_isfile,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0f40ff96ede9ff4e414efcc4305f8bbb,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_2639ee77a618efc467e27b9fb46d3224,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_6_assert_not_isfile( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_6_assert_not_isfile,
        const_str_plain_assert_not_isfile,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7bfa05d8f9b9666d0d96e7677f2e6282,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_e404fd3ebeb2f7e85fde30500347e80f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_7_assert_isdir( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_7_assert_isdir,
        const_str_plain_assert_isdir,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f550c408b9dac82d64ffd0426bed0126,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_21d1847ad164e2393145e8409b162cca,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_8_assert_not_isdir( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_8_assert_not_isdir,
        const_str_plain_assert_not_isdir,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8e223bf6284e9d3d0985a6c76eb4260e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_e28dcdb876ab4c23e5e2969de17f1424,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_testpath$asserts$$$function_9_assert_islink( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_testpath$asserts$$$function_9_assert_islink,
        const_str_plain_assert_islink,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7bda7506e2b0249e7dc3fb32110b2d84,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_testpath$asserts,
        const_str_digest_fa793fa99a864abb5e2c1d809feff419,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_testpath$asserts =
{
    PyModuleDef_HEAD_INIT,
    "testpath.asserts",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(testpath$asserts)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(testpath$asserts)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_testpath$asserts );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("testpath.asserts: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("testpath.asserts: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("testpath.asserts: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in inittestpath$asserts" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_testpath$asserts = Py_InitModule4(
        "testpath.asserts",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_testpath$asserts = PyModule_Create( &mdef_testpath$asserts );
#endif

    moduledict_testpath$asserts = MODULE_DICT( module_testpath$asserts );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_testpath$asserts,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_testpath$asserts,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_testpath$asserts,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_testpath$asserts,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_testpath$asserts );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_41c54cd2205f8bd04d958692f00d178a, module_testpath$asserts );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    struct Nuitka_FrameObject *frame_ffada3128b04d5a1636cdcadcc36e436;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_testpath$asserts_11 = NULL;
    PyObject *tmp_dictset_value;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_ffada3128b04d5a1636cdcadcc36e436 = MAKE_MODULE_FRAME( codeobj_ffada3128b04d5a1636cdcadcc36e436, module_testpath$asserts );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_ffada3128b04d5a1636cdcadcc36e436 );
    assert( Py_REFCNT( frame_ffada3128b04d5a1636cdcadcc36e436 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_os;
        tmp_globals_name_1 = (PyObject *)moduledict_testpath$asserts;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = 1;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_stat;
        tmp_globals_name_2 = (PyObject *)moduledict_testpath$asserts;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = 2;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_stat, tmp_assign_source_5 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_pathlib;
        tmp_globals_name_3 = (PyObject *)moduledict_testpath$asserts;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_Path_tuple;
        tmp_level_name_3 = const_int_0;
        frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = 5;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Path );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_Path, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_ffada3128b04d5a1636cdcadcc36e436, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_ffada3128b04d5a1636cdcadcc36e436, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_import_name_from_2;
            PyObject *tmp_name_name_4;
            PyObject *tmp_globals_name_4;
            PyObject *tmp_locals_name_4;
            PyObject *tmp_fromlist_name_4;
            PyObject *tmp_level_name_4;
            tmp_name_name_4 = const_str_plain_pathlib2;
            tmp_globals_name_4 = (PyObject *)moduledict_testpath$asserts;
            tmp_locals_name_4 = Py_None;
            tmp_fromlist_name_4 = const_tuple_str_plain_Path_tuple;
            tmp_level_name_4 = const_int_0;
            frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = 9;
            tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
            if ( tmp_import_name_from_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 9;

                goto try_except_handler_3;
            }
            tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Path );
            Py_DECREF( tmp_import_name_from_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 9;

                goto try_except_handler_3;
            }
            UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_Path, tmp_assign_source_7 );
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_2 == NULL )
        {
            exception_keeper_tb_2 = MAKE_TRACEBACK( frame_ffada3128b04d5a1636cdcadcc36e436, exception_keeper_lineno_2 );
        }
        else if ( exception_keeper_lineno_2 != 0 )
        {
            exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_ffada3128b04d5a1636cdcadcc36e436, exception_keeper_lineno_2 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
        PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
        PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_ImportError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 10;

                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            // Tried code:
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_dircall_arg1_1;
                tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
                Py_INCREF( tmp_dircall_arg1_1 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
                    tmp_assign_source_8 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                assert( tmp_class_creation_1__bases == NULL );
                tmp_class_creation_1__bases = tmp_assign_source_8;
            }
            {
                PyObject *tmp_assign_source_9;
                tmp_assign_source_9 = PyDict_New();
                assert( tmp_class_creation_1__class_decl_dict == NULL );
                tmp_class_creation_1__class_decl_dict = tmp_assign_source_9;
            }
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_metaclass_name_1;
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_key_name_1;
                PyObject *tmp_dict_name_1;
                PyObject *tmp_dict_name_2;
                PyObject *tmp_key_name_2;
                nuitka_bool tmp_condition_result_4;
                int tmp_truth_name_1;
                PyObject *tmp_type_arg_1;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                PyObject *tmp_bases_name_1;
                tmp_key_name_1 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
                tmp_key_name_2 = const_str_plain_metaclass;
                tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
                if ( tmp_metaclass_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                goto condexpr_end_1;
                condexpr_false_1:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
                if ( tmp_truth_name_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                tmp_condition_result_4 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_2;
                }
                else
                {
                    goto condexpr_false_2;
                }
                condexpr_true_2:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_subscribed_name_1 = tmp_class_creation_1__bases;
                tmp_subscript_name_1 = const_int_0;
                tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
                if ( tmp_type_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
                Py_DECREF( tmp_type_arg_1 );
                if ( tmp_metaclass_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                goto condexpr_end_2;
                condexpr_false_2:;
                tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_1 );
                condexpr_end_2:;
                condexpr_end_1:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_bases_name_1 = tmp_class_creation_1__bases;
                tmp_assign_source_10 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
                Py_DECREF( tmp_metaclass_name_1 );
                if ( tmp_assign_source_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                assert( tmp_class_creation_1__metaclass == NULL );
                tmp_class_creation_1__metaclass = tmp_assign_source_10;
            }
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_key_name_3;
                PyObject *tmp_dict_name_3;
                tmp_key_name_3 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_5;
                }
                branch_no_3:;
            }
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_source_name_1;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_source_name_1 = tmp_class_creation_1__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_11;
                    PyObject *tmp_called_name_1;
                    PyObject *tmp_source_name_2;
                    PyObject *tmp_args_name_1;
                    PyObject *tmp_tuple_element_1;
                    PyObject *tmp_kw_name_1;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_source_name_2 = tmp_class_creation_1__metaclass;
                    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
                    if ( tmp_called_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 11;

                        goto try_except_handler_5;
                    }
                    tmp_tuple_element_1 = const_str_plain_Path;
                    tmp_args_name_1 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_tuple_element_1 = tmp_class_creation_1__bases;
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
                    CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                    tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
                    frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = 11;
                    tmp_assign_source_11 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_name_1 );
                    if ( tmp_assign_source_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 11;

                        goto try_except_handler_5;
                    }
                    assert( tmp_class_creation_1__prepared == NULL );
                    tmp_class_creation_1__prepared = tmp_assign_source_11;
                }
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_operand_name_1;
                    PyObject *tmp_source_name_3;
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_source_name_3 = tmp_class_creation_1__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
                    tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 11;

                        goto try_except_handler_5;
                    }
                    tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_raise_type_1;
                        PyObject *tmp_raise_value_1;
                        PyObject *tmp_left_name_1;
                        PyObject *tmp_right_name_1;
                        PyObject *tmp_tuple_element_2;
                        PyObject *tmp_getattr_target_1;
                        PyObject *tmp_getattr_attr_1;
                        PyObject *tmp_getattr_default_1;
                        PyObject *tmp_source_name_4;
                        PyObject *tmp_type_arg_2;
                        tmp_raise_type_1 = PyExc_TypeError;
                        tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_1__metaclass );
                        tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                        tmp_getattr_attr_1 = const_str_plain___name__;
                        tmp_getattr_default_1 = const_str_angle_metaclass;
                        tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                        if ( tmp_tuple_element_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 11;

                            goto try_except_handler_5;
                        }
                        tmp_right_name_1 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                        CHECK_OBJECT( tmp_class_creation_1__prepared );
                        tmp_type_arg_2 = tmp_class_creation_1__prepared;
                        tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                        assert( !(tmp_source_name_4 == NULL) );
                        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_4 );
                        if ( tmp_tuple_element_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_1 );

                            exception_lineno = 11;

                            goto try_except_handler_5;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                        tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                        Py_DECREF( tmp_right_name_1 );
                        if ( tmp_raise_value_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 11;

                            goto try_except_handler_5;
                        }
                        exception_type = tmp_raise_type_1;
                        Py_INCREF( tmp_raise_type_1 );
                        exception_value = tmp_raise_value_1;
                        exception_lineno = 11;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                        goto try_except_handler_5;
                    }
                    branch_no_5:;
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    PyObject *tmp_assign_source_12;
                    tmp_assign_source_12 = PyDict_New();
                    assert( tmp_class_creation_1__prepared == NULL );
                    tmp_class_creation_1__prepared = tmp_assign_source_12;
                }
                branch_end_4:;
            }
            {
                PyObject *tmp_assign_source_13;
                {
                    PyObject *tmp_set_locals_1;
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_set_locals_1 = tmp_class_creation_1__prepared;
                    locals_testpath$asserts_11 = tmp_set_locals_1;
                    Py_INCREF( tmp_set_locals_1 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_41c54cd2205f8bd04d958692f00d178a;
                tmp_res = PyObject_SetItem( locals_testpath$asserts_11, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_7;
                }
                tmp_dictset_value = const_str_digest_a5517f4c1d5669fd49cf3dd044b005ae;
                tmp_res = PyObject_SetItem( locals_testpath$asserts_11, const_str_plain___doc__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_7;
                }
                tmp_dictset_value = const_str_plain_Path;
                tmp_res = PyObject_SetItem( locals_testpath$asserts_11, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 11;

                    goto try_except_handler_7;
                }
                {
                    nuitka_bool tmp_condition_result_8;
                    PyObject *tmp_compexpr_left_3;
                    PyObject *tmp_compexpr_right_3;
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_compexpr_left_3 = tmp_class_creation_1__bases;
                    tmp_compexpr_right_3 = const_tuple_type_object_tuple;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 11;

                        goto try_except_handler_7;
                    }
                    tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    tmp_dictset_value = const_tuple_type_object_tuple;
                    tmp_res = PyObject_SetItem( locals_testpath$asserts_11, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 11;

                        goto try_except_handler_7;
                    }
                    branch_no_6:;
                }
                {
                    PyObject *tmp_assign_source_14;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_args_name_2;
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_kw_name_2;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_called_name_2 = tmp_class_creation_1__metaclass;
                    tmp_tuple_element_3 = const_str_plain_Path;
                    tmp_args_name_2 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_tuple_element_3 = tmp_class_creation_1__bases;
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
                    tmp_tuple_element_3 = locals_testpath$asserts_11;
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
                    CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
                    frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = 11;
                    tmp_assign_source_14 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
                    Py_DECREF( tmp_args_name_2 );
                    if ( tmp_assign_source_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 11;

                        goto try_except_handler_7;
                    }
                    assert( outline_0_var___class__ == NULL );
                    outline_0_var___class__ = tmp_assign_source_14;
                }
                CHECK_OBJECT( outline_0_var___class__ );
                tmp_assign_source_13 = outline_0_var___class__;
                Py_INCREF( tmp_assign_source_13 );
                goto try_return_handler_7;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( testpath$asserts );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_7:;
                Py_DECREF( locals_testpath$asserts_11 );
                locals_testpath$asserts_11 = NULL;
                goto try_return_handler_6;
                // Exception handler code:
                try_except_handler_7:;
                exception_keeper_type_3 = exception_type;
                exception_keeper_value_3 = exception_value;
                exception_keeper_tb_3 = exception_tb;
                exception_keeper_lineno_3 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_testpath$asserts_11 );
                locals_testpath$asserts_11 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_3;
                exception_value = exception_keeper_value_3;
                exception_tb = exception_keeper_tb_3;
                exception_lineno = exception_keeper_lineno_3;

                goto try_except_handler_6;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( testpath$asserts );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_6:;
                CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
                Py_DECREF( outline_0_var___class__ );
                outline_0_var___class__ = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_6:;
                exception_keeper_type_4 = exception_type;
                exception_keeper_value_4 = exception_value;
                exception_keeper_tb_4 = exception_tb;
                exception_keeper_lineno_4 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_4;
                exception_value = exception_keeper_value_4;
                exception_tb = exception_keeper_tb_4;
                exception_lineno = exception_keeper_lineno_4;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( testpath$asserts );
                return MOD_RETURN_VALUE( NULL );
                outline_exception_1:;
                exception_lineno = 11;
                goto try_except_handler_5;
                outline_result_1:;
                UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_Path, tmp_assign_source_13 );
            }
            goto try_end_3;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_1__bases );
            tmp_class_creation_1__bases = NULL;

            Py_XDECREF( tmp_class_creation_1__class_decl_dict );
            tmp_class_creation_1__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_1__metaclass );
            tmp_class_creation_1__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_1__prepared );
            tmp_class_creation_1__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto try_except_handler_4;
            // End of try:
            try_end_3:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
            Py_DECREF( tmp_class_creation_1__bases );
            tmp_class_creation_1__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
            Py_DECREF( tmp_class_creation_1__class_decl_dict );
            tmp_class_creation_1__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
            Py_DECREF( tmp_class_creation_1__metaclass );
            tmp_class_creation_1__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
            Py_DECREF( tmp_class_creation_1__prepared );
            tmp_class_creation_1__prepared = NULL;

            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 7;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ffada3128b04d5a1636cdcadcc36e436->m_frame) frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = exception_tb->tb_lineno;

            goto try_except_handler_4;
            branch_end_2:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_2;
        // End of try:
        try_end_4:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_2;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( testpath$asserts );
        return MOD_RETURN_VALUE( NULL );
        // End of try:
        try_end_2:;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 4;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_ffada3128b04d5a1636cdcadcc36e436->m_frame) frame_ffada3128b04d5a1636cdcadcc36e436->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_2;
        branch_end_1:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( testpath$asserts );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_1:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ffada3128b04d5a1636cdcadcc36e436 );
#endif
    popFrameStack();

    assertFrameObject( frame_ffada3128b04d5a1636cdcadcc36e436 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ffada3128b04d5a1636cdcadcc36e436 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ffada3128b04d5a1636cdcadcc36e436, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ffada3128b04d5a1636cdcadcc36e436->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ffada3128b04d5a1636cdcadcc36e436, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = LIST_COPY( const_list_6ea49453a896f73b83ca69d0042bbfcd_list );
        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_testpath$asserts$$$function_1__strpath(  );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__strpath, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_17 = MAKE_FUNCTION_testpath$asserts$$$function_2__stat_for_assert( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__stat_for_assert, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_18 = MAKE_FUNCTION_testpath$asserts$$$function_3_assert_path_exists( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_path_exists, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_19 = MAKE_FUNCTION_testpath$asserts$$$function_4_assert_not_path_exists( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_not_path_exists, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_defaults_4;
        tmp_defaults_4 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_20 = MAKE_FUNCTION_testpath$asserts$$$function_5_assert_isfile( tmp_defaults_4 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_isfile, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_defaults_5;
        tmp_defaults_5 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_5 );
        tmp_assign_source_21 = MAKE_FUNCTION_testpath$asserts$$$function_6_assert_not_isfile( tmp_defaults_5 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_not_isfile, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_defaults_6;
        tmp_defaults_6 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_6 );
        tmp_assign_source_22 = MAKE_FUNCTION_testpath$asserts$$$function_7_assert_isdir( tmp_defaults_6 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_isdir, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_defaults_7;
        tmp_defaults_7 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_7 );
        tmp_assign_source_23 = MAKE_FUNCTION_testpath$asserts$$$function_8_assert_not_isdir( tmp_defaults_7 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_not_isdir, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = const_str_digest_e6bb3e6719e1d50cf6fb128e59c8727c;
        UPDATE_STRING_DICT0( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain__link_target_msg, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_defaults_8;
        tmp_defaults_8 = const_tuple_none_none_tuple;
        Py_INCREF( tmp_defaults_8 );
        tmp_assign_source_25 = MAKE_FUNCTION_testpath$asserts$$$function_9_assert_islink( tmp_defaults_8 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_islink, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_defaults_9;
        tmp_defaults_9 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_9 );
        tmp_assign_source_26 = MAKE_FUNCTION_testpath$asserts$$$function_10_assert_not_islink( tmp_defaults_9 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_not_islink, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_defaults_10;
        tmp_defaults_10 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_10 );
        tmp_assign_source_27 = MAKE_FUNCTION_testpath$asserts$$$function_11_assert_ispipe( tmp_defaults_10 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_ispipe, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_defaults_11;
        tmp_defaults_11 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_11 );
        tmp_assign_source_28 = MAKE_FUNCTION_testpath$asserts$$$function_12_assert_not_ispipe( tmp_defaults_11 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_not_ispipe, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_defaults_12;
        tmp_defaults_12 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_12 );
        tmp_assign_source_29 = MAKE_FUNCTION_testpath$asserts$$$function_13_assert_issocket( tmp_defaults_12 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_issocket, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_defaults_13;
        tmp_defaults_13 = const_tuple_true_none_tuple;
        Py_INCREF( tmp_defaults_13 );
        tmp_assign_source_30 = MAKE_FUNCTION_testpath$asserts$$$function_14_assert_not_issocket( tmp_defaults_13 );



        UPDATE_STRING_DICT1( moduledict_testpath$asserts, (Nuitka_StringObject *)const_str_plain_assert_not_issocket, tmp_assign_source_30 );
    }

    return MOD_RETURN_VALUE( module_testpath$asserts );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
