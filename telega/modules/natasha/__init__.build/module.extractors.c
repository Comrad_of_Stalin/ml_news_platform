/* Generated code for Python module 'extractors'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_extractors" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_extractors;
PyDictObject *moduledict_extractors;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_7a8755ce17f7205c83ae005912c59603;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_index;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_str_plain_match_str_plain_span_str_plain_fact_str_plain_type_tuple;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_normalize_text;
static PyObject *const_tuple_str_plain_self_str_plain_spans_str_plain_markup_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_str_plain_markup;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_digest_83e873120260d3def03f951b7dad8c98;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_get_markup_notebook;
extern PyObject *const_str_digest_b869d9b1f0243a03bd697608af4d0e39;
extern PyObject *const_tuple_str_plain___tuple;
static PyObject *const_str_digest_6974fe154db97d23424e5c78f6bc0e71;
extern PyObject *const_str_plain_RATE;
extern PyObject *const_str_plain_MoneyExtractor;
static PyObject *const_tuple_str_plain_LOCATION_tuple;
static PyObject *const_str_digest_5bf4a2a9706394be9d4440d549a31536;
extern PyObject *const_tuple_str_plain_self_str_plain_index_tuple;
extern PyObject *const_str_plain_get_name_features;
static PyObject *const_str_digest_a24c917b45973000ed35d65e8a8aed02;
extern PyObject *const_str_plain_serialize;
extern PyObject *const_str_plain_spans;
extern PyObject *const_str_plain_TOKENIZER;
static PyObject *const_str_digest_4a078a5d1dd525bba77f16f60697435f;
extern PyObject *const_str_plain_PERSON;
static PyObject *const_str_plain_MONEY_RANGE;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_utils;
static PyObject *const_str_digest_17e5723dff9d43381a69dc3386061511;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_str_plain_TOKENIZER_tuple;
static PyObject *const_tuple_str_plain_normalize_text_tuple;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_de1bf256424628eeb1e24e0b3cdd5144;
extern PyObject *const_str_plain___len__;
static PyObject *const_str_plain_Extractor;
extern PyObject *const_str_plain_AddressExtractor;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple;
extern PyObject *const_str_plain_DATE;
extern PyObject *const_str_plain_tokenizer;
static PyObject *const_str_digest_02080cca8982a13adda93a4c9e5c0090;
extern PyObject *const_str_plain_NAME_MODEL;
extern PyObject *const_str_digest_fc6305b0a92eca56d8c5a8c90bb2b9bc;
extern PyObject *const_str_plain___bool__;
static PyObject *const_str_digest_235c6d8c525921d31b027d6921e962a9;
static PyObject *const_str_digest_032c1b601478e70a93b6e2874f88ecb4;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_digest_28ca5c1e855316b7737046f32ddceb9c;
extern PyObject *const_str_plain_collections;
extern PyObject *const_str_plain_Parser;
extern PyObject *const_str_plain_STREET_MODEL;
extern PyObject *const_str_plain_LocationExtractor;
static PyObject *const_str_digest_949f8580537704fb48aee760d4649a27;
extern PyObject *const_str_plain_tagger;
extern PyObject *const_str_plain_PersonExtractor;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_CrfTagger;
extern PyObject *const_str_plain__repr_html_;
static PyObject *const_tuple_str_plain_get_markup_notebook_tuple;
extern PyObject *const_str_plain_get_street_features;
extern PyObject *const_str_plain_OrganisationExtractor;
static PyObject *const_tuple_str_plain_PERSON_tuple;
extern PyObject *const_str_plain_span;
static PyObject *const_tuple_str_plain_ORGANISATION_tuple;
extern PyObject *const_str_plain_DatesExtractor;
extern PyObject *const_str_plain_parser;
extern PyObject *const_str_plain_match;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_yargy;
extern PyObject *const_str_plain_preprocess;
extern PyObject *const_str_plain_MONEY;
static PyObject *const_str_digest_094816650b7074d09bfcd3300cae3234;
extern PyObject *const_int_0;
static PyObject *const_str_digest_493c8827148ae5bbf6698830439b90e8;
extern PyObject *const_str_plain_NAME;
static PyObject *const_tuple_str_plain_MONEY_str_plain_RATE_str_plain_RANGE_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain___class___tuple;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_plain_origin;
static PyObject *const_tuple_str_plain_can_be_normalized_tuple;
extern PyObject *const_str_plain_OrderedDict;
extern PyObject *const_str_plain_MoneyRangeExtractor;
extern PyObject *const_str_plain_Record;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_angle_listcomp;
static PyObject *const_str_digest_d1428bc079dfd4571773b648e7a68318;
static PyObject *const_tuple_str_plain_self_str_plain_text_str_plain_matches_tuple;
extern PyObject *const_str_plain_SIMPLE_NAME;
extern PyObject *const_str_plain_NamesExtractor;
static PyObject *const_tuple_str_plain_DATE_tuple;
static PyObject *const_str_plain_Matches;
extern PyObject *const_str_plain_extractors;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_type;
static PyObject *const_str_digest_80a15adde89c34e6847d68394c2dc3c7;
extern PyObject *const_str_plain_fact;
extern PyObject *const_str_plain_RANGE;
extern PyObject *const_str_plain_normalized;
extern PyObject *const_str_plain_matches;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain__;
static PyObject *const_str_digest_59b4b535d9d81f63da4ad07e95e34cac;
static PyObject *const_str_digest_968083e00bdde0e0530232a85082bbb8;
extern PyObject *const_str_digest_d2a7a56a4d20853487c008ece870c4f1;
static PyObject *const_str_digest_018bd899a39ebf074e18ff4ddd45794d;
extern PyObject *const_tuple_str_plain_Record_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain_rule;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_tuple_str_plain_SIMPLE_NAME_str_plain_NAME_tuple;
extern PyObject *const_str_digest_f2be9345b65867d2e5ac284a768ec655;
extern PyObject *const_str_plain_findall;
extern PyObject *const_str_plain_ORGANISATION;
static PyObject *const_tuple_str_plain_ADDRESS_tuple;
static PyObject *const_tuple_20b90037841e99e016cf8af8157e0184_tuple;
static PyObject *const_str_digest_258415cdd8d8d9cf8ec18b9871cb6e96;
extern PyObject *const_str_plain_key;
extern PyObject *const_str_plain_MoneyRateExtractor;
static PyObject *const_str_plain_as_html;
extern PyObject *const_str_plain_dsl;
extern PyObject *const_str_plain_as_json;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_list_str_plain_text_str_plain_matches_list;
static PyObject *const_str_digest_a91f498c8a5a67fbdbf118e331d3f2f5;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_76e8d2b8d052b542a756b4c04f0f2b8b;
extern PyObject *const_str_plain_can_be_normalized;
static PyObject *const_str_plain_MONEY_RATE;
extern PyObject *const_str_digest_09460345ee7f4232b97d5c10b6119bc9;
extern PyObject *const_tuple_str_plain_Parser_tuple;
extern PyObject *const_str_digest_754bbdb51d29ec017fab6feb3184da49;
extern PyObject *const_str_plain___call__;
extern PyObject *const_str_plain_ADDRESS;
extern PyObject *const_str_plain_SimpleNamesExtractor;
extern PyObject *const_str_plain___attributes__;
extern PyObject *const_str_plain_LOCATION;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_property;
extern PyObject *const_tuple_str_plain_OrderedDict_tuple;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain___iter__;
extern PyObject *const_str_plain_crf;
static PyObject *const_tuple_efe854522e600ccac4706d77ea2e5556_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_7a8755ce17f7205c83ae005912c59603 = UNSTREAM_STRING_ASCII( &constant_bin[ 666840 ], 27, 0 );
    const_tuple_str_plain_match_str_plain_span_str_plain_fact_str_plain_type_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_match_str_plain_span_str_plain_fact_str_plain_type_tuple, 0, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    PyTuple_SET_ITEM( const_tuple_str_plain_match_str_plain_span_str_plain_fact_str_plain_type_tuple, 1, const_str_plain_span ); Py_INCREF( const_str_plain_span );
    PyTuple_SET_ITEM( const_tuple_str_plain_match_str_plain_span_str_plain_fact_str_plain_type_tuple, 2, const_str_plain_fact ); Py_INCREF( const_str_plain_fact );
    PyTuple_SET_ITEM( const_tuple_str_plain_match_str_plain_span_str_plain_fact_str_plain_type_tuple, 3, const_str_plain_type ); Py_INCREF( const_str_plain_type );
    const_tuple_str_plain_self_str_plain_spans_str_plain_markup_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_spans_str_plain_markup_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_spans_str_plain_markup_tuple, 1, const_str_plain_spans ); Py_INCREF( const_str_plain_spans );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_spans_str_plain_markup_tuple, 2, const_str_plain_markup ); Py_INCREF( const_str_plain_markup );
    const_str_digest_83e873120260d3def03f951b7dad8c98 = UNSTREAM_STRING_ASCII( &constant_bin[ 666867 ], 26, 0 );
    const_str_digest_6974fe154db97d23424e5c78f6bc0e71 = UNSTREAM_STRING_ASCII( &constant_bin[ 666893 ], 29, 0 );
    const_tuple_str_plain_LOCATION_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LOCATION_tuple, 0, const_str_plain_LOCATION ); Py_INCREF( const_str_plain_LOCATION );
    const_str_digest_5bf4a2a9706394be9d4440d549a31536 = UNSTREAM_STRING_ASCII( &constant_bin[ 666922 ], 13, 0 );
    const_str_digest_a24c917b45973000ed35d65e8a8aed02 = UNSTREAM_STRING_ASCII( &constant_bin[ 666935 ], 24, 0 );
    const_str_digest_4a078a5d1dd525bba77f16f60697435f = UNSTREAM_STRING_ASCII( &constant_bin[ 666959 ], 16, 0 );
    const_str_plain_MONEY_RANGE = UNSTREAM_STRING_ASCII( &constant_bin[ 666975 ], 11, 1 );
    const_str_digest_17e5723dff9d43381a69dc3386061511 = UNSTREAM_STRING_ASCII( &constant_bin[ 666986 ], 19, 0 );
    const_tuple_str_plain_TOKENIZER_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_TOKENIZER_tuple, 0, const_str_plain_TOKENIZER ); Py_INCREF( const_str_plain_TOKENIZER );
    const_tuple_str_plain_normalize_text_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_normalize_text_tuple, 0, const_str_plain_normalize_text ); Py_INCREF( const_str_plain_normalize_text );
    const_str_digest_de1bf256424628eeb1e24e0b3cdd5144 = UNSTREAM_STRING_ASCII( &constant_bin[ 667005 ], 19, 0 );
    const_str_plain_Extractor = UNSTREAM_STRING_ASCII( &constant_bin[ 666849 ], 9, 1 );
    const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple, 1, const_str_plain_tagger ); Py_INCREF( const_str_plain_tagger );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple, 2, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_02080cca8982a13adda93a4c9e5c0090 = UNSTREAM_STRING_ASCII( &constant_bin[ 666899 ], 23, 0 );
    const_str_digest_235c6d8c525921d31b027d6921e962a9 = UNSTREAM_STRING_ASCII( &constant_bin[ 667024 ], 30, 0 );
    const_str_digest_032c1b601478e70a93b6e2874f88ecb4 = UNSTREAM_STRING_ASCII( &constant_bin[ 667054 ], 16, 0 );
    const_str_digest_949f8580537704fb48aee760d4649a27 = UNSTREAM_STRING_ASCII( &constant_bin[ 667070 ], 23, 0 );
    const_tuple_str_plain_get_markup_notebook_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_markup_notebook_tuple, 0, const_str_plain_get_markup_notebook ); Py_INCREF( const_str_plain_get_markup_notebook );
    const_tuple_str_plain_PERSON_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PERSON_tuple, 0, const_str_plain_PERSON ); Py_INCREF( const_str_plain_PERSON );
    const_tuple_str_plain_ORGANISATION_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ORGANISATION_tuple, 0, const_str_plain_ORGANISATION ); Py_INCREF( const_str_plain_ORGANISATION );
    const_str_digest_094816650b7074d09bfcd3300cae3234 = UNSTREAM_STRING_ASCII( &constant_bin[ 667093 ], 16, 0 );
    const_str_digest_493c8827148ae5bbf6698830439b90e8 = UNSTREAM_STRING_ASCII( &constant_bin[ 667109 ], 15, 0 );
    const_tuple_str_plain_MONEY_str_plain_RATE_str_plain_RANGE_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_MONEY_str_plain_RATE_str_plain_RANGE_tuple, 0, const_str_plain_MONEY ); Py_INCREF( const_str_plain_MONEY );
    PyTuple_SET_ITEM( const_tuple_str_plain_MONEY_str_plain_RATE_str_plain_RANGE_tuple, 1, const_str_plain_RATE ); Py_INCREF( const_str_plain_RATE );
    PyTuple_SET_ITEM( const_tuple_str_plain_MONEY_str_plain_RATE_str_plain_RANGE_tuple, 2, const_str_plain_RANGE ); Py_INCREF( const_str_plain_RANGE );
    const_tuple_str_plain_can_be_normalized_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_can_be_normalized_tuple, 0, const_str_plain_can_be_normalized ); Py_INCREF( const_str_plain_can_be_normalized );
    const_str_digest_d1428bc079dfd4571773b648e7a68318 = UNSTREAM_STRING_ASCII( &constant_bin[ 667124 ], 25, 0 );
    const_tuple_str_plain_self_str_plain_text_str_plain_matches_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_text_str_plain_matches_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_text_str_plain_matches_tuple, 1, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_text_str_plain_matches_tuple, 2, const_str_plain_matches ); Py_INCREF( const_str_plain_matches );
    const_tuple_str_plain_DATE_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_DATE_tuple, 0, const_str_plain_DATE ); Py_INCREF( const_str_plain_DATE );
    const_str_plain_Matches = UNSTREAM_STRING_ASCII( &constant_bin[ 666959 ], 7, 1 );
    const_str_digest_80a15adde89c34e6847d68394c2dc3c7 = UNSTREAM_STRING_ASCII( &constant_bin[ 667149 ], 23, 0 );
    const_str_digest_59b4b535d9d81f63da4ad07e95e34cac = UNSTREAM_STRING_ASCII( &constant_bin[ 667172 ], 34, 0 );
    const_str_digest_968083e00bdde0e0530232a85082bbb8 = UNSTREAM_STRING_ASCII( &constant_bin[ 667206 ], 15, 0 );
    const_str_digest_018bd899a39ebf074e18ff4ddd45794d = UNSTREAM_STRING_ASCII( &constant_bin[ 666849 ], 18, 0 );
    const_tuple_str_plain_SIMPLE_NAME_str_plain_NAME_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SIMPLE_NAME_str_plain_NAME_tuple, 0, const_str_plain_SIMPLE_NAME ); Py_INCREF( const_str_plain_SIMPLE_NAME );
    PyTuple_SET_ITEM( const_tuple_str_plain_SIMPLE_NAME_str_plain_NAME_tuple, 1, const_str_plain_NAME ); Py_INCREF( const_str_plain_NAME );
    const_tuple_str_plain_ADDRESS_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ADDRESS_tuple, 0, const_str_plain_ADDRESS ); Py_INCREF( const_str_plain_ADDRESS );
    const_tuple_20b90037841e99e016cf8af8157e0184_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_20b90037841e99e016cf8af8157e0184_tuple, 0, const_str_plain_CrfTagger ); Py_INCREF( const_str_plain_CrfTagger );
    PyTuple_SET_ITEM( const_tuple_20b90037841e99e016cf8af8157e0184_tuple, 1, const_str_plain_STREET_MODEL ); Py_INCREF( const_str_plain_STREET_MODEL );
    PyTuple_SET_ITEM( const_tuple_20b90037841e99e016cf8af8157e0184_tuple, 2, const_str_plain_get_street_features ); Py_INCREF( const_str_plain_get_street_features );
    PyTuple_SET_ITEM( const_tuple_20b90037841e99e016cf8af8157e0184_tuple, 3, const_str_plain_NAME_MODEL ); Py_INCREF( const_str_plain_NAME_MODEL );
    PyTuple_SET_ITEM( const_tuple_20b90037841e99e016cf8af8157e0184_tuple, 4, const_str_plain_get_name_features ); Py_INCREF( const_str_plain_get_name_features );
    const_str_digest_258415cdd8d8d9cf8ec18b9871cb6e96 = UNSTREAM_STRING_ASCII( &constant_bin[ 667221 ], 18, 0 );
    const_str_plain_as_html = UNSTREAM_STRING_ASCII( &constant_bin[ 667239 ], 7, 1 );
    const_list_str_plain_text_str_plain_matches_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_text_str_plain_matches_list, 0, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyList_SET_ITEM( const_list_str_plain_text_str_plain_matches_list, 1, const_str_plain_matches ); Py_INCREF( const_str_plain_matches );
    const_str_digest_a91f498c8a5a67fbdbf118e331d3f2f5 = UNSTREAM_STRING_ASCII( &constant_bin[ 667246 ], 28, 0 );
    const_str_digest_76e8d2b8d052b542a756b4c04f0f2b8b = UNSTREAM_STRING_ASCII( &constant_bin[ 667274 ], 19, 0 );
    const_str_plain_MONEY_RATE = UNSTREAM_STRING_ASCII( &constant_bin[ 667293 ], 10, 1 );
    const_tuple_efe854522e600ccac4706d77ea2e5556_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_efe854522e600ccac4706d77ea2e5556_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_efe854522e600ccac4706d77ea2e5556_tuple, 1, const_str_plain_rule ); Py_INCREF( const_str_plain_rule );
    PyTuple_SET_ITEM( const_tuple_efe854522e600ccac4706d77ea2e5556_tuple, 2, const_str_plain_tokenizer ); Py_INCREF( const_str_plain_tokenizer );
    PyTuple_SET_ITEM( const_tuple_efe854522e600ccac4706d77ea2e5556_tuple, 3, const_str_plain_tagger ); Py_INCREF( const_str_plain_tagger );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_extractors( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_ef2246482b1c0bc696917ea9e27d2385;
static PyCodeObject *codeobj_22c18be366d2cb10bb659412a89e98d4;
static PyCodeObject *codeobj_3b0a39a6441e72d49a6ef0e0144956ef;
static PyCodeObject *codeobj_90775d2cb57f1deebc6c000eaebfc0ab;
static PyCodeObject *codeobj_67f4e11334b14bcc38f1503fd88ebbe9;
static PyCodeObject *codeobj_30e6bfaa91e1fe1699cd0d1f3701d474;
static PyCodeObject *codeobj_46a8fcf03fef20828841399f33983311;
static PyCodeObject *codeobj_234de8d20548cbb66367c1849eae78b7;
static PyCodeObject *codeobj_dca7af48eb89b217f352818e300bd4eb;
static PyCodeObject *codeobj_c193d8406a0c58560a78960b7c28d662;
static PyCodeObject *codeobj_a41ed17644233d58ca4331a71292e9ee;
static PyCodeObject *codeobj_09787e6b9a91e7687ca4e65eb22138ad;
static PyCodeObject *codeobj_8622361ddd34898470abb18eba55960a;
static PyCodeObject *codeobj_4791fd6c754f72a7bb928a049a6ef26d;
static PyCodeObject *codeobj_adec1ac16e1ded723ba15306b0680e21;
static PyCodeObject *codeobj_1443d96cbf56a5429511b6be48c0967d;
static PyCodeObject *codeobj_9845148c54d14eb362538d5109eaba5a;
static PyCodeObject *codeobj_d98cdbfbba03de8c57d0034eda1e1322;
static PyCodeObject *codeobj_e5f08ba339e3be5f023758c864399b8d;
static PyCodeObject *codeobj_f4ddace6a23b56a5c123e81367e43f67;
static PyCodeObject *codeobj_e482d0a083a8d5083009eb13a00f5c44;
static PyCodeObject *codeobj_2a31304be7c4eda2980beeb162ceb6a9;
static PyCodeObject *codeobj_377d1b82a16c6bd5a43c4a811a42e14c;
static PyCodeObject *codeobj_cec9f570a4528a5025e67dd8e0635f81;
static PyCodeObject *codeobj_010b0a082fd533c1a869d90fdf922aaf;
static PyCodeObject *codeobj_d8902479b79e76e9acccb888184746ee;
static PyCodeObject *codeobj_eb591d8b86d75b02c5ff881b29d05a79;
static PyCodeObject *codeobj_bbc093b62d27feba681e8d45cf8cf221;
static PyCodeObject *codeobj_ed80cb50da18e4f1ff553f4dbb6dc9b3;
static PyCodeObject *codeobj_172f1fb96ddce7923ee472843aa4e313;
static PyCodeObject *codeobj_bcea94b0c6934dc87211bd1e5b8ad491;
static PyCodeObject *codeobj_ff10c1783cc6922768ae8684086f4412;
static PyCodeObject *codeobj_fe66caf202d9d48de5fc1f3f6edd2cf4;
static PyCodeObject *codeobj_d19e1fc688c45b0dc836fcb7b7ee81e7;
static PyCodeObject *codeobj_55bfd59a8f126110783324f26ab466c7;
static PyCodeObject *codeobj_173e07ebc9698c39d13226b10c6ccfc2;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_5bf4a2a9706394be9d4440d549a31536 );
    codeobj_ef2246482b1c0bc696917ea9e27d2385 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 60, const_tuple_str_plain___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_22c18be366d2cb10bb659412a89e98d4 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 76, const_tuple_str_plain___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3b0a39a6441e72d49a6ef0e0144956ef = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 79, const_tuple_str_plain___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_90775d2cb57f1deebc6c000eaebfc0ab = MAKE_CODEOBJ( module_filename_obj, const_str_digest_17e5723dff9d43381a69dc3386061511, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_67f4e11334b14bcc38f1503fd88ebbe9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AddressExtractor, 143, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_30e6bfaa91e1fe1699cd0d1f3701d474 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_DatesExtractor, 123, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_46a8fcf03fef20828841399f33983311 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Extractor, 84, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_234de8d20548cbb66367c1849eae78b7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_LocationExtractor, 155, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dca7af48eb89b217f352818e300bd4eb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Matches, 55, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c193d8406a0c58560a78960b7c28d662 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MoneyExtractor, 128, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a41ed17644233d58ca4331a71292e9ee = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MoneyRangeExtractor, 138, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_09787e6b9a91e7687ca4e65eb22138ad = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MoneyRateExtractor, 133, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8622361ddd34898470abb18eba55960a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_NamesExtractor, 94, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4791fd6c754f72a7bb928a049a6ef26d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_OrganisationExtractor, 160, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_adec1ac16e1ded723ba15306b0680e21 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_PersonExtractor, 111, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1443d96cbf56a5429511b6be48c0967d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_SimpleNamesExtractor, 106, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9845148c54d14eb362538d5109eaba5a = MAKE_CODEOBJ( module_filename_obj, const_str_plain___bool__, 71, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d98cdbfbba03de8c57d0034eda1e1322 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___call__, 88, const_tuple_str_plain_self_str_plain_text_str_plain_matches_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e5f08ba339e3be5f023758c864399b8d = MAKE_CODEOBJ( module_filename_obj, const_str_plain___getitem__, 65, const_tuple_str_plain_self_str_plain_index_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f4ddace6a23b56a5c123e81367e43f67 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 107, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e482d0a083a8d5083009eb13a00f5c44 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 124, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2a31304be7c4eda2980beeb162ceb6a9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 129, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_377d1b82a16c6bd5a43c4a811a42e14c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 134, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_cec9f570a4528a5025e67dd8e0635f81 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 139, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_010b0a082fd533c1a869d90fdf922aaf = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 156, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d8902479b79e76e9acccb888184746ee = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 161, const_tuple_str_plain_self_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_eb591d8b86d75b02c5ff881b29d05a79 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 85, const_tuple_efe854522e600ccac4706d77ea2e5556_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bbc093b62d27feba681e8d45cf8cf221 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 95, const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ed80cb50da18e4f1ff553f4dbb6dc9b3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 112, const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_172f1fb96ddce7923ee472843aa4e313 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 144, const_tuple_str_plain_self_str_plain_tagger_str_plain___class___tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bcea94b0c6934dc87211bd1e5b8ad491 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 58, const_tuple_str_plain_self_str_plain_text_str_plain_matches_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ff10c1783cc6922768ae8684086f4412 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___iter__, 62, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_fe66caf202d9d48de5fc1f3f6edd2cf4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___len__, 68, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d19e1fc688c45b0dc836fcb7b7ee81e7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__repr_html_, 78, const_tuple_str_plain_self_str_plain_spans_str_plain_markup_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_55bfd59a8f126110783324f26ab466c7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_as_json, 74, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_173e07ebc9698c39d13226b10c6ccfc2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_serialize, 42, const_tuple_str_plain_match_str_plain_span_str_plain_fact_str_plain_type_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_extractors$$$function_10___call__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_11___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_12___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_13___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_14___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_15___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_16___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_17___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_18___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_19___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_1_serialize(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_20___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_2___init__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_2___init__$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_3___iter__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_4___getitem__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_5___len__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_6___bool__(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_7_as_json(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_8__repr_html_(  );


static PyObject *MAKE_FUNCTION_extractors$$$function_9___init__( PyObject *defaults );


// The module function definitions.
static PyObject *impl_extractors$$$function_1_serialize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_match = python_pars[ 0 ];
    PyObject *var_span = NULL;
    PyObject *var_fact = NULL;
    PyObject *var_type = NULL;
    struct Nuitka_FrameObject *frame_173e07ebc9698c39d13226b10c6ccfc2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_173e07ebc9698c39d13226b10c6ccfc2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_173e07ebc9698c39d13226b10c6ccfc2, codeobj_173e07ebc9698c39d13226b10c6ccfc2, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_173e07ebc9698c39d13226b10c6ccfc2 = cache_frame_173e07ebc9698c39d13226b10c6ccfc2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_173e07ebc9698c39d13226b10c6ccfc2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_173e07ebc9698c39d13226b10c6ccfc2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_match );
        tmp_source_name_1 = par_match;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_span );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_span == NULL );
        var_span = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_match );
        tmp_source_name_2 = par_match;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fact );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_fact == NULL );
        var_fact = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_can_be_normalized );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_can_be_normalized );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "can_be_normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_fact );
        tmp_args_element_name_1 = var_fact;
        frame_173e07ebc9698c39d13226b10c6ccfc2->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 45;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( var_fact );
            tmp_source_name_3 = var_fact;
            tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_normalized );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_fact;
                assert( old != NULL );
                var_fact = tmp_assign_source_3;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_fact );
        tmp_source_name_5 = var_fact;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_5 );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_type == NULL );
        var_type = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_tuple_element_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_OrderedDict );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_tuple_element_1 = const_str_plain_type;
        tmp_list_element_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_type );
        tmp_tuple_element_1 = var_type;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_1 );
        tmp_args_element_name_2 = PyList_New( 3 );
        PyList_SET_ITEM( tmp_args_element_name_2, 0, tmp_list_element_1 );
        tmp_tuple_element_2 = const_str_plain_fact;
        tmp_list_element_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( var_fact );
        tmp_source_name_6 = var_fact;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_as_json );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 50;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_2 );
        PyList_SET_ITEM( tmp_args_element_name_2, 1, tmp_list_element_1 );
        tmp_tuple_element_3 = const_str_plain_span;
        tmp_list_element_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( var_span );
        tmp_tuple_element_3 = var_span;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_3 );
        PyList_SET_ITEM( tmp_args_element_name_2, 2, tmp_list_element_1 );
        frame_173e07ebc9698c39d13226b10c6ccfc2->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_173e07ebc9698c39d13226b10c6ccfc2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_173e07ebc9698c39d13226b10c6ccfc2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_173e07ebc9698c39d13226b10c6ccfc2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_173e07ebc9698c39d13226b10c6ccfc2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_173e07ebc9698c39d13226b10c6ccfc2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_173e07ebc9698c39d13226b10c6ccfc2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_173e07ebc9698c39d13226b10c6ccfc2,
        type_description_1,
        par_match,
        var_span,
        var_fact,
        var_type
    );


    // Release cached frame.
    if ( frame_173e07ebc9698c39d13226b10c6ccfc2 == cache_frame_173e07ebc9698c39d13226b10c6ccfc2 )
    {
        Py_DECREF( frame_173e07ebc9698c39d13226b10c6ccfc2 );
    }
    cache_frame_173e07ebc9698c39d13226b10c6ccfc2 = NULL;

    assertFrameObject( frame_173e07ebc9698c39d13226b10c6ccfc2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_1_serialize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_match );
    Py_DECREF( par_match );
    par_match = NULL;

    CHECK_OBJECT( (PyObject *)var_span );
    Py_DECREF( var_span );
    var_span = NULL;

    CHECK_OBJECT( (PyObject *)var_fact );
    Py_DECREF( var_fact );
    var_fact = NULL;

    CHECK_OBJECT( (PyObject *)var_type );
    Py_DECREF( var_type );
    var_type = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_match );
    Py_DECREF( par_match );
    par_match = NULL;

    Py_XDECREF( var_span );
    var_span = NULL;

    Py_XDECREF( var_fact );
    var_fact = NULL;

    Py_XDECREF( var_type );
    var_type = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_1_serialize );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_2___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *par_matches = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_bcea94b0c6934dc87211bd1e5b8ad491;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_bcea94b0c6934dc87211bd1e5b8ad491 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bcea94b0c6934dc87211bd1e5b8ad491, codeobj_bcea94b0c6934dc87211bd1e5b8ad491, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bcea94b0c6934dc87211bd1e5b8ad491 = cache_frame_bcea94b0c6934dc87211bd1e5b8ad491;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bcea94b0c6934dc87211bd1e5b8ad491 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bcea94b0c6934dc87211bd1e5b8ad491 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_text );
        tmp_assattr_name_1 = par_text;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_text, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_assattr_target_2;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( par_matches );
        tmp_tuple_element_1 = par_matches;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_key;
        tmp_dict_value_1 = MAKE_FUNCTION_extractors$$$function_2___init__$$$function_1_lambda(  );



        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_bcea94b0c6934dc87211bd1e5b8ad491->m_frame.f_lineno = 60;
        tmp_assattr_name_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_matches, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bcea94b0c6934dc87211bd1e5b8ad491 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bcea94b0c6934dc87211bd1e5b8ad491 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bcea94b0c6934dc87211bd1e5b8ad491, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bcea94b0c6934dc87211bd1e5b8ad491->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bcea94b0c6934dc87211bd1e5b8ad491, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bcea94b0c6934dc87211bd1e5b8ad491,
        type_description_1,
        par_self,
        par_text,
        par_matches
    );


    // Release cached frame.
    if ( frame_bcea94b0c6934dc87211bd1e5b8ad491 == cache_frame_bcea94b0c6934dc87211bd1e5b8ad491 )
    {
        Py_DECREF( frame_bcea94b0c6934dc87211bd1e5b8ad491 );
    }
    cache_frame_bcea94b0c6934dc87211bd1e5b8ad491 = NULL;

    assertFrameObject( frame_bcea94b0c6934dc87211bd1e5b8ad491 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_2___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_matches );
    Py_DECREF( par_matches );
    par_matches = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_matches );
    Py_DECREF( par_matches );
    par_matches = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_2___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_2___init__$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par__ = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ef2246482b1c0bc696917ea9e27d2385;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ef2246482b1c0bc696917ea9e27d2385 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ef2246482b1c0bc696917ea9e27d2385, codeobj_ef2246482b1c0bc696917ea9e27d2385, module_extractors, sizeof(void *) );
    frame_ef2246482b1c0bc696917ea9e27d2385 = cache_frame_ef2246482b1c0bc696917ea9e27d2385;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ef2246482b1c0bc696917ea9e27d2385 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ef2246482b1c0bc696917ea9e27d2385 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par__ );
        tmp_source_name_1 = par__;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_span );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef2246482b1c0bc696917ea9e27d2385 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef2246482b1c0bc696917ea9e27d2385 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ef2246482b1c0bc696917ea9e27d2385 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ef2246482b1c0bc696917ea9e27d2385, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ef2246482b1c0bc696917ea9e27d2385->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ef2246482b1c0bc696917ea9e27d2385, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ef2246482b1c0bc696917ea9e27d2385,
        type_description_1,
        par__
    );


    // Release cached frame.
    if ( frame_ef2246482b1c0bc696917ea9e27d2385 == cache_frame_ef2246482b1c0bc696917ea9e27d2385 )
    {
        Py_DECREF( frame_ef2246482b1c0bc696917ea9e27d2385 );
    }
    cache_frame_ef2246482b1c0bc696917ea9e27d2385 = NULL;

    assertFrameObject( frame_ef2246482b1c0bc696917ea9e27d2385 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_2___init__$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_2___init__$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_3___iter__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ff10c1783cc6922768ae8684086f4412;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ff10c1783cc6922768ae8684086f4412 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ff10c1783cc6922768ae8684086f4412, codeobj_ff10c1783cc6922768ae8684086f4412, module_extractors, sizeof(void *) );
    frame_ff10c1783cc6922768ae8684086f4412 = cache_frame_ff10c1783cc6922768ae8684086f4412;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ff10c1783cc6922768ae8684086f4412 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ff10c1783cc6922768ae8684086f4412 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_matches );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff10c1783cc6922768ae8684086f4412 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff10c1783cc6922768ae8684086f4412 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff10c1783cc6922768ae8684086f4412 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ff10c1783cc6922768ae8684086f4412, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ff10c1783cc6922768ae8684086f4412->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ff10c1783cc6922768ae8684086f4412, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ff10c1783cc6922768ae8684086f4412,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_ff10c1783cc6922768ae8684086f4412 == cache_frame_ff10c1783cc6922768ae8684086f4412 )
    {
        Py_DECREF( frame_ff10c1783cc6922768ae8684086f4412 );
    }
    cache_frame_ff10c1783cc6922768ae8684086f4412 = NULL;

    assertFrameObject( frame_ff10c1783cc6922768ae8684086f4412 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_3___iter__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_3___iter__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_4___getitem__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_index = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_e5f08ba339e3be5f023758c864399b8d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e5f08ba339e3be5f023758c864399b8d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e5f08ba339e3be5f023758c864399b8d, codeobj_e5f08ba339e3be5f023758c864399b8d, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_e5f08ba339e3be5f023758c864399b8d = cache_frame_e5f08ba339e3be5f023758c864399b8d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e5f08ba339e3be5f023758c864399b8d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e5f08ba339e3be5f023758c864399b8d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_matches );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_index );
        tmp_subscript_name_1 = par_index;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5f08ba339e3be5f023758c864399b8d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5f08ba339e3be5f023758c864399b8d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5f08ba339e3be5f023758c864399b8d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e5f08ba339e3be5f023758c864399b8d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e5f08ba339e3be5f023758c864399b8d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e5f08ba339e3be5f023758c864399b8d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e5f08ba339e3be5f023758c864399b8d,
        type_description_1,
        par_self,
        par_index
    );


    // Release cached frame.
    if ( frame_e5f08ba339e3be5f023758c864399b8d == cache_frame_e5f08ba339e3be5f023758c864399b8d )
    {
        Py_DECREF( frame_e5f08ba339e3be5f023758c864399b8d );
    }
    cache_frame_e5f08ba339e3be5f023758c864399b8d = NULL;

    assertFrameObject( frame_e5f08ba339e3be5f023758c864399b8d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_4___getitem__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_index );
    Py_DECREF( par_index );
    par_index = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_index );
    Py_DECREF( par_index );
    par_index = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_4___getitem__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_5___len__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_fe66caf202d9d48de5fc1f3f6edd2cf4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_fe66caf202d9d48de5fc1f3f6edd2cf4 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_fe66caf202d9d48de5fc1f3f6edd2cf4, codeobj_fe66caf202d9d48de5fc1f3f6edd2cf4, module_extractors, sizeof(void *) );
    frame_fe66caf202d9d48de5fc1f3f6edd2cf4 = cache_frame_fe66caf202d9d48de5fc1f3f6edd2cf4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_matches );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_fe66caf202d9d48de5fc1f3f6edd2cf4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_fe66caf202d9d48de5fc1f3f6edd2cf4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_fe66caf202d9d48de5fc1f3f6edd2cf4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_fe66caf202d9d48de5fc1f3f6edd2cf4,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 == cache_frame_fe66caf202d9d48de5fc1f3f6edd2cf4 )
    {
        Py_DECREF( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 );
    }
    cache_frame_fe66caf202d9d48de5fc1f3f6edd2cf4 = NULL;

    assertFrameObject( frame_fe66caf202d9d48de5fc1f3f6edd2cf4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_5___len__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_5___len__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_6___bool__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9845148c54d14eb362538d5109eaba5a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_9845148c54d14eb362538d5109eaba5a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9845148c54d14eb362538d5109eaba5a, codeobj_9845148c54d14eb362538d5109eaba5a, module_extractors, sizeof(void *) );
    frame_9845148c54d14eb362538d5109eaba5a = cache_frame_9845148c54d14eb362538d5109eaba5a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9845148c54d14eb362538d5109eaba5a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9845148c54d14eb362538d5109eaba5a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_value_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_value_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_matches );
        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9845148c54d14eb362538d5109eaba5a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9845148c54d14eb362538d5109eaba5a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9845148c54d14eb362538d5109eaba5a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9845148c54d14eb362538d5109eaba5a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9845148c54d14eb362538d5109eaba5a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9845148c54d14eb362538d5109eaba5a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9845148c54d14eb362538d5109eaba5a,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_9845148c54d14eb362538d5109eaba5a == cache_frame_9845148c54d14eb362538d5109eaba5a )
    {
        Py_DECREF( frame_9845148c54d14eb362538d5109eaba5a );
    }
    cache_frame_9845148c54d14eb362538d5109eaba5a = NULL;

    assertFrameObject( frame_9845148c54d14eb362538d5109eaba5a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_6___bool__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_6___bool__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_7_as_json( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *outline_0_var__ = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_55bfd59a8f126110783324f26ab466c7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_22c18be366d2cb10bb659412a89e98d4_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_22c18be366d2cb10bb659412a89e98d4_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_55bfd59a8f126110783324f26ab466c7 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_55bfd59a8f126110783324f26ab466c7, codeobj_55bfd59a8f126110783324f26ab466c7, module_extractors, sizeof(void *) );
    frame_55bfd59a8f126110783324f26ab466c7 = cache_frame_55bfd59a8f126110783324f26ab466c7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_55bfd59a8f126110783324f26ab466c7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_55bfd59a8f126110783324f26ab466c7 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_matches );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_2;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_22c18be366d2cb10bb659412a89e98d4_2, codeobj_22c18be366d2cb10bb659412a89e98d4, module_extractors, sizeof(void *) );
    frame_22c18be366d2cb10bb659412a89e98d4_2 = cache_frame_22c18be366d2cb10bb659412a89e98d4_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_22c18be366d2cb10bb659412a89e98d4_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_22c18be366d2cb10bb659412a89e98d4_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "o";
                exception_lineno = 76;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var__;
            outline_0_var__ = tmp_assign_source_4;
            Py_INCREF( outline_0_var__ );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_append_list_1;
        PyObject *tmp_append_value_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_append_list_1 = tmp_listcomp_1__contraction;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_serialize );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_serialize );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "serialize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;
            type_description_2 = "o";
            goto try_except_handler_3;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( outline_0_var__ );
        tmp_args_element_name_1 = outline_0_var__;
        frame_22c18be366d2cb10bb659412a89e98d4_2->m_frame.f_lineno = 76;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_append_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        assert( PyList_Check( tmp_append_list_1 ) );
        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
        Py_DECREF( tmp_append_value_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        type_description_2 = "o";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_7_as_json );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22c18be366d2cb10bb659412a89e98d4_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_22c18be366d2cb10bb659412a89e98d4_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22c18be366d2cb10bb659412a89e98d4_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_22c18be366d2cb10bb659412a89e98d4_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_22c18be366d2cb10bb659412a89e98d4_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_22c18be366d2cb10bb659412a89e98d4_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_22c18be366d2cb10bb659412a89e98d4_2,
        type_description_2,
        outline_0_var__
    );


    // Release cached frame.
    if ( frame_22c18be366d2cb10bb659412a89e98d4_2 == cache_frame_22c18be366d2cb10bb659412a89e98d4_2 )
    {
        Py_DECREF( frame_22c18be366d2cb10bb659412a89e98d4_2 );
    }
    cache_frame_22c18be366d2cb10bb659412a89e98d4_2 = NULL;

    assertFrameObject( frame_22c18be366d2cb10bb659412a89e98d4_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "o";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_7_as_json );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var__ );
    outline_0_var__ = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var__ );
    outline_0_var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_7_as_json );
    return NULL;
    outline_exception_1:;
    exception_lineno = 76;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_55bfd59a8f126110783324f26ab466c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_55bfd59a8f126110783324f26ab466c7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_55bfd59a8f126110783324f26ab466c7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_55bfd59a8f126110783324f26ab466c7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_55bfd59a8f126110783324f26ab466c7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_55bfd59a8f126110783324f26ab466c7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_55bfd59a8f126110783324f26ab466c7,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_55bfd59a8f126110783324f26ab466c7 == cache_frame_55bfd59a8f126110783324f26ab466c7 )
    {
        Py_DECREF( frame_55bfd59a8f126110783324f26ab466c7 );
    }
    cache_frame_55bfd59a8f126110783324f26ab466c7 = NULL;

    assertFrameObject( frame_55bfd59a8f126110783324f26ab466c7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_7_as_json );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_7_as_json );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_8__repr_html_( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_spans = NULL;
    PyObject *var_markup = NULL;
    PyObject *outline_0_var__ = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_d19e1fc688c45b0dc836fcb7b7ee81e7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_3b0a39a6441e72d49a6ef0e0144956ef_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_3b0a39a6441e72d49a6ef0e0144956ef_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d19e1fc688c45b0dc836fcb7b7ee81e7 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d19e1fc688c45b0dc836fcb7b7ee81e7, codeobj_d19e1fc688c45b0dc836fcb7b7ee81e7, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d19e1fc688c45b0dc836fcb7b7ee81e7 = cache_frame_d19e1fc688c45b0dc836fcb7b7ee81e7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_1 = par_self;
            tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_matches );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3b0a39a6441e72d49a6ef0e0144956ef_2, codeobj_3b0a39a6441e72d49a6ef0e0144956ef, module_extractors, sizeof(void *) );
        frame_3b0a39a6441e72d49a6ef0e0144956ef_2 = cache_frame_3b0a39a6441e72d49a6ef0e0144956ef_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 79;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var__;
                outline_0_var__ = tmp_assign_source_5;
                Py_INCREF( outline_0_var__ );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var__ );
            tmp_source_name_2 = outline_0_var__;
            tmp_append_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_span );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_1 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors$$$function_8__repr_html_ );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3b0a39a6441e72d49a6ef0e0144956ef_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3b0a39a6441e72d49a6ef0e0144956ef_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3b0a39a6441e72d49a6ef0e0144956ef_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3b0a39a6441e72d49a6ef0e0144956ef_2,
            type_description_2,
            outline_0_var__
        );


        // Release cached frame.
        if ( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 == cache_frame_3b0a39a6441e72d49a6ef0e0144956ef_2 )
        {
            Py_DECREF( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 );
        }
        cache_frame_3b0a39a6441e72d49a6ef0e0144956ef_2 = NULL;

        assertFrameObject( frame_3b0a39a6441e72d49a6ef0e0144956ef_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors$$$function_8__repr_html_ );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var__ );
        outline_0_var__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var__ );
        outline_0_var__ = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors$$$function_8__repr_html_ );
        return NULL;
        outline_exception_1:;
        exception_lineno = 79;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_spans == NULL );
        var_spans = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_get_markup_notebook );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_markup_notebook );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_markup_notebook" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_text );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_spans );
        tmp_args_element_name_2 = var_spans;
        frame_d19e1fc688c45b0dc836fcb7b7ee81e7->m_frame.f_lineno = 80;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_markup == NULL );
        var_markup = tmp_assign_source_6;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_5;
        tmp_source_name_4 = const_str_empty;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_join );
        assert( !(tmp_called_name_2 == NULL) );
        CHECK_OBJECT( var_markup );
        tmp_source_name_5 = var_markup;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_as_html );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 81;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_d19e1fc688c45b0dc836fcb7b7ee81e7->m_frame.f_lineno = 81;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d19e1fc688c45b0dc836fcb7b7ee81e7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d19e1fc688c45b0dc836fcb7b7ee81e7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d19e1fc688c45b0dc836fcb7b7ee81e7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d19e1fc688c45b0dc836fcb7b7ee81e7,
        type_description_1,
        par_self,
        var_spans,
        var_markup
    );


    // Release cached frame.
    if ( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 == cache_frame_d19e1fc688c45b0dc836fcb7b7ee81e7 )
    {
        Py_DECREF( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 );
    }
    cache_frame_d19e1fc688c45b0dc836fcb7b7ee81e7 = NULL;

    assertFrameObject( frame_d19e1fc688c45b0dc836fcb7b7ee81e7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_8__repr_html_ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_spans );
    Py_DECREF( var_spans );
    var_spans = NULL;

    CHECK_OBJECT( (PyObject *)var_markup );
    Py_DECREF( var_markup );
    var_markup = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_spans );
    var_spans = NULL;

    Py_XDECREF( var_markup );
    var_markup = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_8__repr_html_ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_9___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_rule = python_pars[ 1 ];
    PyObject *par_tokenizer = python_pars[ 2 ];
    PyObject *par_tagger = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_eb591d8b86d75b02c5ff881b29d05a79;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_eb591d8b86d75b02c5ff881b29d05a79 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eb591d8b86d75b02c5ff881b29d05a79, codeobj_eb591d8b86d75b02c5ff881b29d05a79, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_eb591d8b86d75b02c5ff881b29d05a79 = cache_frame_eb591d8b86d75b02c5ff881b29d05a79;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eb591d8b86d75b02c5ff881b29d05a79 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eb591d8b86d75b02c5ff881b29d05a79 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Parser );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Parser );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Parser" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_rule );
        tmp_tuple_element_1 = par_rule;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_tokenizer;
        CHECK_OBJECT( par_tokenizer );
        tmp_dict_value_1 = par_tokenizer;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_tagger;
        CHECK_OBJECT( par_tagger );
        tmp_dict_value_2 = par_tagger;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_eb591d8b86d75b02c5ff881b29d05a79->m_frame.f_lineno = 86;
        tmp_assattr_name_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_parser, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb591d8b86d75b02c5ff881b29d05a79 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb591d8b86d75b02c5ff881b29d05a79 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eb591d8b86d75b02c5ff881b29d05a79, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eb591d8b86d75b02c5ff881b29d05a79->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb591d8b86d75b02c5ff881b29d05a79, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eb591d8b86d75b02c5ff881b29d05a79,
        type_description_1,
        par_self,
        par_rule,
        par_tokenizer,
        par_tagger
    );


    // Release cached frame.
    if ( frame_eb591d8b86d75b02c5ff881b29d05a79 == cache_frame_eb591d8b86d75b02c5ff881b29d05a79 )
    {
        Py_DECREF( frame_eb591d8b86d75b02c5ff881b29d05a79 );
    }
    cache_frame_eb591d8b86d75b02c5ff881b29d05a79 = NULL;

    assertFrameObject( frame_eb591d8b86d75b02c5ff881b29d05a79 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_9___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_rule );
    Py_DECREF( par_rule );
    par_rule = NULL;

    CHECK_OBJECT( (PyObject *)par_tokenizer );
    Py_DECREF( par_tokenizer );
    par_tokenizer = NULL;

    CHECK_OBJECT( (PyObject *)par_tagger );
    Py_DECREF( par_tagger );
    par_tagger = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_rule );
    Py_DECREF( par_rule );
    par_rule = NULL;

    CHECK_OBJECT( (PyObject *)par_tokenizer );
    Py_DECREF( par_tokenizer );
    par_tokenizer = NULL;

    CHECK_OBJECT( (PyObject *)par_tagger );
    Py_DECREF( par_tagger );
    par_tagger = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_9___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_10___call__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_text = python_pars[ 1 ];
    PyObject *var_matches = NULL;
    struct Nuitka_FrameObject *frame_d98cdbfbba03de8c57d0034eda1e1322;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d98cdbfbba03de8c57d0034eda1e1322 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d98cdbfbba03de8c57d0034eda1e1322, codeobj_d98cdbfbba03de8c57d0034eda1e1322, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d98cdbfbba03de8c57d0034eda1e1322 = cache_frame_d98cdbfbba03de8c57d0034eda1e1322;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d98cdbfbba03de8c57d0034eda1e1322 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d98cdbfbba03de8c57d0034eda1e1322 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_normalize_text );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalize_text );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalize_text" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_d98cdbfbba03de8c57d0034eda1e1322->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_text;
            assert( old != NULL );
            par_text = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parser );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_args_element_name_2 = par_text;
        frame_d98cdbfbba03de8c57d0034eda1e1322->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_findall, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_matches == NULL );
        var_matches = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Matches );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Matches );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Matches" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_3 = par_text;
        CHECK_OBJECT( var_matches );
        tmp_args_element_name_4 = var_matches;
        frame_d98cdbfbba03de8c57d0034eda1e1322->m_frame.f_lineno = 91;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d98cdbfbba03de8c57d0034eda1e1322 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d98cdbfbba03de8c57d0034eda1e1322 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d98cdbfbba03de8c57d0034eda1e1322 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d98cdbfbba03de8c57d0034eda1e1322, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d98cdbfbba03de8c57d0034eda1e1322->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d98cdbfbba03de8c57d0034eda1e1322, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d98cdbfbba03de8c57d0034eda1e1322,
        type_description_1,
        par_self,
        par_text,
        var_matches
    );


    // Release cached frame.
    if ( frame_d98cdbfbba03de8c57d0034eda1e1322 == cache_frame_d98cdbfbba03de8c57d0034eda1e1322 )
    {
        Py_DECREF( frame_d98cdbfbba03de8c57d0034eda1e1322 );
    }
    cache_frame_d98cdbfbba03de8c57d0034eda1e1322 = NULL;

    assertFrameObject( frame_d98cdbfbba03de8c57d0034eda1e1322 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_10___call__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)var_matches );
    Py_DECREF( var_matches );
    var_matches = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_matches );
    var_matches = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_10___call__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_11___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_tagger = NULL;
    struct Nuitka_FrameObject *frame_bbc093b62d27feba681e8d45cf8cf221;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_bbc093b62d27feba681e8d45cf8cf221 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bbc093b62d27feba681e8d45cf8cf221, codeobj_bbc093b62d27feba681e8d45cf8cf221, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bbc093b62d27feba681e8d45cf8cf221 = cache_frame_bbc093b62d27feba681e8d45cf8cf221;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bbc093b62d27feba681e8d45cf8cf221 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bbc093b62d27feba681e8d45cf8cf221 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_CrfTagger );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CrfTagger );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CrfTagger" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_NAME_MODEL );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME_MODEL );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME_MODEL" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_get_name_features );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_name_features );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_name_features" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_3;
        frame_bbc093b62d27feba681e8d45cf8cf221->m_frame.f_lineno = 96;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        assert( var_tagger == NULL );
        var_tagger = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_NamesExtractor );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NamesExtractor );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NamesExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_4;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_NAME );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = tmp_mvar_value_5;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_tagger;
        CHECK_OBJECT( var_tagger );
        tmp_dict_value_1 = var_tagger;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_bbc093b62d27feba681e8d45cf8cf221->m_frame.f_lineno = 100;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bbc093b62d27feba681e8d45cf8cf221 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bbc093b62d27feba681e8d45cf8cf221 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bbc093b62d27feba681e8d45cf8cf221, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bbc093b62d27feba681e8d45cf8cf221->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bbc093b62d27feba681e8d45cf8cf221, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bbc093b62d27feba681e8d45cf8cf221,
        type_description_1,
        par_self,
        var_tagger,
        NULL
    );


    // Release cached frame.
    if ( frame_bbc093b62d27feba681e8d45cf8cf221 == cache_frame_bbc093b62d27feba681e8d45cf8cf221 )
    {
        Py_DECREF( frame_bbc093b62d27feba681e8d45cf8cf221 );
    }
    cache_frame_bbc093b62d27feba681e8d45cf8cf221 = NULL;

    assertFrameObject( frame_bbc093b62d27feba681e8d45cf8cf221 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_11___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_tagger );
    Py_DECREF( var_tagger );
    var_tagger = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_tagger );
    var_tagger = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_11___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_12___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f4ddace6a23b56a5c123e81367e43f67;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f4ddace6a23b56a5c123e81367e43f67 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f4ddace6a23b56a5c123e81367e43f67, codeobj_f4ddace6a23b56a5c123e81367e43f67, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_f4ddace6a23b56a5c123e81367e43f67 = cache_frame_f4ddace6a23b56a5c123e81367e43f67;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f4ddace6a23b56a5c123e81367e43f67 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f4ddace6a23b56a5c123e81367e43f67 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_SimpleNamesExtractor );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SimpleNamesExtractor );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SimpleNamesExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_SIMPLE_NAME );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIMPLE_NAME );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SIMPLE_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_f4ddace6a23b56a5c123e81367e43f67->m_frame.f_lineno = 108;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4ddace6a23b56a5c123e81367e43f67 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4ddace6a23b56a5c123e81367e43f67 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f4ddace6a23b56a5c123e81367e43f67, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f4ddace6a23b56a5c123e81367e43f67->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f4ddace6a23b56a5c123e81367e43f67, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f4ddace6a23b56a5c123e81367e43f67,
        type_description_1,
        par_self,
        NULL
    );


    // Release cached frame.
    if ( frame_f4ddace6a23b56a5c123e81367e43f67 == cache_frame_f4ddace6a23b56a5c123e81367e43f67 )
    {
        Py_DECREF( frame_f4ddace6a23b56a5c123e81367e43f67 );
    }
    cache_frame_f4ddace6a23b56a5c123e81367e43f67 = NULL;

    assertFrameObject( frame_f4ddace6a23b56a5c123e81367e43f67 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_12___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_12___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_13___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_tagger = NULL;
    struct Nuitka_FrameObject *frame_ed80cb50da18e4f1ff553f4dbb6dc9b3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ed80cb50da18e4f1ff553f4dbb6dc9b3, codeobj_ed80cb50da18e4f1ff553f4dbb6dc9b3, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 = cache_frame_ed80cb50da18e4f1ff553f4dbb6dc9b3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_CrfTagger );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CrfTagger );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CrfTagger" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_NAME_MODEL );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME_MODEL );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME_MODEL" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_get_name_features );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_name_features );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_name_features" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_3;
        frame_ed80cb50da18e4f1ff553f4dbb6dc9b3->m_frame.f_lineno = 113;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        assert( var_tagger == NULL );
        var_tagger = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_PersonExtractor );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PersonExtractor );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PersonExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_4;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_PERSON );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PERSON );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PERSON" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = tmp_mvar_value_5;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_tagger;
        CHECK_OBJECT( var_tagger );
        tmp_dict_value_1 = var_tagger;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_ed80cb50da18e4f1ff553f4dbb6dc9b3->m_frame.f_lineno = 117;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ed80cb50da18e4f1ff553f4dbb6dc9b3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ed80cb50da18e4f1ff553f4dbb6dc9b3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ed80cb50da18e4f1ff553f4dbb6dc9b3,
        type_description_1,
        par_self,
        var_tagger,
        NULL
    );


    // Release cached frame.
    if ( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 == cache_frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 )
    {
        Py_DECREF( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 );
    }
    cache_frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 = NULL;

    assertFrameObject( frame_ed80cb50da18e4f1ff553f4dbb6dc9b3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_13___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_tagger );
    Py_DECREF( var_tagger );
    var_tagger = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_tagger );
    var_tagger = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_13___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_14___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e482d0a083a8d5083009eb13a00f5c44;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e482d0a083a8d5083009eb13a00f5c44 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e482d0a083a8d5083009eb13a00f5c44, codeobj_e482d0a083a8d5083009eb13a00f5c44, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_e482d0a083a8d5083009eb13a00f5c44 = cache_frame_e482d0a083a8d5083009eb13a00f5c44;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e482d0a083a8d5083009eb13a00f5c44 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e482d0a083a8d5083009eb13a00f5c44 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_DatesExtractor );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DatesExtractor );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DatesExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_DATE );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DATE );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DATE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_e482d0a083a8d5083009eb13a00f5c44->m_frame.f_lineno = 125;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e482d0a083a8d5083009eb13a00f5c44 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e482d0a083a8d5083009eb13a00f5c44 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e482d0a083a8d5083009eb13a00f5c44, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e482d0a083a8d5083009eb13a00f5c44->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e482d0a083a8d5083009eb13a00f5c44, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e482d0a083a8d5083009eb13a00f5c44,
        type_description_1,
        par_self,
        NULL
    );


    // Release cached frame.
    if ( frame_e482d0a083a8d5083009eb13a00f5c44 == cache_frame_e482d0a083a8d5083009eb13a00f5c44 )
    {
        Py_DECREF( frame_e482d0a083a8d5083009eb13a00f5c44 );
    }
    cache_frame_e482d0a083a8d5083009eb13a00f5c44 = NULL;

    assertFrameObject( frame_e482d0a083a8d5083009eb13a00f5c44 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_14___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_14___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_15___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2a31304be7c4eda2980beeb162ceb6a9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2a31304be7c4eda2980beeb162ceb6a9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2a31304be7c4eda2980beeb162ceb6a9, codeobj_2a31304be7c4eda2980beeb162ceb6a9, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_2a31304be7c4eda2980beeb162ceb6a9 = cache_frame_2a31304be7c4eda2980beeb162ceb6a9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2a31304be7c4eda2980beeb162ceb6a9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2a31304be7c4eda2980beeb162ceb6a9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MoneyExtractor );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MoneyExtractor );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MoneyExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MONEY );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONEY );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONEY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_2a31304be7c4eda2980beeb162ceb6a9->m_frame.f_lineno = 130;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a31304be7c4eda2980beeb162ceb6a9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a31304be7c4eda2980beeb162ceb6a9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2a31304be7c4eda2980beeb162ceb6a9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2a31304be7c4eda2980beeb162ceb6a9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2a31304be7c4eda2980beeb162ceb6a9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2a31304be7c4eda2980beeb162ceb6a9,
        type_description_1,
        par_self,
        NULL
    );


    // Release cached frame.
    if ( frame_2a31304be7c4eda2980beeb162ceb6a9 == cache_frame_2a31304be7c4eda2980beeb162ceb6a9 )
    {
        Py_DECREF( frame_2a31304be7c4eda2980beeb162ceb6a9 );
    }
    cache_frame_2a31304be7c4eda2980beeb162ceb6a9 = NULL;

    assertFrameObject( frame_2a31304be7c4eda2980beeb162ceb6a9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_15___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_15___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_16___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_377d1b82a16c6bd5a43c4a811a42e14c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_377d1b82a16c6bd5a43c4a811a42e14c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_377d1b82a16c6bd5a43c4a811a42e14c, codeobj_377d1b82a16c6bd5a43c4a811a42e14c, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_377d1b82a16c6bd5a43c4a811a42e14c = cache_frame_377d1b82a16c6bd5a43c4a811a42e14c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_377d1b82a16c6bd5a43c4a811a42e14c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_377d1b82a16c6bd5a43c4a811a42e14c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MoneyRateExtractor );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MoneyRateExtractor );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MoneyRateExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MONEY_RATE );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONEY_RATE );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONEY_RATE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_377d1b82a16c6bd5a43c4a811a42e14c->m_frame.f_lineno = 135;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_377d1b82a16c6bd5a43c4a811a42e14c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_377d1b82a16c6bd5a43c4a811a42e14c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_377d1b82a16c6bd5a43c4a811a42e14c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_377d1b82a16c6bd5a43c4a811a42e14c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_377d1b82a16c6bd5a43c4a811a42e14c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_377d1b82a16c6bd5a43c4a811a42e14c,
        type_description_1,
        par_self,
        NULL
    );


    // Release cached frame.
    if ( frame_377d1b82a16c6bd5a43c4a811a42e14c == cache_frame_377d1b82a16c6bd5a43c4a811a42e14c )
    {
        Py_DECREF( frame_377d1b82a16c6bd5a43c4a811a42e14c );
    }
    cache_frame_377d1b82a16c6bd5a43c4a811a42e14c = NULL;

    assertFrameObject( frame_377d1b82a16c6bd5a43c4a811a42e14c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_16___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_16___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_17___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_cec9f570a4528a5025e67dd8e0635f81;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_cec9f570a4528a5025e67dd8e0635f81 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cec9f570a4528a5025e67dd8e0635f81, codeobj_cec9f570a4528a5025e67dd8e0635f81, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_cec9f570a4528a5025e67dd8e0635f81 = cache_frame_cec9f570a4528a5025e67dd8e0635f81;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cec9f570a4528a5025e67dd8e0635f81 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cec9f570a4528a5025e67dd8e0635f81 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MoneyRangeExtractor );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MoneyRangeExtractor );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MoneyRangeExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MONEY_RANGE );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONEY_RANGE );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MONEY_RANGE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_cec9f570a4528a5025e67dd8e0635f81->m_frame.f_lineno = 140;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cec9f570a4528a5025e67dd8e0635f81 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cec9f570a4528a5025e67dd8e0635f81 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cec9f570a4528a5025e67dd8e0635f81, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cec9f570a4528a5025e67dd8e0635f81->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cec9f570a4528a5025e67dd8e0635f81, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cec9f570a4528a5025e67dd8e0635f81,
        type_description_1,
        par_self,
        NULL
    );


    // Release cached frame.
    if ( frame_cec9f570a4528a5025e67dd8e0635f81 == cache_frame_cec9f570a4528a5025e67dd8e0635f81 )
    {
        Py_DECREF( frame_cec9f570a4528a5025e67dd8e0635f81 );
    }
    cache_frame_cec9f570a4528a5025e67dd8e0635f81 = NULL;

    assertFrameObject( frame_cec9f570a4528a5025e67dd8e0635f81 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_17___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_17___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_18___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_tagger = NULL;
    struct Nuitka_FrameObject *frame_172f1fb96ddce7923ee472843aa4e313;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_172f1fb96ddce7923ee472843aa4e313 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_172f1fb96ddce7923ee472843aa4e313, codeobj_172f1fb96ddce7923ee472843aa4e313, module_extractors, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_172f1fb96ddce7923ee472843aa4e313 = cache_frame_172f1fb96ddce7923ee472843aa4e313;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_172f1fb96ddce7923ee472843aa4e313 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_172f1fb96ddce7923ee472843aa4e313 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_CrfTagger );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CrfTagger );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CrfTagger" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_STREET_MODEL );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STREET_MODEL );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STREET_MODEL" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_get_street_features );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_street_features );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_street_features" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_3;
        frame_172f1fb96ddce7923ee472843aa4e313->m_frame.f_lineno = 145;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        assert( var_tagger == NULL );
        var_tagger = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_AddressExtractor );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AddressExtractor );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AddressExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_4;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_ADDRESS );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ADDRESS );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ADDRESS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = tmp_mvar_value_5;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_tagger;
        CHECK_OBJECT( var_tagger );
        tmp_dict_value_1 = var_tagger;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_172f1fb96ddce7923ee472843aa4e313->m_frame.f_lineno = 149;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_172f1fb96ddce7923ee472843aa4e313 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_172f1fb96ddce7923ee472843aa4e313 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_172f1fb96ddce7923ee472843aa4e313, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_172f1fb96ddce7923ee472843aa4e313->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_172f1fb96ddce7923ee472843aa4e313, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_172f1fb96ddce7923ee472843aa4e313,
        type_description_1,
        par_self,
        var_tagger,
        NULL
    );


    // Release cached frame.
    if ( frame_172f1fb96ddce7923ee472843aa4e313 == cache_frame_172f1fb96ddce7923ee472843aa4e313 )
    {
        Py_DECREF( frame_172f1fb96ddce7923ee472843aa4e313 );
    }
    cache_frame_172f1fb96ddce7923ee472843aa4e313 = NULL;

    assertFrameObject( frame_172f1fb96ddce7923ee472843aa4e313 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_18___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_tagger );
    Py_DECREF( var_tagger );
    var_tagger = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_tagger );
    var_tagger = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_18___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_19___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_010b0a082fd533c1a869d90fdf922aaf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_010b0a082fd533c1a869d90fdf922aaf = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_010b0a082fd533c1a869d90fdf922aaf, codeobj_010b0a082fd533c1a869d90fdf922aaf, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_010b0a082fd533c1a869d90fdf922aaf = cache_frame_010b0a082fd533c1a869d90fdf922aaf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_010b0a082fd533c1a869d90fdf922aaf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_010b0a082fd533c1a869d90fdf922aaf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_LocationExtractor );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LocationExtractor );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LocationExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 157;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_LOCATION );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LOCATION );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LOCATION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 157;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_010b0a082fd533c1a869d90fdf922aaf->m_frame.f_lineno = 157;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_010b0a082fd533c1a869d90fdf922aaf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_010b0a082fd533c1a869d90fdf922aaf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_010b0a082fd533c1a869d90fdf922aaf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_010b0a082fd533c1a869d90fdf922aaf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_010b0a082fd533c1a869d90fdf922aaf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_010b0a082fd533c1a869d90fdf922aaf,
        type_description_1,
        par_self,
        NULL
    );


    // Release cached frame.
    if ( frame_010b0a082fd533c1a869d90fdf922aaf == cache_frame_010b0a082fd533c1a869d90fdf922aaf )
    {
        Py_DECREF( frame_010b0a082fd533c1a869d90fdf922aaf );
    }
    cache_frame_010b0a082fd533c1a869d90fdf922aaf = NULL;

    assertFrameObject( frame_010b0a082fd533c1a869d90fdf922aaf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_19___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_19___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_extractors$$$function_20___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d8902479b79e76e9acccb888184746ee;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d8902479b79e76e9acccb888184746ee = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d8902479b79e76e9acccb888184746ee, codeobj_d8902479b79e76e9acccb888184746ee, module_extractors, sizeof(void *)+sizeof(void *) );
    frame_d8902479b79e76e9acccb888184746ee = cache_frame_d8902479b79e76e9acccb888184746ee;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d8902479b79e76e9acccb888184746ee );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d8902479b79e76e9acccb888184746ee ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_OrganisationExtractor );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrganisationExtractor );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrganisationExtractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_ORGANISATION );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ORGANISATION );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ORGANISATION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_2;
        frame_d8902479b79e76e9acccb888184746ee->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d8902479b79e76e9acccb888184746ee );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d8902479b79e76e9acccb888184746ee );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d8902479b79e76e9acccb888184746ee, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d8902479b79e76e9acccb888184746ee->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d8902479b79e76e9acccb888184746ee, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d8902479b79e76e9acccb888184746ee,
        type_description_1,
        par_self,
        NULL
    );


    // Release cached frame.
    if ( frame_d8902479b79e76e9acccb888184746ee == cache_frame_d8902479b79e76e9acccb888184746ee )
    {
        Py_DECREF( frame_d8902479b79e76e9acccb888184746ee );
    }
    cache_frame_d8902479b79e76e9acccb888184746ee = NULL;

    assertFrameObject( frame_d8902479b79e76e9acccb888184746ee );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( extractors$$$function_20___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( extractors$$$function_20___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_10___call__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_10___call__,
        const_str_plain___call__,
#if PYTHON_VERSION >= 300
        const_str_digest_258415cdd8d8d9cf8ec18b9871cb6e96,
#endif
        codeobj_d98cdbfbba03de8c57d0034eda1e1322,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_11___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_11___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_02080cca8982a13adda93a4c9e5c0090,
#endif
        codeobj_bbc093b62d27feba681e8d45cf8cf221,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_12___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_12___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_6974fe154db97d23424e5c78f6bc0e71,
#endif
        codeobj_f4ddace6a23b56a5c123e81367e43f67,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_13___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_13___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_a24c917b45973000ed35d65e8a8aed02,
#endif
        codeobj_ed80cb50da18e4f1ff553f4dbb6dc9b3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_14___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_14___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_80a15adde89c34e6847d68394c2dc3c7,
#endif
        codeobj_e482d0a083a8d5083009eb13a00f5c44,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_15___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_15___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_949f8580537704fb48aee760d4649a27,
#endif
        codeobj_2a31304be7c4eda2980beeb162ceb6a9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_16___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_16___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_7a8755ce17f7205c83ae005912c59603,
#endif
        codeobj_377d1b82a16c6bd5a43c4a811a42e14c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_17___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_17___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_a91f498c8a5a67fbdbf118e331d3f2f5,
#endif
        codeobj_cec9f570a4528a5025e67dd8e0635f81,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_18___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_18___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_d1428bc079dfd4571773b648e7a68318,
#endif
        codeobj_172f1fb96ddce7923ee472843aa4e313,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_19___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_19___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_83e873120260d3def03f951b7dad8c98,
#endif
        codeobj_010b0a082fd533c1a869d90fdf922aaf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_1_serialize(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_1_serialize,
        const_str_plain_serialize,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_173e07ebc9698c39d13226b10c6ccfc2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_20___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_20___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_235c6d8c525921d31b027d6921e962a9,
#endif
        codeobj_d8902479b79e76e9acccb888184746ee,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_2___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_2___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_032c1b601478e70a93b6e2874f88ecb4,
#endif
        codeobj_bcea94b0c6934dc87211bd1e5b8ad491,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_2___init__$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_2___init__$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_59b4b535d9d81f63da4ad07e95e34cac,
#endif
        codeobj_ef2246482b1c0bc696917ea9e27d2385,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_3___iter__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_3___iter__,
        const_str_plain___iter__,
#if PYTHON_VERSION >= 300
        const_str_digest_4a078a5d1dd525bba77f16f60697435f,
#endif
        codeobj_ff10c1783cc6922768ae8684086f4412,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_4___getitem__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_4___getitem__,
        const_str_plain___getitem__,
#if PYTHON_VERSION >= 300
        const_str_digest_de1bf256424628eeb1e24e0b3cdd5144,
#endif
        codeobj_e5f08ba339e3be5f023758c864399b8d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_5___len__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_5___len__,
        const_str_plain___len__,
#if PYTHON_VERSION >= 300
        const_str_digest_968083e00bdde0e0530232a85082bbb8,
#endif
        codeobj_fe66caf202d9d48de5fc1f3f6edd2cf4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_6___bool__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_6___bool__,
        const_str_plain___bool__,
#if PYTHON_VERSION >= 300
        const_str_digest_094816650b7074d09bfcd3300cae3234,
#endif
        codeobj_9845148c54d14eb362538d5109eaba5a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_7_as_json(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_7_as_json,
        const_str_plain_as_json,
#if PYTHON_VERSION >= 300
        const_str_digest_493c8827148ae5bbf6698830439b90e8,
#endif
        codeobj_55bfd59a8f126110783324f26ab466c7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_8__repr_html_(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_8__repr_html_,
        const_str_plain__repr_html_,
#if PYTHON_VERSION >= 300
        const_str_digest_76e8d2b8d052b542a756b4c04f0f2b8b,
#endif
        codeobj_d19e1fc688c45b0dc836fcb7b7ee81e7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_extractors$$$function_9___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_extractors$$$function_9___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_018bd899a39ebf074e18ff4ddd45794d,
#endif
        codeobj_eb591d8b86d75b02c5ff881b29d05a79,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_extractors,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_extractors =
{
    PyModuleDef_HEAD_INIT,
    "extractors",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(extractors)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(extractors)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_extractors );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("extractors: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("extractors: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("extractors: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initextractors" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_extractors = Py_InitModule4(
        "extractors",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_extractors = PyModule_Create( &mdef_extractors );
#endif

    moduledict_extractors = MODULE_DICT( module_extractors );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_extractors,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_extractors,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_extractors,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_extractors,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_extractors );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_plain_extractors, module_extractors );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *outline_4_var___class__ = NULL;
    PyObject *outline_5_var___class__ = NULL;
    PyObject *outline_6_var___class__ = NULL;
    PyObject *outline_7_var___class__ = NULL;
    PyObject *outline_8_var___class__ = NULL;
    PyObject *outline_9_var___class__ = NULL;
    PyObject *outline_10_var___class__ = NULL;
    PyObject *outline_11_var___class__ = NULL;
    PyObject *tmp_class_creation_10__bases = NULL;
    PyObject *tmp_class_creation_10__bases_orig = NULL;
    PyObject *tmp_class_creation_10__class_decl_dict = NULL;
    PyObject *tmp_class_creation_10__metaclass = NULL;
    PyObject *tmp_class_creation_10__prepared = NULL;
    PyObject *tmp_class_creation_11__bases = NULL;
    PyObject *tmp_class_creation_11__bases_orig = NULL;
    PyObject *tmp_class_creation_11__class_decl_dict = NULL;
    PyObject *tmp_class_creation_11__metaclass = NULL;
    PyObject *tmp_class_creation_11__prepared = NULL;
    PyObject *tmp_class_creation_12__bases = NULL;
    PyObject *tmp_class_creation_12__bases_orig = NULL;
    PyObject *tmp_class_creation_12__class_decl_dict = NULL;
    PyObject *tmp_class_creation_12__metaclass = NULL;
    PyObject *tmp_class_creation_12__prepared = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__bases_orig = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_class_creation_5__bases = NULL;
    PyObject *tmp_class_creation_5__bases_orig = NULL;
    PyObject *tmp_class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_class_creation_5__metaclass = NULL;
    PyObject *tmp_class_creation_5__prepared = NULL;
    PyObject *tmp_class_creation_6__bases = NULL;
    PyObject *tmp_class_creation_6__bases_orig = NULL;
    PyObject *tmp_class_creation_6__class_decl_dict = NULL;
    PyObject *tmp_class_creation_6__metaclass = NULL;
    PyObject *tmp_class_creation_6__prepared = NULL;
    PyObject *tmp_class_creation_7__bases = NULL;
    PyObject *tmp_class_creation_7__bases_orig = NULL;
    PyObject *tmp_class_creation_7__class_decl_dict = NULL;
    PyObject *tmp_class_creation_7__metaclass = NULL;
    PyObject *tmp_class_creation_7__prepared = NULL;
    PyObject *tmp_class_creation_8__bases = NULL;
    PyObject *tmp_class_creation_8__bases_orig = NULL;
    PyObject *tmp_class_creation_8__class_decl_dict = NULL;
    PyObject *tmp_class_creation_8__metaclass = NULL;
    PyObject *tmp_class_creation_8__prepared = NULL;
    PyObject *tmp_class_creation_9__bases = NULL;
    PyObject *tmp_class_creation_9__bases_orig = NULL;
    PyObject *tmp_class_creation_9__class_decl_dict = NULL;
    PyObject *tmp_class_creation_9__metaclass = NULL;
    PyObject *tmp_class_creation_9__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    struct Nuitka_FrameObject *frame_90775d2cb57f1deebc6c000eaebfc0ab;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_extractors_55 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_dca7af48eb89b217f352818e300bd4eb_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_dca7af48eb89b217f352818e300bd4eb_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *locals_extractors_84 = NULL;
    struct Nuitka_FrameObject *frame_46a8fcf03fef20828841399f33983311_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_46a8fcf03fef20828841399f33983311_3 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *locals_extractors_94 = NULL;
    struct Nuitka_FrameObject *frame_8622361ddd34898470abb18eba55960a_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_8622361ddd34898470abb18eba55960a_4 = NULL;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *locals_extractors_106 = NULL;
    struct Nuitka_FrameObject *frame_1443d96cbf56a5429511b6be48c0967d_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_1443d96cbf56a5429511b6be48c0967d_5 = NULL;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *locals_extractors_111 = NULL;
    struct Nuitka_FrameObject *frame_adec1ac16e1ded723ba15306b0680e21_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    static struct Nuitka_FrameObject *cache_frame_adec1ac16e1ded723ba15306b0680e21_6 = NULL;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *locals_extractors_123 = NULL;
    struct Nuitka_FrameObject *frame_30e6bfaa91e1fe1699cd0d1f3701d474_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    static struct Nuitka_FrameObject *cache_frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 = NULL;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *locals_extractors_128 = NULL;
    struct Nuitka_FrameObject *frame_c193d8406a0c58560a78960b7c28d662_8;
    NUITKA_MAY_BE_UNUSED char const *type_description_8 = NULL;
    static struct Nuitka_FrameObject *cache_frame_c193d8406a0c58560a78960b7c28d662_8 = NULL;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;
    PyObject *locals_extractors_133 = NULL;
    struct Nuitka_FrameObject *frame_09787e6b9a91e7687ca4e65eb22138ad_9;
    NUITKA_MAY_BE_UNUSED char const *type_description_9 = NULL;
    static struct Nuitka_FrameObject *cache_frame_09787e6b9a91e7687ca4e65eb22138ad_9 = NULL;
    PyObject *exception_keeper_type_25;
    PyObject *exception_keeper_value_25;
    PyTracebackObject *exception_keeper_tb_25;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_25;
    PyObject *exception_keeper_type_26;
    PyObject *exception_keeper_value_26;
    PyTracebackObject *exception_keeper_tb_26;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_26;
    PyObject *exception_keeper_type_27;
    PyObject *exception_keeper_value_27;
    PyTracebackObject *exception_keeper_tb_27;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_27;
    PyObject *locals_extractors_138 = NULL;
    struct Nuitka_FrameObject *frame_a41ed17644233d58ca4331a71292e9ee_10;
    NUITKA_MAY_BE_UNUSED char const *type_description_10 = NULL;
    static struct Nuitka_FrameObject *cache_frame_a41ed17644233d58ca4331a71292e9ee_10 = NULL;
    PyObject *exception_keeper_type_28;
    PyObject *exception_keeper_value_28;
    PyTracebackObject *exception_keeper_tb_28;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_28;
    PyObject *exception_keeper_type_29;
    PyObject *exception_keeper_value_29;
    PyTracebackObject *exception_keeper_tb_29;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_29;
    PyObject *exception_keeper_type_30;
    PyObject *exception_keeper_value_30;
    PyTracebackObject *exception_keeper_tb_30;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_30;
    PyObject *locals_extractors_143 = NULL;
    struct Nuitka_FrameObject *frame_67f4e11334b14bcc38f1503fd88ebbe9_11;
    NUITKA_MAY_BE_UNUSED char const *type_description_11 = NULL;
    static struct Nuitka_FrameObject *cache_frame_67f4e11334b14bcc38f1503fd88ebbe9_11 = NULL;
    PyObject *exception_keeper_type_31;
    PyObject *exception_keeper_value_31;
    PyTracebackObject *exception_keeper_tb_31;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_31;
    PyObject *exception_keeper_type_32;
    PyObject *exception_keeper_value_32;
    PyTracebackObject *exception_keeper_tb_32;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_32;
    PyObject *exception_keeper_type_33;
    PyObject *exception_keeper_value_33;
    PyTracebackObject *exception_keeper_tb_33;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_33;
    PyObject *locals_extractors_155 = NULL;
    struct Nuitka_FrameObject *frame_234de8d20548cbb66367c1849eae78b7_12;
    NUITKA_MAY_BE_UNUSED char const *type_description_12 = NULL;
    static struct Nuitka_FrameObject *cache_frame_234de8d20548cbb66367c1849eae78b7_12 = NULL;
    PyObject *exception_keeper_type_34;
    PyObject *exception_keeper_value_34;
    PyTracebackObject *exception_keeper_tb_34;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_34;
    PyObject *exception_keeper_type_35;
    PyObject *exception_keeper_value_35;
    PyTracebackObject *exception_keeper_tb_35;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_35;
    PyObject *exception_keeper_type_36;
    PyObject *exception_keeper_value_36;
    PyTracebackObject *exception_keeper_tb_36;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_36;
    PyObject *locals_extractors_160 = NULL;
    struct Nuitka_FrameObject *frame_4791fd6c754f72a7bb928a049a6ef26d_13;
    NUITKA_MAY_BE_UNUSED char const *type_description_13 = NULL;
    static struct Nuitka_FrameObject *cache_frame_4791fd6c754f72a7bb928a049a6ef26d_13 = NULL;
    PyObject *exception_keeper_type_37;
    PyObject *exception_keeper_value_37;
    PyTracebackObject *exception_keeper_tb_37;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_37;
    PyObject *exception_keeper_type_38;
    PyObject *exception_keeper_value_38;
    PyTracebackObject *exception_keeper_tb_38;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_38;
    PyObject *exception_keeper_type_39;
    PyObject *exception_keeper_value_39;
    PyTracebackObject *exception_keeper_tb_39;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_39;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_90775d2cb57f1deebc6c000eaebfc0ab = MAKE_MODULE_FRAME( codeobj_90775d2cb57f1deebc6c000eaebfc0ab, module_extractors );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_90775d2cb57f1deebc6c000eaebfc0ab );
    assert( Py_REFCNT( frame_90775d2cb57f1deebc6c000eaebfc0ab ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_extractors, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_collections;
        tmp_globals_name_1 = (PyObject *)moduledict_extractors;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_OrderedDict_tuple;
        tmp_level_name_1 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 4;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_OrderedDict );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_OrderedDict, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_yargy;
        tmp_globals_name_2 = (PyObject *)moduledict_extractors;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_Parser_tuple;
        tmp_level_name_2 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 6;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_Parser );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Parser, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_utils;
        tmp_globals_name_3 = (PyObject *)moduledict_extractors;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_Record_tuple;
        tmp_level_name_3 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 8;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Record );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Record, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_preprocess;
        tmp_globals_name_4 = (PyObject *)moduledict_extractors;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_normalize_text_tuple;
        tmp_level_name_4 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 9;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_normalize_text );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_normalize_text, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_markup;
        tmp_globals_name_5 = (PyObject *)moduledict_extractors;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_get_markup_notebook_tuple;
        tmp_level_name_5 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 10;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_get_markup_notebook );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_get_markup_notebook, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_tokenizer;
        tmp_globals_name_6 = (PyObject *)moduledict_extractors;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_TOKENIZER_tuple;
        tmp_level_name_6 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 12;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_TOKENIZER );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_TOKENIZER, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_crf;
        tmp_globals_name_7 = (PyObject *)moduledict_extractors;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_20b90037841e99e016cf8af8157e0184_tuple;
        tmp_level_name_7 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 14;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_11;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_8 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_CrfTagger );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_CrfTagger, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_9 = tmp_import_from_1__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_STREET_MODEL );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_STREET_MODEL, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_10 = tmp_import_from_1__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_get_street_features );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_get_street_features, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_11 = tmp_import_from_1__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_NAME_MODEL );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_NAME_MODEL, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_12 = tmp_import_from_1__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_get_name_features );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_get_name_features, tmp_assign_source_16 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_28ca5c1e855316b7737046f32ddceb9c;
        tmp_globals_name_8 = (PyObject *)moduledict_extractors;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_SIMPLE_NAME_str_plain_NAME_tuple;
        tmp_level_name_8 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 24;
        tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_17;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_13 = tmp_import_from_2__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_SIMPLE_NAME );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_SIMPLE_NAME, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_14 = tmp_import_from_2__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_NAME );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_NAME, tmp_assign_source_19 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_15;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_b869d9b1f0243a03bd697608af4d0e39;
        tmp_globals_name_9 = (PyObject *)moduledict_extractors;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_DATE_tuple;
        tmp_level_name_9 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 28;
        tmp_import_name_from_15 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_DATE );
        Py_DECREF( tmp_import_name_from_15 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_DATE, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_754bbdb51d29ec017fab6feb3184da49;
        tmp_globals_name_10 = (PyObject *)moduledict_extractors;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_MONEY_str_plain_RATE_str_plain_RANGE_tuple;
        tmp_level_name_10 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 29;
        tmp_assign_source_21 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_21;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_16 = tmp_import_from_3__module;
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_MONEY );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MONEY, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_17 = tmp_import_from_3__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_RATE );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MONEY_RATE, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_18 = tmp_import_from_3__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_RANGE );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MONEY_RANGE, tmp_assign_source_24 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_19;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_d2a7a56a4d20853487c008ece870c4f1;
        tmp_globals_name_11 = (PyObject *)moduledict_extractors;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_LOCATION_tuple;
        tmp_level_name_11 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 34;
        tmp_import_name_from_19 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_LOCATION );
        Py_DECREF( tmp_import_name_from_19 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_LOCATION, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_20;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_f2be9345b65867d2e5ac284a768ec655;
        tmp_globals_name_12 = (PyObject *)moduledict_extractors;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_ADDRESS_tuple;
        tmp_level_name_12 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 35;
        tmp_import_name_from_20 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_import_name_from_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_ADDRESS );
        Py_DECREF( tmp_import_name_from_20 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_ADDRESS, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_21;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_digest_09460345ee7f4232b97d5c10b6119bc9;
        tmp_globals_name_13 = (PyObject *)moduledict_extractors;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = const_tuple_str_plain_ORGANISATION_tuple;
        tmp_level_name_13 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 36;
        tmp_import_name_from_21 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        if ( tmp_import_name_from_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_ORGANISATION );
        Py_DECREF( tmp_import_name_from_21 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_ORGANISATION, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_22;
        PyObject *tmp_name_name_14;
        PyObject *tmp_globals_name_14;
        PyObject *tmp_locals_name_14;
        PyObject *tmp_fromlist_name_14;
        PyObject *tmp_level_name_14;
        tmp_name_name_14 = const_str_digest_fc6305b0a92eca56d8c5a8c90bb2b9bc;
        tmp_globals_name_14 = (PyObject *)moduledict_extractors;
        tmp_locals_name_14 = Py_None;
        tmp_fromlist_name_14 = const_tuple_str_plain_PERSON_tuple;
        tmp_level_name_14 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 37;
        tmp_import_name_from_22 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
        if ( tmp_import_name_from_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_PERSON );
        Py_DECREF( tmp_import_name_from_22 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_PERSON, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_import_name_from_23;
        PyObject *tmp_name_name_15;
        PyObject *tmp_globals_name_15;
        PyObject *tmp_locals_name_15;
        PyObject *tmp_fromlist_name_15;
        PyObject *tmp_level_name_15;
        tmp_name_name_15 = const_str_plain_dsl;
        tmp_globals_name_15 = (PyObject *)moduledict_extractors;
        tmp_locals_name_15 = Py_None;
        tmp_fromlist_name_15 = const_tuple_str_plain_can_be_normalized_tuple;
        tmp_level_name_15 = const_int_0;
        frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 39;
        tmp_import_name_from_23 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
        if ( tmp_import_name_from_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_can_be_normalized );
        Py_DECREF( tmp_import_name_from_23 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_can_be_normalized, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = MAKE_FUNCTION_extractors$$$function_1_serialize(  );



        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_serialize, tmp_assign_source_30 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Record );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Record );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Record" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;

            goto try_except_handler_4;
        }

        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_assign_source_31 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_31, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_32 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_34 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_34;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_4;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;

                goto try_except_handler_4;
            }
            tmp_tuple_element_2 = const_str_plain_Matches;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 55;
            tmp_assign_source_35 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_35;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;

                goto try_except_handler_4;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 55;

                    goto try_except_handler_4;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 55;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 55;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 55;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_36;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_37;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_extractors_55 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain_Matches;
        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_dca7af48eb89b217f352818e300bd4eb_2, codeobj_dca7af48eb89b217f352818e300bd4eb, module_extractors, sizeof(void *) );
        frame_dca7af48eb89b217f352818e300bd4eb_2 = cache_frame_dca7af48eb89b217f352818e300bd4eb_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_dca7af48eb89b217f352818e300bd4eb_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_dca7af48eb89b217f352818e300bd4eb_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_text_str_plain_matches_list );
        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_2___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_3___iter__(  );



        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___iter__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_4___getitem__(  );



        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___getitem__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_5___len__(  );



        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___len__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_6___bool__(  );



        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___bool__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_2;
            tmp_res = MAPPING_HAS_ITEM( locals_extractors_55, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_2 = PyObject_GetItem( locals_extractors_55, const_str_plain_property );

            if ( tmp_called_name_2 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 74;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_1 = MAKE_FUNCTION_extractors$$$function_7_as_json(  );



            frame_dca7af48eb89b217f352818e300bd4eb_2->m_frame.f_lineno = 74;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_called_name_3 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_2 = MAKE_FUNCTION_extractors$$$function_7_as_json(  );



            frame_dca7af48eb89b217f352818e300bd4eb_2->m_frame.f_lineno = 74;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain_as_json, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_8__repr_html_(  );



        tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain__repr_html_, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_dca7af48eb89b217f352818e300bd4eb_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_dca7af48eb89b217f352818e300bd4eb_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_dca7af48eb89b217f352818e300bd4eb_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_dca7af48eb89b217f352818e300bd4eb_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_dca7af48eb89b217f352818e300bd4eb_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_dca7af48eb89b217f352818e300bd4eb_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_dca7af48eb89b217f352818e300bd4eb_2 == cache_frame_dca7af48eb89b217f352818e300bd4eb_2 )
        {
            Py_DECREF( frame_dca7af48eb89b217f352818e300bd4eb_2 );
        }
        cache_frame_dca7af48eb89b217f352818e300bd4eb_2 = NULL;

        assertFrameObject( frame_dca7af48eb89b217f352818e300bd4eb_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;

                goto try_except_handler_6;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_55, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;

                goto try_except_handler_6;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_38;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_4 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_Matches;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_extractors_55;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 55;
            tmp_assign_source_38 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;

                goto try_except_handler_6;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_38;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_37 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_37 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_extractors_55 );
        locals_extractors_55 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_55 );
        locals_extractors_55 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 55;
        goto try_except_handler_4;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Matches, tmp_assign_source_37 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_dircall_arg1_2;
        tmp_dircall_arg1_2 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_39 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_39;
    }
    {
        PyObject *tmp_assign_source_40;
        tmp_assign_source_40 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        tmp_condition_result_9 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_5:;
        condexpr_end_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_41 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_41;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_7;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_42;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_7;
            }
            tmp_tuple_element_5 = const_str_plain_Extractor;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_5 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 84;
            tmp_assign_source_42 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_42 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_7;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_42;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_7;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 84;

                    goto try_except_handler_7;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 84;

                    goto try_except_handler_7;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 84;

                    goto try_except_handler_7;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 84;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_7;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_43;
            tmp_assign_source_43 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_43;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_44;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_extractors_84 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_84, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_plain_Extractor;
        tmp_res = PyObject_SetItem( locals_extractors_84, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_9;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_46a8fcf03fef20828841399f33983311_3, codeobj_46a8fcf03fef20828841399f33983311, module_extractors, sizeof(void *) );
        frame_46a8fcf03fef20828841399f33983311_3 = cache_frame_46a8fcf03fef20828841399f33983311_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_46a8fcf03fef20828841399f33983311_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_46a8fcf03fef20828841399f33983311_3 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_1;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_mvar_value_4;
            tmp_tuple_element_7 = PyObject_GetItem( locals_extractors_84, const_str_plain_TOKENIZER );

            if ( tmp_tuple_element_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_TOKENIZER );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TOKENIZER );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TOKENIZER" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_7 = tmp_mvar_value_4;
                Py_INCREF( tmp_tuple_element_7 );
                }
            }

            tmp_defaults_1 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_7 );
            tmp_tuple_element_7 = Py_None;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_defaults_1, 1, tmp_tuple_element_7 );
            tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_9___init__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_extractors_84, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_10___call__(  );



        tmp_res = PyObject_SetItem( locals_extractors_84, const_str_plain___call__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_46a8fcf03fef20828841399f33983311_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_46a8fcf03fef20828841399f33983311_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_46a8fcf03fef20828841399f33983311_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_46a8fcf03fef20828841399f33983311_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_46a8fcf03fef20828841399f33983311_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_46a8fcf03fef20828841399f33983311_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_46a8fcf03fef20828841399f33983311_3 == cache_frame_46a8fcf03fef20828841399f33983311_3 )
        {
            Py_DECREF( frame_46a8fcf03fef20828841399f33983311_3 );
        }
        cache_frame_46a8fcf03fef20828841399f33983311_3 = NULL;

        assertFrameObject( frame_46a8fcf03fef20828841399f33983311_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_9;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            tmp_compexpr_right_2 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_9;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_extractors_84, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_9;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_45;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_6 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_Extractor;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_extractors_84;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 84;
            tmp_assign_source_45 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_45 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_9;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_45;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_44 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_44 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        Py_DECREF( locals_extractors_84 );
        locals_extractors_84 = NULL;
        goto try_return_handler_8;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_84 );
        locals_extractors_84 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 84;
        goto try_except_handler_7;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor, tmp_assign_source_44 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_tuple_element_9;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto try_except_handler_10;
        }

        tmp_tuple_element_9 = tmp_mvar_value_5;
        tmp_assign_source_46 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_assign_source_46, 0, tmp_tuple_element_9 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_46;
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_47 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_47;
    }
    {
        PyObject *tmp_assign_source_48;
        tmp_assign_source_48 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_48;
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_15;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        tmp_condition_result_15 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_3 = tmp_class_creation_3__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_7:;
        condexpr_end_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_49 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_49;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_10;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_9 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_50;
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_10 = tmp_class_creation_3__metaclass;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;

                goto try_except_handler_10;
            }
            tmp_tuple_element_10 = const_str_plain_NamesExtractor;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_10 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 94;
            tmp_assign_source_50 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_50 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;

                goto try_except_handler_10;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_50;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_11 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;

                goto try_except_handler_10;
            }
            tmp_condition_result_18 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_11;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_11 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 94;

                    goto try_except_handler_10;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_11 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 94;

                    goto try_except_handler_10;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_11 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 94;

                    goto try_except_handler_10;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 94;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_10;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_51;
            tmp_assign_source_51 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_51;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_52;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_extractors_94 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_94, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_12;
        }
        tmp_dictset_value = const_str_plain_NamesExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_94, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;

            goto try_except_handler_12;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_8622361ddd34898470abb18eba55960a_4, codeobj_8622361ddd34898470abb18eba55960a, module_extractors, sizeof(void *) );
        frame_8622361ddd34898470abb18eba55960a_4 = cache_frame_8622361ddd34898470abb18eba55960a_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_8622361ddd34898470abb18eba55960a_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_8622361ddd34898470abb18eba55960a_4 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_11___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_94, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8622361ddd34898470abb18eba55960a_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8622361ddd34898470abb18eba55960a_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_8622361ddd34898470abb18eba55960a_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_8622361ddd34898470abb18eba55960a_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_8622361ddd34898470abb18eba55960a_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_8622361ddd34898470abb18eba55960a_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_8622361ddd34898470abb18eba55960a_4 == cache_frame_8622361ddd34898470abb18eba55960a_4 )
        {
            Py_DECREF( frame_8622361ddd34898470abb18eba55960a_4 );
        }
        cache_frame_8622361ddd34898470abb18eba55960a_4 = NULL;

        assertFrameObject( frame_8622361ddd34898470abb18eba55960a_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_12;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;

                goto try_except_handler_12;
            }
            tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_94, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;

                goto try_except_handler_12;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_53;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_8 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_12 = const_str_plain_NamesExtractor;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_12 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_12 );
            tmp_tuple_element_12 = locals_extractors_94;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 94;
            tmp_assign_source_53 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_53 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;

                goto try_except_handler_12;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_53;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_52 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_52 );
        goto try_return_handler_12;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_12:;
        Py_DECREF( locals_extractors_94 );
        locals_extractors_94 = NULL;
        goto try_return_handler_11;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_94 );
        locals_extractors_94 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto try_except_handler_11;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 94;
        goto try_except_handler_10;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_NamesExtractor, tmp_assign_source_52 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_tuple_element_13;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;

            goto try_except_handler_13;
        }

        tmp_tuple_element_13 = tmp_mvar_value_6;
        tmp_assign_source_54 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_13 );
        PyTuple_SET_ITEM( tmp_assign_source_54, 0, tmp_tuple_element_13 );
        assert( tmp_class_creation_4__bases_orig == NULL );
        tmp_class_creation_4__bases_orig = tmp_assign_source_54;
    }
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_dircall_arg1_4;
        CHECK_OBJECT( tmp_class_creation_4__bases_orig );
        tmp_dircall_arg1_4 = tmp_class_creation_4__bases_orig;
        Py_INCREF( tmp_dircall_arg1_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
            tmp_assign_source_55 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_55;
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_56;
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_metaclass_name_4;
        nuitka_bool tmp_condition_result_20;
        PyObject *tmp_key_name_10;
        PyObject *tmp_dict_name_10;
        PyObject *tmp_dict_name_11;
        PyObject *tmp_key_name_11;
        nuitka_bool tmp_condition_result_21;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_7;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_4;
        tmp_key_name_10 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_8;
        }
        else
        {
            goto condexpr_false_8;
        }
        condexpr_true_8:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_11 = const_str_plain_metaclass;
        tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        goto condexpr_end_8;
        condexpr_false_8:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        tmp_condition_result_21 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_9;
        }
        else
        {
            goto condexpr_false_9;
        }
        condexpr_true_9:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_4 = tmp_class_creation_4__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
        Py_DECREF( tmp_type_arg_7 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        goto condexpr_end_9;
        condexpr_false_9:;
        tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_4 );
        condexpr_end_9:;
        condexpr_end_8:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_4 = tmp_class_creation_4__bases;
        tmp_assign_source_57 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
        Py_DECREF( tmp_metaclass_name_4 );
        if ( tmp_assign_source_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_57;
    }
    {
        nuitka_bool tmp_condition_result_22;
        PyObject *tmp_key_name_12;
        PyObject *tmp_dict_name_12;
        tmp_key_name_12 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_13;
        }
        branch_no_13:;
    }
    {
        nuitka_bool tmp_condition_result_23;
        PyObject *tmp_source_name_13;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_13 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_13, const_str_plain___prepare__ );
        tmp_condition_result_23 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_assign_source_58;
            PyObject *tmp_called_name_9;
            PyObject *tmp_source_name_14;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_14 = tmp_class_creation_4__metaclass;
            tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain___prepare__ );
            if ( tmp_called_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;

                goto try_except_handler_13;
            }
            tmp_tuple_element_14 = const_str_plain_SimpleNamesExtractor;
            tmp_args_name_7 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_14 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_4__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 106;
            tmp_assign_source_58 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_58 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;

                goto try_except_handler_13;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_58;
        }
        {
            nuitka_bool tmp_condition_result_24;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_15 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;

                goto try_except_handler_13;
            }
            tmp_condition_result_24 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_4;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_tuple_element_15;
                PyObject *tmp_getattr_target_4;
                PyObject *tmp_getattr_attr_4;
                PyObject *tmp_getattr_default_4;
                PyObject *tmp_source_name_16;
                PyObject *tmp_type_arg_8;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_4 = const_str_plain___name__;
                tmp_getattr_default_4 = const_str_angle_metaclass;
                tmp_tuple_element_15 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 106;

                    goto try_except_handler_13;
                }
                tmp_right_name_4 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_15 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_8 = tmp_class_creation_4__prepared;
                tmp_source_name_16 = BUILTIN_TYPE1( tmp_type_arg_8 );
                assert( !(tmp_source_name_16 == NULL) );
                tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_16 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_4 );

                    exception_lineno = 106;

                    goto try_except_handler_13;
                }
                PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_15 );
                tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_right_name_4 );
                if ( tmp_raise_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 106;

                    goto try_except_handler_13;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_4;
                exception_lineno = 106;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_13;
            }
            branch_no_15:;
        }
        goto branch_end_14;
        branch_no_14:;
        {
            PyObject *tmp_assign_source_59;
            tmp_assign_source_59 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_59;
        }
        branch_end_14:;
    }
    {
        PyObject *tmp_assign_source_60;
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_4 = tmp_class_creation_4__prepared;
            locals_extractors_106 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_106, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_15;
        }
        tmp_dictset_value = const_str_plain_SimpleNamesExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_106, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;

            goto try_except_handler_15;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_1443d96cbf56a5429511b6be48c0967d_5, codeobj_1443d96cbf56a5429511b6be48c0967d, module_extractors, sizeof(void *) );
        frame_1443d96cbf56a5429511b6be48c0967d_5 = cache_frame_1443d96cbf56a5429511b6be48c0967d_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_1443d96cbf56a5429511b6be48c0967d_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_1443d96cbf56a5429511b6be48c0967d_5 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_12___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_106, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1443d96cbf56a5429511b6be48c0967d_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1443d96cbf56a5429511b6be48c0967d_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_1443d96cbf56a5429511b6be48c0967d_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_1443d96cbf56a5429511b6be48c0967d_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_1443d96cbf56a5429511b6be48c0967d_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_1443d96cbf56a5429511b6be48c0967d_5,
            type_description_2,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_1443d96cbf56a5429511b6be48c0967d_5 == cache_frame_1443d96cbf56a5429511b6be48c0967d_5 )
        {
            Py_DECREF( frame_1443d96cbf56a5429511b6be48c0967d_5 );
        }
        cache_frame_1443d96cbf56a5429511b6be48c0967d_5 = NULL;

        assertFrameObject( frame_1443d96cbf56a5429511b6be48c0967d_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;

        goto try_except_handler_15;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_25;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_4 = tmp_class_creation_4__bases;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_compexpr_right_4 = tmp_class_creation_4__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;

                goto try_except_handler_15;
            }
            tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_dictset_value = tmp_class_creation_4__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_106, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;

                goto try_except_handler_15;
            }
            branch_no_16:;
        }
        {
            PyObject *tmp_assign_source_61;
            PyObject *tmp_called_name_10;
            PyObject *tmp_args_name_8;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_10 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_16 = const_str_plain_SimpleNamesExtractor;
            tmp_args_name_8 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_16 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_16 );
            tmp_tuple_element_16 = locals_extractors_106;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 106;
            tmp_assign_source_61 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_8, tmp_kw_name_8 );
            Py_DECREF( tmp_args_name_8 );
            if ( tmp_assign_source_61 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;

                goto try_except_handler_15;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_61;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_assign_source_60 = outline_3_var___class__;
        Py_INCREF( tmp_assign_source_60 );
        goto try_return_handler_15;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_15:;
        Py_DECREF( locals_extractors_106 );
        locals_extractors_106 = NULL;
        goto try_return_handler_14;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_106 );
        locals_extractors_106 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto try_except_handler_14;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 106;
        goto try_except_handler_13;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_SimpleNamesExtractor, tmp_assign_source_60 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases_orig );
    Py_DECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_62;
        PyObject *tmp_tuple_element_17;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;

            goto try_except_handler_16;
        }

        tmp_tuple_element_17 = tmp_mvar_value_7;
        tmp_assign_source_62 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_17 );
        PyTuple_SET_ITEM( tmp_assign_source_62, 0, tmp_tuple_element_17 );
        assert( tmp_class_creation_5__bases_orig == NULL );
        tmp_class_creation_5__bases_orig = tmp_assign_source_62;
    }
    {
        PyObject *tmp_assign_source_63;
        PyObject *tmp_dircall_arg1_5;
        CHECK_OBJECT( tmp_class_creation_5__bases_orig );
        tmp_dircall_arg1_5 = tmp_class_creation_5__bases_orig;
        Py_INCREF( tmp_dircall_arg1_5 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
            tmp_assign_source_63 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        assert( tmp_class_creation_5__bases == NULL );
        tmp_class_creation_5__bases = tmp_assign_source_63;
    }
    {
        PyObject *tmp_assign_source_64;
        tmp_assign_source_64 = PyDict_New();
        assert( tmp_class_creation_5__class_decl_dict == NULL );
        tmp_class_creation_5__class_decl_dict = tmp_assign_source_64;
    }
    {
        PyObject *tmp_assign_source_65;
        PyObject *tmp_metaclass_name_5;
        nuitka_bool tmp_condition_result_26;
        PyObject *tmp_key_name_13;
        PyObject *tmp_dict_name_13;
        PyObject *tmp_dict_name_14;
        PyObject *tmp_key_name_14;
        nuitka_bool tmp_condition_result_27;
        int tmp_truth_name_5;
        PyObject *tmp_type_arg_9;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_bases_name_5;
        tmp_key_name_13 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_13 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        tmp_condition_result_26 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_10;
        }
        else
        {
            goto condexpr_false_10;
        }
        condexpr_true_10:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_14 = tmp_class_creation_5__class_decl_dict;
        tmp_key_name_14 = const_str_plain_metaclass;
        tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        goto condexpr_end_10;
        condexpr_false_10:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_class_creation_5__bases );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        tmp_condition_result_27 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_11;
        }
        else
        {
            goto condexpr_false_11;
        }
        condexpr_true_11:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_subscribed_name_5 = tmp_class_creation_5__bases;
        tmp_subscript_name_5 = const_int_0;
        tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
        if ( tmp_type_arg_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
        Py_DECREF( tmp_type_arg_9 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        goto condexpr_end_11;
        condexpr_false_11:;
        tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_5 );
        condexpr_end_11:;
        condexpr_end_10:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_bases_name_5 = tmp_class_creation_5__bases;
        tmp_assign_source_65 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
        Py_DECREF( tmp_metaclass_name_5 );
        if ( tmp_assign_source_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        assert( tmp_class_creation_5__metaclass == NULL );
        tmp_class_creation_5__metaclass = tmp_assign_source_65;
    }
    {
        nuitka_bool tmp_condition_result_28;
        PyObject *tmp_key_name_15;
        PyObject *tmp_dict_name_15;
        tmp_key_name_15 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_15 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        tmp_condition_result_28 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_17;
        }
        else
        {
            goto branch_no_17;
        }
        branch_yes_17:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_5__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_16;
        }
        branch_no_17:;
    }
    {
        nuitka_bool tmp_condition_result_29;
        PyObject *tmp_source_name_17;
        CHECK_OBJECT( tmp_class_creation_5__metaclass );
        tmp_source_name_17 = tmp_class_creation_5__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_17, const_str_plain___prepare__ );
        tmp_condition_result_29 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_18;
        }
        else
        {
            goto branch_no_18;
        }
        branch_yes_18:;
        {
            PyObject *tmp_assign_source_66;
            PyObject *tmp_called_name_11;
            PyObject *tmp_source_name_18;
            PyObject *tmp_args_name_9;
            PyObject *tmp_tuple_element_18;
            PyObject *tmp_kw_name_9;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_source_name_18 = tmp_class_creation_5__metaclass;
            tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain___prepare__ );
            if ( tmp_called_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 111;

                goto try_except_handler_16;
            }
            tmp_tuple_element_18 = const_str_plain_PersonExtractor;
            tmp_args_name_9 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_18 );
            PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_18 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_18 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_18 );
            PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_18 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_9 = tmp_class_creation_5__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 111;
            tmp_assign_source_66 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_9, tmp_kw_name_9 );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_name_9 );
            if ( tmp_assign_source_66 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 111;

                goto try_except_handler_16;
            }
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_66;
        }
        {
            nuitka_bool tmp_condition_result_30;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_source_name_19;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_source_name_19 = tmp_class_creation_5__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_19, const_str_plain___getitem__ );
            tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 111;

                goto try_except_handler_16;
            }
            tmp_condition_result_30 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_19;
            }
            else
            {
                goto branch_no_19;
            }
            branch_yes_19:;
            {
                PyObject *tmp_raise_type_5;
                PyObject *tmp_raise_value_5;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                PyObject *tmp_tuple_element_19;
                PyObject *tmp_getattr_target_5;
                PyObject *tmp_getattr_attr_5;
                PyObject *tmp_getattr_default_5;
                PyObject *tmp_source_name_20;
                PyObject *tmp_type_arg_10;
                tmp_raise_type_5 = PyExc_TypeError;
                tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_5__metaclass );
                tmp_getattr_target_5 = tmp_class_creation_5__metaclass;
                tmp_getattr_attr_5 = const_str_plain___name__;
                tmp_getattr_default_5 = const_str_angle_metaclass;
                tmp_tuple_element_19 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                if ( tmp_tuple_element_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;

                    goto try_except_handler_16;
                }
                tmp_right_name_5 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_19 );
                CHECK_OBJECT( tmp_class_creation_5__prepared );
                tmp_type_arg_10 = tmp_class_creation_5__prepared;
                tmp_source_name_20 = BUILTIN_TYPE1( tmp_type_arg_10 );
                assert( !(tmp_source_name_20 == NULL) );
                tmp_tuple_element_19 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_20 );
                if ( tmp_tuple_element_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_5 );

                    exception_lineno = 111;

                    goto try_except_handler_16;
                }
                PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_19 );
                tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_raise_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;

                    goto try_except_handler_16;
                }
                exception_type = tmp_raise_type_5;
                Py_INCREF( tmp_raise_type_5 );
                exception_value = tmp_raise_value_5;
                exception_lineno = 111;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_16;
            }
            branch_no_19:;
        }
        goto branch_end_18;
        branch_no_18:;
        {
            PyObject *tmp_assign_source_67;
            tmp_assign_source_67 = PyDict_New();
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_67;
        }
        branch_end_18:;
    }
    {
        PyObject *tmp_assign_source_68;
        {
            PyObject *tmp_set_locals_5;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_set_locals_5 = tmp_class_creation_5__prepared;
            locals_extractors_111 = tmp_set_locals_5;
            Py_INCREF( tmp_set_locals_5 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_111, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_18;
        }
        tmp_dictset_value = const_str_plain_PersonExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_111, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto try_except_handler_18;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_adec1ac16e1ded723ba15306b0680e21_6, codeobj_adec1ac16e1ded723ba15306b0680e21, module_extractors, sizeof(void *) );
        frame_adec1ac16e1ded723ba15306b0680e21_6 = cache_frame_adec1ac16e1ded723ba15306b0680e21_6;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_adec1ac16e1ded723ba15306b0680e21_6 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_adec1ac16e1ded723ba15306b0680e21_6 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_13___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_111, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_adec1ac16e1ded723ba15306b0680e21_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_5;

        frame_exception_exit_6:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_adec1ac16e1ded723ba15306b0680e21_6 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_adec1ac16e1ded723ba15306b0680e21_6, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_adec1ac16e1ded723ba15306b0680e21_6->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_adec1ac16e1ded723ba15306b0680e21_6, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_adec1ac16e1ded723ba15306b0680e21_6,
            type_description_2,
            outline_4_var___class__
        );


        // Release cached frame.
        if ( frame_adec1ac16e1ded723ba15306b0680e21_6 == cache_frame_adec1ac16e1ded723ba15306b0680e21_6 )
        {
            Py_DECREF( frame_adec1ac16e1ded723ba15306b0680e21_6 );
        }
        cache_frame_adec1ac16e1ded723ba15306b0680e21_6 = NULL;

        assertFrameObject( frame_adec1ac16e1ded723ba15306b0680e21_6 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_5;

        frame_no_exception_5:;
        goto skip_nested_handling_5;
        nested_frame_exit_5:;

        goto try_except_handler_18;
        skip_nested_handling_5:;
        {
            nuitka_bool tmp_condition_result_31;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_compexpr_left_5 = tmp_class_creation_5__bases;
            CHECK_OBJECT( tmp_class_creation_5__bases_orig );
            tmp_compexpr_right_5 = tmp_class_creation_5__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 111;

                goto try_except_handler_18;
            }
            tmp_condition_result_31 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            CHECK_OBJECT( tmp_class_creation_5__bases_orig );
            tmp_dictset_value = tmp_class_creation_5__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_111, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 111;

                goto try_except_handler_18;
            }
            branch_no_20:;
        }
        {
            PyObject *tmp_assign_source_69;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_name_10;
            PyObject *tmp_tuple_element_20;
            PyObject *tmp_kw_name_10;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_called_name_12 = tmp_class_creation_5__metaclass;
            tmp_tuple_element_20 = const_str_plain_PersonExtractor;
            tmp_args_name_10 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_20 );
            PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_20 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_20 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_20 );
            PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_20 );
            tmp_tuple_element_20 = locals_extractors_111;
            Py_INCREF( tmp_tuple_element_20 );
            PyTuple_SET_ITEM( tmp_args_name_10, 2, tmp_tuple_element_20 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_10 = tmp_class_creation_5__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 111;
            tmp_assign_source_69 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_10, tmp_kw_name_10 );
            Py_DECREF( tmp_args_name_10 );
            if ( tmp_assign_source_69 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 111;

                goto try_except_handler_18;
            }
            assert( outline_4_var___class__ == NULL );
            outline_4_var___class__ = tmp_assign_source_69;
        }
        CHECK_OBJECT( outline_4_var___class__ );
        tmp_assign_source_68 = outline_4_var___class__;
        Py_INCREF( tmp_assign_source_68 );
        goto try_return_handler_18;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_18:;
        Py_DECREF( locals_extractors_111 );
        locals_extractors_111 = NULL;
        goto try_return_handler_17;
        // Exception handler code:
        try_except_handler_18:;
        exception_keeper_type_16 = exception_type;
        exception_keeper_value_16 = exception_value;
        exception_keeper_tb_16 = exception_tb;
        exception_keeper_lineno_16 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_111 );
        locals_extractors_111 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_16;
        exception_value = exception_keeper_value_16;
        exception_tb = exception_keeper_tb_16;
        exception_lineno = exception_keeper_lineno_16;

        goto try_except_handler_17;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_17:;
        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_17 = exception_type;
        exception_keeper_value_17 = exception_value;
        exception_keeper_tb_17 = exception_tb;
        exception_keeper_lineno_17 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_17;
        exception_value = exception_keeper_value_17;
        exception_tb = exception_keeper_tb_17;
        exception_lineno = exception_keeper_lineno_17;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_5:;
        exception_lineno = 111;
        goto try_except_handler_16;
        outline_result_5:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_PersonExtractor, tmp_assign_source_68 );
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_16:;
    exception_keeper_type_18 = exception_type;
    exception_keeper_value_18 = exception_value;
    exception_keeper_tb_18 = exception_tb;
    exception_keeper_lineno_18 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_5__bases_orig );
    tmp_class_creation_5__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    Py_XDECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_18;
    exception_value = exception_keeper_value_18;
    exception_tb = exception_keeper_tb_18;
    exception_lineno = exception_keeper_lineno_18;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases_orig );
    Py_DECREF( tmp_class_creation_5__bases_orig );
    tmp_class_creation_5__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases );
    Py_DECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__class_decl_dict );
    Py_DECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__metaclass );
    Py_DECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__prepared );
    Py_DECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_70;
        PyObject *tmp_tuple_element_21;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;

            goto try_except_handler_19;
        }

        tmp_tuple_element_21 = tmp_mvar_value_8;
        tmp_assign_source_70 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_21 );
        PyTuple_SET_ITEM( tmp_assign_source_70, 0, tmp_tuple_element_21 );
        assert( tmp_class_creation_6__bases_orig == NULL );
        tmp_class_creation_6__bases_orig = tmp_assign_source_70;
    }
    {
        PyObject *tmp_assign_source_71;
        PyObject *tmp_dircall_arg1_6;
        CHECK_OBJECT( tmp_class_creation_6__bases_orig );
        tmp_dircall_arg1_6 = tmp_class_creation_6__bases_orig;
        Py_INCREF( tmp_dircall_arg1_6 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_6};
            tmp_assign_source_71 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        assert( tmp_class_creation_6__bases == NULL );
        tmp_class_creation_6__bases = tmp_assign_source_71;
    }
    {
        PyObject *tmp_assign_source_72;
        tmp_assign_source_72 = PyDict_New();
        assert( tmp_class_creation_6__class_decl_dict == NULL );
        tmp_class_creation_6__class_decl_dict = tmp_assign_source_72;
    }
    {
        PyObject *tmp_assign_source_73;
        PyObject *tmp_metaclass_name_6;
        nuitka_bool tmp_condition_result_32;
        PyObject *tmp_key_name_16;
        PyObject *tmp_dict_name_16;
        PyObject *tmp_dict_name_17;
        PyObject *tmp_key_name_17;
        nuitka_bool tmp_condition_result_33;
        int tmp_truth_name_6;
        PyObject *tmp_type_arg_11;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_bases_name_6;
        tmp_key_name_16 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_16 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_16, tmp_key_name_16 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        tmp_condition_result_32 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_12;
        }
        else
        {
            goto condexpr_false_12;
        }
        condexpr_true_12:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_17 = tmp_class_creation_6__class_decl_dict;
        tmp_key_name_17 = const_str_plain_metaclass;
        tmp_metaclass_name_6 = DICT_GET_ITEM( tmp_dict_name_17, tmp_key_name_17 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        goto condexpr_end_12;
        condexpr_false_12:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_truth_name_6 = CHECK_IF_TRUE( tmp_class_creation_6__bases );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        tmp_condition_result_33 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_13;
        }
        else
        {
            goto condexpr_false_13;
        }
        condexpr_true_13:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_subscribed_name_6 = tmp_class_creation_6__bases;
        tmp_subscript_name_6 = const_int_0;
        tmp_type_arg_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
        if ( tmp_type_arg_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        tmp_metaclass_name_6 = BUILTIN_TYPE1( tmp_type_arg_11 );
        Py_DECREF( tmp_type_arg_11 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        goto condexpr_end_13;
        condexpr_false_13:;
        tmp_metaclass_name_6 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_6 );
        condexpr_end_13:;
        condexpr_end_12:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_bases_name_6 = tmp_class_creation_6__bases;
        tmp_assign_source_73 = SELECT_METACLASS( tmp_metaclass_name_6, tmp_bases_name_6 );
        Py_DECREF( tmp_metaclass_name_6 );
        if ( tmp_assign_source_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        assert( tmp_class_creation_6__metaclass == NULL );
        tmp_class_creation_6__metaclass = tmp_assign_source_73;
    }
    {
        nuitka_bool tmp_condition_result_34;
        PyObject *tmp_key_name_18;
        PyObject *tmp_dict_name_18;
        tmp_key_name_18 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_18 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_18, tmp_key_name_18 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        tmp_condition_result_34 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_21;
        }
        else
        {
            goto branch_no_21;
        }
        branch_yes_21:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_6__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_19;
        }
        branch_no_21:;
    }
    {
        nuitka_bool tmp_condition_result_35;
        PyObject *tmp_source_name_21;
        CHECK_OBJECT( tmp_class_creation_6__metaclass );
        tmp_source_name_21 = tmp_class_creation_6__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_21, const_str_plain___prepare__ );
        tmp_condition_result_35 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_35 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_22;
        }
        else
        {
            goto branch_no_22;
        }
        branch_yes_22:;
        {
            PyObject *tmp_assign_source_74;
            PyObject *tmp_called_name_13;
            PyObject *tmp_source_name_22;
            PyObject *tmp_args_name_11;
            PyObject *tmp_tuple_element_22;
            PyObject *tmp_kw_name_11;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_source_name_22 = tmp_class_creation_6__metaclass;
            tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain___prepare__ );
            if ( tmp_called_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;

                goto try_except_handler_19;
            }
            tmp_tuple_element_22 = const_str_plain_DatesExtractor;
            tmp_args_name_11 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_22 );
            PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_22 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_22 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_22 );
            PyTuple_SET_ITEM( tmp_args_name_11, 1, tmp_tuple_element_22 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_11 = tmp_class_creation_6__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 123;
            tmp_assign_source_74 = CALL_FUNCTION( tmp_called_name_13, tmp_args_name_11, tmp_kw_name_11 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_name_11 );
            if ( tmp_assign_source_74 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;

                goto try_except_handler_19;
            }
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_74;
        }
        {
            nuitka_bool tmp_condition_result_36;
            PyObject *tmp_operand_name_6;
            PyObject *tmp_source_name_23;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_source_name_23 = tmp_class_creation_6__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_23, const_str_plain___getitem__ );
            tmp_operand_name_6 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;

                goto try_except_handler_19;
            }
            tmp_condition_result_36 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_36 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_23;
            }
            else
            {
                goto branch_no_23;
            }
            branch_yes_23:;
            {
                PyObject *tmp_raise_type_6;
                PyObject *tmp_raise_value_6;
                PyObject *tmp_left_name_6;
                PyObject *tmp_right_name_6;
                PyObject *tmp_tuple_element_23;
                PyObject *tmp_getattr_target_6;
                PyObject *tmp_getattr_attr_6;
                PyObject *tmp_getattr_default_6;
                PyObject *tmp_source_name_24;
                PyObject *tmp_type_arg_12;
                tmp_raise_type_6 = PyExc_TypeError;
                tmp_left_name_6 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_6__metaclass );
                tmp_getattr_target_6 = tmp_class_creation_6__metaclass;
                tmp_getattr_attr_6 = const_str_plain___name__;
                tmp_getattr_default_6 = const_str_angle_metaclass;
                tmp_tuple_element_23 = BUILTIN_GETATTR( tmp_getattr_target_6, tmp_getattr_attr_6, tmp_getattr_default_6 );
                if ( tmp_tuple_element_23 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 123;

                    goto try_except_handler_19;
                }
                tmp_right_name_6 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_6, 0, tmp_tuple_element_23 );
                CHECK_OBJECT( tmp_class_creation_6__prepared );
                tmp_type_arg_12 = tmp_class_creation_6__prepared;
                tmp_source_name_24 = BUILTIN_TYPE1( tmp_type_arg_12 );
                assert( !(tmp_source_name_24 == NULL) );
                tmp_tuple_element_23 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_24 );
                if ( tmp_tuple_element_23 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_6 );

                    exception_lineno = 123;

                    goto try_except_handler_19;
                }
                PyTuple_SET_ITEM( tmp_right_name_6, 1, tmp_tuple_element_23 );
                tmp_raise_value_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                Py_DECREF( tmp_right_name_6 );
                if ( tmp_raise_value_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 123;

                    goto try_except_handler_19;
                }
                exception_type = tmp_raise_type_6;
                Py_INCREF( tmp_raise_type_6 );
                exception_value = tmp_raise_value_6;
                exception_lineno = 123;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_19;
            }
            branch_no_23:;
        }
        goto branch_end_22;
        branch_no_22:;
        {
            PyObject *tmp_assign_source_75;
            tmp_assign_source_75 = PyDict_New();
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_75;
        }
        branch_end_22:;
    }
    {
        PyObject *tmp_assign_source_76;
        {
            PyObject *tmp_set_locals_6;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_set_locals_6 = tmp_class_creation_6__prepared;
            locals_extractors_123 = tmp_set_locals_6;
            Py_INCREF( tmp_set_locals_6 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_123, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_21;
        }
        tmp_dictset_value = const_str_plain_DatesExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_123, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;

            goto try_except_handler_21;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_30e6bfaa91e1fe1699cd0d1f3701d474_7, codeobj_30e6bfaa91e1fe1699cd0d1f3701d474, module_extractors, sizeof(void *) );
        frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 = cache_frame_30e6bfaa91e1fe1699cd0d1f3701d474_7;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_14___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_123, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_6;

        frame_exception_exit_7:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_30e6bfaa91e1fe1699cd0d1f3701d474_7->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_30e6bfaa91e1fe1699cd0d1f3701d474_7, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_30e6bfaa91e1fe1699cd0d1f3701d474_7,
            type_description_2,
            outline_5_var___class__
        );


        // Release cached frame.
        if ( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 == cache_frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 )
        {
            Py_DECREF( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 );
        }
        cache_frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 = NULL;

        assertFrameObject( frame_30e6bfaa91e1fe1699cd0d1f3701d474_7 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_6;

        frame_no_exception_6:;
        goto skip_nested_handling_6;
        nested_frame_exit_6:;

        goto try_except_handler_21;
        skip_nested_handling_6:;
        {
            nuitka_bool tmp_condition_result_37;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_compexpr_left_6 = tmp_class_creation_6__bases;
            CHECK_OBJECT( tmp_class_creation_6__bases_orig );
            tmp_compexpr_right_6 = tmp_class_creation_6__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;

                goto try_except_handler_21;
            }
            tmp_condition_result_37 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_37 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_24;
            }
            else
            {
                goto branch_no_24;
            }
            branch_yes_24:;
            CHECK_OBJECT( tmp_class_creation_6__bases_orig );
            tmp_dictset_value = tmp_class_creation_6__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_123, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;

                goto try_except_handler_21;
            }
            branch_no_24:;
        }
        {
            PyObject *tmp_assign_source_77;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_name_12;
            PyObject *tmp_tuple_element_24;
            PyObject *tmp_kw_name_12;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_called_name_14 = tmp_class_creation_6__metaclass;
            tmp_tuple_element_24 = const_str_plain_DatesExtractor;
            tmp_args_name_12 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_24 );
            PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_24 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_24 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_24 );
            PyTuple_SET_ITEM( tmp_args_name_12, 1, tmp_tuple_element_24 );
            tmp_tuple_element_24 = locals_extractors_123;
            Py_INCREF( tmp_tuple_element_24 );
            PyTuple_SET_ITEM( tmp_args_name_12, 2, tmp_tuple_element_24 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_12 = tmp_class_creation_6__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 123;
            tmp_assign_source_77 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_12, tmp_kw_name_12 );
            Py_DECREF( tmp_args_name_12 );
            if ( tmp_assign_source_77 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;

                goto try_except_handler_21;
            }
            assert( outline_5_var___class__ == NULL );
            outline_5_var___class__ = tmp_assign_source_77;
        }
        CHECK_OBJECT( outline_5_var___class__ );
        tmp_assign_source_76 = outline_5_var___class__;
        Py_INCREF( tmp_assign_source_76 );
        goto try_return_handler_21;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_21:;
        Py_DECREF( locals_extractors_123 );
        locals_extractors_123 = NULL;
        goto try_return_handler_20;
        // Exception handler code:
        try_except_handler_21:;
        exception_keeper_type_19 = exception_type;
        exception_keeper_value_19 = exception_value;
        exception_keeper_tb_19 = exception_tb;
        exception_keeper_lineno_19 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_123 );
        locals_extractors_123 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_19;
        exception_value = exception_keeper_value_19;
        exception_tb = exception_keeper_tb_19;
        exception_lineno = exception_keeper_lineno_19;

        goto try_except_handler_20;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_20:;
        CHECK_OBJECT( (PyObject *)outline_5_var___class__ );
        Py_DECREF( outline_5_var___class__ );
        outline_5_var___class__ = NULL;

        goto outline_result_6;
        // Exception handler code:
        try_except_handler_20:;
        exception_keeper_type_20 = exception_type;
        exception_keeper_value_20 = exception_value;
        exception_keeper_tb_20 = exception_tb;
        exception_keeper_lineno_20 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_20;
        exception_value = exception_keeper_value_20;
        exception_tb = exception_keeper_tb_20;
        exception_lineno = exception_keeper_lineno_20;

        goto outline_exception_6;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_6:;
        exception_lineno = 123;
        goto try_except_handler_19;
        outline_result_6:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_DatesExtractor, tmp_assign_source_76 );
    }
    goto try_end_9;
    // Exception handler code:
    try_except_handler_19:;
    exception_keeper_type_21 = exception_type;
    exception_keeper_value_21 = exception_value;
    exception_keeper_tb_21 = exception_tb;
    exception_keeper_lineno_21 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_6__bases_orig );
    tmp_class_creation_6__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    Py_XDECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_21;
    exception_value = exception_keeper_value_21;
    exception_tb = exception_keeper_tb_21;
    exception_lineno = exception_keeper_lineno_21;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases_orig );
    Py_DECREF( tmp_class_creation_6__bases_orig );
    tmp_class_creation_6__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases );
    Py_DECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__class_decl_dict );
    Py_DECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__metaclass );
    Py_DECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__prepared );
    Py_DECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_78;
        PyObject *tmp_tuple_element_25;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;

            goto try_except_handler_22;
        }

        tmp_tuple_element_25 = tmp_mvar_value_9;
        tmp_assign_source_78 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_25 );
        PyTuple_SET_ITEM( tmp_assign_source_78, 0, tmp_tuple_element_25 );
        assert( tmp_class_creation_7__bases_orig == NULL );
        tmp_class_creation_7__bases_orig = tmp_assign_source_78;
    }
    {
        PyObject *tmp_assign_source_79;
        PyObject *tmp_dircall_arg1_7;
        CHECK_OBJECT( tmp_class_creation_7__bases_orig );
        tmp_dircall_arg1_7 = tmp_class_creation_7__bases_orig;
        Py_INCREF( tmp_dircall_arg1_7 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_7};
            tmp_assign_source_79 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_79 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        assert( tmp_class_creation_7__bases == NULL );
        tmp_class_creation_7__bases = tmp_assign_source_79;
    }
    {
        PyObject *tmp_assign_source_80;
        tmp_assign_source_80 = PyDict_New();
        assert( tmp_class_creation_7__class_decl_dict == NULL );
        tmp_class_creation_7__class_decl_dict = tmp_assign_source_80;
    }
    {
        PyObject *tmp_assign_source_81;
        PyObject *tmp_metaclass_name_7;
        nuitka_bool tmp_condition_result_38;
        PyObject *tmp_key_name_19;
        PyObject *tmp_dict_name_19;
        PyObject *tmp_dict_name_20;
        PyObject *tmp_key_name_20;
        nuitka_bool tmp_condition_result_39;
        int tmp_truth_name_7;
        PyObject *tmp_type_arg_13;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_subscript_name_7;
        PyObject *tmp_bases_name_7;
        tmp_key_name_19 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dict_name_19 = tmp_class_creation_7__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_19, tmp_key_name_19 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        tmp_condition_result_38 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_38 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_14;
        }
        else
        {
            goto condexpr_false_14;
        }
        condexpr_true_14:;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dict_name_20 = tmp_class_creation_7__class_decl_dict;
        tmp_key_name_20 = const_str_plain_metaclass;
        tmp_metaclass_name_7 = DICT_GET_ITEM( tmp_dict_name_20, tmp_key_name_20 );
        if ( tmp_metaclass_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        goto condexpr_end_14;
        condexpr_false_14:;
        CHECK_OBJECT( tmp_class_creation_7__bases );
        tmp_truth_name_7 = CHECK_IF_TRUE( tmp_class_creation_7__bases );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        tmp_condition_result_39 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_39 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_15;
        }
        else
        {
            goto condexpr_false_15;
        }
        condexpr_true_15:;
        CHECK_OBJECT( tmp_class_creation_7__bases );
        tmp_subscribed_name_7 = tmp_class_creation_7__bases;
        tmp_subscript_name_7 = const_int_0;
        tmp_type_arg_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 0 );
        if ( tmp_type_arg_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        tmp_metaclass_name_7 = BUILTIN_TYPE1( tmp_type_arg_13 );
        Py_DECREF( tmp_type_arg_13 );
        if ( tmp_metaclass_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        goto condexpr_end_15;
        condexpr_false_15:;
        tmp_metaclass_name_7 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_7 );
        condexpr_end_15:;
        condexpr_end_14:;
        CHECK_OBJECT( tmp_class_creation_7__bases );
        tmp_bases_name_7 = tmp_class_creation_7__bases;
        tmp_assign_source_81 = SELECT_METACLASS( tmp_metaclass_name_7, tmp_bases_name_7 );
        Py_DECREF( tmp_metaclass_name_7 );
        if ( tmp_assign_source_81 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        assert( tmp_class_creation_7__metaclass == NULL );
        tmp_class_creation_7__metaclass = tmp_assign_source_81;
    }
    {
        nuitka_bool tmp_condition_result_40;
        PyObject *tmp_key_name_21;
        PyObject *tmp_dict_name_21;
        tmp_key_name_21 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dict_name_21 = tmp_class_creation_7__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_21, tmp_key_name_21 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        tmp_condition_result_40 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_40 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_25;
        }
        else
        {
            goto branch_no_25;
        }
        branch_yes_25:;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_7__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_22;
        }
        branch_no_25:;
    }
    {
        nuitka_bool tmp_condition_result_41;
        PyObject *tmp_source_name_25;
        CHECK_OBJECT( tmp_class_creation_7__metaclass );
        tmp_source_name_25 = tmp_class_creation_7__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_25, const_str_plain___prepare__ );
        tmp_condition_result_41 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_41 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_26;
        }
        else
        {
            goto branch_no_26;
        }
        branch_yes_26:;
        {
            PyObject *tmp_assign_source_82;
            PyObject *tmp_called_name_15;
            PyObject *tmp_source_name_26;
            PyObject *tmp_args_name_13;
            PyObject *tmp_tuple_element_26;
            PyObject *tmp_kw_name_13;
            CHECK_OBJECT( tmp_class_creation_7__metaclass );
            tmp_source_name_26 = tmp_class_creation_7__metaclass;
            tmp_called_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain___prepare__ );
            if ( tmp_called_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;

                goto try_except_handler_22;
            }
            tmp_tuple_element_26 = const_str_plain_MoneyExtractor;
            tmp_args_name_13 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_26 );
            PyTuple_SET_ITEM( tmp_args_name_13, 0, tmp_tuple_element_26 );
            CHECK_OBJECT( tmp_class_creation_7__bases );
            tmp_tuple_element_26 = tmp_class_creation_7__bases;
            Py_INCREF( tmp_tuple_element_26 );
            PyTuple_SET_ITEM( tmp_args_name_13, 1, tmp_tuple_element_26 );
            CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
            tmp_kw_name_13 = tmp_class_creation_7__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 128;
            tmp_assign_source_82 = CALL_FUNCTION( tmp_called_name_15, tmp_args_name_13, tmp_kw_name_13 );
            Py_DECREF( tmp_called_name_15 );
            Py_DECREF( tmp_args_name_13 );
            if ( tmp_assign_source_82 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;

                goto try_except_handler_22;
            }
            assert( tmp_class_creation_7__prepared == NULL );
            tmp_class_creation_7__prepared = tmp_assign_source_82;
        }
        {
            nuitka_bool tmp_condition_result_42;
            PyObject *tmp_operand_name_7;
            PyObject *tmp_source_name_27;
            CHECK_OBJECT( tmp_class_creation_7__prepared );
            tmp_source_name_27 = tmp_class_creation_7__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_27, const_str_plain___getitem__ );
            tmp_operand_name_7 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;

                goto try_except_handler_22;
            }
            tmp_condition_result_42 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_42 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_27;
            }
            else
            {
                goto branch_no_27;
            }
            branch_yes_27:;
            {
                PyObject *tmp_raise_type_7;
                PyObject *tmp_raise_value_7;
                PyObject *tmp_left_name_7;
                PyObject *tmp_right_name_7;
                PyObject *tmp_tuple_element_27;
                PyObject *tmp_getattr_target_7;
                PyObject *tmp_getattr_attr_7;
                PyObject *tmp_getattr_default_7;
                PyObject *tmp_source_name_28;
                PyObject *tmp_type_arg_14;
                tmp_raise_type_7 = PyExc_TypeError;
                tmp_left_name_7 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_7__metaclass );
                tmp_getattr_target_7 = tmp_class_creation_7__metaclass;
                tmp_getattr_attr_7 = const_str_plain___name__;
                tmp_getattr_default_7 = const_str_angle_metaclass;
                tmp_tuple_element_27 = BUILTIN_GETATTR( tmp_getattr_target_7, tmp_getattr_attr_7, tmp_getattr_default_7 );
                if ( tmp_tuple_element_27 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 128;

                    goto try_except_handler_22;
                }
                tmp_right_name_7 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_7, 0, tmp_tuple_element_27 );
                CHECK_OBJECT( tmp_class_creation_7__prepared );
                tmp_type_arg_14 = tmp_class_creation_7__prepared;
                tmp_source_name_28 = BUILTIN_TYPE1( tmp_type_arg_14 );
                assert( !(tmp_source_name_28 == NULL) );
                tmp_tuple_element_27 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_28 );
                if ( tmp_tuple_element_27 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_7 );

                    exception_lineno = 128;

                    goto try_except_handler_22;
                }
                PyTuple_SET_ITEM( tmp_right_name_7, 1, tmp_tuple_element_27 );
                tmp_raise_value_7 = BINARY_OPERATION_REMAINDER( tmp_left_name_7, tmp_right_name_7 );
                Py_DECREF( tmp_right_name_7 );
                if ( tmp_raise_value_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 128;

                    goto try_except_handler_22;
                }
                exception_type = tmp_raise_type_7;
                Py_INCREF( tmp_raise_type_7 );
                exception_value = tmp_raise_value_7;
                exception_lineno = 128;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_22;
            }
            branch_no_27:;
        }
        goto branch_end_26;
        branch_no_26:;
        {
            PyObject *tmp_assign_source_83;
            tmp_assign_source_83 = PyDict_New();
            assert( tmp_class_creation_7__prepared == NULL );
            tmp_class_creation_7__prepared = tmp_assign_source_83;
        }
        branch_end_26:;
    }
    {
        PyObject *tmp_assign_source_84;
        {
            PyObject *tmp_set_locals_7;
            CHECK_OBJECT( tmp_class_creation_7__prepared );
            tmp_set_locals_7 = tmp_class_creation_7__prepared;
            locals_extractors_128 = tmp_set_locals_7;
            Py_INCREF( tmp_set_locals_7 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_128, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_24;
        }
        tmp_dictset_value = const_str_plain_MoneyExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_128, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;

            goto try_except_handler_24;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_c193d8406a0c58560a78960b7c28d662_8, codeobj_c193d8406a0c58560a78960b7c28d662, module_extractors, sizeof(void *) );
        frame_c193d8406a0c58560a78960b7c28d662_8 = cache_frame_c193d8406a0c58560a78960b7c28d662_8;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_c193d8406a0c58560a78960b7c28d662_8 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_c193d8406a0c58560a78960b7c28d662_8 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_15___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_128, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_2 = "o";
            goto frame_exception_exit_8;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c193d8406a0c58560a78960b7c28d662_8 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_7;

        frame_exception_exit_8:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c193d8406a0c58560a78960b7c28d662_8 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_c193d8406a0c58560a78960b7c28d662_8, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_c193d8406a0c58560a78960b7c28d662_8->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_c193d8406a0c58560a78960b7c28d662_8, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_c193d8406a0c58560a78960b7c28d662_8,
            type_description_2,
            outline_6_var___class__
        );


        // Release cached frame.
        if ( frame_c193d8406a0c58560a78960b7c28d662_8 == cache_frame_c193d8406a0c58560a78960b7c28d662_8 )
        {
            Py_DECREF( frame_c193d8406a0c58560a78960b7c28d662_8 );
        }
        cache_frame_c193d8406a0c58560a78960b7c28d662_8 = NULL;

        assertFrameObject( frame_c193d8406a0c58560a78960b7c28d662_8 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_7;

        frame_no_exception_7:;
        goto skip_nested_handling_7;
        nested_frame_exit_7:;

        goto try_except_handler_24;
        skip_nested_handling_7:;
        {
            nuitka_bool tmp_condition_result_43;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            CHECK_OBJECT( tmp_class_creation_7__bases );
            tmp_compexpr_left_7 = tmp_class_creation_7__bases;
            CHECK_OBJECT( tmp_class_creation_7__bases_orig );
            tmp_compexpr_right_7 = tmp_class_creation_7__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;

                goto try_except_handler_24;
            }
            tmp_condition_result_43 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_43 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_28;
            }
            else
            {
                goto branch_no_28;
            }
            branch_yes_28:;
            CHECK_OBJECT( tmp_class_creation_7__bases_orig );
            tmp_dictset_value = tmp_class_creation_7__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_128, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;

                goto try_except_handler_24;
            }
            branch_no_28:;
        }
        {
            PyObject *tmp_assign_source_85;
            PyObject *tmp_called_name_16;
            PyObject *tmp_args_name_14;
            PyObject *tmp_tuple_element_28;
            PyObject *tmp_kw_name_14;
            CHECK_OBJECT( tmp_class_creation_7__metaclass );
            tmp_called_name_16 = tmp_class_creation_7__metaclass;
            tmp_tuple_element_28 = const_str_plain_MoneyExtractor;
            tmp_args_name_14 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_28 );
            PyTuple_SET_ITEM( tmp_args_name_14, 0, tmp_tuple_element_28 );
            CHECK_OBJECT( tmp_class_creation_7__bases );
            tmp_tuple_element_28 = tmp_class_creation_7__bases;
            Py_INCREF( tmp_tuple_element_28 );
            PyTuple_SET_ITEM( tmp_args_name_14, 1, tmp_tuple_element_28 );
            tmp_tuple_element_28 = locals_extractors_128;
            Py_INCREF( tmp_tuple_element_28 );
            PyTuple_SET_ITEM( tmp_args_name_14, 2, tmp_tuple_element_28 );
            CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
            tmp_kw_name_14 = tmp_class_creation_7__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 128;
            tmp_assign_source_85 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_14, tmp_kw_name_14 );
            Py_DECREF( tmp_args_name_14 );
            if ( tmp_assign_source_85 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;

                goto try_except_handler_24;
            }
            assert( outline_6_var___class__ == NULL );
            outline_6_var___class__ = tmp_assign_source_85;
        }
        CHECK_OBJECT( outline_6_var___class__ );
        tmp_assign_source_84 = outline_6_var___class__;
        Py_INCREF( tmp_assign_source_84 );
        goto try_return_handler_24;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_24:;
        Py_DECREF( locals_extractors_128 );
        locals_extractors_128 = NULL;
        goto try_return_handler_23;
        // Exception handler code:
        try_except_handler_24:;
        exception_keeper_type_22 = exception_type;
        exception_keeper_value_22 = exception_value;
        exception_keeper_tb_22 = exception_tb;
        exception_keeper_lineno_22 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_128 );
        locals_extractors_128 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_22;
        exception_value = exception_keeper_value_22;
        exception_tb = exception_keeper_tb_22;
        exception_lineno = exception_keeper_lineno_22;

        goto try_except_handler_23;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_23:;
        CHECK_OBJECT( (PyObject *)outline_6_var___class__ );
        Py_DECREF( outline_6_var___class__ );
        outline_6_var___class__ = NULL;

        goto outline_result_7;
        // Exception handler code:
        try_except_handler_23:;
        exception_keeper_type_23 = exception_type;
        exception_keeper_value_23 = exception_value;
        exception_keeper_tb_23 = exception_tb;
        exception_keeper_lineno_23 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_23;
        exception_value = exception_keeper_value_23;
        exception_tb = exception_keeper_tb_23;
        exception_lineno = exception_keeper_lineno_23;

        goto outline_exception_7;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_7:;
        exception_lineno = 128;
        goto try_except_handler_22;
        outline_result_7:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MoneyExtractor, tmp_assign_source_84 );
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_22:;
    exception_keeper_type_24 = exception_type;
    exception_keeper_value_24 = exception_value;
    exception_keeper_tb_24 = exception_tb;
    exception_keeper_lineno_24 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_7__bases_orig );
    tmp_class_creation_7__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_7__bases );
    tmp_class_creation_7__bases = NULL;

    Py_XDECREF( tmp_class_creation_7__class_decl_dict );
    tmp_class_creation_7__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_7__metaclass );
    tmp_class_creation_7__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_7__prepared );
    tmp_class_creation_7__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_24;
    exception_value = exception_keeper_value_24;
    exception_tb = exception_keeper_tb_24;
    exception_lineno = exception_keeper_lineno_24;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__bases_orig );
    Py_DECREF( tmp_class_creation_7__bases_orig );
    tmp_class_creation_7__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__bases );
    Py_DECREF( tmp_class_creation_7__bases );
    tmp_class_creation_7__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__class_decl_dict );
    Py_DECREF( tmp_class_creation_7__class_decl_dict );
    tmp_class_creation_7__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__metaclass );
    Py_DECREF( tmp_class_creation_7__metaclass );
    tmp_class_creation_7__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__prepared );
    Py_DECREF( tmp_class_creation_7__prepared );
    tmp_class_creation_7__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_86;
        PyObject *tmp_tuple_element_29;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;

            goto try_except_handler_25;
        }

        tmp_tuple_element_29 = tmp_mvar_value_10;
        tmp_assign_source_86 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_29 );
        PyTuple_SET_ITEM( tmp_assign_source_86, 0, tmp_tuple_element_29 );
        assert( tmp_class_creation_8__bases_orig == NULL );
        tmp_class_creation_8__bases_orig = tmp_assign_source_86;
    }
    {
        PyObject *tmp_assign_source_87;
        PyObject *tmp_dircall_arg1_8;
        CHECK_OBJECT( tmp_class_creation_8__bases_orig );
        tmp_dircall_arg1_8 = tmp_class_creation_8__bases_orig;
        Py_INCREF( tmp_dircall_arg1_8 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_8};
            tmp_assign_source_87 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_87 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        assert( tmp_class_creation_8__bases == NULL );
        tmp_class_creation_8__bases = tmp_assign_source_87;
    }
    {
        PyObject *tmp_assign_source_88;
        tmp_assign_source_88 = PyDict_New();
        assert( tmp_class_creation_8__class_decl_dict == NULL );
        tmp_class_creation_8__class_decl_dict = tmp_assign_source_88;
    }
    {
        PyObject *tmp_assign_source_89;
        PyObject *tmp_metaclass_name_8;
        nuitka_bool tmp_condition_result_44;
        PyObject *tmp_key_name_22;
        PyObject *tmp_dict_name_22;
        PyObject *tmp_dict_name_23;
        PyObject *tmp_key_name_23;
        nuitka_bool tmp_condition_result_45;
        int tmp_truth_name_8;
        PyObject *tmp_type_arg_15;
        PyObject *tmp_subscribed_name_8;
        PyObject *tmp_subscript_name_8;
        PyObject *tmp_bases_name_8;
        tmp_key_name_22 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dict_name_22 = tmp_class_creation_8__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_22, tmp_key_name_22 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        tmp_condition_result_44 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_44 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_16;
        }
        else
        {
            goto condexpr_false_16;
        }
        condexpr_true_16:;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dict_name_23 = tmp_class_creation_8__class_decl_dict;
        tmp_key_name_23 = const_str_plain_metaclass;
        tmp_metaclass_name_8 = DICT_GET_ITEM( tmp_dict_name_23, tmp_key_name_23 );
        if ( tmp_metaclass_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        goto condexpr_end_16;
        condexpr_false_16:;
        CHECK_OBJECT( tmp_class_creation_8__bases );
        tmp_truth_name_8 = CHECK_IF_TRUE( tmp_class_creation_8__bases );
        if ( tmp_truth_name_8 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        tmp_condition_result_45 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_45 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_17;
        }
        else
        {
            goto condexpr_false_17;
        }
        condexpr_true_17:;
        CHECK_OBJECT( tmp_class_creation_8__bases );
        tmp_subscribed_name_8 = tmp_class_creation_8__bases;
        tmp_subscript_name_8 = const_int_0;
        tmp_type_arg_15 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_8, tmp_subscript_name_8, 0 );
        if ( tmp_type_arg_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        tmp_metaclass_name_8 = BUILTIN_TYPE1( tmp_type_arg_15 );
        Py_DECREF( tmp_type_arg_15 );
        if ( tmp_metaclass_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        goto condexpr_end_17;
        condexpr_false_17:;
        tmp_metaclass_name_8 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_8 );
        condexpr_end_17:;
        condexpr_end_16:;
        CHECK_OBJECT( tmp_class_creation_8__bases );
        tmp_bases_name_8 = tmp_class_creation_8__bases;
        tmp_assign_source_89 = SELECT_METACLASS( tmp_metaclass_name_8, tmp_bases_name_8 );
        Py_DECREF( tmp_metaclass_name_8 );
        if ( tmp_assign_source_89 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        assert( tmp_class_creation_8__metaclass == NULL );
        tmp_class_creation_8__metaclass = tmp_assign_source_89;
    }
    {
        nuitka_bool tmp_condition_result_46;
        PyObject *tmp_key_name_24;
        PyObject *tmp_dict_name_24;
        tmp_key_name_24 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dict_name_24 = tmp_class_creation_8__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_24, tmp_key_name_24 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        tmp_condition_result_46 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_46 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_29;
        }
        else
        {
            goto branch_no_29;
        }
        branch_yes_29:;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_8__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_25;
        }
        branch_no_29:;
    }
    {
        nuitka_bool tmp_condition_result_47;
        PyObject *tmp_source_name_29;
        CHECK_OBJECT( tmp_class_creation_8__metaclass );
        tmp_source_name_29 = tmp_class_creation_8__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_29, const_str_plain___prepare__ );
        tmp_condition_result_47 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_47 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_30;
        }
        else
        {
            goto branch_no_30;
        }
        branch_yes_30:;
        {
            PyObject *tmp_assign_source_90;
            PyObject *tmp_called_name_17;
            PyObject *tmp_source_name_30;
            PyObject *tmp_args_name_15;
            PyObject *tmp_tuple_element_30;
            PyObject *tmp_kw_name_15;
            CHECK_OBJECT( tmp_class_creation_8__metaclass );
            tmp_source_name_30 = tmp_class_creation_8__metaclass;
            tmp_called_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain___prepare__ );
            if ( tmp_called_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;

                goto try_except_handler_25;
            }
            tmp_tuple_element_30 = const_str_plain_MoneyRateExtractor;
            tmp_args_name_15 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_30 );
            PyTuple_SET_ITEM( tmp_args_name_15, 0, tmp_tuple_element_30 );
            CHECK_OBJECT( tmp_class_creation_8__bases );
            tmp_tuple_element_30 = tmp_class_creation_8__bases;
            Py_INCREF( tmp_tuple_element_30 );
            PyTuple_SET_ITEM( tmp_args_name_15, 1, tmp_tuple_element_30 );
            CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
            tmp_kw_name_15 = tmp_class_creation_8__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 133;
            tmp_assign_source_90 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_15, tmp_kw_name_15 );
            Py_DECREF( tmp_called_name_17 );
            Py_DECREF( tmp_args_name_15 );
            if ( tmp_assign_source_90 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;

                goto try_except_handler_25;
            }
            assert( tmp_class_creation_8__prepared == NULL );
            tmp_class_creation_8__prepared = tmp_assign_source_90;
        }
        {
            nuitka_bool tmp_condition_result_48;
            PyObject *tmp_operand_name_8;
            PyObject *tmp_source_name_31;
            CHECK_OBJECT( tmp_class_creation_8__prepared );
            tmp_source_name_31 = tmp_class_creation_8__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_31, const_str_plain___getitem__ );
            tmp_operand_name_8 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;

                goto try_except_handler_25;
            }
            tmp_condition_result_48 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_48 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_31;
            }
            else
            {
                goto branch_no_31;
            }
            branch_yes_31:;
            {
                PyObject *tmp_raise_type_8;
                PyObject *tmp_raise_value_8;
                PyObject *tmp_left_name_8;
                PyObject *tmp_right_name_8;
                PyObject *tmp_tuple_element_31;
                PyObject *tmp_getattr_target_8;
                PyObject *tmp_getattr_attr_8;
                PyObject *tmp_getattr_default_8;
                PyObject *tmp_source_name_32;
                PyObject *tmp_type_arg_16;
                tmp_raise_type_8 = PyExc_TypeError;
                tmp_left_name_8 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_8__metaclass );
                tmp_getattr_target_8 = tmp_class_creation_8__metaclass;
                tmp_getattr_attr_8 = const_str_plain___name__;
                tmp_getattr_default_8 = const_str_angle_metaclass;
                tmp_tuple_element_31 = BUILTIN_GETATTR( tmp_getattr_target_8, tmp_getattr_attr_8, tmp_getattr_default_8 );
                if ( tmp_tuple_element_31 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 133;

                    goto try_except_handler_25;
                }
                tmp_right_name_8 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_8, 0, tmp_tuple_element_31 );
                CHECK_OBJECT( tmp_class_creation_8__prepared );
                tmp_type_arg_16 = tmp_class_creation_8__prepared;
                tmp_source_name_32 = BUILTIN_TYPE1( tmp_type_arg_16 );
                assert( !(tmp_source_name_32 == NULL) );
                tmp_tuple_element_31 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_32 );
                if ( tmp_tuple_element_31 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_8 );

                    exception_lineno = 133;

                    goto try_except_handler_25;
                }
                PyTuple_SET_ITEM( tmp_right_name_8, 1, tmp_tuple_element_31 );
                tmp_raise_value_8 = BINARY_OPERATION_REMAINDER( tmp_left_name_8, tmp_right_name_8 );
                Py_DECREF( tmp_right_name_8 );
                if ( tmp_raise_value_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 133;

                    goto try_except_handler_25;
                }
                exception_type = tmp_raise_type_8;
                Py_INCREF( tmp_raise_type_8 );
                exception_value = tmp_raise_value_8;
                exception_lineno = 133;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_25;
            }
            branch_no_31:;
        }
        goto branch_end_30;
        branch_no_30:;
        {
            PyObject *tmp_assign_source_91;
            tmp_assign_source_91 = PyDict_New();
            assert( tmp_class_creation_8__prepared == NULL );
            tmp_class_creation_8__prepared = tmp_assign_source_91;
        }
        branch_end_30:;
    }
    {
        PyObject *tmp_assign_source_92;
        {
            PyObject *tmp_set_locals_8;
            CHECK_OBJECT( tmp_class_creation_8__prepared );
            tmp_set_locals_8 = tmp_class_creation_8__prepared;
            locals_extractors_133 = tmp_set_locals_8;
            Py_INCREF( tmp_set_locals_8 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_133, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_27;
        }
        tmp_dictset_value = const_str_plain_MoneyRateExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_133, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto try_except_handler_27;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_09787e6b9a91e7687ca4e65eb22138ad_9, codeobj_09787e6b9a91e7687ca4e65eb22138ad, module_extractors, sizeof(void *) );
        frame_09787e6b9a91e7687ca4e65eb22138ad_9 = cache_frame_09787e6b9a91e7687ca4e65eb22138ad_9;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_09787e6b9a91e7687ca4e65eb22138ad_9 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_09787e6b9a91e7687ca4e65eb22138ad_9 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_16___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_133, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_2 = "o";
            goto frame_exception_exit_9;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_09787e6b9a91e7687ca4e65eb22138ad_9 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_8;

        frame_exception_exit_9:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_09787e6b9a91e7687ca4e65eb22138ad_9 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_09787e6b9a91e7687ca4e65eb22138ad_9, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_09787e6b9a91e7687ca4e65eb22138ad_9->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_09787e6b9a91e7687ca4e65eb22138ad_9, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_09787e6b9a91e7687ca4e65eb22138ad_9,
            type_description_2,
            outline_7_var___class__
        );


        // Release cached frame.
        if ( frame_09787e6b9a91e7687ca4e65eb22138ad_9 == cache_frame_09787e6b9a91e7687ca4e65eb22138ad_9 )
        {
            Py_DECREF( frame_09787e6b9a91e7687ca4e65eb22138ad_9 );
        }
        cache_frame_09787e6b9a91e7687ca4e65eb22138ad_9 = NULL;

        assertFrameObject( frame_09787e6b9a91e7687ca4e65eb22138ad_9 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_8;

        frame_no_exception_8:;
        goto skip_nested_handling_8;
        nested_frame_exit_8:;

        goto try_except_handler_27;
        skip_nested_handling_8:;
        {
            nuitka_bool tmp_condition_result_49;
            PyObject *tmp_compexpr_left_8;
            PyObject *tmp_compexpr_right_8;
            CHECK_OBJECT( tmp_class_creation_8__bases );
            tmp_compexpr_left_8 = tmp_class_creation_8__bases;
            CHECK_OBJECT( tmp_class_creation_8__bases_orig );
            tmp_compexpr_right_8 = tmp_class_creation_8__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;

                goto try_except_handler_27;
            }
            tmp_condition_result_49 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_49 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_32;
            }
            else
            {
                goto branch_no_32;
            }
            branch_yes_32:;
            CHECK_OBJECT( tmp_class_creation_8__bases_orig );
            tmp_dictset_value = tmp_class_creation_8__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_133, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;

                goto try_except_handler_27;
            }
            branch_no_32:;
        }
        {
            PyObject *tmp_assign_source_93;
            PyObject *tmp_called_name_18;
            PyObject *tmp_args_name_16;
            PyObject *tmp_tuple_element_32;
            PyObject *tmp_kw_name_16;
            CHECK_OBJECT( tmp_class_creation_8__metaclass );
            tmp_called_name_18 = tmp_class_creation_8__metaclass;
            tmp_tuple_element_32 = const_str_plain_MoneyRateExtractor;
            tmp_args_name_16 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_32 );
            PyTuple_SET_ITEM( tmp_args_name_16, 0, tmp_tuple_element_32 );
            CHECK_OBJECT( tmp_class_creation_8__bases );
            tmp_tuple_element_32 = tmp_class_creation_8__bases;
            Py_INCREF( tmp_tuple_element_32 );
            PyTuple_SET_ITEM( tmp_args_name_16, 1, tmp_tuple_element_32 );
            tmp_tuple_element_32 = locals_extractors_133;
            Py_INCREF( tmp_tuple_element_32 );
            PyTuple_SET_ITEM( tmp_args_name_16, 2, tmp_tuple_element_32 );
            CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
            tmp_kw_name_16 = tmp_class_creation_8__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 133;
            tmp_assign_source_93 = CALL_FUNCTION( tmp_called_name_18, tmp_args_name_16, tmp_kw_name_16 );
            Py_DECREF( tmp_args_name_16 );
            if ( tmp_assign_source_93 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;

                goto try_except_handler_27;
            }
            assert( outline_7_var___class__ == NULL );
            outline_7_var___class__ = tmp_assign_source_93;
        }
        CHECK_OBJECT( outline_7_var___class__ );
        tmp_assign_source_92 = outline_7_var___class__;
        Py_INCREF( tmp_assign_source_92 );
        goto try_return_handler_27;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_27:;
        Py_DECREF( locals_extractors_133 );
        locals_extractors_133 = NULL;
        goto try_return_handler_26;
        // Exception handler code:
        try_except_handler_27:;
        exception_keeper_type_25 = exception_type;
        exception_keeper_value_25 = exception_value;
        exception_keeper_tb_25 = exception_tb;
        exception_keeper_lineno_25 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_133 );
        locals_extractors_133 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_25;
        exception_value = exception_keeper_value_25;
        exception_tb = exception_keeper_tb_25;
        exception_lineno = exception_keeper_lineno_25;

        goto try_except_handler_26;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_26:;
        CHECK_OBJECT( (PyObject *)outline_7_var___class__ );
        Py_DECREF( outline_7_var___class__ );
        outline_7_var___class__ = NULL;

        goto outline_result_8;
        // Exception handler code:
        try_except_handler_26:;
        exception_keeper_type_26 = exception_type;
        exception_keeper_value_26 = exception_value;
        exception_keeper_tb_26 = exception_tb;
        exception_keeper_lineno_26 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_26;
        exception_value = exception_keeper_value_26;
        exception_tb = exception_keeper_tb_26;
        exception_lineno = exception_keeper_lineno_26;

        goto outline_exception_8;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_8:;
        exception_lineno = 133;
        goto try_except_handler_25;
        outline_result_8:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MoneyRateExtractor, tmp_assign_source_92 );
    }
    goto try_end_11;
    // Exception handler code:
    try_except_handler_25:;
    exception_keeper_type_27 = exception_type;
    exception_keeper_value_27 = exception_value;
    exception_keeper_tb_27 = exception_tb;
    exception_keeper_lineno_27 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_8__bases_orig );
    tmp_class_creation_8__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_8__bases );
    tmp_class_creation_8__bases = NULL;

    Py_XDECREF( tmp_class_creation_8__class_decl_dict );
    tmp_class_creation_8__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_8__metaclass );
    tmp_class_creation_8__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_8__prepared );
    tmp_class_creation_8__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_27;
    exception_value = exception_keeper_value_27;
    exception_tb = exception_keeper_tb_27;
    exception_lineno = exception_keeper_lineno_27;

    goto frame_exception_exit_1;
    // End of try:
    try_end_11:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__bases_orig );
    Py_DECREF( tmp_class_creation_8__bases_orig );
    tmp_class_creation_8__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__bases );
    Py_DECREF( tmp_class_creation_8__bases );
    tmp_class_creation_8__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__class_decl_dict );
    Py_DECREF( tmp_class_creation_8__class_decl_dict );
    tmp_class_creation_8__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__metaclass );
    Py_DECREF( tmp_class_creation_8__metaclass );
    tmp_class_creation_8__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__prepared );
    Py_DECREF( tmp_class_creation_8__prepared );
    tmp_class_creation_8__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_94;
        PyObject *tmp_tuple_element_33;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;

            goto try_except_handler_28;
        }

        tmp_tuple_element_33 = tmp_mvar_value_11;
        tmp_assign_source_94 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_33 );
        PyTuple_SET_ITEM( tmp_assign_source_94, 0, tmp_tuple_element_33 );
        assert( tmp_class_creation_9__bases_orig == NULL );
        tmp_class_creation_9__bases_orig = tmp_assign_source_94;
    }
    {
        PyObject *tmp_assign_source_95;
        PyObject *tmp_dircall_arg1_9;
        CHECK_OBJECT( tmp_class_creation_9__bases_orig );
        tmp_dircall_arg1_9 = tmp_class_creation_9__bases_orig;
        Py_INCREF( tmp_dircall_arg1_9 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_9};
            tmp_assign_source_95 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_95 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        assert( tmp_class_creation_9__bases == NULL );
        tmp_class_creation_9__bases = tmp_assign_source_95;
    }
    {
        PyObject *tmp_assign_source_96;
        tmp_assign_source_96 = PyDict_New();
        assert( tmp_class_creation_9__class_decl_dict == NULL );
        tmp_class_creation_9__class_decl_dict = tmp_assign_source_96;
    }
    {
        PyObject *tmp_assign_source_97;
        PyObject *tmp_metaclass_name_9;
        nuitka_bool tmp_condition_result_50;
        PyObject *tmp_key_name_25;
        PyObject *tmp_dict_name_25;
        PyObject *tmp_dict_name_26;
        PyObject *tmp_key_name_26;
        nuitka_bool tmp_condition_result_51;
        int tmp_truth_name_9;
        PyObject *tmp_type_arg_17;
        PyObject *tmp_subscribed_name_9;
        PyObject *tmp_subscript_name_9;
        PyObject *tmp_bases_name_9;
        tmp_key_name_25 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dict_name_25 = tmp_class_creation_9__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_25, tmp_key_name_25 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        tmp_condition_result_50 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_50 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_18;
        }
        else
        {
            goto condexpr_false_18;
        }
        condexpr_true_18:;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dict_name_26 = tmp_class_creation_9__class_decl_dict;
        tmp_key_name_26 = const_str_plain_metaclass;
        tmp_metaclass_name_9 = DICT_GET_ITEM( tmp_dict_name_26, tmp_key_name_26 );
        if ( tmp_metaclass_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        goto condexpr_end_18;
        condexpr_false_18:;
        CHECK_OBJECT( tmp_class_creation_9__bases );
        tmp_truth_name_9 = CHECK_IF_TRUE( tmp_class_creation_9__bases );
        if ( tmp_truth_name_9 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        tmp_condition_result_51 = tmp_truth_name_9 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_51 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_19;
        }
        else
        {
            goto condexpr_false_19;
        }
        condexpr_true_19:;
        CHECK_OBJECT( tmp_class_creation_9__bases );
        tmp_subscribed_name_9 = tmp_class_creation_9__bases;
        tmp_subscript_name_9 = const_int_0;
        tmp_type_arg_17 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_9, tmp_subscript_name_9, 0 );
        if ( tmp_type_arg_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        tmp_metaclass_name_9 = BUILTIN_TYPE1( tmp_type_arg_17 );
        Py_DECREF( tmp_type_arg_17 );
        if ( tmp_metaclass_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        goto condexpr_end_19;
        condexpr_false_19:;
        tmp_metaclass_name_9 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_9 );
        condexpr_end_19:;
        condexpr_end_18:;
        CHECK_OBJECT( tmp_class_creation_9__bases );
        tmp_bases_name_9 = tmp_class_creation_9__bases;
        tmp_assign_source_97 = SELECT_METACLASS( tmp_metaclass_name_9, tmp_bases_name_9 );
        Py_DECREF( tmp_metaclass_name_9 );
        if ( tmp_assign_source_97 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        assert( tmp_class_creation_9__metaclass == NULL );
        tmp_class_creation_9__metaclass = tmp_assign_source_97;
    }
    {
        nuitka_bool tmp_condition_result_52;
        PyObject *tmp_key_name_27;
        PyObject *tmp_dict_name_27;
        tmp_key_name_27 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dict_name_27 = tmp_class_creation_9__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_27, tmp_key_name_27 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        tmp_condition_result_52 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_52 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_33;
        }
        else
        {
            goto branch_no_33;
        }
        branch_yes_33:;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_9__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_28;
        }
        branch_no_33:;
    }
    {
        nuitka_bool tmp_condition_result_53;
        PyObject *tmp_source_name_33;
        CHECK_OBJECT( tmp_class_creation_9__metaclass );
        tmp_source_name_33 = tmp_class_creation_9__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_33, const_str_plain___prepare__ );
        tmp_condition_result_53 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_53 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_34;
        }
        else
        {
            goto branch_no_34;
        }
        branch_yes_34:;
        {
            PyObject *tmp_assign_source_98;
            PyObject *tmp_called_name_19;
            PyObject *tmp_source_name_34;
            PyObject *tmp_args_name_17;
            PyObject *tmp_tuple_element_34;
            PyObject *tmp_kw_name_17;
            CHECK_OBJECT( tmp_class_creation_9__metaclass );
            tmp_source_name_34 = tmp_class_creation_9__metaclass;
            tmp_called_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain___prepare__ );
            if ( tmp_called_name_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_28;
            }
            tmp_tuple_element_34 = const_str_plain_MoneyRangeExtractor;
            tmp_args_name_17 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_34 );
            PyTuple_SET_ITEM( tmp_args_name_17, 0, tmp_tuple_element_34 );
            CHECK_OBJECT( tmp_class_creation_9__bases );
            tmp_tuple_element_34 = tmp_class_creation_9__bases;
            Py_INCREF( tmp_tuple_element_34 );
            PyTuple_SET_ITEM( tmp_args_name_17, 1, tmp_tuple_element_34 );
            CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
            tmp_kw_name_17 = tmp_class_creation_9__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 138;
            tmp_assign_source_98 = CALL_FUNCTION( tmp_called_name_19, tmp_args_name_17, tmp_kw_name_17 );
            Py_DECREF( tmp_called_name_19 );
            Py_DECREF( tmp_args_name_17 );
            if ( tmp_assign_source_98 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_28;
            }
            assert( tmp_class_creation_9__prepared == NULL );
            tmp_class_creation_9__prepared = tmp_assign_source_98;
        }
        {
            nuitka_bool tmp_condition_result_54;
            PyObject *tmp_operand_name_9;
            PyObject *tmp_source_name_35;
            CHECK_OBJECT( tmp_class_creation_9__prepared );
            tmp_source_name_35 = tmp_class_creation_9__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_35, const_str_plain___getitem__ );
            tmp_operand_name_9 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_9 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_28;
            }
            tmp_condition_result_54 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_54 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_35;
            }
            else
            {
                goto branch_no_35;
            }
            branch_yes_35:;
            {
                PyObject *tmp_raise_type_9;
                PyObject *tmp_raise_value_9;
                PyObject *tmp_left_name_9;
                PyObject *tmp_right_name_9;
                PyObject *tmp_tuple_element_35;
                PyObject *tmp_getattr_target_9;
                PyObject *tmp_getattr_attr_9;
                PyObject *tmp_getattr_default_9;
                PyObject *tmp_source_name_36;
                PyObject *tmp_type_arg_18;
                tmp_raise_type_9 = PyExc_TypeError;
                tmp_left_name_9 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_9__metaclass );
                tmp_getattr_target_9 = tmp_class_creation_9__metaclass;
                tmp_getattr_attr_9 = const_str_plain___name__;
                tmp_getattr_default_9 = const_str_angle_metaclass;
                tmp_tuple_element_35 = BUILTIN_GETATTR( tmp_getattr_target_9, tmp_getattr_attr_9, tmp_getattr_default_9 );
                if ( tmp_tuple_element_35 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;

                    goto try_except_handler_28;
                }
                tmp_right_name_9 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_9, 0, tmp_tuple_element_35 );
                CHECK_OBJECT( tmp_class_creation_9__prepared );
                tmp_type_arg_18 = tmp_class_creation_9__prepared;
                tmp_source_name_36 = BUILTIN_TYPE1( tmp_type_arg_18 );
                assert( !(tmp_source_name_36 == NULL) );
                tmp_tuple_element_35 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_36 );
                if ( tmp_tuple_element_35 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_9 );

                    exception_lineno = 138;

                    goto try_except_handler_28;
                }
                PyTuple_SET_ITEM( tmp_right_name_9, 1, tmp_tuple_element_35 );
                tmp_raise_value_9 = BINARY_OPERATION_REMAINDER( tmp_left_name_9, tmp_right_name_9 );
                Py_DECREF( tmp_right_name_9 );
                if ( tmp_raise_value_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;

                    goto try_except_handler_28;
                }
                exception_type = tmp_raise_type_9;
                Py_INCREF( tmp_raise_type_9 );
                exception_value = tmp_raise_value_9;
                exception_lineno = 138;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_28;
            }
            branch_no_35:;
        }
        goto branch_end_34;
        branch_no_34:;
        {
            PyObject *tmp_assign_source_99;
            tmp_assign_source_99 = PyDict_New();
            assert( tmp_class_creation_9__prepared == NULL );
            tmp_class_creation_9__prepared = tmp_assign_source_99;
        }
        branch_end_34:;
    }
    {
        PyObject *tmp_assign_source_100;
        {
            PyObject *tmp_set_locals_9;
            CHECK_OBJECT( tmp_class_creation_9__prepared );
            tmp_set_locals_9 = tmp_class_creation_9__prepared;
            locals_extractors_138 = tmp_set_locals_9;
            Py_INCREF( tmp_set_locals_9 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_138, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_30;
        }
        tmp_dictset_value = const_str_plain_MoneyRangeExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_138, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_30;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_a41ed17644233d58ca4331a71292e9ee_10, codeobj_a41ed17644233d58ca4331a71292e9ee, module_extractors, sizeof(void *) );
        frame_a41ed17644233d58ca4331a71292e9ee_10 = cache_frame_a41ed17644233d58ca4331a71292e9ee_10;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_a41ed17644233d58ca4331a71292e9ee_10 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_a41ed17644233d58ca4331a71292e9ee_10 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_17___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_138, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_2 = "o";
            goto frame_exception_exit_10;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a41ed17644233d58ca4331a71292e9ee_10 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_9;

        frame_exception_exit_10:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_a41ed17644233d58ca4331a71292e9ee_10 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_a41ed17644233d58ca4331a71292e9ee_10, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_a41ed17644233d58ca4331a71292e9ee_10->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_a41ed17644233d58ca4331a71292e9ee_10, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_a41ed17644233d58ca4331a71292e9ee_10,
            type_description_2,
            outline_8_var___class__
        );


        // Release cached frame.
        if ( frame_a41ed17644233d58ca4331a71292e9ee_10 == cache_frame_a41ed17644233d58ca4331a71292e9ee_10 )
        {
            Py_DECREF( frame_a41ed17644233d58ca4331a71292e9ee_10 );
        }
        cache_frame_a41ed17644233d58ca4331a71292e9ee_10 = NULL;

        assertFrameObject( frame_a41ed17644233d58ca4331a71292e9ee_10 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_9;

        frame_no_exception_9:;
        goto skip_nested_handling_9;
        nested_frame_exit_9:;

        goto try_except_handler_30;
        skip_nested_handling_9:;
        {
            nuitka_bool tmp_condition_result_55;
            PyObject *tmp_compexpr_left_9;
            PyObject *tmp_compexpr_right_9;
            CHECK_OBJECT( tmp_class_creation_9__bases );
            tmp_compexpr_left_9 = tmp_class_creation_9__bases;
            CHECK_OBJECT( tmp_class_creation_9__bases_orig );
            tmp_compexpr_right_9 = tmp_class_creation_9__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_30;
            }
            tmp_condition_result_55 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_55 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_36;
            }
            else
            {
                goto branch_no_36;
            }
            branch_yes_36:;
            CHECK_OBJECT( tmp_class_creation_9__bases_orig );
            tmp_dictset_value = tmp_class_creation_9__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_138, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_30;
            }
            branch_no_36:;
        }
        {
            PyObject *tmp_assign_source_101;
            PyObject *tmp_called_name_20;
            PyObject *tmp_args_name_18;
            PyObject *tmp_tuple_element_36;
            PyObject *tmp_kw_name_18;
            CHECK_OBJECT( tmp_class_creation_9__metaclass );
            tmp_called_name_20 = tmp_class_creation_9__metaclass;
            tmp_tuple_element_36 = const_str_plain_MoneyRangeExtractor;
            tmp_args_name_18 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_18, 0, tmp_tuple_element_36 );
            CHECK_OBJECT( tmp_class_creation_9__bases );
            tmp_tuple_element_36 = tmp_class_creation_9__bases;
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_18, 1, tmp_tuple_element_36 );
            tmp_tuple_element_36 = locals_extractors_138;
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_18, 2, tmp_tuple_element_36 );
            CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
            tmp_kw_name_18 = tmp_class_creation_9__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 138;
            tmp_assign_source_101 = CALL_FUNCTION( tmp_called_name_20, tmp_args_name_18, tmp_kw_name_18 );
            Py_DECREF( tmp_args_name_18 );
            if ( tmp_assign_source_101 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_30;
            }
            assert( outline_8_var___class__ == NULL );
            outline_8_var___class__ = tmp_assign_source_101;
        }
        CHECK_OBJECT( outline_8_var___class__ );
        tmp_assign_source_100 = outline_8_var___class__;
        Py_INCREF( tmp_assign_source_100 );
        goto try_return_handler_30;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_30:;
        Py_DECREF( locals_extractors_138 );
        locals_extractors_138 = NULL;
        goto try_return_handler_29;
        // Exception handler code:
        try_except_handler_30:;
        exception_keeper_type_28 = exception_type;
        exception_keeper_value_28 = exception_value;
        exception_keeper_tb_28 = exception_tb;
        exception_keeper_lineno_28 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_138 );
        locals_extractors_138 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_28;
        exception_value = exception_keeper_value_28;
        exception_tb = exception_keeper_tb_28;
        exception_lineno = exception_keeper_lineno_28;

        goto try_except_handler_29;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_29:;
        CHECK_OBJECT( (PyObject *)outline_8_var___class__ );
        Py_DECREF( outline_8_var___class__ );
        outline_8_var___class__ = NULL;

        goto outline_result_9;
        // Exception handler code:
        try_except_handler_29:;
        exception_keeper_type_29 = exception_type;
        exception_keeper_value_29 = exception_value;
        exception_keeper_tb_29 = exception_tb;
        exception_keeper_lineno_29 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_29;
        exception_value = exception_keeper_value_29;
        exception_tb = exception_keeper_tb_29;
        exception_lineno = exception_keeper_lineno_29;

        goto outline_exception_9;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_9:;
        exception_lineno = 138;
        goto try_except_handler_28;
        outline_result_9:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_MoneyRangeExtractor, tmp_assign_source_100 );
    }
    goto try_end_12;
    // Exception handler code:
    try_except_handler_28:;
    exception_keeper_type_30 = exception_type;
    exception_keeper_value_30 = exception_value;
    exception_keeper_tb_30 = exception_tb;
    exception_keeper_lineno_30 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_9__bases_orig );
    tmp_class_creation_9__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_9__bases );
    tmp_class_creation_9__bases = NULL;

    Py_XDECREF( tmp_class_creation_9__class_decl_dict );
    tmp_class_creation_9__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_9__metaclass );
    tmp_class_creation_9__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_9__prepared );
    tmp_class_creation_9__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_30;
    exception_value = exception_keeper_value_30;
    exception_tb = exception_keeper_tb_30;
    exception_lineno = exception_keeper_lineno_30;

    goto frame_exception_exit_1;
    // End of try:
    try_end_12:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__bases_orig );
    Py_DECREF( tmp_class_creation_9__bases_orig );
    tmp_class_creation_9__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__bases );
    Py_DECREF( tmp_class_creation_9__bases );
    tmp_class_creation_9__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__class_decl_dict );
    Py_DECREF( tmp_class_creation_9__class_decl_dict );
    tmp_class_creation_9__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__metaclass );
    Py_DECREF( tmp_class_creation_9__metaclass );
    tmp_class_creation_9__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__prepared );
    Py_DECREF( tmp_class_creation_9__prepared );
    tmp_class_creation_9__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_102;
        PyObject *tmp_tuple_element_37;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;

            goto try_except_handler_31;
        }

        tmp_tuple_element_37 = tmp_mvar_value_12;
        tmp_assign_source_102 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_37 );
        PyTuple_SET_ITEM( tmp_assign_source_102, 0, tmp_tuple_element_37 );
        assert( tmp_class_creation_10__bases_orig == NULL );
        tmp_class_creation_10__bases_orig = tmp_assign_source_102;
    }
    {
        PyObject *tmp_assign_source_103;
        PyObject *tmp_dircall_arg1_10;
        CHECK_OBJECT( tmp_class_creation_10__bases_orig );
        tmp_dircall_arg1_10 = tmp_class_creation_10__bases_orig;
        Py_INCREF( tmp_dircall_arg1_10 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_10};
            tmp_assign_source_103 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_103 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        assert( tmp_class_creation_10__bases == NULL );
        tmp_class_creation_10__bases = tmp_assign_source_103;
    }
    {
        PyObject *tmp_assign_source_104;
        tmp_assign_source_104 = PyDict_New();
        assert( tmp_class_creation_10__class_decl_dict == NULL );
        tmp_class_creation_10__class_decl_dict = tmp_assign_source_104;
    }
    {
        PyObject *tmp_assign_source_105;
        PyObject *tmp_metaclass_name_10;
        nuitka_bool tmp_condition_result_56;
        PyObject *tmp_key_name_28;
        PyObject *tmp_dict_name_28;
        PyObject *tmp_dict_name_29;
        PyObject *tmp_key_name_29;
        nuitka_bool tmp_condition_result_57;
        int tmp_truth_name_10;
        PyObject *tmp_type_arg_19;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_subscript_name_10;
        PyObject *tmp_bases_name_10;
        tmp_key_name_28 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dict_name_28 = tmp_class_creation_10__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_28, tmp_key_name_28 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        tmp_condition_result_56 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_56 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_20;
        }
        else
        {
            goto condexpr_false_20;
        }
        condexpr_true_20:;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dict_name_29 = tmp_class_creation_10__class_decl_dict;
        tmp_key_name_29 = const_str_plain_metaclass;
        tmp_metaclass_name_10 = DICT_GET_ITEM( tmp_dict_name_29, tmp_key_name_29 );
        if ( tmp_metaclass_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        goto condexpr_end_20;
        condexpr_false_20:;
        CHECK_OBJECT( tmp_class_creation_10__bases );
        tmp_truth_name_10 = CHECK_IF_TRUE( tmp_class_creation_10__bases );
        if ( tmp_truth_name_10 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        tmp_condition_result_57 = tmp_truth_name_10 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_57 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_21;
        }
        else
        {
            goto condexpr_false_21;
        }
        condexpr_true_21:;
        CHECK_OBJECT( tmp_class_creation_10__bases );
        tmp_subscribed_name_10 = tmp_class_creation_10__bases;
        tmp_subscript_name_10 = const_int_0;
        tmp_type_arg_19 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_10, tmp_subscript_name_10, 0 );
        if ( tmp_type_arg_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        tmp_metaclass_name_10 = BUILTIN_TYPE1( tmp_type_arg_19 );
        Py_DECREF( tmp_type_arg_19 );
        if ( tmp_metaclass_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        goto condexpr_end_21;
        condexpr_false_21:;
        tmp_metaclass_name_10 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_10 );
        condexpr_end_21:;
        condexpr_end_20:;
        CHECK_OBJECT( tmp_class_creation_10__bases );
        tmp_bases_name_10 = tmp_class_creation_10__bases;
        tmp_assign_source_105 = SELECT_METACLASS( tmp_metaclass_name_10, tmp_bases_name_10 );
        Py_DECREF( tmp_metaclass_name_10 );
        if ( tmp_assign_source_105 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        assert( tmp_class_creation_10__metaclass == NULL );
        tmp_class_creation_10__metaclass = tmp_assign_source_105;
    }
    {
        nuitka_bool tmp_condition_result_58;
        PyObject *tmp_key_name_30;
        PyObject *tmp_dict_name_30;
        tmp_key_name_30 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dict_name_30 = tmp_class_creation_10__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_30, tmp_key_name_30 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        tmp_condition_result_58 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_58 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_37;
        }
        else
        {
            goto branch_no_37;
        }
        branch_yes_37:;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_10__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_31;
        }
        branch_no_37:;
    }
    {
        nuitka_bool tmp_condition_result_59;
        PyObject *tmp_source_name_37;
        CHECK_OBJECT( tmp_class_creation_10__metaclass );
        tmp_source_name_37 = tmp_class_creation_10__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_37, const_str_plain___prepare__ );
        tmp_condition_result_59 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_59 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_38;
        }
        else
        {
            goto branch_no_38;
        }
        branch_yes_38:;
        {
            PyObject *tmp_assign_source_106;
            PyObject *tmp_called_name_21;
            PyObject *tmp_source_name_38;
            PyObject *tmp_args_name_19;
            PyObject *tmp_tuple_element_38;
            PyObject *tmp_kw_name_19;
            CHECK_OBJECT( tmp_class_creation_10__metaclass );
            tmp_source_name_38 = tmp_class_creation_10__metaclass;
            tmp_called_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain___prepare__ );
            if ( tmp_called_name_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;

                goto try_except_handler_31;
            }
            tmp_tuple_element_38 = const_str_plain_AddressExtractor;
            tmp_args_name_19 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_38 );
            PyTuple_SET_ITEM( tmp_args_name_19, 0, tmp_tuple_element_38 );
            CHECK_OBJECT( tmp_class_creation_10__bases );
            tmp_tuple_element_38 = tmp_class_creation_10__bases;
            Py_INCREF( tmp_tuple_element_38 );
            PyTuple_SET_ITEM( tmp_args_name_19, 1, tmp_tuple_element_38 );
            CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
            tmp_kw_name_19 = tmp_class_creation_10__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 143;
            tmp_assign_source_106 = CALL_FUNCTION( tmp_called_name_21, tmp_args_name_19, tmp_kw_name_19 );
            Py_DECREF( tmp_called_name_21 );
            Py_DECREF( tmp_args_name_19 );
            if ( tmp_assign_source_106 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;

                goto try_except_handler_31;
            }
            assert( tmp_class_creation_10__prepared == NULL );
            tmp_class_creation_10__prepared = tmp_assign_source_106;
        }
        {
            nuitka_bool tmp_condition_result_60;
            PyObject *tmp_operand_name_10;
            PyObject *tmp_source_name_39;
            CHECK_OBJECT( tmp_class_creation_10__prepared );
            tmp_source_name_39 = tmp_class_creation_10__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_39, const_str_plain___getitem__ );
            tmp_operand_name_10 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;

                goto try_except_handler_31;
            }
            tmp_condition_result_60 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_60 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_39;
            }
            else
            {
                goto branch_no_39;
            }
            branch_yes_39:;
            {
                PyObject *tmp_raise_type_10;
                PyObject *tmp_raise_value_10;
                PyObject *tmp_left_name_10;
                PyObject *tmp_right_name_10;
                PyObject *tmp_tuple_element_39;
                PyObject *tmp_getattr_target_10;
                PyObject *tmp_getattr_attr_10;
                PyObject *tmp_getattr_default_10;
                PyObject *tmp_source_name_40;
                PyObject *tmp_type_arg_20;
                tmp_raise_type_10 = PyExc_TypeError;
                tmp_left_name_10 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_10__metaclass );
                tmp_getattr_target_10 = tmp_class_creation_10__metaclass;
                tmp_getattr_attr_10 = const_str_plain___name__;
                tmp_getattr_default_10 = const_str_angle_metaclass;
                tmp_tuple_element_39 = BUILTIN_GETATTR( tmp_getattr_target_10, tmp_getattr_attr_10, tmp_getattr_default_10 );
                if ( tmp_tuple_element_39 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 143;

                    goto try_except_handler_31;
                }
                tmp_right_name_10 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_10, 0, tmp_tuple_element_39 );
                CHECK_OBJECT( tmp_class_creation_10__prepared );
                tmp_type_arg_20 = tmp_class_creation_10__prepared;
                tmp_source_name_40 = BUILTIN_TYPE1( tmp_type_arg_20 );
                assert( !(tmp_source_name_40 == NULL) );
                tmp_tuple_element_39 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_40 );
                if ( tmp_tuple_element_39 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_10 );

                    exception_lineno = 143;

                    goto try_except_handler_31;
                }
                PyTuple_SET_ITEM( tmp_right_name_10, 1, tmp_tuple_element_39 );
                tmp_raise_value_10 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
                Py_DECREF( tmp_right_name_10 );
                if ( tmp_raise_value_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 143;

                    goto try_except_handler_31;
                }
                exception_type = tmp_raise_type_10;
                Py_INCREF( tmp_raise_type_10 );
                exception_value = tmp_raise_value_10;
                exception_lineno = 143;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_31;
            }
            branch_no_39:;
        }
        goto branch_end_38;
        branch_no_38:;
        {
            PyObject *tmp_assign_source_107;
            tmp_assign_source_107 = PyDict_New();
            assert( tmp_class_creation_10__prepared == NULL );
            tmp_class_creation_10__prepared = tmp_assign_source_107;
        }
        branch_end_38:;
    }
    {
        PyObject *tmp_assign_source_108;
        {
            PyObject *tmp_set_locals_10;
            CHECK_OBJECT( tmp_class_creation_10__prepared );
            tmp_set_locals_10 = tmp_class_creation_10__prepared;
            locals_extractors_143 = tmp_set_locals_10;
            Py_INCREF( tmp_set_locals_10 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_143, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_33;
        }
        tmp_dictset_value = const_str_plain_AddressExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_143, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;

            goto try_except_handler_33;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_67f4e11334b14bcc38f1503fd88ebbe9_11, codeobj_67f4e11334b14bcc38f1503fd88ebbe9, module_extractors, sizeof(void *) );
        frame_67f4e11334b14bcc38f1503fd88ebbe9_11 = cache_frame_67f4e11334b14bcc38f1503fd88ebbe9_11;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_67f4e11334b14bcc38f1503fd88ebbe9_11 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_67f4e11334b14bcc38f1503fd88ebbe9_11 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_18___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_143, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_2 = "o";
            goto frame_exception_exit_11;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_67f4e11334b14bcc38f1503fd88ebbe9_11 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_10;

        frame_exception_exit_11:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_67f4e11334b14bcc38f1503fd88ebbe9_11 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_67f4e11334b14bcc38f1503fd88ebbe9_11, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_67f4e11334b14bcc38f1503fd88ebbe9_11->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_67f4e11334b14bcc38f1503fd88ebbe9_11, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_67f4e11334b14bcc38f1503fd88ebbe9_11,
            type_description_2,
            outline_9_var___class__
        );


        // Release cached frame.
        if ( frame_67f4e11334b14bcc38f1503fd88ebbe9_11 == cache_frame_67f4e11334b14bcc38f1503fd88ebbe9_11 )
        {
            Py_DECREF( frame_67f4e11334b14bcc38f1503fd88ebbe9_11 );
        }
        cache_frame_67f4e11334b14bcc38f1503fd88ebbe9_11 = NULL;

        assertFrameObject( frame_67f4e11334b14bcc38f1503fd88ebbe9_11 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_10;

        frame_no_exception_10:;
        goto skip_nested_handling_10;
        nested_frame_exit_10:;

        goto try_except_handler_33;
        skip_nested_handling_10:;
        {
            nuitka_bool tmp_condition_result_61;
            PyObject *tmp_compexpr_left_10;
            PyObject *tmp_compexpr_right_10;
            CHECK_OBJECT( tmp_class_creation_10__bases );
            tmp_compexpr_left_10 = tmp_class_creation_10__bases;
            CHECK_OBJECT( tmp_class_creation_10__bases_orig );
            tmp_compexpr_right_10 = tmp_class_creation_10__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;

                goto try_except_handler_33;
            }
            tmp_condition_result_61 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_61 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_40;
            }
            else
            {
                goto branch_no_40;
            }
            branch_yes_40:;
            CHECK_OBJECT( tmp_class_creation_10__bases_orig );
            tmp_dictset_value = tmp_class_creation_10__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_143, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;

                goto try_except_handler_33;
            }
            branch_no_40:;
        }
        {
            PyObject *tmp_assign_source_109;
            PyObject *tmp_called_name_22;
            PyObject *tmp_args_name_20;
            PyObject *tmp_tuple_element_40;
            PyObject *tmp_kw_name_20;
            CHECK_OBJECT( tmp_class_creation_10__metaclass );
            tmp_called_name_22 = tmp_class_creation_10__metaclass;
            tmp_tuple_element_40 = const_str_plain_AddressExtractor;
            tmp_args_name_20 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_20, 0, tmp_tuple_element_40 );
            CHECK_OBJECT( tmp_class_creation_10__bases );
            tmp_tuple_element_40 = tmp_class_creation_10__bases;
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_20, 1, tmp_tuple_element_40 );
            tmp_tuple_element_40 = locals_extractors_143;
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_20, 2, tmp_tuple_element_40 );
            CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
            tmp_kw_name_20 = tmp_class_creation_10__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 143;
            tmp_assign_source_109 = CALL_FUNCTION( tmp_called_name_22, tmp_args_name_20, tmp_kw_name_20 );
            Py_DECREF( tmp_args_name_20 );
            if ( tmp_assign_source_109 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;

                goto try_except_handler_33;
            }
            assert( outline_9_var___class__ == NULL );
            outline_9_var___class__ = tmp_assign_source_109;
        }
        CHECK_OBJECT( outline_9_var___class__ );
        tmp_assign_source_108 = outline_9_var___class__;
        Py_INCREF( tmp_assign_source_108 );
        goto try_return_handler_33;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_33:;
        Py_DECREF( locals_extractors_143 );
        locals_extractors_143 = NULL;
        goto try_return_handler_32;
        // Exception handler code:
        try_except_handler_33:;
        exception_keeper_type_31 = exception_type;
        exception_keeper_value_31 = exception_value;
        exception_keeper_tb_31 = exception_tb;
        exception_keeper_lineno_31 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_143 );
        locals_extractors_143 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_31;
        exception_value = exception_keeper_value_31;
        exception_tb = exception_keeper_tb_31;
        exception_lineno = exception_keeper_lineno_31;

        goto try_except_handler_32;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_32:;
        CHECK_OBJECT( (PyObject *)outline_9_var___class__ );
        Py_DECREF( outline_9_var___class__ );
        outline_9_var___class__ = NULL;

        goto outline_result_10;
        // Exception handler code:
        try_except_handler_32:;
        exception_keeper_type_32 = exception_type;
        exception_keeper_value_32 = exception_value;
        exception_keeper_tb_32 = exception_tb;
        exception_keeper_lineno_32 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_32;
        exception_value = exception_keeper_value_32;
        exception_tb = exception_keeper_tb_32;
        exception_lineno = exception_keeper_lineno_32;

        goto outline_exception_10;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_10:;
        exception_lineno = 143;
        goto try_except_handler_31;
        outline_result_10:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_AddressExtractor, tmp_assign_source_108 );
    }
    goto try_end_13;
    // Exception handler code:
    try_except_handler_31:;
    exception_keeper_type_33 = exception_type;
    exception_keeper_value_33 = exception_value;
    exception_keeper_tb_33 = exception_tb;
    exception_keeper_lineno_33 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_10__bases_orig );
    tmp_class_creation_10__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_10__bases );
    tmp_class_creation_10__bases = NULL;

    Py_XDECREF( tmp_class_creation_10__class_decl_dict );
    tmp_class_creation_10__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_10__metaclass );
    tmp_class_creation_10__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_10__prepared );
    tmp_class_creation_10__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_33;
    exception_value = exception_keeper_value_33;
    exception_tb = exception_keeper_tb_33;
    exception_lineno = exception_keeper_lineno_33;

    goto frame_exception_exit_1;
    // End of try:
    try_end_13:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__bases_orig );
    Py_DECREF( tmp_class_creation_10__bases_orig );
    tmp_class_creation_10__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__bases );
    Py_DECREF( tmp_class_creation_10__bases );
    tmp_class_creation_10__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__class_decl_dict );
    Py_DECREF( tmp_class_creation_10__class_decl_dict );
    tmp_class_creation_10__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__metaclass );
    Py_DECREF( tmp_class_creation_10__metaclass );
    tmp_class_creation_10__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__prepared );
    Py_DECREF( tmp_class_creation_10__prepared );
    tmp_class_creation_10__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_110;
        PyObject *tmp_tuple_element_41;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;

            goto try_except_handler_34;
        }

        tmp_tuple_element_41 = tmp_mvar_value_13;
        tmp_assign_source_110 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_41 );
        PyTuple_SET_ITEM( tmp_assign_source_110, 0, tmp_tuple_element_41 );
        assert( tmp_class_creation_11__bases_orig == NULL );
        tmp_class_creation_11__bases_orig = tmp_assign_source_110;
    }
    {
        PyObject *tmp_assign_source_111;
        PyObject *tmp_dircall_arg1_11;
        CHECK_OBJECT( tmp_class_creation_11__bases_orig );
        tmp_dircall_arg1_11 = tmp_class_creation_11__bases_orig;
        Py_INCREF( tmp_dircall_arg1_11 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_11};
            tmp_assign_source_111 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_111 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        assert( tmp_class_creation_11__bases == NULL );
        tmp_class_creation_11__bases = tmp_assign_source_111;
    }
    {
        PyObject *tmp_assign_source_112;
        tmp_assign_source_112 = PyDict_New();
        assert( tmp_class_creation_11__class_decl_dict == NULL );
        tmp_class_creation_11__class_decl_dict = tmp_assign_source_112;
    }
    {
        PyObject *tmp_assign_source_113;
        PyObject *tmp_metaclass_name_11;
        nuitka_bool tmp_condition_result_62;
        PyObject *tmp_key_name_31;
        PyObject *tmp_dict_name_31;
        PyObject *tmp_dict_name_32;
        PyObject *tmp_key_name_32;
        nuitka_bool tmp_condition_result_63;
        int tmp_truth_name_11;
        PyObject *tmp_type_arg_21;
        PyObject *tmp_subscribed_name_11;
        PyObject *tmp_subscript_name_11;
        PyObject *tmp_bases_name_11;
        tmp_key_name_31 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dict_name_31 = tmp_class_creation_11__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_31, tmp_key_name_31 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        tmp_condition_result_62 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_62 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_22;
        }
        else
        {
            goto condexpr_false_22;
        }
        condexpr_true_22:;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dict_name_32 = tmp_class_creation_11__class_decl_dict;
        tmp_key_name_32 = const_str_plain_metaclass;
        tmp_metaclass_name_11 = DICT_GET_ITEM( tmp_dict_name_32, tmp_key_name_32 );
        if ( tmp_metaclass_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        goto condexpr_end_22;
        condexpr_false_22:;
        CHECK_OBJECT( tmp_class_creation_11__bases );
        tmp_truth_name_11 = CHECK_IF_TRUE( tmp_class_creation_11__bases );
        if ( tmp_truth_name_11 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        tmp_condition_result_63 = tmp_truth_name_11 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_63 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_23;
        }
        else
        {
            goto condexpr_false_23;
        }
        condexpr_true_23:;
        CHECK_OBJECT( tmp_class_creation_11__bases );
        tmp_subscribed_name_11 = tmp_class_creation_11__bases;
        tmp_subscript_name_11 = const_int_0;
        tmp_type_arg_21 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_11, tmp_subscript_name_11, 0 );
        if ( tmp_type_arg_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        tmp_metaclass_name_11 = BUILTIN_TYPE1( tmp_type_arg_21 );
        Py_DECREF( tmp_type_arg_21 );
        if ( tmp_metaclass_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        goto condexpr_end_23;
        condexpr_false_23:;
        tmp_metaclass_name_11 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_11 );
        condexpr_end_23:;
        condexpr_end_22:;
        CHECK_OBJECT( tmp_class_creation_11__bases );
        tmp_bases_name_11 = tmp_class_creation_11__bases;
        tmp_assign_source_113 = SELECT_METACLASS( tmp_metaclass_name_11, tmp_bases_name_11 );
        Py_DECREF( tmp_metaclass_name_11 );
        if ( tmp_assign_source_113 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        assert( tmp_class_creation_11__metaclass == NULL );
        tmp_class_creation_11__metaclass = tmp_assign_source_113;
    }
    {
        nuitka_bool tmp_condition_result_64;
        PyObject *tmp_key_name_33;
        PyObject *tmp_dict_name_33;
        tmp_key_name_33 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dict_name_33 = tmp_class_creation_11__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_33, tmp_key_name_33 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        tmp_condition_result_64 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_64 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_41;
        }
        else
        {
            goto branch_no_41;
        }
        branch_yes_41:;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_11__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_34;
        }
        branch_no_41:;
    }
    {
        nuitka_bool tmp_condition_result_65;
        PyObject *tmp_source_name_41;
        CHECK_OBJECT( tmp_class_creation_11__metaclass );
        tmp_source_name_41 = tmp_class_creation_11__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_41, const_str_plain___prepare__ );
        tmp_condition_result_65 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_65 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_42;
        }
        else
        {
            goto branch_no_42;
        }
        branch_yes_42:;
        {
            PyObject *tmp_assign_source_114;
            PyObject *tmp_called_name_23;
            PyObject *tmp_source_name_42;
            PyObject *tmp_args_name_21;
            PyObject *tmp_tuple_element_42;
            PyObject *tmp_kw_name_21;
            CHECK_OBJECT( tmp_class_creation_11__metaclass );
            tmp_source_name_42 = tmp_class_creation_11__metaclass;
            tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain___prepare__ );
            if ( tmp_called_name_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;

                goto try_except_handler_34;
            }
            tmp_tuple_element_42 = const_str_plain_LocationExtractor;
            tmp_args_name_21 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_42 );
            PyTuple_SET_ITEM( tmp_args_name_21, 0, tmp_tuple_element_42 );
            CHECK_OBJECT( tmp_class_creation_11__bases );
            tmp_tuple_element_42 = tmp_class_creation_11__bases;
            Py_INCREF( tmp_tuple_element_42 );
            PyTuple_SET_ITEM( tmp_args_name_21, 1, tmp_tuple_element_42 );
            CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
            tmp_kw_name_21 = tmp_class_creation_11__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 155;
            tmp_assign_source_114 = CALL_FUNCTION( tmp_called_name_23, tmp_args_name_21, tmp_kw_name_21 );
            Py_DECREF( tmp_called_name_23 );
            Py_DECREF( tmp_args_name_21 );
            if ( tmp_assign_source_114 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;

                goto try_except_handler_34;
            }
            assert( tmp_class_creation_11__prepared == NULL );
            tmp_class_creation_11__prepared = tmp_assign_source_114;
        }
        {
            nuitka_bool tmp_condition_result_66;
            PyObject *tmp_operand_name_11;
            PyObject *tmp_source_name_43;
            CHECK_OBJECT( tmp_class_creation_11__prepared );
            tmp_source_name_43 = tmp_class_creation_11__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_43, const_str_plain___getitem__ );
            tmp_operand_name_11 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;

                goto try_except_handler_34;
            }
            tmp_condition_result_66 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_66 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_43;
            }
            else
            {
                goto branch_no_43;
            }
            branch_yes_43:;
            {
                PyObject *tmp_raise_type_11;
                PyObject *tmp_raise_value_11;
                PyObject *tmp_left_name_11;
                PyObject *tmp_right_name_11;
                PyObject *tmp_tuple_element_43;
                PyObject *tmp_getattr_target_11;
                PyObject *tmp_getattr_attr_11;
                PyObject *tmp_getattr_default_11;
                PyObject *tmp_source_name_44;
                PyObject *tmp_type_arg_22;
                tmp_raise_type_11 = PyExc_TypeError;
                tmp_left_name_11 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_11__metaclass );
                tmp_getattr_target_11 = tmp_class_creation_11__metaclass;
                tmp_getattr_attr_11 = const_str_plain___name__;
                tmp_getattr_default_11 = const_str_angle_metaclass;
                tmp_tuple_element_43 = BUILTIN_GETATTR( tmp_getattr_target_11, tmp_getattr_attr_11, tmp_getattr_default_11 );
                if ( tmp_tuple_element_43 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 155;

                    goto try_except_handler_34;
                }
                tmp_right_name_11 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_11, 0, tmp_tuple_element_43 );
                CHECK_OBJECT( tmp_class_creation_11__prepared );
                tmp_type_arg_22 = tmp_class_creation_11__prepared;
                tmp_source_name_44 = BUILTIN_TYPE1( tmp_type_arg_22 );
                assert( !(tmp_source_name_44 == NULL) );
                tmp_tuple_element_43 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_44 );
                if ( tmp_tuple_element_43 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_11 );

                    exception_lineno = 155;

                    goto try_except_handler_34;
                }
                PyTuple_SET_ITEM( tmp_right_name_11, 1, tmp_tuple_element_43 );
                tmp_raise_value_11 = BINARY_OPERATION_REMAINDER( tmp_left_name_11, tmp_right_name_11 );
                Py_DECREF( tmp_right_name_11 );
                if ( tmp_raise_value_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 155;

                    goto try_except_handler_34;
                }
                exception_type = tmp_raise_type_11;
                Py_INCREF( tmp_raise_type_11 );
                exception_value = tmp_raise_value_11;
                exception_lineno = 155;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_34;
            }
            branch_no_43:;
        }
        goto branch_end_42;
        branch_no_42:;
        {
            PyObject *tmp_assign_source_115;
            tmp_assign_source_115 = PyDict_New();
            assert( tmp_class_creation_11__prepared == NULL );
            tmp_class_creation_11__prepared = tmp_assign_source_115;
        }
        branch_end_42:;
    }
    {
        PyObject *tmp_assign_source_116;
        {
            PyObject *tmp_set_locals_11;
            CHECK_OBJECT( tmp_class_creation_11__prepared );
            tmp_set_locals_11 = tmp_class_creation_11__prepared;
            locals_extractors_155 = tmp_set_locals_11;
            Py_INCREF( tmp_set_locals_11 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_155, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_36;
        }
        tmp_dictset_value = const_str_plain_LocationExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_155, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto try_except_handler_36;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_234de8d20548cbb66367c1849eae78b7_12, codeobj_234de8d20548cbb66367c1849eae78b7, module_extractors, sizeof(void *) );
        frame_234de8d20548cbb66367c1849eae78b7_12 = cache_frame_234de8d20548cbb66367c1849eae78b7_12;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_234de8d20548cbb66367c1849eae78b7_12 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_234de8d20548cbb66367c1849eae78b7_12 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_19___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_155, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_2 = "o";
            goto frame_exception_exit_12;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_234de8d20548cbb66367c1849eae78b7_12 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_11;

        frame_exception_exit_12:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_234de8d20548cbb66367c1849eae78b7_12 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_234de8d20548cbb66367c1849eae78b7_12, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_234de8d20548cbb66367c1849eae78b7_12->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_234de8d20548cbb66367c1849eae78b7_12, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_234de8d20548cbb66367c1849eae78b7_12,
            type_description_2,
            outline_10_var___class__
        );


        // Release cached frame.
        if ( frame_234de8d20548cbb66367c1849eae78b7_12 == cache_frame_234de8d20548cbb66367c1849eae78b7_12 )
        {
            Py_DECREF( frame_234de8d20548cbb66367c1849eae78b7_12 );
        }
        cache_frame_234de8d20548cbb66367c1849eae78b7_12 = NULL;

        assertFrameObject( frame_234de8d20548cbb66367c1849eae78b7_12 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_11;

        frame_no_exception_11:;
        goto skip_nested_handling_11;
        nested_frame_exit_11:;

        goto try_except_handler_36;
        skip_nested_handling_11:;
        {
            nuitka_bool tmp_condition_result_67;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            CHECK_OBJECT( tmp_class_creation_11__bases );
            tmp_compexpr_left_11 = tmp_class_creation_11__bases;
            CHECK_OBJECT( tmp_class_creation_11__bases_orig );
            tmp_compexpr_right_11 = tmp_class_creation_11__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;

                goto try_except_handler_36;
            }
            tmp_condition_result_67 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_67 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_44;
            }
            else
            {
                goto branch_no_44;
            }
            branch_yes_44:;
            CHECK_OBJECT( tmp_class_creation_11__bases_orig );
            tmp_dictset_value = tmp_class_creation_11__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_155, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;

                goto try_except_handler_36;
            }
            branch_no_44:;
        }
        {
            PyObject *tmp_assign_source_117;
            PyObject *tmp_called_name_24;
            PyObject *tmp_args_name_22;
            PyObject *tmp_tuple_element_44;
            PyObject *tmp_kw_name_22;
            CHECK_OBJECT( tmp_class_creation_11__metaclass );
            tmp_called_name_24 = tmp_class_creation_11__metaclass;
            tmp_tuple_element_44 = const_str_plain_LocationExtractor;
            tmp_args_name_22 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_44 );
            PyTuple_SET_ITEM( tmp_args_name_22, 0, tmp_tuple_element_44 );
            CHECK_OBJECT( tmp_class_creation_11__bases );
            tmp_tuple_element_44 = tmp_class_creation_11__bases;
            Py_INCREF( tmp_tuple_element_44 );
            PyTuple_SET_ITEM( tmp_args_name_22, 1, tmp_tuple_element_44 );
            tmp_tuple_element_44 = locals_extractors_155;
            Py_INCREF( tmp_tuple_element_44 );
            PyTuple_SET_ITEM( tmp_args_name_22, 2, tmp_tuple_element_44 );
            CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
            tmp_kw_name_22 = tmp_class_creation_11__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 155;
            tmp_assign_source_117 = CALL_FUNCTION( tmp_called_name_24, tmp_args_name_22, tmp_kw_name_22 );
            Py_DECREF( tmp_args_name_22 );
            if ( tmp_assign_source_117 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;

                goto try_except_handler_36;
            }
            assert( outline_10_var___class__ == NULL );
            outline_10_var___class__ = tmp_assign_source_117;
        }
        CHECK_OBJECT( outline_10_var___class__ );
        tmp_assign_source_116 = outline_10_var___class__;
        Py_INCREF( tmp_assign_source_116 );
        goto try_return_handler_36;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_36:;
        Py_DECREF( locals_extractors_155 );
        locals_extractors_155 = NULL;
        goto try_return_handler_35;
        // Exception handler code:
        try_except_handler_36:;
        exception_keeper_type_34 = exception_type;
        exception_keeper_value_34 = exception_value;
        exception_keeper_tb_34 = exception_tb;
        exception_keeper_lineno_34 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_155 );
        locals_extractors_155 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_34;
        exception_value = exception_keeper_value_34;
        exception_tb = exception_keeper_tb_34;
        exception_lineno = exception_keeper_lineno_34;

        goto try_except_handler_35;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_35:;
        CHECK_OBJECT( (PyObject *)outline_10_var___class__ );
        Py_DECREF( outline_10_var___class__ );
        outline_10_var___class__ = NULL;

        goto outline_result_11;
        // Exception handler code:
        try_except_handler_35:;
        exception_keeper_type_35 = exception_type;
        exception_keeper_value_35 = exception_value;
        exception_keeper_tb_35 = exception_tb;
        exception_keeper_lineno_35 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_35;
        exception_value = exception_keeper_value_35;
        exception_tb = exception_keeper_tb_35;
        exception_lineno = exception_keeper_lineno_35;

        goto outline_exception_11;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_11:;
        exception_lineno = 155;
        goto try_except_handler_34;
        outline_result_11:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_LocationExtractor, tmp_assign_source_116 );
    }
    goto try_end_14;
    // Exception handler code:
    try_except_handler_34:;
    exception_keeper_type_36 = exception_type;
    exception_keeper_value_36 = exception_value;
    exception_keeper_tb_36 = exception_tb;
    exception_keeper_lineno_36 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_11__bases_orig );
    tmp_class_creation_11__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_11__bases );
    tmp_class_creation_11__bases = NULL;

    Py_XDECREF( tmp_class_creation_11__class_decl_dict );
    tmp_class_creation_11__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_11__metaclass );
    tmp_class_creation_11__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_11__prepared );
    tmp_class_creation_11__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_36;
    exception_value = exception_keeper_value_36;
    exception_tb = exception_keeper_tb_36;
    exception_lineno = exception_keeper_lineno_36;

    goto frame_exception_exit_1;
    // End of try:
    try_end_14:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__bases_orig );
    Py_DECREF( tmp_class_creation_11__bases_orig );
    tmp_class_creation_11__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__bases );
    Py_DECREF( tmp_class_creation_11__bases );
    tmp_class_creation_11__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__class_decl_dict );
    Py_DECREF( tmp_class_creation_11__class_decl_dict );
    tmp_class_creation_11__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__metaclass );
    Py_DECREF( tmp_class_creation_11__metaclass );
    tmp_class_creation_11__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__prepared );
    Py_DECREF( tmp_class_creation_11__prepared );
    tmp_class_creation_11__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_118;
        PyObject *tmp_tuple_element_45;
        PyObject *tmp_mvar_value_14;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_Extractor );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Extractor );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Extractor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto try_except_handler_37;
        }

        tmp_tuple_element_45 = tmp_mvar_value_14;
        tmp_assign_source_118 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_45 );
        PyTuple_SET_ITEM( tmp_assign_source_118, 0, tmp_tuple_element_45 );
        assert( tmp_class_creation_12__bases_orig == NULL );
        tmp_class_creation_12__bases_orig = tmp_assign_source_118;
    }
    {
        PyObject *tmp_assign_source_119;
        PyObject *tmp_dircall_arg1_12;
        CHECK_OBJECT( tmp_class_creation_12__bases_orig );
        tmp_dircall_arg1_12 = tmp_class_creation_12__bases_orig;
        Py_INCREF( tmp_dircall_arg1_12 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_12};
            tmp_assign_source_119 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_119 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        assert( tmp_class_creation_12__bases == NULL );
        tmp_class_creation_12__bases = tmp_assign_source_119;
    }
    {
        PyObject *tmp_assign_source_120;
        tmp_assign_source_120 = PyDict_New();
        assert( tmp_class_creation_12__class_decl_dict == NULL );
        tmp_class_creation_12__class_decl_dict = tmp_assign_source_120;
    }
    {
        PyObject *tmp_assign_source_121;
        PyObject *tmp_metaclass_name_12;
        nuitka_bool tmp_condition_result_68;
        PyObject *tmp_key_name_34;
        PyObject *tmp_dict_name_34;
        PyObject *tmp_dict_name_35;
        PyObject *tmp_key_name_35;
        nuitka_bool tmp_condition_result_69;
        int tmp_truth_name_12;
        PyObject *tmp_type_arg_23;
        PyObject *tmp_subscribed_name_12;
        PyObject *tmp_subscript_name_12;
        PyObject *tmp_bases_name_12;
        tmp_key_name_34 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dict_name_34 = tmp_class_creation_12__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_34, tmp_key_name_34 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        tmp_condition_result_68 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_68 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_24;
        }
        else
        {
            goto condexpr_false_24;
        }
        condexpr_true_24:;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dict_name_35 = tmp_class_creation_12__class_decl_dict;
        tmp_key_name_35 = const_str_plain_metaclass;
        tmp_metaclass_name_12 = DICT_GET_ITEM( tmp_dict_name_35, tmp_key_name_35 );
        if ( tmp_metaclass_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        goto condexpr_end_24;
        condexpr_false_24:;
        CHECK_OBJECT( tmp_class_creation_12__bases );
        tmp_truth_name_12 = CHECK_IF_TRUE( tmp_class_creation_12__bases );
        if ( tmp_truth_name_12 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        tmp_condition_result_69 = tmp_truth_name_12 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_69 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_25;
        }
        else
        {
            goto condexpr_false_25;
        }
        condexpr_true_25:;
        CHECK_OBJECT( tmp_class_creation_12__bases );
        tmp_subscribed_name_12 = tmp_class_creation_12__bases;
        tmp_subscript_name_12 = const_int_0;
        tmp_type_arg_23 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_12, tmp_subscript_name_12, 0 );
        if ( tmp_type_arg_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        tmp_metaclass_name_12 = BUILTIN_TYPE1( tmp_type_arg_23 );
        Py_DECREF( tmp_type_arg_23 );
        if ( tmp_metaclass_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        goto condexpr_end_25;
        condexpr_false_25:;
        tmp_metaclass_name_12 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_12 );
        condexpr_end_25:;
        condexpr_end_24:;
        CHECK_OBJECT( tmp_class_creation_12__bases );
        tmp_bases_name_12 = tmp_class_creation_12__bases;
        tmp_assign_source_121 = SELECT_METACLASS( tmp_metaclass_name_12, tmp_bases_name_12 );
        Py_DECREF( tmp_metaclass_name_12 );
        if ( tmp_assign_source_121 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        assert( tmp_class_creation_12__metaclass == NULL );
        tmp_class_creation_12__metaclass = tmp_assign_source_121;
    }
    {
        nuitka_bool tmp_condition_result_70;
        PyObject *tmp_key_name_36;
        PyObject *tmp_dict_name_36;
        tmp_key_name_36 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dict_name_36 = tmp_class_creation_12__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_36, tmp_key_name_36 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        tmp_condition_result_70 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_70 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_45;
        }
        else
        {
            goto branch_no_45;
        }
        branch_yes_45:;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_12__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_37;
        }
        branch_no_45:;
    }
    {
        nuitka_bool tmp_condition_result_71;
        PyObject *tmp_source_name_45;
        CHECK_OBJECT( tmp_class_creation_12__metaclass );
        tmp_source_name_45 = tmp_class_creation_12__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_45, const_str_plain___prepare__ );
        tmp_condition_result_71 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_71 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_46;
        }
        else
        {
            goto branch_no_46;
        }
        branch_yes_46:;
        {
            PyObject *tmp_assign_source_122;
            PyObject *tmp_called_name_25;
            PyObject *tmp_source_name_46;
            PyObject *tmp_args_name_23;
            PyObject *tmp_tuple_element_46;
            PyObject *tmp_kw_name_23;
            CHECK_OBJECT( tmp_class_creation_12__metaclass );
            tmp_source_name_46 = tmp_class_creation_12__metaclass;
            tmp_called_name_25 = LOOKUP_ATTRIBUTE( tmp_source_name_46, const_str_plain___prepare__ );
            if ( tmp_called_name_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;

                goto try_except_handler_37;
            }
            tmp_tuple_element_46 = const_str_plain_OrganisationExtractor;
            tmp_args_name_23 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_46 );
            PyTuple_SET_ITEM( tmp_args_name_23, 0, tmp_tuple_element_46 );
            CHECK_OBJECT( tmp_class_creation_12__bases );
            tmp_tuple_element_46 = tmp_class_creation_12__bases;
            Py_INCREF( tmp_tuple_element_46 );
            PyTuple_SET_ITEM( tmp_args_name_23, 1, tmp_tuple_element_46 );
            CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
            tmp_kw_name_23 = tmp_class_creation_12__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 160;
            tmp_assign_source_122 = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_23, tmp_kw_name_23 );
            Py_DECREF( tmp_called_name_25 );
            Py_DECREF( tmp_args_name_23 );
            if ( tmp_assign_source_122 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;

                goto try_except_handler_37;
            }
            assert( tmp_class_creation_12__prepared == NULL );
            tmp_class_creation_12__prepared = tmp_assign_source_122;
        }
        {
            nuitka_bool tmp_condition_result_72;
            PyObject *tmp_operand_name_12;
            PyObject *tmp_source_name_47;
            CHECK_OBJECT( tmp_class_creation_12__prepared );
            tmp_source_name_47 = tmp_class_creation_12__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_47, const_str_plain___getitem__ );
            tmp_operand_name_12 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_12 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;

                goto try_except_handler_37;
            }
            tmp_condition_result_72 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_72 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_47;
            }
            else
            {
                goto branch_no_47;
            }
            branch_yes_47:;
            {
                PyObject *tmp_raise_type_12;
                PyObject *tmp_raise_value_12;
                PyObject *tmp_left_name_12;
                PyObject *tmp_right_name_12;
                PyObject *tmp_tuple_element_47;
                PyObject *tmp_getattr_target_12;
                PyObject *tmp_getattr_attr_12;
                PyObject *tmp_getattr_default_12;
                PyObject *tmp_source_name_48;
                PyObject *tmp_type_arg_24;
                tmp_raise_type_12 = PyExc_TypeError;
                tmp_left_name_12 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_12__metaclass );
                tmp_getattr_target_12 = tmp_class_creation_12__metaclass;
                tmp_getattr_attr_12 = const_str_plain___name__;
                tmp_getattr_default_12 = const_str_angle_metaclass;
                tmp_tuple_element_47 = BUILTIN_GETATTR( tmp_getattr_target_12, tmp_getattr_attr_12, tmp_getattr_default_12 );
                if ( tmp_tuple_element_47 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;

                    goto try_except_handler_37;
                }
                tmp_right_name_12 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_12, 0, tmp_tuple_element_47 );
                CHECK_OBJECT( tmp_class_creation_12__prepared );
                tmp_type_arg_24 = tmp_class_creation_12__prepared;
                tmp_source_name_48 = BUILTIN_TYPE1( tmp_type_arg_24 );
                assert( !(tmp_source_name_48 == NULL) );
                tmp_tuple_element_47 = LOOKUP_ATTRIBUTE( tmp_source_name_48, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_48 );
                if ( tmp_tuple_element_47 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_12 );

                    exception_lineno = 160;

                    goto try_except_handler_37;
                }
                PyTuple_SET_ITEM( tmp_right_name_12, 1, tmp_tuple_element_47 );
                tmp_raise_value_12 = BINARY_OPERATION_REMAINDER( tmp_left_name_12, tmp_right_name_12 );
                Py_DECREF( tmp_right_name_12 );
                if ( tmp_raise_value_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;

                    goto try_except_handler_37;
                }
                exception_type = tmp_raise_type_12;
                Py_INCREF( tmp_raise_type_12 );
                exception_value = tmp_raise_value_12;
                exception_lineno = 160;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_37;
            }
            branch_no_47:;
        }
        goto branch_end_46;
        branch_no_46:;
        {
            PyObject *tmp_assign_source_123;
            tmp_assign_source_123 = PyDict_New();
            assert( tmp_class_creation_12__prepared == NULL );
            tmp_class_creation_12__prepared = tmp_assign_source_123;
        }
        branch_end_46:;
    }
    {
        PyObject *tmp_assign_source_124;
        {
            PyObject *tmp_set_locals_12;
            CHECK_OBJECT( tmp_class_creation_12__prepared );
            tmp_set_locals_12 = tmp_class_creation_12__prepared;
            locals_extractors_160 = tmp_set_locals_12;
            Py_INCREF( tmp_set_locals_12 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_plain_extractors;
        tmp_res = PyObject_SetItem( locals_extractors_160, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_39;
        }
        tmp_dictset_value = const_str_plain_OrganisationExtractor;
        tmp_res = PyObject_SetItem( locals_extractors_160, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;

            goto try_except_handler_39;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_4791fd6c754f72a7bb928a049a6ef26d_13, codeobj_4791fd6c754f72a7bb928a049a6ef26d, module_extractors, sizeof(void *) );
        frame_4791fd6c754f72a7bb928a049a6ef26d_13 = cache_frame_4791fd6c754f72a7bb928a049a6ef26d_13;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_4791fd6c754f72a7bb928a049a6ef26d_13 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_4791fd6c754f72a7bb928a049a6ef26d_13 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_extractors$$$function_20___init__(  );



        tmp_res = PyObject_SetItem( locals_extractors_160, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_2 = "o";
            goto frame_exception_exit_13;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_4791fd6c754f72a7bb928a049a6ef26d_13 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_12;

        frame_exception_exit_13:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_4791fd6c754f72a7bb928a049a6ef26d_13 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_4791fd6c754f72a7bb928a049a6ef26d_13, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_4791fd6c754f72a7bb928a049a6ef26d_13->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_4791fd6c754f72a7bb928a049a6ef26d_13, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_4791fd6c754f72a7bb928a049a6ef26d_13,
            type_description_2,
            outline_11_var___class__
        );


        // Release cached frame.
        if ( frame_4791fd6c754f72a7bb928a049a6ef26d_13 == cache_frame_4791fd6c754f72a7bb928a049a6ef26d_13 )
        {
            Py_DECREF( frame_4791fd6c754f72a7bb928a049a6ef26d_13 );
        }
        cache_frame_4791fd6c754f72a7bb928a049a6ef26d_13 = NULL;

        assertFrameObject( frame_4791fd6c754f72a7bb928a049a6ef26d_13 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_12;

        frame_no_exception_12:;
        goto skip_nested_handling_12;
        nested_frame_exit_12:;

        goto try_except_handler_39;
        skip_nested_handling_12:;
        {
            nuitka_bool tmp_condition_result_73;
            PyObject *tmp_compexpr_left_12;
            PyObject *tmp_compexpr_right_12;
            CHECK_OBJECT( tmp_class_creation_12__bases );
            tmp_compexpr_left_12 = tmp_class_creation_12__bases;
            CHECK_OBJECT( tmp_class_creation_12__bases_orig );
            tmp_compexpr_right_12 = tmp_class_creation_12__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;

                goto try_except_handler_39;
            }
            tmp_condition_result_73 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_73 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_48;
            }
            else
            {
                goto branch_no_48;
            }
            branch_yes_48:;
            CHECK_OBJECT( tmp_class_creation_12__bases_orig );
            tmp_dictset_value = tmp_class_creation_12__bases_orig;
            tmp_res = PyObject_SetItem( locals_extractors_160, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;

                goto try_except_handler_39;
            }
            branch_no_48:;
        }
        {
            PyObject *tmp_assign_source_125;
            PyObject *tmp_called_name_26;
            PyObject *tmp_args_name_24;
            PyObject *tmp_tuple_element_48;
            PyObject *tmp_kw_name_24;
            CHECK_OBJECT( tmp_class_creation_12__metaclass );
            tmp_called_name_26 = tmp_class_creation_12__metaclass;
            tmp_tuple_element_48 = const_str_plain_OrganisationExtractor;
            tmp_args_name_24 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_48 );
            PyTuple_SET_ITEM( tmp_args_name_24, 0, tmp_tuple_element_48 );
            CHECK_OBJECT( tmp_class_creation_12__bases );
            tmp_tuple_element_48 = tmp_class_creation_12__bases;
            Py_INCREF( tmp_tuple_element_48 );
            PyTuple_SET_ITEM( tmp_args_name_24, 1, tmp_tuple_element_48 );
            tmp_tuple_element_48 = locals_extractors_160;
            Py_INCREF( tmp_tuple_element_48 );
            PyTuple_SET_ITEM( tmp_args_name_24, 2, tmp_tuple_element_48 );
            CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
            tmp_kw_name_24 = tmp_class_creation_12__class_decl_dict;
            frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame.f_lineno = 160;
            tmp_assign_source_125 = CALL_FUNCTION( tmp_called_name_26, tmp_args_name_24, tmp_kw_name_24 );
            Py_DECREF( tmp_args_name_24 );
            if ( tmp_assign_source_125 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;

                goto try_except_handler_39;
            }
            assert( outline_11_var___class__ == NULL );
            outline_11_var___class__ = tmp_assign_source_125;
        }
        CHECK_OBJECT( outline_11_var___class__ );
        tmp_assign_source_124 = outline_11_var___class__;
        Py_INCREF( tmp_assign_source_124 );
        goto try_return_handler_39;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_39:;
        Py_DECREF( locals_extractors_160 );
        locals_extractors_160 = NULL;
        goto try_return_handler_38;
        // Exception handler code:
        try_except_handler_39:;
        exception_keeper_type_37 = exception_type;
        exception_keeper_value_37 = exception_value;
        exception_keeper_tb_37 = exception_tb;
        exception_keeper_lineno_37 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_extractors_160 );
        locals_extractors_160 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_37;
        exception_value = exception_keeper_value_37;
        exception_tb = exception_keeper_tb_37;
        exception_lineno = exception_keeper_lineno_37;

        goto try_except_handler_38;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_38:;
        CHECK_OBJECT( (PyObject *)outline_11_var___class__ );
        Py_DECREF( outline_11_var___class__ );
        outline_11_var___class__ = NULL;

        goto outline_result_12;
        // Exception handler code:
        try_except_handler_38:;
        exception_keeper_type_38 = exception_type;
        exception_keeper_value_38 = exception_value;
        exception_keeper_tb_38 = exception_tb;
        exception_keeper_lineno_38 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_38;
        exception_value = exception_keeper_value_38;
        exception_tb = exception_keeper_tb_38;
        exception_lineno = exception_keeper_lineno_38;

        goto outline_exception_12;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( extractors );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_12:;
        exception_lineno = 160;
        goto try_except_handler_37;
        outline_result_12:;
        UPDATE_STRING_DICT1( moduledict_extractors, (Nuitka_StringObject *)const_str_plain_OrganisationExtractor, tmp_assign_source_124 );
    }
    goto try_end_15;
    // Exception handler code:
    try_except_handler_37:;
    exception_keeper_type_39 = exception_type;
    exception_keeper_value_39 = exception_value;
    exception_keeper_tb_39 = exception_tb;
    exception_keeper_lineno_39 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_12__bases_orig );
    tmp_class_creation_12__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_12__bases );
    tmp_class_creation_12__bases = NULL;

    Py_XDECREF( tmp_class_creation_12__class_decl_dict );
    tmp_class_creation_12__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_12__metaclass );
    tmp_class_creation_12__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_12__prepared );
    tmp_class_creation_12__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_39;
    exception_value = exception_keeper_value_39;
    exception_tb = exception_keeper_tb_39;
    exception_lineno = exception_keeper_lineno_39;

    goto frame_exception_exit_1;
    // End of try:
    try_end_15:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_90775d2cb57f1deebc6c000eaebfc0ab );
#endif
    popFrameStack();

    assertFrameObject( frame_90775d2cb57f1deebc6c000eaebfc0ab );

    goto frame_no_exception_13;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_90775d2cb57f1deebc6c000eaebfc0ab );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_90775d2cb57f1deebc6c000eaebfc0ab, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_90775d2cb57f1deebc6c000eaebfc0ab->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_90775d2cb57f1deebc6c000eaebfc0ab, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_13:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__bases_orig );
    Py_DECREF( tmp_class_creation_12__bases_orig );
    tmp_class_creation_12__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__bases );
    Py_DECREF( tmp_class_creation_12__bases );
    tmp_class_creation_12__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__class_decl_dict );
    Py_DECREF( tmp_class_creation_12__class_decl_dict );
    tmp_class_creation_12__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__metaclass );
    Py_DECREF( tmp_class_creation_12__metaclass );
    tmp_class_creation_12__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__prepared );
    Py_DECREF( tmp_class_creation_12__prepared );
    tmp_class_creation_12__prepared = NULL;


    return MOD_RETURN_VALUE( module_extractors );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
